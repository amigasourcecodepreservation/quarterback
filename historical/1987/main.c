/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
#include <libraries/dosextens.h>
#include <intuition/intuition.h>
#include "vd0:data.h"

struct IntuitionBase *IntuitionBase;
struct GfxBase *GfxBase;
static struct IntuiMessage *message;
static struct Window *Wdw;
static struct ViewPort *WVP;
struct FileHandle *out;

ULONG  val = 0, val1 = 0;
SHORT  totalnames = 0;
BYTE   Fsize[7];
static UBYTE date[15] = {0,0,47,0,0,47,0,0,32,0,0,58,0,0,0};
static USHORT colormap[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                              0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
SHORT err = 0;

struct tnode *first = 0;

static struct Image img;                                /* scroll */
static struct PropInfo prop = {
   FREEVERT | AUTOKNOB,0,0,0,0x1000,0,0,0,0,0,0 };

static struct Gadget gad = {
    NULL,371,72,20,89,GADGHCOMP,
    GADGIMMEDIATE | FOLLOWMOUSE, PROPGADGET,
    (APTR)&img,NULL,NULL,NULL,(APTR)&prop,1,NULL
   };

UBYTE undo[STRING];
UBYTE dirstr[STRING] = "DH0:";                     /* Dir */
static struct StringInfo dirstring = {
   dirstr,undo,NULL,STRING,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};

static struct Gadget gad1 = {
    &gad,70,45,295,8,GADGHCOMP,RELVERIFY,STRGADGET,
    NULL,NULL,NULL,NULL,(APTR)&dirstring,2,NULL };

UBYTE datestr[15] = "";                      /* Date */
static struct StringInfo datestring = {
   datestr,undo,NULL,15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};

static struct Gadget gad2 = {
    &gad1,485,23,120,8,GADGHCOMP,RELVERIFY,STRGADGET,
    NULL,NULL,NULL,NULL,(APTR)&datestring,3,NULL };
                                             /* file gadgets */
static struct Gadget gad3 = {
    &gad2,70,69,290,8,GADGHCOMP,RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,4,NULL };
static struct Gadget gad4 = {
    &gad3,70,77,290,8,GADGHCOMP,RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,5,NULL };
static struct Gadget gad5 = {
    &gad4,70,85,290,8,GADGHCOMP,RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,6,NULL };
static struct Gadget gad6 = {
    &gad5,70,93,290,8,GADGHCOMP,RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,7,NULL };
static struct Gadget gad7 = {
    &gad6,70,101,290,8,GADGHCOMP,RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,8,NULL };
static struct Gadget gad8 = {
    &gad7,70,109,290,8,GADGHCOMP,RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,9,NULL };
static struct Gadget gad9 = {
    &gad8,70,117,290,8,GADGHCOMP,RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,10,NULL };
static struct Gadget gad10 = {
    &gad9,70,125,290,8,GADGHCOMP,RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,11,NULL };
static struct Gadget gad11 = {
    &gad10,70,133,290,8,GADGHCOMP,RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,12,NULL };
static struct Gadget gad12 = {
    &gad11,70,141,290,8,GADGHCOMP,RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,13,NULL };
static struct Gadget gad13 = {
    &gad12,70,149,290,8,GADGHCOMP,RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,14,NULL };
static struct Gadget gad14 = {
    &gad13,70,157,290,8,GADGHCOMP,RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,15,NULL };
                                                 /* dirX/parent */
static struct Gadget gad15 = {
    &gad14,24,44,22,9,GADGHCOMP,GADGIMMEDIATE | RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,16,NULL };
static struct Gadget gad16 = {
    &gad15,374,44,15,9,GADGHCOMP,GADGIMMEDIATE | RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,17,NULL };
                                   /* restore/backup/verify/all/select */
static struct Gadget gad17 = {
    &gad16,24,21,61,11,GADGHCOMP,GADGIMMEDIATE | RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,18,NULL };
static struct Gadget gad18 = {
    &gad17,109,21,57,11,GADGHCOMP,GADGIMMEDIATE | RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,19,NULL };
static struct Gadget gad19 = {
    &gad18,190,21,64,11,GADGHCOMP,GADGIMMEDIATE | RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,20,NULL };
static struct Gadget gad20 = {
    &gad19,279,21,33,11,GADGHCOMP,GADGIMMEDIATE | RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,21,NULL };
static struct Gadget gad21 = {
    &gad20,337,21,60,11,GADGHCOMP,GADGIMMEDIATE | RELVERIFY,BOOLGADGET,
    NULL,NULL,NULL,NULL,NULL,22,NULL };

static struct NewWindow NewWdw =
   {
      0,0,640,200,
      0,1,
      CLOSEWINDOW | GADGETUP | GADGETDOWN | MOUSEMOVE,
      SMART_REFRESH | ACTIVATE | BORDERLESS
      | WINDOWCLOSE | WINDOWDEPTH | REPORTMOUSE,
      &gad21, NULL,NULL,NULL, NULL,0,0,0,0,
      WBENCHSCREEN
   };

void main()
{
FAST USHORT code, x, y, temp[9];
FAST UBYTE *test;
FAST ULONG i, t, class;
FAST BOOL loop = TRUE, scroll = FALSE;
FAST struct Gadget *igad;
FAST struct FileHandle *file;
FAST struct tnode *mp;

   IntuitionBase=(struct IntuitionBase *)OpenLibrary("intuition.library",1);
   if (IntuitionBase == NULL) exit(FALSE);
   GfxBase = (struct GfxBase *)OpenLibrary("graphics.library",1);
   if (GfxBase == NULL) exit(FALSE);

   if(( Wdw=(struct Window *)OpenWindow(&NewWdw)) == NULL)  exit(FALSE);
   WVP = (struct ViewPort *)ViewPortAddress(Wdw);
   LoadRGB4(WVP,&colormap,4); /* blank screen */

/* Reads picture file and stores it directly into the intuition window
   bit planes. Pic file is a modified Dpaint med res picture. The gadgets
   have no images, but use the screens imagery. */

   file = Open("PIC_DATA", MODE_OLDFILE);
   if(file)
     {
      if( (Read(file,temp,8)) == -1)
       { printf("Read error header\n"); goto quit; }
      if( (Read(file,colormap,temp[3]*2)) == -1)
       { printf("Read error colors\n"); goto quit; }
      for(x=0; x<2; x++)
       {
        test = Wdw->RPort->BitMap->Planes[x];
        if( (Read(file,test,temp[0]*temp[1])) == -1)
         { printf("Read error plane %d\n",x); goto quit; }
       }
      quit:
      Close(file);
     }
    else
     printf("Can't open file\n");

   LoadRGB4(WVP,&colormap,4);      /* Load my screens colors */
   RefreshGadgets(&gad2,Wdw,NULL);

SetAPen(Rp,1);

/* The code below is for the scrolling  */

   Wait( 1L << Wdw->UserPort->mp_SigBit );
while(loop)
 {
  while ( ! (message = (struct IntuiMessage *)GetMsg(Wdw->UserPort)))
   {
     if(scroll)
      {
      if(val != ((totalnames-12) * prop.VertPot) / MAXBODY)
        {
         if(val > ((totalnames-12) * prop.VertPot) / MAXBODY)
          {
           if(val-1 > ((totalnames-12) * prop.VertPot) / MAXBODY)
             {
              val = (totalnames-12) * prop.VertPot / MAXBODY;
              val1=val;            
              skip(val);
             }
            else
             {
              val = (totalnames-12) * prop.VertPot / MAXBODY;
              ScrollRaster(Rp,0,-8, 30, 69,38, 164);
              ScrollRaster(Rp,0,-8, 70, 69,359,164);
              ScrollRaster(Rp,0,-8, 415,69,527,164);
              ScrollRaster(Rp,0,-8, 559,69,607,164);
              mp = first;
              for(x=0; x<val; x++) mp = mp->next;
              if(mp->data.DirType > 0)
               {
                Move(Rp,310,75);
                SetAPen(Rp,3);
                Text(Rp," (dir)",6);
                SetAPen(Rp,1);
               }
              Move(Rp,70,75);
              t=0;
              while(mp->data.FileName[t++]);
              Text(Rp,&mp->data.FileName[0],t-1);
              Move(Rp,30,75);
              Text(Rp," ",1);
              Move(Rp,415,75);
              GetDate(mp->data.Date.ds_Days,mp->data.Date.ds_Minute);
              Text(Rp,date,14);
              Move(Rp,559,75);
              if(mp->data.DirType < 0)
                 {
                  to_ascii(mp->data.Size);              
                  Text(Rp,Fsize,6);
                 }
               else
                 Text(Rp,"      ",6);         
              Move(Rp,30,75);
              if(mp->data.Flag == 1)
                 Text(Rp,"X",1); 
               else
                 Text(Rp," ",1);     
             }
          }
         else
          {
           if(val+1 < ((totalnames-12) * prop.VertPot) / MAXBODY)
             {
              val = (totalnames-12) * prop.VertPot / MAXBODY;
              val1=val;
              skip(val);
             }
            else
             {
              val = (totalnames-12) * prop.VertPot / MAXBODY;
              ScrollRaster(Rp,0,8, 30, 69,38, 164);
              ScrollRaster(Rp,0,8, 70, 69,359,164);
              ScrollRaster(Rp,0,8, 415,69,527,164);
              ScrollRaster(Rp,0,8, 559,69,607,164);
              mp = first;
              for(x=0; x<val+11; x++) mp = mp->next;
              if(mp->data.DirType > 0)
               {
                Move(Rp,310,163);
                SetAPen(Rp,3);
                Text(Rp," (dir)",6);
                SetAPen(Rp,1);
               }
              Move(Rp,70,163);
              t=0;
              while(mp->data.FileName[t++]);
              Text(Rp,&mp->data.FileName[0],t-1);
              Move(Rp,30,163);
              Text(Rp," ",1);
              Move(Rp,415,163);
              GetDate(mp->data.Date.ds_Days,mp->data.Date.ds_Minute);
              Text(Rp,date,14);
              Move(Rp,559,163);
              if(mp->data.DirType < 0)
                 {
                  to_ascii(mp->data.Size);              
                  Text(Rp,Fsize,6);
                 }
               else
                 Text(Rp,"      ",6);
              Move(Rp,30,163);
              if(mp->data.Flag == 1)
                 Text(Rp,"X",1); 
               else
                 Text(Rp," ",1); 
             }
          }
        }
      scroll = FALSE;
      }
     WaitPort(Wdw->UserPort);
   }
    igad = (struct Gadget *)message->IAddress;
    class = message->Class;
    code  = message->Code;
    ReplyMsg(message);
    switch(class)
      {
       case MOUSEMOVE:   if(totalnames > 12) scroll = TRUE;
                         break;
       case CLOSEWINDOW: loop = FALSE;
                         break;
       case GADGETUP:
       case GADGETDOWN:
          switch(i = igad->GadgetID)
           {
            case 1:                               /* Scroll Gadget */
                    if(totalnames > 12) scroll = TRUE;
                    break;
            case 2:                               /* Dir String    */

/* This section selects the files, scroll files.
   It now needs to save the selections */
 
                    freenode();
                    out = Open( "TEMP", MODE_NEWFILE );   
                    SetAPen(Rp,0);
                    RectFill(Rp,30, 69,38, 164);
                    RectFill(Rp,70, 69,359,164);
                    RectFill(Rp,415,69,527,164);
                    RectFill(Rp,559,69,607,164);
                    SetAPen(Rp,1);
                    if(out != NULL)
                      {
                        DoSize( &dirstring.Buffer[0] ,1);
                        Close(out);                           
                        sort("TEMP");
                        if(totalnames < 13)
                          y = MAXBODY;
                         else
                          y = (MAXBODY/totalnames*12) + (MAXBODY/12);
                        ModifyProp(&gad,Wdw,0,FREEVERT | AUTOKNOB,0,0,0,y);
                        val = 0;
                        y = (totalnames > 12) ? 12 : totalnames;
                        mp = first;
                        for(x=0; x<y; x++)
                          {
                            Move(Rp,70,75+x*8);
                            SetAPen(Rp,3);
                            if(mp->data.DirType < 0)
                       Text(Rp,"                                    ",36);
                             else
                       Text(Rp,"                               (dir)",36);
                            Move(Rp,70,75+x*8);
                            SetAPen(Rp,1);
                            t=0;
                            while(mp->data.FileName[t++]);
                            Text(Rp,&mp->data.FileName[0],t-1);
                            Move(Rp,415,75+x*8);
                            GetDate(mp->data.Date.ds_Days,
                                    mp->data.Date.ds_Minute);
                            Text(Rp,date,14);
                            Move(Rp,559,75+x*8);
                            if(mp->data.DirType < 0)
                              {
                               to_ascii(mp->data.Size);              
                               Text(Rp,Fsize,6);
                              }
                             else
                               Text(Rp,"      ",6);
                            mp = mp->next;
                          }
                      }
                     else
                      printf("Error = %d\n",IoErr() );
                    break;                    
            case 3:                               /* Date String   */
                    break;
            case 4:  case 5:  case 6:  case 7:    /* File Gadgets  */
            case 8:  case 9:  case 10: case 11:
            case 12: case 13: case 14: case 15:
/* used to select the files. Doesn't yet save which files were selected */
                    mp=first;
                    for(x=0; x<val1; x++)
                      mp = mp->next;
                    for(x=0; x<i-4; x++)
                      mp = mp->next;
                    if(mp->data.Flag==0)
                      {
                        mp->data.Flag = 1;
                        Move(Rp,30,75+(i-4)*8);
                        Text(Rp,"X",1);   
                      }
                     else
                      {
                        mp->data.Flag = 0;
                        Move(Rp,30,75+(i-4)*8);
                        Text(Rp," ",1);   
                      }
                    break;

/* these gadgets are to select the whole dir and go to back to the parent */

            case 16:                              /* Dir X         */
                    break;
            case 17:                              /* Parent        */
                    break;

/* these gadgets are the flags to tell you what operation is selected */

            case 18:                              /* Restore       */
                    break;
            case 19:                              /* Backup        */
                    break;
            case 20:                              /* Verify        */
                    break;
            case 21:                              /* All           */
                    break;
            case 22:                              /* Select        */
                    break;
           }
      } /* ---------------------> switch(class) */
 } /* --------------------------> while(loop)   */

   freenode();
   CloseWindow(Wdw);
   CloseLibrary(GfxBase);
   CloseLibrary(IntuitionBase);

}

skip(ttt)     /* Used to skip around for large scrolls */
SHORT ttt;
{
struct tnode *mp;
LONG x,t;

   mp = first;
   for(x=0; x<ttt; x++)
      mp = mp->next;
   for(x=0; x<12; x++)
    {
     Move(Rp,70,75+x*8);
     SetAPen(Rp,3);
     if(mp->data.DirType < 0)
       Text(Rp,"                                    ",36);
      else
       Text(Rp,"                               (dir)",36);
     Move(Rp,70,75+x*8);
     SetAPen(Rp,1);
     t=0;
     while(mp->data.FileName[t++]);
     Text(Rp,&mp->data.FileName[0],t-1);
     Move(Rp,415,75+x*8);
     GetDate(mp->data.Date.ds_Days,mp->data.Date.ds_Minute);
     Text(Rp,date,14);
     Move(Rp,559,75+x*8);
     if(mp->data.DirType < 0)
        {
         to_ascii(mp->data.Size);              
         Text(Rp,Fsize,6);
        }
      else
        Text(Rp,"      ",6);
     Move(Rp,30,75+x*8);
     if(mp->data.Flag==1)
        Text(Rp,"X",1); 
       else
        Text(Rp," ",1); 
     mp = mp->next;
     }
return(0);
}

GetDate(v0,v1)      /* Converts the datestamp to ascii */
LONG v0,v1;
{
LONG d,mn;
UBYTE m=1,y=78,h;
static UBYTE days[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };

   d=v0+1;
   while(d > 0)
    {
      if(y/4*4 == y)
        days[1] = 29;
       else
        days[1] = 28;
      if(days[m-1] < d)
        d -= days[m-1];
       else
        break;
      if(++m > 12)
        {
         y++;
         m=1;
        }
    }

   date[0] = m/10+48; date[1] = m%10+48;         /* month */
   date[3] = d/10+48; date[4] = d%10+48;         /*  day  */
   if(y>99) y-=100;                              /* past year 1999 */
   date[6] = y/10+48; date[7] = y%10+48;         /* year  */
   h = v1/60; mn = v1%60;
   date[9] = h/10+48; date[10] = h%10+48;        /* hours */
   date[12] = mn/10+48; date[13] = mn%10+48;     /* mins  */
return(0);
}

to_ascii(value)
LONG value;
{
LONG x, y=5;
BYTE z,d=0;

  Fsize[6] = NULL;
  for(x=0; x<6; x++)
   {
     z = (value % 10) + '0';
     value /= 10;
     Fsize[y--] = z;
     if(value < 1)
       {
         if(d)
           Fsize[y+1] = 32;
         d=1;
       }
   }  
return(0);
}



   
