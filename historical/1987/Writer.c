/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/

/* 

This is the heart of the program. It will write files to the disk one
after the other, track by track. The Format is 'NDOS' so DOS won't mess
with the disk, then the disk number starting at 0, then the name followed
by either a $FF if it's a DIR, or $FE for a file, then one longword for the
file size, followed by the file data. After that if you find a $FF that
will be the end of that DIR, else another name.Look below,(ignore commas)

NDOS,0,samplefile,FE,00000123,$123 bytes,dir1,FF,file1,FE,00000456,
$456 bytes,file2,FE,00000789,$789 bytes,FF

For the harddisk backup you need a datestamp after the disk number so as
not to mix up different sets of backups.

*/

#include <exec/types.h>
#include <exec/io.h>
#include <devices/trackdisk.h>
#include <libraries/dosextens.h>
#include <intuition/intuition.h>
#include <exec/memory.h>
#include <ctype.h>
#include <stdio.h>

extern struct MsgPort *CreatePort();
extern struct IOStdReq *CreateStd();
extern struct FileHandle *Open();
extern struct Process *FindTask();
extern void *AllocMem();

struct Process *Me;
struct MsgPort  *diskport1 = 0;
struct IOStdReq *diskreq1  = 0;
struct IntuitionBase *IntuitionBase;
LONG  size = 0;
UBYTE *buff1;
UBYTE disk=1;
SHORT err;

#define MAX 5632     /* 512*11 End of Buffer */

void main( argc, argv )
int argc;
char *argv[];
{
LONG drv;

  drv = atoi(argv[2]);
  if(drv < 0 || drv > 3)
    { printf("Drive must be between 0 and 3\n"); exit(0); }

  IntuitionBase = (struct IntuitionBase *)OpenLibrary("intuition.library",0);
     if(IntuitionBase==NULL)  { printf("no Intuition\n"); exit(0); }
  diskport1 = (struct MsgPort *)CreatePort(0,0);
     if(diskport1==0)  { printf("no diskport\n"); exit(0); }
  diskreq1  = (struct IOStdReq *)CreateStdIO(diskport1);
     if(diskreq1==0)  { printf("no diskreq\n"); goto out3; }

   buff1 = (UBYTE *)AllocMem(512*11, MEMF_CHIP|MEMF_CLEAR);
     if( !buff1 )  { printf("Can't allocate buffer 1\n"); goto out2; }

   if((err = OpenDevice(TD_NAME, drv, diskreq1, 0)) != 0)
     { printf("no Drive %d  %d\n",drv,err); goto out1; }

     *(buff1+size++) = 0x4E;
     *(buff1+size++) = 0x44;
     *(buff1+size++) = 0x4F;
     *(buff1+size++) = 0x53;
     *(buff1+size++) = disk++;  /* disk number */

/* also needs a datestamp so this group of disks will be unique */

   Me = FindTask(0);

   DoDir( argv[1], 0 );
   if(!err)
     {
     WriteTrk();
     printf("Write successful\n");
     }
   else
     printf("\nWrite failed.\n");
   MotorOff();
   CloseDevice(diskreq1);
out1:
   FreeMem(buff1,512*11);
out2:
   DeleteStdIO(diskreq1);
out3:
   DeletePort(diskport1);
}                            /* End of main */

DoDir( name, depth )
UBYTE *name;
ULONG depth;
{
   struct FileInfoBlock *fib;
   ULONG lock, oldlock, x;
   BOOL success;

   fib = (struct FileInfoBlock *)
          AllocMem( sizeof( struct FileInfoBlock ),MEMF_PUBLIC|MEMF_CLEAR );
   if(!fib)
      { printf("\nCan't allocate memory.\n\n"); err=1; return(0); }
   else
    {
      lock = Lock( name, ACCESS_READ );
      if(!lock)
         {
         err=1;
         printf("\nCan't find %s Directory.\n\n",name);
         FreeMem( fib, sizeof( struct FileInfoBlock ) );
         return(0);
         }
      else
       {
         oldlock = CurrentDir( lock );
         success = Examine( lock, fib );
         success = ExNext( lock, fib );
         while ( success )
         {
            for( x=0; x<depth; x++) printf("\t");
            printf("%s",fib->fib_FileName);
            if ( fib->fib_DirEntryType < 0 )             /* file */
               {
               Dump(fib->fib_FileName,fib->fib_Size,1);
               if(!err)
                  printf(" ...copied\n");
               }
            if ( fib->fib_DirEntryType > 0 )             /* directory */
               {
               printf("  (dir)");
               Dump(fib->fib_FileName,fib->fib_Size,0);
               if(!err)
                 {
                  printf("  [created]\n");
                  DoDir( fib->fib_FileName, depth+1 );
                 }
               }
            if(err) break;
            success = ExNext( lock, fib );
         }
         lock = CurrentDir( oldlock );
         UnLock( lock );
       }
      FreeMem( fib, sizeof( struct FileInfoBlock ) );
    }
    if(!err)
      {
      *(buff1+size++) = 0xFF;       /* Write $FF to mark end of dir */
      if(size >= MAX) WriteTrk();
      }
return(0);
}

Dump(fname,fsize,typ)
UBYTE  *fname;
ULONG fsize;
BOOL  typ;
{
SHORT i=0;
SHORT s;
struct FileHandle *src;

   while(fname[i] > 0)  /* put name in buffer */
      {
      *(buff1+size++) = fname[i++];
      if(size >= MAX) WriteTrk();
      if(err) return(0);
      }
   if(!typ)             /*  Put $FF at end to mark dir */
      {
      *(buff1+size++) = 0xFF;
      if(size >= MAX) WriteTrk();
      return(0);
      }
   if(typ)              /*  Put $FE at end to mark file */
      {
      *(buff1+size++) = 0xFE;
      if(size >= MAX) WriteTrk();
      if(err) return(0);
      i=4;
      while(i--)        /* Put file size at end of name */
         {
         *(buff1+size++) = fsize >> i*8  & 0xFF;
         if(size >= MAX) WriteTrk();
         if(err) return(0);
         }
      if((src = Open(fname, MODE_OLDFILE)) == NULL)
         { printf("\nCan't open file %s\n\n",fname); err=1; return(0); }

/*  MAX-size is the room left in buffer, so try to read as much as you    */
/*  can at a time. s is the amount of bytes you actually read so add that */
/*  to size. Any time you put anything in the buffer (add to size) check  */
/*  to see if it is greater or equal than MAX, if so write it out.        */

      while((s = Read(src, buff1+size, MAX-size) ) >0 )
         {
         if(s == -1)
            {
            printf("\nError %d while reading %s.\n\n",IoErr(),fname);
            err=1;
            Close(src);
            return(0);
            }
         size += s;
         if(size >= MAX) WriteTrk();
         if(err) break;
         }
      Close(src);
      }
return(0);
}

WriteTrk()      /* Trak increments itself to the next track so never */
{               /* worry about which track you are on.               */
static SHORT Trak = 0;

   size = 0;
   diskreq1->io_Length  = TD_SECTOR * NUMSECS;
   diskreq1->io_Data    = (APTR)buff1;
   diskreq1->io_Command = TD_FORMAT;
   diskreq1->io_Offset  = TD_SECTOR * Trak * 11;
   DoIO(diskreq1);
   if((err = diskreq1->io_Error) != 0)
      printf("\nError No. %d on TRACK %d\n",err,Trak);
   Trak++;
   if(Trak > 159)
    {
     MotorOff();
     if(!request("Disk Full - Insert Next Disk"))
        { err=1; return(0); }
     while(TRUE)
      {
       diskreq1->io_Command = TD_CHANGESTATE;
       DoIO(diskreq1);
       if(diskreq1->io_Actual == 0)
         break;
       if(!request("No Disk in Drive"))
         { err=1; return(0); }
      }
     while(TRUE)
      {
       diskreq1->io_Command = TD_PROTSTATUS;
       DoIO(diskreq1);
       if(diskreq1->io_Actual == 0)
         break;
       if(!request("Write Protect On"))
         { err=1; return(0); }
      }
     *(buff1+size++) = 0x4E;   /* Put NDOS in at start of buffer  */
     *(buff1+size++) = 0x44;   /* so that the DOS won't mess with */
     *(buff1+size++) = 0x4F;   /* the disk. Gets written to the   */
     *(buff1+size++) = 0x53;   /* boot block.                     */
     *(buff1+size++) = disk++;
     Trak = 0;
    }
return(0);
}

static struct IntuiText yes ={0,1,JAM2,5,3,NULL,"OK",    NULL};
static struct IntuiText no  ={0,1,JAM2,5,3,NULL,"CANCEL",NULL};
static struct IntuiText txt ={0,1,JAM2,8,5,NULL,NULL,    NULL};

request(text)
BYTE *text;
{
   txt.IText = text;
   return((long)AutoRequest(Me->pr_WindowPtr,&txt,&yes,&no,
             0,0,(IntuiTextLength(&txt)+50),50));
}

MotorOff()
{
    diskreq1->io_Length  = 0;
    diskreq1->io_Command = TD_MOTOR;
    DoIO(diskreq1);
return(0);
}

