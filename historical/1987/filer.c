/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/

#include <exec/types.h>
#include <exec/memory.h>
#include <libraries/dosextens.h>
#include <stdio.h>
#include "vd0:data.h"

extern SHORT err;
extern SHORT totalnames;
LONG tot = 0;

extern struct FileHandle *Open();
extern struct FileHandle *out;
extern struct tnode *first;

/* Read the entire DIR and put it into a structured list.
   This function is a recursive call and uses "depth"
   to measure it's depth.    */

DoSize( name, depth )  
UBYTE *name;
ULONG depth;
{
   struct MyBlockInfo *my = NULL;
   struct MyFib *my_fib = NULL;
   ULONG lock = NULL,  oldlock;
   BOOL success;
   LONG x;

   my = (struct MyBlockInfo *)
         AllocMem( sizeof( struct MyBlockInfo ),MEMF_PUBLIC|MEMF_CLEAR );
   my_fib = (struct MyFib *)
         AllocMem( sizeof( struct MyFib ),MEMF_PUBLIC|MEMF_CLEAR );
   if(!my || !my_fib)
      { printf("\nCan't allocate memory.\n\n"); err = 1; return(0); }
   if ( my )
   {
      lock = Lock( name, ACCESS_READ );
      if(!lock)
        {
         err = 1;
         printf("\nCan't Find %s Directory\n\n",name);
         goto quit;
        }
      if(lock)
      {
         oldlock = CurrentDir( lock );
         success = Examine( lock, &my->fib );
         success = ExNext ( lock, &my->fib );

         while ( success )
         {
                        my->Flag = depth;
            my_fib->Flag           = my->Flag;
            my_fib->DirType        = my->fib.fib_DirEntryType;
            for(x=0; x<30; x++)
              my_fib->FileName[x]  = my->fib.fib_FileName[x];
            my_fib->Protect        = ~my->fib.fib_Protection;
            my_fib->Size           = my->fib.fib_Size;
            my_fib->Date.ds_Days   = my->fib.fib_Date.ds_Days;
            my_fib->Date.ds_Minute = my->fib.fib_Date.ds_Minute;

            if( ( Write( out,my_fib,sizeof(struct MyFib) ) ) == -1)
              { printf("Write file failed\n"); err=1; goto quit1; }

            if( my->fib.fib_DirEntryType > 0 )
               DoSize(my->fib.fib_FileName,depth+1);

            if(err==1) break;
            tot++;
            success = ExNext( lock, &my->fib );
         }
         quit1:
         lock = CurrentDir( oldlock );
         if(lock) { UnLock(lock);  lock = NULL; }
      }
      quit:
      FreeMem( my_fib, sizeof( struct MyFib ) );
      FreeMem( my, sizeof( struct MyBlockInfo ) );
   }
 return(0);
}

/*
   sort() takes the file 'out' and stores it into a dynamically allocated
   one way linked list. Then calls order() to sort the files to the top.
   then calls sort2() twice, once to sort the filenames then the dirs.  
*/

sort(name)  
UBYTE *name;
{
struct tnode *temp=0, *last;
LONG files=0, dirs=0, tmp;

  if((out = Open(name, MODE_OLDFILE)) == NULL)
    { printf("Can't open file  error = \n", IoErr()); err=1; return(0); }

   first = (struct tnode *)AllocMem( sizeof( struct tnode ),MEMF_CLEAR );
   if(!first) { printf("\nCan't allocate first.\n\n"); Close(out);
                        err=1; return(0); }

   first->next = NULL;
   temp = first;

   while(TRUE)
     {
      if((tmp = Read(out, &temp->data, sizeof(struct MyFib))) == -1)
        { printf("Read Error %d\n",IoErr()); err=1; goto quit; }
      if(tmp==0)   break;
      if(temp->data.Flag == 1)
       {
        last = (struct tnode *)AllocMem( sizeof( struct tnode ),MEMF_CLEAR );
        if(!last) { printf("\nAllocation Err.\n"); err=1; goto quit; }

        temp->next = last;
        temp = last;
        temp->next = NULL;
       }
     }
   if(temp == first)
    { printf("No Entries\n"); err=1; goto quit; }

   temp = first;      /* Free up the unused node at the end */
   while(TRUE)
    {
     last = temp;
     temp = temp->next;
     if(temp->next == NULL)
      {
       last->next = NULL;
       FreeMem(temp, sizeof(struct tnode));
       break;
      }
    }

   temp = first;                 /* Count files */
   while(temp)
    {
     if(temp->data.DirType < 0)  files++;
     temp = temp->next;
    }

   temp = first;                 /* Count dirs */
   while(temp)
    {
     if(temp->data.DirType > 0)  dirs++;
     temp = temp->next;
    }

   totalnames = dirs+files;
   order(first);                    /* put all files at top of list */
   sort2(first,files);              /* sort all files in list */
   temp = first;
   for(tmp=0; tmp<files; tmp++)     /* skip through files to dirs */
     temp = temp->next;
   sort2(temp,dirs);                /* sort all dirs in list */
   temp = first;

quit:
  if(err==1)
     freenode();

Close(out);
return(0);
}

freenode()  /* Free all memory for One Way Linked List */
{
struct tnode *tmp1, *tmp2;

   tmp1 = first;
   while(tmp1)
    {
     tmp2 = tmp1->next;
     FreeMem(tmp1, sizeof(struct tnode));
     tmp1 = tmp2;
    }
return(0);
}

order(temp)        /* Put all the files at the top of the list */
struct tnode *temp;
{
struct tnode *w, *x, *y;
LONG a=0;
USHORT *one, *two, tp;

 y = temp;
 w = temp;
 while(w)
  {
   x = w;
   if(x->data.DirType > 0)      /* is dir ??? */
    {
     y = x->next;
     while(y)                   /* find file to swap with */
        {
           if(y->data.DirType < 0)
             {
               one = (USHORT *)&x->data;
               two = (USHORT *)&y->data;
               for(a=0; a<21; a++)
                 {
                  tp = *(one+a);
                  *(one+a) = *(two+a);
                  *(two+a) = tp;
                 }
               break;
             }
           y = y->next;
        }
    }
   if(!y) break;
   w = w->next;
  }
return(0);
}

sort2( temp, num)    /* Alphabetize list */
struct tnode *temp;
LONG num;
{
struct tnode *w, *x;
LONG   a=0, b=0, c, d;
USHORT *one, *two, tp;

  if(num > 1)
   {
     while(TRUE)
      {
       d = 0;
       w = temp;
       for(b=0; b<num-1; b++)
         {
           x = w->next;
           c = comp(&w->data.FileName,x->data.FileName);
           if(c == 1)
             {
                d++;
                one = (USHORT *)&w->data;
                two = (USHORT *)&x->data;
                for(a=0; a<21; a++)
                  {
                   tp = *(one+a);
                   *(one+a) = *(two+a);
                   *(two+a) = tp;
                  }
             }
           w = w->next;
         }
       if(d==0) break;
      }
   }
return(0);
}

comp(s,t)        /* Compare for Sort2() */
register char *s, *t;
{
register test;

   while(TRUE)
    {
     test = tolower(*s)-tolower(*t);
     if(test<0)
       return(-1);
      else
       {
        if(test>0)
          return(1);
         else
          {
           if(*s == '\0')
             return(0);
           t++;
           s++;
          }
       }
    }
return(0);
}

tolower(x)    /* For comp() */
register x;
{
   if( (x >= 'A') && (x <= 'Z') )
     return(x - 'A' + 'a');
return(x);
}


