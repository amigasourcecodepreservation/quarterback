/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/

#include <exec/types.h>
#include <exec/io.h>
#include <devices/trackdisk.h>
#include <libraries/dosextens.h>
#include <exec/memory.h>
#include <ctype.h>
#include <stdio.h>
#include <intuition/intuition.h>

extern struct MsgPort *CreatePort();
extern struct IOStdReq *CreateStd();
extern struct FileHandle *Open();
extern struct Process *FindTask();
extern void *AllocMem();

struct MsgPort  *diskport1 = 0;
struct IOStdReq *diskreq1  = 0;
struct Process *Me;
struct IntuitionBase *IntuitionBase;
LONG  size = 0;
UBYTE *buff1;
UBYTE disk=1;
SHORT err;

#define MAX 5632     /* 512*11 End of Buffer */

void main( argc, argv )
int argc;
char *argv[];
{
LONG drv;

  drv = atoi(argv[1]);
  if(drv < 0 || drv > 3)
   { printf("Drive must be between 0 and 3\n"); exit(0); }

  IntuitionBase = (struct IntuitionBase *)OpenLibrary("intuition.library",0);
     if(IntuitionBase==NULL)  { printf("no Intuition\n"); exit(0); }
  diskport1 = (struct MsgPort *)CreatePort(0,0);
     if(diskport1==0)  { printf("no diskport\n"); exit(0); }
  diskreq1  = (struct IOStdReq *)CreateStdIO(diskport1);
     if(diskreq1==0)  { printf("no diskreq\n"); goto out3; }

   buff1 = (UBYTE *)AllocMem(512*11, MEMF_CHIP|MEMF_CLEAR);
     if( !buff1 )  { printf("Can't allocate buffer 1\n"); goto out2; }

   if((err = OpenDevice(TD_NAME, drv, diskreq1, 0)) != 0)
     { printf("no Drive %d  %d\n",drv,err); goto out1; }

   Me = FindTask(0);
   ReadTrk();
   if(err)
      printf("\nRead failed\n\n");
    else
      {
      DoDir( argv[2], 0 );
      if(err)
         printf("Read failed.\n\n");
      else
         printf("Read successful.\n\n");
      }
   MotorOff();
   CloseDevice(diskreq1);
out1:
   FreeMem(buff1,512*11);
out2:
   DeleteStdIO(diskreq1);
out3:
   DeletePort(diskport1);
}                            /* End of main */

DoDir( name, depth )
UBYTE *name;
ULONG depth;
{
   struct FileInfoBlock *fib;
   ULONG flock, lock, oldlock, x, i, terr;
   UBYTE fname[35];
   UBYTE tmp;

   fib = (struct FileInfoBlock *)
          AllocMem( sizeof( struct FileInfoBlock ),MEMF_PUBLIC|MEMF_CLEAR );
   if(!fib)
    { printf("\nCan't allocate memory.\n\n"); err=1; return(0); }
   else
    {
      lock = Lock( name, ACCESS_READ );
      if(!lock)
         {
         err=1;
         printf("\nCan't find %s Directory.\n\n",name);
         FreeMem( fib, sizeof( struct FileInfoBlock ) );
         return(0);
         }
      else
       {
         oldlock = CurrentDir( lock );
         while ( *(buff1+size) != 0xFF )
         {
            for( x=0; x<depth; x++) printf("\t");
            i=0;
            for(;;)
              {
              tmp = *(buff1+size);
              if( tmp == 0xFF ) break;
              if( tmp == 0xFE ) break;
              fname[i++] = *(buff1+size++);
              if(i>30)
                { printf("\nDisk data corrupt\n\n"); err=1; break; }
              if(size >= MAX) ReadTrk();
              if(err) break;
              }
            fname[i] = 0;       /* pad name */
            size++;
            if(size >= MAX) ReadTrk();
            if(err) break;
            switch(tmp)
              {
              case 0xFE:
                        printf("%s ",fname);
                        Dump(fname);
                        if(!err)
                           printf("...copied\n");
                        break;
              case 0xFF:
                        printf("%s  (dir)",fname);
                        flock = CreateDir(fname);
                        if(!flock)
                          {
                          terr = IoErr();
                          if(terr == 203 || terr == 216 || terr == 202)
                            {
                            UnLock(flock);
                            printf("   [exists]\n");
                            DoDir( fname, depth+1 );
                            }
                           else
                            {
                            err=1;
                            printf("\nError %d creating Directory.\n",terr);
                            }
                          }
                         else
                          {
                          UnLock(flock);
                          printf("   [created]\n");
                          DoDir( fname, depth+1 );
                          }
                        break;
              default:
                        err=1;
                        printf("\nDisk data corrupt\n\n");
              }
            if(err) break;
         }
         lock = CurrentDir( oldlock );
         UnLock( lock );
       }
       FreeMem( fib, sizeof( struct FileInfoBlock ) );
    }
    if(!err)
     {
      size++;
      if(size >= MAX) ReadTrk();
     }
return(0);
}

Dump(fname)
UBYTE  *fname;
{
SHORT i;
SHORT s;
ULONG tot, fsize=0;
struct FileHandle *src;

      for(i=0; i<4; i++)        /* Get file size */
         {
         fsize = fsize * 0x100 + *(buff1+size++);
         if(size >= MAX) ReadTrk();
         if(err) return(0);
         }
      if((src = Open(fname, MODE_NEWFILE)) == NULL)
         { printf("\nCan't open file %s\n",fname); err=1; return(0); }
      while(fsize)
        {
        if(fsize > (MAX-size))
          { tot = MAX-size; fsize -= tot; }
         else
          { tot = fsize; fsize -= tot; }
        s = Write(src, buff1+size, tot);
        if(s == -1)
          {
          printf("\nError %d while writing %s\n\n",IoErr(),fname);
          err=1;
          Close(src);
          return(0);
          }
        size += s;
        if(size >= MAX) ReadTrk();
        if(err) break;
        }
      Close(src);
return(0);
}

ReadTrk()
{
static SHORT Trak = 0;

again:
   size = (Trak == 0) ? 5 : 0;
   if(Trak==0)
     {
        while(TRUE)
         {
          diskreq1->io_Command = TD_CHANGESTATE;
          DoIO(diskreq1);
          if(diskreq1->io_Actual == 0)
            break;
          if(!request("No Disk in Drive"))
            { err=1; return(0); }
         }
     }
   if(Trak>159)         /* last track on this disk */
     {
       disk++;         /* increment disk number */
       Trak = 0;       /* Reset Trak to 0 */
       size = 5;       /* set buffer pointer to 1st byte of data */
       MotorOff();
       if(!request("Insert Next Disk"))
          { err=1; return(0); }
       while(TRUE)
        {
         diskreq1->io_Command = TD_CHANGESTATE;
         DoIO(diskreq1);
         if(diskreq1->io_Actual == 0)
           break;
         if(!request("No Disk in Drive"))
           { err=1; return(0); }
        }
     }
   diskreq1->io_Length  = TD_SECTOR * NUMSECS;
   diskreq1->io_Data    = (APTR)buff1;
   diskreq1->io_Command = CMD_READ;
   diskreq1->io_Offset  = TD_SECTOR * Trak * 11;
   DoIO(diskreq1);
   if((err = diskreq1->io_Error) != 0)
      printf("\nError No. %d on TRACK %d\n",err,Trak);
   if(Trak==0)
     {
      if(buff1[4] != disk)
        {
          if(!request("Incorrect Disk"))
            { err=1; return(0); }
          goto again;
        }
     }
   Trak++;
return(0);
}

MotorOff()
{
    diskreq1->io_Length  = 0;
    diskreq1->io_Command = TD_MOTOR;
    DoIO(diskreq1);
return(0);
}

static struct IntuiText yes ={0,1,JAM2,5,3,NULL,"OK",    NULL};
static struct IntuiText no  ={0,1,JAM2,5,3,NULL,"CANCEL",NULL};
static struct IntuiText txt ={0,1,JAM2,8,5,NULL,NULL,    NULL};

request(text)
BYTE *text;
{
   txt.IText = text;
   return((long)AutoRequest(Me->pr_WindowPtr,&txt,&yes,&no,
             0,0,(IntuiTextLength(&txt)+50),50));
}


