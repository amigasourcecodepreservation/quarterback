;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*							*
*	QUARTERBACK TOOLS				*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1990 Central Coast Software	*
*	424 Vista Avenue, Golden, CO 80401		*
*							*
*********************************************************

	INCLUDE	"vd0:MACROS.ASM"
	INCLUDE "vd0:EQUATES.ASM"
	INCLUDE	"vd0:BOXES.ASM"

	XDEF	MAIN,MLoop,MLoopErr
	XDEF	SysBase,_SysBase,DosBase
	XDEF	IntuitionBase,GraphicsBase
	XDEF	RexxBase
	XDEF	RefreshFunctions
	XDEF	DevGadHit
	XDEF	AboutTools
	XDEF	QuitTools
	XDEF	FunExit
	XDEF	LoadParams,SaveParams
	XDEF	MenuPk
	XDEF	VolMnu,DrvMnu
;	XDEF	EnableMenu,DisableMenu
	XDEF	Function3,Function5
	XDEF	WaitForAction,ProcessMsg
	XDEF	ProcHit,CanHit,PauseHit
	XDEF	RProcHit,RCanHit,RSkipHit
	XDEF	RYesHit,RNoHit
	XDEF	RexVol,RexDrv
	XDEF	RexAbtTls

	XDEF	ToolsTask
	XDEF	Window
	XDEF	TMP.,TMP2.,TMP3.
	XDEF	FontPtr
	XDEF	MSG_CLASS,MSG_CODE,GAD_PTR
	XDEF	VolNameBuf.
	XDEF	DispDevFlg
	XDEF	FunGadList
	XDEF	ProcFlg
	XDEF	CancelFlg
	XDEF	AbortFlg
	XDEF	SkipFlg
	XDEF	YesFlg
	XDEF	PauseFlg
	XDEF	VolumeFlg

* Parameters defined here and saved/restored

	XDEF	CacheBufMode
	XDEF	ReportPath.
	XDEF	ScreenMode,ReportMode
	XDEF	WBModeFlg

* Externals in BITMAP.ASM

	XREF	FreeBMBuffers

	XREF	BitmapFlg

* EXTERNALS IN GADGETS.ASM

	XREF	WINI
	XREF	T80
	XREF	FunPGad,FunCGad,AbortGad
	XREF	DevGList,Fun1,Fun10,Fun20,Fun30
	XREF	DnGad,PropGad

	XREF	ReqBRptGad,ReqRRptGad,BRptInfo,RRptInfo,DevInfo
	XREF	BR1Info,BR2Info
	XREF	RptPathTxt
	XREF	DevGadTbl
	XREF	AboutTxt
	XREF	ReqOK,REQTXT
	XREF	PauseTxt,ContinueTxt
	XREF	PauseGad

* Externals in FUN.ASM

	XREF	AddGadgets,RemoveGadgets,CLRWIN
	XREF	REFRESH,CloseSafely
	XREF	RefreshAllGads

* Externals in Beep.asm

	XREF	AudioCleanup,AudioBeep

* Externals in MESSAGES.ASM

	XREF	VolLst.,DrvLst.
	XREF	ClkVol.,ClkDrv.
	XREF	FunVol.,FunDrv.
	XREF	Fun1.,Fun3.,Fun4.
	XREF	Fun5.,Fun6.,Fun7.
	XREF	Fun10.,Fun11.,Fun12.
	XREF	Fun21.,Fun22.,Fun23.
	XREF	Fun30.,Fun31.,Fun32.
	XREF	DiffDrv.,DiffVol.
	XREF	MainMenu.,BadMenu.
	XREF	ReorgMenu.,RepairMenu.
	XREF	RetMenu.
	XREF	RunErr.
	XREF	ArexErr.
	XREF	ALibErr.

* Externals in MENUS.ASM

	XREF	ToolsMenu,FunctionMenu
	XREF	RepairMenu,ReorgMenu
	XREF	SCNMODE_ITEMS
	XREF	RPT_ITEMS
	XREF	MEMORY_ITEMS
	XREF	MODE_ITEMS

* Externals in Device.asm

;	XREF	CheckDeviceList
	XREF	LoadDeviceParams
	XREF	CheckDriveStatus
	XREF	CheckVolumeStatus
	XREF	CalcVolParams
	XREF	BldDevGadList
	XREF	VolumeNamePtr
	XREF	OpenDriver,CloseDriver
	XREF	ScanVolNames
	XREF	InhibitDos
	XREF	ForceOn

	XREF	DevErrFlg
	XREF	VolumeCnt
	XREF	VolValidFlg
	XREF	NoDiskFlg
	XREF	NotDosFlg
	XREF	NotValidFlg
	XREF	WrtProtFlg
	XREF	VolStatusMsg
	XREF	VolNamDevFlg
	XREF	DeviceNameBuf.
	XREF	DeviceNode
	XREF	ADOSFlg

* Externals in CAT.ASM

	XREF	InitWindowList
	XREF	NextWindow
;;	XREF	PushMenu,PopMenu
	XREF	NextMenu
	XREF	RefreshRoutine
	XREF	ScrollWindow
;;	XREF	InitMenuPtr,PushMenu,PopMenu

	XREF	WindowListPtr
;	XREF	MenuListPtr
	XREF	UpdWinFlg

* Externals in RESTORE.ASM

	XREF	FreeDirBlocks

* Externals in Version.asm

	XREF	PGM_NAME
;;	XREF	About1.,About2.,About3.,About4.,About5.

* Externals in PGU (Patch GiveUnit)

	XREF	PatchGiveUnit,UnPatchGiveUnit

* Externsals in BUFFERS.ASM

	XREF	AllocateCache,FreeCacheBuffers
	XREF	FlushCache
	XREF	FreeBlock,FreeImageBuf,FreeExtBuffer,FreeBadBlkBuf
	XREF	FreeRemapBuffer

* Externals in REORG.ASM


* Externals in DISK.ASM

	XREF	MotorOff

* Externals in VTEST.ASM

	XREF	InsertVolName

* External in AREXX.ASM

	XREF	OpenArexx,CloseArexx
	XREF	SendArexxReply
	XREF	GetArexxMsg
	XREF	NextToken

	XREF	ArexxFlg
	XREF	ArexxResult1
	XREF	ResultBuf

CheckMask	EQU	$FEFF

**************************************************************************

GO:	MOVE.L  A7,initialSP		;initial task stack pointer
	MOVE.L	A0,A2			;save ptr to CLI params
	MOVE.L	D0,D2
	MOVEA.L 4,A6
	MOVE.L  A6,SysBase
	MOVEQ	#20,D0
	MOVE.L	D0,ReturnCode		;in case we fail to complete initialization
	BSR	PatchGiveUnit
	LEA     DosLib.,A1
	MOVEQ   #LIBVER,D0		;********** specify V1.2 lib here
	CALLSYS	OpenLibrary		;OPEN DOS LIBRARY
	MOVE.L  D0,DosBase
	BEQ	EXIT_TO_DOS
	ZAPA	A1			;find this task
	CALLSYS	FindTask
	MOVE.L	D0,ToolsTask		;save ptr for msg ports
	MOVEA.L D0,A4
	TST.L   $AC(A4)			;invoked from cli?
	BEQ	FROM_WB			;NO...MUST BE WorkBench
;;	IFZW	D2,COMMON,L		;no options
	DECW	D2			;don't count line terminator
	BEQ	COMMON
	LEA	TMP.,A0
	MOVE.W	#TmpSize,D0
	IFLEW	D2,D0,2$
	MOVE.L	D0,D2
	BRA.S	2$
5$:	MOVE.B	(A2)+,(A0)+
2$:	DBF	D2,5$
	LEA	TMP.,A2
;;	CLR.B	-1(A2,D2.W)		;overwrite LF and make null-term	
	MOVE.L	A2,A0
	STRIP_LB. A0			;clean it up
	IFZB	(A2),COMMON,L		;nothing left

	CALLSYS	Input,DosBase
	MOVE.L	D0,STDIN
	CALLSYS	Output			;where does output go?
	MOVE.L	D0,STDOUT
	BEQ	EXIT_TO_DOS		;oops...none

	MOVE.W	(A2),D0			;get input command
	AND.W	#$FF5F,D0		;force upper case
	CMP.W	#'-R',D0
	BNE.S	1$			;input option error
	LEA     RexxLib.,A1
	MOVEQ   #LIBVER,D0
	CALLSYS	OpenLibrary,SysBase
	MOVE.L  D0,RexxBase
	BNE.S	4$			;got it
	LEA	ALibErr.,A0
	BRA.S	3$
4$:	BSR	OpenArexx		;else initialize Arexx input
	NOERROR	COMMON
	LEA	ArexErr.,A0
	BRA.S	3$
1$:	LEA	RunErr.,A0
3$:	ZAP	D3
	MOVE.B	(A0)+,D3
	MOVE.L	A0,D2
	MOVE.L	STDOUT,D1
	CALLSYS	Write,DosBase
	BRA	EXIT_TO_DOS

FROM_WB:
	LEA     TaskPort(A4),A0
	CALLSYS	WaitPort
	LEA     TaskPort(A4),A0
	CALLSYS	GetMsg
	MOVE.L	D0,WBenchMsg		;save WBench msg for reply
	MOVE.L	D0,A2
	MOVE.L	28(A2),D0		;NUMBER OF PARAMS
	MOVE.L	36(A2),A2		;PTR TO PARAM LIST
	MOVE.L	(A2),D1			;GET BPTR TO LOCK
	BEQ.S	COMMON			;no lock
	IFZL	DosBase,COMMON		;ADOS version error, from WB
	CALLSYS	CurrentDir,DosBase
	MOVE.L	D0,OldDir		;save lock on original dir
COMMON:	IFZL	DosBase,EXIT_TO_DOS,L
	LEA	GraphicsLib.,A1
	MOVEQ   #LIBVER,D0
	CALLSYS	OpenLibrary,SysBase	;OPEN GRAPHICS LIBRARY
	MOVE.L	D0,GraphicsBase
	BEQ	EXIT_TO_DOS
	LEA	IntuitionLib.,A1
	MOVEQ   #LIBVER,D0
	CALLSYS	OpenLibrary
	MOVE.L	D0,IntuitionBase
	BEQ	EXIT_TO_DOS
	BSR	OpenWindow
	ERROR	ABORT_EXIT		;failed to open window
	BSR	LoadParams		;try to load existing parameters
	LEA	T80,A0
	CALLSYS	OpenFont,GraphicsBase	;open Topaz 80
	MOVE.L	D0,FontPtr		;save ptr to this font
	BEQ	ABORT_EXIT
	IFNZB	ArexxFlg,1$
	BSR	AboutTools		;display initial greeting
	ERROR	EXIT_TO_DOS
;;	MOVEQ	#1,D0
;;	MOVE.B	D0,WBModeFlg
1$:	CLR.L	ReturnCode
	BSR	BldDevGadList
	LEA	DevGList,A0
	LEA	RefreshVolume,A1
	MOVE.L	#ToolsMenu,D0
	BSR	InitWindowList
	BRA.S	MLoop
	
MAIN:	BSR	FreeCacheBuffers
	BSR	DisplayVolumes		;build and display volumes
	BRA.S	MLoop
MLoopErr:
	MOVEQ	#SeriousError,D0
	MOVE.L	D0,ArexxResult1
MLoop:
	BSR	MotorOff
	IFZB	ArexxFlg,6$
	BSR	SendArexxReply
	JMP	GetArexxMsg

6$:	IFNZB	AbortFlg,QuitTools,L
	MOVEA.L	Window,A5
	MOVE.L	wd_UserPort(A5),A0
	CALLSYS	GetMsg,SysBase		;msg ready?
	IFNZL	D0,1$			;yes
	MOVE.L	wd_UserPort(A5),A0
	CALLSYS	WaitPort		;wait for something to happen
	BRA.S	6$			;it did

1$:	MOVE.L	D0,A1
	MOVE.L	im_Class(A1),MSG_CLASS	;SAVE MSG CLASS
	MOVE.W	im_Code(A1),MSG_CODE
	MOVE.L	im_IAddress(A1),GAD_PTR	;SAVE PTR TO POSSIBLE GADGET
	CALLSYS	ReplyMsg		;LET INTUITION KNOW WE HAVE IT
	MOVE.L	MSG_CLASS,D0
	IFEQIL	D0,#CLOSEWINDOW,ABORT_EXIT,L
	IFNEIL	D0,#MENUPICK,2$
	BSR	MenuPk
	BRA	MLoop
2$:	IFNEIL	D0,#REFRESHWINDOW,3$
	BSR	REFRESH			;keep intuition happy
	BRA.L	MLoop
3$:	IFEQIB	D0,#GADGETUP,7$
	IFNEIB	D0,#GADGETDOWN,4$	;not gadget
7$:	MOVE.L	GAD_PTR,A0		;ptr to gadget
;;;	LEA	AbortGad,A6		;just in case we get a late hit on...
;;;	IFEQL	A0,A6,MLoop,L		;...the abort gadget
	MOVE.L	gg_UserData(A0),A6	;proper routine for this gadget
	JSR	(A6)			;and away we go...JMP back to MLoop
	BRA	MLoop

4$:	AND.L	#$18000,D0		;disk changed?
	BEQ.L	MLoop			;no

* Called when a disk change event occurs.  If the volume display is
* still up, this will rebuild and redisplay the volume list.
* This makes it possible for QBTools to respond in real time to 
* changing diskettes.

	IFNZB	DispDevFlg,9$		;not displaying volumes
	IFNEIL	RefreshRoutine,#RefreshVolume,5$
	MOVEQ	#4,D1			;wait a bit...let the list get fixed
	CALLSYS	Delay,DosBase
	BSR	BldDevGadList		;rebuild volume list
	BSR	RefreshVolume		;put up the new list
	BSR	RefreshGList
	BRA.S	9$
5$:	IFNEIL	RefreshRoutine,#RefreshFunctions,9$
	BSR	CheckDriveStatus	;find out if our drive has changed
	IFZB	NoDiskFlg,9$		;no change
	CLR.B	FunctionFlg		;oops...gotta reset this mess
	BRA	FunExit
9$:	BRA	MLoop

* Processes menu pick IDCMP msgs.

MenuPk:	MOVE.W	MSG_CODE,D0		;MENU NUMBER TO D0
1$:	MOVE.L	WindowListPtr,A0
	MOVE.L	8(A0),A0
	CALLSYS	ItemAddress,IntuitionBase
	IFZL	D0,9$			;NULL...NO MENU SELECTED
	MOVE.L	D0,A0			;POINT TO MENUITEM
	MOVE.L	A0,MENUPTR		;SAVE PTR TO THIS MENUITEM
	MOVE.L	mi_ROUTINE(A0),A6	;GET ADDRESS OF MENU ROUTINE
	JSR	(A6)			;PROCESS THIS MENU ITEM
	MOVE.L	MENUPTR,A0		;RESTORE PTR TO LAST MENU ITEM
	MOVE.W	mi_NextSelect(A0),D0	;CHECK FOR EXTENDED SELECT
	BRA.S	1$			;TRY NEXT
9$:	RTS

* Waits in loop for operator to act with a proceed or cancel.

WaitForAction:
	PUSH	A2
	BSR	MotorOff
	CLR.B	ProcFlg
	CLR.B	CancelFlg
	CLR.B	AbortFlg	
1$:	MOVEA.L	Window,A2
	MOVE.L	wd_UserPort(A2),A0
	CALLSYS	GetMsg,SysBase		;msg ready?
	IFNZL	D0,2$			;yes
	MOVE.L	wd_UserPort(A2),A0
	CALLSYS	WaitPort		;wait for something to happen
	BRA.S	1$			;it did
2$:	BSR	GotMsg
	IFZB	UpdWinFlg,3$
	BSR	ScrollWindow
	BRA.S	1$
3$:	IFNZB	ProcFlg,9$
	IFNZB	AbortFlg,9$
	IFZB	CancelFlg,1$
9$:	POP	A2
	RTS

* Process IDCMP inputs during processing.  Gadgets called from here are expected
* to RTS, returning to GotMsg caller.  This routine does not wait (except for Pause).

ProcessMsg:
	PUSH	A2
	CLR.B	PauseFlg
	MOVE.L	Window,A2
1$:	MOVE.L	wd_UserPort(A2),A0	;get port
	CALLSYS	GetMsg,SysBase		;get any pending msg
	IFZL	D0,2$			;got nothing
	BSR	GotMsg			;process input
	BRA.S	1$
2$:;;	IFZB	UpdWinFlg,3$
;;	BSR	ScrollWindow
;;	BRA.S	1$
;;3$:
	IFZB	PauseFlg,9$		;not pausing
	CLR.B	PauseFlg		;wait for something
	BSR.S	StartPause
4$:	MOVE.L	wd_UserPort(A2),A0
	CALLSYS	WaitPort		;wait for something to happen
5$:	MOVE.L	wd_UserPort(A2),A0
	CALLSYS	GetMsg,SysBase
	IFZL	D0,4$
	BSR	GotMsg
	IFNZB	CancelFlg,6$
	IFZB	PauseFlg,5$
6$:	BSR.S	EndPause
9$:	POP	A2
	RTS

StartPause:
	BSR	MotorOff		;turn off drive
	LEA	ContinueTxt,A1
	BRA.S	PCom

EndPause:
	LEA	PauseTxt,A1
PCom:	PUSH	A2
	LEA	PauseGad,A2
	MOVE.L	A1,gg_GadgetText(A2)
	MOVE.L	A2,A0
	GadColor A0,ORANGE
	MOVE.L	A2,A0
	MOVE.L	Window,A1
	ZAPA	A2
	CALLSYS	RefreshGadgets,IntuitionBase
	POP	A2
	RTS

* Decode and process IDCMP msg in D0.  Ignore it if not one we want.

GotMsg:	MOVE.L	D0,A1
	MOVE.L	im_Class(A1),MSG_CLASS	;SAVE MSG CLASS
	MOVE.W	im_Code(A1),MSG_CODE
	MOVE.L	im_IAddress(A1),GAD_PTR	;SAVE PTR TO POSSIBLE GADGET
	CALLSYS	ReplyMsg		;LET INTUITION KNOW WE HAVE IT
	MOVE.L	MSG_CLASS,D0
	CMP.L	#CLOSEWINDOW,D0		;TERMINATE PROGRAM?
	BNE.S	1$
	SETF	AbortFlg		;EXIT PROGRAM IMMEDIATELY
	SETF	CancelFlg
	RTS

1$:	CMP.L	#REFRESHWINDOW,D0	;REFRESH REQUEST?
	BNE.S	2$			;yes
	BSR	REFRESH
	RTS

2$:	IFNEIL	D0,#MOUSEMOVE,5$
	SETF	UpdWinFlg
	RTS

5$:	IFEQIB	D0,#GADGETDOWN,3$
	IFNEIB	D0,#GADGETUP,4$		;not gadget...maybe menu?
3$:	MOVE.L	GAD_PTR,A0		;ptr to gadget
	IFZW	gg_GadgetID(A0),6$	;ignore some types of gadgets in this code
	MOVE.L	gg_UserData(A0),A6	;proper routine for this gadget
	JMP	(A6)			;and away we go...

4$:	IFNEIW	D0,#MENUPICK,6$		;not menu...
	BSR	MenuPk			;else process menu
6$:	RTS


PauseHit:
	SETF	PauseFlg
	RTS

RProcHit:
ProcHit:
	SETF	ProcFlg
	RTS

RCanHit:
CanHit:
	SETF	CancelFlg
	RTS

RSkipHit:
	SETF	SkipFlg
	RTS

RYesHit:
	SETF	YesFlg
	RTS
RNoHit:
	CLR.B	YesFlg
	RTS

QuitTools:
ABORT_EXIT:
	BSR	AudioCleanup
	BSR	FreeCacheBuffers
	BSR	FreeBMBuffers
	BSR	FreeDirBlocks
	BSR	FreeBlock
	BSR	FreeImageBuf
	BSR	FreeExtBuffer
	BSR	FreeBadBlkBuf
	BSR	FreeRemapBuffer
	BSR	CloseArexx
	BSR	ForceOn
EXIT_TO_DOS:
	MOVEA.L initialSP,A7
	MOVE.L	Window,A0
	IFZL	A0,1$
	CALLSYS	ClearMenuStrip,IntuitionBase
	MOVE.L	Window,A0
	BSR	CloseSafely		;close window properly
1$:	MOVE.L	FontPtr,A1
	IFZL	A1,2$
	CALLSYS	CloseFont,GraphicsBase
2$:	MOVE.L	OldDir,D1		;is there an original dir?
	BEQ.S	3$			;NO
	CALLSYS	CurrentDir,DosBase
3$:	MOVEA.L SysBase,A6
	MOVEA.L DosBase,A1
	IFZL	A1,4$
	CALLSYS	CloseLibrary		;CLOSE THE LIBRARIES
4$:	MOVE.L	GraphicsBase,A1
	IFZL	A1,5$
	CALLSYS	CloseLibrary
5$:	MOVE.L	IntuitionBase,A1
	IFZL	A1,6$
	CALLSYS	CloseLibrary
6$:	MOVE.L	RexxBase,A1
	IFZL	A1,7$
	CALLSYS	CloseLibrary
7$:	TST.L   WBenchMsg
	BEQ.S   8$
	CALLSYS	Forbid
	MOVEA.L WBenchMsg,A1
	CALLSYS	ReplyMsg
8$:	BSR	UnPatchGiveUnit
	MOVE.L	ReturnCode,D0		;load return code
	RTS

* Builds and displays list of available AmigaDOS volumes.

DisplayVolumes:
	BSR	FreeBMBuffers		;release previous bitmap buffer
;	MOVEQ	#1,D0
;	BSR	EnableMenu
	BSR	BldDevGadList
	LEA	DevGList,A0
	LEA	RefreshVolume,A1
	MOVE.L	#ToolsMenu,D0
	BSR	NextWindow
	RTS

RefreshVolume:
	PUSH	D2/A2
	BSR	CLRWIN
	LEA	VolLst.,A0
	LEA	ClkVol.,A2
	IFZB	DispDevFlg,2$
	LEA	DrvLst.,A0
	LEA	ClkDrv.,A2
2$:	DispCent #320,#DevTxt_TE,A0,WHITE
	MOVE.L	A2,A0
	DispCent #320,#DevTxt2_TE,A0,WHITE
	ZAP	D2
	MOVE.W	VolumeCnt,D2
	LEA	DevGadTbl,A2
	BRA.S	3$
1$:	MOVE.L	(A2)+,A0
	GadColor A0,ORANGE
3$:	DBF	D2,1$
	BSR	RefreshAllGads
	POP	D2/A2
	RTS

* Refresh list of available functions.  The list depends upon whether or not 
* the selected volume is validated.  If not, the list of functions is
* much shorter.  The gadget list varies accordingly, and must change if 
* the list of functions changes.  Since volume status is affected by certain
* functions, the volume status must be rechecked each time the list is
* refreshed, and the list of gadgets changed then also.  This greatly
* complicates the management of the gadgets associated with this window.
* D0=flag for changing gadgets: 0=ok, 1=don't

RefreshFunctions:
	PUSH	D2/A2-A3
	MOVE.B	D0,FunGadFlg
	BSR	CLRWIN
	BSR	CheckDriveStatus	;find out about volume
	IFZB	NoDiskFlg,8$		;no change
	MOVE.	DeviceNameBuf.,VolNameBuf. ;lost our volume
8$:	BSR	CheckVolumeStatus
	IFNZB	VolValidFlg,1$,L
	DispCent #320,#MenuTitle_TE,BadMenu.
	LEA	FunDrv.,A0
	BSR	InsertVolName		;insert volume
	DispCent #320,#FunTitle_TE,A0,WHITE
	DispMsg	#FunTxt_LE,#FunLin1_TE,Fun1.
	DispMsg	#FunTxt_LE,#FunLin2_TE,Fun21.
	DispMsg	#FunTxt_LE,#FunLin3_TE,Fun22.
	DispMsg	#FunTxt_LE,#FunLin4_TE,Fun23.
	DispMsg	#FunTxt_LE,#FunLin5_TE,DiffDrv.
	MOVEQ	#BFunCnt-1,D2
	LEA	Fun20,A2
	BRA	2$

1$:	LEA	FunVol.,A0
	BSR	InsertVolName		;insert volume
	DispCent #320,#FunTitle_TE,A0,WHITE
	MOVE.B	FunctionFlg,D0			;regular function?
	BNE	6$				;no
	DispCent #320,#MenuTitle_TE,MainMenu.
	DispMsg	#FunTxt_LE,#FunLin1_TE,Fun1.
	DispMsg	#FunTxt_LE,#FunLin2_TE,Fun3.
	DispMsg	#FunTxt_LE,#FunLin3_TE,Fun4.
	DispMsg	#FunTxt_LE,#FunLin4_TE,Fun5.
	DispMsg	#FunTxt_LE,#FunLin5_TE,Fun6.
	DispMsg	#FunTxt_LE,#FunLin6_TE,Fun7.
	DispMsg	#FunTxt_LE,#FunLin7_TE,DiffVol.
	MOVEQ	#GFunCnt-1,D2
	LEA	Fun1,A2
	BRA	2$

6$:	DECB	D0
	BNE	7$
	DispCent #320,#MenuTitle_TE,RepairMenu.
	LEA	RepairMenu,A0
	BSR	NextMenu
	DispMsg	#FunTxt_LE,#FunLin1_TE,Fun10.
	DispMsg	#FunTxt_LE,#FunLin2_TE,Fun11.
	DispMsg	#FunTxt_LE,#FunLin3_TE,Fun12.
	DispMsg	#FunTxt_LE,#FunLin4_TE,RetMenu.
	MOVEQ	#GFixCnt-1,D2
	LEA	Fun10,A2
	BRA	2$

7$:	DispCent #320,#MenuTitle_TE,ReorgMenu.
	LEA	ReorgMenu,A0
	BSR	NextMenu
	DispMsg	#FunTxt_LE,#FunLin1_TE,Fun30.
	DispMsg	#FunTxt_LE,#FunLin2_TE,Fun31.
	DispMsg	#FunTxt_LE,#FunLin3_TE,Fun32.
	DispMsg	#FunTxt_LE,#FunLin4_TE,RetMenu.
	MOVEQ	#GOrgCnt-1,D2
	LEA	Fun30,A2
2$:	MOVE.L	A2,A3
3$:	MOVE.L	A3,A0
	GadColor A0,ORANGE
	MOVE.L	(A3),A3			;fwd link to next gadget
	DBF	D2,3$
;	GadColor FunCGad,BLACK
	MOVE.L	FunGadList,A0		;get previous gadget list
	IFZL	A0,5$			;no current list...don't change
	IFEQL	A0,A2,4$		;gadget lists same...no change
	IFNZB	FunGadFlg,5$		;don't change the gadgets...yet
	BSR	RemoveGadgets		;else remove old list, if any
	MOVE.L	A2,A0			;and add new list
	BSR	AddGadgets
5$:	MOVE.L	WindowListPtr,A3
	MOVE.L	A2,(A3)			;and correct window list ptr
4$:	MOVE.L	A2,FunGadList
	BSR	RefreshAllGads
	POP	D2/A2-A3
	RTS


* Gadget processing routines

Function3:
	MOVEQ	#2,D0
	BRA.S	FunCom
Function5:
	MOVE.B	#1,D0
FunCom:	MOVE.B	D0,FunctionFlg
	ZAP	D0
	BSR	RefreshFunctions
	BRA	MLoop

DevGadHit:
	MOVE.L	GAD_PTR,A0
	MOVE.L	gg_DevNode(A0),DeviceNode
	MOVE.L	gg_GadgetText(A0),A0
	MOVE.L	it_IText(A0),A0
	MOVE.	A0,VolNameBuf.
	MOVE.B	DispDevFlg,VolNamDevFlg	;mark vol name as vol or device
	BSR	LoadDeviceParams	;get device driver params
	BSR	OpenDriver
	ERROR	MLoopErr
	SETF	VolumeFlg
	BSR	AllocateCache		;rellocate cache
	BSR	CalcVolParams
	MOVE.L	FunGadList,A0
	LEA	RefreshFunctions,A1
	MOVE.L	#FunctionMenu,D0
	BSR	NextWindow
;	BSR	AllocateCache		;rellocate cache
	IFZB	ADOSFlg,MLoop,L
	BSR	InhibitDos		;
	BRA	MLoop

FunExit:
	BSR	FlushCache
	CLR.B	BitmapFlg		;no valid bitmap now
	IFZB	FunctionFlg,1$		;running in a special function mode?
	LEA	FunctionMenu,A0
	BSR	NextMenu
	CLR.B	FunctionFlg		;not now
	ZAP	D0
	BSR	RefreshFunctions
	BRA	MLoop
1$:;;	BSR	FlushCache
	CLR.L	FunGadList
	BSR	ForceOn			;EnableDOS
	BSR	CloseDriver
	CLR.B	VolumeFlg
	BRA	MAIN

* Load and save parameters

LoadParams:
	PUSH	D2-d4
	MOVE.L	#Params.,D1		;name of param file
	MOVE.L	#EXISTING,D2		;read in existing params, if any
	CALLSYS	Open,DosBase		;try to open it
	MOVE.L	D0,d4			;handle for write
	BEQ.S	9$			;oops...open maybe never defined
	MOVE.L	d4,D1
	MOVE.L	#Params,D2		;they are stored here
	MOVE.L	#ParamLen,D3		;this many
	CALLSYS	Read			;do it
	MOVE.L	d4,D1
	CALLSYS	Close
	LEA	MODE_ITEMS,A0
	MOVE.B	WBModeFlg,D0
	BSR.S	SetCheckMark
	LEA	MEMORY_ITEMS,A0
	MOVE.B	CacheBufMode,D0
	BSR.S	SetCheckMark
	LEA	RPT_ITEMS,A0
	MOVE.B	ReportMode,D0
	BSR.S	SetCheckMark
	LEA	SCNMODE_ITEMS,A0
	MOVE.B	ScreenMode,D0
	BSR.S	SetCheckMark
9$:	POP	D2-d4
	RTS

* Called to set the check mark on the proper menu item depending on flag value in D0.
* IF D0=0, first item is set, D0=1, second item, and so on.  Ptr to menu list in A0.

SetCheckMark:
	PUSH	D2
	ZAP	D2
1$:	MOVE.W	mi_Flags(A0),D1
	AND.W	#CheckMask,D1
	IFNEB	D0,D2,2$
	OR.W	#$100,D1
2$:	MOVE.W	D1,mi_Flags(A0)
	INCB	D2
	MOVE.L	mi_NextItem(A0),A0
	IFNZL	A0,1$
	POP	D2
	RTS

SaveParams:
	PUSH	D2-d4
	MOVE.L	#Params.,D1		;name of param file
	MOVE.L	#NEW,D2			;write out a new version
	CALLSYS	Open,DosBase		;try to open it
	MOVE.L	D0,d4			;handle for write
	BEQ.S	9$			;oops...open failed
	MOVE.L	d4,D1
	MOVE.L	#Params,D2		;they are stored here
	MOVE.L	#ParamLen,D3		;this many
	CALLSYS	Write			;do it
	MOVE.L	d4,D1
	CALLSYS	Close
9$:	POP	D2-d4
	RTS

* Open Tools own window

OpenWindow:
	PUSH	A2
	LEA	ScnData,A0		;screen data goes here
	MOVEQ	#16,D0			;16 bytes of screen data only
	MOVEQ	#1,D1			;workbench screen
	CALLSYS	GetScreenData,IntuitionBase
	IFZL	D0,8$,L
	LEA	NewWindow,A0		;OPEN (ANOTHER) WINDOW
	LEA	WINI,A2
	MOVEQ	#2,D0			;Win_LE
	MOVE.W	D0,(A2)+
	MOVEQ	#10,D0			;Win_TE
	MOVE.W	D0,(A2)+
	MOVE.W	ScnData+12,D0		;width of new window
	MOVE.W	D0,4(A0)
	SUBQ	#4,D0
;;	SUB.W	#10,D0
	MOVE.W	D0,(A2)+
	MOVE.W	ScnData+14,D0		;height of new window
;	DECW	D0
	MOVE.W	D0,6(A0)
	IFNEIW	D0,#200,1$		;not standard NTSC
	SUB.W	#9,DnGad+gg_TopEdge
	SUB.W	#9,PropGad+gg_Height
1$:	SUB.W	#10,D0
	MOVE.W	D0,(A2)+
	CALLSYS	OpenWindow,IntuitionBase
	MOVE.L	D0,Window		;SAVE PTR TO Window STRUCTURE
	BNE.S	9$
8$:	STC
9$:	POP	A2
	RTS

* Arexx drive and volume routines

RexVol:
	BSR.S	VolMnu
	BRA.S	RexCom

RexDrv:
	BSR	DrvMnu
RexCom:	BSR	NextToken		;get name of device or drive
	ERROR	MLoopErr
	BSR	ScanVolNames		;in the list?
	ERROR	MLoopErr
	MOVE.L	A0,GAD_PTR
	BRA	DevGadHit

DrvMnu:	SETF	DispDevFlg		;display devices
	BRA.S	VCom

VolMnu:	CLR.B	DispDevFlg		;display volumes
VCom:	BSR	BldDevGadList		;else rebuild list
	BSR	RefreshVolume		;and redisplay
	BSR.S	RefreshGList
9$:	RTS	

* Refreshes volume gadgets after change.

RefreshGList:
	LEA	DevGList,A0
	MOVE.L	Window,A1
	ZAPA	A2
	MOVEQ	#-1,D0
	CALLSYS	RefreshGList,IntuitionBase
	RTS

AboutTools:
	DispReq	AboutTxt,ReqOK
	RTS

RexAbtTls:
	MOVE.	PGM_NAME,ResultBuf
	RTS

DosLib.		TEXTZ	'dos.library'
GraphicsLib.	TEXTZ	'graphics.library'
IntuitionLib.	TEXTZ	'intuition.library'
RexxLib.	TEXTZ	'rexxsyslib.library'
Params.		TEXTZ	'S:QBT.Opt'

	CNOP	0,2

Params
CacheBufMode	DC.B	1	;0=low, 1=norm, 2=all
ReportMode	DC.B	0	;0=none, 1=disk, 2=printer
ScreenMode	DC.B	2	;0=quiet, 1=informative, 2=interactive
WBModeFlg	DC.B	1	;1=WorkBench mode, 0=CLI mode
		DC.L	0	;reserved
		DC.L	0	;reserved
ReportPath.	DCB.B	90,0
ParamLen	EQU	*-Params

* Window DEFINITION

	CNOP	0,4

NewWindow
NW_LE	DC.W	0		;Ledt edge
NW_TE	DC.W	0		;Top edge
NW_WD	DC.W	640		;Width
NW_HT	DC.W	200		;Height
	DC.B	BLUE,WHITE	;DetailPen, BlockPen
NW_IDCMP DC.L	$183E4		;DSKCHG, CLSWIN, GADDN, MNUPCK, REQSET
NW_FLAGS DC.L	$100F		;Flags - DEPTH+CLOSE GADGETS, ACTIVATE
	DC.L	0		;FirstGadget (CUSTOM)
	DC.L	0		;CheckMark=DEFAULT
NW_TITLE DC.L	PGM_NAME 	;Title  
	DC.L	0		;NO CUSTOM Screen
	DC.L	0		;NO CUSTOM BitMap
	DC.W	200,60		;MinWidth, MinHeight
	DC.W	999,999		;MaxWidth, MaxHeight
	DC.W	1		;WORKBENCH TYPE

	BSS,PUBLIC

GAD_PTR		DS.L	1
MSG_CLASS	DS.L	1	;IDCMP MSG CLASS
MSG_CODE	DS.W	1	;IDCMP MSG CODE

STDIN		DS.L	1
STDOUT		DS.L	1
ToolsTask	DS.L	1
_SysBase
SysBase		DS.L	1
DosBase		DS.L	1
GraphicsBase	DS.L	1
IntuitionBase	DS.L	1
RexxBase	DS.L	1
WBenchMsg	DS.L	1	;0 IF CLI, <>0 IF WORKBENCH
initialSP	DS.L	1
Window		DS.L	1	;OUR OWN Window
ScnData		DS.L	4	;16 bytes of screen data
MENUPTR		DS.L	1	;ptr to selected menu item
FontPtr		DS.L	1	;points to font structure
OldDir		DS.L	1	;initial directory
FunGadList	DS.L	1
ReturnCode	DS.L	1	;Tools return code
VolumeFlg	DS.B	1	;1=got a volume/device, 0=don't
FunGadFlg	DS.B	1	;0=ok to change gadgets, 1=don't change gadgets
SaveLastChar	DS.B	1	;last char of BRDevPath
DispDevFlg	DS.B	1	;1=display drives, 0=display volumes
FunctionFlg	DS.B	1	;1=fix volume, 2=reorg, 0=normal functions
ProcFlg		DS.B	1
CancelFlg	DS.B	1
AbortFlg	DS.B	1
PauseFlg	DS.B	1
YesFlg		DS.B	1	;1=op chose Yes
SkipFlg		DS.B	1
		DS.B	1	;for BSTR usage in Main.asm
TMP.		DS.B	TmpSize
TMP2.		DS.B	80
TMP3.		DS.B	80
VolNameBuf.	DS.B	VolNameLen

	END
