;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*****************************************************************
*								*
*		FORMAT.ASM					*
*								*
*	Quarterback Tools Fast Format Functions			*
*								*
*****************************************************************

	INCLUDE "vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"VD0:BOXES.ASM"

	XDEF	Function6,Function7
	XDEF	Function23
	XDEF	FFSHit,OFSHit
	XDEF	FmtFlop
	XDEF	FmtNamGadHit
	XDEF	InitBoot

	XDEF	Form.
	XDEF	NewType

* Externals in TOOLS.ASM

	XREF	MLoop
	XREF	RefreshFunctions
	XREF	ProcessMsg

	XREF	SysBase,DosBase,IntuitionBase,GraphicsBase
	XREF	TMP.
	XREF	VolNameBuf.
	XREF	FunGadList
	XREF	CancelFlg
	XREF	AbortFlg
	XREF	SkipFlg
	XREF	YesFlg
	XREF	ProcFlg

* Externals in FILES.ASM

	XREF	ReadRoot
	XREF	WriteRoot
	XREF	OpenFile,CreateFile,DeleteFile
	XREF	ReadCtrlBlock,ReadExtHdr
	XREF	BuildExtHdr
	XREF	LinkParentDir
	XREF	OpenBadBlocksFile

	XREF	BadBlksFileKey

* Externals in BITMAP.ASM

	XREF	ReadBitmap,WriteBitmap
	XREF	InitBitmap
	XREF	AllocFreeKey
	XREF	CountFreeKeys
	XREF	InitBitmapBuffer
	XREF	FreeBMBuffers
	XREF	MarkKeyBusy

	XREF	BMDirtyFlg
	XREF	BitmapBuffer
	XREF	BitmapDataPtr
	XREF	BitmapBufSize
	XREF	BitmapKeyBuffer

* Externals in Device.asm

	XREF	CheckVolumeStatus,CheckDiskLoaded
	XREF	CalcVolParams
	XREF	OpenDriver,CloseDriver
	XREF	InhibitDos,EnableDos
	XREF	CheckProtection
	XREF	LoadAltParams
	XREF	LoadDeviceParams

	XREF	NumSoftErrors
	XREF	UnitNumber
	XREF	DiskState
	XREF	NumBlocks
	XREF	NumBlocksUsed
	XREF	BytesPerBlock
	XREF	DiskType
	XREF	VolumeNode
	XREF	InUse	
	XREF	MaxBytes
	XREF	MaxBlocks
	XREF	FreeBytes
	XREF	FreeBlocks
	XREF	VolumeOffset
	XREF	LowCyl
	XREF	HighCyl
	XREF	MaxCylinders
	XREF	Surfaces
	XREF	MaxTracks
	XREF	BlockSize,BlockSizeL
	XREF	Reserved
	XREF	BlocksPerTrack
	XREF	NumBuffers
	XREF	BufMemType
	XREF	MaxTransfer
	XREF	PercentFull
	XREF	DosType
	XREF	DeviceNameBuf.
	XREF	DriverNameBuf.
	XREF	VolNamDevFlg
	XREF	VolValidFlg
	XREF	NoDiskFlg
	XREF	NotDosFlg
	XREF	NotValidFlg
	XREF	WrtProtFlg
	XREF	VolStatusMsg
	XREF	HashTblSize
	XREF	RootBlkNbr
	XREF	BitmapBlocks,BMExtBlocks
	XREF	FFSFlg
	XREF	BlkSizeShift
	XREF	HighestKey,LowestKey
	XREF	ADOSFlg

* Externals in Gadgets.asm

	XREF	RightPGad,CanGad,PauseGad
	XREF	ReqYes,ReqPreserve
	XREF	ReqOK,ReqRetry
	XREF	ReqVolNamGad,ReqNameGad
	XREF	ReqFFSGad
	XREF	FileSys
	XREF	FmtName
	XREF	NamBuf,NamInfo
	XREF	UnFormat
	XREF	LowFormat
	XREF	BadFlop
	XREF	FlopDrv
	XREF	EraseFlop

* Externals in DISK.ASM

	XREF	WriteBlock,ReadBlock
	XREF	ReadCyl
	XREF	ReadBoot,WriteBoot
	XREF	MotorOff
	XREF	FormatCyl

* Externals in BUFFERS.ASM

	XREF	GetCacheBuffer,PutCacheBuffer
	XREF	CalcCkSum,ZapBlock
	XREF	LinkCache
	XREF	WriteCache,FlushCache
	XREF	LinkCacheKey
	XREF	AllocateBlock,FreeBlock
	XREF	AllocateIOBuf

	XREF	BlockBuffer

* Externals in MESSAGES.ASM

	XREF	InvFilNam.
	XREF	NameTooLong.
	XREF	Unnamed.
	XREF	NotSafe.
	XREF	FmtErr.
	XREF	FmtVol.
	XREF	UnFmtVol.
	XREF	FmtFlop.
	XREF	NotFlop.
	XREF	NoDev.
	XREF	FmtRes1.,FmtRes2.,FmtRes3.,FmtRes4.

* Externals in FUN.ASM

	XREF	CLRWIN
	XREF	RefreshAllGads

* Externals in CAT.ASM

	XREF	PushWindow,PopWindow

* Externals in REPAIR.ASM

	XREF	Function12
	XREF	ValidateName

* Externals in RESTORE.ASM

	XREF	Function4

* Externals in MOVE.ASM

	XREF	ReadCtrlBlk

* Externals in VTEST.ASM

	XREF	InsertVolName

* Externals in GRAPHICS.ASM

	XREF	InitBar
	XREF	UpdateStatusBar
	XREF	RefreshBar

* Enter here to format an invalid volume

Function23:
	BSR	CalcVolParams		;calc volume params
	BSR	CheckVolumeStatus	;get device params
	ERROR	MLoop			;problems
	BSR	CheckDiskLoaded
	ERROR	MLoop			;oops...
	BSR	CheckProtection		;make sure we can write
	ERROR	MLoop			;op canceled
	BSR	AllocateBlock
	ERROR	Fun6Exit
	ZAPA	A0			;no special gadgets
	LEA	Fun6Refresh,A1
	ZAP	D0
	BSR	PushWindow		;remove gadgets and clear window
	BSR	InhibitDos		;lock out ADOS
	IFNE.	TrkDisk.,DriverNameBuf.,4$ ;not floppy
	DispReq	EraseFlop,ReqYes
	IFNZB	YesFlg,2$		;fully erase
4$:	PUSH	A2
	MOVE.L	BlockBuffer,A2
	MOVE.L	A2,A0	
	BSR	ReadBoot		;is boot readable?
	ERROR	1$
	MOVE.L	RootBlkNbr,D0
	MOVE.L	A2,A0
	BSR	ReadBlock		;is root readable?
	ERROR	1$
	MOVE.L	HighestKey,D0
	MOVE.L	A2,A0
	BSR	ReadBlock
1$:	POP	A2
	NOERROR	3$			;disk appears to be usable
	IFEQ.	TrkDisk.,DriverNameBuf.,2$ ;this device is a floppy
	DispReq	LowFormat,ReqOK
	BRA	Fun6Exit		;that's all we can do

2$:	BSR	FormatFloppy
	ERROR	Fun6Exit

3$:	MOVE.	Unnamed.,NamBuf
	BSR	GetVolName
	ERROR	Fun6Exit
	MOVE.	NamBuf,VolNameBuf.
	DispReq	FileSys,ReqFFSGad
	ERROR	Fun6Exit
	BSR.S	FormatQuickly
	NOERROR	Fun6Exit
	DispErr	FmtErr.
	BRA	Fun6Exit	

FormatQuickly:
	BSR	InitBoot
	ERROR	9$
	BSR	InitRoot
	BSR	InitBitmap
	BSR	WriteBitmap
	ERROR	9$
	BSR	WriteCache		;force cache data to disk
	NOERROR	9$	
	BSR	FlushCache
	STC
9$:	RTS

* Enter here to format a valid volume

Function6:
	BSR	CheckVolumeStatus		;get device params
	ERROR	MLoop			;problems
	BSR	CheckDiskLoaded
	ERROR	MLoop			;oops...
	BSR	CheckProtection		;make sure we can write
	ERROR	MLoop			;op canceled
	BSR	CalcVolParams		;calc volume params
	ZAPA	A0			;no special gadgets
	LEA	Fun6Refresh,A1
	ZAP	D0
	BSR	PushWindow		;remove gadgets and clear window
	MOVE.	VolNameBuf.,NamBuf
	BSR	GetVolName		;get new name of volume
	ERROR	Fun6Exit		;op elected to bail out
	BldWarn	0,0,RUSur1.,RUSur2.,RUSur3.,RUSur4.
	DispReq	A0,ReqPreserve
	IFNZB	CancelFlg,Fun6Exit,L
	MOVE.	NamBuf,VolNameBuf.
	MOVE.L	DosType,NewType
	BSR	InhibitDos		;lock out ADOS
	IFNZB	YesFlg,Reformat		;op elected to format safely
	IFNE.	TrkDisk.,DriverNameBuf.,2$ ;this device is NOT a floppy
	BSR	FormatFloppy
	ERROR	Fun6Exit
2$:	BSR	FormatQuickly		;else get it done quickly
	BRA	Fun6Exit

* Need to be sure root block is readable.  also, if bitmap
* blocks are readable, reuse them to improve restoration chances.
* also need to preserve root/bitmap blocks somewhere for fast
* "unformat"

Reformat:
	PUSH	D2/A2
	BSR	ReadBitmap		;make sure we have something to work with
	ERROR	8$			;we don't
	BSR	OpenBadBlocksFile	;find out if we have bad blocks
	IFZL	D0,3$			;no bad blocks...
	ADD.L	BlockSize,A0
	MOVE.L	fh_HashChain(A0),BadHashChain ;save linkage
3$:	BSR	SaveFormat		;try to save existing root/bm

	BSR	InitRoot
	BSR	CopyBMPtrs		;copy old bm ptrs into root
	MOVE.L	RootBlkNbr,D2
	MOVE.L	QBTKey,D0
	BEQ.S	1$
	MOVE.L	D2,D1
	BSR	LinkParentDir		;link QBT.FMT into new root
1$:	MOVE.L	BadBlksFileKey,D0
	BEQ.S	2$
	MOVE.L	D2,D1
	BSR	LinkParentDir		;link BAD.BLOCKS into new root
2$:	MOVE.L	BitmapDataPtr,A0
	BSR	InitBitmapBuffer	;reset to empty bitmap
	MOVE.L	RootBlkNbr,D0
	BSR	MarkKeyBusy		;mark root busy in new bm

	MOVE.L	BitmapKeyBuffer,A2	;mark bitmap keys busy in bm
	MOVE.L	BitmapBlocks,D2
	ADD.L	BMExtBlocks,D2
	BRA.S	5$
4$:	MOVE.L	(A2)+,D0		;get a bitmap key
	BCLR	#31,D0
	BSR	MarkKeyBusy
5$:	DBF	D2,4$
	MOVE.L	QBTKey,D0		;mark QBT.FMT busy in new bitmap
	BSR	MarkFileBusy
	MOVE.L	BadBlksFileKey,D0	;mark BAD.BLOCKS busy in new bitmap
	BSR	MarkFileBusy

	BSR	WriteBitmap
	ERROR	8$
	BSR	InitBoot
	ERROR	8$
	BSR	WriteCache		;force cache data to disk
	NOERROR	9$
8$:	DispErr	FmtErr.
9$:	POP	D2/A2

Fun6Exit:
	BSR	Cleanup
	BRA	MLoop

* Called to find/create QBT.FMT file in Root of valid volume.  If not
* found, it is allocated, if there is room for it.  Then the existing
* root and bitmap blocks are copied to QBT.FMT for later use by UnFormat.

SaveFormat:
	PUSH	D2-D3/A2-A3
	CLR.L	QBTKey
	IFZB	VolValidFlg,9$,L	;not valid volume...forget it

	BSR	CountFreeKeys
	BSR	AllocateBlock		;make sure we have a block buffer
	ERROR	9$

	LEA	Form.,A2		
	MOVE.L	RootBlkNbr,D2
	MOVE.L	A2,A0
	MOVE.L	D2,D0
	BSR	OpenFile		;try to find existing saved format file
	ERROR	1$			;nonesuch...create it
	MOVE.L	D0,QBTKey
	MOVE.L	A0,A3			;ptr to header
	IFEQL	fh_BlkCnt(A3),D3,2$	;has right number of blocks...
	MOVE.L	D2,D1
	BSR	DeleteFile		;else get rid of it, whatever it is
1$:	MOVE.L	A2,A0
	MOVE.L	D2,D0
	BSR	CreateFile		;else add saved format file
	ERROR	9$			;oops...can't
	MOVE.L	A0,A3			;filehdr
	MOVE.L	D0,QBTKey

	MOVE.L	BitmapBlocks,D3		;need 1 block for each bitmap block
	ADD.L	BMExtBlocks,D3		;gotta have room for them, too!
	INCL	D3			;plus 1 for root
	MOVE.L	BlkSizeShift,D1
	LSL.L	D1,D3			;number of bytes
	ADDQ.L	#8,D3			;hashchain params
	MOVE.L	BlockSize,D0
	ADD.L	D0,A0
	MOVE.L	D3,fh_FileSize(A0)	;this many bytes in file
	IFNZB	FFSFlg,3$
	MOVE.W	#488,D0
3$:	ADD.L	D0,D3			;round up
	DECL	D3
	DIVU	D0,D3			;number of blocks needed
	EXT.L	D3
	MOVE.L	A3,A0			;filehdr
	MOVE.L	D3,D0			;number of keys needed
	BSR	AllocDataKeys		;alloc keys for needed data blocks
	ERROR	8$			;didn't get enough...clean up and exit
	MOVE.L	D3,D0
	MOVE.L	A3,A0
	ADD.L	BlockSize,A0
	MOVE.L	fh_BlkPtr1(A0),fh_FirstBlk(A3)
	MOVE.L	fh_HashChain(A0),QBTHashChain ;save existing hashchain
2$:	MOVE.L	A3,A0			;filehdr
	MOVE.L	D3,D0			;this many blocks to process
	BSR	SaveFormatData		;update QBT.FMT
	ERROR	8$
	BSR	WriteCache		;flush new data to disk
	BSR	FlushCache		;clean up
	BRA.S	9$
8$:	DispErr	NotSafe.		;error saving format data
9$:	BSR	FreeBlock		
	POP	D2-D3/A2-A3
	RTS

* Allocates D0 free keys for QBT.FMT file and enters them into the filehdr A0.
* Returns CY=1 if there aren't enough.  Gotta allow for extensions!  Key of
* filehdr in D1.

AllocDataKeys:
	PUSH	D2-D4/A2-A3
	MOVE.L	D0,D3			;need this many keys, total
	MOVE.L	D1,D4			;predecessor key
3$:	MOVE.L	A0,A2
	LEA	fh_BlkPtr1+4(A2),A3
	ADD.L	BlockSize,A3
	MOVE.L	HashTblSize,D2		;filehdr holds this many
	BRA.S	2$
1$:	BSR	AllocFreeKey		;get a key
	ERROR	8$
;;	MOVE.L	D0,-(A7)
;;	BSR	MarkAltBusy		;mark this key busy
;;	MOVE.L	(A7)+,D0
	MOVE.L	D0,-(A3)		;store into header
	INCL	fh_BlkCnt(A2)		;count it
	DECL	D3
	BEQ.S	9$			;that's all we need
2$:	DBF	D2,1$
	MOVE.L	D4,D0			;predecessor key
	BSR	BuildExtHdr		;allocate and init exthdr
	ERROR	8$			;oops...
	MOVE.L	D0,D4			;save this key in case another exthdr...
;;	BSR	MarkAltBusy		;mark exthdr busy too...
	BRA.S	3$
8$:	CLR.B	BMDirtyFlg		;ignore bitmap changes
	BSR	FlushCache		;clean up cache
	STC
9$:	POP	D2-D4/A2-A3
	RTS

* Copies root and bitmap data into QBT.FMT data blocks.  Ptr to filehdr in A0.
* Bitmap data in memory ASSUMED to be valid.

SaveFormatData:
	PUSH	D2-D7/A2-A5
	MOVE.L	A0,A2			;ptr to filehdr/exthdr
	MOVE.L	A2,A3
	ADD.L	BlockSize,A3		;point to list of keys
	MOVE.L	fh_Ext(A3),D6		;key of exthdr, if any
	LEA	fh_BlkPtr1+4(A3),A3
	MOVE.L	HashTblSize,D7		;number of keys in this hdr
	MOVE.L	RootBlkNbr,D0
	BSR	ReadCtrlBlock		;get root
	ERROR	9$
	MOVE.L	A0,A4			;first input
	MOVE.L	BlockSizeL,D4		;this many longwords
	MOVEQ	#1,D3			;sequence number for OFS
	BSR	NextBuf			;init first output buffer
	BSR.S	SaveData		;copy root data
	MOVE.L	BitmapBuffer,A4
	MOVE.L	BitmapBufSize,D4
	LSR.L	#2,D4			;treat as longwords
	BSR.S	SaveData		;copy bitmap data
	LEA	QBTHashChain,A4
	MOVEQ	#2,D4
	BSR.S	SaveData		;save hash chain contents for UnFormat
	ZAP	D2
	BSR.S	WriteBuf		;write out last buffer
9$:	POP	D2-D7/A2-A5
	RTS

* Uses D4/A4 (input) and D5/A5 (output) as byte count/data ptrs respectively.

SaveData:
1$:	IFZL	D4,9$			;data gone
	MOVE.L	(A4)+,D0		;else get a longword
	DECL	D4
	IFNZL	D5,2$			;there is room in output buffer
	MOVEQ	#1,D2
	PUSH	D0
	BSR.S	WriteBuf		;else write out buffer
	BSR.S	NextBuf			;and get another
	POP	D0
	ERROR	9$
2$:	MOVE.L	D0,(A5)+
	DECL	D5
	BRA.S	1$
9$:	RTS

WriteBuf:
	IFNZL	D7,1$			;no more keys
	MOVE.L	D6,D0			;get exthdr key
	BEQ.S	9$			;oops...none, bail out
	BSR	ReadExtHdr		;get exthdr
	ERROR	9$
	MOVE.L	A0,A3
	ADD.L	BlockSize,A3
	MOVE.L	fe_Ext(A3),D6		;key of next exthdr
	LEA	fe_BlkPtr1+4(A3),A3
	MOVE.L	HashTblSize,D7		;max keys in this exthdr
1$:	MOVE.L	-(A3),D0		;get key for this write
	BEQ.S	9$			;just in case...
	ZAP	D1
	MOVE.L	LocalBuffer,A0		;buffer
	IFNZB	FFSFlg,2$
	IFZL	D2,2$			;last block...no link
	MOVE.L	-4(A3),od_NextData(A0)	;set up link
2$:	ZAP	D1			;data block
	BSR	LinkCacheKey		;link to busy queue
	INCL	D3			;sequence count for next block
	DECL	D7			;any more keys left?
9$:	RTS

NextBuf:
	BSR	GetCacheBuffer
	ERROR	9$
	DIRTY	A0
	MOVE.L	A0,LocalBuffer
	MOVE.L	A0,A5
	MOVE.L	BlockSizeL,D5
	IFNZB	FFSFlg,9$
	MOVE.L	#488,D5
	MOVEQ	#T_DATA,D0
	MOVE.L	D0,(A5)+		;type
	MOVE.L	fh_Ownkey(A2),(A5)+	;filehdr key
	MOVE.L	D3,(A5)+		;sequence number
	MOVE.L	D5,(A5)+		;number of bytes
	LSR.L	#2,D5			;convert to longwords
	CLR.L	(A5)+			;no next key for now
	CLR.L	(A5)+			;zap checksum
9$:	RTS

* Marks all blocks of file key in D0 busy in the main bitmap.

MarkFileBusy:
	PUSH	D2-D3/A2-A3
3$:	MOVE.L	D0,D2			;filehdr/exthdr key
	BEQ.S	9$			;no filehdr key
	BSR	MarkKeyBusy		;mark it
	MOVE.L	D2,D0
	BSR	ReadCtrlBlk		;read filehdr/exthdr
	ERROR	9$
	MOVE.L	A0,A2			;block ptr
	MOVE.L	fh_BlkCnt(A2),D3	;this many blocks in header
	ADD.L	BlockSize,A2
	MOVE.L	A2,A3
	LEA	fh_BlkPtr1+4(A2),A2
	BRA.S	2$
1$:	MOVE.L	-(A2),D0
	BSR	MarkKeyBusy
2$:	DBF	D3,1$
	MOVE.L	fh_Ext(A3),D0
	BNE.S	3$
9$:	POP	D2-D3/A2-A3
	RTS

InitRoot:
	PUSH	A2-A3
	BSR	GetCacheBuffer		;find a buffer for root
	ERROR	9$			;no cache??
	MOVE.L	A0,A2
	MOVE.L	A0,A3
	BSR	ZapBlock		;zap block to 0
	MOVEQ	#T_SHORT,D0
	MOVE.L	D0,rb_Type(A3)		;set primary type
	MOVE.L	HashTblSize,rb_HashSize(A3)
	ADD.L	BlockSize,A3
	MOVEQ	#-1,D0
	MOVE.L	D0,rb_BMFlg(A3)
	MOVE.L	#ST_ROOT,D0
	MOVE.L	D0,rb_SecType(A3)	;set secondary type
	LEA	rb_RMDate(A3),A1	;root modification date
	MOVE.L	A1,D1
	CALLSYS	DateStamp,DosBase
	Z_STG.	VolNameBuf.,rb_VolName(A3)
	LEA	rb_DMDate(A3),A1	;disk mod date
	MOVE.L	A1,D1
	CALLSYS	DateStamp,DosBase
	LEA	rb_CDate(A3),A1		;creation date
	MOVE.L	A1,D1
	CALLSYS	DateStamp,DosBase
	MOVE.L	A2,A0
	BSR	CalcCkSum
	SUB.L	D0,rb_CkSum(A2) 	;not bitmap
	MOVE.L	A2,A0
	DIRTY	A0
	MOVE.L	RootBlkNbr,D0
	MOVEQ	#-1,D1			;this is control block
	BSR	LinkCacheKey
9$:	POP	A2-A3
	RTS

* Copies bitmap keys into root.

CopyBMPtrs:
	PUSH	D2/A2
	MOVE.L	RootBlkNbr,D0
	BSR	ReadCtrlBlock
	ERROR	9$
	DIRTY	A0
	LEA	rb_BMPtrs(A0),A0
	ADD.L	BlockSize,A0
	MOVE.L	BitmapKeyBuffer,A2	;mark bitmap keys busy in bm
	MOVE.L	BitmapBlocks,D2
	ADD.L	BMExtBlocks,D2
	MOVEQ	#26,D1			;max number in root plus ext, if any
	IFLEW	D2,D1,5$
	MOVE.W	D1,D2
	BRA.S	5$
4$:	MOVE.L	(A2)+,D0		;get a bitmap key
	BCLR	#31,D0
	MOVE.L	D0,(A0)+
5$:	DBF	D2,4$
9$:	POP	D2/A2
	RTS

* Init boot block with DOS0 or DOS1, depending on NewType

InitBoot:
	PUSH	A2
	BSR	GetCacheBuffer
	ERROR	9$
	MOVE.L	A0,A2
	BSR	PutCacheBuffer
	MOVE.L	A2,A0
	BSR	ReadBoot
	ERROR	9$
	MOVE.L	NewType,0(A2)
	BSR	CalcCkSum
	SUB.L	D0,4(A2)
	MOVE.L	A2,A0
	BSR	WriteBoot		;write block to disk
9$:	POP	A2
	RTS

Fun6Refresh:
	BSR	CLRWIN
	LEA	FmtVol.,A0
	BSR	InsertVolName		;insert volume
	DispCent #320,#BSTitle_TE,A0,WHITE
	BSR	RefreshAllGads
	RTS

OFSHit:
	MOVE.L	#$444F5300,NewType
	SETF	ProcFlg
	RTS

FFSHit:
	MOVE.L	#$444F5301,NewType
	SETF	ProcFlg
	RTS

**********************************************************************************

* Attempt to unformat a volume.  Looks for QBT.FMT in root.  If can't find it,
* then the only recourse is scanning for deleted files...

Function7:
	BSR	CheckVolumeStatus
	ERROR	MLoop			;problems
	BSR	CheckDiskLoaded
	ERROR	MLoop			;oops...
	BSR	CheckProtection		;make sure we can write
	ERROR	MLoop			;op canceled
	BSR	CalcVolParams		;calc volume params
	ZAPA	A0			;no special gadgets
	LEA	Fun7Refresh,A1
	ZAP	D0
	BSR	PushWindow		;remove gadgets and clear window
	BSR	AllocateBlock
	ERROR	Fun7Exit
	DispReq	UnFormat,ReqYes		;ask if operator is sure...
	IFZB	YesFlg,Fun7Exit		;op elected to bail out
	BSR	InhibitDos
	BSR	ReadBitmap		;make sure bitmap is loaded
	ERROR	NoSavedFormat		;oops
	LEA	Form.,A0
	MOVE.L	RootBlkNbr,D0
	BSR	OpenFile		;try to find saved format info
	ERROR	NoSavedFormat		;can't restore old format
	BSR	LoadFormatData
	ERROR	NoSavedFormat
	BSR	UpdateHashChains	;correct hashchain keys
	BSR	WriteBitmap
	BSR	WriteCache
	BldWarn	0,0,FmtRes1.,FmtRes2.,FmtRes3.,FmtRes4.
	DispReq	A0,ReqYes
Fun7Exit:
	BSR	Cleanup
	IFNZB	YesFlg,Function12,L	;check it out
	BRA	MLoop

NoSavedFormat:
	BldWarn	0,0,NoOld1.,NoOld2.,NoOld3.,NoOld4.
	DispReq	A0,ReqYes
	BSR	Cleanup
	IFZB	YesFlg,MLoop,L
	BRA	Function4

* Copies root and bitmap data from QBT.FMT data blocks.  Ptr to QBT.FMT 
* filehdr in D0.

LoadFormatData:
	PUSH	D2-D7/A2-A5
	MOVE.L	A0,A2			;ptr to filehdr/exthdr
	MOVE.L	fh_Ownkey(A2),D2	;filehdr key
	MOVE.L	A2,A3
	ADD.L	BlockSize,A3		;point to list of keys
	MOVE.L	fh_Ext(A3),D6		;key of exthdr, if any
	LEA	fh_BlkPtr1+4(A3),A3
	MOVE.L	HashTblSize,D7		;max number of keys in this hdr
	MOVE.L	RootBlkNbr,D0
	BSR	ReadCtrlBlock		;get root
	ERROR	9$
	DIRTY	A0
	MOVE.L	A0,A5			;first buffer to be updated
	MOVE.L	BlockSizeL,D5		;this many longwords
	BSR	NextInput		;read first input buffer
	ERROR	9$
	BSR.S	LoadData		;copy root data
	ERROR	9$
	MOVE.L	BitmapBuffer,A5
	MOVE.L	BitmapBufSize,D5
	LSR.L	#2,D5
	BSR.S	LoadData		;copy bitmap data
	ERROR	9$
	LEA	QBTHashChain,A5
	MOVEQ	#2,D5
	BSR.S	LoadData		;restore hashchain keys
	ERROR	9$
	SETF	BMDirtyFlg
9$:	POP	D2-D7/A2-A5
	RTS

* Uses D4/A4 (input) and D5/A5 (output) as byte count/data ptrs respectively.

LoadData:
1$:	IFNZL	D4,2$			;more data, read it
	BSR.S	NextInput
	ERROR	9$
2$:	MOVE.L	(A4)+,(A5)+		;else get a longword
	DECL	D4
	DECL	D5
	BNE.S	1$
9$:	RTS

NextInput:
	IFNZL	D7,1$
	MOVE.L	D6,D0			;get exthdr key
	BEQ.S	9$			;oops...none, bail out
	BSR	ReadExtHdr		;get exthdr
	ERROR	9$
	MOVE.L	A0,A3
	ADD.L	BlockSize,A3
	MOVE.L	fe_Ext(A3),D6		;key of next exthdr
	LEA	fe_BlkPtr1+4(A3),A3
	MOVE.L	HashTblSize,D7		;max keys in this exthdr
1$:	DECL	D7
	MOVE.L	-(A3),D0		;get key of next entry
	BEQ.S	8$
	BCLR	#31,D0			;just in case...
	MOVE.L	BlockBuffer,A4
	MOVE.L	A4,A0
	BSR	ReadBlock		;get bitmap data
	ERROR	9$
	MOVE.L	BlockSizeL,D4
	IFNZB	FFSFlg,9$
	LEA	24(A4),A4		;skip OFS header
	MOVE.L	#488/4,D4
	BRA.S	9$
8$:	STC
9$:	RTS

* Updates hashchain linkages saved with QBT.FMT for BAD.BLOCKS and/or QBT.FMT.
* This prevents files which MIGHT have been linked with those files on the hash 
* collision list from "disappearing" after UnFormatting...

UpdateHashChains:
	MOVE.L	QBTKey,D0
	MOVE.L	QBTHashChain,D1
	BSR.S	UpdateChain
	MOVE.L	BadBlksFileKey,D0
	MOVE.L	BadHashChain,D1
UpdateChain:
	IFZL	D0,9$
	IFZL	D1,9$
	PUSH	D1
	BSR	ReadCtrlBlk
	POP	D1
	ERROR	9$
	DIRTY	A0
	ADD.L	BlockSize,A0
	MOVE.L	D1,fh_HashChain(A0)
9$:	RTS

Fun7Refresh:
	BSR	CLRWIN
	LEA	UnFmtVol.,A0
	BSR	InsertVolName		;insert volume
	DispCent #320,#BSTitle_TE,A0,WHITE
	BSR	RefreshAllGads
	RTS

Cleanup:
	BSR	FlushCache
	BSR	FreeBlock
	BSR	FreeBMBuffers
	BSR	EnableDos
	BSR	PopWindow
	RTS

***************************************************************************

* Called to initialize (low-level format) floppy in active drive.  

FormatFloppy:
	PUSH	D2-D4/A2-A3
;	BSR	CalcVolParams		;calc volume params
	MOVE.L	MaxCylinders,D0
	BSR	InitBar
	ZAPA	A0			;no special gadgets
	LEA	FlopRefresh,A1
	ZAP	D0			;or menus
	BSR	PushWindow		;remove gadgets and clear window
	MOVEQ	#22,D0
;;	MULU	Surfaces+2,D0
	MOVE.L	D0,D3			;blocks per cylinder
	MULU	BlockSize+2,D0		;Size of cylinder buffer
	MOVE.L	D0,CylBufSize
	BSR	AllocateIOBuf
	ERROR	9$
	MOVE.L	D0,A2	
3$:	BSR	CheckDiskLoaded
	ERROR	9$			;oops...
	BSR	CheckProtection		;make sure we can write
	ERROR	9$			;op canceled
	MOVE.L	MaxCylinders,D2
	ZAP	D4			;block counter
	BRA.S	2$
1$:	BSR	ProcessMsg
	IFNZB	CancelFlg,9$
	MOVE.L	D4,D0			;block number
	MOVE.L	D3,D1			;number of blocks
	MOVE.L	A2,A0			;write this empty buffer
	BSR	FormatCyl		;write to disk using format cmd
	ERROR	8$
	MOVE.L	D4,D0
	MOVE.L	D3,D1
	MOVE.L	A2,A0			;read to this empty buffer
	BSR	ReadCyl			;read track just written
	ERROR	8$
	ADD.L	D3,D4
	BSR	UpdateStatusBar		;show progress
2$:	DBF	D2,1$
	ZAP	D2
	BRA.S	9$
8$:	DispReq	BadFlop,ReqRetry
	IFNZB	CancelFlg,4$
	IFZB	ProcFlg,4$
	MOVE.L	MaxCylinders,D0
	BSR	InitBar
	BSR	FlopRefresh
	BRA	3$

4$:	MOVEQ	#1,D2
9$:	BSR	MotorOff
	MOVE.L	A2,A1
	IFZL	A1,10$
	MOVE.L	CylBufSize,D0
	CALLSYS	FreeMem,SysBase
;;	BSR	EnableDos
;;	BSR	CloseDriver
10$:	BSR	PopWindow
	IFZL	D2,11$
	STC
11$:	POP	D2-D4/A2-A3
	RTS

FlopRefresh:
	BSR	CLRWIN
	LEA	FmtFlop.,A0
	BSR	InsertVolName		;insert volume
	DispCent #320,#BSTitle_TE,A0,WHITE
	BSR	RefreshBar
	BSR	RefreshAllGads
	RTS

Cleanup2:

* Asks for and verifies new volume name.  Old name not changed unless new
* name valid.

GetVolName:
1$:	LEN.	NamBuf
	MOVE.W	D0,NamInfo+8
	DispReq	FmtName,ReqVolNamGad
	IFZB	CancelFlg,3$
	STC
	BRA.S	9$
3$:	LEN.	NamBuf
	IFLEIW	D0,#FileNameLen,4$
	DispErr	NameTooLong.
	BRA.S	1$
4$:	Z_STG.	NamBuf,TMP.
	LEA	TMP.,A0
	BSR	ValidateName
	NOERROR	2$
	DispErr	InvFilNam.
	BRA	1$
2$:;;	MOVE.	NamBuf,VolNameBuf.
9$:	RTS


* Invoked from the RESTORE menu to format a floppy disk for copying files.

FmtFlop:
	ZAPA	A0			;no special gadgets
	LEA	FFRefresh,A1
	ZAP	D0			;or menus
	BSR	PushWindow		;remove gadgets and clear window
	LEA	-32(A7),A7		;make room on stack
	MOVE.L	A7,A1
	MOVE.	VolNameBuf.,A1
	LEA	-32(A7),A7
	MOVE.L	A7,A1
	MOVE.	DeviceNameBuf.,A1
	MOVE.W	ADOSFlg,SavedFlags	;WARNING---POTENTIAL TROUBLE!
	SETF	VolNamDevFlg
1$:	MOVE.	DF0.,NamBuf
	MOVEQ	#4,D0
	MOVE.W	D0,NamInfo+8
	BSR	CloseDriver		;temporarily close primary device
4$:	DispReq	FlopDrv,ReqNameGad	;enter drive
	IFNZB	CancelFlg,9$,L		;forget it
	LEA	NamBuf,A0
	IFZB	(A0),1$
	MOVE.	A0,VolNameBuf.
	LEA	VolNameBuf.,A0
	MOVE.L	A0,A1
2$:	TST.B	(A1)+
	BNE.S	2$
	SUBQ.L	#2,A1
	IFNEIB	(A1),#':',3$
	CLR.B	(A1)
3$:	BSR	LoadAltParams
	NOERROR	6$
	MOVE.	VolNameBuf.,TMP.
	APPEND.	NoDev.,TMP.
	DispErr	TMP.			;oops...no drive
	BRA.S	4$
6$:	IFEQ.	TrkDisk.,DriverNameBuf.,5$ ;this device is a floppy
	MOVE.	VolNameBuf.,TMP.
	APPEND.	NotFlop.,TMP.
	DispErr	TMP.			;oops...no drive
	BRA	4$			;that's all we can do
5$:	MOVE.	Unnamed.,NamBuf
	BSR	GetVolName
	ERROR	9$
	MOVE.	NamBuf,VolNameBuf.
	BSR	OpenDriver		;open alt device
	ERROR	9$
	BSR	CheckDiskLoaded
	ERROR	4$
	BSR	CheckProtection
	ERROR	9$
	BSR	CalcVolParams
	BSR	InhibitDos
	BSR	FormatFloppy
	ERROR	7$
	MOVE.L	#$444F5300,NewType
	BSR	FormatQuickly
7$:;;	BSR	EnableDos
9$:	BSR	MotorOff
	BSR	CloseDriver		;close alt device
	BSR	EnableDos
	MOVE.W	SavedFlags,ADOSFlg
	MOVE.L	A7,A0
	MOVE.	A0,DeviceNameBuf.
	LEA	32(A7),A7
	MOVE.L	A7,A0
	MOVE.	A0,VolNameBuf.
	LEA	32(A7),A7
	LEA	DeviceNameBuf.,A0
	BSR	LoadDeviceParams	;reload params for primary device
	BSR	OpenDriver		;reopen primary driver
	BSR	CalcVolParams
	CLR.B	CancelFlg
	CLR.B	ProcFlg
	BSR	PopWindow
	RTS

FFRefresh:
	BSR	CLRWIN
	LEA	FmtFlop.,A0
	BSR	InsertVolName		;insert volume
	DispCent #320,#BSTitle_TE,A0,WHITE
	BSR	RefreshAllGads
	RTS

FmtNamGadHit:
	SETF	ProcFlg
	RTS

Form.		DC.B	7,'QBT.FMT'	;name of safe format file
TrkDisk.	TEXTZ	'trackdisk.device'
DF0.		TEXTZ	'DF0:'

	BSS,PUBLIC

LocalBuffer	DS.L	1
CylBufSize	DS.L	1
NewType		DS.L	1
QBTKey		DS.L	1

* WATCH OUT...THE NEXT TWO MUST BE KEPT TOGETHER

QBTHashChain	DS.L	1
BadHashChain	DS.L	1

SavedFlags	DS.W	1

	END
