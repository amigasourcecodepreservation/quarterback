;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*****************************************************************
*								*
*		COPY.ASM					*
*								*
*	Quarterback Tools Copy Files to Another Volume		*
*								*
*****************************************************************

	INCLUDE "vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"VD0:BOXES.ASM"

	XDEF	CopySelectedFiles


* Externals in RESTORE.ASM

	XREF	DisplayFilePath
;;	XREF	FileName.

* Externals in TOOLS.ASM

	XREF	SysBase,DosBase,IntuitionBase,GraphicsBase
	XREF	ProcessMsg

	XREF	TMP.,TMP2.,TMP3.
	XREF	CancelFlg
	XREF	AbortFlg
	XREF	ProcFlg
	XREF	YesFlg
	XREF	SkipFlg

* Externals in VTEST.ASM

	XREF	InsertVolName

* Externals in Fun.asm

	XREF	CLRWIN

* Externals in FILES.ASM

	XREF	ReadCtrlBlock,ReadExtHdr
	XREF	GetBusyStatus
	XREF	LinkParentDir
	XREF	CheckFileBlocks,BusyFileBlocks
	XREF	FixFileHdr
	XREF	VerifyCtrlBlock
	XREF	CheckBadBlocks
	XREF	GetBadStatus

	XREF	BadBlkFlg

* Externals in DISK.ASM

	XREF	ReadBlock
	XREF	MotorOff

* Externals in Device.asm

	XREF	CheckDeviceList,CheckDiskLoaded
	XREF	CalcVolParams
	XREF	OpenDriver,CloseDriver
	XREF	InhibitDos,EnableDos
	XREF	CheckProtection

	XREF	NumSoftErrors
	XREF	UnitNumber
	XREF	DiskState
	XREF	NumBlocks
	XREF	NumBlocksUsed
	XREF	BytesPerBlock
	XREF	DiskType
	XREF	VolumeNode
	XREF	InUse	
	XREF	MaxBytes
	XREF	MaxBlocks
	XREF	FreeBytes
	XREF	FreeBlocks
	XREF	VolumeOffset
	XREF	LowCyl
	XREF	HighCyl
	XREF	MaxCylinders
	XREF	Surfaces
	XREF	MaxTracks
	XREF	BlockSize,BlockSizeL
	XREF	Reserved
	XREF	BlocksPerTrack
	XREF	NumBuffers
	XREF	BufMemType
	XREF	MaxTransfer
	XREF	PercentFull
	XREF	DosType,FFSFlg
	XREF	DeviceNameBuf.
	XREF	DriverNameBuf.
	XREF	VolValidFlg
	XREF	NoDiskFlg
	XREF	NotDosFlg
	XREF	NotValidFlg
	XREF	WrtProtFlg
	XREF	VolStatusMsg
	XREF	RootBlkNbr
	XREF	HashTblSize
	XREF	BitmapBlocks
	XREF	LowestKey,HighestKey
	XREF	BlkSizeShift

* Externals in GRAPHICS.ASM


* Externals in Messages.asm

	XREF	Copy1.
	XREF	NoFilesSel.
	XREF	CopyFile.
	XREF	BadPath.
	XREF	CantSub.
	XREF	OpenErr.
	XREF	Proc.

* Externals in Gadgets.asm

	XREF	CanGad,PauseGad
	XREF	ReqVolGad,ReqOK,ReqYes,ReqCancel
	XREF	ReqDupGad,ReqProc2,ReqProc1,ReqSalv
	XREF	ResVol
	XREF	NamBuf
	XREF	DelNam1.,DelNam2.
	XREF	NoDelEr1.,NoDelEr2.
	XREF	NoNamFndTxt
	XREF	NoDelFndTxt

* Externals in CAT.ASM

	XREF	DispFileCatalog
	XREF	PushWindow,PopWindow,NextWindow
	XREF	UpdWinFlg
	XREF	AppendFIB,TruncatePath

	XREF	DirTree
	XREF	SelDeleted,SelActive
	XREF	Path.
	XREF	ParentDir

* Externals in BUFFERS.ASM

	XREF	ReadControl,ReadData
	XREF	GetCacheBuffer,PutCacheBuffer
	XREF	ZapBlock,ZapAnyBlock
	XREF	AllocateBuffer,AllocateIOBuf
	XREF	ReadCache,WriteCache
	XREF	AllocateBlock,FreeBlock
	XREF	AllocateExtBuffer,FreeExtBuffer
	XREF	CalcCkSum
	XREF	FreeBadBlkBuf

	XREF	BlockBuffer
	XREF	ExtentBuffer

* EXternals in BITMAP.ASM

* Copies files to a different volume

CopySelectedFiles:
	PUSH	A2
	CLR.B	NewDestFlg
	LEA	PauseGad,A0
	LEA	CopyRefresh,A1
	ZAP	D0
	BSR	NextWindow
	MOVE.L	SelDeleted,D0
	ADD.L	SelActive,D0
	BNE.S	1$			;got something to copy
	DispErr	NoFilesSel.		;nothing to restore
	BRA	9$
1$:	BSR	VerifyDestPath		;try to open dest path
	ERROR	9$			;problems, or op bailed out
	BSR	AllocateExtBuffer
	BSR	InitTree		;reset to top
2$:	BSR	NextFile		;get next deleted file from list
	ERROR	9$			;end of list
	MOVE.L	A0,A2			;save ptr to file to be restored
	BSR	CopyOneFile
	IFNZB	AbortFlg,9$
	BSR	ProcessMsg
	IFZB	CancelFlg,2$,L		;loop unless op cancels
9$:	BSR	FreeExtBuffer
	POP	A2
	RTS

* This routine actually restores the file whose FIB is in A0.

CopyOneFile:
	PUSH	D2-D4/A2-A3
	MOVE.L	A0,A2			;ptr to entry for this file
	BSR	AppendFIB		;add name path for now
	BSR	DispFilePath
	MOVE.L	#Path.,D1
	MOVE.L	#NEW,D2
	CALLSYS	Open,DosBase
	MOVE.L	D0,D2
	BEQ.S	8$			;oops...trouble opening file
	MOVE.L	A2,A0
	BSR	CopyData		;copy file data to dest file
	ERROR	1$
	BCLR	#fib_Selected,df_Flags(A2) ;clear selected bit if successful
1$:	MOVE.L	D2,D1
	CALLSYS	Close,DosBase		;close file
	CLC
	BRA.S	9$

8$:	DispErr	OpenErr.
	SETF	CancelFlg		;get us out of here
9$:	BSR	TruncatePath
	POP	D2-D4/A2-A3
	RTS

* Initializes the tree structure so that NextFile can return the selected
* files in order.

InitTree:
	MOVE.L	DirTree,A0
	MOVE.L	A0,ParentDir
	LEA	df_Child(A0),A1		;tricky...
	MOVE.L	A1,df_CurFib(A0)
	RTS

* Advances to next selected file in tree structure.  Returns ptr in A0 or 
* CY=1 if end of list.  This version of the code creates ADOS subdirectories
* if they do not already exist.

NextFile:
	PUSH	D2/A2-A3
	MOVE.L	ParentDir,A2
	MOVE.L	df_CurFib(A2),A0	;ptr to current item
1$:	MOVE.L	df_Next(A0),A0		;get fwd link
	IFZL	A0,3$			;pop to previous level
	MOVE.B	df_Flags(A0),D0		;get the flags
;	BTST	#fib_NotBusy,D0		;no, a file...deleted file?
;	BEQ.S	1$			;not deleted, go on
	BTST	#fib_Selected,D0	;selected to be restored?
	BEQ.S	1$			;no...skip over
	BTST	#fib_Dir,D0		;directory?
	BNE.S	2$			;yes...drop down a level
	MOVE.L	A0,df_CurFib(A2)	;save ptr to this entry
	BRA.S	9$			;and return to caller
2$:	MOVE.L	A0,A2			;new entry is now parent
	MOVE.L	A2,ParentDir
	BSR	AppendFIB
	LEA	Path.,A3
	MOVE.L	A3,D1
	MOVEQ	#ACCESS_READ,D2
	CALLSYS	Lock,DosBase		;subdir already exists?
	MOVE.L	D0,D1
	BNE.S	4$			;yes
	MOVE.L	A3,D1
	CALLSYS	CreateDir		;else create missing subdir
	MOVE.L	D0,D1
	BNE.S	4$
	DispErr	CantSub.
	BRA.S	8$
4$:	CALLSYS	UnLock
	LEA	df_Child(A2),A0		;init to first entry
	MOVE.L	A0,df_CurFib(A2)
	BRA.S	1$
3$:	BSR	TruncatePath
	MOVE.L	A2,A0			;make parent the current entry
	MOVE.L	df_Parent(A2),A2	;back up a level
	MOVE.L	A2,ParentDir
	IFNZL	A2,1$,L			;valid level, scan this level
8$:	STC				;else bail out
9$:	POP	D2/A2-A3
	RTS

* Called with destination filehandle in D0 and filehdr entry in A0.

CopyData:
	PUSH	D2-D4/A2-A3
	MOVE.L	D0,D2
	MOVE.L	A0,A2
	MOVE.L	df_Ownkey(A2),D0
	BSR	ReadCtrlBlock		;get filehdr
	ERROR	8$
	MOVE.L	A0,A3
	ADD.L	BlockSize,A0
	MOVE.L	fh_FileSize(A0),D3	;filehdr claims file is this big
1$:	MOVE.L	A3,A0
	BSR	ReadExtent
;;	ERROR	5$
	NOERROR	6$
;;	BSR	FixData			;number of data blocks in D0
;;	NOERROR	6$
5$:	MOVE.L	D0,D4			;save adjusted byte count
	MOVE.	df_Name(A2),TMP.
	BldWarn	0,0,CpyErr1.,CpyErr2.,CpyErr3.,CpyErr4.
	DispReq	A0,ReqSalv
	IFNZB	SkipFlg,8$,L
	IFNZB	CancelFlg,8$,L	
	MOVE.L	D4,D0
6$:	IFLEL	D0,D3,2$		;max byte count in this extent
	MOVE.L	D3,D0			;only this many left anyway
2$:	MOVE.L	D0,D1			;pass byte count
	SUB.L	D1,D3			;have written this many
	MOVE.L	D2,D0			;ADOS file handle
	BSR	WriteExtent
	NOERROR	7$
	MOVE.	df_Name(A2),TMP.
	MOVE.	NamBuf,TMP2.
	BldWarn	TMP2.,0,CpyErr5.,CpyErr6.,CpyErr7.,CpyErr8.
	DispReq	A0,ReqCancel
	SETF	CancelFlg
	BRA.S	8$
7$:	ADD.L	BlockSize,A3
	MOVE.L	fh_Ext(A3),D0
	IFZL	D0,9$			;no extension hdr
	MOVE.L	df_Ownkey(A2),D1	;parent key
	BSR	ReadExtHdr
	ERROR	5$
	MOVE.L	A0,A3
	BRA	1$
8$:	STC
9$:	POP	D2-D4/A2-A3
	RTS

* Reads data blocks for exthdr in A0 into extent buffer.  If error reading 
* block, leaves empty bytes for that block and proceeds to next block, if any.  
* Returns count of blocks read in D0, CY=1 on errors.

ReadExtent:
	PUSH	D2-D5/A2-A3
	ZAP	D3			;errors
	MOVE.L	fh_BlkCnt(A0),D2
	MOVE.L	D2,D4
	LEA	fh_BlkPtr1+4(A0),A2
	ADD.L	BlockSize,A2		;point to data block keys
	MOVE.L	ExtentBuffer,A3
	BRA.S	3$

2$:	SUBQ	#4,A2
	MOVEQ	#2,D5
4$:	MOVE.L	(A2),D0			;get data block key
	MOVE.L	A3,A0			;read to this address
	BSR	ReadBlock
	NOERROR	1$
	DBF	D5,4$
	INCL	D3
	MOVE.L	A3,A0
	BSR	ZapBlock		;replace bad data with null bytes
1$:	ADD.L	BlockSize,A3

3$:	DBF	D2,2$
	MOVE.L	D4,D0
	BSR	FixData			;number of data blocks in D0
	ERROR	8$
	IFZL	D3,9$			;no errors
8$:	STC
9$:	POP	D2-D5/A2-A3
	RTS

* Checks and corrects OFS data by performing checksum calc and stripping off
* header data, leaving raw file data in extent buffer.  Block count in D0.
* Returns updated byte count in D0.  If checksum fails, returns CY=1 but DOES
* complete the operation and returns byte count needed by next step.

FixData:
	PUSH	D2-D3/A2-A3
	ZAP	D3
	IFNZB	FFSFlg,7$		;FFS volume...do nothing
	MOVE.L	D0,D2			;block count
	BEQ.S	9$			;no data blocks
	MOVE.L	BlkSizeShift,D1
	LSL.L	D1,D0			;byte count based on block count
	MOVE.L	ExtentBuffer,A0
	MOVE.L	A0,A3
	BRA.S	4$
3$:	MOVE.L	A2,A0
	BSR	CalcCkSum
	BEQ.S	5$	
	INCL	D3			;oops...failed checksum test
5$:	LEA	24(A2),A0		;skip past OFS data header
	MOVE.L	#488/4,D0
	BRA.S	2$
1$:	MOVE.L	(A0)+,(A3)+
2$:	DBF	D0,1$
4$:	MOVE.L	A0,A2
	DBF	D2,3$
	MOVE.L	A3,D0
	SUB.L	ExtentBuffer,D0		;calc final byte count
	IFZL	D3,9$			;errors?
	STC
	BRA.S	9$
7$:	MULU	BlockSize+2,D0
9$:	POP	D2-D3/A2-A3
	RTS

* Writes D1 bytes data bytes from the extent buffer to AmigaDOS filehandle D0.
* The byte count is taken from the filehdr, and is assumed to be correct.

WriteExtent:
	PUSH	D2-D3
	MOVE.L	D1,D3
	MOVE.L	D0,D1
	MOVE.L	ExtentBuffer,D2
	CALLSYS	Write,DosBase
	INCL	D0
	BNE.S	9$			;good write
	STC
9$:	POP	D2-D3
	RTS

* Gets and checks the destination volume path provided by user.  
* If it can't be found, gives user a chance to change the path and 
* try again or bail out.

VerifyDestPath:
	PUSH	D2
	CLR.B	NamBuf			;start with no name
1$:	DispReq	ResVol,ReqVolGad
	IFNZB	CancelFlg,8$
	MOVE.L	#NamBuf,D1
	MOVEQ	#ACCESS_READ,D2
	CALLSYS	Lock,DosBase
	MOVE.L	D0,D1
	BNE.S	2$
	MOVE.	BadPath.,TMP.
	APPEND.	NamBuf,TMP.
	DispErr	TMP.
	BRA.S	1$

2$:	CALLSYS	UnLock
	MOVE.	NamBuf,Path.		;init destination path
	MOVE.W	#PathSize-3,D1		;max path with room for '/' and null
	LEA	Path.,A1
3$:	TST.B	(A1)
	BEQ.S	4$			;end of Path.?
	INCL	A1			;no
	DBF	D1,3$			;keep searching
	BRA.S	8$			;oops...already too long
4$:	IFEQIB	-1(A1),#':',9$		;found device/vol
	MOVE.B	#'/',(A1)+		;else add trailing slash
	CLR.B	(A1)			;null term
	BRA.S	9$
8$:	STC
9$:	POP	D2
	RTS

CopyRefresh:
	PUSH	D2/A2-A4
	BSR	CLRWIN
	LEA	Copy1.,A0
	BSR	InsertVolName		;insert volume
	DispCent #320,#BSTitle_TE,A0,WHITE
	BSR	DispFilePath
	LEA	PauseGad,A0
	GadColor A0,ORANGE
	LEA	CanGad,A0
	GadColor A0,BLACK
	POP	D2/A2-A4
	RTS


DispFilePath:
	DispMsg	#RFilNam_LE,#RFilNam_TE,Proc.,WHITE ;processing file
	DispCur	Path.,ORANGE,BLUE	;path
	EraseEOL
	RTS

NewDestFlg	DC.B	0		;1=bail out (try new dest vol)


	END
