;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*****************************************************************
*								*
*		GRAPHICS.ASM					*
*								*
*	Quarterback Tools Fast Graphics Functions		*
*								*
*****************************************************************

	INCLUDE "vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"VD0:BOXES.ASM"

	XDEF	InitBar
	XDEF	UpdateStatusBar
	XDEF	RefreshBar

* Externals in TOOLS.ASM

	XREF	SysBase,DosBase,IntuitionBase,GraphicsBase
	XREF	Window,TMP.,TMP2.

* Externals in BITMAP.ASM

	XREF	BitmapDataPtr

* Externals in Device.asm

	XREF	MaxBlocks
	XREF	HighestKey,LowestKey


* Called with max item count (such as MaxBlocks) in D0, to calculate
* the delta between bar updates.

InitBar:
	PUSH	D2
	CLR.B	BarFlg
	MOVE.L	D0,D1			;make a copy
	MOVE.L	#320,D2
	IFGEL	D0,D2,1$		;at least one count per bar step
	SETF	BarFlg
	DIVU	D0,D2			;find out how large the step should be
	MOVE.W	D2,BarIncrement		;each count adds this much to BarSize
	MOVE.L	D2,D1
	MULU	D0,D1			;calculate full bar size
	BRA.S	2$
1$:	ADD.L	D2,D0			;round up for accuracy
	DECL	D0
	DIVU	D2,D0			;divide to get update delta
	MOVE.W	D0,UpdateCount		;nbr of iterations before bar update
	MOVE.W	D0,BarCntr		;init bar counter
	DIVU	D0,D1			;get max number of bar updates
2$:	ADDQ.L	#6,D1			;new size of bar
	MOVE.W	D1,ProgBar_WD		;this is width of bar
	MOVE.W	#640,D0			;window width
	SUB.W	D1,D0			;calc left edge
	ASR.W	#1,D0
	MOVE.W	D0,ProgBar_LE		;this is left edge of bar
9$:	CLR.W	BarSize			;start the bar from 0
	POP	D2
	RTS

* This routine updates the progress bar without high processing overhead
* and multiple precision math.

UpdateStatusBar:
	PUSH	D2
	MOVE.W	BarIncrement,D2
	IFNZB	BarFlg,3$
	DECW	BarCntr			;one more count
	BNE.S	9$			;don't update yet
	MOVE.W	UpdateCount,BarCntr	;reset bar counter
	MOVEQ	#1,D2
	BRA.S	3$
2$:	INCW	BarSize			;one more bar
	MOVE.W	BarSize,D1
	IFLTW	ProgBar_WD,D1,9$
	DispWBar ProgBar_LE,#ProgBar_TE,D1,Window
3$:	DBF	D2,2$
9$:	POP	D2
	RTS

* Called to refresh progress bar.

RefreshBar:
	PUSH	D2
	CLRBOX	ProgBar_LE,#ProgBar_TE,ProgBar_WD,#ProgBar_HT,ORANGE
	MOVE.W	BarSize,D2
	BEQ.S	9$
;;	ADDQ.W	#2,D2
	MOVE.W	#-1,BarSize
1$:	INCW	BarSize
	DispWBar ProgBar_LE,#ProgBar_TE,BarSize,Window
	IFLEW	BarSize,D2,1$
9$:	POP	D2
	RTS

	BSS,PUBLIC

ProgBar_LE	DS.W	1	;progress bar left edge
ProgBar_WD	DS.W	1	;progress bar width
UpdateCount	DS.W	1	;Number of operations before updating bar
BarSize		DS.W	1	;bar size variable
BarIncrement	DS.W	1	;BarSize increment if BarFlg=1
BarCntr		DS.W	1	;countdown counter for bar updates
BarFlg		DS.B	1	;0=normal, 1=multi-step

	END


