;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*************************************************************************
*									*
*			FUN.ASM - UTILITY FUNCTIONS			*
*									*
*		Copyright 1990 Central Coast Software			*
*									*
*************************************************************************

	INCLUDE "vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"

	XDEF	LEN..,CMP..,SCAN..,MOVE..,CLIP..,STRIP_LB..,STRIP_TB..
	XDEF	CMPUC,MOVEL..
	XDEF	STG_Z..,Z_STG..,BSTR_Z..,STR..,LEFT..,RIGHT..
	XDEF	APPEND..,ACHAR..,STRING..,UCASE..,GTLCHAR..
	XDEF	MoveChars
	XDEF	DISP_REQ,DISP_ERR
	XDEF	DISP_CURRENT,DISP_MSG,DISP_CENT
;;	XDEF	DISP_YN,ReqYN
	XDEF	EraseEOL..
	XDEF	AddGadgets,RemoveGadgets  
	XDEF	MoveText,MoveRight,MoveLeft
	XDEF	DRAW_BOXES,CLRWIN,CLEAR_BOX,GAD_COLOR,ClearBox
	XDEF	StripAppend
	XDEF	REFRESH,CloseSafely
	XDEF	MoveInsertDevice,MoveDevice
	XDEF	DISP_WBAR,DISP_RBAR
	XDEF	RefreshAllGads

	XDEF	LongDivide
	XDEF	BldWarningMsg,LoadTmps
	XDEF	SetSpacing

	XDEF	GadList

* Externals in TOOLS.ASM

	XREF	SysBase,DosBase,IntuitionBase,GraphicsBase
	XREF	RefreshRoutine

	XREF	ABORT_EXIT
	XREF	Window
	XREF	GAD_PTR,MSG_CLASS,MSG_CODE
	XREF	TMP.,TMP2.,TMP3.

* Externals in BEEP.ASM

	XREF	AudioBeep

* Externals in DEVICE.ASM

	XREF	VolNameBuf.

* Externals in DISK.ASM

	XREF	MotorOff

* Externals in Gadgets.asm

	XREF	RQText,RQTxtBuf.,RQTxtBf1.,RQTxtBf2.,RQTxtBf3.,RQTxtBf4.
	XREF	REQUEST,REQTXT,REQGAD
	XREF	OKTXT

	XREF	WINI,IDATA,WINDSIZE
	XREF	ERRTXT,POSTXT,NEGTXT,DT_TXT

* Externals in VTEST.ASM

	XREF	AbortFlg
	XREF	CancelFlg,ProcFlg

* Externals in AREXX.ASM

	XREF	ArexxFlg

MP_MSGLIST	EQU	20	;msg fwd link offset into MsgPort
ReqLayer	EQU	32
LayerRP		EQU	12

* Returns length in D0 of null-terminated string pointed to be A0.
* Does not change A0.  Length does NOT include the null termination char.

LEN..:	MOVEQ	#-1,D0
1$:	INCL	D0
	TST.B	0(A0,D0.W)
	BNE.S	1$
	RTS

* COMPARES TWO STRINGS.  SOURCE STRING PTR IN A0, DEST PTR IN A1.  
* RETURNS Z-BIT SET ON STRINGS EQUAL, CLEAR IF NOT.  Handles null strings.

CMP..:	MOVEQ	#-1,D0
1$:	INCL	D0		;measure length of first string
	TST.B	0(A0,D0.W)
	BNE.S	1$
2$:	CMPM.B	(A0)+,(A1)+	;DO THE COMPARISON..including null bytes
	BNE.S	3$		;NO MATCH
	DBF	D0,2$		;LOOP FOR ALL BYTES
3$:	RTS

* Does the same as above, but forces upper case, so NOT case sensitive.

CMPUC:	PUSH	D2
	MOVEQ	#-1,D0
1$:	INCL	D0		;measure length of first string
	TST.B	0(A0,D0.W)
	BNE.S	1$
2$:	MOVE.B	(A0)+,D1
	UCASE	D1
	MOVE.B	(A1)+,D2
	UCASE	D2
	CMP.B	D1,D2		;DO THE COMPARISON..including null bytes
	BNE.S	3$		;NO MATCH
	DBF	D0,2$		;LOOP FOR ALL BYTES
3$:	POP	D2
	RTS

* SCANS A TOKEN FROM THE SOURCE STRING (A0) INTO THE DEST
* STRING (A1).

SCAN..:
	PUSH	A0-A1		;save string ptr
	BSR	STRIP_LB..	;get rid of any leading blanks
	POP	A0-A1
	MOVE.L	A0,D0		;save ptr to start of source
	MOVEQ	#70,D1		;max token size
2$:	MOVE.B	(A0),(A1)	;move token to dest
	BEQ.S	1$		;end of source, token terminated
	CMPI.B	#' ',(A0)	;was it BLANK?
	BEQ.S	3$		;YES...STOP HERE
	CMPI.B	#10,(A0)	;was it LF?
	BEQ.S	3$		;yes...treat same as blank
	INCL	A1
5$:	INCL	A0		;POINT TO NEXT TEXT BYTE
	DECW	D1		;limit token length
	BNE.S	2$		;still okay
	BRA.S	3$

1$:	MOVE.L	D0,A0
	CLR.B	(A0)		;SOURCE STG NOW EMPTY
	BRA.S	9$

3$:	CLR.B	(A1)		;change blank to null in token
	MOVE.L	D0,A1
4$:	MOVE.B	(A0)+,(A1)+	;move up remainder of text in source
	BNE.S	4$
9$:	RTS

* Truncates leading blanks from TMP2. and appends result to TMP.

StripAppend:
	LEA	TMP2.,A0
	BSR	STRIP_LB..
	LEA	TMP2.,A0
	LEA	TMP.,A1
;	RTS

* APPENDS SOURCE STRING (A0) TO END OF DEST STRING (A1)

APPEND..:
1$:	TST.B	(A1)+		;position to end of dest
	BNE.S	1$
	DECL	A1		;backup to null

* MOVES SOURCE STRING TO DESTINATION STRING.

MOVE..:	MOVE.B	(A0)+,(A1)+	;MOVE TEXT
	BNE.S	MOVE..
	RTS

MOVEL..:
	BRA.S	2$
1$:	MOVE.B	(A0)+,(A1)+
	BEQ.S	9$
2$:	DBF	D0,1$
	CLR.B	(A1)
9$:	RTS

* CONVERTS NULL-TERMINATED STRING IN A0 TO leading-length STRING IN A1.

Z_STG..:
	PUSH	A0-A2
	MOVE.L	A1,A2
	CLR.B	(A1)+		;START WITH ZERO LENGTH
1$:	MOVE.B	(A0)+,D0	;GET A BYTE
	BEQ	2$		;END OF STRING
	BCLR.B	#7,D0		;GET RID OF POSSIBLE HIGH BIT
	MOVE.B	D0,(A1)+	;STORE THE CHAR
	INCB	(A2)		;AND COUNT IT
	BRA.S	1$
2$:	POP	A0-A2
	RTS

* Converts leading-length string pointed to by A0 into null-terminated string.

STG_Z..:
	MOVE.L	A0,A1		;POINT A1 TO DEST
	BRA.S	StgCom

* Converts BSTR pointed to by A0 into null-terminated string stored at A1.
* Returns destination string ptr in A0.

BSTR_Z..:
	AFIX	A0		;BPTR-->APTR
StgCom:
	ZAP	D0
	MOVE.B	(A0)+,D0	;GET LENGTH AND POINT TO 1ST TEXT BYTE
	BEQ.S	9$		;NOTHING TO MOVE
	DECW	D0		;ADJUST FOR DBF
1$:	MOVE.B	(A0)+,(A1)+	;MOVE one BYTE
	DBF	D0,1$
9$:	CLR.B	(A1)		;PUT NULL AT END OF STRING
	RTS

* RETURNS THE LAST CHAR IN D0 OF STRING pointed to by A0.
* COMPARES THAT CHAR TO BYTE IN D7.

GTLCHAR..
	MOVEQ	#-1,D0
1$:	INCL	D0
	TST.B	0(A0,D0.W)
	BNE.S	1$
	MOVE.B	-1(A0,D0.W),D0	;GET LAST CHAR
	CMP.B	D0,D7
	RTS

* STRIPS LEADING BLANKS FROM STRING IN A0.

STRIP_LB..:
	PUSH	A1
	MOVE.L	A0,A1		;SAVE PTR TO SOURCE
	TST.B	(A0)
	BEQ.S	2$		;NULL STRING
1$:	CMP.B	#' ',(A0)
	BNE.S	2$		;NO BLANK...STOP SCAN
	INCL	A0
	BRA.S	1$		;SEARCH ENTIRE STRING
2$:	MOVE.B	(A0)+,(A1)+	;MOVE STRING UP
	BNE.S	2$
	POP	A1
	RTS

*STRIPS TRAILING BLANKS FROM A STRING.  Doesn't change A0.

STRIP_TB..:
	PUSH	D0
	BSR	LEN..		;GET LENGTH IN D0
	TST.W	D0
	BEQ.S	1$		;NOTHING TO SCAN
	SUB.L	#1,D0
2$:	CMP.B	#' ',0(A0,D0.W)	;GOT A BLANK?
	BNE.S	1$		;NO...ALL DONE
	CLR.B	0(A0,D0.W)	;else make it null
	DBF	D0,2$
1$:	POP	D0
	RTS

* CLIPS D0 LEADING CHARS FROM STRING IN A0.

CLIP..:
	PUSH	D1/A1
	MOVE.W	D0,D1
	BSR	LEN..
	IFLEW	D1,D0,1$	;okay...we have enough bytes
	CLR.B	(A0)		;ELSE NOW HAVE NULL STRING
	BRA.S	3$
1$:	MOVE.L	A0,A1
	ADD.W	D1,A0		;POINT TO REMAINDER OF STRING
2$:	MOVE.B	(A0)+,(A1)+
	BNE.S	2$
3$:	POP	D1/A1
	RTS

* CONVERTS STRING A0 FROM LOWER CASE TO UPPER CASE.

UCASE..:
1$:	MOVE.B	(A0)+,D0	;GET A BYTE
	BEQ.S	2$		;end of string
	CMPI.B	#'a',D0
	BLT.S	1$		;NOT LOWER CASE ALPHA
	CMPI.B	#'z',D0
	BGT.S	1$		;NOT LOWER CASE ALPHA
	ANDI.B	#$5F,D0		;MAKE IT UPPER
	MOVE.B	D0,-1(A0)	;RESTORE CHAR TO STRING
	BRA.S	1$
2$:	RTS

* CONVERTS THE 32-bit UNSIGNED BINARY NUMBER IN D0 TO n-CHAR ASCII
* STRING POINTED TO BY A0.  CHAR IN D1 IS THE FILL CHAR IF RESULT HAS 
* LEADING 0'S; D1 high word=number of digits in result.
* Warning - the destination string must be long enough to accept result!

STR..:
	PUSH	A0/D2-D4
	PUSH	D1
	SWAP	D1
	MOVE.W	D1,D3		;number of DIGITS
	ADD.W	D3,A0		;POINT PAST END OF STRING
	CLR.B	(A0)		;terminate string
	MOVE.L	#10,D2		;FOR DIVIDE
	MOVEQ	#3,D4		;for inserting commas
1$:	PUSH	D0		;SAVE 32-BIT DIVIDEND
	ZAP	D0
	MOVE.W	(SP)+,D0	;GET UPPER 16-BITS
	DIVU	D2,D0		;DIVIDE NUMBER BY 10
	MOVE.L	D0,D1		;SAVE REMAINDER FOR NEXT DIVISION
	SWAP	D0		;SAVE QUOTIENT FOR UPPER RESULT
	MOVE.W	(SP)+,D1	;GET LOWER WORD
	DIVU	D2,D1		;DIVIDE UPPER REMAINDER AND LOWER WORD
	MOVE.W	D1,D0		;QUOTIENT TO D0
	SWAP	D1		;REMAINDER
	ADD.B	#$30,D1		;MAKE INTO ASCII CHAR
	IFNZW	D4,5$		;no comma yet
	MOVE.B	#',',-(A0)	;insert comma
	DECW	D3
	BEQ.S	4$
	MOVEQ	#3,D4		;and reset counter
5$:	MOVE.B	D1,-(A0)	;AND STORE INTO BUFFER
	DECW	D4		;comma counter
	DECW	D3
	BEQ.S	4$		;oops...results too big...bail out
	TST.L	D0		;ANY QUOTIENT LEFT?
	BNE.S	1$		;YES...GO AGAIN
4$:	POP	D0		;GET FILLER CHAR
	IFZW	D3,3$		;no more room...don't fill
	DECW	D3		;ADJUST FOR DBF
2$:	MOVE.B	D0,-(A0)	;STORE FILLER CHAR
	DBF	D3,2$
3$:	POP	A0/D2-D4
	RTS

* FILLS STRING (A0) WITH CHAR IN D0, FOR COUNT IN D1.

STRING..:
	TST.W	D1
	BMI.S	9$
	BRA.S	2$
1$:	MOVE.B	D0,(A0)+
2$:	DBF	D1,1$
	CLR.B	(A0)		;add null termination
9$:	RTS

* APPENDS A SINGLE CHAR IN D0 TO STRING POINTED TO BY A0.

ACHAR..:
1$:	TST.B	(A0)+
	BNE.S	1$		;position to end of string
	MOVE.B	D0,-1(A0)
	CLR.B	(A0)
	RTS

* EXTRACTS LEFT SUBSTRING (A1) FROM SRC STRING (A0) FOR D0 CHARS OR LESS.

LEFT..:
	IFZW	D0,9$		;zero length
	DECW	D0
1$:	MOVE.B	(A0)+,(A1)+
	DBEQ	D0,1$		;copy until null or length
9$:	CLR.B	(A1)		;TERMINATE substring
	RTS

* EXTRACTS RIGHT SUBSTRING (A1) FROM SRC STRING (A0) FOR D0 CHARS OR LESS.

RIGHT..:
	CLR.B	(A1)		;just in case...
	MOVE.W	D0,D1		;save length desired
	BEQ.S	9$		;null...bail out
	BSR	LEN..		;get length of source
	IFGEW	D1,D0,1$	;use all of source
	SUB.W	D1,D0
	ADDA.W	D0,A0		;point to proper place for move
1$:	MOVE.B	(A0)+,(A1)+
	BNE.S	1$
9$:	RTS

* MOVES N TEXT CHARS.  A0=SOURCE, A1=DEST.  D0=MAX BYTE COUNT.

MoveChars:
	DECW	D0		;ADJUST FOR DBF
1$:	MOVE.B	(A0)+,(A1)+
	DBF	D0,1$
	RTS

* DISPLAYS INTUITEXT IN THE Window.  POINTER TO TEXT STRING IN A0.
* X,Y OFFSET FROM 0,0 IN D0 (WORDS).  Colors and mode in D1.
* Enter at DISP_CURRENT to add this string to current position.
* Enter at DISP_CENT to center the string on the x position.

DISP_CURRENT:
	PUSH	D2/A2
	LEA	DT_TXT,A2
	BRA.S	D_com1

DISP_MSG:
	PUSH	D2/A2
	LEA	DT_TXT,A2		;POINTER TO INTUITEXT STRUCT
	MOVE.L	D0,it_LeftEdge(A2)	;OFFSET TO LEFT, TOP EDGE
D_com1:
	MOVE.W	D1,it_FrontPen(A2)	;set colors
	SWAP	D1
	MOVE.B	D1,it_DrawMode(A2)
	MOVE.L	A0,it_IText(A2)		;POINTER TO NULL-TERM STRING
	MOVE.L	A2,A0
	CALLSYS	IntuiTextLength,IntuitionBase
	MOVE.W	D0,D2			;length of string in pixels
	LEA	-80(SP),SP		;make room on stack
	MOVE.L	Window,A0
	ADD.W	it_LeftEdge(A2),D0
	IFGTW	wd_Width(A0),D0,4$	;string fits available space
	MOVE.W	wd_Width(A0),D0
	SUB.W	it_LeftEdge(A2),D0	;count remaining pixels
	SUBQ.W	#8,D0			;allow one extra char spacing
	BMI.S	9$			;oops...no room
	ASR.L	#3,D0			;count chars remaining
	MOVE.L	it_IText(A2),A0		;move old string from here
	MOVE.L	SP,A1
	MOVE.L	A1,it_IText(A2)		;ptr to temp string
	BRA.S	2$
1$:	MOVE.B	(A0)+,(A1)+
2$:	DBF	D0,1$
	CLR.B	(A1)+			;terminate string
4$:	MOVE.L	Window,A0
	MOVE.L	wd_RPort(A0),A0		;RPort
	MOVE.L	A2,A1
	ZAP	D0
	ZAP	D1
	CALLSYS	PrintIText		;PUT IT ON THE WINDOW
	ADD.W	D2,it_LeftEdge(A2)	;add length of stg to left edge
9$:	LEA	80(SP),SP		;clean up stack
	POP	D2/A2
	RTS


DISP_CENT:
	LEA	DT_TXT,A1
	MOVE.L	D0,it_LeftEdge(A1)	;OFFSET TO LEFT, TOP EDGE
	MOVE.W	D1,it_FrontPen(A1)	;color set colors
	SWAP	D1
	MOVE.B	D1,it_DrawMode(A1)
	MOVE.L	A0,it_IText(A1)		;POINTER TO NULL-TERM STRING
	MOVE.L	A1,A0
	CALLSYS	IntuiTextLength,IntuitionBase
	LEA	DT_TXT,A1
	ASR.W	#1,D0			;divide by 2
	SUB.W	D0,it_LeftEdge(A1)	;offset left edge of text
	BRA.S	D_com2	

EraseEOL..:
	LEA	DT_TXT,A1
	MOVE.L	Window,A0
	MOVE.W	wd_Width(A0),D1
	SUB.W	#4,D1			;allow for right margin
	SUB.W	it_LeftEdge(A1),D1
	BPL.S	1$
	RTS
1$:	ASR.W	#3,D1			;calc number of chars
	LEA	TMP.,A0
	MOVE.L	A0,it_IText(A1)
	MOVEQ	#' ',D0
	BSR	STRING..
D_com2:	MOVE.L	Window,A0
	MOVE.L	wd_RPort(A0),A0		;RPort
	ZAP	D0
	ZAP	D1
	CALLSYS	PrintIText,IntuitionBase ;PUT IT ON THE WINDOW
	RTS


* DISPLAYS ERROR MSG POINTED TO BY A0 IN STANDARD REQUESTER WITH NO
* POSITIVE OPTION.  WAITS FOR NEGATIVE REPLY BY OPERATOR.

DISP_ERR:
	PUSH	D2-D3/A2-A3/A6
	IFNZB	ArexxFlg,2$
	MOVE.L	A0,-(SP)	;save msg
	BSR	MotorOff	;turn motor off
	MOVE.L	(SP)+,A0
	ZAPA	A2
	LEA	OKTXT,A3
	LEA	ERRTXT,A1	;ITEXT STRUCT FOR ERR MSGS
	MOVE.L	A0,it_IText(A1)	;SAVE STRING
	PUSH	A1		;save ptr to body IText
	MOVE.L	A1,A0
	CALLSYS	IntuiTextLength,IntuitionBase
	MOVE.L	D0,D2		;length of msg in pixels of default font
	ADD.W	#50,D2		;allow a bit of room
	MOVE.L	#REQ_WD,D3	;min requester width
	IFGEL	D2,D3,1$	;wide enough...
	MOVE.L	D3,D2		;else use min width
1$:	POP	A1		;body IText
	MOVE.L	Window,A0
	ZAP	D0		;NO POS FLAGS
	ZAP	D1		;NO NEG FLAGS
	MOVEQ	#REQ_HT,D3	;STANDARD REQUESTER HEIGHT
	CALLSYS	AutoRequest
	TST.L	D0
	BNE.S	2$
	STC
2$:	POP	D2-D3/A2-A3/A6
	RTS


* Displays requester and waits for operator action.
* Requester text structure in A0, gadget list in A1.

DISP_REQ:
	MOVE.L	A0,REQTXT
	MOVE.L	A1,REQGAD
	BSR.S	OpenRequester
	ERROR	8$
	BSR	MotorOff
	BSR	WaitRequester
	ERROR	8$
	BSR	CloseRequester
	CLC
	RTS
8$:	SETF	AbortFlg
	STC
	RTS

* REQUESTER SUBRS

OpenRequester:
	LEA	REQUEST,A0	;OPEN SPECIAL REQUESTER
	MOVE.L	Window,A1
	CALLSYS	Request,IntuitionBase
	IFNZL	D0,1$		;good open
	STC			;oops...no requester
1$:	RTS

WaitRequester:
	ZAP	D0
	MOVE.B	D0,CancelFlg
	MOVE.B	D0,ProcFlg
	MOVE.B	D0,AbortFlg
1$:	BSR	WaitForMsg
	AND.W	#$60,D0			;CHECK FOR GADGET FLAGS
	BEQ.S	1$			;IGNORE IT, WHATEVER IT WAS
	MOVE.L	GAD_PTR,A0
	IFZW	gg_GadgetID(A0),1$	;ignore if not requester gadget
	MOVE.L	gg_UserData(A0),A0
	IFZL	A0,1$			;gadget requires no action
	JMP	(A0)			;process the gadget hit
;;	RTS

CloseRequester:
	MOVE.L	Window,A1
	LEA	REQUEST,A0
	CALLSYS	EndRequest,IntuitionBase ;clear the requester
	RTS

WaitForMsg:
	PUSH	A2/A5
	MOVEA.L	Window,A5
3$:	MOVE.L	wd_UserPort(A5),A0
	CALLSYS	GetMsg,SysBase		;msg ready?
	IFNZL	D0,1$			;yes
	MOVE.L	wd_UserPort(A5),A0
	CALLSYS	WaitPort		;wait for something to happen
	BRA.S	3$			;it did

1$:	MOVE.L	D0,A1
	MOVE.L	im_Class(A1),MSG_CLASS	;SAVE MSG CLASS
	MOVE.W	im_Code(A1),MSG_CODE
	MOVE.L	im_IAddress(A1),GAD_PTR	;SAVE PTR TO POSSIBLE GADGET
	CALLSYS	ReplyMsg		;LET INTUITION KNOW WE HAVE IT
	MOVE.L	MSG_CLASS,D0
	CMP.L	#REFRESHWINDOW,D0	;REFRESH REQUEST?
	BNE.S	2$
	BSR	REFRESH			;keep intuition happy
	BRA.S	3$

2$:	IFNEIL	D0,#REQSET,9$		;not REQSET
	MOVE.L	A5,A1			;window
	LEA	REQUEST,A2
	MOVE.L	REQGAD,A0		;this is gadget to activate
	IFNEIW	gg_GadgetType(A0),#$1004,9$
	CALLSYS	ActivateGadget,IntuitionBase
9$:	POP	A2/A5
	RTS

* Called to display progess.  Offset from 0,0 in D0.  Percent complete
* given in pixels in D1 (active).
* Called at DISP_RBAR for requester in A0 or DISP_WBAR for window in A0.

DISP_WBAR:
	MOVE.L	wd_RPort(A0),A0
	BRA.S	DBar

DISP_RBAR:
	MOVE.L	ReqLayer(A0),A0		;point to layer
	MOVE.L	LayerRP(A0),A0		;point to rasterport
DBar:	PUSH	D2-D5/A2/A6
	MOVE.L	A0,A2			;raster port
	ZAP	D2
	ZAP	D3
	MOVE.W	D0,D2			;save x
	ADDQ.W	#2,D2			;offset x by 2 to leave a margin
	SWAP	D0
	MOVE.W	D0,D3			;save y
	INCW	D3			;offset y by 1 to leave margin
	MOVE.L	D1,D4			;save active pixel count
	MOVEQ	#BLACK,D0		;set A pen to black
	MOVE.L	A2,A1
	CALLSYS	SetAPen,GraphicsBase
	MOVEQ	#8-1,D5			;8 rows
1$:	MOVE.L	D2,D0			;x offset into rport
	ADD.W	D4,D0			;width offset
	MOVE.L	D3,D1			;y offset into rport
	MOVE.L	A2,A1			;rport
	CALLSYS	WritePixel		;do it
	INCW	D3			;increment y offset
	DBF	D5,1$			;loop for all rows
	POP	D2-D5/A2/A6
	RTS

* Operator error alert.

BEEP..:
	JMP	AudioBeep
;	IFZB	BeepFlg,1$
;	BSR	AudioBeep
;1$:	IFZB	FlashFlg,9$
;	ZAPA	A0
;	CALLSYS	DisplayBeep,IntuitionBase
;9$:	RTS

* Moves a large string from one place in memory to another.  Source in
* A0, destination in A1, and byte count in D0.  Handles any size block.
* Enter at MoveText for the general case, MoveRight when the destination
* is higher than the source (moves from high address backwards) or MoveLeft
* when the destination is lower than the source.

MoveText:
	IFLEL	A1,A0,MoveLeft	;DEST LOWER, COPY FORWARD
	ADDA.L	D0,A0		;ELSE COPY BACKWARD
	ADDA.L	D0,A1

MoveRight:
	IFZL	D0,4$		;just in case...
	MOVEQ	#3,D1
	AND.L	D0,D1
	BRA.S	2$
1$:	MOVE.B	-(A0),-(A1)
2$:	DBF	D1,1$
	ASR.L	#2,D0
	BEQ.S	4$
3$:	MOVE.B	-(A0),-(A1)
	MOVE.B	-(A0),-(A1)
	MOVE.B	-(A0),-(A1)
	MOVE.B	-(A0),-(A1)
	SUBQ.L	#1,D0
	BGT.S	3$
4$:	RTS


MoveLeft:
	IFZL	D0,9$
	MOVEQ	#3,D1
	AND.L	D0,D1
	BRA.S	5$
6$:	MOVE.B	(A0)+,(A1)+
5$:	DBF	D1,6$
	ASR.L	#2,D0
	BEQ.S	9$
7$:	MOVE.B	(A0)+,(A1)+
	MOVE.B	(A0)+,(A1)+
	MOVE.B	(A0)+,(A1)+
	MOVE.B	(A0)+,(A1)+
	SUBQ.L	#1,D0
	BGT.S	7$
9$:	RTS

* Draws a series of boxes with borders and colors defined in table
* pointed to by A0.  Table ends in 0 entry.

DRAW_BOXES:
	PUSH	D2/A4
	MOVE.L	A0,A4
1$:	MOVE.L	(A4)+,D0	;GET LE, TE
	BEQ.S	9$		;DONE
	MOVE.L	(A4)+,D1	;GET WD, HT
	MOVE.W	(A4)+,D2	;PEN COLORS
	PUSH	D0-D2		;SAVE PARAMS
	SWAB	D2
	BSR	CLEAR_BOX	;CLEAR IT TO BORDER COLOR
	POP	D0-D2
	ADD.L	#$20001,D0	;MOVE OVER BY 2, DOWN BY 1
	SUB.L	#$40002,D1	;REDUCE WIDTH BY 4, HT BY 2
	BSR.S	CLEAR_BOX	;CLEAR INSIDES TO PROPER COLOR
	BRA.S	1$
9$:	POP	D2/A4
	RTS

* Clears the INSIDE of a box to the defined color.  Ptr to box data in A0.

ClearBox:
	MOVE.L	(A0)+,D0	;GET LE, TE
	MOVE.L	(A0)+,D1	;GET WD, HT
	MOVE.W	(A0)+,D2	;PEN COLORS
	ADD.L	#$20001,D0	;MOVE OVER BY 2, DOWN BY 1
	SUB.L	#$40002,D1	;REDUCE WIDTH BY 4, HT BY 2
;	BSR.S	CLEAR_BOX	;CLEAR INSIDES TO PROPER COLOR
;	RTS

* CLEARS A BOX TO THE COLOR IN D2.  LEFT, TOP IN D0, WIDTH, HT IN D1.

CLEAR_BOX:
	MOVE.L	Window,A0
	MOVE.L	wd_RPort(A0),A0
	LEA	IDATA,A1	;IMAGE DATA BLOCK
	MOVE.L	D0,(A1)		;L, T
	MOVE.L	D1,4(A1)	;W, H
	MOVE.B	D2,IPEN(A1)	;BORDER PEN
	ZAP	D0
	ZAP	D1
	CALLSYS	DrawImage,IntuitionBase
	RTS

* Colors the box of gadget in A0 to color in D0.

GAD_COLOR:
	MOVE.L	D2,-(A7)
	MOVE.B	D0,D2
	MOVE.L	gg_LeftEdge(A0),D0
	MOVE.L	gg_Width(A0),D1
	BSR.S	CLEAR_BOX
	MOVE.L	(A7)+,D2
	RTS

* CLEARS THE WHOLE WINDOW

CLRWIN:	MOVE.L	Window,A0
	LEA	WINI,A1
	MOVE.W	wd_Width(A0),D0
	SUB.W	#4,D0
	MOVE.W	D0,4(A1)
	MOVE.W	wd_Height(A0),D0
	SUB.W	#11,D0
	MOVE.W	D0,6(A1)
	ZAP	D0
	ZAP	D1
	MOVE.L	wd_RPort(A0),A0		;GET RASTPORT
	CALLSYS	DrawImage,IntuitionBase
	RTS

* Adds gadget list in A0 to window and refreshs gadgets.

AddGadgets: 
	PUSH	A2-A3
	MOVE.L	A0,A3
	MOVE.L	A3,GadList		;save, just in case
	IFZL	A3,1$
	MOVE.L	A3,A1
	MOVE.L	Window,A0
	MOVEQ	#-1,D0
	MOVEQ	#-1,D1
	ZAPA	A2
	CALLSYS	AddGList,IntuitionBase
	MOVE.L	A3,A0
	MOVE.L	Window,A1
	ZAPA	A2
	MOVEQ	#-1,D0
	CALLSYS	RefreshGList
1$:	POP	A2-A3
	RTS

* Removes gadgets in list in A0 from window.

RemoveGadgets: 
	IFZL	A0,1$
	MOVE.L	A0,A1
	MOVE.L	Window,A0
	MOVEQ	#-1,D0
	CALLSYS	RemoveGList,IntuitionBase
1$:	RTS

REFRESH:
	PUSH	A2/A6
	MOVE.L	Window,A0
	CALLSYS	BeginRefresh,IntuitionBase	;KEEP INTUITION HAPPY
	MOVE.L	RefreshRoutine,A6		;specific refresh routine
	IFZL	A6,1$				;oops...no routine
	JSR	(A6)				;do it
1$:	MOVE.L	Window,A0
	MOVEQ	#TRUE,D0			;COMPLETE
	CALLSYS	EndRefresh,IntuitionBase
;	MOVE.L	Window,A1
;	ZAPA	A2
;	MOVE.L	wd_FirstGadget(A1),A0
;	CALLSYS	RefreshGadgets
	POP	A2/A6
	RTS

RefreshAllGads:
	PUSH	A2
	MOVE.L	Window,A1
	ZAPA	A2
	MOVE.L	wd_FirstGadget(A1),A0
	CALLSYS	RefreshGadgets,IntuitionBase
	POP	A2
	RTS

* CLOSES A WINDOW SAFELY.  POINTER TO WINDOW IN A0

CloseSafely:
	PUSH	A3-A4
	MOVE.L	A0,A4
	CALLSYS	Forbid,SysBase		;Don't let Intuition run now
	MOVE.L	wd_UserPort(A4),A3
	ADD.L	#MP_MSGLIST,A3		;point to head cell in list
1$:	MOVE.L	(A3),A3
	IFZL	A3,3$			;NO MORE MSGS IN USER PORT
	IFNEL	im_IDCMPWindow(A3),A4,1$
	MOVE.L	A3,A1			;ELSE GET RID OF THIS MSG
	CALLSYS	Remove,SysBase	
	MOVE.L	A3,A1
	CALLSYS	ReplyMsg
	BRA.S	1$			;LOOP
3$:	MOVE.L	A4,A0
;;	CLR.L	wd_UserPort(A0)		;ZAP USER PORT FOR INTUITION
	ZAP	D0
	CALLSYS	ModifyIDCMP,IntuitionBase ;no more msgs
	CALLSYS	Permit,SysBase		;OKAY FOR TASK SWAP NOW
	MOVE.L	A4,A0
	CALLSYS	CloseWindow,IntuitionBase
	POP	A3-A4
	RTS

* Move source string A0 into destination string A1.  At '@' insert VolNameBuf.
* At '%' insert TMP2, at '^' insert TMP3.

MoveInsertDevice:
	IFZL	A0,9$,L			;handle no source string gracefully
1$:	MOVE.B	(A0)+,D0		;get byte of source string
	BEQ	9$			;end of source
	CMP.B	#'@',D0			;insert device from TMP.?
	BEQ.S	2$			;yes
	CMP.B	#'%',D0			;insert TMP2.?
	BEQ.S	3$			;yes
	CMP.B	#'^',D0			;insert TMP3.?
	BEQ.S	6$			;yes
	CMP.B	#'$',D0			;insert TMP.?
	BEQ.S	7$
	MOVE.B	D0,(A1)+		;else just save byte in dest string
	BRA.S	1$
2$:	PUSH	A0			;save ptr to where we are in msg
;;	MOVE.B	#' ',(A1)+
	LEA	VolNameBuf.,A0
	BSR	MoveDevice
;;	DECL	A1			;take out space just added
	POP	A0			;restore ptr
	BRA.S	1$
6$:	PUSH	A0
	LEA	TMP3.,A0
	BRA.S	4$
3$:	PUSH	A0
	LEA	TMP2.,A0
	BRA.S	4$
7$:	PUSH	A0
	LEA	TMP.,A0
4$:	MOVE.B	(A0)+,D0
	BEQ.S	5$			;end of "n" string
	MOVE.B	D0,(A1)+		;else store char
	BRA.S	4$			;loop
5$:	POP	A0
	BRA	1$
9$:	CLR.B	(A1)+			;terminate dest string
	RTS


* Moves device name with unit number (pointed to by A0) into text area
* pointed to by A1.  Zaps trailing colon, if any.  Appends trailing blank.

MoveDevice:
1$:	MOVE.B	(A0)+,D1		;get a byte
	BEQ.S	2$
	MOVE.B	D1,(A1)+		;else store byte
	BRA.S	1$
2$:	CMP.B	#':',-1(A1)		;trailing colon?
	BNE.S	3$			;no
	DECL	A1			;backup and overwrite colon
3$:;;	MOVE.B	#' ',(A1)+		;trailing blank
	CLR.B	(A1)			;terminate string
	RTS

* Loads up to 5 text string pointers into requester structure.  Strings
* are pushed onto stack in normal order (string 1...string 5).
* Loads volume name into any string at position marked by '@'.  Inserts
* TMP. at position marked by '$'.  Inserts TMP2. at position marked by '%', 
* and TMP3. at position marked by '^'.
* If no source string is provided (pointer=0) dest is set to 0.
* Returns ptr to text structure in A0.

BldWarningMsg:
	MOVE.L	(A7)+,A6		;get return address
	MOVE.L	(A7)+,A0		;string 5
	LEA	RQTxtBf4.,A1
	BSR	MoveInsertDevice
	MOVE.L	(A7)+,A0		;string 4
	LEA	RQTxtBf3.,A1
	BSR	MoveInsertDevice
	MOVE.L	(A7)+,A0		;string 3
	LEA	RQTxtBf2.,A1
	BSR	MoveInsertDevice
	MOVE.L	(A7)+,A0		;string 2
	LEA	RQTxtBf1.,A1
	BSR	MoveInsertDevice
	MOVE.L	(A7)+,A0		;string 1
	LEA	RQTxtBuf.,A1
	BSR	MoveInsertDevice
	MOVEQ	#16,D0
	MOVEQ	#12,D1
	BSR.S	SetSpacing
	LEA	RQText,A0		;returns ptr to text
	JMP	(A6)			;simulate RTS

* Called with TopEdge in D0 and y-increment in D1.  Resets RQText spacing
* for various types of msgs.

SetSpacing:
	PUSH	D2
	LEA	RQText,A0
	MOVEQ	#5-1,D2
1$:	MOVE.W	D0,it_TopEdge(A0)
	ADD.W	D1,D0
	MOVE.L	it_NextText(A0),A0
	DBF	D2,1$
	POP	D2
	RTS

* Loads TMP2. and TMP3. from longword values in D0 and D1.  If values are 0,
* sets the temp strings to null.  Leading zeros are stripped.

LoadTmps:
	PUSH	D1			;save string 2 value
	LEA	TMP2.,A0
	CLR.B	(A0)			;start with null string
	TST.L	D0
	BMI.S	1$			;no parameter
	STR.	L,D0,A0,#32,#5		;else build string
	STRIP_LB. TMP2.
1$:	POP	D0			;restore string 2 value
	LEA	TMP3.,A0
	CLR.B	(A0)			;start with null string
	TST.L	D0
	BMI.S	9$			;no parameter
	STR.	L,D0,A0,#32,#5		;else build string
	STRIP_LB. TMP3.
9$:	RTS

* Divides 32-bit value in D0 by 32-bit value in D1.  Returns quotient in D0,
* remainder in D1.  This is slow and very crude, but works well for small 
* numbers.

LongDivide:
	PUSH	D2
	ZAP	D2
	IFZL	D1,9$			;zero divide is illegal
1$:	INCL	D2
	SUB.L	D1,D0			;crude but effective 32 bit divide
	BHI.S	1$			;this could be wrong branch...
	DECL	D2			;don't count last one
	ADD.L	D1,D0			;restore remainder
9$:	MOVE.L	D2,D1
	EXG	D1,D0			;quotient to D0, remainder to D1
	POP	D2
	RTS

GadList	DC.L	0

	END
