;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
***************************************************************** 
*								* 
*		REORG.ASM					* 
*								* 
*	Quarterback Tools Disk Reorganization Function		*
*	Copyright (c) 1990 Central Coast Software		*
*								*
*****************************************************************

	INCLUDE "vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"VD0:BOXES.ASM"

	XDEF	Function31
	XDEF	Function32
	XDEF	RexFrag
	XDEF	AdjustBlkCnt
	XDEF	CheckBadKey
	XDEF	AdvanceKey

	XDEF	ModeWB,ModeCLI
	XDEF	LowestCtrlKey,LowestFreeKey
	XDEF	ReorgFlg

* Externals in TOOLS.ASM

	XREF	MLoop,MLoopErr

	XREF	SysBase,DosBase,IntuitionBase,GraphicsBase
	XREF	TMP.,TMP2.
	XREF	VolNameBuf.
	XREF	FunGadList
	XREF	WBModeFlg
	XREF	ScreenMode

* Externals in VTEST.ASM

	XREF	ProcessMsg
	XREF	CountFragments
	XREF	RefreshBitmap,UpdateBitmap
	XREF	CalcBMMode

	XREF	CancelFlg
	XREF	AbortFlg
	XREF	SkipFlg
	XREF	YesFlg
	XREF	ProcFlg
	XREF	InsertVolName
	XREF	FragmentCount

* Externals in FILES.ASM

	XREF	CalcHash
	XREF	CheckBadBlocks
	XREF	GetBadStatus
	XREF	OpenFile,DeleteFile

	XREF	BadBlkFlg
	XREF	BadBlksFileKey
	XREF	BadBlks.

* Externals in RESTORE.ASM

	XREF	AllocateDirBlock,FreeDirBlocks
	XREF	SearchDummyTree,SearchDirTree
	XREF	UnLinkDummy,LinkChild
	XREF	UpdateCounts,DispCntTxt
	XREF	UpdateDBPtr
	XREF	MoveFileName

	XREF	CurDBPtr
	XREF	CurDBCnt
	XREF	BaseDirBlock,CurDBBase
	XREF	DirTree,DummyTree
	XREF	ActFileCnt
	XREF	ActDirCnt
	XREF	FileCntMode

* Externals in GRAPHICS.ASM

	XREF	InitBar
	XREF	UpdateStatusBar
	XREF	RefreshBar

* Externals in BITMAP.ASM

	XREF	BldFreeKeyList
	XREF	GetBusyStatus
	XREF	MarkKeyFree,MarkKeyBusy
	XREF	ReadBitmap,WriteBitmap
	XREF	InitBitmap,WriteBMExt
	XREF	RepositionBitmap
	XREF	UpdateBitmapKeys
	XREF	CountFreeKeys
	XREF	AllocateAltBitmap
	XREF	FreeBMBuffers

	XREF	FreeKeys

* Externals in FUN.ASM

	XREF	CLRWIN
	XREF	RefreshAllGads
	XREF	MoveChars

* Externals in Device.asm

	XREF	CheckVolumeStatus,CalcVolParams
	XREF	OpenDriver,CloseDriver
	XREF	InhibitDos,EnableDos
	XREF	CheckProtection
	XREF	UpdateAssigns
	XREF	FlushDosCache

	XREF	NumSoftErrors
	XREF	UnitNumber
	XREF	DiskState
	XREF	NumBlocks
	XREF	NumBlocksUsed
	XREF	BytesPerBlock
	XREF	DiskType
	XREF	VolumeNode
	XREF	InUse	
	XREF	MaxBytes
	XREF	MaxBlocks
	XREF	FreeBytes
	XREF	FreeBlocks
	XREF	VolumeOffset
	XREF	LowCyl
	XREF	HighCyl
	XREF	MaxCylinders
	XREF	Surfaces
	XREF	MaxTracks
	XREF	BlockSize,BlockSizeL
	XREF	Reserved,PreAlloc
	XREF	BlocksPerTrack
	XREF	NumBuffers
	XREF	BufMemType
	XREF	MaxTransfer
	XREF	PercentFull
	XREF	DosType
	XREF	DeviceNameBuf.
	XREF	DriverNameBuf.
	XREF	VolNamDevFlg
	XREF	VolValidFlg
	XREF	NoDiskFlg
	XREF	NotDosFlg
	XREF	NotValidFlg
	XREF	WrtProtFlg
	XREF	VolStatusMsg
	XREF	BlkSizeOffset
	XREF	HashTblSize
	XREF	RootBlkNbr
	XREF	HighestKey,LowestKey
	XREF	BitmapBlocks,BMExtBlocks
	XREF	MaxKeys
	XREF	FFSFlg

* Externals in Gadgets.asm

	XREF	RightPGad,CanGad,PauseGad
	XREF	ReqProceed,ReqYes,ReqProc1,ReqCancel
	XREF	WrnBak
	XREF	ReorgOK,ReorgErr
	XREF	ScanRdErr,ScanBlkErr
	XREF	CantReorg,MaybeBad

* Externals in WIND.asm

	XREF	RefreshRoutine

* Externals in DISK.ASM

	XREF	ReadBlock,WriteBlock
	XREF	MotorOff

* Externals in BUFFERS.ASM

;;	XREF	ReadControl,ReadBitmap,ReadData
;;	XREF	GetCacheBuffer,PutCacheBuffer
	XREF	ZapBlock,ZapAnyBlock
	XREF	AllocateBuffer
	XREF	ReadCache,WriteCache,FlushCache
	XREF	AllocateCache,FreeCacheBuffers
	XREF	CalcCkSum
	XREF	AllocateBlock,FreeBlock
	XREF	BlockBuffer
	XREF	AllocateExtBuffer,FreeExtBuffer
	XREF	LinkCacheKey,UnLinkCache
	XREF	AllocImageBuf,FreeImageBuf
	XREF	FreeBadBlkBuf
	XREF	FreeRemapBuffer

	XREF	BlockBuffer

* Externals in CAT.ASM

	XREF	PushWindow,PopWindow,NextWindow

* Externals in MESSAGES.ASM

;	XREF	Reorg.
	XREF	Defrag.,Analyze.
	XREF	PrepReorg.
;	XREF	Cleanup.
	XREF	NotImp.
	XREF	Fatal1.
	XREF	FragFiles4.
	XREF	ProcFile.
;;	XREF	ReorgErr.
	XREF	DirOptErr.
	XREF	KeyConf.
	XREF	NoOwner.
	XREF	OptErr.
	XREF	RexFragCnt.

* Externals in REPAIR.ASM

	XREF	VerifyCtrlBlock
	XREF	ErrorCount

* Externals in MOVE.ASM

	XREF	RepositionFiles
	XREF	CheckRemapSpace
	XREF	InitRemapping
	XREF	CloseRemapping

	XREF	RemapBlocks

* Externals in REPORT.ASM

	XREF	DisplayFile,UpdateFileName

	XREF	SavedSMode

* Externals in AREXX.ASM

	XREF	ArexxFlg
	XREF	ResultBuf

* Externals in FUN.ASM

	XREF	MoveInsertDevice

* Externals in FORMAT.ASM

	XREF	Form.

;;IconMagic	EQU	$E310
;;FFSFlg		EQU	DosType+3

* Check and report volume and file fragmentation.

Function31:
	ZAP	D0		;mode=check and report
	BRA.S	Fun31Com

* Reorganize volume space and defragment files.

Function32:
	MOVEQ	#1,D0		;full reorganization
Fun31Com:
	MOVE.B	D0,ReorgFlg	;show which mode
	BSR	AllocateCache
	MOVE.B	ScreenMode,SavedSMode
	ZAP	D0
	MOVE.B	D0,CancelFlg
	MOVE.L	D0,FragFileCnt
	MOVE.L	D0,ActFileCnt
	MOVE.L	D0,ActDirCnt
	MOVE.W	D0,ErrorCount
	MOVE.B	D0,FileCntMode		;active only
	BSR	FreeDirBlocks		;free any previous dir blocks
	BSR	CheckVolumeStatus	;get device params
	ERROR	MLoopErr		;problems
	BSR	CalcVolParams		;calc volume params
	IFZB	ReorgFlg,3$		;read only...
	IFNZB	ArexxFlg,4$
	BSR	WarnBackup
	IFZB	YesFlg,MLoop,L		;op wants to bail out
4$:	BSR	CheckProtection		;make sure we can write
	ERROR	MLoopErr		;op canceled
3$:	BSR	InhibitDos		;lock out ADOS
	BSR	ReadBitmap		;forces RootBlock read
	ERROR	Fun31Error		;problems
	BSR	AllocateExtBuffer	;enough to hold one extent
	ERROR	MLoopErr		;didn't get any!!!
	BSR	AllocateAltBitmap	;needed for remapping control
	ERROR	MLoopErr
	BSR	AllocImageBuf
	ERROR	MLoopErr
	MOVE.L	MaxKeys,D0
	BSR	InitBar			;initialize the bar counts
	LEA	PauseGad,A0
	LEA	Fun31Refresh,A1
	ZAP	D0
	BSR	PushWindow		;remove gadgets and clear window
	BSR	DeleteSavedFormat	;get rid of saved format if real reorg
	BSR	CountFreeKeys
	ERROR	Fun31Error
	BSR	CheckRemapSpace
	ERROR	Fun31Error
	BSR	AllocateBlock
	ERROR	Fun31Error
	BSR	InitDirLists		;initialize directory tree
	ERROR	Fun31Error		;oops
	BSR	ScanForFiles		;find and count fragmented files
	IFNZB	CancelFlg,Fun31Exit,L
	IFNZB	ArexxFlg,5$,L
	IFZB	ReorgFlg,2$		;reporting only
	IFLTIB	SavedSMode,#2,ReorgVolume,L ;don't bother the operator
2$:	MOVE.	FragFiles4.,TMP.
	STR.	L,FragmentCount,TMP2.,#32,#6
	STRIP_LB. TMP2.
	APPEND.	TMP2.,TMP.
	MOVE.L	FragFileCnt,D0
	MOVE.L	ActFileCnt,D1
	BldWarn	D0,D1,FragFiles1.,FragFiles2.,FragFiles3.,TMP.
	DispReq	A0,ReqProc1
	IFNZB	CancelFlg,Fun31Exit,L
5$:	IFZB	ReorgFlg,Fun31Exit,L

**************************************************************************************
* Note: the ideal "defragged" file has the filehdr in the control area, close to
* the parent dir, and has the exthdrs, if any, in order immediately below the data
* blocks.  Filehdrs and userdirs are grouped immediately above the root, along with
* icon file data blocks (if WB mode).  Free space is concentrated just above the 
* control area.

ReorgVolume:
	MOVE.L	RemapBlocks,D0
	ADD.L	D0,D0
	BSR	InitBar			;initialize the bar counts
	ZAPA	A0
	LEA	InitializeRefresh,A1
	ZAP	D0
	BSR	NextWindow		;remove gadgets and clear window
	BSR	CalcLowestKeys		;figure out where things get stored	
	BSR	CalcBMMode
	BSR	AssignNewKeys		;assign new keys to everything

	BSR	InitRemapping
	ERROR	Fun31Error
	LEA	PauseGad,A0
	LEA	DefragRefresh,A1
	ZAP	D0
	BSR	NextWindow		;remove gadgets and clear window
	BSR	RepositionBitmap	;get bitmap blocks relocated
	ERROR	8$
	BSR	RepositionFiles		;move ctrl blocks into position
	ERROR	8$
	BSR	CloseRemapping

	BSR	UpdateBitmapKeys	;reassign bitmap keys
	ERROR	8$
;;;	BSR	CloseRemapping
	BSR	WriteBitmap		;write out new bitmap
	BSR	WriteCache		;write out any pending buffers
;;;	BSR	UpdateAssigns
;;;	BSR	FlushDosCache
	DispReq	ReorgOK,ReqProceed
	BRA.S	Fun31Exit

8$:	IFNZB	ArexxFlg,Fun31Error
	DispReq	ReorgErr,ReqCancel
Fun31Error:
	BSR	CloseRemapping
	BSR.S	CleanUp
	BRA	MLoopErr

Fun31Exit:
	BSR.S	CleanUp
	BRA	MLoop

CleanUp:
	BSR	EnableDos
	BSR	FreeBadBlkBuf
	BSR	FreeDirBlocks
	BSR	FreeExtBuffer
	BSR	FreeBlock
	BSR	FreeImageBuf
	BSR	FreeRemapBuffer
	BSR	FreeBMBuffers
	BSR	PopWindow		;restore window
	CLR.B	ReorgFlg
	RTS

Fun31Refresh:
	BSR	CLRWIN
	LEA	Analyze.,A0
	BSR	InsertVolName		;insert volume
	DispCent #320,#BSTitle_TE,A0,WHITE
	BSR	RefreshBar
	MOVE.W	#SFC_LE,D0
	MOVE.W	#SFC_TE,D1
	BSR	DispCntTxt
	LEA	PauseGad,A0
	GadColor A0,ORANGE
	LEA	CanGad,A0
	GadColor A0,BLACK
	BSR	UpdateCounts
	BSR	RefreshAllGads
	RTS

DefragRefresh:
	BSR	CLRWIN
	LEA	Defrag.,A0
	BSR	InsertVolName		;insert volume
	DispCent #320,#Bitmap_TE-14,A0,WHITE
	BSR	CountFragments
	BSR	RefreshBitmap
	LEA	PauseGad,A0
	GadColor A0,ORANGE
	LEA	CanGad,A0
	GadColor A0,BLACK
	BSR	RefreshAllGads
	RTS

InitializeRefresh:
	BSR	CLRWIN
	LEA	PrepReorg.,A0
	BSR	InsertVolName		;insert volume
	DispCent #320,#BSTitle_TE,A0,WHITE
	BSR	RefreshBar
	BSR	RefreshAllGads
	RTS

WarnBackup:
	DispReq	WrnBak,ReqYes
	RTS

RexFrag:
	STR.	L,FragFileCnt,TMP2.,#32,#6
	STRIP_LB. TMP2.
	LEA	RexFragCnt.,A0
	LEA	ResultBuf,A1
	BSR	MoveInsertDevice
	RTS


* Scans volume looking for active files and directories.

ScanForFiles:
	PUSH	D2-D3
	BSR	CheckBadBlocks		;find out if there is a bad block file
	IFZB	ReorgFlg,11$		;search only
	IFZB	BadBlkFlg,11$
	IFNZB	ArexxFlg,10$,L
	DispReq	CantReorg,ReqCancel
	BRA	10$
11$:	BSR	FreeCacheBuffers
	MOVE.L	LowestKey,D2
	MOVE.L	HighestKey,D3
;;	SUB.L	PreAlloc,D3

1$:	MOVE.L	D2,D0
	IFZB	BadBlkFlg,7$		;no bad blocks...don't check
	BSR	GetBadStatus		;known bad block?
	BEQ.S	3$			;yes...skip it
	MOVE.L	D2,D0			;else check it
7$:	MOVE.L	BlockBuffer,A0
	BSR	ReadBlock
	NOERROR	6$
	IFNZB	ArexxFlg,10$,L
	DispReq	ScanRdErr,ReqCancel
	BRA	10$

6$:	MOVE.L	D2,D0
	BSR	GetBusyStatus		;is this filehdr marked busy (not deleted)?
	BNE.S	3$			;block free...ignore it
	MOVE.L	BlockBuffer,A0
	MOVE.L	D2,D0
	MOVE.L	fh_Type(A0),D1
	IFNEIL	D1,#T_SHORT,2$		;not root, user dir, or filehdr
	BSR	ProcessControlBlock
	ERROR	8$
	BSR	UpdateCounts
	BRA.S	3$

2$:	IFNEIL	D1,#T_LIST,3$		;not a filehdr extension
	MOVE.L	A0,A1
	ADD.L	BlockSize,A1
	IFNEIL	fh_SecType(A1),#ST_FILE,3$ ;not a filehdr extension
	BSR	ProcessExtHdr
	ERROR	8$
3$:	BSR	UpdateStatusBar		;show something happening
	BSR	ProcessMsg		;check for op action
	IFNZB	AbortFlg,9$,L		;check for operator abort
	IFNZB	CancelFlg,9$,L		;op has canceled
	INCL	D2
;;	IFLEL	D2,HighestKey,1$,L 	;loop for all blocks of volume
	IFLEL	D2,D3,1$,L 		;loop for all blocks of volume
	BSR	UpdateBlockCounts	;combine ext blk cnts and Frag flags
	BSR	CountFragments
	IFNZW	ErrorCount,8$		;serious errors found in scan
	IFNZL	ExtList,4$		;ExtList has not been emptied...
	MOVE.L	DummyTree,A0		;ptr to root of dummy
	IFZL	A0,9$
	IFZL	rl_Child(A0),9$		;nothing linked into dummy list
4$:	IFNZB	ArexxFlg,9$
	DispReq	MaybeBad,ReqProc1
	BRA.S	9$

8$:	IFNZB	ArexxFlg,10$
	DispReq	ScanBlkErr,ReqCancel
10$:	SETF	CancelFlg
9$:	BSR	AllocateCache
	POP	D2-D3
	RTS

* Processes file header or user dir block.  Key in D0 and ptr to block buffer
* in A0.

ProcessControlBlock:
	PUSH	D2-D5/A2-A4
	MOVE.L	A0,A3			;ptr to filehdr/userdir block
	MOVE.L	D0,D2			;control block key
	IFEQL	RootBlkNbr,D2,3$
	IFNEL	fh_Ownkey(A0),D2,8$	;not valid control block
	IFNZL	fh_Skip1(A0),8$		;better be 0
3$:	BSR	CalcCkSum		;valid ADOS control block?
	IFNZL	D0,8$			;bad checksum...ignore this block
	MOVE.L	A3,A0
	BSR	VerifyCtrlBlock		;make sure it passes basic integrity tests
	ERROR	8$
	IFNEIL	D0,#ST_USERDIR,1$
	IFNZL	db_Skip(A3),8$
	BRA	ProcessUserDir

1$:	IFNEIL	D0,#ST_FILE,2$		;not file hdr
	IFGTL	fh_BlkCnt(A3),HashTblSize,8$
	BRA.S	ProcessFileHdr

2$:	IFNEL	RootBlkNbr,D2,8$
	IFNZL	rb_Skip1(A3),8$
	INCL	ActDirCnt		;count the root
	BRA.S	9$
8$:	IFNZB	FFSFlg,9$		;ignore if FFS...
***	DispErr	Err1.			;**************
	SETF	CancelFlg
	STC
9$:	POP	D2-D5/A2-A4
	RTS

ProcessFileHdr:
	INCL	ActFileCnt
	MOVE.L	A3,A0
	ADD.L	BlockSize,A0
	MOVE.L	fh_Parent(A0),D3 	;get parent dir key
	MOVE.L	fh_Ext(A0),D4		;get key of extension, if any
	IFEQL	D2,D3,7$		;filehdr claims itself as parent
	MOVE.L	D3,D0
	BSR	SearchDirTree		;parent dir already defined in dirtree?
	ERROR	5$			;no...not previously found
	MOVE.L	D0,A2
	BTST	#rlt_Dir,rl_Type(A2)	;is it really a dir?
	BNE.S	2$			;yes

***************************************************************************
* if we get here, we have a new filehdr which claims to have a parent dir
* which is already in the dirtree as a FILE.  The error is probably in the 
* new filehdr, since we only enter a file into the dirtree if it is real.
***************************************************************************

7$:	MOVEQ	#-2,D3			;force an illegal parent key

5$:	MOVE.L	D3,D0
	BSR	SearchDummyTree		;else is parent dir defined in dummy list?
	NOERROR	2$			;yes, use it
	MOVE.L	D3,D0
	BSR	CreateDummyParent	;else add dummy parent dir
	ERROR	8$
2$:	MOVEA.L D0,A2			;ptr to real or dummy parent dir entry
	MOVE.L	D2,D0			;filehdr ownkey
	BSR	SearchDummyTree		;anybody refer to this filehdr as a parent?
	ERROR	6$			;no...all is well
	MOVE.L	D0,A0
	MOVEQ	#-2,D0			;yes...zap the dummy parent's key
	MOVE.L	D0,rl_Ownkey(A0)

6$:	MOVE.L	D2,D0			;file hdr key
	MOVEQ	#$40,D1			;flag=filehdr
	BSR	BuildFileEntry		;build new file entry
	ERROR	8$
	MOVE.L	D0,A4
;;	MOVE.L	fh_FirstBlk(A3),rl_First(A4) ;key of first data block
	MOVE.L	A3,A0			;block ptr
	MOVE.L	A4,A1			;entry ptr
	BSR	CalcHashEntry		;calc parent's hash table offset
	MOVE.L	A3,A0
	BSR	CountBlocks		;get hdr data block count
	BCC.S	1$			;not fragmented
	BSET	#rlf_HFrag,rl_Flags(A4)	;hdr is fragmented
1$:	MOVE.W	D0,rl_HBlks(A4)		;filehdr has this many
	IFNEL	BadBlksFileKey,D2,11$	;not Bad.Blocks file
	BSET	#rlf_DontMove,rl_Flags(A4) ;dont reorg this file
11$:	MOVE.L	A3,A0			;block ptr
	MOVE.L	A4,A1			;entry ptr
	BSR	CheckForIcon		;find out if this is an icon file
	MOVE.L	D4,D0
	BEQ.S	4$			;no extensions for this file
	BSR	SearchExtList		;search ExtList Ownkey field
	NOERROR	3$			;it is in ExtList
	MOVE.L	D4,D0
	MOVE.L	A4,A0			;ptr to parent entry for rl_Parent
	BSR	CreateDummyExt		;make dummy entry and add to ExtList
	ERROR	8$
	BRA.S	4$
3$:	MOVE.L	A0,rl_ExtHdr(A4)	;ptr to ext hdr entry
	BSR	UnLinkExtList		;unlink exthdr from ext list
4$:	MOVE.L	A2,A0			;parent dir entry
	MOVE.L	A4,A1			;new filehdr entry
	BSR	LinkParent		;link filehdr into parent's list
	BRA.S	9$
8$:;***	DispErr	Err2.			;*************************
	SETF	CancelFlg
	STC
9$:	POP	D2-D5/A2-A4
	RTS

* Process user directory block.
* A3=user directory block buffer, D2=userdir key.

ProcessUserDir:
	INCL	ActDirCnt
	MOVE.L	A3,A0
	ADD.L	BlockSize,A0
	MOVE.L	db_Parent(A0),D3	;parent dir blk #
	IFEQL	D2,D3,10$		;dir claims to be its own parent
	MOVE.L	D3,D0
	BSR	SearchDirTree		;is parent dir already in dir list?
	ERROR	2$			;no
	MOVE.L	D0,A2			;ptr to existing dir entry
	BTST	#rlt_Dir,rl_Type(A2)	;really parent dir?
	BNE.S	1$			;yes, parent dir exists...link this dir to it

***********************************************************************
* if we get here, we have a user dir which claims to have a parent dir
* which is already in the dirtree as a FILE.  The error is probably in the 
* userdir, especially if it is deleted, since we can only get a file entry 
* into the dirtree if we found a filehdr block with that Ownkey.
***********************************************************************
10$:	MOVEQ	#-2,D3			;force illeal parent key

2$:	MOVE.L	D3,D0
	BSR	SearchDummyTree		;is parent dir in dummy dir list?
	NOERROR	1$			;yes, use it
	MOVE.L	D3,D0			;else add parent...parent blk #
	BSR.L	CreateDummyParent	;make entry for parent in dummy list
	ERROR	8$
1$:	MOVEA.L D0,A2			;Ptr to parent entry
	MOVE.L	D2,D0			;new dir key
	BSR	SearchDirTree		;is this dir already in dir tree?
	ERROR	7$			;no...it shouldn't be...

* The dir block is already in dir list.
* A current block should NEVER be in the dirtree unless it has actually
* been read and processed.  This is a logical problem in Tools code...

	DispErr	Fatal1.			;current dir block already in dir list
	STC
	BRA.S	9$

7$:	MOVE.L	D2,D0
	BSR	SearchDummyTree		;is new dir in dummy dir list?
	ERROR	3$			;no...need to build new dir entry
	MOVE.L	D0,A4			;yes...save ptr to existing entry
	MOVE.L	A4,A0
	BSR	UnLinkDummy		;unlink dummy entry from dummy list
	BRA.S	4$

* normal case...new userdir not already in dir list...add it now

3$:	MOVE.L	D2,D0			;new dir key
	BSR	BuildDirEntry		;build new dir entry for this user dir
	ERROR	8$
	MOVE.L	D0,A4			;save ptr to new dir entry
4$:	MOVE.L	A3,A0			;block ptr
	MOVE.L	A4,A1			;entry ptr
	BSR	CalcHashEntry		;calc parent's hash table offset
	MOVEA.L A2,A0			;parent dir entry (real or dummy)
	MOVEA.L A4,A1			;ptr to new dir entry
	BSR	LinkParent		;link new dir into parent dir's list
	BRA.S	9$
8$:;***	DispErr	Err3.			;************************
	SETF	CancelFlg
	STC
9$:	POP	D2-D5/A2-A4
	RTS

* Control block is file extension header.  Link to existing filehdr or ext hdr
* or add to ExtList.  Ptr to ext block in A0, key in D0.

ProcessExtHdr:
	PUSH	D2-D4/A2-A4
	MOVE.L	A0,A3			;ptr to filehdr/userdir block
	MOVE.L	D0,D2			;control block key
	IFNEL	fe_Ownkey(A0),D2,6$	;ownkey mismatch
	IFGTL	fe_BlkCnt(A0),HashTblSize,6$
	MOVE.L	A3,A0			;else treat as filehdr
	BSR	CalcCkSum		;valid ADOS control block?
	IFZL	D0,5$			;good checksum...probably a valid exthdr
6$:	IFNZB	FFSFlg,9$,L		;else ignore it if new filesys
	BRA	8$			;else bad exthdr
5$:	MOVE.L	A3,A0
	ADD.L	BlockSize,A0
	IFNEIL	fe_SecType(A0),#-3,8$,L	;sec type error...not ext hdr after all
	MOVE.L	fe_Parent(A0),D3 	;get parent filehdr key
	MOVE.L	fe_Ext(A0),D4		;get key of extension, if any
;;	MOVE.L	fe_BlkPtr1(A0),D5
	IFEQL	D2,D3,8$		;ext claims itself as parent...trouble
	MOVE.L	D2,D0			;ext hdr key
	BSR	SearchExtList		;already a reference to this ext?
	ERROR	1$			;no...create one now
	MOVE.L	A0,A4			;ptr to existing "dummy" ext hdr
	BCLR	#rlf_Dummy,rl_Flags(A4) ;no longer a dummy...found real
	BEQ.S	8$			;oops...was already "real"...problems
	BSR	UnLinkExtList		;get current exthdr out of list
	MOVE.L	rl_Parent(A4),A0	;ptr to parent hdr previously found
	MOVE.L	A4,rl_ExtHdr(A0)	;link this exthdr to parent hdr
	BCLR	#rlf_WaitExt,rl_Flags(A0) ;parent hdr not waiting anymore
	BRA.S	2$			;looks good...use existing entry
1$:	MOVE.L	D2,D0			;ext hdr key
	MOVEQ	#$20,D1
	BSR	BuildFileEntry		;else build an entry for this ext hdr
	ERROR	8$
	MOVE.L	D0,A4
	MOVE.L	D0,A0
	BSR	LinkExtList		;add new entry to ext list
2$:;;	MOVE.L	D5,rl_First(A4) 	;key of first data block
	MOVE.L	A3,A0
	BSR	CountBlocks		;get hdr data block count
	BCC.S	3$			;not fragmented
	BSET	#rlf_HFrag,rl_Flags(A4)	;hdr is fragmented
3$:	MOVE.W	D0,rl_HBlks(A4)		;blocks in this ext
	MOVE.L	D4,D0			;key of next ext, if any
	BEQ.S	9$			;no more exts beyond this one
	BSR	SearchExtList		;else find out if next ext already defined
	NOERROR	4$			;it was in the list
	MOVE.L	D4,D0			;key of next ext
	MOVE.L	A4,A0			;rl_Parent of new dummy points to this ext
	BSR	CreateDummyExt		;create dummy ext and link into ExtList
	ERROR	8$
	BRA.S	9$

4$:	MOVE.L	A0,rl_ExtHdr(A4)	;link next ext hdr entry to this one
	BSR	UnLinkExtList		;unlink next ext hdr from ExtList
	BRA.S	9$
8$:;***	DispErr	Err4.			;***********************
	SETF	CancelFlg
	STC
9$:	POP	D2-D4/A2-A4
	RTS

* Calculates hash table offset for parent dir based on name of hilehdr or userdir.
* Ptr to block in A0, catalog entry in A1.  All this so we don't need to save the
* actual name of the object.

CalcHashEntry:
	PUSH	A2
	LEA	rl_HashEntry(A1),A2
	LEA	fh_FileName(A0),A0
	ADD.L	BlockSize,A0
	BSR	CalcHash		;calculate hash table offset
	MOVE.W	D0,(A2)			;store offset for later
	POP	A2
	RTS

* Examines the file name for file whose block is in A0 and whose filehdr entry is
* in A4.  If name matches ....info, first data block is examined to find out if file
* is an icon.  If so, sets rfl_Icon.

CheckForIcon:
	PUSH	D2/A2-A3
	IFZB	WBModeFlg,9$,L		;not in WB mode
	MOVE.L	A1,A2			;cat entry
	ADD.L	BlockSize,A0
	LEA	fh_FileName(A0),A0
	LEA	TMP.,A1
	BSR	MoveFileName
	RIGHT.	TMP.,#5,TMP2.		;name must end in ".info"
	UCASE.	TMP2.			;force upper case
	IFNE.	TMP2.,Info.,9$		;name doesn't end in ".info"
;;	MOVE.L	rl_First(A2),D0
;;	BSR	ReadData		;read icon data block into cache
;;	ERROR	9$			;bad read
;;	ZAP	D2			;FFS data offset
;;	IFNZB	FFSFlg,1$		;FFS, no checksum on data
;;	MOVEQ	#od_Data,D2		;OFS data offset
;;1$:	IFNEIW	<0(A0,D2.W)>,#IconMagic,9$ ;no, not an icon
	BSET	#rlf_Icon,rl_Flags(A2)	;else set icon flag
;;	BSR	PutCacheBuffer		;release icon buffer
9$:	POP	D2/A2-A3
	RTS

* Builds a full dir entry for linkage into dir tree.  Key in D0.
* Returns ptr to the new dir entry buffer in D0.

BuildDirEntry:
	PUSH	D2/A2
	MOVE.L	D0,D2			;key
	IFGEIL	CurDBCnt,#rl_SIZEOF,1$	;have enuf room
	BSR	AllocateDirBlock	;else get another block
	ERROR	9$
1$:	MOVE.L	CurDBPtr,A2		;point to new block
	MOVE.L	A2,A1
	ZAP	D0
	MOVE.L	D0,(A1)+		;rl_Next
	MOVE.L	D0,(A1)+		;rl_Parent
	MOVE.L	D0,(A1)+		;rl_Child
	MOVE.L	D2,(A1)+		;rl_Ownkey
	MOVE.B	D0,(A1)+		;rl_Flags
	MOVE.B	#$80,(A1)+		;rl_Type=dir
	MOVE.L	D0,(A1)+		;rl_Newkey
;;	MOVE.L	D0,(A1)+		;rl_RelocLink
	MOVE.W	D0,(A1)+		;rl_HashEntry
	BSR	UpdateDBPtr
	MOVE.L	A2,D0
9$:	POP	D2/A2
	RTS

* Creates dummy ext hdr entry for linking into ExtList.
* D0=dummy key, A0=ptr to parent filehdr/exthdr entry.

CreateDummyExt:
	PUSH	A2-A3
	MOVE.L	A0,A3			;save ptr to parent
	BSET	#rlf_WaitExt,rl_Flags(A3) ;filehdr or exthdr links to another exthdr
	MOVEQ	#$20,D1			;ext hdr
	BSR	BuildFileEntry
	ERROR	9$
	MOVE.L	D0,A2
	BSET	#rlf_Dummy,rl_Flags(A2)
	MOVE.L	A3,rl_Parent(A2)	;points back to parent filehdr/exthdr
	MOVE.L	A2,A0
	BSR	LinkExtList		;add to ext list
9$:	POP	A2-A3
	RTS

* Builds a file header entry for linkage into parent dir's child list.
* Key in D0.  Type code in D1 (can be called for ext hdr).
* Returns ptr to the new file entry buffer in D0.

BuildFileEntry:
	PUSH	D2-D3/A2
	MOVE.L	D0,D2			;key
	MOVE.L	D1,D3			;type byte (filehdr or exthdr)
	IFGEIL	CurDBCnt,#rl_SIZEOF,1$	;have enuf room
	BSR	AllocateDirBlock	;else get another block
	ERROR	9$
1$:	MOVE.L	CurDBPtr,A2		;point to new block
	MOVE.L	A2,A1
	ZAP	D0
	MOVE.L	D0,(A1)+		;rl_Next
	MOVE.L	D0,(A1)+		;rl_Parent
	MOVE.L	D0,(A1)+		;rl_ExtHdr
	MOVE.L	D2,(A1)+		;rl_Ownkey
	MOVE.B	D0,(A1)+		;rl_Flags
	MOVE.B	D3,(A1)+		;rl_Type=filehdr or exthdr
	MOVE.L	D0,(A1)+		;rl_Newkey
	MOVE.W	D0,(A1)+		;rl_HashEntry
	MOVE.L	D0,(A1)+		;rl_NewDatakey
;;	MOVE.W	D0,(A1)+		;rl_Exts
;;	MOVE.L	D0,(A1)+		;rl_Blocks
	MOVE.W	D0,(A1)+		;rl_HBlks
;;	MOVE.L	D0,(A1)+		;rl_First
	BSR	UpdateDBPtr
	MOVE.L	A2,D0
9$:	POP	D2-D3/A2
	RTS

* Key in D0.  Looks for match on Ownkey.  CY=1 on no match or A0=ptr to entry.

SearchExtList:
	MOVE.L	ExtList,A0
1$:	IFZL	A0,8$		;end of list...no match
	IFEQL	rl_Ownkey(A0),D0,9$
	MOVE.L	(A0),A0
	BRA.S	1$
8$:	STC
9$:	RTS

* Link entry in A0 into ExtList.

LinkExtList:
	LEA	ExtList,A1
	MOVE.L	(A1),D0
	MOVE.L	A0,(A1)
	MOVE.L	D0,(A0)
	RTS

* Remove item in A0 from ExtList:

UnLinkExtList:
	LEA	ExtList,A1
1$:	IFZL	A1,9$
	IFEQL	(A1),A0,2$
	MOVE.L	(A1),A1
	BRA.S	1$
2$:	MOVE.L	(A0),(A1)
	CLR.L	(A0)
9$:	RTS

* This routine is called when a file hdr is found which references a parent
* directory which is not yet present in the directory list.
* Creates a dummy directory entry for assumed parent key in D0.
* Param: D0=parent key.
* Returns ptr to dummy parent in D0.

CreateDummyParent:
	BSR	BuildDirEntry
	ERROR	9$
	MOVE.L	D0,-(A7)
	MOVE.L	D0,A1			;ptr to this new entry
	MOVE.L	DummyTree,A0		;link to this list
	BSR	LinkChild		;link into dummy dir list
	MOVE.L	(A7)+,D0		;returns ptr to new parent
9$:	RTS

* Inserts new entry into parent dir's child chain in order by increasing hashchain
* offset.  A0 points to parent dir.  New dir or file entry in A1.

LinkParent:
	PUSH	A2
	MOVE.L	A0,rl_Parent(A1)	;set up link to parent
	LEA	rl_Child(A0),A0		;point to parent's child "head" of chain
	MOVE.W	rl_HashEntry(A1),D0	;get new entry's hashentry

1$:	MOVE.L	A0,A2			;save ptr to previous entry
	MOVE.L	rl_Next(A0),D1		;get ptr to first entry
	BEQ.S	2$			;end of chain...add new entry to end
	MOVE.L	D1,A0			;else advance to next entry
	IFLEW	rl_HashEntry(A0),D0,1$	;new entry goes after current entry

2$:	MOVE.L	D1,rl_Next(A1)
	MOVE.L	A1,rl_Next(A2)		;and correct previous linkage
	POP	A2
	RTS

* Counts filehdr/exthdr data blocks and returns the number in D0.
* Ptr to filehdr/exthdr in A0.  Sets CY=1 if blocks fragmented (not contiguous).

CountBlocks:
	PUSH	D2-D3
	ADD.L	BlockSize,A0
	LEA	fh_BlkPtr1(A0),A0	;point to first block
	MOVE.L	(A0)+,D2		;starting block for check
	ZAP	D0			;block count
	ZAP	D1			;fragmented flag
1$:	MOVE.L	-(A0),D3
	BEQ.S	8$			;end of blocks
	IFEQL	D3,D2,3$		;contiguous so far
	INCL	D1			;else fragmented
3$:	INCL	D2
	INCL	D0			;count the block
2$:	IFGTL	HashTblSize,D0,1$	;not reached max yet
8$:	IFZL	D1,9$			;fully contiguous
	STC				;else report fragmented
9$:	POP	D2-D3
	RTS

* Adds together block counts for each filehdr/exthdrs for all files in DirTree.
* Also counts exthdrs.  Updates total counts in filehdr entry and also
* updates FFrag flag for file.  WARNING - calls itself recursively.

UpdateBlockCounts:
	CLR.L	IconBlkCount
	MOVE.L	DirTree,A0		;point to root
NextLev:
	PUSH	A2
	LEA	rl_Child(A0),A2		;point to chain of this level
1$:	MOVE.L	rl_Next(A2),A2		;point to next (first) entry in current chain
	IFZL	A2,9$			;all done at this level
	BTST	#rlt_Dir,rl_Type(A2)	;is it a dir?
	BEQ.S	2$			;no
	MOVE.L	A2,A0
	BSR.S	NextLev			;yes, process new level
	BRA.S	1$
2$:	BTST	#rlt_FileHdr,rl_Type(A2) ;file?
	BEQ.S	1$			;oops...should be one or other
	MOVE.L	A2,A0
	BTST	#rlf_WaitExt,rl_Flags(A0) ;waiting for ext hdr?
	BEQ.S	3$
	INCW	ErrorCount		;else something missing...trouble
***	DispErr	Err5.
3$:	BSR.S	AdjustFileEntry		;cound total blocks for this file
	BRA.S	1$
9$:	POP	A2
	RTS

* Checks for fragmented extents and counts icon blocks.

AdjustFileEntry:
	PUSH	D2
	ZAP	D1			;contiguous flag
	ZAP	D2			;block running total
	MOVE.L	A0,A1			;filehdr entry
1$:	ZAP	D0
	MOVE.W	rl_HBlks(A1),D0		;count of blocks in this ext
	ADD.L	D0,D2			;running total of blocks
	BTST	#rlf_HFrag,rl_Flags(A1)	;ext fragmented?
	BEQ.S	2$			;no
	INCL	D1			;else file is fragmented
2$:	MOVE.L	rl_ExtHdr(A1),A1	;point to next (first) ext
	IFNZL	A1,1$			;done

	BTST	#rlf_Icon,rl_Flags(A0)	;icon file?
	BEQ.S	3$			;no
	ADD.L	D2,IconBlkCount		;else count the icon blocks
3$:	IFZL	D1,9$			;fully contiguous
	BSET	#rlf_FFrag,rl_Flags(A0)	;else show file fragmented
	INCL	FragFileCnt		;count the file
9$:	POP	D2
	RTS

* This calculates the amount of space which must be reserved immediately following
* the root block for filehdrs and userdirs.  Exthdrs are NOT counted here, since
* they are stored with the data blocks.  Bitmap blocks, bitmap ext blocks, and the 
* root are counted.  BadBlocks in range must be counted.

CalcLowestKeys:
	MOVE.L	RootBlkNbr,D0		;everything starts at root key
	INCL	D0
	ADD.L	BitmapBlocks,D0		;bitmap blocks
	ADD.L	BMExtBlocks,D0		;bitmap extension blocks
	MOVE.L	D0,LowestCtrlKey

	MOVE.L	ActFileCnt,D1		;filehdr count
	ADD.L	ActDirCnt,D1		;userdir count (including root)
	IFZB	WBModeFlg,2$
	ADD.L	IconBlkCount,D1		;icons get stored with control blks
2$:	DECL	D1			;adjust for root, counted twice
	BSR.S	AdjustBadBlks		;adjust D0 for bad blocks
	MOVE.L	D0,LowestFreeKey	;free space starts here
	ADD.L	FreeKeys,D0		;first free data key
	IFGEL	HighestKey,D0,1$	;starts in upper end
	SUB.L	MaxKeys,D0		;else swap to lower end
1$:	MOVE.L	D0,LowestDataKey	;first data block goes here
	RTS

* Adds D1 to D0 after ascertaining how many bad blocks exist in the range.
* Base key in D0, count in D1.

AdjustBadBlks:
	PUSH	D2-D5
	MOVE.L	D0,D2			;base key
	MOVE.L	D0,D3
	MOVE.L	D1,D4			;unadjusted count
	MOVE.L	D1,D5
	IFZB	BadBlkFlg,7$		;no bad blocks
	BRA.S	3$
1$:	MOVE.L	D3,D0
	BSR	GetBadStatus		;key is good?
	BNE.S	2$			;no
	INCL	D4			;else add an extra key
	INCL	D3
	BRA.S	1$			;wasn't good, skip

2$:	INCL	D3			;try next key
3$:	DBF	D5,1$

7$:	ADD.L	D4,D2
	MOVE.L	D2,D0
	POP	D2-D5
	RTS

* Assigns new key to each entry in catalog.  All entries in a given directory are
* grouped together in order by hashchain.  Exthdrs are allocated immediately before
* the data blocks.  Data blocks are grouped contiguously, with a couple of minor
* exceptions relating to bad blocks (and files which cannot be moved).  Data blocks
* for icon files are placed immediately after the filehdr if reorganizing into
* Workbench mode.  For CLI mode icon data blocks are treated the same as those for
* any other type of file.

AssignNewKeys:
	PUSH	D2/A2-A3
	MOVE.L	DirTree,A0		;start at top of tree
	MOVE.L	RootBlkNbr,rl_Newkey(A0) ;give root entry a newkey
	MOVE.L	LowestCtrlKey,D2
	BSR	AssignCtrlKeys
;;	ERROR	9$
	MOVE.L	LowestDataKey,D2
	BSR	AssignDataKeys
9$:	POP	D2/A2-A3
	RTS

* Assigns keys to control blocks (user dirs and filehdrs), based on key in D2.
* File data blocks are NOT assigned here unless file is marked as icon file.

AssignCtrlKeys:
	MOVE.L	DirTree,A0
AssCtrl:
	PUSH	A2-A3
	LEA	rl_Child(A0),A2
	MOVE.L	A2,A3
1$:	MOVE.L	rl_Next(A3),A3
	IFZL	A3,2$
	BSR.S	CheckBadKey		;check current key and skip if bad
	MOVE.L	D2,rl_Newkey(A3)	;assign new key to ctrl block
	BSR.S	AdvanceKey
	BTST	#rlt_Dir,rl_Type(A3)	;a dir entry?
	BNE.S	1$			;yes, ignore on this pass
	BTST	#rlt_FileHdr,rl_Type(A3) ;filehdr?
	BEQ.S	1$			;no...link maybe? ignore it for now
	BTST	#rlf_Icon,rl_Flags(A3)	;icon file?
	BEQ.S	1$			;no
	BSR	AssignFile
;;	ERROR	9$
	BRA.S	1$

2$:	MOVE.L	rl_Next(A2),A2
	IFZL	A2,9$
	BTST	#rlt_Dir,rl_Type(A2)	;directory?
	BEQ.S	2$			;no
	MOVE.L	A2,A0			;drop down a level and assign keys
	BSR.S	AssCtrl
	BRA.S	2$

9$:	POP	A2/A3
	RTS

* Checks that key in D2 is NOT a bad block.  Advances key if so.

CheckBadKey:
	IFZB	BadBlkFlg,2$
1$:	MOVE.L	D2,D0
	BSR	GetBadStatus
	BNE.S	2$
	INCL	D2
	IFGEL	HighestKey,D2,1$
	MOVE.L	LowestKey,D2
	BRA.S	1$
2$:	RTS

AdvanceKey:
	INCL	D2
	IFGEL	HighestKey,D2,1$
	MOVE.L	LowestKey,D2
1$:	RTS

* Assigns keys to files, based on key in D2.

AssignDataKeys:
	MOVE.L	DirTree,A0
AssData:
	PUSH	A2-A3
	LEA	rl_Child(A0),A2
	MOVE.L	A2,A3
1$:	MOVE.L	rl_Next(A3),A3
	IFZL	A3,2$
	BTST	#rlt_FileHdr,rl_Type(A3) ;filehdr?
	BEQ.S	1$			;no
	BTST	#rlf_Icon,rl_Flags(A3)	;icon file?
	BNE.S	1$			;yes...already assigned
	BSR.S	AssignFile
	BRA.S	1$

2$:	MOVE.L	rl_Next(A2),A2
	IFZL	A2,9$
	BTST	#rlt_Dir,rl_Type(A2)	;directory?
	BEQ.S	2$			;no
	MOVE.L	A2,A0			;drop down a level and assign keys
	BSR.S	AssData
	BRA.S	2$

9$:	POP	A2/A3
	RTS

* Assigns keys to file whose entry is A3. 

AssignFile:
	PUSH	A2
	MOVE.L	A3,A2			;else allocate ext keys first
1$:	MOVE.L	rl_ExtHdr(A2),A2	;get (next) exthdr
	IFZL	A2,2$			;thats all
	BSR	CheckBadKey		;check current key and skip if bad
	MOVE.L	D2,rl_Newkey(A2)	;assign new key to exthdr
	BSR.S	AdvanceKey
	BRA.S	1$

2$:	MOVE.L	A3,A0			;else assign data keys to filehdr
	BSR.S	AssignExtent
	MOVE.L	A3,A2
3$:	MOVE.L	rl_ExtHdr(A2),A2	;get (next) exthdr
	IFZL	A2,9$			;no more
	MOVE.L	A2,A0
	BSR.S	AssignExtent		;assign data keys to exthdr
	BRA.S	3$
9$:	POP	A2
	RTS

* Assigns data block keys to extent in A0.  If the extent must wrap around
* one or more bad keys, D2 is adjusted properly, and the bad keys get skipped
* at a later time.
  
AssignExtent:
	PUSH	A2
	MOVE.L	A0,A2
	IFZW	rl_HBlks(A2),9$		;no blocks to be assigned
	BTST	#rlf_DontMove,rl_Flags(A2) ;don't move this file?
	BNE.S	9$			;move it
	BSR	CheckBadKey
	MOVE.L	D2,rl_NewDatakey(A2)	;data starts here
	MOVE.L	A2,A0
	BSR.S	AdjustBlkCnt
	ADD.L	D0,D2
	IFGEL	HighestKey,D2,9$
	SUB.L	MaxKeys,D2
	BSET	#rlf_Wrapped,rl_Flags(A2)
9$:	POP	A2
	RTS

* Given filehdr/exthdr entry in A0, returns adjusted count of blocks in D0.  
* Adds one to block count for every bad key that falls into the range of 
* this extent. Preserves A0.  Returns adjusted block count in D0.

AdjustBlkCnt:
	PUSH	D2-D4/A0
	MOVE.W	rl_HBlks(A0),D3
	EXT.L	D3
	MOVE.L	D3,D4
	IFZB	BadBlkFlg,9$		;do nothing
	MOVE.L	rl_NewDatakey(A0),D2
	BRA.S	4$
1$:	INCL	D2
	IFGEL	HighestKey,D2,2$
	MOVE.L	LowestKey,D2
2$:	MOVE.L	D2,D0
	BSR	GetBadStatus
	BNE.S	4$			;not bad
	INCL	D4			;else add one to count and go again
	BRA.S	1$
4$:	DBF	D3,1$			;loop for all blocks in hdr
9$:	MOVE.L	D4,D0
	MOVE.L	D2,D1
	POP	D2-D4/A0
	RTS

***************************************************************************

* Deletes QBT.FMT, if any.  The saved format is useless after the reorg.

DeleteSavedFormat:
	PUSH	D2
	IFZB	ReorgFlg,9$		;not updating...
	MOVE.L	RootBlkNbr,D2
	LEA	Form.,A0		;QBT.FMT
	MOVE.L	D2,D0
	BSR	OpenFile		;try to find existing saved format file
	ERROR	1$			;nonesuch...create it
	MOVE.L	D2,D1
	BSR	DeleteFile		;else get rid of it, whatever it is
	ERROR	9$
	BSR	WriteCache
1$:	CLC
9$:	POP	D2
	RTS

**********************************************************************************
* Init dir/file lists

InitDirLists:
	MOVE.L	#BaseDirBlock,CurDBBase
	CLR.L	CurDBCnt
	MOVE.L	RootBlkNbr,D0	;root block number
	BSR	BuildDirEntry
	ERROR	9$
	MOVE.L	D0,DirTree	;init ptr to root directory list
	MOVEQ	#-1,D0		;dummy root block must never match
	BSR	BuildDirEntry
	ERROR	9$
	MOVE.L	D0,DummyTree
	CLR.L	ExtList
9$:	RTS

**********************************************************************************


ModeWB:	MOVEQ	#1,D0
	BRA.S	WBCom

ModeCLI:
	ZAP	D0
WBCom:	MOVE.B	D0,WBModeFlg
	RTS

;Err1.	TEXTZ	'error 1'
;Err2.	TEXTZ	'error 2'
;Err3.	TEXTZ	'error 3'
;Err4.	TEXTZ	'error 4'
;Err5.	TEXTZ	'error 5'
;Err6.	TEXTZ	'error 6'
;Err7.	TEXTZ	'error 7'
;Err8.	TEXTZ	'error 8'
;Err9.	TEXTZ	'error 9'
;Err10.	TEXTZ	'error 10'


Info.		TEXTZ	'.INFO'

		BSS,PUBLIC

ExtList			DS.L	1	;list of ext hdrs
FragFileCnt		DS.L	1
LowestCtrlKey		DS.L	1	;low key of control area
LowestFreeKey		DS.L	1	;first key of free space
LowestDataKey		DS.L	1	;first key of newly-allocated data keys
IconBlkCount		DS.L	1	;count of blocks of icon files
ReorgFlg		DS.B	1	;1=full, 0=defrag only

