;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
********************************************************* 
*							*
*	Quarterback Tools Disk I/O Routines		*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1990 Central Coast Software	*
*********************************************************


	INCLUDE	"vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"vd0:BOXES.ASM"
	INCLUDE "I/IO.i"

	XDEF	MotorOff,CmdClear
	XDEF	SendPacket
	XDEF	DiskIO
	XDEF	ReadBlock,WriteBlock
	XDEF	ReadMulti,WriteMulti
	XDEF	ReadBoot,WriteBoot
	XDEF	FormatCyl,ReadCyl

	XDEF	PktRes1
	XDEF	PktRes2
	XDEF	PktArg1
	XDEF	PktArg2
	XDEF	PktArg3
	XDEF	PktArg4

* Externals defined in TOOLS.ASM

	XREF	SysBase,DosBase,IntuitionBase,GraphicsBase
	XREF	Window,TMP.,TMP2.
	XREF	ToolsTask

* Externals in DEVICE.ASM

	XREF	DriverIOB,DriverPort
	XREF	VolumeOffset
	XREF	BlockSize,BlkSizeShift
	XREF	MaxTransfer
	XREF	HighestKey,LowestKey

* Externals in BUFFERS.ASM

pr_MsgPort	EQU	$5C
ln_Name		EQU	10	;list node name offset
MP_SIGBIT	EQU	$F	;signal bit number in msg port

ERROR_OBJECT_IN_USE	EQU	202
ERROR_DISK_FULL		EQU	221
ERROR_DELETE_PROTECTED	EQU	222
ERROR_WRITE_PROTECTED	EQU	223
ERROR_READ_PROTECTED	EQU	224

ACTION_PROT	EQU	21	;packet command codes
ACTION_DISK_INFO EQU	25
ACTION_COMMENT	EQU	28
ACTION_INHIBIT	EQU	31
ACTION_DATE	EQU	34

TDERR_NotSpecified	EQU	20
TDERR_NoSecHdr		EQU	21
TDERR_BadSecPreamble	EQU	22
TDERR_BadSecID		EQU	23
TDERR_BadHdrSum		EQU	24
TDERR_BadSecSum		EQU	25
TDERR_TooFewSecs	EQU	26
TDERR_BadSecHdr		EQU	27
TDERR_WriteProt		EQU	28
TDERR_DiskChanged	EQU	29
TDERR_SeekError		EQU	30
TDERR_NoMem		EQU	31
TDERR_BadUnitNum	EQU	32
TDERR_BadDriveType	EQU	33
TDERR_DriveInUse	EQU	34
TDERR_PostReset		EQU	35

* Reads D1 blocks, starting at key in D0 into buffer in A0.

ReadCyl:
	MOVE.L	D0,A1
	BRA.S	RdMCom

ReadMulti:
	IFZL	D1,RngErr,L	;must have at least 1 block
	MOVE.L	D0,A1
	IFGTL	LowestKey,D0,RngErr,L
	ADD.L	D1,D0
	DECL	D0
	IFLTL	HighestKey,D0,RngErr,L
RdMCom:	MOVE.L	BlkSizeShift,D0
	LSL.L	D0,D1		;convert block count to size
;;	MOVE.L	MaxTransfer,D0
;;	IFLEL	D1,D0,1$	;limit transfer size of MaxTransfer
;;	MOVE.L	D0,D1
1$:	MOVE.L	A1,D0	
	BRA.S	RdCom

ReadBoot:
	ZAP	D0
	BRA.S	RdOne

* Reads one block whose key is in D0 from open device into buffer in A0.

ReadBlock:
	IFGTL	LowestKey,D0,RngErr
	IFLTL	HighestKey,D0,RngErr
RdOne:	MOVE.L	BlockSize,D1
RdCom:	MOVE.L	DriverIOB,A1		;IOBlock
	IFZL	A1,2$			;none...error
	MOVE.L	D1,io_Length(A1)
	MOVE.L	A0,io_Data(A1)		;data goes here
	MOVE.L	BlkSizeShift,D1		;converts key to byte offset
	LSL.L	D1,D0			;get byte offset to desired block
	ADD.L	VolumeOffset,D0		;correct for possible partition
	MOVE.L	D0,io_Offset(A1)
	MOVE.W	#CMD_READ,io_Command(A1)
	CLR.B	io_Error(A1)
	MOVE.L	D2,-(SP)		;VD0: sometimes clobbers D2
	CALLSYS	DoIO,SysBase
	MOVE.L	(SP)+,D2
	MOVE.L	DriverIOB,A1		;IOBlock
	IFNZL	D0,1$			;error
	MOVE.L	io_Actual(A1),D0	;status, if any, returned in D0
	RTS

1$:	ZAP	D0
	MOVE.B	io_Error(A1),D0
2$:	STC
	RTS

RngErr:	MOVEQ	#-1,D0
	STC
	RTS

* Writes D1 blocks, starting at key D0 from buffer A0.

WriteMulti:
	IFZL	D1,RngErr	;must have at least 1 block
	MOVE.L	D0,A1
	IFGTL	LowestKey,D0,RngErr
	ADD.L	D1,D0
	DECL	D0
	IFLTL	HighestKey,D0,RngErr
	MOVE.L	BlkSizeShift,D0
	LSL.L	D0,D1		;convert block count to size
;;	MOVE.L	MaxTransfer,D0
;;	IFLEL	D1,D0,1$	;limit transfer size to MaxTransfer
;;	MOVE.L	D0,D1
1$:	MOVE.L	A1,D0	
	BRA.S	WtCom

WriteBoot:
	ZAP	D0
	BRA.S	WtOne

* Writes one block whose key is in D0 to open device from buffer in A0.

WriteBlock:
	IFGTL	LowestKey,D0,RngErr
	IFLTL	HighestKey,D0,RngErr
WtOne:	MOVE.L	BlockSize,D1
WtCom:	MOVE.L	DriverIOB,A1		;IOBlock
	IFZL	A1,2$
	MOVE.L	D1,io_Length(A1)
	MOVE.L	A0,io_Data(A1)		;data goes here
	MOVE.L	BlkSizeShift,D1		;converts key to byte offset
	LSL.L	D1,D0			;get byte offset to desired block
	ADD.L	VolumeOffset,D0		;correct for possible partition
	MOVE.L	D0,io_Offset(A1)
	MOVE.W	#CMD_WRITE,io_Command(A1)
	CLR.B	io_Error(A1)
	MOVE.L	D2,-(SP)		;old VD0: sometimes clobbers D2
	CALLSYS	DoIO,SysBase
	MOVE.L	(SP)+,D2
	MOVE.L	DriverIOB,A1		;IOBlock
	IFNZL	D0,1$			;error
	MOVE.L	io_Actual(A1),D0	;status, if any, returned in D0
	RTS

1$:	ZAP	D0
	MOVE.B	io_Error(A1),D0
2$:	STC
	RTS

* Formats D1 blocks, starting at key D0 from buffer A0.
* (Trackdisk.device only).

FormatCyl:
	PUSH	D2
	MOVE.L	DriverIOB,A1		;IOBlock
	IFZL	A1,2$
	MOVE.L	BlkSizeShift,D2
	LSL.L	D2,D1			;convert to size in bytes
	LSL.L	D2,D0			;byte offset for starting block
	MOVE.L	D1,io_Length(A1)
	MOVE.L	D0,io_Offset(A1)
	MOVE.L	A0,io_Data(A1)		;data goes here
	MOVE.W	#TD_FORMAT,io_Command(A1)
	CALLSYS	DoIO,SysBase
	MOVE.L	DriverIOB,A1		;IOBlock
	IFNZL	D0,1$			;error
	MOVE.L	io_Actual(A1),D0	;status, if any, returned in D0
	BRA.S	9$

1$:	ZAP	D0
	MOVE.B	io_Error(A1),D0
2$:	STC
9$:	POP	D2
	RTS

* Miscellaneous synchronous IO.  IOblock in A1.  Command in D0.

DiskIO:
	PUSH	A1			;preserve ptr to IOB
	MOVE.W	D0,io_Command(A1)
	CALLSYS	DoIO,SysBase
	POP	A1
	IFZL	D0,1$
	MOVE.B	io_Error(A1),D0
	STC
	RTS
1$:	MOVE.L	io_Actual(A1),D0	;status, if any, returned in D0
	RTS

* Sends packet.  Called with packet type code in D0, ProcessID in A0.
* Remainder of packet must already be set up prior to calling.  

SendPacket:
	PUSH	A2
	MOVE.L	D0,PktType		;save packet type
	MOVE.L	ToolsTask,A2
	LEA	pr_MsgPort(A2),A2	;point to our own port
	MOVE.L	A2,PktPort		;reload reply port
	LEA	Packet,A1
	MOVE.L	#DosPkt,ln_Name(A1)
	CALLSYS	PutMsg,SysBase		;send the packet
	MOVE.L	A2,A0
	CALLSYS	WaitPort		;wait for a reply
	LEA	Packet,A1
	CALLSYS	Remove			;dequeue the packet
	MOVE.L	PktRes1,D0		;response in D0
	POP	A2
	RTS

CmdClear:
	MOVEQ	#CMD_CLEAR,D0
	BRA.S	TDCom

* Turns selected drive motor off.

MotorOff:
	MOVEQ	#CMD_UPDATE,D0
	BSR.S	TDCom
	MOVEQ	#TD_MOTOR,D0		;turn off the motor
TDCom:	MOVE.L	DriverIOB,A1
	IFZL	A1,9$
	MOVE.W	D0,io_Command(A1)
	ZAP	D0
	MOVE.L	D0,io_Length(A1)
	CALLSYS	DoIO,SysBase
9$:	RTS

NODE    MACRO
        DC.L    0       ;SUCC
        DC.L    0       ;PRED
        DC.B    \1      ;TYPE
        DC.B    \2      ;PRI
        DC.L    \3      ;NAME POINTER
        ENDM

***** WARNING -- WB1.2 REQUIRES PACKETS TO LONG-WORD ALIGNED***********

	CNOP	0,4

Packet
PktMsg	NODE	5,0,DosPkt
	DC.L	0
	DC.W	PMsgSize
DosPkt	DC.L	PktMsg
PktPort	DC.L	0
PktType	DC.L	0
PktRes1	DC.L	0
PktRes2	DC.L	0
PktArg1	DC.L	0
PktArg2	DC.L	0
PktArg3	DC.L	0
PktArg4	DC.L	0,0,0,0
PMsgSize EQU	*-PktMsg

IOError	DC.W	0

	END

