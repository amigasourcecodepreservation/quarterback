;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*****************************************************************
*								*
*		RESTORE.ASM					*
*								*
*	Quarterback Tools Restore Deleted Files			*
*								*
*****************************************************************

	INCLUDE "vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"VD0:BOXES.ASM"

	XDEF	Function4,Function22
	XDEF	AllocateDirBlock
	XDEF	FreeDirBlocks
	XDEF	UpdateFileCounts
	XDEF	DispCntTxt
	XDEF	NameGadHit,DupGadHit
	XDEF	ScanForFiles
	XDEF	InitDirLists
	XDEF	DisplayFilePath
	XDEF	MoveFileName
	XDEF	SearchDummyTree
	XDEF	SearchDirTree
	XDEF	UnLinkDummy
	XDEF	LinkChild
	XDEF	DispCntTxt
	XDEF	UpdateCounts
	XDEF	UpdateDBPtr
	XDEF	RestCurrent,RestDifferent

	XDEF	DirTree,DummyTree
	XDEF	FileName.
	XDEF	CurDBPtr
	XDEF	CurDBCnt
	XDEF	BaseDirBlock,CurDBBase
	XDEF	ActFileCnt,ActDirCnt
	XDEF	FileCntMode

* Externals in TOOLS.ASM

	XREF	SysBase,DosBase,IntuitionBase,GraphicsBase
	XREF	MLoop,MLoopErr
	XREF	ProcessMsg

	XREF	TMP.,TMP2.,TMP3.
	XREF	CancelFlg
	XREF	AbortFlg
	XREF	ProcFlg
	XREF	YesFlg
	XREF	SkipFlg

* Externals in COPY.ASM

	XREF	CopySelectedFiles

* Externals in VTEST.ASM

	XREF	InsertVolName
	XREF	CurrentBlock
	XREF	ForceRead

* Externals in Fun.asm

	XREF	CLRWIN
	XREF	RefreshAllGads

* Externals in FILES.ASM

	XREF	ReadCtrlBlock
	XREF	GetBusyStatus
	XREF	LinkParentDir
	XREF	OpenFile
	XREF	DeleteFile
	XREF	CheckFileBlocks,BusyFileBlocks
	XREF	CheckBadBlocks,GetBadStatus

	XREF	BadBlkFlg

* Externals in DISK.ASM

	XREF	ReadBlock
	XREF	MotorOff

* Externals in Device.asm

	XREF	CheckVolumeStatus,CheckDiskLoaded
	XREF	CalcVolParams
	XREF	OpenDriver,CloseDriver
	XREF	InhibitDos,EnableDos
	XREF	CheckProtection

	XREF	NumSoftErrors
	XREF	UnitNumber
	XREF	DiskState
	XREF	NumBlocks
	XREF	NumBlocksUsed
	XREF	BytesPerBlock
	XREF	DiskType
	XREF	VolumeNode
	XREF	InUse	
	XREF	MaxBytes
	XREF	MaxBlocks
	XREF	FreeBytes
	XREF	FreeBlocks
	XREF	VolumeOffset
	XREF	LowCyl
	XREF	HighCyl
	XREF	MaxCylinders
	XREF	Surfaces
	XREF	MaxTracks
	XREF	BlockSize,BlockSizeL
	XREF	Reserved,PreAlloc
	XREF	BlocksPerTrack
	XREF	NumBuffers
	XREF	BufMemType
	XREF	MaxTransfer
	XREF	PercentFull
	XREF	DosType,FFSFlg
	XREF	DeviceNameBuf.
	XREF	DriverNameBuf.
	XREF	VolValidFlg
	XREF	NoDiskFlg
	XREF	NotDosFlg
	XREF	NotValidFlg
	XREF	WrtProtFlg
	XREF	VolStatusMsg
	XREF	BlkSizeOffset
	XREF	RootBlkNbr
	XREF	HashTblSize
	XREF	BitmapBlocks
	XREF	LowestKey,HighestKey

* Externals in GRAPHICS.ASM

	XREF	InitBar
	XREF	UpdateStatusBar
	XREF	RefreshBar

* Externals in Messages.asm

	XREF	Fatal1.
	XREF	NoMemErr.
	XREF	Rest1.,Rest2.
	XREF	ActFil.
	XREF	DelFil.
	XREF	ActDir.
	XREF	DelDir.
	XREF	NoFilesSel.
;;	XREF	Damag1.,Damag2.,Damag3.,Damag4.
	XREF	RestFile.

* Externals in Gadgets.asm

	XREF	CanGad,PauseGad
	XREF	ReqNameGad,ReqProc1,ReqYes
	XREF	ReqDupGad,ReqProc2
	XREF	ReqProceed
	XREF	ResName,ResDup,ResDir
	XREF	NamBuf
	XREF	DelNam1.,DelNam2.
	XREF	NoDelEr1.,NoDelEr2.
	XREF	NoNamFndTxt
	XREF	NoDelFndTxt

* Externals in CAT.ASM

	XREF	DispFileCatalog
	XREF	PushWindow,PopWindow,NextWindow
	XREF	UpdWinFlg
	XREF	InitTree,NextFile

	XREF	SelDeleted
	XREF	Path.

* Externals in BUFFERS.ASM

	XREF	ReadControl,ReadData
	XREF	GetCacheBuffer,PutCacheBuffer
	XREF	ZapBlock,ZapAnyBlock
	XREF	AllocateBuffer,AllocateIOBuf
	XREF	ReadCache,WriteCache
	XREF	AllocateBlock,FreeBlock
	XREF	CalcCkSum
	XREF	FreeBadBlkBuf

	XREF	BlockBuffer

* EXternals in BITMAP.ASM

	XREF	ReadBitmap,WriteBitmap
	XREF	MarkKeyBusy

* Externals in AREXX.ASM

	XREF	ArexxFlg,CmdBuf

DirBlockSize	EQU	2100	;size of directory block

* Externals in REPAIR.ASM

	XREF	VerifyCtrlBlock

* Restore a file or a directory..

Function22:
Function4:
	ZAP	D0
	MOVE.L	D0,ActFileCnt		;init file counts
	MOVE.L	D0,DelFileCnt
	MOVE.L	D0,ActDirCnt
	MOVE.L	D0,DelDirCnt
	MOVE.B	D0,NoBitmapFlg
	MOVE.B	D0,CancelFlg
	SETF	FileCntMode		;active and deleted
	BSR	FreeDirBlocks		;free any previous dir blocks
	BSR	CheckVolumeStatus		;get device params
	ERROR	MLoop			;problems
	BSR	CheckDiskLoaded
	ERROR	MLoop
	BSR	CalcVolParams		;calc volume params
	BSR	GetFileName		;get name of file to be restored
	IFNZB	CancelFlg,MLoop,L	;op wants to bail out
	MOVE.L	MaxBlocks,D0
	BSR	InitBar			;initialize the bar counts
	LEA	PauseGad,A0
	LEA	Fun5Refresh,A1
	ZAP	D0
	BSR	PushWindow		;remove gadgets and clear window
	BSR	ReadBitmap		;gotta have the bitmap
	NOERROR	4$
	IFNZB	VolValidFlg,Fun4Exit,L	;should have been able to read bitmap
	SETF	NoBitmapFlg		;continue on...just don't use bitmap
4$:	BSR	InitDirLists		;initialize directory tree
	ERROR	Fun4Error		;oops
	BSR	CheckBadBlocks
	BSR	InhibitDos		;lock out ADOS
	BSR	ScanForFiles		;scan for deleted files
	IFNZB	CancelFlg,Fun4Exit,L
	BSR	CheckDummyTree		;find out what's left unaccounted
	ERROR	Fun4Exit
	IFZB	VolValidFlg,3$,L	;bad volume...just display catalog
	IFNZL	DelFileCnt,3$,L		;found one or more deleted files
	IFNZB	ArexxFlg,5$,L		;for Arexx, go right to restore
	LEA	NamBuf,A0
	IFZB	(A0),1$			;no name specified
	MOVE.	A0,TMP.			;move to TMP. for error msg
	BldWarn	0,0,DelNam1.,DelNam2.
	BRA.S	2$
1$:	BldWarn	0,0,NoDelEr1.,NoDelEr2.
2$:	DispReq	A0,ReqProc1
	IFNZB	CancelFlg,Fun4Exit,L

3$:	BSR	DispFileCatalog		;display file for op to select
	ERROR	Fun4Exit		;anything but proceed
	IFZB	CopyFlg,5$		;not going to different vol
	BSR	CopySelectedFiles	;else copy some and go back for more
	BSR	MotorOff
	BRA.S	3$

5$:	BSR	CheckProtection
	ERROR	Fun4Exit
	BSR	RestoreSelectedFiles
	IFNZB	CancelFlg,Fun4Exit
	BSR	WriteCache
	BSR	WriteBitmap		;rewrite bitmap blocks
	BRA.S	3$

Fun4Exit:
	BSR	EnableDos
Fun4Error:
	BSR	FreeDirBlocks
	BSR	FreeBadBlkBuf
	BSR	PopWindow		;restore window
	IFNZB	CancelFlg,MLoopErr
	BRA	MLoop

GetFileName:
	CLR.B	NamBuf			;start with no name
	IFZB	VolValidFlg,9$		;forget it if invalid vol
	IFNZB	ArexxFlg,1$
	DispReq	ResName,ReqNameGad
	BRA.S	9$
1$:	SCAN.	CmdBuf,NamBuf
9$:	RTS

* Scans volume looking for active/deleted files and directories.

ScanForFiles:
	PUSH	D2-D3
	CLR.B	CancelFlg
	MOVE.L	LowestKey,D2
	MOVE.L	HighestKey,D3
;;	SUB.L	PreAlloc,D3
	MOVE.L	D2,CurrentBlock		;start with block 0
	BSR	AllocateBlock
	BEQ	9$
1$:	IFZB	BadBlkFlg,2$
	MOVE.L	D2,D0			;key of current block
	BSR	GetBadStatus		;known bad block?
	BEQ.S	6$			;yes
2$:	MOVE.L	D2,D0
	MOVE.L	BlockBuffer,A0
	BSR	ReadBlock
	ERROR	6$
	MOVE.L	BlockBuffer,A0
	MOVE.L	CurrentBlock,D0
	MOVE.L	fh_Type(A0),D1
	IFNEIL	D1,#T_SHORT,6$		;not root, user dir, or filehdr
	BSR	ProcessControlBlock
	BSR	UpdateCounts
6$:	INCL	D2
	MOVE.L	D2,CurrentBlock		;advance to next block
	BSR	UpdateStatusBar		;show something happening
	BSR	ProcessMsg		;check for op action
	IFNZB	AbortFlg,9$		;check for operator abort
	IFNZB	CancelFlg,9$		;op has canceled
;	IFLEL	D2,HighestKey,1$ 	;loop for all blocks of volume
	IFLEL	D2,D3,1$	 	;loop for all blocks of volume

9$:	BSR	ForceRead		;2090 patch
	BSR	FreeBlock
	POP	D2-D3
	RTS

* This is the code to actually restores selected files.

RestoreSelectedFiles:
	PUSH	A2
	LEA	PauseGad,A0
	LEA	RestoreRefresh,A1
	ZAP	D0
	BSR	NextWindow
	IFNZL	SelDeleted,1$		;got something
	DispErr	NoFilesSel.		;nothing to restore
	BRA	9$
1$:	BSR	InitTree		;reset to top
2$:	BSR	NextFile		;get next deleted file from list
	ERROR	9$			;end of list
	MOVE.L	A0,A2			;save ptr to file to be restored
	MOVE.L	df_Parent(A2),A0	;ptr to parent fib
	BSR	RestoreParent		;check parent dir and restore
	NOERROR	3$			;parent is good...proceed

* We can't restore the parent, for some reason...so we'll give the op a
* chance to restore to some other directory?

	NOP

3$:	MOVE.L	A2,A0			;ptr to filehdr entry
	BSR.S	RestoreOneFile
	IFNZB	CancelFlg,9$
;;	ERROR	9$
	IFNZB	AbortFlg,9$
	BSR	ProcessMsg
	IFZB	CancelFlg,2$,L		;loop unless op cancels
9$:	POP	A2
	RTS

* This routine actually restores the file whose FIB is in A0.

RestoreOneFile:
	PUSH	D2-D4/A2-A3
	MOVE.L	A0,A2			;ptr to entry for this file
	MOVE.	df_Name(A2),TMP.
	LEN.	TMP.
	MOVEQ	#FileNameLen,D1
	SUB.W	D0,D1
	STRING.	#32,D1,TMP3.
	LEA	FileName.,A0
	MOVE.B	#'/',(A0)+
	CLR.B	(A0)
	APPEND.	TMP.,FileName.
	APPEND.	TMP3.,FileName.
	LEA	RestFile.,A0
	BSR	DisplayFilePath
	MOVE.L	df_Ownkey(A2),D2	;file hdr key
	MOVE.L	df_Parent(A2),A3	;ptr to parent's entry
	MOVE.L	df_Ownkey(A3),D3	;parent key
1$:	MOVE.L	D2,D0			;filehdr key
	BSR	CheckFileBlocks		;are all blocks of file still free?
	ERROR	8$			;critical blocks busy...abort
	IFZL	D0,3$			;all blocks are free
	IFNZB	ArexxFlg,9$,L		;treat as skip for arexx
;;	MOVE.L	BlockSize,D1		;some blocks are busy...restore them?
;;	MULU	D1,D0			;calc number of missing bytes
;;	BldWarn	D0,0,Damag1.,Damag2.,Damag3.,Damag4.
;;	DispReq	A0,ReqProc1
	BRA	8$
;;;	IFNZB	CancelFlg,9$,L		;op wants to bail out
;;;	IFNZB	SkipFlg,9$,L		;skip this file
;;;	MOVE.L	D2,D0
;;;	BSR	FixFileHdr		;else get rid of busy blocks from hdr
3$:	LEA	df_Name(A2),A0		;point to name of this file
	MOVE.L	D3,D0			;parent key
	BSR	OpenFile		;does duplicate file already exist?
	ERROR	2$			;no...proceed
	IFNZB	ArexxFlg,5$		;arexx...replace file
	MOVE.L	D0,D4			;key of matching entry
	MOVE.	df_Name(A2),NamBuf
	DispReq	ResDup,ReqDupGad
	IFNZB	CancelFlg,9$,L		;op wants to bail out
	IFNZB	SkipFlg,9$,L		;skip this file
	IFNZB	ProcFlg,5$		;op says overwrite other file
	MOVE.	NamBuf,df_Name(A2)	;use this as new name for file
	MOVE.L	df_Ownkey(A2),D0
	BSR	ReadControl
	MOVE.L	A0,A1
	Z_STG.	NamBuf,A1		;rewrite name in filehdr
	BRA.S	3$			;make sure this name is ok

5$:	MOVE.L	D4,D0			;key of file to be deleted
	MOVE.L	D3,D1			;parent key
	BSR	DeleteFile		;get rid of duplicate file
	ERROR	9$			;error deleting duplicate...*****

2$:	BTST	#fib_NewParent,df_Flags(A2) ;needs update to parent key?
	BEQ.S	4$
	MOVE.L	D2,D0
	BSR	ReadControl
	ERROR	4$
	DIRTY	A0
	ADD.L	BlockSize,A0
	MOVE.L	D3,fh_Parent(A0)	;update parent key
4$:	MOVE.L	D2,D0			;key of file to be restored
	BSR	BusyFileBlocks		;mark blocks of file busy again
	MOVE.L	D2,D0			;filehdr key
	MOVE.L	D3,D1			;parent's key
	BSR	LinkParentDir		;link to parent dir
	ERROR	8$
	BSR	WriteCache		;force all dirty blocks to disk
	ERROR	8$
	BCLR	#fib_Selected,df_Flags(A2) ;clear selected bit if successful
	BRA.S	9$
8$:	IFNZB	ArexxFlg,10$
	BldWarn	D0,0,Damag1.,Damag2.,Damag3.,Damag4.
	DispReq	A0,ReqProc2
;;	BldWarn	0,0,RestEr1.,RestEr2.,RestEr3.
;;	DispReq	A0,ReqProc2		;error restoring file
10$:	STC
9$:	POP	D2-D4/A2-A3
	RTS

* Called with ptr to filename prefix in A0.
* Displays text describing the operation, then the
* path and then the filename.

DisplayFilePath:
	DispMsg	#RFilNam_LE,#RFilNam_TE,A0,WHITE ;restoring file
	DispCur	Path.,ORANGE,BLUE	;path
	DispCur	FileName.,ORANGE,BLUE 	;file name
	RTS

* Checks for existence of parent and restores it if necessary.
* A0=ptr to parent fib for file being restored.
* Calls itself recursively, if it is necessary to restore several
* levels of parent dirs.  This can go all the way back to the root.
* Returns CY=1 on all sorts of errors.

RestoreParent:
	PUSH	A2-A4
	MOVE.L	A0,A2
	BCLR	#fib_NotBusy,df_Flags(A2) ;is parent dir block already busy?
	BEQ	9$			;yes, better 
	MOVE.L	df_Parent(A2),A3	;else backup another level
	MOVE.L	A3,A0
	BSR.S	RestoreParent		;make sure the parent block is busy
	ERROR	9$			;problem at previous level
	MOVE.L	df_Ownkey(A2),D0	;parent entry is ready for data
	BSR	ReadCtrlBlock		;read parent block
	ERROR	9$			;free parent block looks bad
	MOVE.L	A0,A4
	DIRTY	A0
	MOVE.L	df_Ownkey(A2),D0
	BSR	MarkKeyBusy		;parent block is now busy again
	MOVE.L	HashTblSize,D1		;longwords
	ZAP	D0
	LEA	db_HashTable(A4),A0	;zap the hashtable
	BRA.S	5$
4$:	MOVE.L	D0,(A0)+
5$:	DBF	D1,4$
	ADD.L	BlockSize,A4
	CLR.L	db_HashChain(A4)	;start with no linkage

3$:	LEA	df_Name(A2),A0		;point to name of this dir
	MOVE.L	df_Ownkey(A3),D0	;parent of parent's key
	BSR	OpenFile		;see if dir name already exists
	ERROR	2$			;no...proceed to add new entry
	IFNZB	ArexxFlg,8$
	MOVE.	df_Name(A2),NamBuf
	DispReq	ResDir,ReqNameGad	;ask op for a new name
	IFNZB	CancelFlg,9$,L		;op wants to bail out
	MOVE.	NamBuf,df_Name(A2)
	Z_STG.	NamBuf,db_DirName(A4)	;use this as new name for dir
	BRA.S	3$			;make sure this name is ok

2$:	MOVE.L	df_Ownkey(A2),D0	;new dir
	MOVE.L	df_Ownkey(A3),D1	;parent dir key
	BSR	LinkParentDir		;link parent dir to it's parent dir
	NOERROR	9$
8$:	SETF	CancelFlg
	STC
9$:	POP	A2-A4
	RTS

Fun5Refresh:
	PUSH	D2/A2-A4
	BSR	CLRWIN
	LEA	Rest1.,A0
	BSR	InsertVolName		;insert volume
	DispCent #320,#BSTitle_TE,A0,WHITE
	BSR	RefreshBar
	MOVE.W	#SFC_LE,D0
	MOVE.W	#SFC_TE,D1
	BSR	DispCntTxt
	LEA	PauseGad,A0
	GadColor A0,ORANGE
	LEA	CanGad,A0
	GadColor A0,BLACK
	BSR	UpdateCounts
	BSR	RefreshAllGads
	POP	D2/A2-A4
	RTS

RestoreRefresh:
	PUSH	D2/A2-A4
	BSR	CLRWIN
	LEA	Rest2.,A0
	BSR	InsertVolName		;insert volume
	DispMsg	#RFilNam_LE,#BSTitle_TE,A0,WHITE
	LEA	RestFile.,A0
	BSR	DisplayFilePath
	LEA	PauseGad,A0
	GadColor A0,ORANGE
	LEA	CanGad,A0
	GadColor A0,BLACK
	BSR	RefreshAllGads
	POP	D2/A2-A4
	RTS

* Called with left edge in D0 and top edge in D1.

DispCntTxt:
	PUSH	D2-D3
	MOVE.L	D0,D2
	MOVE.L	D1,D3
	LEA	ActFil.,A0
	BSR.S	DspTxt
	LEA	ActDir.,A0
	BSR.S	DspTxt
	IFZB	FileCntMode,9$
	LEA	DelFil.,A0
	BSR.S	DspTxt
	LEA	DelDir.,A0
	BSR.S	DspTxt
9$:	POP	D2-D3
	RTS
DspTxt:
	DispMsg	D2,D3,A0,WHITE
	ADD.W	#NbrSpacing,D3
	RTS

UpdateCounts:
	MOVE.W	#SFC_LE+136,D0
	MOVE.W	#SFC_TE,D1

* Called with left edge in D0, initial top edge in D1.

UpdateFileCounts:
	PUSH	D2-D3
	MOVE.L	D0,D2
	MOVE.L	D1,D3
	MOVE.L	ActFileCnt,D0
	BSR.S	DispNbr
	MOVE.L	ActDirCnt,D0
	BSR.S	DispNbr
	IFZB	FileCntMode,9$
	MOVE.L	DelFileCnt,D0
	BSR.S	DispNbr
	MOVE.L	DelDirCnt,D0
	BSR.S	DispNbr
9$:	POP	D2-D3
	RTS

DispNbr:
	STR.	L,D0,TMP3.,#32,#6
	STRIP_LB. TMP3.
	DispMsg	D2,D3,TMP3.,ORANGE,BLUE
	ADD.W	#NbrSpacing,D3
	RTS

* Init dir/file lists

InitDirLists:
	MOVE.L	#BaseDirBlock,CurDBBase
	CLR.L	CurDBCnt
	MOVE.L	RootBlkNbr,D0	;root block number
	LEA	Colon.,A0	;volume name ":"
	BSR	BuildRootDir
	ERROR	9$
	MOVE.L	D0,DirTree	;init ptr to root directory list
	MOVEQ	#-1,D0		;dummy root block must never match
	LEA	Dummy.,A0	;dummy-tree
	BSR	BuildRootDir
	ERROR	9$
	MOVE.L	D0,DummyTree
9$:	RTS

* Processes file header or user dir block.  Key in D0 and ptr to block buffer
* in A0.

ProcessControlBlock:
	PUSH	D2-D5/A2-A4
	MOVE.L	A0,A3		;ptr to filehdr/userdir block
	MOVE.L	D0,D2		;control block key
	BSR	CalcCkSum	;valid ADOS control block?
	IFNZL	D0,8$,L		;bad checksum...ignore this block
	MOVE.L	A3,A0
	BSR	VerifyCtrlBlock	;make sure it passes basic integrity tests
	ERROR	8$
	IFEQIL	D0,#1,ProcessUserDir,L
	IFEQIL	D0,#2,ProcessUserDir,L ;user dir, not filehdr
	IFNEIL	D0,#-3,8$,L	;probably root...ignore it
	MOVE.L	D2,D0
	BSR	GetBusyStatus	;is this filehdr marked busy (not deleted)?
	SEQ	D4		;D4=$FF if block is busy
	BNE.S	3$
	INCL	ActFileCnt
	BRA.S	1$
3$:	INCL	DelFileCnt
1$:	MOVE.L	A3,A0
	ADD.L	BlockSize,A0
	MOVE.L	fh_Parent(A0),D3 ;get parent dir block #
	IFEQL	D2,D3,7$	;filehdr claims itself as parent
	MOVE.L	D3,D0
	BSR	SearchDirTree	;parent dir already defined in dirtree?
	ERROR	5$		;no...not previously found
	MOVE.L	D0,A2
	BTST	#fib_Dir,df_Flags(A2) ;is it really a dir?
	BNE.S	2$		;yes

*************************************************************************
* if we get here, we have a new filehdr which claims to have a parent dir
* which is already in the dirtree as a FILE.  The error is probably in the 
* new filehdr, especially if it is deleted, since we only enter a file into
* the dirtree if it is real.
*************************************************************************

7$:	MOVEQ	#-2,D3		;force an illegal parent key

5$:	MOVE.L	D3,D0
	BSR	SearchDummyTree	;else is parent dir defined in dummy list?
	NOERROR	2$		;yes, use it
	MOVE.L	D3,D0
	MOVE.L	D4,D1
	BSR	CreateDummyParent ;else add dummy parent dir
	ERROR	9$
2$:	MOVEA.L D0,A2		;ptr to real or dummy parent dir entry
	MOVE.L	D2,D0		;filehdr ownkey
	BSR	SearchDummyTree	;anybody refer to this filehdr as a parent?
	ERROR	6$		;no...all is well
	MOVE.L	D0,A0
	MOVEQ	#-2,D0		;yes...zap the dummy parent's key
	MOVE.L	D0,df_Ownkey(A0)

6$:	MOVE.L	D2,D0		;file hdr block number
	MOVE.L	A3,A0		;ptr to file hdr block
	MOVE.L	D4,D1		;filehdr busy status
	BSR	BuildFileEntry	;build new file entry
	ERROR	9$
	MOVE.L	D0,A4

	IFNZB	D4,4$		;file active, not selected
	IFZB	NamBuf,10$	;no named file...mark all deleted as selected
	MOVE.L	A3,A0
	ADD.L	BlockSize,A0
	LEA	fh_FileName(A0),A0	;point to name
	LEA	TMP.,A1
	BSR	MoveFileName	;move name into TMP.
	IFNEUC.	NamBuf,TMP.,4$	;no match...leave unselected

;;	IFZL	WildPattern,1$	;no wild card pattern
;;	LEA	fh_FileName(A3),A0	;point to name
;;	BSR	WildMatch	;does this name qualify?
;;	ERROR	6$		;no match...skip this file

10$:	BSET	#fib_Selected,df_Flags(A4)

4$:	MOVEA.L A2,A0		;parent dir entry
	MOVEA.L A4,A1		;new filehdr entry
	BSR	SortInsert	;link into parent dir's list in sorted order
	BRA.S	9$
8$:	STC
9$:	POP	D2-D5/A2-A4
	RTS

* Process user directory block.
* A0=user directory block buffer, D0=block #.

ProcessUserDir:
;	PUSH	D2-D5/A2-A4
;	MOVEA.L A0,A3		;ptr to file hdr block
;	MOVE.L	D0,D2		;dir ownkey based on currentblock
;	BSR	VerifyBlock
;	ERROR	9$
	MOVE.L	D2,D0
	BSR	GetBusyStatus	;is this dir marked busy (not deleted)?
	SEQ	D4		;D4=$FF if block is busy
	BEQ.S	5$
	INCL	DelDirCnt
	BRA.S	6$
5$:	INCL	ActDirCnt
6$:	IFEQL	RootBlkNbr,D2,9$,L ;found root, count it
	MOVE.L	A3,A0
	ADD.L	BlockSize,A0
	MOVE.L	db_Parent(A0),D3 ;parent dir key
	IFEQL	D2,D3,10$	;dir claims to be its own parent
;;	IFGTL	D3,D2,3$	;parent dir hasn't been found yet...
	MOVE.L	D3,D0
	BSR	SearchDirTree	;is parent dir already in dir list?
	ERROR	2$		;no
	MOVE.L	D0,A2
	BTST	#fib_Dir,df_Flags(A2) ;really parent dir?
	BNE.S	1$		;yes, parent dir exists...link this dir to it

*******************************************************************
* if we get here, we have a user dir which claims to have a parent dir
* which is already in the dirtree as a FILE.  The error is probably in the 
* userdir, especially if it is deleted, since we can only get a file entry 
* into the dirtree if we found a filehdr block with that Ownkey.
*******************************************************************

10$:	MOVEQ	#-2,D3		;force illeal parent key

2$:	MOVE.L	D3,D0
	BSR	SearchDummyTree	;is parent dir in dummy dir list?
	NOERROR	1$		;yes, use it
	MOVE.L	D3,D0		;else add parent...parent blk #
	MOVE.L	D4,D1
	BSR.L	CreateDummyParent ;make entry for parent in dummy list
	ERROR	9$
1$:	MOVEA.L D0,A2		;ptr to parent entry
	MOVE.L	D2,D0		;new dir key
	BSR	SearchDirTree	;is this dir already in dir tree?
	ERROR	7$		;no...it shouldn't be...

* The dir block is already in dir list.
* A current block should NEVER be in the dirtree unless it has actually
* been read and processed.  This is a logical problem in Tools code...

	DispErr	Fatal1.		;current dir block already in dir list
	STC
	BRA.S	9$

7$:	MOVE.L	D2,D0
	BSR	SearchDummyTree	;is this dir in dummy dir list?
	ERROR	3$		;no...need to build new dir entry
	MOVE.L	D0,A4
	MOVE.L	A3,A0		;pass ptr to dir block
	MOVE.L	A4,A1		;ptr to entry in dummy list
	MOVE.L	D4,D1		;block busy status
	BSR	UpdateDummyDir	;update dir info in dummy dir entry
	MOVE.L	A4,A0
	BSR	UnLinkDummy	;unlink dummy entry from dummy list
	BRA.S	4$

* normal case...new userdir not already in dir list...add it now

3$:	MOVE.L	A3,A0		;ptr to dir block
	MOVE.L	D2,D0		;dir key
	MOVE.L	D4,D1		;block busy status
	BSR	BuildDirEntry	;build new dir entry for this user dir
	ERROR	9$
	MOVE.L	D0,A4		;save ptr to new dir entry
4$:	MOVEA.L A2,A0		;parent dir entry
	MOVEA.L A4,A1		;ptr to new dir entry
	BSR	SortInsert	;link into parent dir's list in sorted order
	CLC
9$:	POP	D2-D5/A2-A4
	RTS

* Builds a full dir entry for linkage into dir tree.
* Ptr to dir block in A0, block number in D0.
* File status in D1, 1=dir block is busy...dir really exists
* Params: A0, D0.  Returns ptr to the new dir entry buffer in D0.

BuildDirEntry:
	PUSH	D2-D3/A2-A3
	MOVE.L	D0,D2			;block #
	MOVE.L	D1,D3			;real status
	MOVE.L	A0,A3			;dir block
	ADD.L	BlockSize,A3
	IFGEIL	CurDBCnt,#df_SIZEOF,1$	;have enuf room
	BSR	AllocateDirBlock	;else get another block
	ERROR	9$
1$:	MOVE.L	CurDBPtr,A2		;point to new block
	MOVE.L	A2,A1
	ZAP	D0
	MOVE.L	D0,(A1)+		;no linkage yet
	MOVE.L	D0,(A1)+		;no parent yet
	MOVE.L	D0,(A1)+		;no children yet
	MOVE.L	D2,(A1)+		;own block #
	MOVE.B	D0,(A1)+		;unused
	MOVE.B	#$80,D0			;dir, not selected, not restorable
	IFNZL	D3,2$			;dir already exists
	MOVE.B	#$89,D0			;else dir, selected and restorable
2$:	MOVE.B	D0,(A1)+		;store flags
	MOVE.L	D0,(A1)+		;no CurFib
	MOVE.W	D0,(A1)+		;no CurEnt
	MOVE.W	D0,(A1)+		;unused
	LEA	db_DirName(A3),A0
	BSR	MoveFileName		;move name into entry
	BSR	UpdateDBPtr
	MOVE.L	A2,D0
9$:	POP	D2-D3/A2-A3
	RTS

* Builds a full dir entry for root block of tree.
* Ptr to root dir name in A0, block number in D0.
* Params: A0, D0.  Returns ptr to the new dir entry buffer in D0.

BuildRootDir:
	PUSH	D2/A2-A3
	MOVE.L	D0,D2			;block #
	MOVE.L	A0,A3			;ptr to null-term root name
	IFGEIL	CurDBCnt,#df_SIZEOF,1$	;have enuf room
	BSR	AllocateDirBlock	;else get another block
	ERROR	9$
1$:	MOVE.L	CurDBPtr,A2		;point to new block
	MOVE.L	A2,A1
	ZAP	D0
	MOVE.L	D0,(A1)+		;no linkage yet
	MOVE.L	D0,(A1)+		;no parent yet
	MOVE.L	D0,(A1)+		;no children yet
	MOVE.L	D2,(A1)+		;own block #
	MOVE.B	D0,(A1)+		;unused
	MOVE.B	#$80,(A1)+		;dir, not selected, not restorable
	MOVE.L	D0,(A1)+		;no CurFib
	MOVE.W	D0,(A1)+		;no CurEnt
	MOVE.W	D0,(A1)+		;unused
	MOVE.L	A3,A0
	BSR	MoveFileName		;move name into entry
	BSR	UpdateDBPtr
	MOVE.L	A2,D0
9$:	POP	D2/A2-A3
	RTS

* Builds a full file entry for linkage into dir tree.
* Ptr to filehdr block in A0, block number in D0.
* File status in D1, 1=filehdr block is busy...file hasn't been deleted.
* Returns ptr to the new file entry buffer in D0.

BuildFileEntry:
	PUSH	D2-D3/A2-A3
	MOVE.L	D0,D2			;block #
	MOVE.L	D1,D3			;busy status
	MOVE.L	A0,A3			;dir block
	ADD.L	BlockSize,A3
	IFGEIL	CurDBCnt,#df_SIZEOF,1$	;have enuf room
	BSR	AllocateDirBlock	;else get another block
	ERROR	9$
1$:	MOVE.L	CurDBPtr,A2		;point to new block
	MOVE.L	A2,A1
	ZAP	D0
	MOVE.L	D0,(A1)+		;no linkage yet
	MOVE.L	D0,(A1)+		;no parent yet
	MOVE.L	D0,(A1)+		;unused at this time...
	MOVE.L	D2,(A1)+		;own block #
	MOVE.B	D0,(A1)+		;unused
	MOVEQ	#$40,D0			;file, not selected, not restorable
	IFZB	VolValidFlg,3$		;for invalid vol all files restorable
	IFNZL	D3,2$			;file status=filehdr block busy
3$:	MOVE.B	#$48,D0			;filehdr and restorable
2$:	MOVE.B	D0,(A1)+		;flags
	MOVE.L	fh_FileSize(A3),(A1)+	;file size
	LEA	fh_MDate(A3),A0
	MOVE.L	(A0)+,D0
	MOVE.W	D0,(A1)+		;date
	MOVE.L	(A0)+,D0
	MOVE.W	D0,(A1)+		;time
	LEA	fh_FileName(A3),A0
	BSR	MoveFileName		;move name into entry
	BSR	UpdateDBPtr
	MOVE.L	A2,D0
9$:	POP	D2-D3/A2-A3
	RTS

* This routine is called when a file hdr is found which references a parent
* directory which is not yet present in the directory list.
* Creates a dummy directory entry for assumed parent dir block # in D0.
* Param: D0=parent dir block #, D1=deleted status, 1=filehdr child is busy.
* Returns ptr to dummy parent in D0.

CreateDummyParent:
	PUSH	D2-D3/A2
	MOVE.L	D0,D2			;parent dir key
	MOVE.L	D1,D3			;1=busy (not deleted)
	IFGEIL	CurDBCnt,#df_SIZEOF,1$	;have enuf room
	BSR	AllocateDirBlock	;else get another block
	ERROR	9$
1$:	MOVE.L	CurDBPtr,A2		;point to new block
	MOVE.L	A2,A1
	ZAP	D0
	MOVE.L	D0,(A1)+		;no fwd linkage yet
	MOVE.L	D0,(A1)+		;no parent yet
	MOVE.L	D0,(A1)+		;no children yet
	MOVE.L	D2,(A1)+		;block number of this dir
	MOVE.B	D0,(A1)+
	MOVE.B	#$80,D1			;dir, not selected, not restorable
	IFNZL	D3,3$
	MOVE.B	#$89,D1			;dir, selected, restorable
3$:	MOVE.B	D1,(A1)+
	MOVE.L	D0,(A1)+		;no CurFib
	MOVE.L	D0,(A1)+		;no CurEnt
	MOVEQ	#FileNameLen,D1		;clear name buffer + 1 null
2$:	MOVE.B	D0,(A1)+
	DBF	D1,2$
	BSR	UpdateDBPtr
	MOVE.L	A2,A1			;ptr to this new entry
	MOVE.L	DummyTree,A0		;link to this list
	BSR	LinkChild		;link into dummy dir list
	MOVE.L	A2,D0			;returns ptr to new parent
9$:	POP	D2-D3/A2
	RTS

* Called when real dir entry is found, and dummy dir exists.
* Updates dummy entry in A1 from data block in A0.
* File status in D1, 1=dir block is busy...dir really exists

UpdateDummyDir:
	PUSH	A2-A3
	MOVE.L	A0,A3
	MOVE.L	A1,A2
	ADD.L	BlockSize,A3
	LEA	df_Flags(A2),A1		;point to flags field
	MOVE.B	#$80,D0			;dir, NOT selected, not restorable
	IFNZL	D1,1$			;dir is real
	MOVE.B	#$89,D0			;else dir, selected, restorable
1$:	MOVE.B	D0,(A1)			;store flag byte
	LEA	df_Name(A2),A1
	LEA	db_DirName(A3),A0
	BSR	MoveFileName		;move name into entry
	POP	A2-A3
	RTS

* Scans dummy dir tree looking for files which can be restored, but which
* have become "lost" - the parent dir was not found during the scan, so the
* file was never moved into the DirTree.  Also tries to rescue files
* in dummy tree which referenced as parent either themselves or a filehdr
* block (parent key=-2).

CheckDummyTree:
	PUSH	D2-D3/A2-A4
	ZAP	D2			;Lost Drawer count
	ZAP	D3			;lost file count
	MOVE.L	DummyTree,A2
	LEA	df_Child(A2),A0		;start scan here
1$:	MOVE.L	df_Next(A0),A0		;next dummy dir
	IFZL	A0,3$			;end of dummy list
	INCL	D2
	LEA	df_Child(A0),A1		;point to files of that dir
2$:	MOVE.L	df_Next(A1),A1		;next file
	IFZL	A1,1$			;end of list of files, try next dir
	INCL	D3			;count file
	BRA.S	2$
3$:	IFZL	D3,9$,L			;nothing to process
	BldWarn	D3,D2,Lost1.,Lost2.,Lost3.,Lost4.
	DispReq	A0,ReqProceed		;restore lost files to root?
;;	IFZB	YesFlg,9$		;op says NO!

	MOVE.L	DirTree,A2		;point to DirTree root
	MOVE.L	DummyTree,A3		;"root" of dummy list
	LEA	df_Child(A3),A3
4$:	MOVE.L	df_Next(A3),A3		;point to next dummy dir in list
	IFZL	A3,9$			;end of list...all done
	LEA	df_Child(A3),A4
5$:	MOVE.L	df_Next(A4),A1
	IFZL	A1,4$
	MOVE.L	df_Next(A1),(A4)	;unlink item
;;	CLR.L	df_Next(A1)		;no fwd link...just in case
	MOVEA.L A2,A0			;parent dir entry
	BSET	#fib_NewParent,df_Flags(A1) ;needs parent key updated
	BSR	SortInsert		;link into parent dir's list in sorted order
	BRA.S	5$			;loop for next entry

9$:	POP	D2-D3/A2-A4
	RTS

* Assigns a name for the new drawer, builds an empty dir block in buffer
* in A0, finds a free key for the drawer, and writes the block data to
* that key.  Returns newly assigned key for new drawer in D0.
***************unused****************

BuildLostDrawer:
	RTS

* Inserts new entry into chain at proper place, depending upon name.  Dirs
* sort ahead of files.  A0 points to parent dir.  New dir or file entry in A1.
* Chain is maintained in sorted order, so linear search is sufficient.

SortInsert:
	PUSH	D2/A2-A3
	MOVE.L	A1,A2			;ptr to new entry
	CLR.L	df_Next(A2)		;make sure no extraneous fwd link
	MOVE.L	A0,df_Parent(A2)	;set up link to parent
	LEA	df_Child(A0),A3		;point to "head" of chain
	MOVE.L	df_Next(A3),A1		;get ptr to first entry
	BTST	#fib_Dir,df_Flags(A2)	;is new entry a dir?
	BEQ.S	2$			;no...a file, skip over dirs
1$:	IFZL	A1,5$			;end of chain...add new entry to end
	BTST	#fib_Dir,df_Flags(A1)	;is this entry a dir?
	BEQ.S	4$			;no, a file...new dir goes in front
	MOVE.L	A2,A0
	BSR.S	CompareNames		;check sort order
	BCS.S	4$			;put new entry in front of this entry
	MOVE.L	A1,A3			;else advance to next entry
	MOVE.L	df_Next(A3),A1
	BRA.S	1$

2$:	IFZL	A1,5$			;end of chain...add new entry to end
	BTST	#fib_Dir,df_Flags(A1)	;is this entry a dir?
	BNE.S	3$			;yes...files sort after dirs
	MOVE.L	A2,A0
	BSR.S	CompareNames		;else check sort order
	BCS.S	4$			;put new entry in front of this entry
3$:	MOVE.L	A1,A3			;else advance to next entry
	MOVE.L	df_Next(A3),A1
	BRA.S	2$

4$:	MOVE.L	A1,df_Next(A2)		;new one points to old one
5$:	MOVE.L	A2,df_Next(A3)		;and correct previous linkage
	POP	D2/A2-A3
	RTS

* Compares names of new entry (A0) to current entry (A1).  Returns
* CY=1 if new item sorts ahad of current item.  CY=0 otherwise.

CompareNames:
	PUSH	A0/A1
	LEA	df_Name(A0),A0		;name of new entry
	LEA	df_Name(A1),A1		;name to this entry
1$:	MOVE.B	(A0)+,D0		;get a byte of new name
	BEQ.S	9$			;new item ended...sort ahead
	UCASE	D0
	MOVE.B	(A1)+,D1		;get a byte of current name
	UCASE	D1
	IFEQB	D1,D0,1$		;loop if equal
	POP	A0/A1			;else return with CY set properly
	RTS
9$:	POP	A0/A1
	STC				;short return
	RTS

* Links new dir entry in A1 to front of child list for parent dir in A0.

LinkChild:
	MOVE.L	A2,-(A7)
	MOVEA.L df_Child(A0),A2		;get ptr to current first dir in list
	MOVE.L	A1,df_Child(A0)		;new entry is new first dir in list
	MOVE.L	A2,df_Next(A1)		;fwd link for new entry
	MOVE.L	A0,df_Parent(A1)
	MOVEA.L (A7)+,A2
	RTS

* Called to search top level of dummy tree and remove entry in A0 from it.
* Entry is known to exist at top level of dummy tree.

UnLinkDummy:
	PUSH	A2
	MOVE.L	DummyTree,A2
	LEA	df_Child(A2),A2
1$:	MOVE.L	df_Next(A2),A1		;get fwd link
	IFZL	A1,9$			;oops...unlink failed
	IFEQL	A0,A1,2$		;this entry links to unlink entry
	MOVE.L	A1,A2			;else advance to next entry
	BRA.S	1$
2$:	MOVE.L	df_Next(A0),df_Next(A2)	;fix up linkage
9$:	POP	A2
	RTS

* Given a block number in D0, searches selected list for matching entry.  
* Returns ptr in D0 or 0 if no match.  Routine automatically handles 
* directory tree structure by recursively calling itself.

SearchDummyTree:
	MOVE.L	DummyTree,A0
	BRA.S	SearchLevel

SearchDirTree:
	MOVE.L	DirTree,A0

* Params:  A0=dir tree to search, D0=desired dir block #
* Returns ptr to matching entry in D0.

SearchLevel:
	PUSH	D2/A2
	MOVE.L	D0,D2			;desired dir blk number
	MOVE.L	A0,A2			;ptr to root of this tree
	BRA.S	4$

1$:	IFNEL	df_Ownkey(A2),D2,2$	;no match
	MOVE.L	A2,D0			;yes...return matching dir entry
	BRA.S	9$

2$:	BTST	#fib_Dir,df_Flags(A2)	;dir entry?
	BEQ.S	3$			;not a dir entry...try next
	MOVE.L	df_Child(A2),A0		;get link to lower level
	IFZL	A0,3$			;no lower level
	MOVE.L	D2,D0			;else search lower level
	BSR.S	SearchLevel		;recursive call to self
	NOERROR	9$

3$:	MOVE.L df_Next(A2),A2		;link to next dir at this level
4$:	IFNZL	A2,1$			;loop if not end of items at this level
5$:	STC				;no match
9$:	POP	D2/A2
	RTS

* Allocates memory for directory blocks.

AllocateDirBlock:
	MOVE.L	#DirBlockSize,D0
	ZAP	D1			;any kind of memory
	PUSH	A6
	CALLSYS	AllocMem,SysBase
	POP	A6
	MOVE.L	CurDBBase,A0		;ptr to base of current dir block
	MOVE.L	D0,(A0)			;set up link to new dir block
	BEQ.S	9$			;oops...no memory...bad awful
	INCW	DirBlkCnt		;one more
	MOVE.L	D0,A0
	MOVE.L	A0,CurDBBase
	CLR.L	(A0)+			;no fwd link yet...
	MOVE.L	A0,CurDBPtr		;load dir block here
	MOVE.L	#DirBlockSize-4,CurDBCnt
	RTS
9$:	DispErr	NoMemErr.
	STC
	RTS

FreeDirBlocks:
	MOVE.L	BaseDirBlock,A2		;free the chain of dir blocks
1$:	IFZL	A2,2$			;nothing left in chain
	MOVE.L	A2,A1
	MOVE.L	(A2),A2			;pick up fwd link BEFORE releasing...
	MOVE.L	#DirBlockSize,D0
	CALLSYS	FreeMem,SysBase
	DECW	DirBlkCnt
	BRA.S	1$
2$:	MOVE.L	A2,BaseDirBlock		;show chain released
;	IFZW	DirBlkCnt,9$
;	DispErr	DirBlk.			;dir blocks not all released
9$:	CLR.L	DirTree			;no dir tree
	CLR.L	DummyTree
	RTS

* Moves BSTR filename string A0 into buffer A1 up to 30 chars.
* Inserts null if limit reached.

MoveFileName:
	MOVEQ	#FileNameLen,D1
	ZAP	D0
	IFZL	A0,3$
	MOVE.B	(A0)+,D0		;get length from file hdr
	BEQ.S	3$
	IFLEW	D0,D1,1$		;use actual
	MOVE.W	D1,D0			;else limit move to 30 chars
	BRA.S	1$
2$:	MOVE.B	(A0)+,(A1)+
1$:	DBEQ	D0,2$
	BEQ.S	9$			;string ended ok
3$:	CLR.B	(A1)+			;put in a null if limit reached
9$:	RTS

UpdateDBPtr:
	MOVE.L	A1,D0
	INCL	D0
	ANDI.L	#$FFFFFFFE,D0
	MOVE.L	D0,D1
	SUB.L	A2,D1			;get size of this FIB
	MOVE.L	D0,CurDBPtr		;align ptr properly
	SUB.L	D1,CurDBCnt
	RTS

DupGadHit:
NameGadHit:
	RTS

RestCurrent:
	CLR.B	CopyFlg
	BRA.S	RestCom

RestDifferent:
	SETF	CopyFlg
RestCom:
	SETF	ProcFlg
	RTS


* These strings MUST be BSTR format (leading length)

Colon.	DC.B	1,':',0
Dummy.	DC.B	10,'Dummy-tree',0

	BSS,PUBLIC

DirBlkCnt	DS.W	1	;count of blocks
ActFileCnt	DS.L	1	;count of real files found
DelFileCnt	DS.L	1	;count of deleted files found
ActDirCnt	DS.L	1	;count of real drawers found
DelDirCnt	DS.L	1	;count of deleted drawers found
DirTree		DS.L	1	;ptr to root level of real dir tree
DummyTree	DS.L	1	;ptr to root level of dummy dir tree
BaseDirBlock	DS.L	1	;points to first allocated dir block
CurDBBase	DS.L	1	;base of current dir block
CurDBPtr	DS.L	1	;dir block ptr
CurDBCnt	DS.L	1	;count of remaining bytes in dir block
FileName.	DS.L	16	;resored file name
CopyFlg		DS.B	1	;0=restore, 1=copy to another volume
FileCntMode	DS.B	1	;0=active only, 1=active+deleted
NoBitmapFlg	DS.B	1	;0=got bitmap, 1=can't use bitmap

	END
