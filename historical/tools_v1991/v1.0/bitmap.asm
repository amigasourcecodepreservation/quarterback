;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*****************************************************************
*								*
* 		BITMAP.ASM					*									*
*	Quarterback Tools Bitmap Functions			*
*	Copyright (c) 1990 Central Coast Software		*
*								*
*****************************************************************

	INCLUDE "vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"VD0:BOXES.ASM"

	XDEF	ReadBitmap
	XDEF	WriteBitmap
	XDEF	InitBitmap
	XDEF	FreeBMBuffers
	XDEF	MarkKeyFree,MarkKeyBusy,GetBusyStatus
	XDEF	MarkAltFree,MarkAltBusy,GetAltStatus
	XDEF	MarkBadFree,MarkBadBusy,GetBadStatus
	XDEF	AllocFreeKey
	XDEF	AllocateEmptyBitmap
	XDEF	AllocateAltBitmap
	XDEF	AllocateBadBitmap
	XDEF	LoadBitmapKeys
	XDEF	StoreBitmapKeys
	XDEF	RepositionBitmap
	XDEF	UpdateBitmapKeys
;;	XDEF	FindBitmapKey
	XDEF	CountFreeKeys
	XDEF	InitBitmapBuffer

	XDEF	BitmapFlg
	XDEF	BMDirtyFlg
	XDEF	BitmapDataPtr
	XDEF	BitmapBuffer
	XDEF	BitmapKeyBuffer
	XDEF	BitmapBufSize
	XDEF	FreeKeys
	XDEF	LastUsedKey

* Externals in TOOLS.ASM

	XREF	SysBase

* Externals in FILES.ASM

	XREF	ReadRoot
	XREF	ZapBlock

* Externals in Device.asm

	XREF	NumSoftErrors
	XREF	UnitNumber
	XREF	DiskState
	XREF	NumBlocks
	XREF	NumBlocksUsed
	XREF	BytesPerBlock
	XREF	DiskType
	XREF	VolumeNode
	XREF	InUse	
	XREF	MaxBytes
	XREF	MaxBlocks
	XREF	RootBlkNbr
	XREF	FreeBytes
	XREF	FreeBlocks
	XREF	VolumeOffset
	XREF	LowCyl
	XREF	HighCyl
	XREF	MaxCylinders
	XREF	Surfaces
	XREF	MaxTracks
	XREF	BlockSize,BlockSizeL
	XREF	BlkSizeOffset
	XREF	BlkSizeShift,BlkLongShift
	XREF	HashTblSize
	XREF	Reserved,PreAlloc
	XREF	BlocksPerTrack
	XREF	NumBuffers
	XREF	BufMemType
	XREF	MaxTransfer
	XREF	PercentFull
	XREF	DosType
	XREF	FFSFlg
	XREF	DriverNameBuf.
	XREF	RootBlkNbr
	XREF	BitmapBlkBits
	XREF	BitmapBlocks,BMExtBlocks
	XREF	HighestKey,LowestKey
	XREF	MaxKeys
	XREF	VolValidFlg

* Externals in Messages.asm

	XREF	BMInvalid.
	XREF	NoMemErr.
	XREF	BMRdErr.,BMWrtErr.
	XREF	BMInvalid.
;;	XREF	BMCkSum.
;;	XREF	BMCErr.
;;	XREF	BMPErr.

* Externals in DISK.ASM

	XREF	ReadBlock,WriteBlock
	XREF	MotorOff

* Externals in BUFFERS.ASM

	XREF	GetCacheBuffer
	XREF	ReadControl,ReadData
	XREF	AllocateBuffer
	XREF	AllocateIOBuf

* Externals in REORG.ASM

	XREF	LowestCtrlKey,LowestFreeKey

* Externals in MOVE.ASM

	XREF	RelocateBlock
	XREF	RemapKey

* Externals in AREXX.ASM

	XREF	ArexxFlg

***************************************************************************************
* The code here really is the only code which limits the maximum volume size.  The
* number of bitmap extensions is the limiting factor (and, possibly, memory limits).
* Currently, the code supports volumes up to 1.1 GB.
***************************************************************************************

* Allocates a single free key from bitmap.  Returns key in D0 or CY=1.

AllocFreeKey:
	PUSH	D2-D4
	IFNZL	FreeKeys,6$
	STC
	BRA.S	9$
6$:	MOVE.L	LastUsedKey,D2
	MOVEQ	#31,D3
	MOVE.L	HighestKey,D4
1$:	MOVE.L	D2,D0		;try this key
	SUB.L	LowestKey,D0
	MOVE.L	D3,D1
	AND.L	D0,D1		;bit in longword
	LSR.L	#5,D0		;divide by 32 to get longword index
	LSL.L	#2,D0		;convert to longword access
	MOVE.L	BitmapDataPtr,A0
	ADD.L	D0,A0
5$:	MOVE.L	(A0),D0
2$:	BCLR	D1,D0		;find a free key?
	BNE.S	4$		;yes
	INCL	D2		;advance to next key
	IFLEL	D2,D4,3$	;high end of volume?
	MOVE.L	LowestKey,D2	;yes, reset to low end
	BRA.S	1$
3$:	INCL	D1		;advance to next bit
	IFLEL	D1,D3,2$	;end of longword?
	ZAP	D1
	ADDQ.L	#4,A0		;yes, advance to next longword
	BRA.S	5$
4$:	MOVE.L	D0,(A0)		;store updated bitmap data
	MOVE.L	D2,D0		;allocated key
	MOVE.L	D0,LastUsedKey
	SETF	BMDirtyFlg
	DECL	FreeKeys
9$:	POP	D2-D4
	RTS

* Given Key in D0, returns Key busy status in Z bit.
* Z=1 if Key is marked busy, Z=0 if not busy.

GetBadStatus:
	MOVE.L	BitmapBadPtr,A0
	BRA.S	GBS

GetAltStatus:
	MOVE.L	BitmapAltPtr,A0
	BRA.S	GBS

GetBusyStatus:
	MOVE.L	BitmapDataPtr,A0
GBS:	IFLTL	HighestKey,D0,8$
	SUB.L	LowestKey,D0
	BMI.S	8$
	MOVEQ	#$1F,D1
	AND.L	D0,D1		;bit in longword
	LSR.L	#5,D0		;divide by 32 to get longword index
	LSL.L	#2,D0		;convert to longword access
	MOVE.L	0(A0,D0.L),D0
	BTST	D1,D0
8$:	RTS

* Called with key to mark busy in D0.  Returns Z=previous status of Key.
* Z=1 if Key was already marked busy, Z=0 if not busy.

MarkBadBusy:
	MOVE.L	BitmapBadPtr,A0
	BRA.S	MKB

MarkAltBusy:
	MOVE.L	BitmapAltPtr,A0
	BRA.S	MKB

MarkKeyBusy:
	MOVE.L	BitmapDataPtr,A0
MKB:	IFLTL	HighestKey,D0,8$	;don't write outside bitmap buffer
	SUB.L	LowestKey,D0		;range check the key
	BMI.S	8$
	SETF	BMDirtyFlg		;bitmap is no longer clean
	MOVEQ	#$1F,D1
	AND.L	D0,D1			;bit in longword
	LSR.L	#5,D0			;divide by 32 to get longword index
	LSL.L	#2,D0			;convert to longword access
	ADD.L	D0,A0			;index into bitmap buffer
	MOVE.L	(A0),D0			;get longword data
	BCLR	D1,D0			;mark it busy (bit clear)
	MOVEM.L	D0,(A0)			;save result without destroying Z-bit
8$:	RTS

* Called with key to mark free in D0.  Returns Z=previous status of Key.
* Z=1 if Key was previously marked busy, Z=0 if not busy.

MarkBadFree:
	MOVE.L	BitmapBadPtr,A0
	BRA.S	MKF

MarkAltFree:
	MOVE.L	BitmapAltPtr,A0
	BRA.S	MKF

MarkKeyFree:
	MOVE.L	BitmapDataPtr,A0
MKF:	IFLTL	HighestKey,D0,8$
	SUB.L	LowestKey,D0
	BMI.S	8$
	SETF	BMDirtyFlg
	MOVEQ	#$1F,D1
	AND.L	D0,D1		;bit in longword
	LSR.L	#5,D0		;divide by 32 to get longword index
	LSL.L	#2,D0		;convert to longword access
	ADD.L	D0,A0
	MOVE.L	(A0),D0		;get bitmap data
	BSET	D1,D0		;key is free
	MOVEM.L	D0,(A0)		;save result without destroying Z-bit
8$:	RTS

************************************************************************************
* Reads root block, figures out how big the bitmap will be, allocates
* a buffer of the required size, and reads the bitmap into the buffer.  
* NOTE: this routine (and the WriteBitmap routine) may have a problem
* with the OFS.  The documentation is not clear on whether there are 25 or
* 26 entries in the bitmap table for the OFS.  The 26th entry for the FFS
* is the bitmap extension field.  That is how it is treated here.

ReadBitmap:
	IFNZB	BitmapFlg,9$		;already have bitmap loaded
	BSR	FreeBMBuffers		;release any prior bitmap buffer
	BSR	AllocateBMBuffers	;allocate bitmap buffer
	ERROR	7$			;not enough memory
	BSR.S	LoadBitmapKeys
	ERROR	7$
	BSR	LoadBitmapData
	ERROR	7$
	BSR	VerifyBitmap
	NOERROR	9$
	DispErr	BMInvalid.
	BRA.S	8$
7$:	DispErr	BMRdErr.
8$:	STC
9$:	RTS

* Builds list of bitmap keys from root and any bitmap extension blocks.  NOTE:
* this code uses a dirty little stunt to mark BitmapExtBlk keys, which are stored
* in the BitmapKeyBuffer along with bitmap block keys.  The high bit it SET to
* mark extension block keys.  This way they are allocated and stored along with
* the bitmap block keys, providing an essentially unlimited volume size capability.
* Using the high bit this way limits us to about 2 billion bitmap block keys...(sigh).

LoadBitmapKeys:
	PUSH	D2-D3/A2-A3
	ZAP	D0			;minimal checking
	BSR	ReadRoot
	ERROR	8$
	MOVE.L	A0,A2
	ADD.L	BlockSize,A2
	IFNZL	rb_BMFlg(A2),1$		;bitmap is valid
	DispErr	BMInvalid.		;bad bitmap
;;	BRA	8$

1$:	MOVE.L	BitmapKeyBuffer,A3
	MOVE.L	BitmapBlocks,D2
	ADD.L	BMExtBlocks,D2		;max keys to deal with
	MOVE.L	D2,D3
	LEA	rb_BMPtrs(A2),A2	;point to bitmap keys
	MOVEQ	#26,D1			;root has 25 keys + extension key max
	BRA.S	4$

2$:	MOVE.L	(A2)+,D0		;get a bitmap key
	BEQ.S	3$			;end of list...
	MOVE.L	D0,(A3)+		;save into list
	DECL	D2			;all done?
	BEQ.S	3$			;limit scan, just in case
4$:	DBF	D1,2$			;loop for all bitmap keys
	BSET	#7,-4(A3)		;mark the key as an extension key
	BSR	ReadData
	ERROR	8$
	MOVE.L	A0,A2
	MOVE.L	BlockSizeL,D1		;this many keys+ext in block
	BRA.S	4$

3$:	MOVE.L	A3,D0			;how many keys did we find?
	SUB.L	BitmapKeyBuffer,D0
	LSR.L	#2,D0
	IFEQL	D3,D0,9$		;all is well
8$:	STC				;else bitmap corrupted
9$:	POP	D2-D3/A2-A3
	RTS

* Reads bitmap data into contiguous buffer, checks and removes checksums.
* Result is one big contiguous bitmap.  Great for fast access.  Loads in
* reverse order to allow overwriting checksum in first longword.

LoadBitmapData:
	PUSH	D2-D3/A2-A3
	MOVE.L	BitmapBlocks,D2
	MOVE.L	BlockSize,D3
	MOVE.L	BitmapBuffer,A2
	ADD.L	BitmapBufSize,A2	;point to end of bitmap data
	MOVE.L	BitmapKeyBuffer,A3
	ADD.L	BitmapKeyBufSize,A3	;point to end of list of keys
	BRA.S	2$
1$:	MOVE.L	-(A3),D0		;get bitmap key
	BEQ.S	8$			;something clobbered key list...
	BMI.S	1$			;extension key...skip it
	SUB.L	D3,A2			;backup by one block
	MOVE.L	A2,A0
	BSR.S	ReadBMBlk		;read bitmap block and check cksum
	ERROR	8$			;oops...
	ADDQ.L	#4,A2			;overwrite checksum
2$:	DBF	D2,1$

	MOVE.L	A2,BitmapDataPtr	;actual bitmap data starts here
	SETF	BitmapFlg		;got a valid bitmap
	CLR.B	BMDirtyFlg		;no need to rewrite bitmap...yet
;;	BSR	MotorOff
	BRA.S	9$			;ok...got it
8$:;;	DispErr	BMRdErr.
	STC
9$:	POP	D2-D3/A2-A3
	RTS

ReadBMBlk:
	PUSH	A2
	MOVE.L	A0,A2
	BSR	ReadBlock
	ERROR	8$
	ZAP	D0
	MOVE.L	BlockSizeL,D1
	BRA.S	2$
1$:	ADD.L	(A2)+,D0
2$:	DBF	D1,1$
	BEQ.S	9$
8$:	STC
	POP	A2
	RTS
9$:	CLC
	POP	A2
	RTS

VerifyBitmap:
	PUSH	D2/A2
	MOVE.L	RootBlkNbr,D0
	BSR	MarkKeyBusy
	BNE.S	8$			;root not busy
	MOVE.L	BitmapBlocks,D2
	MOVE.L	BitmapKeyBuffer,A2
	BRA.S	2$
1$:	MOVE.L	(A2)+,D0		;get bitmap key
	BEQ.S	8$			;something clobbered key list...
	BCLR	#31,D0
	BSR	MarkKeyBusy
	BNE.S	8$
2$:	DBF	D2,1$
	BRA.S	9$
8$:	STC
9$:	POP	D2/A2
	RTS

* Writes existing bitmap buffers back to volume.  Note: bitmap blocks are
* rewritten into their previous keys.  They ARE NOT moved to different
* keys, as old ADOS does.

WriteBitmap:
	PUSH	D2-D3/A2-A3
	MOVE.L	BitmapBlocks,D2
	MOVE.L	BlockSize,D3
	MOVE.L	BitmapDataPtr,A2
	MOVE.L	BitmapKeyBuffer,A3
	BRA.S	2$
1$:	MOVE.L	(A3)+,D0		;get bitmap key
	BEQ.S	8$			;something clobbered key list...
	BMI.S	1$			;extension key
	SUBQ.L	#4,A2			;backup for checksum
	MOVE.L	A2,A0
	BSR.S	WriteBMBlk		;calc checksum and write to disk
	ERROR	8$			;oops...
	ADD.L	D3,A2			;advance to next bitmap data block
2$:	DBF	D2,1$
	BRA.S	9$

8$:	DispErr	BMWrtErr.
	CLR.B	BitmapFlg		;bitmap not valid
	STC
9$:	CLR.B	BMDirtyFlg
	POP	D2-D3/A2-A3
	RTS

* Calculates and stores a new checksum for bitmap block in A0, then writes
* block to key in D0.

WriteBMBlk:
	PUSH	D2
	MOVE.L	A0,A1			;save ptr
	ZAP	D2
	MOVE.L	BlockSizeL,D1
	BRA.S	2$
1$:	ADD.L	(A1)+,D2
2$:	DBF	D1,1$
	SUB.L	D2,(A0)			;store new checksum
	BSR	WriteBlock
	POP	D2
	RTS

* Initializes a virgin bitmap in memory, and assigns bitmap block keys, 
* which it loads into the root buffer and any bitmap extension buffers, 
* if necessary.  Note: only bitmap ext buffers are directly written to disk, 
* since they are NOT written as part of WriteBitmap.
* Note: if volume is valid, we reuse existing keys.

InitBitmap:
	BSR	AllocateEmptyBitmap	;allocate and init bitmap buffer
	MOVE.L	RootBlkNbr,D0
	MOVE.L	D0,LastUsedKey		;start from root block
	BSR	MarkKeyBusy		;root is busy
;;	BSR.S	AssignBitmapKeys
;;	RTS

AssignBitmapKeys:
	PUSH	D2-D4/A2/A4
	MOVE.L	BitmapBlocks,D3		;need this many keys, total
	MOVE.L	BitmapKeyBuffer,A2
	MOVEQ	#26,D1			;max bitmap keys+ext key in root
1$:	MOVE.L	D3,D0
	BSR	StoreBitmapKeys		;allocate bitmap keys
	MOVE.L	D0,D3
	BEQ.S	2$			;finished allocating keys
	BSR	AllocFreeKey		;else get key for first ext
	BSET	#31,D0			;mark it as an extension key
	MOVE.L	D0,(A2)+		;store bitmap ext key
	MOVE.L	BlockSizeL,D1
	BRA.S	1$

2$:	MOVE.L	RootBlkNbr,D0		;copy keys into buffers
	BSR	ReadControl		;get root block
	ERROR	9$
	DIRTY	A0			;make sure root gets rewritten
	ADD.L	BlockSize,A0
	LEA	rb_BMPtrs(A0),A0
	MOVEQ	#25,D4			;root can hold this many keys
	ZAP	D2
	MOVE.L	BitmapBlocks,D3
	MOVE.L	BitmapKeyBuffer,A2
	BRA.S	4$
3$:	MOVE.L	(A2)+,(A0)+		;store a key
	DECL	D3			;end of keys?
	BEQ.S	5$			;yes
4$:	DBF	D4,3$			;loop if not end of buffer area
5$:	IFZL	D3,6$			;no more keys...no ext link needed
	MOVE.L	(A2)+,D0		;get ext key previously allocated
	BCLR	#31,D0			;get rid of extension key flag bit
	MOVE.L	D0,(A0)			;store ext key
	MOVE.L	D0,D4			;save ext key
6$:	MOVE.L	D2,D0			;key of previous bitmap ext, if any
	BEQ.S	7$			;none...it was root we just completed
	MOVE.L	A4,A0			;else queue extension block
	BSR	WriteBlock		;bitmap ext has no checksum
7$:	IFZL	D3,9$			;all done...
	MOVE.L	D4,D2			;else use key of ext block
	BSR	GetCacheBuffer		;get buffer for next bitmap ext
	ERROR	9$
	DIRTY	A0
	MOVE.L	A0,A4			;ptr to ext buffer
	BSR	ZapBlock		;make sure it is clean
	MOVE.L	A4,A0
	MOVE.L	BlockSizeL,D4
	DECL	D4			;leave room for possible ext
	BRA.S	4$
9$:	POP	D2-D4/A2/A4
	RTS

* D0=remaining count needed, D1=limit for this batch, A2=BitmapKeyBuffer
* Returns updated remaining count in D0 and updated A2.

StoreBitmapKeys:
	PUSH	D2-D3
	MOVE.L	D0,D3			;store this many keys
	MOVE.L	D0,D2			;limit for this batch
	DECL	D1			;allow room for ext key, if any
	IFLEL	D0,D1,3$		;need fewer than limit
	MOVE.L	D1,D2			;else limit to max of this block
3$:	SUB.L	D2,D3			;calculate remaining count
	BRA.S	2$
1$:	BSR	AllocFreeKey
	MOVE.L	D0,(A2)+		;store into key list
2$:	DBF	D2,1$
	MOVE.L	D3,D0
	POP	D2-D3
	RTS

* Allocates an empty bitmap buffer of proper size.  Note that bitmap
* is one large contiguous data area, with proper number of keys marked
* free.

AllocateBadBitmap:
	MOVE.L	BitmapBufSize,D0
	BSR	AllocateBuffer		;get buffer
	ERROR	AllocBMEx
	MOVE.L	D0,BitmapBadBuffer
	ADDQ.L	#4,D0
	MOVE.L	D0,A0
	MOVE.L	A0,BitmapBadPtr
	BRA.S	AllocBMCom

AllocateAltBitmap:
	MOVE.L	BitmapBufSize,D0
	BSR	AllocateBuffer		;get buffer
	ERROR	AllocBMEx
	MOVE.L	D0,BitmapAltBuffer
	ADDQ.L	#4,D0
	MOVE.L	D0,A0
	MOVE.L	A0,BitmapAltPtr
	BRA.S	AllocBMCom

AllocateEmptyBitmap:
	BSR	FreeBMBuffers		;release any previous buffers
	BSR	AllocateBMBuffers	;allocate fresh bitmap buffers
	ERROR	AllocBMEx
	MOVE.L	MaxKeys,FreeKeys
	MOVE.L	BitmapBuffer,A0		;point to bitmap data buffer
	ADDQ.L	#4,A0			;allow for initial checksum
	MOVE.L	A0,BitmapDataPtr
AllocBMCom:
;	BSR.S	InitBitmapBuffer

* Ptr to buffer to be initialized in A0.

InitBitmapBuffer:
	PUSH	D2
	MOVE.L	MaxKeys,D2
;;	SUB.L	PreAlloc,D2
	MOVEQ	#32,D1
	MOVEQ	#-1,D0
1$:	IFLTL	D2,D1,2$		;fewer than 32 keys left
	MOVE.L	D0,(A0)+		;else mark 32 Keys "free"
	SUB.L	D1,D2
	BNE.S	1$			;more keys to do
	BRA.S	9$			;ended on longword boundary
2$:	MOVE.L	(A0),D0			;get last longword
	ZAP	D1			;start with bit 0
3$:	BSET	D1,D0			;mark one key free
	INCL	D1			;advance to next
	DECL	D2			;count it
	BNE.S	3$			;loop for remainder
	MOVE.L	D0,(A0)			;store final longword
9$:
AllocBMEx:
	POP	D2
	RTS

* Relocates bitmap blocks to the control area immediately following the root.
* Marks old bitmap keys free, unless corresponding new key is busy, in which case
* contents of new key is relocated to old key.

RepositionBitmap:
	PUSH	D2-D4/A2
	MOVE.L	RootBlkNbr,D4
	INCL	D4
	MOVE.L	BitmapKeyBuffer,A2
	MOVE.L	BitmapBlocks,D2		;number of blocks to worry with
	BRA.S	3$
1$:	MOVE.L	(A2)+,D0		;get old bitmap key
	BEQ.S	2$
	BCLR	#31,D0			;in case this is a bitmap ext key
	BSR	RemapKey		;just in case
	MOVE.L	D0,D3
	BSR	MarkKeyFree		;release the old key
	IFEQL	D3,D4,2$		;already in right place
	MOVE.L	D4,D0
	BSR	GetBusyStatus		;new key currently busy?
	BNE.S	2$			;no, no need to relocate
	MOVE.L	D4,D0			;relocate contents of this key
	MOVE.L	D3,D1			;to this key
	BSR	RelocateBlock		;move old contents, if any
	ERROR	9$
	MOVE.L	D3,D0
	BSR	MarkKeyBusy		;old key now busy with relocated data
2$:	INCL	D4
3$:	DBF	D2,1$
	MOVE.L	D4,D0			;return updated new key
9$:	POP	D2-D4/A2
	RTS


* Resets bitmap block keys to area immediately following root node.  The bitmap
* isn't actualy written.  That comes at the end of the operation.

UpdateBitmapKeys:
	PUSH	D2-D3
	MOVE.L	BMExtBlocks,D3
	ADD.L	BitmapBlocks,D3
	MOVE.L	RootBlkNbr,D2
	INCL	D2
	BRA.S	4$
3$:	MOVE.L	D2,D0
	BSR	MarkKeyFree
	INCL	D2
4$:	DBF	D3,3$
	MOVE.L	RootBlkNbr,LastUsedKey
	BSR	AssignBitmapKeys	;now assign new keys and write exts
;;	ERROR	9$
;;	MOVE.L	LowestCtrlKey,D2
;;1$:	IFLEL	LowestFreeKey,D2,2$	;far enough
;;	MOVE.L	D2,D0
;;	BSR	MarkKeyBusy		;mark control key busy
;;	INCL	D2
;;	BRA.S	1$
;;2$:	CLC
9$:	POP	D2-D3
	RTS

* Marks old bitmap keys free so they can be allocated again...

;FreeBitmapKeys:
;	PUSH	D2/A2
;	MOVE.L	A0,A2			;pointer to list of bitmap keys
;	MOVE.L	D1,D2			;bitmap key count
;	BRA.S	2$
;1$:	MOVE.L	(A2)+,D0		;get old bitmap key
;	BEQ.S	2$
;	BSR	MarkKeyFree
;2$:	DBF	D2,1$
;	POP	D2/A2
;	RTS

* Given key in D0, looks up corresponding OLD key where contents of bitmap key 
* may hae been relocated.  BitmapExtBlks, if they exist, are positioned in the 
* middle of the sequence of bitmap blocks.

;FindBitmapKey:
;	IFGEL	RootBlkNbr,D0,8$	;lower than root
;	IFLEL	LowestCtrlKey,D0,8$	;higher than bitmap blocks
;	SUB.L	RootBlkNbr,D0
;	DECL	D0
;	MOVE.L	BitmapKeyBuffer,A0
;	LSL.L	#2,D0
;	MOVE.L	0(A0,D0.L),D0		;remap to old key
;	BCLR	#31,D0			;just in case...
;	BRA.S	9$
;8$:	STC
;9$:	RTS

* Counts actual free keys in bitmap.

CountFreeKeys:
	PUSH	D2-D7
	MOVE.L	MaxKeys,D2		;process this many keys
	MOVE.L	D2,D3
	LSR.L	#5,D3			;longword count
	ZAP	D4			;total free count
	MOVEQ	#-1,D5			;"all free" check
	MOVEQ	#32,D6
	MOVEQ	#31,D7
	MOVE.L	BitmapDataPtr,A0	;point to the bitmap data
	BRA.S	4$
1$:	MOVE.L	(A0)+,D0		;any keys free in this longword?
	BEQ.S	4$			;no...
	CMP.L	D0,D5			;all 32 keys free?
	BNE.S	5$			;no
	ADD.L	D6,D4			;else 32 more free
	BRA.S	4$
5$:	MOVE.L	D7,D1			;else check each bit
2$:	BTST	D1,D0			;key free?
	BEQ.S	3$			;no
	INCL	D4
3$:	DBF	D1,2$
4$:	DBF	D3,1$			;loop for all longwords in bitmap
	MOVE.L	D7,D3			;max 32 keys in last longword
	AND.W	D2,D3			;number of bits in last longword
	ZAP	D1			;start with bit 0
	MOVE.L	(A0),D0			;last longword of bitmap
	BRA.S	7$
6$:	BTST	D1,D0			;key busy?
	BEQ.S	8$			;yes
	INCL	D4			;else count it
8$:	INCL	D1			;advance to next bit
7$:	DBF	D3,6$			;loop for all remaining keys
	MOVE.L	D4,FreeKeys		;save new count
	POP	D2-D7
	RTS

* Allocates bitmap buffer to hold number of bitmap blocks for volume based
* on size of volume in blocks.  Bitmap buffer is always a multiple of block
* size.

AllocateBMBuffers:
	MOVE.L	BitmapBlocks,D0
	MOVE.L	BlkSizeShift,D1
	LSL.L	D1,D0			;size of bitmap buffer
	MOVE.L	D0,BitmapBufSize	;save for later release
	BSR	AllocateIOBuf		;get buffer
	MOVE.L	D0,BitmapBuffer		;save ptr to bitmap buffer
	BEQ.S	8$
	MOVE.L	BitmapBlocks,D0		;number of actual bitmap blocks
	ADD.L	BMExtBlocks,D0		;plus number of extension blocks
	LSL.L	#2,D0
	MOVE.L	D0,BitmapKeyBufSize
	BSR	AllocateIOBuf
	MOVE.L	D0,BitmapKeyBuffer
	BNE.S	9$			;oops...
8$:	STC
9$:	RTS

* Frees any allocated bitmap buffers.

FreeBMBuffers:
	PUSH	A2
	MOVE.L	SysBase,A6
	LEA	BitmapBuffer,A2
	MOVE.L	BitmapBufSize,D0
	BSR.S	FreeBuf
	LEA	BitmapKeyBuffer,A2
	MOVE.L	BitmapKeyBufSize,D0
	BSR.S	FreeBuf
	LEA	BitmapAltBuffer,A2
	MOVE.L	BitmapBufSize,D0
	BSR.S	FreeBuf
	LEA	BitmapBadBuffer,A2
	MOVE.L	BitmapBufSize,D0
	BSR.S	FreeBuf
	CLR.B	BitmapFlg
	POP	A2
	RTS

FreeBuf:
	IFZL	(A2),1$
	MOVE.L	(A2),A1
	CALLSYS	FreeMem			;free the bitmap buffer
	CLR.L	(A2)
1$:	RTS

		BSS,PUBLIC

BitmapBuffer		DS.L	1	;ptr to bitmap buffer
BitmapAltBuffer		DS.L	1	;alternate bitmap buffer
BitmapBadBuffer		DS.L	1	;alternate bitmap buffer
BitmapBufSize		DS.L	1	;size of bitmap buffer
BitmapKeyBuffer		DS.L	1	;ptr to list of bitmap keys
BitmapKeyBufSize	DS.L	1	;size of list
BitmapDataPtr		DS.L	1	;ptr to actual start of bitmap data
BitmapAltPtr		DS.L	1	;ptr to actual start of alternate bitmap
BitmapBadPtr		DS.L	1	;ptr to actual start of bad block bitmap
LastUsedKey		DS.L	1	;start search for free key here
FreeKeys		DS.L	1
BitmapFlg		DS.B	1	;1=bitmap valid
BMDirtyFlg		DS.B	1	;1=bitmap dirty...needs to be written

	END
