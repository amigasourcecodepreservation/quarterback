;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*****************************************************************
*								*
*		VTEST.ASM					*
*								*
*	Quarterback Tools Volume Block Test			*
*								*
*****************************************************************

	INCLUDE "vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"VD0:BOXES.ASM"

	XDEF	Function1,Function30
	XDEF	Function10
	XDEF	InsertVolName
	XDEF	CountFragments
	XDEF	RefreshBitmap,UpdateBitmapImage
	XDEF	CalcBMMode
	XDEF	FreePixel,BusyPixel
	XDEF	RexBadBlks
	XDEF	ForceRead

	XDEF	CurrentBlock
	XDEF	FragmentCount
	XDEF	ParamTblPtr
	XDEF	BMBox

* Externals in TOOLS.ASM

	XREF	MLoop,MLoopErr
	XREF	WaitForAction
	XREF	ProcessMsg

	XREF	SysBase,DosBase,IntuitionBase,GraphicsBase
	XREF	Window,TMP.,TMP2.,TMP3.
	XREF	MSG_CLASS,MSG_CODE,GAD_PTR
	XREF	RefreshRoutine
	XREF	ToolsTask
	XREF	VolNameBuf.
	XREF	FunGadList
	XREF	CancelFlg,YesFlg

* Externals in Grahics.asm

	XREF	InitBar
	XREF	UpdateStatusBar
	XREF	RefreshBar

* Externals in FILES.ASM

	XREF	CheckBadBlocks
	XREF	MarkBadBlocks
	XREF	OpenBadBlocksFile
	XREF	AddBadKey
	XREF	DelBadBlocksFile

	XREF	OldBadCount,NewBadCount
	XREF	InUseCount
	XREF	BadBlkFlg

* Externals in Device.asm

	XREF	CheckDiskLoaded
	XREF	CheckVolumeStatus
	XREF	CalcVolParams
	XREF	OpenDriver,CloseDriver
	XREF	InhibitDos,EnableDos
	XREF	CheckProtection

	XREF	NumSoftErrors
	XREF	UnitNumber
	XREF	DiskState
	XREF	NumBlocks
	XREF	NumBlocksUsed
	XREF	BytesPerBlock
	XREF	DiskType
	XREF	VolumeNode
	XREF	InUse	
	XREF	MaxBytes
	XREF	MaxBlocks
	XREF	FreeBytes
	XREF	FreeBlocks
	XREF	VolumeOffset
	XREF	LowCyl
	XREF	HighCyl
	XREF	MaxCylinders
	XREF	Surfaces
	XREF	MaxTracks
	XREF	BlockSize,BlockSizeL
	XREF	Reserved,PreAlloc
	XREF	BlocksPerTrack
	XREF	NumBuffers
	XREF	BufMemType
	XREF	MaxTransfer
	XREF	PercentFull
;	XREF	DosType
	XREF	DeviceNameBuf.
	XREF	DriverNameBuf.
	XREF	DriverVersion.
	XREF	VolValidFlg
	XREF	NoDiskFlg
	XREF	NotDosFlg
	XREF	NotValidFlg
	XREF	WrtProtFlg
	XREF	VolStatusMsg
	XREF	BlkSizeOffset
	XREF	BitmapBlocks
	XREF	LowestKey,HighestKey
	XREF	MaxKeys
	XREF	RootBlkNbr
	XREF	FFSFlg

* Externals in Gadgets.asm

;	XREF	Fun1
	XREF	RightPGad,CanGad,PauseGad
	XREF	ReqProceed,ReqYes

* Externals in Messages.asm

	XREF	NotImp.,NoParms.
	XREF	ParamTitle1.,ParamTitle2.,ParamTitle3.
	XREF	Parm1.
	XREF	Parm2.,Parm3.,Parm4.,Parm5.,Parm6.
	XREF	Parm2M.,Parm4M.
	XREF	Parm7.,Parm8.,Parm9.,Parm10.,Parm11.,Parm12.
	XREF	Parm13.,Parm14.,Parm15.,Parm16.,Parm17.,Parm18.
	XREF	Parm19.
	XREF	Stat1.,Stat2.,Stat3.,Stat4.,Stat5.
	XREF	Old.,Fast.
	XREF	MTChip.,MTFast.,MTPublic.
	XREF	BlkScanTitle.
	XREF	BitmapTitle.
	XREF	Frag.,NoFrag.,NoFree.
	XREF	Period.,Ques.
	XREF	LgFree.,LgUsed.
	XREF	BadScanTitle.,DirsFilesOK.
	XREF	ProcDir.,ProcFile.
	XREF	PartSizeErr.
	XREF	BadCnt.
	XREF	RexBlkErr.

* Externals in Fun.asm

	XREF	AddGadgets,RemoveGadgets
	XREF	CLRWIN
	XREF	REFRESH,RefreshAllGads
	XREF	StripAppend
	XREF	MoveInsertDevice
	XREF	LongDivide
	XREF	DRAW_BOXES
	XREF	ReqRQTxt

* Externals in DISK.ASM

	XREF	ReadBlock,WriteBlock
	XREF	MotorOff

* Externals in CAT.ASM

	XREF	PushWindow,PopWindow

* Externals in BUFFERS.ASM

	XREF	ReadControl,ReadData
	XREF	AllocCacheBuffers,FreeCacheBuffers
	XREF	GetCacheBuffer,PutCacheBuffer
	XREF	ZapBlock,ZapAnyBlock
	XREF	AllocateBuffer,AllocateIOBuf
	XREF	AllocImageBuf,FreeImageBuf
	XREF	ReadCache,WriteCache
	XREF	AllocateBlock,FreeBlock
	XREF	AllocBadBlkBuf,FreeBadBlkBuf

	XREF	BlockBuffer
	XREF	ImageBuffer

* Externals in BITMAP.ASM

	XREF	ReadBitmap
	XREF	GetBadStatus
	XREF	GetBusyStatus
	XREF	CountFreeKeys

	XREF	BitmapDataPtr
	XREF	BitmapFlg

* Externals in RESTORE.ASM

	XREF	DisplayFilePath
;;	XREF	FileName.

* Externals in REORG.ASM

	XREF	ReorgFlg

* Externals in AREXX.ASM

	XREF	ArexxFlg
	XREF	ResultBuf

ig_ImageData	EQU	$A

*********************************************************************************

* Displays volume parameters for selected volume.  If volume is invalid,
* displays only device information.

Function1:
	BSR	CheckVolumeStatus	;get device params
	NOERROR	1$			;got params
	DispErr	NoParms.		;else warn op
	BRA	MLoop
1$:	BSR	CalcVolParams		;calc volume params
	IFNZB	NoDiskFlg,2$		;no disk...
	BSR	OpenBadBlocksFile	;see if there are any bad blocks
;	BSR	MotorOff
2$:	LEA	RightPGad,A0
	LEA	Fun1Refresh,A1
	ZAP	D0
	BSR	PushWindow		;remove gadgets and clear window
	BSR	WaitForAction		;wait for operator to act
	BSR	PopWindow		;restore window
	BRA	MLoop

Fun1Refresh:
	PUSH	D2-D3/A2-A4
	BSR	CLRWIN
	IFZB	VolValidFlg,1$		;volume not valid
	DispMsg #PrmLTxt_LE,#PrmTitle_TE,ParamTitle1.,WHITE
	DispCur	VolNameBuf.,ORANGE,BLUE
	DispCur	ParamTitle2.,WHITE
	BRA.S	2$
1$:	DispMsg	#PrmLTxt_LE,#PrmTitle_TE,ParamTitle3.,WHITE
2$:	DispCur	DeviceNameBuf.,ORANGE,BLUE
	DispCur	Period.,WHITE

	DispMsg	#PrmLTxt_LE,#PrmLin1_TE,Parm15.,WHITE
	MOVE.L	VolStatusMsg,A0
	DispCur	A0,ORANGE

	DispMsg	#PrmLTxt_LE,#PrmLin2_TE,Parm13.,WHITE
	DispCur	DriverNameBuf.,ORANGE,BLUE
	DispCur	DriverVersion.,ORANGE,BLUE

CaclVolSizes:
	MOVE.L	#1024,D3
	MOVEQ	#10,D1			;shift count for division by 1024

	MOVE.L	MaxBytes,D0
	LEA	Parm2.,A0
	LSR.L	D1,D0			;divide by 1024
	IFLTL	D0,D3,1$
	LEA	Parm2M.,A0
1$:	MOVE.L	D0,MaxSpace
	MOVE.L	A0,MaxSp+4

	MOVE.L	FreeBytes,D0
	LEA	Parm4.,A0
	LSR.L	D1,D0			;divide by 1024
	IFLTL	D0,D3,2$
	LEA	Parm4M.,A0
2$:	MOVE.L	D0,FreeSpace
	MOVE.L	A0,FreeSp+4

	MOVE.L	MaxTransfer,D0
	LSR.L	D1,D0
	MOVE.L	D0,MxTran

DispPrms:
	LEA	ParamTable,A2
	MOVEQ	#JAM1,D2
	SWAP	D2
	MOVE.W	#WHITE*256,D2
1$:	MOVE.L	(A2)+,D0		;LE,TE
	BEQ.S	3$			;end of table
	MOVE.L	D2,D1			;mode and color
	MOVE.L	(A2)+,A0		;get msg ptr
	BSR	DISP_MSG		;put up text
	MOVE.L	(A2)+,A0		;get ptr to data
	STR.	L,(A0),TMP2.,#32,#10
	STRIP_LB. TMP2.
	MOVE.W	(A2)+,D3		;get flag
	IFNEIB	D3,#2,6$		;don't scale
	LEA	TMP2.,A0
	BSR	ScaleMB			;convert to MB
6$:	IFZW	D3,2$			;don't check volvalidflg
	IFNZB	VolValidFlg,2$		;volume IS valid...use data
	MOVE.	Ques.,TMP2.		;else substitute question mark
2$:	DispCur	TMP2.,ORANGE,BLUE
	BRA.S	1$

3$:	DispMsg	#PrmLTxt_LE,#PrmLin8_TE,Parm1.,WHITE
	MOVE.	Ques.,TMP2.
	IFZB	VolValidFlg,5$
	STR.	L,PercentFull,TMP2.,#32,#3
	STRIP_LB. TMP2.
	ACHAR.	#'%',TMP2.
5$:	DispCur	TMP2.,ORANGE,BLUE

	DispMsg	#PrmLTxt_LE,#PrmLin10_TE,Parm14.,WHITE
	LEA	Ques.,A0
	IFZB	VolValidFlg,4$
	LEA	Old.,A0
	IFZB	FFSFlg,4$
	LEA	Fast.,A0
4$:	DispCur	A0,ORANGE,BLUE

	DispMsg	#PrmRTxt_LE,#PrmLin13_TE,Parm18.,WHITE
	LEA	MTPublic.,A0
	MOVE.L	BufMemType,D0
	AND.W	#6,D0
	BEQ.S	7$
	LEA	MTChip.,A0
	BTST	#1,D0
	BNE.S	7$
	LEA	MTFast.,A0
7$:	DispCur	A0,ORANGE,BLUE

	LEA	RightPGad,A0
	GadColor A0,ORANGE
	BSR	RefreshAllGads
	POP	D2-D3/A2-A4
	RTS

**********************************************************************************

* Displays volume bit map to reveal degree of volume space fragmentation.

Function30:
	BSR	CheckVolumeStatus	;get device params
	ERROR	MLoop			;problems
	BSR	CheckDiskLoaded
	ERROR	MLoop
	BSR	CalcVolParams		;calc volume params
	BSR	AllocImageBuf		;get image buffer
	ERROR	8$
	BSR	CalcBMMode
	CLR.B	BitmapFlg		;force bitmap read
	LEA	RightPGad,A0
	LEA	Fun30Refresh,A1
	ZAP	D0
	BSR	PushWindow		;remove gadgets and clear window
;	BSR	OpenDriver		;got the device?
;	ERROR	8$			;NO
	BSR	ReadBitmap
	ERROR	7$			;problems
	CLR.B	BusyFragFlg
	BSR	CountFragments
	BSR	RefreshBitmap		;put up the bitmap
	BSR	WaitForAction
7$:;	BSR	CloseDriver		;close the Driver
8$:	BSR	FreeImageBuf
	BSR	PopWindow		;restore window
	BRA	MLoop

Fun30Refresh:
	PUSH	D2/A2-A4
	BSR	CLRWIN
	LEA	BitmapTitle.,A0
	BSR	InsertVolName		;insert volume
	DispCent #320,#BMTitle_TE,A0,WHITE
	BSR.S	RefreshBitmap
	LEA	RightPGad,A0
	GadColor A0,ORANGE
	BSR	RefreshAllGads
	POP	D2/A2-A4
	RTS

* Draws bitmap display, based on data in BitmapBuffer and size of
* volume.

RefreshBitmap:
	PUSH	D2-D7/A2-A6
	IFZB	BitmapFlg,10$,L		;no valid bitmap to display

	BSR	CalcBox
	MOVE.L	ParamTblPtr,A4
	MOVE.L	BitmapDataPtr,A2
	ZAP	D3
	ZAP	D4
	ZAP	D5
	MOVE.L	MaxKeys,D7
	MOVE.L	bpt_Incr(A4),D0
	DIVU	D0,D7			;calculate number of pixels to display
	MOVE.L	bpt_GetKey(A4),A5	;get key routine
	MOVE.L	bpt_DispPix(A4),A6
	BRA.S	2$
1$:	JSR	(A5)			;get key status
	JSR	(A6)			;display status
	INCL	D5			;count pixel
2$:	DBF	D7,1$

5$:	MOVE.L	#Bitmap_WD-8,D6
	DIVU	bpt_PixX+2(A4),D6	;number of keys per row
	MOVE.L	D5,D0
	DIVU	D6,D0			;number of rows plus remainder
	SWAP	D0			;remainder is how much on this row
	SUB.W	D0,D6			;number of keys remaining to fill
	BRA.S	7$

6$:	TST.B	BusyFragFlg
	JSR	(A6)
	INCL	D5
7$:	DBF	D6,6$

9$:	BSR.S	UpdateBitmapImage
	IFNZB	ReorgFlg,10$		;don't display frag count during reorg
	BSR	DispFragCount
10$:	POP	D2-D7/A2-A6
	RTS

* Displays updated bitmap image onto screen.

UpdateBitmapImage:
	LEA	BitmapImage,A1
	MOVE.L	ImageBuffer,ig_ImageData(A1)
	MOVE.L	Window,A0
	MOVE.L	wd_RPort(A0),A0		;ptr to raster port
	MOVEQ	#4,D0
	MOVEQ	#2,D1
	CALLSYS	DrawImage,IntuitionBase
	RTS

* These routines check bitmap keys on a bit, 2-bit, 4-bit, 8-bit, 16-bit or 32-bit
* basis.  D2-D4 are reserved for local use.  A group of keys is said to be free if
* any one key in the group is free.  This reveals free space fragmentation better.

Get32Key:
	TST.L	(A2)+
	RTS

Get16Key:
	BCHG	#0,D3
	BNE.S	1$
	MOVE.L	(A2)+,D2
	TST.W	D2
	RTS
1$:	SWAP	D2
	TST.W	D2
	RTS


Get8Key:
	IFNZB	D4,1$
	MOVE.L	(A2)+,D2
	MOVE.L	#$FF000000,D3
	MOVEQ	#4,D4
1$:	DECB	D4
	ROL.L	#8,D3
	MOVE.L	D2,D0
	AND.L	D3,D0
	RTS


Get4Key:
	IFNZB	D4,1$
	MOVE.L	(A2)+,D2
	MOVE.L	#$F0000000,D3
	MOVEQ	#8,D4
1$:	DECB	D4
	ROL.L	#4,D3
	MOVE.L	D2,D0
	AND.L	D3,D0
	RTS


Get2Key:
	IFNZB	D4,1$
	MOVE.L	(A2)+,D2
	MOVE.L	#$C0000000,D3
	MOVEQ	#16,D4
1$:	DECB	D4
	ROL.L	#2,D3
	MOVE.L	D2,D0
	AND.L	D3,D0
	RTS

Get1Key:
	IFNZB	D4,1$
	MOVE.L	(A2)+,D2
	MOVE.L	#$80000000,D3
	MOVEQ	#32,D4
1$:	DECB	D4
	ROL.L	#1,D3
	MOVE.L	D2,D0
	AND.L	D3,D0
	RTS

* Given pixel number (range: 0-65535) in D5 and status in Z-bit, updates
* BitmapImageBuffer by setting or clearing the proper bit.  Z-bit determines
* pixel color: Z=1=all busy, Z=0=some (or all) free.

PutPixel:
	MOVE.L	ImageBuffer,A0
	BEQ.S	1$			;busy
	MOVEQ	#7,D0
	AND.L	D5,D0			;bit number in byte
	MOVEQ	#7,D1
	SUB.B	D0,D1			;reverse the order of bits
	MOVE.L	D5,D0
	ASR.L	#3,D0			;divide to get byte offset
	BSET	D1,0(A0,D0.L)
	SETF	BusyFragFlg
	RTS
1$:	MOVEQ	#7,D0
	AND.L	D5,D0			;bit number in byte
	MOVEQ	#7,D1
	SUB.B	D0,D1			;reverse the order of bits
	MOVE.L	D5,D0
	ASR.L	#3,D0			;divide to get byte offset
	BCLR	D1,0(A0,D0.L)
	CLR.B	BusyFragFlg
	RTS

* Stores key status as a block of 2x1 or 4x2 or 8x4 pixels.  
* Z-bit determines pixel color.  Z=1=busy, Z=0=free.
* A4 points to BitmapParamTbl entry, D5 is the key to be processed.

PixelBlk:
	PUSH	D2-D5
	SNE	D4			;save status
	MOVE.B	D4,BusyFragFlg
	MOVE.L	#Bitmap_WD-8,D1
	DIVU	bpt_PixX+2(A4),D1	;number of keys per row
	MOVE.L	D5,D0			;this is key being processed (0 origin)
	DIVU	D1,D0			;calc which row this key is on
	MOVE.L	D0,D5
	SWAP	D5			;remainder is offset on row
	MOVE.W	#Bitmap_WD-8,D1
	MULU	bpt_PixY+2(A4),D1	;pixel offset per row
	MULU	D1,D0			;pixel offset to start of proper row
	MULU	bpt_PixX+2(A4),D5	;pixel offset on row
	ADD.L	D0,D5
	MOVE.L	D5,D0
	MOVEQ	#7,D1
	AND.L	D5,D1
	MOVEQ	#7,D5
	SUB.B	D1,D5
	ASR.L	#3,D0			;divide to get byte offset
	MOVE.L	ImageBuffer,A0
	MOVE.L	bpt_PixY(A4),D3
	DECW	D3
1$:	MOVE.L	D5,D1
	MOVE.L	bpt_PixX(A4),D2
	DECW	D2
2$:	IFZB	D4,3$
	BSET	D1,0(A0,D0.L)
	BRA.S	4$
3$:	BCLR	D1,0(A0,D0.L)
4$:	DECW	D1
	DBF	D2,2$
	ADD.L	#(Bitmap_WD-8)/8,A0
	DBF	D3,1$
	POP	D2-D5
	RTS

* Called from REORG.ASM to update a pixel of the bitmap image.  Called at
* BusyPixel to mark it busy or FreePixel to mark it free.  Key in D0.

FreePixel:
	MOVEQ	#-1,D1
	BRA.S	PixCom

BusyPixel:
	ZAP	D1
PixCom:	PUSH	D5/A4
	MOVE.L	D0,D5
	SUB.L	LowestKey,D5
	MOVE.L	ParamTblPtr,A4
	MOVE.L	bpt_Shift(A4),D0
	LSR.L	D0,D5
	MOVE.L	bpt_DispPix(A4),A0
	TST.B	D1			;set Z-bit properly
	JSR	(A0)			;update pixel
	POP	D5/A4
	RTS

* Calculates actual size of bitmap display box, based on the number of keys
* in the partition.  This code assumes that ParamTblPtr points to the correct
* BitmapParamTbl entry for this partition size.

CalcBox:
	PUSH	D2
	MOVE.L	ParamTblPtr,A0
	MOVE.L	D0,D2		;save scaling factor
	MOVE.W	#Bitmap_WD,D0
	SUBQ.W	#8,D0		;max x pixels
	EXT.L	D0
	MOVE.L	bpt_PixX(A0),D1	;size of pixel block
	DIVU	D1,D0		;max keys per display line
	EXT.L	D0
	MOVE.L	bpt_Shift(A0),D2
	LSL.L	D2,D0		;calc max of keys per line
	MOVE.L	D0,D1		;add to MaxKeys to round up
	DECL	D1
	ADD.L	MaxKeys,D1
	DIVU	D0,D1		;divide by pix blks per line=max pix y
	MOVE.L	bpt_PixY(A0),D0
	MULU	D0,D1		;times y pixels per line
	MOVE.W	D1,BitmapImage+6
	ADDQ.W	#4,D1		;allow for borders
	MOVE.W	D1,BMBox+6	;save as box height
	LEA	BMBox,A0
	BSR	DRAW_BOXES
	POP	D2
	RTS

* Sizes handled by bitmap display, based on number of keys in a partition
* Note maximum volume size for bitmap display: 1.048 Gb

FloppySize	EQU	64*32		;8x4,	0.0-1.0Mb
Mode1Size	EQU	128*64		;4x2,	1.0-4.1Mb
Mode2Size	EQU	256*128		;2x1,	4.1-16.3Mb
Mode3Size	EQU	512*128		;1x1,	16.3-32.7Mb
Mode4Size	EQU	512*128*2	;1x1x2,	32.7-65.5Mb
Mode5Size	EQU	512*128*4	;1x1x4,	65.5-131Mb
Mode6Size	EQU	512*128*8	;1x1x8,	131-262Mb
Mode7Size	EQU	512*128*16	;1x1x16, 262-524Mb
Mode8Size	EQU	512*128*32	;1x1x32, 524Mb-1.0Gb

 STRUCTURE BitmapParmTbl,0
	LONG	bpt_Size	;max size in KEYS
	LONG	bpt_PixX	;display block width in pixels
	LONG	bpt_PixY	;display block height in pixels
	LONG	bpt_Incr	;key increment per display block
	LONG	bpt_Shift	;display box scale-shift count
	LONG	bpt_GetKey	;routine to get key(s) status
	LONG	bpt_DispPix	;routine to display pixel(s)
	LABEL	bpt_SIZEOF

BitmapParamTbl	;SIZE PixX PixY INCR SCALE-SHIFT ROUTINE
	DC.L	FloppySize,8,4,1,0,Get1Key,PixelBlk
	DC.L	Mode1Size,4,2,1,0,Get1Key,PixelBlk
	DC.L	Mode2Size,2,1,1,0,Get1Key,PixelBlk
	DC.L	Mode3Size,1,1,1,0,Get1Key,PutPixel
	DC.L	Mode4Size,1,1,2,1,Get2Key,PutPixel
	DC.L	Mode5Size,1,1,4,2,Get4Key,PutPixel
	DC.L	Mode6Size,1,1,8,3,Get8Key,PutPixel
	DC.L	Mode7Size,1,1,16,4,Get16Key,PutPixel
	DC.L	Mode8Size,1,1,32,5,Get32Key,PutPixel
	DC.L	0

* Finds proper entry in BitmapParamTbl based on partition size.

CalcBMMode:
	ZAP	D1
	MOVE.L	MaxKeys,D0
	LEA	BitmapParamTbl-bpt_SIZEOF,A0
	BRA.S	2$
1$:	ADD.W	#bpt_SIZEOF,A0
	MOVE.L	(A0),D1			;get next size
	BEQ.S	8$			;oops...exceeds table
2$:	IFGTL	D0,D1,1$
	MOVE.L	A0,ParamTblPtr		;save ptr to proper table entry
	RTS
8$:	DispErr	PartSizeErr.
	STC
	RTS

* Displays fragment count as part of refreshing bitmap display.

DispFragCount:
	PUSH	D2/A2
	LEA	NoFree.,A2
	MOVE.L	FragmentCount,D0
	BEQ.S	1$			;no free space at all
	LEA	NoFrag.,A2
	IFLEIL	D0,#1,1$		;one fragment
	STR.	L,D0,TMP2.,#32,#8
	STRIP_LB. TMP2.
	LEA	Frag.,A0
	LEA	TMP.,A1
	ZAP	D0
	BSR	MoveInsertDevice
	LEA	TMP.,A2
1$:	MOVE.W	BMBox+2,D2		;Bitmap_TE
	ADD.W	BMBox+6,D2		;Bitmap_HT
	ADDQ.W	#8,D2

	DispMsg	#Frag_LE,D2,LgFree.,BLACK,ORANGE
	DispCur	LgUsed.,WHITE,BLACK
	MOVE.L	A2,A0
	DispMsg	#240,D2,A0,WHITE,BLUE
	POP	D2/A2
	RTS

* Counts free fragments in the bitmap

CountFragments:
	PUSH	D2-D4
	ZAP	D4		;running total
	ZAP	D1		;state flag
	ZAP	D3		;bit counter
	MOVE.L	BitmapDataPtr,A0
	MOVE.L	MaxKeys,D2
	MOVE.L	(A0)+,D0
1$:	IFLTIW	D3,#32,2$
	MOVE.L	(A0)+,D0
	ZAP	D3
2$:	BTST	D3,D0
	BEQ.S	5$		;busy
	BSET	#0,D1		;already in free mode?
	BNE.S	3$		;yes
	INCL	D4
	BRA.S	3$
5$:	BCLR	#0,D1		;show in busy area
3$:	DECL	D2
	BEQ.S	4$		;done
	INCW	D3
	BRA.S	1$

4$:	MOVE.L	D4,FragmentCount
	POP	D2-D4
	RTS

********************************************************************************

* Scans volume for bad blocks and optionally marks them bad in bit map.

Function10:
	CLR.B	CancelFlg
	BSR	CheckVolumeStatus
	ERROR	MLoopErr		;problems
	BSR	CheckDiskLoaded
	ERROR	MLoopErr
	BSR	CalcVolParams		;calc volume params
	PUSH	D2-D3
	MOVE.L	MaxKeys,D0
	BSR	InitBar			;initialize the bar counts
	LEA	PauseGad,A0
	LEA	Fun10Refresh,A1
	ZAP	D0
	BSR	PushWindow		;remove gadgets and clear window
	BSR	AllocBadBlkBuf		;try to get a buffer
	ERROR	Fun10Error		;oops
	BSR	AllocateBlock
	BEQ	Fun10Error
	BSR	ReadBitmap		;load original bitmap
	ERROR	Fun10Error		;problems with bitmap
	BSR	CountFreeKeys		;find out how many blocks left
	MOVE.L	LowestKey,CurrentBlock	;start at bottom
	MOVE.L	HighestKey,D3
	SUB.L	PreAlloc,D3
	BSR	CheckBadBlocks		;see if there is already a bad blocks file
	IFZL	D0,1$			;no existing bad blocks	
	IFNZB	ArexxFlg,1$		;don't recheck if arexx
	BldWarn	D0,0,BBExist1.,BBExist2.,BBExist3.,BBExist4.
	DispReq	A0,ReqYes
	IFZB	YesFlg,1$		;don't recheck
	BSR	DelBadBlocksFile	;else delete the bad blocks file
1$:	MOVE.L	CurrentBlock,D2
	IFZB	BadBlkFlg,8$
	MOVE.L	D2,D0
	BSR	GetBadStatus		;already marked bad?
	BEQ.S	2$			;yes...don't recheck
8$:	MOVE.L	D2,D0
	MOVE.L	BlockBuffer,A0
	BSR	ReadBlock
	NOERROR	2$
	MOVE.L	D2,D0
	BSR	AddBadKey		;add it to list
	ERROR	9$			;list is full
	BSR	UpdateBadCount
2$:	INCL	CurrentBlock		;advance to next block
	BSR	UpdateStatusBar		;show something happening
	BSR	ProcessMsg		;check for op action
	IFNZB	CancelFlg,Fun10Exit,L	;op has canceled
;	IFLEL	CurrentBlock,HighestKey,1$ ;loop for all of blocks
	IFLEL	CurrentBlock,D3,1$ 	;loop for all of blocks

9$:	IFNZB	ArexxFlg,Fun10Exit,L	;don't actually mark bad
	MOVE.L	NewBadCount,D0		;any new block errors?
	MOVE.L	OldBadCount,D1
	MOVE.L	D0,D2
	ADD.L	D1,D2
	BNE.S	3$			;yes...some known errors
	BldWarn	0,0,NoBlkEr1.,NoBlkEr2. ;let op know all is well
	BRA	7$

3$:	IFNZL	D0,10$			;found some new bad
	IFNZL	D1,4$			;old existed, no new

10$:	IFZL	D1,5$			;some new, no old
	MOVE.L	D2,D0			;total to D0
	BldWarn	D0,D1,BlkEr4.,BlkEr5.,BlkEr6.,BlkEr7.
	BRA.S	6$

4$:	MOVE.L	D1,D0
	BldWarn	D0,0,BlkEr8.,BlkEr9.,BlkEr10.	;all already marked bad
7$:	DispReq	A0,ReqProceed
	BRA	Fun10Exit

5$:	BldWarn	D0,0,BlkEr1.,BlkEr2.,BlkEr3.,BlkEr3a.
6$:	DispReq	A0,ReqYes
	IFZB	YesFlg,Fun10Exit	;don't create/update bad.blocks
	BSR	InhibitDos
	BSR	ForceRead			
	BSR	MarkBadBlocks		;create/update bad blocks file
	ERROR	Fun10Error
	MOVE.L	InUseCount,D0
	BEQ.S	Fun10Exit		;none already in use
	BldWarn	D0,0,BlkWrn1.,BlkWrn2.,BlkWrn3.,BlkWrn4.
	DispReq	A0,ReqProceed

Fun10Exit:
	BSR	MotorOff
Fun10Error:
	BSR	EnableDos
	BSR	FreeBlock
	BSR	FreeBadBlkBuf
	BSR	PopWindow		;restore window
	POP	D2-D3
	BRA	MLoop

Fun10Refresh:
	PUSH	D2/A2-A4
	BSR	CLRWIN
	LEA	BlkScanTitle.,A0
	BSR	InsertVolName		;insert volume
	DispCent #320,#BSTitle_TE,A0,WHITE
	BSR	RefreshBar
	BSR.S	UpdateBadCount
	LEA	PauseGad,A0
	GadColor A0,ORANGE
	LEA	CanGad,A0
	GadColor A0,BLACK
	BSR	RefreshAllGads
	POP	D2/A2-A4
	RTS

RexBadBlks:
	STR.	L,NewBadCount,TMP2.,#32,#6
	STRIP_LB. TMP2.
	LEA	RexBlkErr.,A0
	LEA	ResultBuf,A1
	BSR	MoveInsertDevice
	RTS

ForceRead:
	PUSH	D7
	MOVEQ	#5,D7
1$:	MOVE.L	BlockBuffer,A0
	MOVE.L	RootBlkNbr,D0
	BSR	ReadBlock
	NOERROR	9$
	DBF	D7,1$
9$:	POP	D7
	RTS

* Updates count of bad blocks on screen

UpdateBadCount:
	DispMsg	#SFC_LE-16,#SFC_TE,BadCnt.,WHITE
	STR.	L,NewBadCount,TMP3.,#32,#5
	STRIP_LB. TMP3.
	DispCur	TMP3.,ORANGE,BLUE
	RTS

* Inserts volume name into msg in A0 at @, and moves to TMP.
* Returns ptr to TMP. in A0.

InsertVolName:
	LEA	TMP.,A1
	BSR	MoveInsertDevice
	LEA	TMP.,A0
	RTS

* Scales number string in A0 (in KB) by 1000 to get MB.

ScaleMB:
	MOVE.L	A0,A1
	BRA.S	2$
1$:	INCL	A0
2$:	TST.B	(A0)
	BNE.S	1$
	MOVE.L	A0,D0
	SUB.L	A1,D0
	IFLEIB	D0,#4,9$
	CLR.B	-(A0)
	CLR.B	-(A0)
	MOVEQ	#'.',D0
	MOVE.B	D0,-2(A0)
9$:	RTS

PrmTbl	MACRO	LE,TE,MsgPtr,DataPtr,Flag
	DC.W	\1,\2
	DC.L	\3,\4
	DC.W	\5
	ENDM

BOX	MACRO	;LE,TE,WD,HT,BORDER PEN,FILL PEN
	DC.W	\1,\2,\3,\4
	IFEQ	NARG-6
	DC.B	\5,\6
	ENDC
	IFEQ	NARG-4
	DC.B	1,0
	ENDC
	ENDM

	CNOP	0,2

* Box definitions

BMBox	BOX	Bitmap_LE,Bitmap_TE,Bitmap_WD,Bitmap_HT,BLACK,WHITE
	DC.L	0

* This table is used to display volume parameters

ParamTable
MaxSp	PrmTbl	PrmLTxt_LE,PrmLin4_TE,Parm2.,MaxSpace,2
FreeSp	PrmTbl	PrmLTxt_LE,PrmLin5_TE,Parm4.,FreeSpace,2
	PrmTbl	PrmLTxt_LE,PrmLin6_TE,Parm3.,MaxBlocks,1
	PrmTbl	PrmLTxt_LE,PrmLin7_TE,Parm5.,FreeBlocks,1
	PrmTbl	PrmLTxt_LE,PrmLin9_TE,Parm17.,OldBadCount,1
	PrmTbl	PrmRTxt_LE,PrmLin4_TE,Parm6.,LowCyl,0
	PrmTbl	PrmRTxt_LE,PrmLin5_TE,Parm7.,HighCyl,0
	PrmTbl	PrmRTxt_LE,PrmLin6_TE,Parm8.,MaxCylinders,0
	PrmTbl	PrmRTxt_LE,PrmLin7_TE,Parm9.,Surfaces,0
	PrmTbl	PrmRTxt_LE,PrmLin8_TE,Parm10.,BlocksPerTrack,0
	PrmTbl	PrmRTxt_LE,PrmLin9_TE,Parm16.,BlockSize,0
	PrmTbl	PrmRTxt_LE,PrmLin10_TE,Parm11.,Reserved,0
	PrmTbl	PrmRTxt_LE,PrmLin11_TE,Parm19.,PreAlloc,0
	PrmTbl	PrmRTxt_LE,PrmLin12_TE,Parm12.,MxTran,0
	DC.L	0

BitmapImage
	DC.W	Bitmap_LE,Bitmap_TE,Bitmap_WD-8,Bitmap_HT
	DC.W	2		;depth (Workbench is always 2)
	DC.L	0		;ptr to image data
	DC.B	1,2		;plane pick, on-off
	DC.L	0		;no other image structure	

	BSS,PUBLIC

MaxSpace	DS.L	1	;adjusted max space
FreeSpace	DS.L	1	;adjusted free space
MxTran		DS.L	1	;local MaxTransfer
CurrentBlock	DS.L	1	;current block number
FragmentCount	DS.L	1
ParamTblPtr	DS.L	1	;pointer to entry in BitmapParamTbl
Fun7ErrFlg	DS.B	1
BusyFragFlg	DS.B	1	;1=in a busy fragment

	END

