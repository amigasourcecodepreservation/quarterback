;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*****************************************************************
*								*
*		CATALOG.ASM					*
*								*
*	Quarterback Tools File Catalog Routines			*
*								*
*****************************************************************

	INCLUDE "vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"VD0:BOXES.ASM"

	XDEF	DnArrow,UpArrow,PropHit,FileGadHit
	XDEF	ProcCat,CanCat
	XDEF	DispFileCatalog
	XDEF	InitWindowList,PushWindow,PopWindow,NextWindow
	XDEF	InitDirectory
	XDEF	Parent,Root
	XDEF	ScrollWindow
	XDEF	InitTree,NextFile
	XDEF	InitPath,AppendPath,TruncatePath
	XDEF	AppendDrawer,AppendFIB
;	XDEF	PushMenu,PopMenu,InitMenuPtr
	XDEF	NextMenu
	XDEF	AddMenuStrip,RemoveMenuStrip
	XDEF	IncAll,IncAct,IncDel
	XDEF	ExcAll,ExcAct,ExcDel

	XDEF	RefreshRoutine
	XDEF	WindowListPtr
;;	XDEF	MenuListPtr
	XDEF	UpdWinFlg
	XDEF	SelDeleted,SelActive
	XDEF	TotDeleted,TotActive
	XDEF	Path.
	XDEF	ParentDir

* Externals in TOOLS.ASM

;;	XREF	MLoop
	XREF	RefreshFunctions
	XREF	MenuPk
	XREF	WaitForAction
	XREF	ProcessMsg

	XREF	SysBase,DosBase,IntuitionBase,GraphicsBase
	XREF	Window
	XREF	TMP.,TMP2.,TMP3.
	XREF	MSG_CLASS,MSG_CODE,GAD_PTR
	XREF	FontPtr
	XREF	ToolsTask
	XREF	VolNameBuf.
	XREF	FunGadList

	XREF	ProcFlg
	XREF	CancelFlg

* Externals in MAIN.ASM


* Externals in RESTORE.ASM

	XREF	UpdateFileCounts
	XREF	DispCntTxt

	XREF	DirTree

* Externals in FILES.ASM

* Externals in Device.asm

	XREF	BlockSize
	XREF	VolValidFlg

* Externals in Gadgets.asm

	XREF	PropGad,PropPot
	XREF	ResCGad,ResDGad
	XREF	RootGad
	XREF	DT_TXT
	XREF	CatGList

* Externals in Messages.asm

	XREF	MONTHS
	XREF	SubDir.
	XREF	CatFor.
;	XREF	Legnd.,LgInc.,LgExc.,LgAct.
	XREF	FSel.,OutOf.
	XREF	CTxt1.,CTxt2.,CTxt3.,CTxt4.
	XREF	CFil.,CDel.,CAct.

* Externals in Fun.asm

	XREF	AddGadgets,RemoveGadgets
	XREF	CLRWIN,ClearBox
	XREF	RefreshAllGads
	XREF	REFRESH
	XREF	StripAppend
	XREF	MoveInsertDevice
	XREF	LongDivide
	XREF	DRAW_BOXES
	XREF	BldWarningMsg
	XREF	ReqRQTxt
	XREF	GAD_COLOR

* Externals in REPORT.ASM

	XREF	MoveFileName

* Externals in DISK.ASM

* Externals in IMAGES.ASM

* Externals in MENUS.ASM

	XREF	RestoreMenu

MaxFileGads	EQU	22	;max files 
FontHt		EQU	8

ScrollWindow:
	IFLEIW	MaxEnt,#MaxFileGads,9$,L ;nothing to scroll
	MOVE.W	MaxEnt,D1
	SUB.W	#MaxFileGads,D1		;calc based on top of window
	MOVE.W	D1,D0
	MULU	PropPot,D0
	DIVU	#$FFFF,D0
;	DECW	D1			;fudge factor
	IFLEW	D0,D1,13$		;valid pos
	MOVE.W	D1,D0
13$:	MOVE.W	D0,D6			;new pos
	MOVE.W	CurEnt,D7		;old pos to D7
	IFEQW	D0,D7,9$		;no change in pos...ignore it
	BCS.S	3$			;new>old...
	SUB.W	D0,D7			;new<old...how far?
	IFGTIW	D7,#5,5$		;more than 5
11$:	BSR	NextUp
	ERROR	9$
	IFGTW	CurEnt,D6,11$		;loop till we'e there
	BRA.S	9$
3$:	SUB.W	D7,D0			;how far?
	IFGTIW	D0,#5,5$		;more than 5
12$:	BSR	NextDown
	ERROR	9$
	IFLTW	CurEnt,D6,12$		;loop till we're there
	BRA.S	9$
5$:	MOVE.L	ParentDir,A0		;parent dir for current list
	MOVE.L	df_Child(A0),A1	;this is where we start counting
	BEQ.S	9$			;nothing to search
	MOVE.W	D6,CurEnt
	MOVE.W	D6,D1			;looking for this entry
	BEQ.S	8$			;first entry
6$:	MOVE.L	df_Next(A1),A1		;try the next one
	IFZL	A1,9$			;end of list...corrupted?
	DECW	D1			;when it goes to 0, we've found it
	BNE.S	6$			;not end of list...keep checking
8$:	MOVE.L	A1,df_CurFib(A0)	;make this one the current top item
	BSR	UpdWinNoClear		;and redisplay window
9$:	CLR.B	UpdWinFlg
	RTS

PropHit:
	MOVE.L	Window,A0
	MOVE.L	wd_IDCMPFlags(A0),D0
	BCLR	#4,D0			;disable mousemove
	IFEQIL	MSG_CLASS,#GADGETUP,1$	;turn off mouse
	BSET	#4,D0			;enable mousemove
	SETF	UpdWinFlg
1$:	CALLSYS	ModifyIDCMP,IntuitionBase ;enable/disable the mouse
;;;	JMP	ProcessMsg
	RTS

FileGadHit:
	MOVE.L	GAD_PTR,A0		;find this gadget
	MOVE.W	gg_GadgetID(A0),D0	;range: 1-20
	MOVE.W	D0,D1			;save the level
	MOVE.L	ParentDir,A0		;ptr to parent dir for current level
	MOVE.L	df_CurFib(A0),A1	;this is where we start counting
	BRA.S	2$			;dont link fwd on first pass
1$:	MOVE.L	df_Next(A1),A1		;try the next one
2$:	IFZL	A1,9$			;end of list...clicked on inactive
	DECW	D1			;when it goes to 0, we've found it
	BNE.S	1$			;not end of list...keep checking
3$:	BTST	#fib_Dir,df_Flags(A1)	;a directory?
	BNE.S	4$			;yes...going to lower level
;;	BTST	#fib_NotBusy,df_Flags(A1) ;deleted item?
;;	BNE.S	5$			;yes
;;	DispErr	NotDel.			;file not deleted
;;	BRA.S	9$
;;5$:
	BCHG	#fib_Selected,df_Flags(A1) ;reverse selected state
	MOVE.L	A1,A0			;and update this line's color
	BSR	DispFileInfo		;line in D0
	BRA.S	9$

4$:;	MOVE.L	A1,df_CurFib(A0)	;start here next time
	MOVE.W	CurEnt,df_CurEnt(A0)	;save prop body value
	MOVE.L	A1,A0			;current Fib becomes new parent dir
	MOVE.L	A0,ParentDir
	MOVE.L	df_Child(A0),A1	;scan from start of new level
	MOVE.L	A1,df_CurFib(A0)
	CLR.W	CurEnt			;beginning of list
	BSR	AppendFIB
	BSR	UpdatePath
	BSR	UpdateWindow
	BSR	CountMaxEnt
	BSR	UpdateScrollBar
9$:	BSR	UpdateNumbers
	JMP	ProcessMsg

* Scrolling arrows

UpArrow:
	IFEQL	MSG_CLASS,#GADGETUP,9$	;ignore up
	BSR	NextUp
	BSR	UpdateScrollBar
	BSR	ScrollDelay
	MOVE.L	GAD_PTR,A0
	BTST	#7,gg_Flags+1(A0)	;gadget still selected?
	BNE.S	UpArrow			;yes
9$:	JMP	ProcessMsg			;else bail out

NextUp:
	PUSH	A2
	MOVE.L	ParentDir,A1
	MOVE.L	df_Child(A1),A0
	MOVE.L	df_CurFib(A1),A2
	IFEQL	A0,A2,9$		;already at top...can't do more
1$:	MOVE.L	A0,A3			;save ptr to 'previous'
	MOVE.L	df_Next(A0),A0
	IFNEL	A0,A2,1$		;loop till we find the current one
	MOVE.L	A3,df_CurFib(A1)	;and make this one current top
	BSR	ScrollDown		;else make room at top
	MOVEQ	#1,D0			;display at line 1
	MOVE.L	A3,A0
	BSR	DispFileInfo		;fill it in
	IFZW	CurEnt,8$
	DECW	CurEnt
	BRA.S	8$
9$:	CLR.W	CurEnt
	STC
8$:	POP	A2
	RTS

DnArrow:
	IFEQL	MSG_CLASS,#GADGETUP,9$	;ignore up
	BSR	NextDown
	BSR	UpdateScrollBar
	BSR	ScrollDelay
	MOVE.L	GAD_PTR,A0
	BTST	#7,gg_Flags+1(A0)	;gadget still selected?
	BNE.S	DnArrow			;yes
9$:	JMP	ProcessMsg			;else bail out

NextDown:
	PUSH	A2-A3
	MOVE.L	ParentDir,A1		;this level
	MOVE.L	df_CurFib(A1),A2	;get current top entry
	MOVE.L	A2,A0
	ZAP	D0			;start at level 1
1$:	MOVE.L	df_Next(A0),A0		;else get link
2$:	IFZL	A0,9$			;end of list before line 20
	INCW	D0
	IFLTIW	D0,#MaxFileGads,1$	;loop till we find last one
	MOVE.L	A0,A3			;save ptr to new last line
	MOVE.L	df_Next(A2),df_CurFib(A1) ;make next entry the top item
	BSR	ScrollUp		;make room at bottom
	MOVEQ	#MaxFileGads,D0		;goes on line 20
	MOVE.L	A3,A0
	BSR	DispFileInfo
	INCW	CurEnt
	BRA.S	8$
9$:	STC
8$:	POP	A2-A3
	RTS

* Waits for a while during scrolling, to prevent scrolling too fast.

ScrollDelay:
	MOVEQ	#4,D1
	CALLSYS	Delay,DosBase
	RTS

CountMaxEnt:
	MOVE.L	ParentDir,A1
	MOVE.L	df_Child(A1),A0
	ZAP	D0
1$:	IFZL	A0,9$
	INCW	D0
	MOVE.L	df_Next(A0),A0
	BRA.S	1$
9$:	MOVE.W	D0,MaxEnt
	RTS

UpdateScrollBar:
	PUSH	D2-D5/A2
	MOVEQ	#-1,D1
	MOVE.W	D1,D4			;default body
	ZAP	D2			;default pot
	MOVEQ	#MaxFileGads,D0
	IFLEW	MaxEnt,D0,1$		;entire list on screen
;	IFGEW	D0,D2,1$		;entire text on screen
;	ZAP	D2
	MOVE.W	MaxEnt,D2
	MULU	D1,D0
	DIVU	D2,D0
	MOVE.W	D0,D4			;new slider size
	ZAP	D0
	MOVE.W	CurEnt,D0
	SUB.W	#MaxFileGads,D2
	MULU	D1,D0
	DIVU	D2,D0
	MOVE.W	D0,D2
1$:	LEA	PropGad,A0
	MOVEQ	#5,D0
	ZAPA	A2
	MOVEQ	#1,D5
	MOVE.L	Window,A1
	CALLSYS	NewModifyProp,IntuitionBase
	POP	D2-D5/A2
	RTS

* Displays files in catalog window.

UpdateWindow:
	LEA	FileBox,A0
	BSR	ClearBox	
UpdWinNoClear:
	PUSH	D2/A2
	MOVE.L	ParentDir,A2		;current directory level ptr
	MOVE.L	df_CurFib(A2),A2	;first file entry in window
	ZAP	D2			;line counter
1$:	INCW	D2
	IFGTIW	D2,#MaxFileGads,2$	;filled all lines
	MOVE.L	A2,A0			;display this entry
	IFZL	A0,2$			;no more files
	MOVE.L	df_Next(A0),A2		;set up link to next entry
	MOVE.L	D2,D0			;on this line of file window
	BSR	DispFileInfo		;display one line of file info
	BRA.S	1$
2$:	POP	D2/A2
	RTS

* Builds and displays one line of directory.  Dir line number (1-20) in D0.
* Pointer to CurFib in A0.

DispFileInfo:
	PUSH	D2-D5/A2-A5
	MOVE.L	D0,D4			;save calling params
	MOVE.L	A0,A4
	MOVE.L	Window,A5
	MOVE.L	wd_RPort(A5),A5
	MOVEQ	#JAM2,D0
	MOVE.L	A5,A1
	CALLSYS	SetDrMd,GraphicsBase
	MOVE.L	FontPtr,A0
	MOVE.L	A5,A1
	CALLSYS	SetFont			;force Topaz 80
	MOVE.B	df_Flags(A4),D0		;get flags for this item
	MOVEQ	#WHITE,D2		;active item foreground
	MOVEQ	#BLACK,D3		;unselected background
	BTST	#fib_NotBusy,D0		;is this item deleted?
	BEQ.S	1$			;no...
	MOVEQ	#ORANGE,D2	
1$:	BTST	#fib_Selected,D0	;selected for restoration?
	BEQ.S	5$			;NO
	MOVEQ	#BLUE,D3		;yes...blue background
5$:	MOVE.L	D2,D0
	MOVE.L	A5,A1
	CALLSYS	SetAPen
	MOVE.L	D3,D0
	MOVE.L	A5,A1
	CALLSYS	SetBPen
	DECW	D4			;change to 0 origin
	MULU	#FontHt,D4		;figure out which line we are on
	ADD.W	#Files_TE+8,D4		;font offset + border
	MOVE.W	D4,D1			;y pos
	MOVE.W	#Files_LE+4,D0		;x pos
	MOVE.L	A5,A1
	CALLSYS	Move			;set x,y pos to proper line
	LEA	df_Name(A4),A0		;name of dir or file
	BSR	MovePad			;move to TMP. and pad to 30 chars
	MOVE.L	A1,A3			;save ptr to end of name string
	BTST	#fib_Dir,df_Flags(A4)	;file?
	BEQ.S	2$			;yes
	APPEND.	SubDir.,TMP.		;no...report as subdirectory
	BRA.S	3$
2$:	STR.	L,df_Size(A4),TMP2.,#32,#8 ;convert size to ASCII, blank fill
	ACHAR.	#32,TMP2.		;add a trailing space

* This kludge puts highest byte of size, if non-blank, over last char of name.
* Only clobbers last name char if file size >9,999,999.

	LEA	TMP2.,A0
	MOVE.B	(A0)+,D0		;get 1st char of size
	IFEQIB	D0,#SPACE,4$		;it is space...ignore it
	MOVE.B	D0,-(A3)		;move 1st size char over last name char
4$:	APPEND.	A0,TMP.
	ZAP	D0
	MOVE.W	df_Date(A4),D0		;GET DAYS SINCE JAN 1, 1978
	BSR	ConvertDosDate		;add the date in dd-mmm-yy format
	ZAP	D0
	MOVE.W	df_Time(A4),D0		;minutes of day
	BSR	ConvertDosTime		;add the time in HH:MM
3$:	LEN.	TMP.			;load A0, set D0 to length
	MOVE.L	A5,A1
	CALLSYS	Text			;send size/date/time to that line
	MOVEQ	#BLACK,D0		;restore background color
	MOVE.L	A5,A1
	CALLSYS	SetBPen
	POP	D2-D5/A2-A5
	RTS

* Calculates day, month, and year from AmigaDOS date stamp in D0.  
* Stores in result in TMP.  FROM EDN OCT 17, 1985 P. 168

ConvertDosDate:
	PUSH	D2
	ADDI.L	#28431,D0		;MAGIC NUMBER CONVERTS TO MARCH 1, 1900
	MULU	#4,D0
	SUBQ.L	#1,D0			;K2=4*K-1
	MOVE.L	D0,D2
	DIVU	#1461,D2		;Y=INT(K2/1461)
	MOVE.L	D2,D1
	SWAP	D1			;D=INT (K2 MOD 1461)
	ADDQ.W	#4,D1
	LSR.W	#2,D1			;D=INT ((D+4)/4)
	MULU	#5,D1
	SUBQ.L	#3,D1
	DIVU	#153,D1	 		;M=INT ((5*D-3)/153)
	MOVE.W	D1,D0
	SWAP	D1
	EXT.L	D1			;D=INT ((5*D-3) MOD 153)
	ADDQ.L	#5,D1
	DIVU	#5,D1			;D=INT ((D+5)/5)
	CMPI.W	#10,D0
	BLT.S	1$
	SUBI.W	#9,D0			;M=M-9
	ADDQ.W	#1,D2			;Y=Y+1
	BRA.S	2$
1$:	ADDQ.W	#3,D0
; month in D0, day in D1, year in D2 at this point
2$:	PUSH	D0			;save month
	STR.	W,D1,TMP2.,#32,#2	;convert day
	APPEND.	TMP2.,TMP.
	ACHAR.	#'-',TMP.
	POP	D0
	DECW	D0
	LSL.W	#2,D0			;change into long index
;;	LEA	MONTHS(D0.W),A0		;index month names
	LEA	MONTHS,A0
	ADDA.W	D0,A0
	APPEND.	A0,TMP.
	ACHAR.	#'-',TMP.
	STR.	W,D2,TMP2.,#'0',#2
	APPEND.	TMP2.,TMP.
	ACHAR.	#32,TMP.		;add a trailing space
	POP	D2
	RTS

* Appends AmigaDOS time in D0in minutes since midnight to HH:MM.  
* Appends to TMP.

ConvertDosTime:
	PUSH	D2
	MOVE.L	D0,D2
	DIVU	#60,D2			;hours in low word, mins in high word
	STR.	W,D2,TMP2.,#32,#2	;convert hours
	APPEND.	TMP2.,TMP.
	ACHAR.	#':',TMP.		;format HH:MM
	SWAP	D2
	STR.	W,D2,TMP2.,#'0',#2	;minutes
	APPEND.	TMP2.,TMP.
	POP	D2
	RTS

* Moves file or dir name to TMP. and pads to 30 chars.  Ptr to name stg in A0.

MovePad:
	MOVEQ	#SPACE,D2
	MOVEQ	#29,D1
	LEA	TMP.,A1
1$:	MOVE.B	(A0)+,D0
	BEQ.S	2$
	MOVE.B	D0,(A1)+
	DBF	D1,1$			;MOVE UP TO 30 CHARS
	BRA.S	9$
2$:	MOVE.B	D2,(A1)+
	DBF	D1,2$
9$:	CLR.B	(A1)			;terminate properly
	RTS

* Scrolls the entire window one row up.

ScrollUp:
	MOVEQ	#FontHt,D1		;delta y
	BRA.S	ScrollCom
* Scrolls the entire window one row down.

ScrollDown:
	MOVEQ	#FontHt,D1		;delta y
	NEG.L	D1			;move away from 0,0
ScrollCom:
	MOVE.L	#Files_TE+1,D3		;miny
	MOVE.L	#Files_TE+Files_HT-3,D5	;maxy
	ZAP	D0			;delta x	
	MOVE.L	#Files_LE+2,D2		;minx
	MOVE.L	#Files_LE+Files_WD-3,D4	;maxx
	MOVE.L	Window,A0
	MOVE.L	wd_RPort(A0),A1
	CALLSYS	ScrollRaster,GraphicsBase
	RTS

* Called to display catalog of files to be restored or copied.

DispFileCatalog:
	BSR.S	InitDirectory
	LEA	ResCGad,A0		;if volume is valid, allow restoration
	MOVE.W	gg_Flags(A0),D0
	BCLR	#8,D0
	IFNZB	VolValidFlg,1$		;to either current or different
	BSET	#8,D0
1$:	MOVE.W	D0,gg_Flags(A0)
	LEA	CatGList,A0
	LEA	RefreshCatalog,A1
	MOVE.L	#RestoreMenu,D0
	BSR	NextWindow
	BSR	WaitForAction
	IFNZB	ProcFlg,9$
	STC
9$:	RTS

InitDirectory:
	MOVE.L	DirTree,A0
	MOVE.L	A0,ParentDir
	MOVE.L	df_Child(A0),df_CurFib(A0)
	CLR.W	CurEnt
	BSR	CountMaxEnt
	BSR	InitPath
	RTS

RefreshCatalog:
	PUSH	D5/A5
	BSR	CLRWIN
	LEA	BOX_TABLE,A0
	BSR	DRAW_BOXES
	LEA	RootGad,A0
	BSR	ColorList
	DispMsg	#CTxt_LE,#CTxt1_TE,CTxt1.,WHITE
	DispMsg	#CTxt_LE,#CTxt2_TE,CTxt2.,WHITE
	DispMsg	#CTxt_LE,#CTxt3_TE,CTxt3.,WHITE
	DispMsg	#CTxt_LE,#CTxt4_TE,CTxt4.,WHITE
	BSR.S	UpdateNumbers
	BSR	UpdatePath
	BSR	UpdateWindow
	BSR	UpdateScrollBar
	BSR	RefreshAllGads
	POP	D5/A5
	RTS


UpdateNumbers:
	BSR	RecalcNumbers
	DispMsg	#CFil_LE,#CFil_TE,CFil.,WHITE
	DispMsg	#CAct_LE,#CAct_TE,CAct.,WHITE
	MOVE.L	SelActive,D0
	MOVE.L	TotActive,D1
	BSR.S	DispCountOf
	DispMsg	#CDel_LE,#CDel_TE,CDel.,WHITE
	MOVE.L	SelDeleted,D0
	MOVE.L	TotDeleted,D1
;;	BSR.S	DispCountOf
;;	RTS

* Displays numbers in D0 and D1: "D0 of D1" at current position.

DispCountOf:
	PUSH	D2/A2
	MOVE.L	D1,D2
	LEA	-24(A7),A7	;a local temp buffer
	MOVE.L	A7,A2
	BSR.S	DispOneNbr
	DispCur	OutOf.,WHITE,BLUE
	MOVE.L	D2,D0
	BSR.S	DispOneNbr
	DispCur	Blanks.,BLUE,BLUE
	LEA	24(A7),A7
	POP	D2/A2
	RTS

DispOneNbr:
	MOVE.L	A2,A0
	STR.	L,D0,A0,#32,#6
	MOVE.L	A2,A0
	STRIP_LB. A0
	MOVE.L	A2,A0
	DispCur	A0,ORANGE,BLUE
	RTS

* Scans the catalog to recalculate selected and total files, 

RecalcNumbers:
	PUSH	D2-D6/A2
	ZAP	D2			;total deleted
	ZAP	D3			;selected deleted
	ZAP	D4			;total active
	ZAP	D5			;selected active
	MOVEQ	#fib_Selected,D6
	MOVE.L	DirTree,A2		;start at highest level
1$:	MOVE.L	df_Child(A2),A0		;start scan at this level
2$:	IFZL	A0,5$			;last item at this level
	BTST	#fib_Dir,df_Flags(A0)	;this entry a dir?
	BNE.S	3$			;yes
	BTST	#fib_NotBusy,df_Flags(A0) ;no, file...deleted item?
	BEQ.S	6$			;no...
	INCL	D2			;total files
	BTST	D6,df_Flags(A0) 	;is this selected?
	BEQ.S	4$			;no...don't count
	INCL	D3			;selected deleted
	BSET	D6,df_Flags(A2) 	;show parent selected
	BRA.S	4$
6$:	INCL	D4			;active file
	BTST	D6,df_Flags(A0) 	;selected?
	BEQ.S	4$			;no
	INCL	D5
	BSET	D6,df_Flags(A2) 	;show parent selected
4$:	MOVE.L	df_Next(A0),A0		;get link to next
	BRA.S	2$			;loop

3$:	BCLR	D6,df_Flags(A0) 	;start with dir unselected
	PUSH	A0			;yes...save our position at this level
	MOVE.L	A0,A2			;new dir becomes parent
	BRA.S	1$			;and process items of this parent

5$:	MOVE.B	df_Flags(A2),D0
	AND.B	#1,D0
	MOVE.L	df_Parent(A2),A2	;back up to previous level
	IFZL	A2,9$			;done
	OR.B	D0,df_Flags(A2)		;update parent's selected flag
	POP	A0
	MOVE.L	df_Next(A0),A0
	BRA.S	2$

9$:	MOVE.L	D2,TotDeleted
	MOVE.L	D3,SelDeleted
	MOVE.L	D4,TotActive
	MOVE.L	D5,SelActive
	POP	D2-D6/A2
	RTS

* Redisplays the subdirectory path.

Root:	PUSH	A2
	MOVE.L	DirTree,A2		;reeset to top level
	IFEQL	ParentDir,A2,Pex,L	;already there
	MOVE.L	A2,ParentDir
	BSR	InitPath
	BRA.S	PCom

Parent:	PUSH	A2
	MOVE.L	ParentDir,A2		;move back to prev level
	MOVE.L	df_Parent(A2),A2
	IFZL	A2,Pex			;already at root level...stop here
	MOVE.L	A2,ParentDir		;this is our current level
	BSR	TruncatePath

PCom:	MOVE.W	df_CurEnt(A2),CurEnt
	BSR	CountMaxEnt
	BSR	UpdatePath
	BSR	UpdateWindow
	BSR	UpdateScrollBar
Pex:	POP	A2
	JMP	ProcessMsg

InitPath:
	MOVE.	VolNameBuf.,Path.
	ACHAR.	#':',Path.
	RTS

* Appends Fib name pointed to by A0 to Path.  Limits Path. to PathSize.

AppendDrawer:
	LEA	db_DirName(A0),A0
	ADD.L	BlockSize,A0
	LEA	-(FileNameLen+2)(A7),A7
	MOVE.L	A7,A1
	BSR	MoveFileName
	MOVE.L	A7,A0
	BSR.S	AppendPath
	LEA	FileNameLen+2(A7),A7
	RTS

AppendFIB:
	LEA	df_Name(A0),A0
AppendPath:
	MOVE.W	#PathSize-3,D1		;max path with room for '/' and null
	LEA	Path.,A1
1$:	TST.B	(A1)
	BEQ.S	2$			;end of Path.?
	INCL	A1			;no
	DBF	D1,1$			;keep searching
	RTS				;oops...already too long
2$:	IFEQIB	-1(A1),#':',3$		;found device/vol
	MOVE.B	#'/',(A1)+		;else add trailing slash
3$:	MOVE.B	(A0)+,(A1)+		;add a new char
	DBEQ	D1,3$			;loop till end of new string or limit
	DECL	A1
;;	MOVE.B	#'/',(A1)+		;trailing slash
	CLR.B	(A1)			;null term
	RTS

* Deletes last subdir name from Path.

TruncatePath:
	MOVE.L	A1,-(A7)
	LEA	Path.,A0		;point to path
	IFZB	(A0),4$			;just in case...
	MOVE.L	A0,A1
1$:	TST.B	(A0)+			;find the end of it
	BNE.S	1$
	DECL	A0
;;	DECL	A0
2$:	MOVE.B	-(A0),D0		;get previous char
	IFEQIB	D0,#':',3$		;thats it...
	IFNEIB	D0,#'/',2$		;no...loop
	DECL	A0
3$:	IFLEL	A0,A1,4$		;out of range of PATH.
	CLR.B	1(A0)			;else terminate string here
4$:	MOVE.L	(A7)+,A1
	RTS
	
UpdatePath:
	LEA	DirBox,A0		;first clear the old path out of box
	BSR	ClearBox
	MOVE.	Path.,TMP.
	LEA	TMP.,A0
	CLR.B	42(A0)			;truncate to safe length
	DispMsg	#Dir_LE+4,#Dir_TE+2,CatFor.,WHITE
	DispCur	TMP.,ORANGE
	RTS

**********************************************************************

* Sets all gadgets in list in A0 to one color.

ColorList:
	PUSH	A5
	MOVE.L	A0,A5
1$:	MOVEQ	#BLACK,D0
	BSR	GAD_COLOR
	MOVE.L	(A5),A5
	MOVE.L	A5,A0
	IFNZL	A0,1$
	POP	A5
	RTS

* These routines handle some of the details involved in switching
* to a different window.  Called to initialize WindowListPtr and
* set up initial window.  First gadget list in A0, first refreshroutine
* in A1, Menu bar in D0.

* Called with ptr to special gadgets in A0, new refreshroutine in A1, and
* menu strip in D0.

PushWindow:
	PUSH	A2-A5
	MOVE.L	A0,A2
	MOVE.L	A1,A3
	MOVE.L	D0,A5
	MOVE.L	WindowListPtr,A4
	MOVE.L	(A4),A0			;get ptr to current gadget list
	BSR	RemoveGadgets		;remove gadgets from current window
	BSR	RemoveMenuStrip
	ADD.W	#12,A4			;advance to next entry
	MOVE.L	A4,WindowListPtr
	MOVE.L	A2,(A4)			;store gadget list for next window
	MOVE.L	A3,4(A4)		;refresh routine for next window
	MOVE.L	A5,8(A4)
	MOVE.L	A5,A0
	BSR	AddMenuStrip
	MOVE.L	A3,RefreshRoutine
	ZAP	D0			;for RefreshFunctions
	JSR	(A3)			;refresh window

* Don't fart with this...refreshroutine may change gadget list

	MOVE.L	(A4),A0			;get ptr to new gadget list 
	BSR	AddGadgets
	POP	A2-A5
	RTS

* Removes current window, and restores previous window and menustrip.

PopWindow:
	PUSH	A2
	MOVE.L	WindowListPtr,A2
	MOVE.L	(A2),A0			;get current gadget list
	BSR	RemoveGadgets
	SUB.W	#12,A2			;backup to previous entry
	MOVE.L	A2,WindowListPtr
	BSR	RemoveMenuStrip
	MOVE.L	8(A2),A0
	BSR	AddMenuStrip
	MOVE.L	4(A2),A0		;previous refreshroutine
	MOVE.L	A0,RefreshRoutine
	MOVEQ	#1,D0			;else set flag=no gadget changes
	JSR	(A0)			;refresh previous window
	MOVE.L	(A2),A0			;get previous gadget list
	BSR	AddGadgets		;restore gadgets
	POP	A2
	RTS

InitWindowList:
	PUSH	A2-A5
	MOVE.L	A0,A2
	MOVE.L	A1,A3
	MOVE.L	D0,A5
	LEA	WindowList,A4
	MOVE.L	A4,WindowListPtr
	BRA.S	NxtCom

* Advances to next window at current level.  Gagdet list in A0,
* RefreshRoutine in A1, and menustrip in D0.

NextWindow:
	PUSH	A2-A5
	MOVE.L	A0,A2
	MOVE.L	A1,A3
	MOVE.L	D0,A5
	MOVE.L	WindowListPtr,A4
	MOVE.L	(A4),A0			;get ptr to current gadget list
	IFZL	A0,NxtCom		;no entry...nothing to remove
	BSR	RemoveGadgets		;remove gadgets from current window
	BSR	RemoveMenuStrip
NxtCom:	MOVE.L	A2,(A4)			;store gadget list for next window
	MOVE.L	A3,4(A4)		;refresh routine for next window
	MOVE.L	A5,8(A4)		;menustrip
	MOVE.L	A5,A0
	BSR	AddMenuStrip
	MOVE.L	A3,RefreshRoutine
	ZAP	D0			;this is for RefreshFunctions
	JSR	(A3)			;refresh window

* Don't fart with this...refreshroutine may change gadget list

	MOVE.L	(A4),A0
	BSR	AddGadgets
	POP	A2-A5
	RTS

* Called to change menu strips for current menu

NextMenu:
	PUSH	A2
	MOVE.L	WindowListPtr,A2
	MOVE.L	A0,8(A2)		;save new menustrip
	BSR.S	RemoveMenuStrip
	MOVE.L	8(A2),A0
	BSR.S	AddMenuStrip
	POP	A2
	RTS


RemoveMenuStrip:
	MOVE.L	Window,A0
	CALLSYS	ClearMenuStrip,IntuitionBase
	RTS

AddMenuStrip:
	IFZL	A0,1$
	MOVE.L	A0,A1
	MOVE.L	Window,A0
	CALLSYS	SetMenuStrip,IntuitionBase
1$:	RTS

***********************************************************************
* Mark all deleted files as "Included".

IncDel:	PUSH	A2
	MOVE.L	DirTree,A2		;start at highest level
1$:	MOVE.L	df_Child(A2),A0		;start scan at this level
2$:	IFZL	A0,5$			;last item at this level
	LEA	df_Flags(A0),A1
	BTST	#fib_Dir,(A1)		;this entry a dir?
	BNE.S	3$			;yes
	BTST	#fib_NotBusy,(A1)	;no, file...deleted item?
	BEQ.S	4$			;no...go on to next item
	BSET	#fib_Selected,(A1)	;mark it selected
4$:	MOVE.L	df_Next(A0),A0		;get link to next
	BRA.S	2$			;loop

3$:	PUSH	A0			;yes...save our position at this level
	MOVE.L	A0,A2			;new dir becomes parent
	BRA.S	1$			;and process items of this parent

5$:	MOVE.L	df_Parent(A2),A2	;back up to previous level
	IFZL	A2,9$			;done
	POP	A0
	MOVE.L	df_Next(A0),A0
	BRA.S	2$

9$:	POP	A2
	BRA	SDExit

ExcDel:	PUSH	A2
	MOVE.L	DirTree,A2		;start at highest level
1$:	MOVE.L	df_Child(A2),A0		;start scan at this level
2$:	IFZL	A0,5$			;last item at this level
	LEA	df_Flags(A0),A1
	BTST	#fib_Dir,(A1)		;this entry a dir?
	BNE.S	3$			;yes
	BTST	#fib_NotBusy,(A1)	;no, file...deleted item?
	BEQ.S	4$			;no...go on to next item
	BCLR	#fib_Selected,(A1)	;mark it selected
4$:	MOVE.L	df_Next(A0),A0		;get link to next
	BRA.S	2$			;loop

3$:	PUSH	A0			;yes...save our position at this level
	MOVE.L	A0,A2			;new dir becomes parent
	BRA.S	1$			;and process items of this parent

5$:	MOVE.L	df_Parent(A2),A2	;back up to previous level
	IFZL	A2,9$			;done
	POP	A0
	MOVE.L	df_Next(A0),A0
	BRA.S	2$

9$:	POP	A2
	BRA	SDExit

IncAct:	PUSH	A2
	MOVE.L	DirTree,A2		;start at highest level
1$:	MOVE.L	df_Child(A2),A0		;start scan at this level
2$:	IFZL	A0,5$			;last item at this level
	LEA	df_Flags(A0),A1
	BTST	#fib_Dir,(A1)		;this entry a dir?
	BNE.S	3$			;yes
	BTST	#fib_NotBusy,(A1)	;no, file...deleted item?
	BNE.S	4$			;no...go on to next item
	BSET	#fib_Selected,(A1)	;mark it selected
4$:	MOVE.L	df_Next(A0),A0		;get link to next
	BRA.S	2$			;loop

3$:	PUSH	A0			;yes...save our position at this level
	MOVE.L	A0,A2			;new dir becomes parent
	BRA.S	1$			;and process items of this parent

5$:	MOVE.L	df_Parent(A2),A2	;back up to previous level
	IFZL	A2,9$			;done
	POP	A0
	MOVE.L	df_Next(A0),A0
	BRA.S	2$

9$:	POP	A2
	BRA	SDExit

ExcAct:	PUSH	A2
	MOVE.L	DirTree,A2		;start at highest level
1$:	MOVE.L	df_Child(A2),A0		;start scan at this level
2$:	IFZL	A0,5$			;last item at this level
	LEA	df_Flags(A0),A1
	BTST	#fib_Dir,(A1)		;this entry a dir?
	BNE.S	3$			;yes
	BTST	#fib_NotBusy,(A1)	;no, file...deleted item?
	BNE.S	4$			;no...go on to next item
	BCLR	#fib_Selected,(A1)	;mark it selected
4$:	MOVE.L	df_Next(A0),A0		;get link to next
	BRA.S	2$			;loop

3$:	PUSH	A0			;yes...save our position at this level
	MOVE.L	A0,A2			;new dir becomes parent
	BRA.S	1$			;and process items of this parent

5$:	MOVE.L	df_Parent(A2),A2	;back up to previous level
	IFZL	A2,9$			;done
	POP	A0
	MOVE.L	df_Next(A0),A0
	BRA.S	2$

9$:	POP	A2
	BRA	SDExit

IncAll:	PUSH	A2
	MOVE.L	DirTree,A2		;start at highest level
1$:	MOVE.L	df_Child(A2),A0		;start scan at this level
2$:	IFZL	A0,5$			;last item at this level
	LEA	df_Flags(A0),A1
	BTST	#fib_Dir,(A1)		;this entry a dir?
	BNE.S	3$			;yes
	BSET	#fib_Selected,(A1)	;mark it selected
	MOVE.L	df_Next(A0),A0		;get link to next
	BRA.S	2$			;loop

3$:	PUSH	A0			;yes...save our position at this level
	MOVE.L	A0,A2			;new dir becomes parent
	BRA.S	1$			;and process items of this parent

5$:	MOVE.L	df_Parent(A2),A2	;back up to previous level
	IFZL	A2,9$			;done
	POP	A0
	MOVE.L	df_Next(A0),A0
	BRA.S	2$

9$:	POP	A2
	BRA	SDExit

ExcAll:	PUSH	A2
	MOVE.L	DirTree,A2		;start at highest level
1$:	MOVE.L	df_Child(A2),A0		;start scan at this level
2$:	IFZL	A0,5$			;last item at this level
	LEA	df_Flags(A0),A1
	BTST	#fib_Dir,(A1)		;this entry a dir?
	BNE.S	3$			;yes
	BCLR	#fib_Selected,(A1)	;mark it selected
	MOVE.L	df_Next(A0),A0		;get link to next
	BRA.S	2$			;loop

3$:	PUSH	A0			;yes...save our position at this level
	MOVE.L	A0,A2			;new dir becomes parent
	BRA.S	1$			;and process items of this parent

5$:	MOVE.L	df_Parent(A2),A2	;back up to previous level
	IFZL	A2,9$			;done
	POP	A0
	MOVE.L	df_Next(A0),A0
	BRA.S	2$

9$:	POP	A2
;	BRA	SDExit

SDExit:	BSR	UpdateNumbers
	BSR	UpdateWindow		;redisplay window
	RTS

* Initializes the tree structure so that NextFile can return the selected
* files in order.

InitTree:
	MOVE.L	DirTree,A0
	MOVE.L	A0,ParentDir
	LEA	df_Child(A0),A1		;tricky...
	MOVE.L	A1,df_CurFib(A0)
	BSR	InitPath
	RTS

* Advances to next selected file in tree structure.  Returns ptr in A0 or 
* CY=1 if end of list.

NextFile:
	PUSH	A2
	MOVE.L	ParentDir,A2
	MOVE.L	df_CurFib(A2),A0	;ptr to current item
1$:	MOVE.L	df_Next(A0),A0		;get fwd link
	IFZL	A0,3$			;pop to previous level
	MOVE.B	df_Flags(A0),D0		;get the flags
	BTST	#fib_Dir,D0		;directory?
	BNE.S	2$			;yes...drop down a level
	BTST	#fib_NotBusy,D0		;no, a file...deleted file?
	BEQ.S	1$			;not deleted, go on
	BTST	#fib_Selected,D0	;selected to be restored?
	BEQ.S	1$			;no...skip over
	MOVE.L	A0,df_CurFib(A2)	;save ptr to this entry
	BRA.S	9$			;and return to caller
2$:	MOVE.L	A0,A2			;new entry is now parent
	MOVE.L	A2,ParentDir
	BSR	AppendFIB
	LEA	df_Child(A2),A0		;init to first entry
	MOVE.L	A0,df_CurFib(A2)
	BRA.S	1$
3$:	BSR	TruncatePath
	MOVE.L	A2,A0			;make parent the current entry
	MOVE.L	df_Parent(A2),A2	;back up a level
	MOVE.L	A2,ParentDir
	IFNZL	A2,1$			;valid level, scan this level
	STC				;else bail out
9$:	POP	A2
	RTS


ProcCat:
	SETF	ProcFlg
	JMP	ProcessMsg

CanCat:
	SETF	CancelFlg
	JMP	ProcessMsg

BOX	MACRO	;LE,TE,WD,HT,BORDER PEN,FILL PEN
	DC.W	\1,\2,\3,\4
	IFEQ	NARG-6
	DC.B	\5,\6
	ENDC
	IFEQ	NARG-4
	DC.B	1,0
	ENDC
	ENDM

BOX_TABLE
FileBox	BOX	Files_LE,Files_TE,Files_WD,Files_HT,WHITE,BLACK
;	BOX	SSBox_LE,SSBox_TE,SSBox_WD,SSBox_HT
;	BOX	RPBox_LE,RPBox_TE,RPBox_WD,RPBox_HT
DirBox	BOX	Dir_LE,Dir_TE,Dir_WD,Dir_HT
;	BOX	Cmd_LE,Cmd_TE,Cmd_WD,Cmd_HT,ORANGE,WHITE
	DC.L	0

Blanks.	DC.B	'   ',0

	BSS,PUBLIC

* This is a clean way to handle windows.  Used by PushWindow, PopWindow,
* and NextWindow.  Each entry is 12 bytes long: Gadget list ptr, Refresh
* Routine, and MenuBar for that window.  No max level check, so watch it!

RefreshRoutine	
	DS.L	1	;ptr to current refresh routine

WindowList:
	DS.L	3
	DS.L	3
	DS.L	3
	DS.L	3

ParentDir	DS.L	1	;ptr to parent dir for current level
WindowListPtr	DS.L	1
SelDeleted	DS.L	1	;actual count of restorable files
TotDeleted	DS.L	1	;total count of files available to restore
SelActive	DS.L	1	;selected count of active files
TotActive	DS.L	1	;total count of active files
FilTotPos	DS.W	1
MaxEnt		DS.W	1	;max number of entries in list
CurEnt		DS.W	1	;number of current entry
UpdWinFlg	DS.B	1

Path.	DS.B	PathSize

	END

