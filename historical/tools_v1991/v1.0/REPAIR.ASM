;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*****************************************************************
*								*
*		REPAIR.ASM					*
*								*
*	Quarterback Tools Drawer/File Repair Functions		*
*								*
*****************************************************************

	INCLUDE "vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"VD0:BOXES.ASM"

	XDEF	Function11,Function12
	XDEF	Function21
	XDEF	VerifyCtrlBlock
	XDEF	CheckKey
	XDEF	ValidateName

	XDEF	CurrentBlkType
	XDEF	LocalDTS
	XDEF	RepairFlg,VolBadFlg
	XDEF	NewName.
	XDEF	MajorErrCnt,MinorErrCnt
	XDEF	BadBlockCount
	XDEF	TotalDirCount
	XDEF	BadDirCount
	XDEF	TotalFileCount
	XDEF	BadFileCount
	XDEF	ErrorCount

* Externals in VTEST.ASM

	XREF	ProcessMsg
	XREF	InsertVolName

	XREF	CurrentBlock
	XREF	ProcFlg
	XREF	CancelFlg
	XREF	AbortFlg
	XREF	SkipFlg
	XREF	YesFlg
	XREF	PauseFlg

* Externals in TOOLS.ASM

	XREF	MLoop,MLoopErr
	XREF	RefreshFunctions

	XREF	SysBase,DosBase,IntuitionBase,GraphicsBase
	XREF	Window,TMP.,TMP2.
	XREF	MSG_CLASS,MSG_CODE,GAD_PTR
	XREF	RefreshRoutine
	XREF	ToolsTask
	XREF	VolNameBuf.
	XREF	FunGadList

* Externals in FILES.ASM

	XREF	CalcHash
	XREF	LinkParentDir
	XREF	CheckBadBlocks
	XREF	GetBadStatus
	XREF	DeleteFile

	XREF	BadBlkFlg
	XREF	BadBlks.

* Externals in Device.asm

	XREF	CheckVolumeStatus,CheckDiskLoaded
	XREF	CalcVolParams
	XREF	OpenDriver,CloseDriver
	XREF	InhibitDos,EnableDos
	XREF	CheckProtection

	XREF	NumSoftErrors
	XREF	UnitNumber
	XREF	DiskState
	XREF	NumBlocks
	XREF	NumBlocksUsed
	XREF	BytesPerBlock
	XREF	DiskType
	XREF	VolumeNode
	XREF	InUse	
	XREF	MaxBytes
	XREF	MaxBlocks
	XREF	FreeBytes
	XREF	FreeBlocks
	XREF	VolumeOffset
	XREF	LowCyl
	XREF	HighCyl
	XREF	MaxCylinders
	XREF	Surfaces
	XREF	MaxTracks
	XREF	BlockSize,BlockSizeL
	XREF	Reserved
	XREF	BlocksPerTrack
	XREF	NumBuffers
	XREF	BufMemType
	XREF	MaxTransfer
	XREF	PercentFull
	XREF	DosType,FFSFlg
	XREF	DeviceNameBuf.
	XREF	DriverNameBuf.
	XREF	VolValidFlg
	XREF	NoDiskFlg
	XREF	NotDosFlg
	XREF	NotValidFlg
	XREF	WrtProtFlg
	XREF	VolStatusMsg
	XREF	BitmapBlocks
	XREF	LowestKey,HighestKey
	XREF	BitmapBlkBits
	XREF	MaxKeys
	XREF	HashTblSize
	XREF	BlkSizeShift
	XREF	RootBlkNbr

* Externals in Gadgets.asm

	XREF	RightPGad,CanGad,PauseGad
	XREF	ReqProceed,ReqYes,ReqFFSGad
	XREF	CopyWarn
	XREF	BootFileSys

* Externals in Messages.asm

	XREF	BadScanFind.,BadScanFix.
	XREF	RootRdErr.
	XREF	RootErr.,BMErr.

* Externals in Fun.asm

	XREF	CLRWIN
	XREF	RefreshAllGads

* Externals in DISK.ASM

	XREF	ReadBlock,WriteBlock
	XREF	MotorOff

* Externals in CAT.ASM

	XREF	PushWindow,PopWindow,NextWindow
	XREF	UpdWinFlg,Path.
	XREF	ScrollWindow
	XREF	InitPath,AppendPath,TruncatePath

* Externals in BUFFERS.ASM

	XREF	ReadControl,ReadData
	XREF	AllocCacheBuffers,FreeCacheBuffers
	XREF	GetCacheBuffer,PutCacheBuffer
	XREF	ZapBlock,ZapAnyBlock
	XREF	AllocateBuffer,AllocateIOBuf
	XREF	ReadCache,WriteCache,FlushCache,WrtBlkCkSum
	XREF	AllocateBlock,FreeBlock
	XREF	LinkCache,LinkCacheKey
	XREF	UnLinkCache
	XREF	FreeBadBlkBuf

	XREF	BlockBuffer

* Externals in BITMAP.ASM

;;	XREF	ReadBitmap
	XREF	WriteBitmap
	XREF	BitmapDataPtr,FreeBMBuffers,CalcCkSum
	XREF	MarkKeyFree,MarkKeyBusy,GetBusyStatus
	XREF	AllocateEmptyBitmap,AllocFreeKey
	XREF	LoadBitmapKeys,StoreBitmapKeys
	XREF	BitmapDataPtr,BitmapKeyBuffer

	XREF	LastUsedKey

* Externals in RESTORE.ASM

	XREF	Function22
	XREF	MoveFileName
	XREF	FileName.

* Externals in REPORT.ASM

	XREF	ReportError,InitReport,CloseReport
	XREF	ReportVolStatus,RootFailure
	XREF	DisplayPath,DisplayFile
	XREF	UpdateNames

* Externals in AREXX.ASM

	XREF	ArexxFlg

* Externals in FORMAT.ASM

	XREF	InitBoot
	XREF	NewType

REPORT	MACRO	;error code, action code, label
	IFNC	'\3',''
	BCC.S	*+14
	MOVEQ	#\1,D0
	MOVEQ	#\2,D1
	BSR.L	ReportError
	BRA.L	\3
	ELSE
	MOVEQ	#\1,D0
	MOVEQ	#\2,D1
	BSR	ReportError
	ENDC
	ENDM

* Scan volume for corrupted directories
* Checks files on volume for linkage errors and read errors.

Function11:
	CLR.B	RepairFlg
	BRA.S	FunCom

Function21:
	SETF	RepairFlg
	BSR	CheckVolumeStatus
	ERROR	MLoopErr		;problems
	BSR	CheckDiskLoaded
	ERROR	MLoopErr
	BSR	CheckProtection		;make sure we can write, if repairing
	ERROR	MLoopErr		;op canceled
	BSR	CheckBootBlock		;check and fix boot block
	ERROR	MLoopErr
	DispReq	CopyWarn,ReqYes
	IFNZB	YesFlg,Function22		

Function12:
	SETF	RepairFlg
FunCom:	BSR	CheckVolumeStatus
	ERROR	MLoopErr		;problems
	BSR	CheckDiskLoaded
	ERROR	MLoopErr
	IFZB	RepairFlg,1$
	BSR	CheckProtection		;make sure we can write, if repairing
	ERROR	MLoopErr		;op canceled
1$:	BSR	CalcVolParams		;calc volume params
;	BSR	SetVolProtected
	BSR	InhibitDos		;keep DOS out of our hair
	CLR.B	CancelFlg
	BSR	InitPath
	LEA	PauseGad,A0
	LEA	Fun11Refresh,A1
	ZAP	D0
	BSR	PushWindow
	BSR	AllocateEmptyBitmap
	ERROR	Fun11Error
	BSR	AllocateBlock
	BEQ	Fun11Error
	BSR	InitReport
	IFNZB	CancelFlg,Fun11Error
	BSR	CheckBadBlocks
	BSR	ScanDirs
	NOERROR	2$
	IFNZB	CancelFlg,Fun11Exit
	BSR	RootFailure
	BRA.S	Fun11Exit
2$:	IFNZB	AbortFlg,Fun11Exit	;check for operator abort
	IFNZB	CancelFlg,Fun11Exit	;op has canceled
	ZAPA	A0
	BSR	DisplayFile		;clear final file name
	BSR	ProcessBitmapKeys	;verify bitmap, mark blocks busy
	BSR	RepairBitmap		;check new and old bitmaps...
	BSR	WriteCache		;force final writes
	BSR	CloseReport
	BSR	EnableDos
	BSR	ReportVolStatus
Fun11Exit:
;	BSR	FlushCache

Fun11Error:
	BSR	EnableDos
	BSR	CloseReport
	BSR	FreeBadBlkBuf
	BSR	FreeBMBuffers
	BSR	FreeBlock
	BSR	PopWindow		;restore window
	CLR.B	RepairFlg
	MOVE.W	MinorErrCnt,D0
	ADD.W	MajorErrCnt,D0
	BEQ	MLoop
	BRA	MLoopErr

Fun11Refresh:
	PUSH	D2/A2-A4
	BSR	CLRWIN
	LEA	BadScanFind.,A0
	IFZB	RepairFlg,1$
	LEA	BadScanFix.,A0
1$:	BSR	InsertVolName		;insert volume
	DispCent #320,#BSTitle_TE,A0,WHITE
	BSR	UpdateNames
	LEA	PauseGad,A0
	GadColor A0,ORANGE
	LEA	CanGad,A0
	GadColor A0,BLACK
	BSR	RefreshAllGads
	POP	D2/A2-A4
	RTS

* Starting with root dir, examines each dir and file, marking blocks busy
* in dummy bitmap.  Checks for corruption in each dir, file header, and
* extension, and watches for cross-linked files.  The goal is to find and
* correct any sort of problem which might cause ADOS and/or QB TOOLS to
* crash or otherwise be unable to access files properly.

* Where problems are found, warns the operator and presents various options 
* for correcting the problem.  WARNING -- This routine uses recursion to 
* handle directory levels.  Only stack space limits the number of directory
* levels supported (about 36 bytes per level).

ScanDirs:
	ZAP	D0
	MOVE.W	D0,BadDirCount
	MOVE.W	D0,BadFileCount
	MOVE.W	D0,TotalDirCount
	MOVE.W	D0,TotalFileCount
	MOVE.W	D0,MinorErrCnt
	MOVE.W	D0,MajorErrCnt
	MOVE.W	D0,BadBlockCount
	MOVE.B	D0,BitmapErrFlg
	MOVE.B	D0,CurrentBlkType
;	BSR	CheckBootBlock		;verify and repair boot block
;	RTSERR
;	MOVEQ	#1,D0			;force rigorous checks
	BSR	ReadRoot		;perform rigorous root checks
	RTSERR
;;	INCW	TotalDirCount		;count root
	MOVE.L	RootBlkNbr,D0
	BSR	MarkKeyBusy
	MOVE.L	RootBlkNbr,D0
;	BRA.S	ProcessDir

***************************************************************************
* Warning - this is VERY complex code.  Don't fuck with it unless you are *
* VERY SURE you know what you are doing!!!				  *
***************************************************************************

* Calls itself for each lower dir level.  This code checks out the hash table
* entries for a dir and processes each.  If an entry fails, it is unlinked
* from the hash chain and the test continues.  The current dir is assumed
* to have passed basic tests as an entry in parent's hashchain, except for
* the root directory, which is checked independently above.

* Register usage:
*	D2=key of current directory
*	D3=offset into hashtable for chain being checked
*	D4=key of current hashchain entry
*	D5=loop counter for hashtable
*	D6=hashchain predecessor: 0 (parent dir) or key of predecessor
*	D7=hash chain fwd link
*	A2=current dir block
*	A3=current entry block
*	A4=current entry block+blocksize
*	A5=sort flag

ProcessDir:
	PUSH	D2-D7/A2-A5
	INCW	TotalDirCount
	MOVE.L	D0,D2			;key of dir being checked
	MOVE.L	D0,CurDirKey
	BSR	DisplayPath
	MOVE.L	D2,D0
	BSR	ReadControl		;should get cache hit even if cksum err
	ERROR	ProcDirEx		;should never happen...
	MOVE.L	A0,A2			;save ptr to dir
	MOVE.L	A0,CurDirBuf
	BSR	UnLinkCache		;get buffer out of busyqueue/cache lists
	MOVEQ	#-4,D3			;hashtable offset for first increment
	MOVE.L	HashTblSize,D5
	BRA	AdvanceHash

* Ready to scan the dir's hashchains.

NextHash:
	ZAPA	A5			;no sort needed (yet)
	CLR.B	SkipFlg
	ZAP	D6			;predecessor is parent dir
	MOVE.L	db_HashTable(A2,D3.W),D4 ;get hash table entry
	BEQ	AdvanceHash		;no entry...try next
NextLink:
	BSR	ProcessMsg		;check for op action
	IFNZB	AbortFlg,ProcDirEx,L	;op has aborted
	IFNZB	CancelFlg,ProcDirEx,L	;op has canceled
	CLR.B	FileName.		;no file...yet
	CLR.W	ErrorCount		;no non-fatal errors, yet
	ZAP	D7			;no fwd link
	MOVE.L	D4,D0
	BSR	CheckKey
	REPORT	-1,1,BadEntry		;key range error, entry deleted
	BSR	GetCacheBuffer		;get a buffer for read
	ERROR	ProcDirEx		;bad awful...
	MOVE.L	A0,A3			;ptr to current entry data block
	MOVE.L	D4,D0
	BSR	ReadBlock		;try to read it
	NOERROR	15$
	INCW	BadBlockCount
	REPORT	-7,1
	BRA	BadEntry		;can't read entry...get rid of it
15$:;;	MOVE.L	D4,D0
;;	MOVEQ	#-1,D1
;;	BSR	LinkCacheKey
	MOVE.L	A3,A4
	ADD.L	BlockSize,A4
	LEA	fh_FileName(A4),A0
	BSR	CheckName
	ERROR	ProcDirEx
	MOVE.L	A3,A0
	BSR	CalcCkSum
	IFZL	D0,1$			;good checksum...go on
	INCW	ErrorCount		;checksum error...try to use it/fix it
	REPORT	2,2
	ERROR	ProcDirEx		;cancelled
	MOVE.L	A3,A0
	BSR	FixCkSum		;fix if repair
1$:	MOVE.L	A3,A0
	BSR	CheckCtrlTypes		;check for legal control block types
	REPORT	-3,1,BadEntry		;bad control block: major problem
	MOVE.B	D0,CurrentBlkType	;save for error reporting
	MOVEQ	#1,D1
	IFNEL	D0,D1,13$		;not root
	REPORT	-4,1			;claims to be root!!
	BRA	BadEntry
13$:;	MOVE.L	A3,A4
;	ADD.L	BlockSize,A4
;	LEA	fh_FileName(A4),A0
;	BSR	CheckName
;	ERROR	ProcDirEx
	MOVE.L	D4,D0
	BSR	GetBusyStatus
	BNE.S	11$			;isn't busy...
	MOVE.L	D4,D0			;entry points to busy block
	BSR	SaveBadKey
	REPORT	-5,1			;linkage error
	BRA	BadEntry		;...bail out

11$:	MOVE.L	fh_Ownkey(A3),D0
	IFEQL	D0,D4,3$		;good ownkey
	INCW	ErrorCount		;thinks he is someone else...
	BSR	SaveBadKey		;remember this key
	MOVE.L	D4,fh_Ownkey(A3)	;else correct ownkey
	REPORT	6,3
	ERROR	ProcDirEx
3$:	IFZL	fh_Skip1(A3),4$		;no garbage
	INCW	ErrorCount		;garbage in Skip1
	CLR.L	fh_Skip1(A3)		;else fix the problem
	REPORT	0,3
	ERROR	ProcDirEx
4$:
* The following error checks are more or less in order of decreasing
* significance, and are common to both directories and filehdrs.

	MOVE.L	fh_HashChain(A4),D7	;get his fwd link
	MOVE.L	D7,D0
	BEQ	5$			;no fwd link
	BSR	CheckKey
	NOERROR	2$
	INCW	ErrorCount		;garbage fwd link
	INCW	MajorErrCnt
	ZAP	D7
	REPORT	-8,1
	ERROR	ProcDirEx
	BRA.S	5$

2$:	BSR	GetBusyStatus		;don't link into a loop...
	BNE.S	10$			;not busy...all is well
8$:	INCW	ErrorCount
	INCW	MajorErrCnt
	MOVE.L	D7,D0
	BSR	SaveBadKey
;	INCW	ErrorCount		;garbage fwd link
;	INCW	MajorErrCnt
	ZAP	D7
	REPORT	-38,13
	ERROR	ProcDirEx
	BRA.S	5$
	
10$:	IFGTL	D7,D4,5$		;make sure links fwd
	INCL	A5			;need to relink hash chain at end
5$:	MOVE.L	fh_Parent(A4),D0
	IFEQL	D0,D2,6$		;parent checks out
	INCW	ErrorCount		;else wrong parent
	BSR	SaveBadKey
	MOVE.L	D2,fh_Parent(A4)	;else correct parent link
	REPORT	9,3
	ERROR	ProcDirEx
6$:	LEA	fh_MDate(A4),A0
	MOVE.L	A3,A1
	BSR	CheckDateTime
	LEA	fh_Comment(A4),A0
	BSR	ValidateComment

	MOVE.W	ErrorCount,D0
;;	IFGTIW	D0,#5,BadEntry,L	;too many errors...(arbitrary)
	IFZW	D0,12$			;no changes...no need to write
	ADD.W	D0,MinorErrCnt		;count minor fixes
	IFZB	RepairFlg,12$		;not repairing...don't mark dirty
	MOVE.L	A3,A0
	DIRTY	A0			;mark entry dirty for rewrite

* entry passed basic tests...now process the entry

12$:	MOVE.L	D4,D0
	BSR	MarkKeyBusy
	MOVE.L	D4,D0			;key
	MOVEQ	#-1,D1			;type=control
	MOVE.L	A3,A0
	BSR	LinkCacheKey		;link to busy queue...no BadEntrys now
	LEA	fh_FileName(A4),A0
	BSR	CalcHash		;hash the file/dir name...
	IFEQL	D0,D3,7$		;hashed into proper chain
	MOVE.L	D0,SaveHashVal		;goes into this chain
	INCW	MajorErrCnt
	REPORT	-10,4
	ERROR	ProcDirEx
	IFZB	RepairFlg,7$		;not repairing...don't relink
	BSR	RelinkHashChain		;relink into proper hash chain
;;	ZAP	D7
	IFGTL	SaveHashVal,D3,16$	;will find him in proper place
	ZAP	D7			;force no fwd link
	BRA.S	7$			;missed him earlier...check him now

16$:	MOVE.L	D4,D0			;else catch him later
	BSR	MarkKeyFree		;else free filehdr...check him later
	MOVE.L	D6,D4			;predecessor
	BRA	EntryEnd		;do next one

7$:	MOVE.L	fh_SecType(A4),D0	;get secondary type again
	IFNEIW	D0,#-3,9$		;not a file
	MOVE.L	D4,D0
	MOVE.L	D2,D1			;key of parent
	BSR	ProcessFile		;check out file, mark data blocks busy
;;	ERROR	BadEntry		;critical problems...get rid of it
	BRA	EntryEnd

9$:	IFNEIW	D0,#2,BadEntry,L	;not userdir...
	LEA	fh_FileName(A4),A0
	LEA	TMP2.,A1
	BSR	MoveFileName
	LEA	TMP2.,A0
	BSR	AppendPath
	MOVE.L	D4,D0			;key of dir to process
	BSR	ProcessDir		;recursive call
	MOVE.L	A2,CurDirBuf
	MOVE.L	D2,CurDirKey
	BSR	TruncatePath
	BSR	DisplayPath		;redisplay dir path
EntryEnd:
	IFNZB	CancelFlg,ProcDirEx
	MOVE.L	D4,D6			;current block becomes predecessor
	MOVE.L	D7,D4			;fwd hash link if any
	BNE	NextLink

	IFZL	A5,AdvanceHash		;no sort required
	IFZB	FFSFlg,AdvanceHash	;not FFS, don't need to sort entries
	REPORT	-11,5
	ERROR	ProcDirEx
	INCW	MajorErrCnt
	IFZB	RepairFlg,AdvanceHash	;not repairing...let it go
;;	MOVE.L	D2,D0			;sort this hash chain offset
;;	MOVE.L	A2,A0			;dir block
	BSR	SortHashChain		;sort the chain
AdvanceHash:
	IFNZB	CancelFlg,ProcDirEx
	ADDQ.L	#4,D3			;advance to next hash table entry
	DBF	D5,NextHash		;loop for all hashtable entries
	IFZB	RepairFlg,1$		;not repairing, release buffer
	IFZW	bc_Dirty(A2),1$		;dir block not dirty...
	MOVE.L	A2,A0
	MOVE.L	D2,D0
	BSR	WrtBlkCkSum		;else write to disk
1$:	MOVE.L	A2,A0
	BSR	PutCacheBuffer		;release the dir buffer
ProcDirEx:
	POP	D2-D7/A2-A5		;we'll ignore CY...
	RTS

* Come here on a bad entry in a dir.  Unlink the bad entry

BadEntry:
	INCW	BadDirCount
	INCW	MajorErrCnt		;probable loss of data
;;	MOVE.L	A3,A0
;;	BSR	UnLinkCache
	MOVE.L	A3,A0
	BSR	PutCacheBuffer		;release entry buffer
	IFZB	RepairFlg,9$		;don't try to fix linkage
	MOVE.L	D6,D0
	BNE.S	1$			;we have a predecessor
	MOVE.L	D7,db_HashTable(A2,D3.W) ;store fwd hash table entry
	DIRTY	A2
	BRA.S	9$
1$:	BSR	ReadControl		;reread predecessor
	ERROR	9$
	DIRTY	A0
	ADD.L	BlockSize,A0
	MOVE.L	D7,db_HashChain(A0)	;fix linkage
9$:	MOVE.L	D6,D4
	BRA	EntryEnd

* Sorts hashchain at offset D3 into dir block.  This is necessary only
* if FFS, where blocks must be sorted in ascending order.

SortHashChain:
	PUSH	D4-D6/A2-A4
	MOVE.L	D2,D0
	MOVEQ	#-1,D1
	MOVE.L	A2,A0
	BSR	LinkCacheKey		;put into cache for a sec...
1$:	MOVE.L	D2,D0			;key of current dir
	BSR	ReadControl
	ERROR	9$
	MOVE.L	A0,A2			;ptr to current dir
	DIRTY	A0
	ZAP	D6			;sort flag
	LEA	db_HashTable(A2,D3.W),A3
2$:	MOVE.L	(A3),D5 		;get hash table entry
	MOVE.L	D5,D0
	BEQ.S	4$
	BSR	ReadControl		;read entry
	ERROR	9$
	DIRTY	A0
	LEA	db_HashChain(A0),A4
	ADD.L	BlockSize,A4
	MOVE.L	(A4),D4			;get hashlink
	BEQ.S	4$			;none...done
	IFGEL	D4,D5,3$		;no swap...in correct order
	INCW	D6			;else show something swapped
	MOVE.L	D4,D0
	BSR	ReadControl
	ERROR	9$
	DIRTY	A0
	LEA	db_HashChain(A0),A0
	ADD.L	BlockSize,A0
	MOVE.L	D4,(A3)
	MOVE.L	(A0),(A4)
	MOVE.L	D5,(A0)
	MOVE.L	A0,A4
3$:	MOVE.L	A4,A3
	BRA	2$

4$:	IFNZW	D6,1$,L			;something swapped...check again
9$:	MOVE.L	A2,A0
	BSR	UnLinkCache
	POP	D4-D6/A2-A4
	RTS

* Key of control block to relink in D4.  Unlinks from current chain and
* relinks into proper chain.  D6=key of predecessor or 0 if dir.

RelinkHashChain:
	MOVE.L	D6,D0			;hash chain pred or dir if 0
	BNE.S	1$
	MOVE.L	D7,db_HashTable(A2,D3.W) ;update dir's hash table entry
	DIRTY	A2
	BRA.S	2$

1$:	BSR	ReadControl		;read predecessor
	ERROR	9$
	DIRTY	A0
	LEA	fh_HashChain(A0),A0
	ADD.L	BlockSize,A0
	MOVE.L	D7,(A0)			;fix pred's fwd link, unlinking entry
2$:	MOVE.L	D2,D0
	MOVEQ	#-1,D1
	MOVE.L	A2,A0
	BSR	LinkCacheKey		;put into cache for a sec...
	MOVE.L	D4,D0			;relink this key
	MOVE.L	D2,D1			;parent dir key
	BSR	LinkParentDir		;link into proper place in parent dir
	MOVE.L	A2,A0
	BSR	UnLinkCache
9$:	RTS

****************************************************************************

* Called with key of file to be processed in D0.  Performs file hdr integrity
* checks, then reads all blocks of the file, including blocks in ext hdrs, 
* marking all valid data blocks and ext hdrs busy in the bitmap.

ProcessFile:
	PUSH	D2-D7/A2-A4
	INCW	TotalFileCount
	ZAPA	A4			;error flag
	MOVE.L	D0,D4			;filehdr key
	MOVE.L	D0,D3
	MOVE.L	D1,D7			;parent key for delete...
	BSR	ReadControl
	ERROR	ProcFileEx
	MOVE.L	A0,A2
	MOVE.L	A0,A3
	ADD.L	BlockSize,A3
	IFZB	BadBlkFlg,1$
	LEA	fh_FileName(A3),A0
	LEA	TMP2.,A1
	BSR	MoveFileName
	IFEQUC.	TMP2.,BadBlks.,ProcFileEx,L	;ignore bad.blocks file
1$:	LEA	fh_FileName(A3),A0
	BSR	DisplayFile
	ZAP	D5			;total block count
	IFEQL	fh_FirstBlk(A2),fh_BlkPtr1(A3),ProcHdr
	INCW	MinorErrCnt
	MOVE.L	fh_BlkPtr1(A3),fh_FirstBlk(A2)
	REPORT	15,3
	ERROR	ProcFileEx
	CLR.W	ErrorCount		;error count for hdr/ext

ProcHdr:
	MOVE.L	D3,D6			;predecessor key for fwd link
	MOVE.L	A2,A0
	BSR	SqueezeBlkList		;count hdr blocks
	IFEQL	fh_BlkCnt(A2),D0,1$	;block count is accurate
	MOVE.L	D0,fh_BlkCnt(A2)	;else fix the block count
	INCW	ErrorCount
	INCW	MinorErrCnt
	REPORT	16,3
	ERROR	ProcFileEx

1$:	LEA	fh_Reserved(A2),A3
	ADD.L	BlockSize,A3
	MOVE.L	fh_BlkCnt(A2),D2	;count blocks in this header
	ADD.L	D2,D5
	BRA	6$

2$:	IFNZB	SkipFlg,ProcFileEx,L
	MOVE.L	-(A3),D3		;get a data block key
	MOVE.L	D3,D0
	BSR	CheckKey		;valid key?
	ERROR	3$
	MOVE.L	D3,D0
	IFZB	BadBlkFlg,9$
	BSR	GetBadStatus		;known bad block?
	BEQ.S	8$			;yes, mark it busy
	MOVE.L	D3,D0
9$:	MOVE.L	BlockBuffer,A0
	BSR	ReadBlock		;can we read it?
	NOERROR	5$
	INCW	BadBlockCount
	REPORT	-18,9
	ERROR	ProcFileEx
	BRA.S	4$

3$:	REPORT	-17,14
	ERROR	ProcFileEx
4$:	MOVE.L	D4,D0			;filehdr key
	MOVE.L	D7,D1			;parent dir key
	BSR	DeleteFileEntry
	INCW	MajorErrCnt
	INCW	A4
	INCW	ErrorCount
	CLR.L	(A3)			;bad key...get rid of it
	BRA.S	6$

5$:	IFNZB	FFSFlg,8$		;ffs has no data checksum or type
	MOVE.L	BlockBuffer,A0
	MOVEQ	#T_DATA,D0
	IFNEL	od_Type(A0),D0,7$	;bad primary type
	IFNEL	od_Hdrkey(A0),D4,7$	;bad header key
	BSR	CalcCkSum		;good data block?
	BEQ.S	8$			;yes
**************wanna fix these and call them minor???*********************
7$:	REPORT	-24,9
	ERROR	ProcFileEx
	BRA.S	4$

8$:	MOVE.L	D3,D0
	BSR	MarkKeyBusy		;mark it busy
	BNE.S	6$			;it was free
	BSR	FileKeyBusy		;remember x-linked key and file
	REPORT	-19,9,4$		;cy=1...get rid of it
6$:	IFNZB	CancelFlg,ProcFileEx,L
	DBF	D2,2$			;else loop for all keys in header

EndOfHdr:
	MOVE.L	A2,A0
	BSR	SqueezeBlkList		;fix up hdr
	MOVE.L	D0,fh_BlkCnt(A2)	;store updated block count
	LEA	fh_Ext(A2),A0
	ADD.L	BlockSize,A0
	MOVE.L	(A0),D3			;get extension filehdr key, if any
	IFZW	ErrorCount,2$		;no errors found in filehdr
	IFZB	RepairFlg,2$		;not repairing
	DIRTY	A2
	BRA.S	1$
2$:	MOVE.L	A2,A0
	BSR	PutCacheBuffer		;header is dirty, but not fixing...
1$:	IFZL	D3,EndOfFile,L		;no ext filehdr...all done

ReadExtension:
	MOVE.L	D3,D0
	BSR	CheckKey		;valid?
	REPORT	-20,10,9$		;no...truncate file here
	BSR	GetCacheBuffer		;get a buffer for read
	ERROR	ProcFileEx		;bad awful...
	MOVE.L	A0,A2			;ptr to current entry data block
	MOVE.L	D3,D0
	BSR	ReadBlock		;try to read it
	NOERROR	1$
	INCW	BadBlockCount
	REPORT	-22,10
	ERROR	ProcFileEx
	BRA	8$			;can't read ext hdr...

1$:	CLR.W	ErrorCount		;error count for hdr/ext
	MOVE.L	A2,A0
	BSR	CalcCkSum
	IFZL	D0,2$			;good checksum...go on
	INCW	ErrorCount
	INCW	MinorErrCnt		;checksum error...try to use it/fix it
	REPORT	23,2
;;	ERROR	ProcFileEx		***
	ERROR	8$
	MOVE.L	A2,A0
	BSR	FixCkSum

2$:	MOVE.L	A2,A3
	ADD.L	BlockSize,A3
	MOVEQ	#16,D0
	IFNEL	fh_Type(A2),D0,3$	;primary type error
	MOVEQ	#-3,D0
	IFEQL	fh_SecType(A3),D0,5$	;wrong secondary type
3$:	REPORT	-25,10
;;	ERROR	ProcFileEx		***
	BRA.S	8$

5$:	MOVE.L	fh_Ownkey(A2),D0
	IFEQL	D0,D3,6$
	BSR	SaveBadKey
	MOVE.L	D3,fh_Ownkey(A2)
	INCW	ErrorCount
	INCW	MinorErrCnt
	REPORT	26,3
;;	ERROR	ProcFileEx		***
	ERROR	8$

6$:	MOVE.L	fh_Parent(A3),D0
	IFEQL	D0,D4,7$		;belongs to proper parent...good ext
	BSR	SaveBadKey
	MOVE.L	D4,fh_Parent(A3)
	INCW	ErrorCount
	INCW	MinorErrCnt
	REPORT	27,3
;;	ERROR	ProcFileEx		***
	ERROR	8$

7$:	MOVE.L	D3,D0
	BSR	MarkKeyBusy
	BEQ.S	4$			;key busy...problems
	MOVE.L	D3,D0			;else exthdr looks good
	MOVE.L	A2,A0
	MOVEQ	#-1,D1
	BSR	LinkCacheKey		;link exthdr block into busycache
	BRA	ProcHdr			;process exthdr like filehdr

4$:	BSR	FileKeyBusy
	REPORT	-28,10
;;	ERROR	ProcFileEx		***

8$:	MOVE.L	A2,A0
	BSR	PutCacheBuffer		;release ext hdr
9$:	IFNZB	CancelFlg,ProcFileEx
	MOVE.L	D4,D0			;filehdr key
	MOVE.L	D7,D1			;parent dir key
	BSR	DeleteFileEntry
	INCW	MajorErrCnt
	INCW	A4
;;	IFZB	RepairFlg,EndOfFile
;;	MOVE.L	D6,D0
;;	BSR	ReadControl
;;	ERROR	ProcFileEx
;;	DIRTY	A0
;;	LEA	fh_HashChain(A0),A0
;;	ADD.L	BlockSize,A0
;;	CLR.L	(A0)			;zap ext hdr key in previous blk
	
EndOfFile:
	MOVE.L	D4,D0
	BSR	ReadControl
	ERROR	ProcFileEx
	MOVE.L	A0,A2
	ADD.L	BlockSize,A0
	MOVE.L	BlkSizeShift,D0
	LSL.L	D0,D5			;max bytes file can have
	IFLEL	fh_FileSize(A0),D5,ProcFileEx	;filesize ok
	MOVE.L	D5,fh_FileSize(A0)
	INCW	MinorErrCnt
	REPORT	21,3
	BCS.S	ProcFileEx
	IFZB	RepairFlg,ProcFileEx
	DIRTY	A2
ProcFileEx:
	IFZL	A4,1$
	INCW	BadFileCount
1$:	POP	D2-D7/A2-A4
	RTS

* Scans filehdr block list, counting active blocks and squeezing out nulls.
* Ptr to filehdr block in A0.  Block count returned in D0.

SqueezeBlkList:
	PUSH	D2
	LEA	fh_Reserved(A0),A0
	ADD.L	BlockSize,A0
	MOVE.L	A0,A1
	MOVE.L	HashTblSize,D1
	ZAP	D0
	BRA.S	2$
1$:	MOVE.L	-(A0),D2
	BEQ.S	2$
	MOVE.L	D2,-(A1)
	INCW	D0
2$:	DBF	D1,1$
	MOVE.L	A1,D1
	SUB.L	A0,D1
	BEQ.S	5$		;no additional fill
	LSR.L	#2,D1		;convert to longword cunt
	BRA.S	4$
3$:	CLR.L	-(A1)
4$:	DBF	D1,3$
5$:	POP	D2
	RTS

* Called only from within ProcessFile to determine what to do if a data
* block is cross-linked.  For now, just record the problem and delete
* the offending block.  Filehdr key in D4 and x-linked key in D3. 

FileKeyBusy:
	RTS

********************************************************************

* Someday maybe we'll keep a list of troublesome blocks...

SaveBadKey:
	RTS

* Validates and hashes name of filehdr or userdir in A0, comparing against
* hash chain value in D0.  If no match, must relink this record into proper
* hash chain.

CheckHashChain:
	PUSH	D2/A2
	LEA	fh_FileName(A0),A2
	ADD.L	BlockSize,A2
	MOVE.L	D0,D2
	MOVE.L	A2,A0
	BSR	CalcHash		;check out which chain
	IFEQW	D2,D0,9$		;chain looks good...let it go
8$:	STC
9$:	POP	D2/A2
	RTS

* Compares new bitmap with old bitmap and writes out new bitmap if they
* differ (assuming RepairFlg is set...)

RepairBitmap:
	PUSH	D2-D4/A2-A5
	CLR.B	CurrentBlkType
	CLR.W	ErrorCount		;start with no errors
	BSR	LoadBitmapKeys		;get list of bitmap blocks
	ERROR	10$			;oops...can't fix bitmap*****
	IFNZB	BitmapErrFlg,9$		;old bitmap is bad...replace it
	MOVE.L	BitmapDataPtr,A3	;ptr to NEW bitmap data
	MOVE.L	BitmapKeyBuffer,A4	;list of bitmap block keys
	MOVE.L	BlockBuffer,A5
	MOVEQ	#32,D3
	MOVE.L	MaxKeys,D4		;check this many keys

1$:	MOVE.L	(A4)+,D0		;get a bitmap block key
	MOVE.L	A5,A0
	BSR	ReadBlock		;read old bitmap block
	ERROR	9$
	MOVE.L	A5,A0
	ADDQ.L	#4,A0			;skip over checksum
	MOVE.L	BlockSizeL,D1		;this many longwords to check, max
	DECL	D1			;don't count checksum
	BRA.S	4$
2$:	IFLTL	D4,D3,5$		;not a full longword left
	CMP.L	(A3)+,(A0)+		;look for bitmap differences
	BEQ.S	3$			;ok
	INCW	ErrorCount		;bitmaps differ
3$:	SUB.L	D3,D4
	BEQ.S	8$			;ended on a longword boundary
4$:	DBF	D1,2$
	BRA.S	1$

5$:	MOVE.L	(A3),D1			;get last data
	MOVE.L	(A0),D0
	EOR.L	D1,D0			;find out if they match
	ZAP	D2			;start with bit 0
6$:	BTST	D2,D0			;any bit left SET is an error
	BEQ.S	7$
	INCW	ErrorCount		;bitmaps differ
7$:	INCL	D2
	DECL	D4
	BNE.S	6$			;loop for remaining bits
8$:	IFZW	ErrorCount,9$		;bitmaps match...no problems
	INCW	MinorErrCnt
	REPORT	39,15
	BCS.S	10$
9$:	IFZB	RepairFlg,10$		;not repairing...ignore problem
	BSR	WriteBitmap		;replace old bitmap with new bitmap
	MOVE.L	RootBlkNbr,D0
	BSR	ReadControl		;reread root
	ERROR	10$
	DIRTY	A0
	ADD.L	BlockSize,A0
	MOVEQ	#-1,D0
	MOVE.L	D0,rb_BMFlg(A0)		;force bitmap valid
10$:	POP	D2-D4/A2-A5
	RTS

* Reads and checks root block.  Returns CY=1 if root fails basic tests.
* This routine is much more thorough than similar routine in files.asm.

ReadRoot:
	PUSH	A2-A3
	CLR.W	ErrorCount
	BSR	GetCacheBuffer		;get a buffer for read
	ERROR	9$			;bad awful...
	MOVE.L	A0,A2			;ptr to current entry data block
	MOVE.L	RootBlkNbr,D0
	BSR	ReadBlock
	REPORT	-29,11,8$		;disk error reading root
*******************need checksum check here************************
	MOVE.L	A2,A0
	BSR	CheckCtrlTypes		;does it pass most basic tests?
	ERROR	1$
	MOVEQ	#1,D1
	IFEQIL	D0,D1,2$		;secondary type better be root
1$:	REPORT	-30,11			;critical root block errors
	ERROR	8$
	BRA	7$

2$:	MOVE.L	A2,A3
	ADD.L	BlockSize,A3
	MOVE.L	HashTblSize,D0
	IFEQL	rb_HashSize(A2),D0,3$
	MOVE.L	D0,rb_HashSize(A2)
	INCW	ErrorCount
	REPORT	30,3
	ERROR	8$
3$:	LEA	rb_Ownkey(A2),A0
	ZAP	D0
	MOVE.L	(A0),D1			;Ownkey
	MOVE.L	D0,(A0)+
	ADD.L	(A0),D1			;0
	MOVE.L	D0,(A0)+
	ADDQ.L	#4,A0			;skip hashtblsize
	ADD.L	(A0),D1			;0
	MOVE.L	D0,(A0)+
	LEA	rb_Skip5(A3),A0
	ADD.L	(A0),D1			;hashchain
	MOVE.L	D0,(A0)+
	ADD.L	(A0),D1			;parent dir
	MOVE.L	D0,(A0)+
	ADD.L	(A0),D1			;extension
	MOVE.L	D0,(A0)+
	IFZL	D1,4$
	INCW	ErrorCount
	REPORT	30,3
	ERROR	8$
4$:	LEA	rb_VolName(A3),A0	;point to volume name
	BSR	CheckName
	ERROR	8$
	LEA	rb_DMDate(A3),A0	;date of last disk mod
	MOVE.L	A2,A1
	BSR	CheckDateTime
	LEA	rb_CDate(A3),A0		;disk creation date
	MOVE.L	A2,A1
	BSR	CheckDateTime
	IFZW	ErrorCount,6$		;no errors
	IFZB	RepairFlg,6$		;not repairing
	DIRTY	A2
6$:	MOVE.L	A2,A0
	MOVE.L	RootBlkNbr,D0
	MOVEQ	#-1,D1			;control block
	BSR	LinkCacheKey		;make root buffer available
	BRA.S	9$
7$:	MOVE.L	A2,A0
	BSR	PutCacheBuffer		;free the buffer
8$:	STC
9$:	POP	A2-A3
	RTS	

* Checks validity of boot block, and asks for correct file system type.

CheckBootBlock:
	IFZB	NotDosFlg,9$		;boot block must be OK
	INCW	MajorErrCnt
	DispReq	BootFileSys,ReqFFSGad
	IFNZB	CancelFlg,8$
	MOVE.L	NewType,D0
	MOVE.B	D0,FFSFlg
	IFZB	RepairFlg,9$		;not repairing
	BSR	InitBoot		;fix boot block
	ERROR	9$
	CLR.B	NotDosFlg
	BRA.S	9$
8$:	STC
9$:	RTS

* Performs validation checks on block in A0 which claims to be filehdr/userdir.
* Returns secondary type in D0 or error code if CY=1.  Returns ptr to block
* in A0.

VerifyCtrlBlock:
	PUSH	D2-D3/A2
	MOVE.L	A0,A2
	ZAP	D3			;error count
	BSR	CheckCtrlTypes		;check for valid control block types
	ERROR	8$			;invalid block types
	MOVE.L	D0,D2			;save secondary type
	LEA	fh_HashChain(A2),A1
	ADD.L	BlockSize,A1
	MOVEQ	#3,D1			;check 3 longwords
	BSR.S	CheckEntries
	MOVE.L	HashTblSize,D1
	LEA	rb_HashTable(A2),A1	;scan the hash table
	BSR.S	CheckEntries
	MOVE.L	D2,D0
	IFZL	D3,7$
8$:	STC
7$:	MOVE.L	A2,A0
9$:	POP	D2-D3/A2
	RTS

CheckEntries:
	BRA.S	2$
1$:	MOVE.L	(A1)+,D0		;get fileblock key
	BEQ.S	2$
	IFGTL	LowestKey,D0,3$
	IFGEL	HighestKey,D0,2$
3$:	INCL	D3
2$:	DBF	D1,1$			;loop for all
	RTS

* Checks key in D0 to be 0 or in valid range.  Returns D0 intact.

CheckKey:
	IFZL	D0,9$
	IFGTL	LowestKey,D0,8$
	IFGEL	HighestKey,D0,9$
8$:	STC
	RTS
9$:	CLC
	RTS

* Verifies valid control block primary and secondary types for block in A0.
* Returns secondary type in D0 or CY=1.

CheckCtrlTypes:
	MOVEQ	#T_SHORT,D0
	IFNEL	fh_Type(A0),D0,8$	;primary type not T_SHORT
	ADD.L	BlockSize,A0
	MOVE.L	fh_SecType(A0),D1	;secondary type
	MOVEQ	#-3,D0
	IFEQL	D1,D0,9$		;good file header
	MOVEQ	#2,D0
	IFEQL	D1,D0,9$		;good user directory
	MOVEQ	#1,D0
	IFEQL	D1,D0,9$		;good root directory
8$:	STC
9$:	RTS

* Verifies valid AmigaDOS file name length and chars.  A0=BSTR name.  
* Returns CY=1 on error.

ValidateName:
	ZAP	D1
	MOVE.B  (A0)+,D1		;get length
	BEQ.S	8$			;null...invalid
	IFGTIB	D1,#FileNameLen,8$	;too long
	BRA.S	2$
1$:	MOVE.B  (A0)+,D0
;;	UCASE	D0
	ANDI.B	#$7F,D0			;get rid of high bits
	IFLTIB	D0,#32,8$		;no ASCII control codes acceptable
	IFEQIB	D0,#'/',8$		;illegal char in filename
	IFEQIB	D0,#':',8$		;ditto
	IFEQIB	D0,#'%',8$
2$:	DBF	D1,1$
	CLC
	BRA.S	9$
8$:	STC
9$:	RTS

* Validates file/userdir name in A0, and optionally replaces a bad name
* with a dummy name of the form BadNamennnnnnnn.

CheckName:
	PUSH	A2
	MOVE.L	A0,A2
	LEA	FileName.,A1
	BSR	MoveFileName
	MOVE.L	A2,A0
	BSR.S	ValidateName
	NOERROR	9$
	INCW	ErrorCount
	IFZB	RepairFlg,8$
	MOVE.L	A2,A0
	BSR.S	StoreDummyName
8$:	REPORT	12,6
9$:	POP	A2
	RTS

* Generates a dummy name of the form "BadNamennnnnnnn" using the current
* date/time and stores it as a BSTR at location A0.

StoreDummyName:
	PUSH	D2/A2-A5
	MOVE.L	A0,A2
	LEA	LocalDTS,A3
	MOVE.L	A3,D1
	CALLSYS	DateStamp,DosBase
	LEA	TMP.,A4
	LEA	TMP2.,A5
	MOVE.L	A4,A1
	MOVE.	DummyName.,A1
;	ADDQ	#4,A3
	ADDQ	#8,A3
;	MOVEQ	#1,D2			;two passes
	MOVEQ	#0,D2			;one pass
1$:	MOVE.L	(A3)+,D0		;get ticks
	MOVE.L	A5,A0
	STR.	L,D0,A0,#48,#4		;convert to decimal
	MOVE.L	A5,A0			;append to TMP2. to TMP.
	MOVE.L	A4,A1
	APPEND.	A0,A1
	DBF	D2,1$
	MOVE.L	A4,A0
	MOVE.L	A2,A1
	Z_STG.	A0,A1
	MOVE.L	A4,A0
	MOVE.	A0,NewName.
	POP	D2/A2-A5
	RTS

* Verifies valid AmigaDOS file comment length and chars.  A0=BSTR name.  
* Returns CY=1 on error.

ValidateComment:
	PUSH	A2
	MOVE.L	A0,A2
	ZAP	D1
	MOVE.B  (A0)+,D1		;get length
	BEQ.S	9$
	IFGTIB	D1,#80,8$		;too long
	BRA.S	2$
1$:	MOVE.B  (A0)+,D0
;;	UCASE	D0
	ANDI.B	#$7F,D0			;get rid of high bits
	IFLTIB	D0,#32,8$		;no ASCII control codes acceptable
2$:	DBF	D1,1$
	CLC
	BRA.S	9$
8$:	INCW	ErrorCount
	REPORT	13,7
	BCS.S	7$
	IFZB	RepairFlg,7$
	CLR.B	(A2)			;zap comment to null length
7$:	STC
9$:	POP	A2
	RTS

***************************************************************************

* Checks root list of bitmap keys and possible extensions.  Ptr to root list
* in A0.  Returns CY=1 on error.  Marks bitmap blocks busy in bitmap.
* Warning - this code MUST execute AFTER the new bitmap is completely built
* so that we can find free keys for bitmap replacement, if necessary.

ProcessBitmapKeys:
	PUSH	D2-D4/A2-A4
	MOVE.L	RootBlkNbr,D0
	BSR	ReadControl		;read root
	ERROR	ProcBMEx1
	MOVE.L	A0,A2			;ptr to root block
	LEA	rb_BMPtrs(A2),A3	;ptr to list of bitmap keys
	ADD.L	BlockSize,A3
	MOVE.L	BlockBuffer,A4
	MOVEQ	#25,D2			;root has 25 max
	MOVE.L	BitmapBlocks,D3		;should have this many...
	BRA	NextKey

ProcKey:
	MOVE.L	(A3),D4			;get a key
	BEQ	BadBMKey		;shouldn't be 0
	MOVE.L	D4,D0
	BSR	CheckKey		;proper range?
	ERROR	BadBMKey		;range error
	MOVE.L	A4,A0
	MOVE.L	D4,D0
	BSR	ReadBlock		;read bitmap block
	NOERROR	1$			;error reading bitmap block
;	INCB	BitmapErrFlg
	REPORT	-32,12
	ERROR	ProcBMEx
	BRA	GetNewBMKey

1$:	MOVE.L	A4,A0
	BSR	CalcCkSum
	BEQ.S	2$
	INCW	MinorErrCnt
	INCW	BitmapErrFlg		;can't trust bitmap
	REPORT	33,15
	ERROR	ProcBMEx
2$:	MOVE.L	D4,D0
	BSR	MarkKeyBusy
	BEQ	BadBMKey		;oops...key already used
FixKey:	DECL	D3			;else count it
	BEQ	CheckRest		;found all active keys
	ADDQ.L	#4,A3			;advance to next bitmap key
NextKey:
	DBF	D2,ProcKey		;loop for entire list

;;;	BSR	MarkDirty		;mark this bitmap list dirty,...
	MOVE.L	(A3),D4			;get ext key
	BEQ.S	BadBMExtKey		;better NOT be 0...need an ext
	MOVE.L	D4,D0
	BSR	CheckKey
	ERROR	BadBMExtKey
	BSR	MarkKeyBusy
	BEQ.S	BadBMExtKey		;was already busy
	MOVE.L	D4,D0
	BSR	ReadData		;don't calc cksum or check type
	NOERROR	1$
	REPORT	-35,12			;cant read ext...
	BRA.S	AllocBMExt

1$:	MOVE.L	A0,A2
	MOVE.L	A2,A3
	MOVE.L	BlockSizeL,D2
	DECL	D2			;leave room for ext link
	BRA.S	NextKey

BadBMExtKey:
	REPORT	-34,12
AllocBMExt:
	IFNZB	CancelFlg,ProcBMEx,L
	INCW	BitmapErrFlg
	INCW	MajorErrCnt
	IFZB	RepairFlg,2$		;not fixing...forget it
	MOVE.L	RootBlkNbr,LastUsedKey
1$:	BSR	AllocFreeKey
	ERROR	BMFatal			;nothing free
	MOVE.L	D0,(A3)			;new bitmap ext key
	MOVE.L	D0,D4
	DIRTY	A2
	MOVE.L	D3,D0			;remaining count needed
	BSR	GetCacheBuffer		;get buffer for new ext
	ERROR	ProcBMEx
	MOVE.L	A0,A2
	MOVE.L	A0,A3
	MOVE.L	BlockSizeL,D1		;max keys needed
	BSR	StoreBitmapKeys		;alloc new keys
	ERROR	BMFatal			;nothing free
	MOVE.L	D0,D3			;remaining count
	EXG.L	A2,A3			;ptr to ext key to A3, buffer ptr to A2
	MOVE.L	D4,D0
	MOVE.L	A2,A0
	DIRTY	A0
	MOVEQ	#-2,D1			;block needs bitmap checksum
	BSR	LinkCacheKey
	IFNZL	D3,1$
2$:	BRA	ProcBMEx

BMFatal:
;;	DispReq	??			;warn op that bitmap update failed
	STC
	BRA	ProcBMEx

BadBMKey:
	REPORT	-31,3
	ERROR	ProcBMEx
GetNewBMKey:
	INCB	BitmapErrFlg		;bitmap needs fixing
	INCW	MajorErrCnt
	IFZB	RepairFlg,1$
	MOVE.L	RootBlkNbr,LastUsedKey
	BSR	AllocFreeKey
	MOVE.L	D0,(A3)			;new key for bitmap
1$:	BRA	FixKey

* The rest of the keys had better be 0, and the extension too!

CheckRest:
	BRA.S	3$
1$:	MOVE.L	(A3),D0			;get an entry 
	BEQ.S	3$			;good entry
	CLR.L	(A3)			;zap extra entry
	INCW	MinorErrCnt
	INCW	BitmapErrFlg
	REPORT	36,3
	BCS.S	ProcBMEx
3$:	ADDQ.L	#4,A3
4$:	DBF	D2,1$
	IFZL	(A3),ProcBMEx		;BM has ext key when not needed
	INCW	BitmapErrFlg
	CLR.L	(A3)
	REPORT	37,3

ProcBMEx:
	BSR.S	MarkDirty
ProcBMEx1:
	POP	D2-D4/A2-A4
	RTS

MarkDirty:
	IFZW	BitmapErrFlg,2$
	IFZB	RepairFlg,2$
	IFNZB	CancelFlg,2$
	DIRTY	A2
	BRA.S	1$
2$:	MOVE.L	A2,A0
	BSR	PutCacheBuffer		;release dirty buffer...no hits
1$:	RTS

***************************************************************************

* Recalculates and corrects checksum if repair mode in effect.
* Ptr to block in A0.

FixCkSum:
	PUSH	A2
	IFZB	RepairFlg,9$
	MOVE.L	A0,A2
	BSR	CalcCkSum
	SUB.L	D0,fh_CkSum(A2)		;update cksum
	DIRTY	A2
9$:	POP	A2
	RTS

* Deletes bad file.  Filehdr key in D0, parent key in D1.

DeleteFileEntry:
	PUSH	D2-D3/A2
	IFZB	RepairFlg,9$		;not repairing...do nothing
	MOVE.L	D0,D2
	MOVE.L	D1,D3
	MOVE.L	CurDirBuf,A2
	MOVE.L	A2,A0
	MOVE.L	CurDirKey,D0
	MOVEQ	#-1,D1
	BSR	LinkCacheKey		;relink parent dir into cache
	MOVE.L	D2,D0
	MOVE.L	D3,D1
	BSR	DeleteFile		;get rid of that sucker
	MOVE.L	A2,A0
	BSR	UnLinkCache		;now unlink parent dir again
	SETF	SkipFlg			;don't read more of that file
9$:	POP	D2-D3/A2
	RTS

* Checks date/time stamp pointed to by A0 for range errors.
* Buffer base in A1, ptr to date/time stamp in A0.

MaxDays		EQU	20000		;this handles dates until year 2032
MaxMins		EQU	24*60		;minutes per day
MaxTicks	EQU	60*50		;ticks per minute

CheckDateTime:
	PUSH	A2
	MOVE.L	A0,A2
	IFGTIL	(A0)+,#MaxDays,8$	;days too large
	IFGTIL	(A0)+,#MaxMins,8$	;minutes too large
	IFGTIL	(A0),#MaxTicks,8$	;ticks ok
	CLC
	BRA.S	9$
8$:	INCW	ErrorCount
	REPORT	14,8
	BCS.S	7$
	IFZB	RepairFlg,7$
	DIRTY	A1
	MOVE.L	A2,D1			;store a valid date/time stamp
	CALLSYS	DateStamp,DosBase
7$:	STC
9$:	POP	A2
	RTS

DummyName.	DC.B	'BadName',0
NewName.	DS.B	FileNameLen

	BSS,PUBLIC

LocalDTS	DS.L	3	;local date/time stamp for dummy name calc
CurDirBuf	DS.L	1
CurDirKey	DS.L	1
SaveHashVal	DS.L	1	;saved hash value for relinked entry
MinorErrCnt	DS.W	1	;found and corrected n minor errors
MajorErrCnt	DS.W	1	;Found and corrected n major errors
ErrorCount	DS.W	1	;dir entry error count
BadBlockCount	DS.W	1	;count of blocks having hard read errors
TotalDirCount	DS.W	1
BadDirCount	DS.W	1	;bad dirs
TotalFileCount	DS.W	1
BadFileCount	DS.W	1	;count of files found to be corrupted (deleted)
BitmapErrFlg	DS.B	1	;1=bitmap needs to be fixed
VolBadFlg	DS.B	1	;1=vol unusable
CurrentBlkType	DS.B	1	;block type of current block
;SortFlg		DS.B	1	;1=need to sort hash chain if FFS
WrongChainFlg	DS.B	1	;1=current entry in wrong hash chain
RepairFlg	DS.B	1	;1=repair, 1=report
	END

