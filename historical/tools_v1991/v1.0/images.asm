;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*	Copyright (c) 1987 Central Coast Software	*
*	    268 Bowie Dr, Los Osos, CA 93402		*
*	     All rights reserved, worldwide		*
*********************************************************

	XDEF	DN_ARROW,UP_ARROW
	XDEF	BeepWave,SizingImage

	SECTION	IMAGES,DATA,CHIP

* Audio beep waveform

BeepWave DC.B	0,60,95,110,127,110,95,60,0,-60,-95,-110,-127,-110,-95,-60

* DOWN AND UP ARROWS USED IN DIRECTORY WINDOW MOVEMENTS.
* 16 PIXELS WIDE, 10 PIXELS HIGH.

UP_ARROW
	DC.W	%1111111111111111
	DC.W	%1111111111111111
	DC.W	%1111111011111111
	DC.W	%1111100000111111
	DC.W	%1110000000001111
	DC.W	%1000000000000011
	DC.W	%1111100000111111
	DC.W	%1111100000111111
	DC.W	%1111111111111111
	DC.W	%1111111111111111

DN_ARROW
	DC.W	%1111111111111111
	DC.W	%1111111111111111
	DC.W	%1111100000111111
	DC.W	%1111100000111111
	DC.W	%1000000000000011
	DC.W	%1110000000001111
	DC.W	%1111100000111111
	DC.W	%1111111011111111
	DC.W	%1111111111111111
	DC.W	%1111111111111111

* Sizing gadget for lower rignt corner

SizingImage
	DC.W	$FFFF,$C0FF,$CCFF,$C003,$FCF3,$FCF3,$FCF3,$FC03,$FFFF
;	DC.W	$0000,$3F00,$3300,$3FFC,$030C,$030C,$030C,$03FC,$0000


	END

