;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*							*
*	Quarterback Tools Repair Report			*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1990 Central Coast Software	*
*********************************************************


	INCLUDE	"vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"VD0:BOXES.ASM"

	XDEF	ReportError
	XDEF	InitReport,CloseReport
	XDEF	ReportVolStatus,RootFailure
	XDEF	DisplayPath,DisplayFile,DisplayFileBlk
	XDEF	UpdateNames,UpdateFileName
	XDEF	RexFind,RexRepair

	XDEF	RptNone,RptDisk,RptPtr
	XDEF	ScnQuiet,ScnInform,ScnInteract
	XDEF	SavedSMode

* Externals in TOOLS.ASM

	XREF	SysBase,DosBase,IntuitionBase
	XREF	TMP.,TMP2.,TMP3.
	XREF	VolNameBuf.
	XREF	ReportPath.
	XREF	ScreenMode,ReportMode
	XREF	WaitForAction

	XREF	ProcFlg,CancelFlg,SkipFlg


* Externals in GADGETS.ASM

	XREF	RQText
	XREF	RQTxtBuf.,RQTxtBf1.,RQTxtBf2.,RQTxtBf3.,RQTxtBf4.
	XREF	ReqProc1,ReqProc2,RightPGad,ReqProceed
	XREF	ReqNameGad,RptName
	XREF	NamBuf

* Externals in VTEST.ASM

* Externals in MESSAGES.ASM

	XREF	MajorErr.,MinorErr.
	XREF	Drawer.,Entry.,Error.,Action.
	XREF	RepairRpt.,RepairName.
	XREF	MONTHS
	XREF	NoPrt1.,NoPrt2.,NoPrt3.
	XREF	NoRpt1.,NoRpt2.,NoRpt3.,NoRpt4.
	XREF	RootFail1.,RootFail2.,RootFail3.,RootFail4.
	XREF	FilHdr.,DirBlk.
	XREF	TestRes.
	XREF	SerProb.
	XREF	MinProb.
	XREF	TotFiles.
	XREF	BadFiles.
	XREF	TotDrawers.
	XREF	BadDrawers.
	XREF	BadBlocks.
	XREF	VolStatus.
	XREF	VolNoProb.
	XREF	VolLostFiles.
	XREF	VolBad1.
	XREF	VolBad2.
	XREF	VolBad3.
	XREF	VolFixed.
	XREF	ProcDir.,ProcFile.

* Externals in RESTORE.ASM

	XREF	MoveFileName
	XREF	FileName.

* Externals in ERRORS.ASM

	XREF	ErrorTable,ErrList
	XREF	ActionTable,ActList

* Externals in FUN.ASM

	XREF	SetSpacing
	XREF	MoveInsertDevice
	XREF	CLRWIN

* Externals in REPAIR.ASM

	XREF	CurrentBlkType
	XREF	RepairFlg
	XREF	LocalDTS
	XREF	NewName.
	XREF	MajorErrCnt,MinorErrCnt
	XREF	TotalDirCount
	XREF	BadDirCount
	XREF	TotalFileCount
	XREF	BadFileCount
	XREF	BadBlockCount
	XREF	VolBadFlg

* Externals in CAT.ASM

	XREF	NextWindow

	XREF	Path.

* Externals in DISK.ASM

	XREF	MotorOff

* Externals in DEVICE.ASM

	XREF	BlockSize

* Externals in AREXX.ASM

	XREF	ResultBuf
	XREF	ArexxFlg

* Page size is based on a 2-line top and bottom margin, 11-inch paper, 6 lpi.

pf_SIZEOF	EQU	232	;from Intuition.i
pf_PaperLength	EQU	178	;ditto

* Prepares final volume status report following test run.

RootFailure:
	BSR	MotorOff
	PUSH	A2
	IFNZB	ArexxFlg,9$
	BldWarn	0,0,RootFail1.,RootFail2.,RootFail3.,RootFail4.
	MOVE.L	A0,A2
	IFZB	SavedRMode,1$		;not printing/disk
	BSR	PrintReq		;else add final results
1$:	IFZB	SavedSMode,9$		;not displaying to screen
	MOVE.L	A2,A0
	DispReq	A0,ReqProceed
9$:	POP	A2
	RTS

ReportVolStatus:
	BSR	MotorOff
	IFZB	SavedRMode,1$		;not printing/disk
	BSR	PrintResults		;else add final results
1$:	IFZB	SavedSMode,9$		;not displaying to screen
	IFNZB	ArexxFlg,9$
	LEA	RightPGad,A0		;else put up final display
	LEA	RefreshResults,A1
	ZAP	D0
	BSR	NextWindow
	BSR	WaitForAction
	IFNZB	ProcFlg,9$
	STC
9$:	RTS
	
RefreshResults:
	PUSH	D2/A2
	BSR	CLRWIN
	DispMsg	#ResTxt_LE,#ResTitle_TE,TestRes.,WHITE
	DispCur	VolNameBuf.,ORANGE,BLUE
	LEA	ResultsTable,A2
	MOVEQ	#JAM1,D2
	SWAP	D2
	MOVE.W	#WHITE*256,D2
1$:	MOVE.L	(A2)+,D0		;LE,TE
	BEQ.S	2$			;end of table
	MOVE.L	D2,D1			;mode and color
	MOVE.L	(A2)+,A0		;get msg ptr
	BSR	DISP_MSG		;put up text
	MOVE.L	(A2)+,A0		;get ptr to data
	STR.	W,(A0),TMP2.,#32,#5
	STRIP_LB. TMP2.
	DispCur	TMP2.,ORANGE,BLUE
	BRA.S	1$
2$:	DispMsg	#ResTxt_LE,#ResLin8_TE,VolStatus.,WHITE
	BSR	LoadStatusMsg
	DispCur	A0,ORANGE,BLUE
	LEA	RightPGad,A0
	GadColor A0,ORANGE
	POP	D2/A2
	RTS

PrintResults:
	PUSH	D2/A2
	IFGEIB	LineCnt,#12,1$		;enough room to start a new item
	BSR	NextPage		;else force a new page
	ERROR	8$	
	BSR	PrintHeader		;and print the header
	ERROR	8$
1$:	BSR	BlankLine
	ERROR	8$
	BSR	BlankLine
	ERROR	8$
	MOVE.	TestRes.,TMP.
	APPEND.	VolNameBuf.,TMP.
	BSR	PrintLine
	ERROR	8$
	BSR	BlankLine
	ERROR	8$
	LEA	ResultsTable,A2
2$:	MOVE.L	(A2)+,D0		;LE,TE
	BEQ.S	3$			;end of table
	MOVE.L	(A2)+,A0		;get msg ptr
	MOVE.	A0,TMP.
	MOVE.L	(A2)+,A0		;get ptr to data
	STR.	W,(A0),TMP2.,#32,#5
	STRIP_LB. TMP2.
	APPEND.	TMP2.,TMP.
	BSR	PrintLine
	ERROR	8$
	BRA.S	2$
3$:	MOVE.	VolStatus.,TMP.
	BSR	LoadStatusMsg
	APPEND.	A0,TMP.
	BSR	PrintLine
	BSR	NextPage		;flush last page
8$:	POP	D2/A2
	RTS

PrintReq:
	PUSH	D2/A2
	IFGEIB	LineCnt,#12,1$		;enough room to start a new item
	BSR	NextPage		;else force a new page
	ERROR	8$	
	BSR	PrintHeader		;and print the header
	ERROR	8$
1$:	BSR	BlankLine
	ERROR	8$
	BSR	BlankLine
	ERROR	8$
	LEA	RQTxtBuf.,A0
	BSR	PrintText
	LEA	RQTxtBf1.,A0
	BSR	PrintText
	LEA	RQTxtBf2.,A0
	BSR	PrintText
	LEA	RQTxtBf3.,A0
	BSR	PrintText
	BSR	NextPage		;flush last pasge
8$:	POP	D2/A2
	RTS

* Loads A0 with proper volume status msg.

LoadStatusMsg:
	LEA	VolNoProb.,A0
	LEA	VolBad1.,A1
	IFZW	MajorErrCnt,1$
	LEA	VolBad2.,A0
	IFNZB	VolBadFlg,9$		;vol unusable...recover to another vol
	LEA	VolBad3.,A0
	IFZB	RepairFlg,9$		;serious problems...use repair
	LEA	VolLostFiles.,A0	;repaired, some files lost
	BRA.S	9$
1$:	IFZW	MinorErrCnt,9$
	MOVE.L	A1,A0
	IFZB	RepairFlg,9$
	LEA	VolFixed.,A0
9$:	RTS

RexFind:
RexRepair:
	BSR.S	LoadStatusMsg
	MOVE.	A0,ResultBuf
	RTS

* Reports errors to operator according to ReportMode.  Error msg code
* in D0, action code in D1.  If RepairFlg=0, action taken is None.
* This routine must preserve A0 and A1, just in case.

* If error code is negative, the error is major, else error is minor.
* A major error is defined as some problem which almost guarantees data
* loss.  Note that path is limited to size of requester text buffer
* (currently 60 chars), even though the report has more room.


ReportError:
	PUSH	A0-A1
	MOVE.W	D0,ErrorCode
	IFNZB	RepairFlg,1$		;repair mode...use action code
	ZAP	D1			;else force "none"
1$:	MOVE.W	D1,ActionCode
	BSR	BuildErrorText
	IFNEIB	SavedSMode,#2,2$	;not interactive
	BSR	Interactive		;display to operator and wait
2$:	IFZB	SavedRMode,3$		;not going to disk/printer
	BSR	DiskPrinter
3$:	IFNZB	SkipFlg,8$
	IFZB	CancelFlg,9$
8$:	STC
9$:	POP	A0-A1
	RTS

BuildErrorText:
	PUSH	A0-A1
	LEA	MinorErr.,A0
	MOVE.W	ErrorCode,D0
	BPL.S	1$			;it is a minor error
	NEG	D0			;else correct sign
	MOVE.W	D0,ErrorCode
	LEA	MajorErr.,A0		;it is a major error
1$:	MOVE.	A0,RQTxtBuf.
	MOVE.	Drawer.,TMP.		;drawer
	APPEND.	Path.,TMP.
	CLR.B	TMP.+RQTxtBufSize	;truncate path at max length
	MOVE.	TMP.,RQTxtBf1.
	MOVE.	Entry.,RQTxtBf2.	;file/dir name
	APPEND.	FileName.,RQTxtBf2.
	LEA	FilHdr.,A0
	IFEQIB	CurrentBlkType,#-3,2$
	LEA	DirBlk.,A0
2$:	MOVE.	A0,TMP2.
	MOVE.	Error.,RQTxtBf3.	;error text with possible merge
	MOVE.W	ErrorCode,D0
	LEA	ErrList,A0
	LEA	ErrorTable,A1
	LSL.W	#1,D0
	ADD.W	0(A1,D0.W),A0		;point to proper msg text
	LEA	TMP.,A1			;move to TMP. to merge
	BSR	MoveInsertDevice
	APPEND.	TMP.,RQTxtBf3.
	MOVE.	Action.,RQTxtBf4.
	MOVE.	NewName.,TMP2.
	MOVE.W	ActionCode,D0
	LEA	ActList,A0
	LEA	ActionTable,A1
	LSL.W	#1,D0
	ADD.W	0(A1,D0.W),A0
	LEA	TMP.,A1
	BSR	MoveInsertDevice
	LEA	TMP.,A0
	APPEND.	A0,RQTxtBf4.
	POP	A0-A1
	RTS

Interactive:
	IFNZB	ArexxFlg,9$
	BEEP
	MOVEQ	#6,D0
	MOVEQ	#10,D1
	BSR	SetSpacing
	BSR	MotorOff
	LEA	ReqProc2,A1
	IFEQIB	CurrentBlkType,#-3,1$
	LEA	ReqProc1,A1
1$:	DispReq	RQText,A1
9$:	RTS

InitReport:
	PUSH	D2
	CLR.L	Handle
	MOVE.L	#LocalDTS,D1
	CALLSYS	DateStamp,DosBase

	MOVE.B	ScreenMode,SavedSMode
	MOVE.B	ReportMode,D0
	MOVE.B	D0,SavedRMode
	DECB	D0
	BEQ.S	2$			;disk mode
	DECB	D0
	BNE	9$			;unknown mode
1$:	MOVE.L	#PRT.,D1
	BRA.S	3$

2$:	BSR	BuildRptName		;build report name in TMP.
	MOVE.L	#TMP.,D1
3$:	MOVE.L	#NEW,D2
	CALLSYS	Open,DosBase
	MOVE.L	D0,Handle		;diskfile/printer handle
	BNE	5$			;good open
	IFEQIB	SavedRMode,#2,4$	;using printer
	BldWarn	0,0,NoRpt1.,NoRpt2.,NoRpt3.,NoRpt4.
	DispReq	A0,ReqProc1
	IFNZB	CancelFlg,9$
	BSR	GetReportPath
	BRA.S	2$
4$:	BldWarn	0,0,NoPrt1.,NoPrt2.,NoPrt3.
	DispReq	A0,ReqProc1
	IFNZB	CancelFlg,9$
	BRA	1$

* If we get here, we have a file or printer open and ready...

5$:	BSR	GetPageSize		;get number of lines per page
	BSR	PrintHeader		;title and device info
9$:	POP	D2
	RTS

CloseReport:
	IFZL	Handle,9$
;;	BSR	NextPage		;finish up last page
	MOVE.L	Handle,D1
	CALLSYS	Close,DosBase
	CLR.L	Handle
9$:	RTS

* Prints one line of text, based on file info block pointed to by A1.

DiskPrinter:
	IFGEIB	LineCnt,#5,1$		;enough room to start a new item
	BSR	NextPage		;else force a new page
	ERROR	8$	
	BSR	PrintHeader		;and print the header
	ERROR	8$
	BRA.S	2$
1$:	BSR	BlankLine		;two blank lines
	ERROR	8$
	BSR	BlankLine
	ERROR	8$
2$:	MOVE.	RQTxtBf1.,TMP.
	BSR	PrintLine
	ERROR	8$
	MOVE.	RQTxtBf2.,TMP.
	BSR	PrintLine
	ERROR	8$
	MOVE.	RQTxtBf3.,TMP.
	BSR	PrintLine
	ERROR	8$
	MOVE.	RQTxtBf4.,TMP.
	BSR	PrintLine
;	ERROR	8$
;	BSR	BlankLine
8$:	RTS

* Outputs page header.

PrintHeader:
	MOVE.B	PageSize,LineCnt	;LINES PER PAGE
	BSR	BlankLine		;START WITH CR, JUST IN CASE
	RTSERR
	BSR	BlankLine
	RTSERR
	BSR	ConvertDosDate		;current date to TMP2.
	MOVE.	TMP2.,TMP.
	ACHAR.	#32,TMP.
	BSR	ConvertDosTime
;;	APPEND.	TMP2.,TMP.
	MOVE.	TMP.,TMP2.
	LEA	RepairRpt.,A0
	LEA	TMP.,A1
	BSR	MoveInsertDevice
	BSR	PrintLine
	RTSERR
	BSR	BlankLine
	RTS

NextPage:
1$:	IFZB	LineCnt,2$		;done
	BSR.S	BlankLine
	BRA.S	1$
2$:	RTS

* prints one blank line

BlankLine:
	LEA	TMP.,A0
	CLR.B	(A0)
;	BSR	PrintLine
;	RTS

* Prints null-terminated text in TMP.  Returns CY=1 on error.
* error.

PrintText:
	PUSH	A2
	MOVE.L	A0,A2
	BRA.S	PrintCom

PrintLine:
	PUSH	A2
	LEA	TMP.,A2
PrintCom:
	MOVE.L	A2,A1
	APPEND.	CRLF.,A1
	PUSH	D2-D3
	MOVE.L	A2,D2
	MOVE.L	A2,A0
	LEN.	A0
	MOVE.L	D0,D3
	MOVE.L	Handle,D1
	IFZL	D1,1$
	CALLSYS	Write,DosBase		;send to file/printer
1$:	POP	D2-D3
	DECB	LineCnt
	TST.L	D0
	BPL.S	9$			;no error
	STC				;else some write problem
9$:	POP	A2
	RTS

* Gets page size in lines from Preferences

GetPageSize:
	PUSH	A2/D2
	MOVE.L	#pf_SIZEOF,D2
	MOVE.L	D2,D0
	ZAP	D1
	CALLSYS	AllocMem,SysBase
	IFZL	D0,8$			;no memory!!
	MOVE.L	D0,A2
	MOVE.L	A2,A0			;preferences buffer
	MOVE.L	D2,D0
	CALLSYS	GetPrefs,IntuitionBase
	MOVE.W	pf_PaperLength(A2),D0
;;	SUBQ	#4,D0			;allow for margin	;2/14/90
	MOVE.B	D0,PageSize
	MOVE.L	A2,A1
	MOVE.L	D2,D0
	CALLSYS	FreeMem,SysBase
	CLC
	BRA.S	9$
8$:	STC
9$:	POP	A2/D2
	RTS

* Builds report name in TMP. and optionally appends device:path.
* This routine tries to get a lock on the report name, if any, defined by the
* operator.  If it is a file, we take the name "as is".  If it does not exist 
* then we assume that it is a file name which does not yet exist. 

* Builds a report name of the form: 'Repair.vol.date', appended to
* ReportPath.

BuildRptName:
	PUSH	A2
	LEA	TMP.,A2
	CLR.B	(A2)
	BSR	ConvertDosTime		;into TMP.
	MOVE.B	#'.',2(A2)		;force a decimal
	MOVE.L	A2,A0
	MOVE.	A0,TMP2.
	LEA	RepairName.,A0
	LEA	TMP3.,A1
	BSR	MoveInsertDevice	;insert volname and date
	CLR.B	TMP3.+30		;truncate to legal length
	MOVE.	ReportPath.,TMP.
	LEA	TMP.,A0
	IFNZB	(A0),1$			;got some path
	MOVE.B	#':',(A0)
	CLR.B	1(A0)
1$:	MOVE.B	(A0)+,D0
	BNE.S	1$
	DECL	A0
	MOVE.B	-1(A0),D0
	IFEQIB	D0,#':',2$
	IFEQIB	D0,#'/',2$
	ACHAR.	#'/',TMP.
2$:	APPEND.	TMP3.,TMP.
	POP	A2
	RTS

GetReportPath:
	MOVE.	ReportPath.,NamBuf
	DispReq	RptName,ReqNameGad
	IFNZB	CancelFlg,9$
	MOVE.	NamBuf,ReportPath.
9$:	RTS

* Ptr to filehdr or userdir block in A0.

DisplayFileBlk:
	LEA	fh_FileName(A0),A0
	ADD.L	BlockSize,A0

* Ptr to BSTR filename in A0.

DisplayFile:
	IFNZB	SavedSMode,1$
	RTS
1$:	LEA	FileName.,A1
	BSR	MoveFileName
	LEA	FileName.,A0
	BSR	Pad30
	BRA.S	UpdateFileName

DisplayPath:
	IFNZB	SavedSMode,1$
	RTS
1$:	LEA	FileName.,A0
	CLR.B	(A0)
	BSR.S	Pad30
UpdateNames:
	MOVE.	Path.,TMP2.
;;	LEA	TMP2.,A0
;;	BSR.S	Pad30	
	DispMsg	#RFilNam_LE,#RFilNam_TE,ProcDir.,WHITE ;restoring file
	DispCur	TMP2.,ORANGE,BLUE
	EraseEOL
UpdateFileName:
	DispMsg	#RFilNam_LE,#RFilNam_TE+14,ProcFile.,WHITE ;restoring file
	DispCur	FileName.,ORANGE,BLUE
	RTS

* Pads Z-string in A0 to a minimum of 30 chars, filling with blanks,
* as necessary.

Pad30:	MOVEQ	#32,D1
	MOVEQ	#FileNameLen,D0
	BRA.S	2$
1$:	TST.B	(A0)+
	BEQ.S	3$
2$:	DBF	D0,1$
	BRA.S	9$
3$:	DECL	A0
4$:	MOVE.B	D1,(A0)+
	DBF	D0,4$
5$:	CLR.B	(A0)
9$:	RTS

* Calculates day, month, and year from AmigaDOS date stamp.
* Stores in result in TMP2.  FROM EDN OCT 17, 1985 P. 168

ConvertDosDate:
	PUSH	D2
	MOVE.L	LocalDTS,D0		;days since Jan 1, 1978.
	ADDI.L	#28431,D0		;MAGIC NUMBER CONVERTS TO MARCH 1, 1900
	MULU	#4,D0
	SUBQ.L	#1,D0			;K2=4*K-1
	MOVE.L	D0,D2
	DIVU	#1461,D2		;Y=INT(K2/1461)
	MOVE.L	D2,D1
	SWAP	D1			;D=INT (K2 MOD 1461)
	ADDQ.W	#4,D1
	LSR.W	#2,D1			;D=INT ((D+4)/4)
	MULU	#5,D1
	SUBQ.L	#3,D1
	DIVU	#153,D1	 		;M=INT ((5*D-3)/153)
	MOVE.W	D1,D0
	SWAP	D1
	EXT.L	D1			;D=INT ((5*D-3) MOD 153)
	ADDQ.L	#5,D1
	DIVU	#5,D1			;D=INT ((D+5)/5)
	CMPI.W	#10,D0
	BLT.S	1$
	SUBI.W	#9,D0			;M=M-9
	ADDQ.W	#1,D2			;Y=Y+1
	BRA.S	2$
1$:	ADDQ.W	#3,D0

; month in D0, day in D1, year in D2 at this point

2$:	PUSH	D0			;save month
	STR.	W,D1,TMP3.,#32,#2	;convert day
	MOVE.	TMP3.,TMP2.
	ACHAR.	#'-',TMP2.
	POP	D0
	DECW	D0
	LSL.W	#2,D0			;change into long index
;;	LEA	MONTHS(D0.W),A0		;index month names
	LEA	MONTHS,A0
	ADDA.W	D0,A0
	APPEND.	A0,TMP2.
	ACHAR.	#'-',TMP2.
	STR.	W,D2,TMP3.,#'0',#2
	APPEND.	TMP3.,TMP2.
;	ACHAR.	#32,TMP2.		;add a trailing space
	POP	D2
	RTS

* Appends AmigaDOS time in HH:MM since midnight ito TMP.

ConvertDosTime:
	PUSH	D2
	MOVE.L	LocalDTS+4,D2		;minutes since midnight
	DIVU	#60,D2			;hours in low word, mins in high word
	STR.	W,D2,TMP2.,#32,#2	;convert hours
	APPEND.	TMP2.,TMP.
	ACHAR.	#':',TMP.		;format HH:MM
	SWAP	D2
	STR.	W,D2,TMP2.,#'0',#2	;minutes
	APPEND.	TMP2.,TMP.
	POP	D2
	RTS

* Menu selection routines:

ScnInteract:
	MOVEQ	#2,D0
	BRA.S	ScnCom
ScnInform:
	MOVEQ	#1,D0
	BRA.S	ScnCom
ScnQuiet:
	ZAP	D0
ScnCom:
	MOVE.B	D0,ScreenMode
	RTS

RptNone:
	ZAP	D0
	BRA.S	RptCom
RptDisk:
	BSR	GetReportPath
	MOVEQ	#1,D0
	BRA.S	RptCom
RptPtr:	MOVEQ	#2,D0
RptCom:	MOVE.B	D0,ReportMode
	RTS

PrmTbl	MACRO	LE,TE,MsgPtr,DataPtr
	DC.W	\1,\2
	DC.L	\3,\4
	ENDM

* This table is used to display results of 

ResultsTable
	PrmTbl	ResTxt_LE,ResLin1_TE,SerProb.,MajorErrCnt
	PrmTbl	ResTxt_LE,ResLin2_TE,MinProb.,MinorErrCnt
	PrmTbl	ResTxt_LE,ResLin3_TE,TotFiles.,TotalFileCount
	PrmTbl	ResTxt_LE,ResLin4_TE,BadFiles.,BadFileCount
	PrmTbl	ResTxt_LE,ResLin5_TE,TotDrawers.,TotalDirCount
	PrmTbl	ResTxt_LE,ResLin6_TE,BadDrawers.,BadDirCount
	PrmTbl	ResTxt_LE,ResLin7_TE,BadBlocks.,BadBlockCount
	DC.L	0

CRLF.		DC.B	10,0
PRT.		TEXTZ	'PRT:'

	BSS,PUBLIC

Handle		DS.L	1		;printer handle
ErrorCode	DS.W	1
ActionCode	DS.W	1
SavedRMode	DS.B	1		;copied from ReportMode for safety
SavedSMode	DS.B	1		;copied from ScreenMode for safety
LineCnt		DS.B	1		;lines remaining on page
;PageNo		DS.B	1		;page number
PageSize	DS.B	1		;pagesize in lines (from Prefs)

	END

