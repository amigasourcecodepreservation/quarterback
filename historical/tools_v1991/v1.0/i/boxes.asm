;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*	Copyright (c) 1987 Central Coast Software	*
*	    268 Bowie Dr, Los Osos, CA 93402		*
*	     All rights reserved, worldwide		*
*********************************************************

CReqLE	EQU	126
CReqTE	EQU	58
CReqWD	EQU	392
CReqHT	EQU	84

NamGad_LE	EQU	72
NamGad_TE	EQU	50
NamGad_WD	EQU	248
NamGad_HT	EQU	10

VNamGad_LE	EQU	72
VNamGad_TE	EQU	40
VNamGad_WD	EQU	248
VNamGad_HT	EQU	10

DupGad_LE	EQU	72
DupGad_TE	EQU	20
DupGad_WD	EQU	248
DupGad_HT	EQU	10

Dir_LE		EQU	196
Dir_TE		EQU	10
Dir_WD		EQU	444
Dir_HT		EQU	11

AR_WD	EQU	16		;DIRECTORY ARROWS
AR_HT	EQU	10

Files_LE	EQU	Dir_LE
Files_TE	EQU	Dir_TE+Dir_HT-1
Files_WD	EQU	Dir_WD-AR_WD+2
Files_HT	EQU	200-Files_TE

CTxt_LE		EQU	20
CTxt1_TE	EQU	14
CTxt2_TE	EQU	23

CTxt3_TE	EQU	38
CTxt4_TE	EQU	47

CFil_LE		EQU	CTxt_LE
CFil_TE		EQU	62

CAct_LE		EQU	CFil_LE
CAct_TE		EQU	71
CDel_LE		EQU	CFil_LE
CDel_TE		EQU	80

Root_LE		EQU	26
Root_TE		EQU	106
Root_WD		EQU	68
Root_HT		EQU	10

Par_LE		EQU	Root_LE+86
Par_TE		EQU	Root_TE
Par_WD		EQU	Root_WD
Par_HT		EQU	Root_HT

ResC_LE		EQU	30
ResC_TE		EQU	Root_TE+18
ResC_WD		EQU	140
ResC_HT		EQU	20

ResD_LE		EQU	ResC_LE
ResD_TE		EQU	ResC_TE+ResC_HT+6
ResD_WD		EQU	ResC_WD
ResD_HT		EQU	ResC_HT

CCan_LE		EQU	ResC_LE
CCan_TE		EQU	ResD_TE+ResD_HT+6
CCan_WD		EQU	ResC_WD
CCan_HT		EQU	ResC_HT

Up_LE		EQU	Dir_LE+Dir_WD-AR_WD
Up_TE		EQU	Files_TE
Up_WD		EQU	AR_WD
Up_HT		EQU	AR_HT

Dn_LE		EQU	Up_LE
Dn_TE		EQU	Files_TE+Files_HT-AR_HT-1
Dn_WD		EQU	AR_WD
Dn_HT		EQU	AR_HT

Prop_LE		EQU	Up_LE
Prop_TE		EQU	Up_TE+AR_HT
Prop_WD		EQU	AR_WD
Prop_HT		EQU	Files_HT-2*AR_HT

LBot_LE		EQU	100
LBot_TE		EQU	180
LBot_WD		EQU	68
LBot_HT		EQU	11

RBot_LE		EQU	472
RBot_TE		EQU	LBot_TE
RBot_WD		EQU	LBot_WD
RBot_HT		EQU	LBot_HT

DevTxt_LE	EQU	20
DevTxt_TE	EQU	20
DevTxt2_TE	EQU	35

Abort_LE	EQU	226
Abort_TE	EQU	183
Abort_WD	EQU	184
Abort_HT	EQU	11

Dev1_LE		EQU	14
Dev1_TE		EQU	35
Dev1_WD		EQU	192
Dev1_HT		EQU	10

Dev_SPACE	EQU	13

OneCol_LE	EQU	194
OneCol_TE	EQU	50
TwoCol_LE	EQU	57
TwoCol_TE	EQU	50
ThreeCol_LE	EQU	14
ThreeCol_TE	EQU	50
DeltaX2		EQU	2*Dev_SPACE
DeltaX3		EQU	Dev_SPACE
DeltaY		EQU	15

Dev2_LE		EQU	Dev1_LE+Dev1_WD+Dev_SPACE
Dev2_TE		EQU	Dev1_TE
Dev2_WD		EQU	Dev1_WD
Dev2_HT		EQU	Dev1_HT

Dev3_LE		EQU	Dev2_LE+Dev1_WD+Dev_SPACE
Dev3_TE		EQU	Dev1_TE
Dev3_WD		EQU	Dev1_WD
Dev3_HT		EQU	Dev1_HT

Dev4_LE		EQU	Dev1_LE
Dev4_TE		EQU	50
Dev4_WD		EQU	Dev1_WD
Dev4_HT		EQU	Dev1_HT

Dev5_LE		EQU	Dev4_LE+Dev1_WD+Dev_SPACE
Dev5_TE		EQU	Dev4_TE
Dev5_WD		EQU	Dev1_WD
Dev5_HT		EQU	Dev1_HT

Dev6_LE		EQU	Dev5_LE+Dev1_WD+Dev_SPACE
Dev6_TE		EQU	Dev4_TE
Dev6_WD		EQU	Dev1_WD
Dev6_HT		EQU	Dev1_HT

Dev7_LE		EQU	Dev1_LE
Dev7_TE		EQU	65
Dev7_WD		EQU	Dev1_WD
Dev7_HT		EQU	Dev1_HT

Dev8_LE		EQU	Dev7_LE+Dev1_WD+Dev_SPACE
Dev8_TE		EQU	Dev7_TE
Dev8_WD		EQU	Dev1_WD
Dev8_HT		EQU	Dev1_HT

Dev9_LE		EQU	Dev8_LE+Dev1_WD+Dev_SPACE
Dev9_TE		EQU	Dev7_TE
Dev9_WD		EQU	Dev1_WD
Dev9_HT		EQU	Dev1_HT

Dev10_LE	EQU	Dev1_LE
Dev10_TE	EQU	80
Dev10_WD	EQU	Dev1_WD
Dev10_HT	EQU	Dev1_HT

Dev11_LE	EQU	Dev10_LE+Dev1_WD+Dev_SPACE
Dev11_TE	EQU	Dev10_TE
Dev11_WD	EQU	Dev1_WD
Dev11_HT	EQU	Dev1_HT

Dev12_LE	EQU	Dev11_LE+Dev1_WD+Dev_SPACE
Dev12_TE	EQU	Dev10_TE
Dev12_WD	EQU	Dev1_WD
Dev12_HT	EQU	Dev1_HT

Dev13_LE	EQU	Dev1_LE
Dev13_TE	EQU	95
Dev13_WD	EQU	Dev1_WD
Dev13_HT	EQU	Dev1_HT

Dev14_LE	EQU	Dev13_LE+Dev1_WD+Dev_SPACE
Dev14_TE	EQU	Dev13_TE
Dev14_WD	EQU	Dev1_WD
Dev14_HT	EQU	Dev1_HT

Dev15_LE	EQU	Dev14_LE+Dev1_WD+Dev_SPACE
Dev15_TE	EQU	Dev13_TE
Dev15_WD	EQU	Dev1_WD
Dev15_HT	EQU	Dev1_HT

Dev16_LE	EQU	Dev1_LE
Dev16_TE	EQU	110
Dev16_WD	EQU	Dev1_WD
Dev16_HT	EQU	Dev1_HT

Dev17_LE	EQU	Dev16_LE+Dev1_WD+Dev_SPACE
Dev17_TE	EQU	Dev16_TE
Dev17_WD	EQU	Dev1_WD
Dev17_HT	EQU	Dev1_HT

Dev18_LE	EQU	Dev17_LE+Dev1_WD+Dev_SPACE
Dev18_TE	EQU	Dev16_TE
Dev18_WD	EQU	Dev1_WD
Dev18_HT	EQU	Dev1_HT


MenuTitle_TE	EQU	20
FunTitle_LE	EQU	40
FunTitle_TE	EQU	35

FunTxt_LE	EQU	180

FunGad_LE	EQU	FunTxt_LE-40
FunGad_WD	EQU	20
FunGad_HT	EQU	11

FunLin1_TE	EQU	54
FunLin2_TE	EQU	74
FunLin3_TE	EQU	94
FunLin4_TE	EQU	114
FunLin5_TE	EQU	134
FunLin6_TE	EQU	154
FunLin7_TE	EQU	174
;FunLin8_TE	EQU	148
;FunLin9_TE	EQU	164
;FunLin10_TE	EQU	180

PrmTitle_TE	EQU	20
PrmLTxt_LE	EQU	40
PrmRTxt_LE	EQU	360

PrmLin1_TE	EQU	36
PrmLin2_TE	EQU	47
PrmLin3_TE	EQU	58
PrmLin4_TE	EQU	69
PrmLin5_TE	EQU	80
PrmLin6_TE	EQU	91
PrmLin7_TE	EQU	102
PrmLin8_TE	EQU	113
PrmLin9_TE	EQU	124
PrmLin10_TE	EQU	135
PrmLin11_TE	EQU	146
PrmLin12_TE	EQU	157
PrmLin13_TE	EQU	168
PrmLin14_TE	EQU	179
PrmLin15_TE	EQU	190

ResTitle_TE	EQU	40
ResTxt_LE	EQU	140

ResLin1_TE	EQU	56
ResLin2_TE	EQU	70
ResLin3_TE	EQU	84
ResLin4_TE	EQU	98
ResLin5_TE	EQU	112
ResLin6_TE	EQU	126
ResLin7_TE	EQU	140
ResLin8_TE	EQU	154

BSTitle_TE	EQU	50
BSTxt_LE	EQU	40

RFilNam_LE	EQU	100
RFilNam_TE	EQU	110

ProgBar_TE	EQU	80
ProgBar_HT	EQU	10

BMTitle_TE	EQU	16
Bitmap_LE	EQU	62
Bitmap_TE	EQU	30
Bitmap_WD	EQU	520
Bitmap_HT	EQU	132

Frag_LE		EQU	Bitmap_LE
Frag_TE		EQU	Bitmap_TE+Bitmap_HT+8

SFC_LE		EQU	232
SFC_TE		EQU	ProgBar_TE+ProgBar_HT+20

;Lgnd_LE		EQU	16
;Lgnd_TE		EQU	12

;LgInc_TE	EQU	Lgnd_TE+10
;LgExc_TE	EQU	LgInc_TE+8
;LgAct_TE	EQU	LgExc_TE+8

NbrSpacing	EQU	11	;vertical pixel spacing for numbers

;;SStat_LE	EQU	CFC_LE
;;SStat_TE	EQU	CFC_TE+4*NbrSpacing

;;SFil_LE	EQU	SStat_LE
;;SFil_TE	EQU	SStat_TE+10

