;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*******************************************************************
*                                                                 *
*                                                                 *
*     DSM MC68000 Disassembler Version 1.0d (09/01/88).           *
*     Copyright (C) 1987, 1988 by OTG Software.                   *
*     All rights reserved.                                        *
*                                                                 *
*     Disassembly of :  sec                                       *
*                                                                 *
*                                                                 *
*******************************************************************


	SECTION	segment0,CODE
seg0
	jmp	L479(pc)

L1	dc.b	'topaz.font',0
	dc.b	0
L2	dc.b	'SECTORAMA by David Joiner / MicroIllusions, Copyright (c) 1987',0
	dc.b	0
L3	dc.b	'Quit',0
L4	dc.b	'Open Save File',0
L5	dc.b	'Close Save File',0
L6	dc.b	'By File Name',0
L7	dc.b	'By Parent Key',0
L8	dc.b	'By Header Key',0
L9	dc.b	'By Whole Block',0
L10	dc.b	'Show Last',0
L11	dc.b	'Compute Hash Value',0
L12	dc.b	'Map',0
	dc.b	0
L13	dc.b	'Write All',0
L14	dc.b	'Verify Directory',0
L15	dc.b	'Repair Directory',0
L16	dc.b	'Verify File',0
L17	dc.b	'Repair File',0
L18	dc.b	'Repair',0
L19	dc.b	'Search',0
L20	dc.b	'Mode',0
	dc.b	0
L21	dc.b	'OK',0
L22	dc.b	'Cancel',0

E23	link	a5,#-$000c
	move.l	$0008(a5),-$7b1a(a4)
	lea	-$7810(a4),a0
	move.l	a0,-$7b16(a4)
	move.l	$000c(a5),-$7c0e(a4)
	move.l	-$7926(a4),-(a7)
	pea	-$7aca(a4)
	jsr	E638(pc)
	addq.w	#8,a7
E24	movea.l	-$7926(a4),a0
	movea.l	$0056(a0),a1
	moveq	#0,d0
	move.b	$000f(a1),d0
	moveq	#1,d1
	asl.l	d0,d1
	move.l	d1,-(a7)
	jsr	L619(pc)
	addq.w	#4,a7
E25	movea.l	-$7926(a4),a0
	move.l	$0056(a0),-(a7)
	jsr	L610(pc)
	addq.w	#4,a7
	move.l	d0,-$000c(a5)
	beq.s	E30
	movea.l	-$000c(a5),a0
	move.l	$0014(a0),-$0004(a5)
	movea.l	-$000c(a5),a0
	move.l	$001c(a0),-$0008(a5)
	move.l	-$000c(a5),-(a7)
	jsr	L617(pc)
	addq.w	#4,a7
	cmpi.l	#$00000040,-$0004(a5)
	bne.s	E29
	lea	-$7b72(a4),a0
	movea.l	-$0008(a5),a1
	cmpa.l	a0,a1
	bne.s	E27
	moveq	#1,d0
E26	unlk	a5
	rts	

E27	lea	-$7af6(a4),a0
	movea.l	-$0008(a5),a1
	cmpa.l	a0,a1
	bne.s	E28
	moveq	#1,d0
	bra.s	E26

E28	moveq	#0,d0
	bra.s	E26

E29	bra.s	E25

E30	bra.s	E24

	dc.b	$60,$e2

L31	link	a5,#-$0020
	jsr	L576(pc)
	move.l	d0,-$78aa(a4)
	clr.l	-$78a6(a4)
	cmpi.l	#$00000002,$0008(a5)
	bge.s	L33
	pea	L144(pc)
	jsr	L369(pc)
	addq.w	#4,a7
	moveq	#0,d0
L32	unlk	a5
	rts	

L33	clr.l	-$78b6(a4)
	movea.l	-$798a(a4),a0
	move.l	$0022(a0),-$78c6(a4)
	movea.l	-$78c6(a4),a0
	move.l	$0018(a0),d0
	asl.l	#2,d0
	move.l	d0,-$78c2(a4)
	movea.l	-$78c2(a4),a0
	move.l	$0004(a0),d0
	asl.l	#2,d0
	move.l	d0,-$78be(a4)
L34	tst.l	-$78be(a4)
	beq.l	L38
	movea.l	-$78be(a4),a0
	tst.l	$0004(a0)
	bne.l	L37
	movea.l	-$78be(a4),a0
	tst.l	$0028(a0)
	beq.l	L37
	movea.l	-$78be(a4),a0
	move.l	$0028(a0),d0
	asl.l	#2,d0
	move.l	d0,-$001a(a5)
	movea.l	$000c(a5),a0
	move.l	$0004(a0),-$001e(a5)
	movea.l	-$001a(a5),a0
	addq.l	#1,-$001a(a5)
	moveq	#0,d0
	move.b	(a0),d0
	move.l	d0,-$0010(a5)
	clr.l	-$0004(a5)
	bra.s	L36

L35	movea.l	-$001e(a5),a0
	addq.l	#1,-$001e(a5)
	moveq	#0,d0
	move.b	(a0),d0
	move.l	d0,-(a7)
	movea.l	-$001a(a5),a1
	addq.l	#1,-$001a(a5)
	moveq	#0,d1
	move.b	(a1),d1
	move.l	d1,-(a7)
	jsr	L415(pc)
	addq.w	#8,a7
	tst.l	d0
	beq.s	L37
	addq.l	#1,-$0004(a5)
L36	move.l	-$0004(a5),d0
	cmp.l	-$0010(a5),d0
	blt.s	L35
	movea.l	-$78be(a4),a0
	move.l	$001c(a0),d0
	asl.l	#2,d0
	move.l	d0,-$78ba(a4)
	tst.l	-$78ba(a4)
	beq.s	L37
	pea	-$76ea(a4)
	movea.l	-$78ba(a4),a0
	move.l	$0004(a0),d0
	asl.l	#2,d0
	move.l	d0,-(a7)
	jsr	L361(pc)
	addq.w	#8,a7
	lea	-$76ea(a4),a0
	move.l	a0,-$78b6(a4)
	movea.l	-$78ba(a4),a0
	move.l	$0008(a0),d0
	asl.l	#2,d0
	move.l	d0,-$78ea(a4)
	movea.l	-$78ba(a4),a0
	move.l	(a0),-$78b2(a4)
	movea.l	-$78ba(a4),a0
	move.l	$000c(a0),-$78ae(a4)
	tst.l	-$78b6(a4)
	beq.s	L37
	tst.l	-$78ea(a4)
L37	movea.l	-$78be(a4),a0
	move.l	(a0),d0
	asl.l	#2,d0
	move.l	d0,-$78be(a4)
	bra.l	L34

L38	tst.l	-$78b6(a4)
	bne.s	L39
	pea	L145(pc)
	jsr	L369(pc)
	addq.w	#4,a7
	movea.l	$000c(a5),a0
	move.l	$0004(a0),-(a7)
	jsr	L369(pc)
	addq.w	#4,a7
	pea	L146(pc)
	jsr	L369(pc)
	addq.w	#4,a7
	bra.l	L131

L39	movea.l	-$78ea(a4),a0
	move.l	$0014(a0),d0
	movea.l	-$78ea(a4),a1
	move.l	$000c(a1),d1
	jsr	L569(pc)
	move.l	d0,-$78da(a4)
	movea.l	-$78ea(a4),a0
	move.l	$0024(a0),d0
	move.l	-$78da(a4),d1
	jsr	L569(pc)
	move.l	d0,-$78e6(a4)
	movea.l	-$78ea(a4),a0
	move.l	$0028(a0),d0
	addq.l	#1,d0
	move.l	-$78da(a4),d1
	jsr	L569(pc)
	move.l	d0,-$78e2(a4)
	move.l	-$78e2(a4),d0
	sub.l	-$78e6(a4),d0
	move.l	d0,-$78de(a4)
	moveq	#2,d1
	move.l	-$78de(a4),d0
	jsr	L533(pc)
	move.l	d0,-$78d6(a4)
	clr.l	-$7932(a4)
	clr.l	-$7936(a4)
	clr.l	-$792e(a4)
	clr.l	-$7926(a4)
	clr.l	-$7916(a4)
	clr.l	-$792a(a4)
	clr.l	-$78d2(a4)
	clr.l	-$78ce(a4)
	clr.l	-$78ca(a4)
	clr.w	-$7944(a4)
	clr.l	-$7922(a4)
	clr.l	-(a7)
	pea	L147(pc)
	jsr	L614(pc)
	addq.w	#8,a7
	move.l	d0,-$7936(a4)
	beq.l	L131
	clr.l	-(a7)
	pea	L148(pc)
	jsr	L614(pc)
	addq.w	#8,a7
	move.l	d0,-$7932(a4)
	beq.l	L131
	clr.l	-(a7)
	pea	L149(pc)
	jsr	L614(pc)
	addq.w	#8,a7
	move.l	d0,-$792e(a4)
	bne.s	L40
	pea	L150(pc)
	jsr	L369(pc)
	addq.w	#4,a7
	bra.l	L131

L40	pea	-$7ffe(a4)
	jsr	L640(pc)
	addq.w	#4,a7
	move.l	d0,-$7922(a4)
	tst.l	-$7922(a4)
	bne.s	L41
	pea	L151(pc)
	jsr	L369(pc)
	addq.w	#4,a7
	bra.l	L131

L41	dc.b	$48,$78,0,$02
				;	pea	($0002).w
	dc.b	$48,$78,$64,0
				;	pea	($6400).w
	jsr	L601(pc)
	addq.w	#8,a7
	move.l	d0,-$78d2(a4)
	tst.l	-$78d2(a4)
	beq.l	L131
	pea	-$7fee(a4)
	jsr	L636(pc)
	addq.w	#4,a7
	move.l	d0,-$792a(a4)
	beq.l	L131
	move.l	-$792a(a4),d0
	add.l	#$0000002c,d0
	move.l	d0,-$791a(a4)
	dc.b	$48,$78,0,4
				;	pea	($0004).w
	pea	-$7ff6(a4)
	move.l	-$791a(a4),-(a7)
	jsr	L623(pc)
	lea	$000c(a7),a7
	move.l	-$792a(a4),-$7fb0(a4)
	pea	-$7fce(a4)
	jsr	L637(pc)
	addq.w	#4,a7
	move.l	d0,-$7926(a4)
	beq.l	L131
	movea.l	-$7926(a4),a0
	move.l	$0032(a0),-$791e(a4)
	pea	-$7c38(a4)
	move.l	-$7926(a4),-(a7)
	jsr	L639(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$01
				;	pea	($0001).w
	move.l	-$791e(a4),-(a7)
	jsr	L625(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$1c
				;	pea	($001c).w
	clr.l	-(a7)
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$1c
				;	pea	($001c).w
	dc.b	$48,$78,1,$c2
				;	pea	($01c2).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,1,$82
				;	pea	($0182).w
	dc.b	$48,$78,1,$c2
				;	pea	($01c2).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,1,$82
				;	pea	($0182).w
	clr.l	-(a7)
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$1d
				;	pea	($001d).w
	clr.l	-(a7)
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$1d
				;	pea	($001d).w
	dc.b	$48,$78,1,$c1
				;	pea	($01c1).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,1,$81
				;	pea	($0181).w
	dc.b	$48,$78,1,$c1
				;	pea	($01c1).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,1,$81
				;	pea	($0181).w
	dc.b	$48,$78,0,$01
				;	pea	($0001).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$1d
				;	pea	($001d).w
	dc.b	$48,$78,0,$01
				;	pea	($0001).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$1c
				;	pea	($001c).w
	dc.b	$48,$78,1,$c7
				;	pea	($01c7).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$1c
				;	pea	($001c).w
	dc.b	$48,$78,2,$7f
				;	pea	($027f).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$1d
				;	pea	($001d).w
	dc.b	$48,$78,1,$c7
				;	pea	($01c7).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$1d
				;	pea	($001d).w
	dc.b	$48,$78,2,$7f
				;	pea	($027f).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$d1
				;	pea	($00d1).w
	dc.b	$48,$78,1,$c7
				;	pea	($01c7).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$d1
				;	pea	($00d1).w
	dc.b	$48,$78,2,$7f
				;	pea	($027f).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$d2
				;	pea	($00d2).w
	dc.b	$48,$78,1,$c7
				;	pea	($01c7).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$d2
				;	pea	($00d2).w
	dc.b	$48,$78,2,$7f
				;	pea	($027f).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,1,$1c
				;	pea	($011c).w
	dc.b	$48,$78,1,$c7
				;	pea	($01c7).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,1,$1c
				;	pea	($011c).w
	dc.b	$48,$78,2,$7f
				;	pea	($027f).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,1,$1d
				;	pea	($011d).w
	dc.b	$48,$78,1,$c7
				;	pea	($01c7).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,1,$1d
				;	pea	($011d).w
	dc.b	$48,$78,2,$7f
				;	pea	($027f).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,1,$5c
				;	pea	($015c).w
	dc.b	$48,$78,1,$c7
				;	pea	($01c7).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,1,$5c
				;	pea	($015c).w
	dc.b	$48,$78,2,$7f
				;	pea	($027f).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,1,$5d
				;	pea	($015d).w
	dc.b	$48,$78,1,$c7
				;	pea	($01c7).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,1,$5d
				;	pea	($015d).w
	dc.b	$48,$78,2,$7f
				;	pea	($027f).w
	move.l	-$791e(a4),-(a7)
	jsr	L622(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,1,$69
				;	pea	($0169).w
	dc.b	$48,$78,1,$c6
				;	pea	($01c6).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L152(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	movea.l	$000c(a5),a0
	move.l	$0004(a0),-(a7)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,1,$74
				;	pea	($0174).w
	dc.b	$48,$78,1,$c6
				;	pea	($01c6).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L153(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$02
				;	pea	($0002).w
	move.l	-$78b2(a4),-(a7)
	jsr	L448(pc)
	addq.w	#8,a7
	dc.b	$48,$78,1,$7f
				;	pea	($017f).w
	dc.b	$48,$78,1,$c6
				;	pea	($01c6).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L154(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	move.l	-$78b6(a4),-(a7)
	jsr	L371(pc)
	addq.w	#4,a7
	clr.l	-(a7)
	pea	-$771a(a4)
	dc.b	$48,$78,$ff,$ff
				;	pea	($ffffffff).w
	pea	L155(pc)
	jsr	L613(pc)
	lea	$0010(a7),a7
	tst.l	d0
	beq.s	L42
	pea	L156(pc)
	jsr	L369(pc)
	addq.w	#4,a7
	bra.l	L131

L42	move.l	-$7706(a4),-$7916(a4)
	clr.l	-(a7)
	clr.l	-(a7)
	jsr	L584(pc)
	addq.w	#8,a7
	move.l	d0,-$78ce(a4)
	tst.l	-$78ce(a4)
	bne.s	L43
	pea	L157(pc)
	jsr	L369(pc)
	addq.w	#4,a7
	bra.l	L131

L43	dc.b	$48,$78,0,$38
				;	pea	($0038).w
	move.l	-$78ce(a4),-(a7)
	jsr	L594(pc)
	addq.w	#8,a7
	move.l	d0,-$78ca(a4)
	tst.l	-$78ca(a4)
	bne.s	L44
	pea	L158(pc)
	jsr	L369(pc)
	addq.w	#4,a7
	bra.l	L131

L44	move.l	-$78ae(a4),-(a7)
	move.l	-$78ca(a4),-(a7)
	move.l	-$78b2(a4),-(a7)
	move.l	-$78b6(a4),-(a7)
	jsr	L613(pc)
	lea	$0010(a7),a7
	move.l	d0,-$000c(a5)
	tst.l	-$000c(a5)
	beq.s	L45
	pea	L159(pc)
	jsr	L369(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,3
				;	pea	($0003).w
	move.l	-$000c(a5),-(a7)
	jsr	L370(pc)
	addq.w	#8,a7
	pea	L160(pc)
	jsr	L369(pc)
	addq.w	#4,a7
	bra.l	L131

L45	move.w	#$0001,-$7944(a4)
	clr.w	-$793c(a4)
	move.l	-$78d2(a4),-$7902(a4)
	move.l	-$78d2(a4),-$78f2(a4)
	move.l	-$7902(a4),d0
	add.l	#$00001400,d0
	move.l	d0,-$78fe(a4)
	move.l	-$7902(a4),d0
	add.l	#$00001200,d0
	move.l	d0,-$78fa(a4)
	move.l	-$7902(a4),d0
	add.l	#$00000600,d0
	move.l	d0,-$78ee(a4)
	jsr	L189(pc)
L46	movea.l	-$7926(a4),a0
	movea.l	$0056(a0),a1
	moveq	#0,d0
	move.b	$000f(a1),d0
	moveq	#1,d1
	asl.l	d0,d1
	move.l	d1,-(a7)
	jsr	L619(pc)
	addq.w	#4,a7
L47	movea.l	-$7926(a4),a0
	move.l	$0056(a0),-(a7)
	jsr	L610(pc)
	addq.w	#4,a7
	move.l	d0,-$790e(a4)
	beq.l	L130
	movea.l	-$790e(a4),a0
	move.l	$0014(a0),-$790a(a4)
	movea.l	-$790e(a4),a0
	move.w	$0018(a0),-$794a(a4)
	movea.l	-$790e(a4),a0
	move.l	$001c(a0),-$7906(a4)
	movea.l	-$790e(a4),a0
	move.w	$001a(a0),-$794c(a4)
	movea.l	-$790e(a4),a0
	move.w	$0020(a0),-$7948(a4)
	movea.l	-$790e(a4),a0
	move.w	$0022(a0),-$7946(a4)
	move.l	-$790e(a4),-(a7)
	jsr	L617(pc)
	addq.w	#4,a7
	cmpi.l	#$00000010,-$790a(a4)
	bne.s	L48
	bra.l	L129

L48	cmpi.l	#$00000400,-$790a(a4)
	bne.l	L107
	move.b	#1,-$7822(a4)
	move.w	-$794a(a4),-$7820(a4)
	move.w	-$794c(a4),d0
	ext.l	d0
	and.l	#$00000003,d0
	move.w	d0,-$781e(a4)
	clr.l	-(a7)
	dc.b	$48,$78,0,$0f
				;	pea	($000f).w
	pea	-$786e(a4)
	pea	-$7826(a4)
	jsr	L641(pc)
	lea	$0010(a7),a7
	move.w	d0,-$794e(a4)
	move.b	-$786e(a4),-$0011(a5)
	cmpi.w	#$0002,-$793c(a4)
	ble.s	L49
	cmpi.w	#$0046,-$794a(a4)
	bge.s	L49
	moveq	#0,d0
	move.b	-$0011(a5),d0
	move.l	d0,-(a7)
	jsr	L166(pc)
	addq.w	#4,a7
	bra.l	L106

L49	tst.w	-$793c(a4)
	beq.l	L58
	cmpi.w	#$0044,-$794a(a4)
	bge.l	L58
	cmpi.w	#$0001,-$793c(a4)
	bne.s	L50
	move.l	-$78f2(a4),-$001a(a5)
	move.w	-$7940(a4),d0
	movea.l	-$001a(a5),a0
	move.b	-$0011(a5),0(a0,d0.w)
	jsr	L299(pc)
	addq.w	#1,-$7940(a4)
	jsr	L298(pc)
	bra.l	L57

L50	cmpi.w	#$0002,-$793c(a4)
	bne.l	L57
	moveq	#0,d0
	move.b	-$0011(a5),d0
	move.l	d0,-$0004(a5)
	cmpi.b	#$30,-$0011(a5)
	bcs.s	L51
	cmpi.b	#$39,-$0011(a5)
	bhi.s	L51
	moveq	#0,d0
	move.b	-$0011(a5),d0
	sub.l	#$00000030,d0
	move.l	d0,-$0004(a5)
	bra.s	L54

L51	cmpi.b	#$41,-$0011(a5)
	bcs.s	L52
	cmpi.b	#$46,-$0011(a5)
	bhi.s	L52
	moveq	#0,d0
	move.b	-$0011(a5),d0
	sub.l	#$00000037,d0
	move.l	d0,-$0004(a5)
	bra.s	L54

L52	cmpi.b	#$61,-$0011(a5)
	bcs.s	L53
	cmpi.b	#$66,-$0011(a5)
	bhi.s	L53
	moveq	#0,d0
	move.b	-$0011(a5),d0
	sub.l	#$00000057,d0
	move.l	d0,-$0004(a5)
	bra.s	L54

L53	bra.s	L57

L54	move.w	-$793e(a4),d0
	ext.l	d0
	asr.l	#1,d0
	move.w	d0,-$7940(a4)
	move.l	-$78f2(a4),-$001a(a5)
	move.w	-$7940(a4),d0
	movea.l	-$001a(a5),a0
	moveq	#0,d1
	move.b	0(a0,d0.w),d1
	move.l	d1,-$0008(a5)
	btst	#0,-$793d(a4)
	beq.s	L55
	move.l	-$0004(a5),d0
	and.l	#$0000000f,d0
	move.l	-$0008(a5),d1
	and.l	#$000000f0,d1
	or.l	d1,d0
	move.l	d0,-$0004(a5)
	bra.s	L56

L55	move.l	-$0004(a5),d0
	asl.l	#4,d0
	and.l	#$000000f0,d0
	move.l	-$0008(a5),d1
	and.l	#$0000000f,d1
	or.l	d1,d0
	move.l	d0,-$0004(a5)
L56	move.w	-$7940(a4),d0
	movea.l	-$001a(a5),a0
	move.b	-$0001(a5),0(a0,d0.w)
	jsr	L299(pc)
	addq.w	#1,-$793e(a4)
	jsr	L298(pc)
L57	bra.l	L106

L58	btst	#$07,-$7949(a4)
	bne.l	L106
	move.l	-$7902(a4),-$001a(a5)
	move.w	-$794a(a4),d0
	ext.l	d0
	bra.l	L105

L59	move.w	-$7942(a4),d0
	ext.l	d0
	asl.l	#2,d0
	movea.l	-$78f2(a4),a0
	move.l	0(a0,d0.l),-(a7)
	jsr	L188(pc)
	addq.w	#4,a7
	bra.l	L106

L60	jsr	L189(pc)
	bra.l	L106

L61	movea.l	-$001a(a5),a0
	tst.l	$01f4(a0)
	beq.s	L62
	movea.l	-$001a(a5),a0
	move.l	$01f4(a0),-(a7)
	jsr	L188(pc)
	addq.w	#4,a7
	bra.s	L63

L62	move.l	-$792a(a4),-(a7)
	jsr	L635(pc)
	addq.w	#4,a7
L63	bra.l	L106

L64	movea.l	-$001a(a5),a0
	tst.l	$01f0(a0)
	beq.s	L65
	movea.l	-$001a(a5),a0
	move.l	$01f0(a0),-(a7)
	jsr	L188(pc)
	addq.w	#4,a7
	bra.s	L66

L65	move.l	-$792a(a4),-(a7)
	jsr	L635(pc)
	addq.w	#4,a7
L66	bra.l	L106

L67	movea.l	-$001a(a5),a0
	tst.l	$01f8(a0)
	beq.s	L68
	movea.l	-$001a(a5),a0
	move.l	$01f8(a0),-(a7)
	jsr	L188(pc)
	addq.w	#4,a7
	bra.s	L69

L68	move.l	-$792a(a4),-(a7)
	jsr	L635(pc)
	addq.w	#4,a7
L69	bra.l	L106

L70	movea.l	-$001a(a5),a0
	tst.l	$0004(a0)
	beq.s	L71
	movea.l	-$001a(a5),a0
	move.l	$0004(a0),-(a7)
	jsr	L188(pc)
	addq.w	#4,a7
	bra.s	L72

L71	move.l	-$792a(a4),-(a7)
	jsr	L635(pc)
	addq.w	#4,a7
L72	bra.l	L106

L73	movea.l	-$001a(a5),a0
	tst.l	$0010(a0)
	beq.s	L74
	movea.l	-$001a(a5),a0
	move.l	$0010(a0),-(a7)
	jsr	L188(pc)
	addq.w	#4,a7
	bra.s	L75

L74	move.l	-$792a(a4),-(a7)
	jsr	L635(pc)
	addq.w	#4,a7
L75	bra.l	L106

L76	jsr	L295(pc)
	bra.l	L106

L77	jsr	L299(pc)
	move.w	#$0002,-$793c(a4)
	move.w	-$7942(a4),d0
	ext.l	d0
	asl.l	#3,d0
	move.w	d0,-$793e(a4)
	jsr	L298(pc)
	bra.l	L106

L78	jsr	L299(pc)
	move.w	#$0001,-$793c(a4)
	move.w	-$7942(a4),d0
	ext.l	d0
	asl.l	#2,d0
	move.w	d0,-$7940(a4)
	jsr	L298(pc)
	bra.l	L106

L79	jsr	L299(pc)
	cmpi.w	#$0001,-$793c(a4)
	bne.s	L80
	move.w	-$7940(a4),d0
	ext.l	d0
	divs	#$0004,d0
	move.w	d0,-$7942(a4)
	bra.s	L81

L80	cmpi.w	#$0002,-$793c(a4)
	bne.s	L81
	move.w	-$793e(a4),d0
	ext.l	d0
	divs	#$0008,d0
	move.w	d0,-$7942(a4)
L81	clr.w	-$793c(a4)
	jsr	L298(pc)
	bra.l	L106

L82	move.l	-$7902(a4),-(a7)
	jsr	L357(pc)
	addq.w	#4,a7
	move.l	d0,-$0004(a5)
	movea.l	-$7902(a4),a0
	move.l	-$0004(a5),$0014(a0)
	jsr	L233(pc)
	bra.l	L106

L83	move.l	-$7902(a4),-(a7)
	jsr	L357(pc)
	addq.w	#4,a7
	move.l	d0,-$0004(a5)
	movea.l	-$7902(a4),a0
	move.l	$0014(a0),d0
	cmp.l	-$0004(a5),d0
	beq.s	L84
	bra.s	L85

L84	move.w	-$794c(a4),d0
	and.w	#$0003,d0
	beq.s	L85
	jsr	L187(pc)
L85	bra.l	L106

L86	tst.l	-$78a6(a4)
	beq.s	L87
	dc.b	$48,$78,1,$e8
				;	pea	($01e8).w
	movea.l	-$7902(a4),a0
	pea	$0018(a0)
	move.l	-$78a6(a4),-(a7)
	jsr	L578(pc)
	lea	$000c(a7),a7
L87	bra.l	L106

L88	jsr	L299(pc)
	cmpi.w	#$0001,-$793c(a4)
	bne.s	L93
	cmpi.w	#$004c,-$794a(a4)
	bne.s	L89
	subi.w	#$0010,-$7940(a4)
	bra.s	L92

L89	cmpi.w	#$004d,-$794a(a4)
	bne.s	L90
	addi.w	#$0010,-$7940(a4)
	bra.s	L92

L90	cmpi.w	#$004e,-$794a(a4)
	bne.s	L91
	addq.w	#1,-$7940(a4)
	bra.s	L92

L91	cmpi.w	#$004f,-$794a(a4)
	bne.s	L92
	subq.w	#1,-$7940(a4)
L92	bra.l	L103

L93	cmpi.w	#$0002,-$793c(a4)
	bne.s	L98
	cmpi.w	#$004c,-$794a(a4)
	bne.s	L94
	subi.w	#$0020,-$793e(a4)
	bra.s	L97

L94	cmpi.w	#$004d,-$794a(a4)
	bne.s	L95
	addi.w	#$0020,-$793e(a4)
	bra.s	L97

L95	cmpi.w	#$004e,-$794a(a4)
	bne.s	L96
	addq.w	#1,-$793e(a4)
	bra.s	L97

L96	cmpi.w	#$004f,-$794a(a4)
	bne.s	L97
	subq.w	#1,-$793e(a4)
L97	bra.s	L103

L98	cmpi.w	#$0002,-$793c(a4)
	ble.s	L99
	move.w	-$794a(a4),d0
	ext.l	d0
	movea.l	d0,a0
	pea	-$004c(a0)
	jsr	L166(pc)
	addq.w	#4,a7
	bra.s	L103

L99	cmpi.w	#$004c,-$794a(a4)
	bne.s	L100
	subq.w	#4,-$7942(a4)
	bra.s	L103

L100	cmpi.w	#$004d,-$794a(a4)
	bne.s	L101
	addq.w	#4,-$7942(a4)
	bra.s	L103

L101	cmpi.w	#$004e,-$794a(a4)
	bne.s	L102
	addq.w	#1,-$7942(a4)
	bra.s	L103

L102	cmpi.w	#$004f,-$794a(a4)
	bne.s	L103
	subq.w	#1,-$7942(a4)
L103	cmpi.w	#$0003,-$793c(a4)
	bge.s	L104
	jsr	L298(pc)
L104	bra.s	L106

L105	sub.l	#$00000011,d0
	beq.l	L86
	subq.l	#1,d0
	beq.l	L77
	subq.l	#1,d0
	beq.l	L60
	subq.l	#3,d0
	beq.l	L83
	subq.l	#3,d0
	beq.l	L61
	subq.l	#$7,d0
	beq.l	L78
	subq.l	#2,d0
	beq.l	L73
	subq.l	#3,d0
	beq.l	L70
	subq.l	#1,d0
	beq.l	L59
	subq.l	#1,d0
	beq.l	L82
	subq.l	#1,d0
	beq.l	L76
	sub.l	#$0000000a,d0
	beq.l	L67
	subq.l	#1,d0
	beq.l	L64
	sub.l	#$00000011,d0
	beq.l	L79
	subq.l	#1,d0
	beq.l	L79
	bra.l	L88

L106	bra.l	L129

L107	cmpi.l	#$00000008,-$790a(a4)
	bne.l	L115
	cmpi.w	#$0068,-$794a(a4)
	bne.l	L114
	jsr	L197(pc)
	tst.l	d0
	beq.s	L108
	bra.l	L114

L108	cmpi.w	#$01c0,-$7948(a4)
	bge.l	L114
	cmpi.w	#$0180,-$7946(a4)
	bge.l	L114
	cmpi.w	#$0003,-$793c(a4)
	bge.l	L114
	move.w	-$7946(a4),d0
	ext.l	d0
	sub.l	#$0000001e,d0
	moveq	#$0b,d1
	jsr	L533(pc)
	move.w	d0,-$001a(a5)
	tst.w	-$001a(a5)
	bge.s	L109
	clr.w	-$001a(a5)
L109	cmpi.w	#$013f,-$7948(a4)
	bge.s	L111
	move.w	-$7948(a4),d0
	ext.l	d0
	sub.l	#$00000020,d0
	move.w	d0,-$0018(a5)
	tst.w	-$0018(a5)
	bge.s	L110
	clr.w	-$0018(a5)
L110	move.w	-$0018(a5),d0
	ext.l	d0
	moveq	#$48,d1
	jsr	L533(pc)
	asl.l	#3,d0
	move.l	d0,-(a7)
	move.w	-$0018(a5),d0
	ext.l	d0
	moveq	#$48,d1
	jsr	L538(pc)
	moveq	#8,d1
	jsr	L533(pc)
	move.l	(a7)+,d2
	add.l	d0,d2
	move.w	-$001a(a5),d3
	ext.l	d3
	asl.l	#$5,d3
	add.l	d3,d2
	move.w	d2,-$001c(a5)
	bra.s	L113

L111	cmpi.w	#$0140,-$7948(a4)
	ble.s	L113
	move.w	-$7948(a4),d0
	ext.l	d0
	sub.l	#$00000140,d0
	moveq	#$04,d1
	jsr	L533(pc)
	move.w	d0,-$0018(a5)
	tst.w	-$0018(a5)
	bge.s	L112
	clr.w	-$0018(a5)
L112	move.w	-$001a(a5),d0
	ext.l	d0
	asl.l	#$5,d0
	move.w	-$0018(a5),d1
	ext.l	d1
	add.l	d1,d0
	move.w	d0,-$001c(a5)
L113	jsr	L299(pc)
	move.w	-$001c(a5),-$793e(a4)
	move.w	-$001c(a5),d0
	ext.l	d0
	divs	#$0002,d0
	move.w	d0,-$7940(a4)
	move.w	-$001c(a5),d0
	ext.l	d0
	divs	#$0008,d0
	move.w	d0,-$7942(a4)
	jsr	L298(pc)
L114	bra.l	L129

L115	cmpi.l	#$00000100,-$790a(a4)
	bne.l	L129
	cmpi.w	#-$0001,-$794a(a4)
	bne.s	L116
	bra.l	L129

L116	move.w	-$794a(a4),d0
	and.w	#$001f,d0
	bne.s	L120
	move.w	-$794a(a4),d0
	ext.l	d0
	asr.l	#$5,d0
	and.l	#$0000003f,d0
	bra.s	L118

	dc.b	$60,$60,$4a,$ac,$87,$5a
	dc.b	$66,$24,$48,$7a,3,$d7
	dc.b	$48,$6c,$88,$68

	jsr	E23(pc)
	addq.w	#8,a7
	tst.l	d0
	beq.s	E117
	dc.b	$48,$78,3,$ee
				;	pea	($03ee).w
	pea	-$7798(a4)
	jsr	E574(pc)
	addq.w	#8,a7
	move.l	d0,-$78a6(a4)
E117	bra.s	L119

	dc.b	$4a,$ac,$87,$5a,$67,$0a
	dc.b	$2f,$2c,$87,$5a

	jsr	L570(pc)
	addq.w	#4,a7
	clr.l	-$78a6(a4)
	bra.s	L119

	dc.b	$60,0,1,$8a,$ff,$e4
	dc.b	$ff,$ce,$ff,$a2,$ff,$a0

L118	cmp.l	#$00000004,d0
	bcc.s	L119
	asl.l	#1,d0
	move.w	*+$02-$14(pc,d0.w),d0
	jmp	*+$02+0(pc,d0.w)

L119	bra.l	L129

L120	move.w	-$794a(a4),d0
	ext.l	d0
	and.l	#$0000001f,d0
	cmp.l	#$00000001,d0
	bne.l	L125
	move.w	-$794a(a4),d0
	ext.l	d0
	asr.l	#$5,d0
	and.l	#$0000003f,d0
	bra.l	L123

	dc.b	$48,$7a,3,$63,$48,$6c
	dc.b	$88,$18

	jsr	E23(pc)
	addq.w	#8,a7
	tst.l	d0
	beq.s	E121
	dc.b	$48,$78,0,$01
				;	pea	($0001).w
	jsr	E309(pc)
	addq.w	#4,a7
E121	bra.l	L124

	dc.b	$48,$78,0,$02

	jsr	E309(pc)
	addq.w	#4,a7
	bra.l	L124

	dc.b	$48,$78,0,3

	jsr	E309(pc)
	addq.w	#4,a7
	bra.s	L124

	dc.b	$48,$78,0,4

	jsr	E309(pc)
	addq.w	#4,a7
	bra.s	L124

	jsr	L295(pc)
	bra.s	L124

	dc.b	$48,$7a,3,$2c,$48,$6c
	dc.b	$88,$40

	jsr	E23(pc)
	addq.w	#8,a7
	tst.l	d0
	beq.s	E122
	jsr	L299(pc)
	pea	-$77c0(a4)
	jsr	E411(pc)
	addq.w	#4,a7
	move.w	d0,-$7942(a4)
	move.w	-$7942(a4),d0
	ext.l	d0
	asl.l	#3,d0
	move.w	d0,-$793e(a4)
	move.w	-$7942(a4),d0
	ext.l	d0
	asl.l	#2,d0
	move.w	d0,-$7940(a4)
	jsr	L298(pc)
E122	bra.s	L124

	dc.b	$ff,$e0,$ff,$a0,$ff,$9a
	dc.b	$ff,$8e,$ff,$82,$ff,$74
	dc.b	$ff,$54

L123	cmp.l	#$00000007,d0
	bcc.s	L124
	asl.l	#1,d0
	move.w	*+$02-$1a(pc,d0.w),d0
	jmp	*+$02+0(pc,d0.w)

L124	bra.l	L129

L125	move.w	-$794a(a4),d0
	ext.l	d0
	and.l	#$0000001f,d0
	cmp.l	#$00000002,d0
	bne.s	L128
	move.w	-$794a(a4),d0
	ext.l	d0
	asr.l	#$5,d0
	and.l	#$0000003f,d0
	bra.s	L126

	jsr	E343(pc)
	dc.b	$48,$78,0,$01
				;	pea	($0001).w
	jsr	E322(pc)
	addq.w	#4,a7
	bra.s	L127

	dc.b	$48,$78,0,$02

	jsr	E322(pc)
	addq.w	#4,a7
	bra.s	L127

	dc.b	$48,$78,0,$01

	jsr	E322(pc)
	addq.w	#4,a7
	bra.s	L127

	dc.b	$48,$78,0,$01

	jsr	E322(pc)
	addq.w	#4,a7
	bra.s	L127

	dc.b	$ff,$da,$ff,$ce,$ff,$c2
	dc.b	$ff,$b6,$ff,$b2

L126	cmp.l	#$00000005,d0
	bcc.s	L127
	asl.l	#1,d0
	move.w	*+$02-$16(pc,d0.w),d0
	jmp	*+$02+0(pc,d0.w)

L127	bra.s	L129

L128	move.w	-$794a(a4),d0
	ext.l	d0
	and.l	#$0000001f,d0
	cmp.l	#$00000002,d0
L129	bra.l	L47

L130	bra.l	L46

L131	jsr	L348(pc)
	tst.l	-$78a6(a4)
	beq.s	L132
	move.l	-$78a6(a4),-(a7)
	jsr	L570(pc)
	addq.w	#4,a7
L132	tst.w	-$7944(a4)
	beq.s	L133
	move.l	-$78ca(a4),-(a7)
	jsr	L581(pc)
	addq.w	#4,a7
L133	tst.l	-$78ca(a4)
	beq.s	L134
	dc.b	$48,$78,0,$38
				;	pea	($0038).w
	move.l	-$78ca(a4),-(a7)
	jsr	L598(pc)
	addq.w	#8,a7
L134	tst.l	-$78ce(a4)
	beq.s	L135
	move.l	-$78ce(a4),-(a7)
	jsr	L590(pc)
	addq.w	#4,a7
L135	tst.l	-$78d2(a4)
	beq.s	L136
	dc.b	$48,$78,$64,0
				;	pea	($6400).w
	move.l	-$78d2(a4),-(a7)
	jsr	L607(pc)
	addq.w	#8,a7
L136	tst.l	-$7916(a4)
	beq.s	L137
	pea	-$771a(a4)
	jsr	L581(pc)
	addq.w	#4,a7
L137	tst.l	-$7926(a4)
	beq.s	L138
	move.l	-$7926(a4),-(a7)
	jsr	L632(pc)
	addq.w	#4,a7
	move.l	-$7926(a4),-(a7)
	jsr	L634(pc)
	addq.w	#4,a7
L138	tst.l	-$792a(a4)
	beq.s	L139
	move.l	-$792a(a4),-(a7)
	jsr	L633(pc)
	addq.w	#4,a7
L139	tst.l	-$7922(a4)
	beq.s	L140
	move.l	-$7922(a4),-(a7)
	jsr	L621(pc)
	addq.w	#4,a7
L140	tst.l	-$792e(a4)
	beq.s	L141
	move.l	-$792e(a4),-(a7)
	jsr	L582(pc)
	addq.w	#4,a7
L141	tst.l	-$7932(a4)
	beq.s	L142
	move.l	-$7932(a4),-(a7)
	jsr	L582(pc)
	addq.w	#4,a7
L142	tst.l	-$7936(a4)
	beq.s	L143
	move.l	-$7936(a4),-(a7)
	jsr	L582(pc)
	addq.w	#4,a7
L143	bra.l	L32

L144	dc.b	'Usage: sec <device>'
	dc.b	$0a,0
L145	dc.b	'Can''t find device ',0
L146	dc.b	$0a,0
L147	dc.b	'intuition.library',0
L148	dc.b	'graphics.library',0
L149	dc.b	'diskfont.library',0
L150	dc.b	'Couldn''t open Diskfo'
	dc.b	'nt.library'
	dc.b	$0a,0
L151	dc.b	'Couldn''t find topaz/'
	dc.b	'11 font.'
	dc.b	$0a,0
L152	dc.b	'Device=',0
L153	dc.b	'Unit=',0
L154	dc.b	'Driver=',0
L155	dc.b	'console.device',0
L156	dc.b	'Couldn''t open the co'
	dc.b	'nsole.'
	dc.b	$0a,0
L157	dc.b	'Couldn''t create disk'
	dc.b	' port'
	dc.b	$0a,0
L158	dc.b	'Couldn''t create disk'
	dc.b	' request'
	dc.b	$0a,0
L159	dc.b	'Couldn''t open device'
	dc.b	', Error = ',0
L160	dc.b	$0a,0
	dc.b	'Name of Save File',0
	dc.b	'Enter Search Pattern',0
	dc.b	'Enter File Name',0

L161	link	a5,#$0000
	move.l	$0008(a5),-$789e(a4)
	move.l	$0010(a5),-$789a(a4)
	move.l	$0014(a5),-$7896(a4)
	move.l	$000c(a5),-$7892(a4)
	move.l	$000c(a5),-(a7)
	movea.l	$0008(a5),a0
	move.l	(a0),-(a7)
	pea	-$7864(a4)
	jsr	L445(pc)
	lea	$000c(a7),a7
	move.l	$0018(a5),d0
	sub.l	$0010(a5),d0
	moveq	#8,d1
	jsr	L533(pc)
	move.w	d0,-$793a(a4)
	move.w	#$0003,-$793c(a4)
	bsr.s	L162
	unlk	a5
	rts	

L162	link	a5,#$0000
	tst.w	-$793a(a4)
	bge.s	L163
	clr.w	-$793a(a4)
L163	move.w	-$793a(a4),d0
	ext.l	d0
	cmp.l	-$7892(a4),d0
	bls.s	L164
	move.w	-$7890(a4),-$793a(a4)
L164	move.l	-$7892(a4),d0
	lea	-$7864(a4),a0
	move.b	#$20,0(a0,d0.l)
	move.l	-$7896(a4),-(a7)
	move.l	-$789a(a4),-(a7)
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	move.l	-$7892(a4),d0
	addq.l	#1,d0
	move.l	d0,-(a7)
	pea	-$7864(a4)
	move.l	-$791e(a4),-(a7)
	jsr	L628(pc)
	lea	$000c(a7),a7
	tst.w	-$793c(a4)
	beq.s	L165
	dc.b	$48,$78,0,3
				;	pea	($0003).w
	move.l	-$791e(a4),-(a7)
	jsr	L625(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$02
				;	pea	($0002).w
	move.l	-$791e(a4),-(a7)
	jsr	L626(pc)
	addq.w	#8,a7
	move.l	-$7896(a4),-(a7)
	move.w	-$793a(a4),d0
	ext.l	d0
	asl.l	#3,d0
	add.l	-$789a(a4),d0
	move.l	d0,-(a7)
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$01
				;	pea	($0001).w
	move.w	-$793a(a4),d0
	ext.l	d0
	lea	-$7864(a4),a0
	add.l	a0,d0
	move.l	d0,-(a7)
	move.l	-$791e(a4),-(a7)
	jsr	L628(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$01
				;	pea	($0001).w
	move.l	-$791e(a4),-(a7)
	jsr	L625(pc)
	addq.w	#8,a7
	clr.l	-(a7)
	move.l	-$791e(a4),-(a7)
	jsr	L626(pc)
	addq.w	#8,a7
L165	unlk	a5
	rts	

L166	link	a5,#-$0006
	cmpi.b	#$30,$000b(a5)
	bcs.s	L167
	cmpi.b	#$39,$000b(a5)
	bhi.s	L167
	move.w	-$793a(a4),d0
	lea	-$7864(a4),a0
	move.b	$000b(a5),0(a0,d0.w)
	addq.w	#1,-$793a(a4)
	bra.l	L180

L167	cmpi.b	#$61,$000b(a5)
	bcs.s	L168
	cmpi.b	#$66,$000b(a5)
	bhi.s	L168
	cmpi.w	#$0004,-$793c(a4)
	bne.s	L168
	moveq	#0,d0
	move.b	$000b(a5),d0
	sub.l	#$00000020,d0
	move.w	-$793a(a4),d1
	lea	-$7864(a4),a0
	move.b	d0,0(a0,d1.w)
	addq.w	#1,-$793a(a4)
	bra.l	L180

L168	cmpi.b	#$41,$000b(a5)
	bcs.s	L169
	cmpi.b	#$46,$000b(a5)
	bhi.s	L169
	cmpi.w	#$0004,-$793c(a4)
	bne.s	L169
	move.w	-$793a(a4),d0
	lea	-$7864(a4),a0
	move.b	$000b(a5),0(a0,d0.w)
	addq.w	#1,-$793a(a4)
	bra.l	L180

L169	cmpi.b	#$0d,$000b(a5)
	beq.s	L170
	cmpi.b	#$0a,$000b(a5)
	bne.s	L174
L170	clr.l	-$0006(a5)
	cmpi.w	#$0003,-$793c(a4)
	bne.s	L173
	clr.w	-$0002(a5)
	bra.s	L172

L171	moveq	#$0a,d1
	move.l	-$0006(a5),d0
	jsr	L569(pc)
	move.l	d0,-$0006(a5)
	move.w	-$0002(a5),d0
	lea	-$7864(a4),a0
	moveq	#0,d1
	move.b	0(a0,d0.w),d1
	sub.l	#$00000030,d1
	add.l	d1,-$0006(a5)
	addq.w	#1,-$0002(a5)
L172	move.w	-$0002(a5),d0
	ext.l	d0
	cmp.l	-$7892(a4),d0
	bcs.s	L171
L173	movea.l	-$789e(a4),a0
	move.l	-$0006(a5),(a0)
	clr.w	-$793c(a4)
	jsr	L205(pc)
	bra.s	L180

L174	cmpi.b	#2,$000b(a5)
	beq.s	L175
	cmpi.b	#$20,$000b(a5)
	bne.s	L176
L175	addq.w	#1,-$793a(a4)
	bra.s	L180

L176	cmpi.b	#3,$000b(a5)
	beq.s	L177
	cmpi.b	#8,$000b(a5)
	bne.s	L178
L177	subq.w	#1,-$793a(a4)
	bra.s	L180

L178	tst.b	$000b(a5)
	bne.s	L179
	move.w	-$793a(a4),d0
	ext.l	d0
	move.l	d0,-(a7)
	dc.b	$48,$78,0,$01
				;	pea	($0001).w
	bsr.s	L181
	addq.w	#8,a7
	bra.s	L180

L179	cmpi.b	#1,$000b(a5)
	bne.s	L180
	move.w	-$793a(a4),d0
	ext.l	d0
	move.l	d0,-(a7)
	dc.b	$48,$78,$ff,$ff
				;	pea	($ffffffff).w
	bsr.s	L181
	addq.w	#8,a7
L180	jsr	L162(pc)
	unlk	a5
	rts	

L181	link	a5,#$0000
L182	tst.w	$000e(a5)
	blt.s	L186
	move.w	$000e(a5),d0
	lea	-$7864(a4),a0
	adda.w	d0,a0
	move.b	$000b(a5),d1
	add.b	d1,(a0)
	move.w	$000e(a5),d0
	lea	-$7864(a4),a0
	cmpi.b	#$39,0(a0,d0.w)
	bls.s	L183
	move.w	$000e(a5),d0
	lea	-$7864(a4),a0
	move.b	#$30,0(a0,d0.w)
	subq.w	#1,$000e(a5)
	bra.s	L185

L183	move.w	$000e(a5),d0
	lea	-$7864(a4),a0
	cmpi.b	#$30,0(a0,d0.w)
	bcc.s	L184
	move.w	$000e(a5),d0
	lea	-$7864(a4),a0
	move.b	#$39,0(a0,d0.w)
	subq.w	#1,$000e(a5)
	bra.s	L185

L184	bra.s	L186

L185	bra.s	L182

L186	unlk	a5
	rts	

L187	link	a5,#$0000
	move.l	-$7902(a4),-(a7)
	move.l	-$7848(a4),d0
	add.l	-$78e6(a4),d0
	move.l	d0,-(a7)
	jsr	L356(pc)
	addq.w	#8,a7
	unlk	a5
	rts	

L188	link	a5,#$0000
	move.l	$0008(a5),-(a7)
	jsr	L194(pc)
	addq.w	#4,a7
	move.l	-$78d2(a4),-$7902(a4)
	move.l	-$7902(a4),-(a7)
	move.l	$0008(a5),d0
	add.l	-$78e6(a4),d0
	move.l	d0,-(a7)
	jsr	L355(pc)
	addq.w	#8,a7
	move.l	-$7902(a4),-$7850(a4)
	move.w	#$0080,-$784c(a4)
	move.l	$0008(a5),-$7848(a4)
	clr.w	-$784a(a4)
	clr.b	-$7844(a4)
	jsr	L233(pc)
	unlk	a5
	rts	

L189	link	a5,#$0000
	move.l	-$78d2(a4),-$7902(a4)
	move.l	-$78d6(a4),-(a7)
	bsr.s	L188
	addq.w	#4,a7
	unlk	a5
	rts	

L190	link	a5,#-$0002
	dc.b	$48,$78,1,$8d
				;	pea	($018d).w
	dc.b	$48,$78,0,$08
				;	pea	($0008).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	-$76b6(a4)
	jsr	L454(pc)
	addq.w	#4,a7
	pea	L193(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	clr.w	-$0002(a5)
	bra.s	L192

L191	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.w	-$0002(a5),d0
	ext.l	d0
	asl.l	#2,d0
	lea	-$7746(a4),a0
	move.l	0(a0,d0.l),-(a7)
	jsr	L473(pc)
	addq.w	#8,a7
	jsr	L463(pc)
	addq.w	#1,-$0002(a5)
L192	cmpi.w	#$0008,-$0002(a5)
	blt.s	L191
	dc.b	$48,$78,1,$8d
				;	pea	($018d).w
	dc.b	$48,$78,0,$08
				;	pea	($0008).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$41
				;	pea	($0041).w
	jsr	L455(pc)
	addq.w	#4,a7
	unlk	a5
	rts	

L193	dc.b	'History: ',0

L194	link	a5,#-$0002
	move.w	#$000a,-$0002(a5)
	bra.s	L196

L195	move.w	-$0002(a5),d0
	ext.l	d0
	subq.l	#1,d0
	asl.l	#2,d0
	lea	-$7746(a4),a0
	move.w	-$0002(a5),d1
	ext.l	d1
	asl.l	#2,d1
	lea	-$7746(a4),a1
	move.l	0(a0,d0.l),0(a1,d1.l)
	subq.w	#1,-$0002(a5)
L196	tst.w	-$0002(a5)
	bgt.s	L195
	move.l	$0008(a5),-$7746(a4)
	jsr	L190(pc)
	unlk	a5
	rts	

L197	link	a5,#-$0006
	lea	-$7a5a(a4),a0
	move.l	a0,-$0006(a5)
	clr.w	-$0002(a5)
	bra.l	L204

L198	movea.l	-$0006(a5),a0
	move.w	-$7948(a4),d0
	cmp.w	(a0),d0
	blt.s	L199
	move.w	-$7948(a4),d1
	ext.l	d1
	movea.l	-$0006(a5),a1
	move.w	$0004(a1),d2
	ext.l	d2
	asl.l	#3,d2
	movea.l	-$0006(a5),a6
	move.w	(a6),d3
	ext.l	d3
	add.l	d3,d2
	cmp.l	d2,d1
	bge.s	L199
	move.w	-$7946(a4),d3
	ext.l	d3
	movea.l	-$0006(a5),a6
	move.w	$0002(a6),d2
	ext.l	d2
	subq.l	#8,d2
	cmp.l	d2,d3
	blt.s	L199
	move.w	-$7946(a4),d3
	ext.l	d3
	movea.l	-$0006(a5),a6
	move.w	$0002(a6),d2
	ext.l	d2
	addq.l	#3,d2
	cmp.l	d2,d3
	blt.s	L200
L199	addi.l	#$0000000c,-$0006(a5)
	bra.s	L203

L200	move.w	-$0002(a5),-$7938(a4)
	cmpi.w	#$0002,-$793c(a4)
	ble.s	L201
	dc.b	$48,$78,0,$0d
				;	pea	($000d).w
	jsr	L166(pc)
	addq.w	#4,a7
L201	move.w	-$7948(a4),d0
	ext.l	d0
	move.l	d0,-(a7)
	movea.l	-$0006(a5),a0
	move.w	$0002(a0),d1
	ext.l	d1
	move.l	d1,-(a7)
	movea.l	-$0006(a5),a1
	move.w	(a1),d2
	ext.l	d2
	move.l	d2,-(a7)
	movea.l	-$0006(a5),a6
	move.w	$0004(a6),d3
	ext.l	d3
	move.l	d3,-(a7)
	movea.l	-$0006(a5),a6
	move.l	$0008(a6),-(a7)
	jsr	L161(pc)
	lea	$0014(a7),a7
	moveq	#1,d0
L202	unlk	a5
	rts	

L203	addq.w	#1,-$0002(a5)
L204	cmpi.w	#$0006,-$0002(a5)
	blt.l	L198
	moveq	#0,d0
	bra.s	L202

L205	link	a5,#$0000
	cmpi.w	#$0006,-$7938(a4)
	bge.s	L208
	cmpi.w	#$0003,-$7938(a4)
	bne.s	L206
	movea.l	-$78ea(a4),a0
	move.l	$000c(a0),d1
	move.l	-$788a(a4),d0
	jsr	L541(pc)
	move.l	d0,-$7882(a4)
	movea.l	-$78ea(a4),a0
	move.l	$000c(a0),d1
	move.l	-$788a(a4),d0
	jsr	L542(pc)
	move.l	d0,-$7886(a4)
L206	tst.w	-$7938(a4)
	ble.s	L207
	cmpi.w	#$0005,-$7938(a4)
	bge.s	L207
	bsr.s	L210
L207	move.l	-$787e(a4),-(a7)
	jsr	L188(pc)
	addq.w	#4,a7
L208	unlk	a5
	rts	

L209	link	a5,#$0000
	move.l	$0008(a5),-$787e(a4)
	movea.l	-$78ea(a4),a0
	move.l	$0014(a0),d1
	move.l	$0008(a5),d0
	jsr	L541(pc)
	move.l	d0,-$788e(a4)
	movea.l	-$78ea(a4),a0
	move.l	$0014(a0),d1
	move.l	$0008(a5),d0
	jsr	L542(pc)
	move.l	d0,-$788a(a4)
	movea.l	-$78ea(a4),a0
	move.l	$000c(a0),d1
	move.l	-$788a(a4),d0
	jsr	L541(pc)
	move.l	d0,-$7882(a4)
	movea.l	-$78ea(a4),a0
	move.l	$000c(a0),d1
	move.l	-$788a(a4),d0
	jsr	L542(pc)
	move.l	d0,-$7886(a4)
	unlk	a5
	rts	

L210	link	a5,#$0000
	movea.l	-$78ea(a4),a0
	move.l	$000c(a0),d0
	move.l	-$7886(a4),d1
	jsr	L569(pc)
	add.l	-$7882(a4),d0
	movea.l	-$78ea(a4),a1
	move.l	$0014(a1),d1
	jsr	L569(pc)
	add.l	-$788e(a4),d0
	move.l	d0,-$787e(a4)
	unlk	a5
	rts	

L211	link	a5,#$0000
	move.l	$0008(a5),-(a7)
	jsr	L209(pc)
	addq.w	#4,a7
	dc.b	$48,$78,1,$29
				;	pea	($0129).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L212(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	-$788e(a4),-(a7)
	jsr	L447(pc)
	addq.w	#8,a7
	pea	L213(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	movea.l	-$78ea(a4),a0
	move.l	$0014(a0),-(a7)
	jsr	L447(pc)
	addq.w	#8,a7
	dc.b	$48,$78,1,$34
				;	pea	($0134).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L214(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	-$788a(a4),-(a7)
	jsr	L447(pc)
	addq.w	#8,a7
	pea	L215(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	movea.l	-$78ea(a4),a0
	move.l	$0028(a0),d0
	addq.l	#1,d0
	movea.l	-$78ea(a4),a1
	move.l	$000c(a1),d1
	jsr	L569(pc)
	move.l	d0,-(a7)
	jsr	L447(pc)
	addq.w	#8,a7
	dc.b	$48,$78,1,$3f
				;	pea	($013f).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L216(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	-$7886(a4),-(a7)
	jsr	L447(pc)
	addq.w	#8,a7
	pea	L217(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	movea.l	-$78ea(a4),a0
	move.l	$0028(a0),-(a7)
	jsr	L447(pc)
	addq.w	#8,a7
	dc.b	$48,$78,1,$4a
				;	pea	($014a).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L218(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	-$7882(a4),-(a7)
	jsr	L447(pc)
	addq.w	#8,a7
	pea	L219(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	movea.l	-$78ea(a4),a0
	move.l	$000c(a0),-(a7)
	jsr	L447(pc)
	addq.w	#8,a7
	dc.b	$48,$78,1,$55
				;	pea	($0155).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L220(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	$0008(a5),-(a7)
	jsr	L447(pc)
	addq.w	#8,a7
	pea	L221(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	-$78de(a4),-(a7)
	jsr	L447(pc)
	addq.w	#8,a7
	unlk	a5
	rts	

L212	dc.b	$20,$20,$53,$65,$63,$74
	dc.b	$6f,$72,$3a,0
L213	dc.b	$20,0
L214	dc.b	$20,$20,$20,$54,$72,$61
	dc.b	$63,$6b,$3a,0
L215	dc.b	$20,0
L216	dc.b	$43,$79,$6c,$69,$6e,$64
	dc.b	$65,$72,$3a,0
L217	dc.b	$20,0
L218	dc.b	$20,$53,$75,$72,$66,$61
	dc.b	$63,$65,$3a,0
L219	dc.b	$20,0
L220	dc.b	$20,$20,$20,$42,$6c,$6f
	dc.b	$63,$6b,$3a,0
L221	dc.b	$20,0

L222	link	a5,#-$0004
	movem.l	d4-d7/a2,-(a7)
	dc.b	$48,$78,0,$01
				;	pea	($0001).w
	move.l	-$791e(a4),-(a7)
	jsr	L625(pc)
	addq.w	#8,a7
	movea.l	$0008(a5),a0
	move.l	(a0),-$0004(a5)
	move.l	-$0004(a5),-$78f2(a4)
	moveq	#$28,d6
	moveq	#0,d4
L223	pea	-$76b6(a4)
	jsr	L454(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$02
				;	pea	($0002).w
	move.l	d4,-(a7)
	jsr	L473(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$3a
				;	pea	($003a).w
	jsr	L464(pc)
	addq.w	#4,a7
	moveq	#0,d5
L224	move.l	d4,d0
	add.l	d5,d0
	movea.l	$0008(a5),a0
	move.w	$0004(a0),d1
	ext.l	d1
	cmp.l	d1,d0
	ble.s	L225
	dc.b	$48,$78,0,$09
				;	pea	($0009).w
	pea	L232(pc)
	jsr	L459(pc)
	addq.w	#8,a7
	bra.s	L226

L225	dc.b	$48,$78,0,$08
				;	pea	($0008).w
	move.l	d4,d0
	add.l	d5,d0
	asl.l	#2,d0
	movea.l	-$0004(a5),a0
	move.l	0(a0,d0.l),-(a7)
	jsr	L473(pc)
	addq.w	#8,a7
	jsr	L463(pc)
L226	addq.l	#1,d5
	cmp.l	#$00000004,d5
	blt.s	L224
	move.l	d4,d0
	asl.l	#2,d0
	movea.l	d0,a2
	adda.l	-$0004(a5),a2
	moveq	#0,d5
L227	movea.l	a2,a0
	addq.l	#1,a2
	move.b	(a0),d7
	movea.l	$0008(a5),a0
	tst.b	$000c(a0)
	bne.s	L228
	moveq	#$04,d1
	move.l	d5,d0
	jsr	L533(pc)
	add.l	d4,d0
	movea.l	$0008(a5),a1
	move.w	$0004(a1),d2
	ext.l	d2
	cmp.l	d2,d0
	ble.s	L229
L228	moveq	#$20,d7
	bra.s	L231

L229	cmp.b	#$20,d7
	bcs.s	L230
	cmp.b	#$7e,d7
	bls.s	L231
L230	moveq	#$2e,d7
L231	moveq	#0,d0
	move.b	d7,d0
	move.l	d0,-(a7)
	jsr	L464(pc)
	addq.w	#4,a7
	addq.l	#1,d5
	cmp.l	#$00000010,d5
	blt.s	L227
	move.l	d6,-(a7)
	dc.b	$48,$78,0,$08
				;	pea	($0008).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$37
				;	pea	($0037).w
	jsr	L455(pc)
	addq.w	#4,a7
	add.l	#$0000000b,d6
	addq.l	#4,d4
	cmp.l	#$00000080,d4
	blt.l	L223
	movem.l	(a7)+,d4-d7/a2
	unlk	a5
	rts	

L232	dcb.b	9,$20
	dc.b	0

L233	link	a5,#-$0004
	movem.l	d4-d6/a2-a3,-(a7)
	movea.l	-$7850(a4),a3
	move.w	-$784a(a4),-$7942(a4)
	dc.b	$48,$78,0,$01
				;	pea	($0001).w
	move.l	-$791e(a4),-(a7)
	jsr	L625(pc)
	addq.w	#8,a7
	pea	-$76b6(a4)
	jsr	L454(pc)
	addq.w	#4,a7
	pea	L261(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	-$7848(a4),-(a7)
	jsr	L473(pc)
	addq.w	#8,a7
	pea	L262(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	move.l	(a3),d0
	bclr	#0,d0
	move.l	d0,-$0004(a5)
	btst	#0,$0003(a3)
	beq.s	L234
	pea	L263(pc)
	jsr	L456(pc)
	addq.w	#4,a7
L234	move.l	-$0004(a5),d0
	bra.l	L244

L235	cmpi.l	#$00000001,$01fc(a3)
	bne.s	L236
	dc.b	$48,$78,0,$0c
				;	pea	($000c).w
	pea	L264(pc)
	jsr	L459(pc)
	addq.w	#8,a7
	bra.s	L239

L236	cmpi.l	#$00000002,$01fc(a3)
	bne.s	L237
	dc.b	$48,$78,0,$0c
				;	pea	($000c).w
	pea	L265(pc)
	jsr	L459(pc)
	addq.w	#8,a7
	bra.s	L239

L237	cmpi.l	#-$00000003,$01fc(a3)
	bne.s	L238
	dc.b	$48,$78,0,$0c
				;	pea	($000c).w
	pea	L266(pc)
	jsr	L459(pc)
	addq.w	#8,a7
	bra.s	L239

L238	dc.b	$48,$78,0,$0c
				;	pea	($000c).w
	pea	L267(pc)
	jsr	L459(pc)
	addq.w	#8,a7
L239	bra.s	L245

L240	dc.b	$48,$78,0,$0c
				;	pea	($000c).w
	pea	L268(pc)
	jsr	L459(pc)
	addq.w	#8,a7
	bra.s	L245

L241	dc.b	$48,$78,0,$0c
				;	pea	($000c).w
	pea	L269(pc)
	jsr	L459(pc)
	addq.w	#8,a7
	bra.s	L245

L242	dc.b	$48,$78,0,$0c
				;	pea	($000c).w
	pea	L270(pc)
	jsr	L459(pc)
	addq.w	#8,a7
	bra.s	L245

L243	pea	L271(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,4
				;	pea	($0004).w
	move.l	(a3),-(a7)
	jsr	L465(pc)
	addq.w	#8,a7
	bra.s	L245

L244	subq.l	#2,d0
	beq.l	L235
	subq.l	#2,d0
	beq.s	L240
	subq.l	#4,d0
	beq.s	L241
	subq.l	#8,d0
	beq.s	L242
	bra.s	L243

L245	cmpi.l	#$00000002,-$0004(a5)
	bne.s	L247
	pea	L272(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	movea.l	a3,a2
	adda.l	#$000001b0,a2
	movea.l	a2,a0
	addq.l	#1,a2
	moveq	#0,d0
	move.b	(a0),d0
	move.l	d0,d4
	cmp.l	#$00000020,d4
	ble.s	L246
	moveq	#$20,d4
L246	move.l	d4,-(a7)
	move.l	a2,-(a7)
	jsr	L459(pc)
	addq.w	#8,a7
L247	dc.b	$48,$78,0,$18
				;	pea	($0018).w
	dc.b	$48,$78,0,$08
				;	pea	($0008).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$4f
				;	pea	($004f).w
	jsr	L455(pc)
	addq.w	#4,a7
	pea	-$7850(a4)
	jsr	L222(pc)
	addq.w	#4,a7
	pea	-$76b6(a4)
	jsr	L454(pc)
	addq.w	#4,a7
	cmpi.l	#$00000002,-$0004(a5)
	bne.s	L248
	pea	L273(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	pea	$01a4(a3)
	jsr	L401(pc)
	addq.w	#4,a7
L248	dc.b	$48,$78,0,$e0
				;	pea	($00e0).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$0f
				;	pea	($000f).w
	jsr	L455(pc)
	addq.w	#4,a7
	pea	-$76b6(a4)
	jsr	L454(pc)
	addq.w	#4,a7
	cmpi.l	#$00000002,-$0004(a5)
	bne.s	L249
	pea	L274(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	pea	$01a4(a3)
	jsr	L410(pc)
	addq.w	#4,a7
L249	dc.b	$48,$78,0,$eb
				;	pea	($00eb).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$0f
				;	pea	($000f).w
	jsr	L455(pc)
	addq.w	#4,a7
	pea	-$76b6(a4)
	jsr	L454(pc)
	addq.w	#4,a7
	cmpi.l	#$00000002,-$0004(a5)
	bne.s	L250
	pea	L275(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	$0008(a3),-(a7)
	jsr	L465(pc)
	addq.w	#8,a7
	bra.s	L252

L250	cmpi.l	#$00000008,-$0004(a5)
	bne.s	L251
	pea	L276(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	$0008(a3),-(a7)
	jsr	L465(pc)
	addq.w	#8,a7
	bra.s	L252

L251	cmpi.l	#$00000010,-$0004(a5)
	bne.s	L252
	pea	L277(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	$0008(a3),-(a7)
	jsr	L465(pc)
	addq.w	#8,a7
L252	dc.b	$48,$78,0,$f6
				;	pea	($00f6).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$15
				;	pea	($0015).w
	jsr	L455(pc)
	addq.w	#4,a7
	pea	-$76b6(a4)
	jsr	L454(pc)
	addq.w	#4,a7
	cmpi.l	#$00000002,-$0004(a5)
	beq.s	L253
	cmpi.l	#$00000008,-$0004(a5)
	beq.s	L253
	cmpi.l	#$00000010,-$0004(a5)
	bne.s	L254
L253	pea	L278(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	$000c(a3),-(a7)
	jsr	L465(pc)
	addq.w	#8,a7
L254	dc.b	$48,$78,1,$01
				;	pea	($0101).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$11
				;	pea	($0011).w
	jsr	L455(pc)
	addq.w	#4,a7
	pea	-$76b6(a4)
	jsr	L454(pc)
	addq.w	#4,a7
	cmpi.l	#$00000002,-$0004(a5)
	bne.s	L258
	pea	L279(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	btst	#3,$0143(a3)
	beq.s	L255
	dc.b	$48,$78,0,$52
				;	pea	($0052).w
	jsr	L464(pc)
	addq.w	#4,a7
L255	btst	#2,$0143(a3)
	beq.s	L256
	dc.b	$48,$78,0,$57
				;	pea	($0057).w
	jsr	L464(pc)
	addq.w	#4,a7
L256	btst	#1,$0143(a3)
	beq.s	L257
	dc.b	$48,$78,0,$58
				;	pea	($0058).w
	jsr	L464(pc)
	addq.w	#4,a7
L257	btst	#0,$0143(a3)
	beq.s	L258
	dc.b	$48,$78,0,$44
				;	pea	($0044).w
	jsr	L464(pc)
	addq.w	#4,a7
L258	dc.b	$48,$78,1,$0c
				;	pea	($010c).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$0f
				;	pea	($000f).w
	jsr	L455(pc)
	addq.w	#4,a7
	pea	-$76b6(a4)
	jsr	L454(pc)
	addq.w	#4,a7
	cmpi.l	#$00000002,-$0004(a5)
	bne.s	L259
	pea	L280(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	$0144(a3),-(a7)
	jsr	L465(pc)
	addq.w	#8,a7
L259	dc.b	$48,$78,1,$17
				;	pea	($0117).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$0f
				;	pea	($000f).w
	jsr	L455(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$28
				;	pea	($0028).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L281(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$33
				;	pea	($0033).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L282(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	-$78d6(a4),-(a7)
	jsr	L440(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$3e
				;	pea	($003e).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L283(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	$01f4(a3),-(a7)
	jsr	L440(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$49
				;	pea	($0049).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L284(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	$01f0(a3),-(a7)
	jsr	L440(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$54
				;	pea	($0054).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L285(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	$01f8(a3),-(a7)
	jsr	L440(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$5f
				;	pea	($005f).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L286(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	$0004(a3),-(a7)
	jsr	L440(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$6a
				;	pea	($006a).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L287(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	$0010(a3),-(a7)
	jsr	L440(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$75
				;	pea	($0075).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L288(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$80
				;	pea	($0080).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L289(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$8b
				;	pea	($008b).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L290(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$96
				;	pea	($0096).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L291(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$08
				;	pea	($0008).w
	move.l	$0014(a3),-(a7)
	jsr	L440(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$01
				;	pea	($0001).w
	pea	L292(pc)
	move.l	-$791e(a4),-(a7)
	jsr	L628(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$a1
				;	pea	($00a1).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L293(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$ac
				;	pea	($00ac).w
	dc.b	$48,$78,1,$c8
				;	pea	($01c8).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	L294(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	move.l	-$7848(a4),-(a7)
	jsr	L211(pc)
	addq.w	#4,a7
	tst.w	-$784c(a4)
	beq.s	L260
	jsr	L298(pc)
L260	movem.l	(a7)+,d4-d6/a2-a3
	unlk	a5
	rts	

L261	dc.b	'Block=',0
L262	dc.b	' Type=',0
L263	dc.b	'DELETED ',0
L264	dc.b	'ROOT DIR',0
L265	dc.b	'DIRECTORY',0
L266	dc.b	'FILE',0
L267	dc.b	'(UNSET)',0
L268	dc.b	'LONG FILE',0
L269	dc.b	'DATA BLOCK',0
L270	dc.b	'FILE LIST',0
L271	dc.b	'CORRUPT/',0
L272	dc.b	' Name=',0
L273	dc.b	'Date: ',0
L274	dc.b	'Time: ',0
L275	dc.b	'Highest Seq #: ',0
L276	dc.b	'Seq #: ',0
L277	dc.b	'Block Count: ',0
L278	dc.b	'Data Size: ',0
L279	dc.b	'Protect: ',0
L280	dc.b	'File Bytes: ',0
L281	dc.b	'J-Jump',0
L282	dc.b	'R-Go Root	=',0
L283	dc.b	'P-Go Parent	=',0
L284	dc.b	'C-Go Hash Chain=',0
L285	dc.b	'X-Go Extension =',0
L286	dc.b	'H-Go Header	=',0
L287	dc.b	'D-Go Next Data =',0
L288	dc.b	'L-Last Search',0
L289	dc.b	'E-Edit',0
L290	dc.b	'A-Edir ASCII',0
L291	dc.b	'K-Checksum [=',0
L292	dc.b	']',0
L293	dc.b	'U-Update block',0
L294	dc.b	'W-Write to file',0
	dc.b	0

L295	link	a5,#$0000
	dc.b	$48,$78,0,$01
				;	pea	($0001).w
	move.l	-$791e(a4),-(a7)
	jsr	L625(pc)
	addq.w	#8,a7
	pea	-$76b6(a4)
	jsr	L454(pc)
	addq.w	#4,a7
	pea	L296(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	pea	L297(pc)
	jsr	L456(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,3
				;	pea	($0003).w
	move.w	-$783e(a4),d0
	ext.l	d0
	move.l	d0,-(a7)
	jsr	L465(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$18
				;	pea	($0018).w
	dc.b	$48,$78,0,$08
				;	pea	($0008).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$4f
				;	pea	($004f).w
	jsr	L455(pc)
	addq.w	#4,a7
	pea	-$7842(a4)
	jsr	L222(pc)
	addq.w	#4,a7
	bsr.s	L298
	unlk	a5
	rts	

L296	dc.b	'Search Results',0
L297	dc.b	'	Blocks Found: ',0

L298	link	a5,#$0000
	dc.b	$48,$78,0,3
				;	pea	($0003).w
	move.l	-$791e(a4),-(a7)
	jsr	L625(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$02
				;	pea	($0002).w
	move.l	-$791e(a4),-(a7)
	jsr	L626(pc)
	addq.w	#8,a7
	bsr.s	L299
	dc.b	$48,$78,0,$01
				;	pea	($0001).w
	move.l	-$791e(a4),-(a7)
	jsr	L625(pc)
	addq.w	#8,a7
	clr.l	-(a7)
	move.l	-$791e(a4),-(a7)
	jsr	L626(pc)
	addq.w	#8,a7
	unlk	a5
	rts	

L299	link	a5,#-$001a
	move.l	-$78f2(a4),-$0012(a5)
	cmpi.w	#$0001,-$793c(a4)
	bne.s	L300
	andi.w	#$01ff,-$7940(a4)
	move.w	-$7940(a4),-$0008(a5)
	move.w	-$7940(a4),d0
	ext.l	d0
	divs	#$0004,d0
	move.w	d0,-$0006(a5)
	move.w	-$0006(a5),d0
	ext.l	d0
	and.l	#$00000003,d0
	moveq	#$48,d1
	jsr	L569(pc)
	move.w	-$7940(a4),d2
	ext.l	d2
	and.l	#$00000003,d2
	asl.l	#4,d2
	add.l	d2,d0
	move.w	d0,-$0002(a5)
	move.w	#$0002,-$000a(a5)
	move.w	-$7940(a4),d0
	movea.l	-$0012(a5),a0
	moveq	#0,d1
	move.b	0(a0,d0.w),d1
	move.l	d1,-$000e(a5)
	bra.l	L303

L300	cmpi.w	#$0002,-$793c(a4)
	bne.s	L302
	andi.w	#$03ff,-$793e(a4)
	move.w	-$793e(a4),d0
	ext.l	d0
	divs	#$0002,d0
	move.w	d0,-$0008(a5)
	move.w	-$793e(a4),d0
	ext.l	d0
	divs	#$0008,d0
	move.w	d0,-$0006(a5)
	move.w	-$0006(a5),d0
	ext.l	d0
	and.l	#$00000003,d0
	moveq	#$48,d1
	jsr	L569(pc)
	move.w	-$793e(a4),d2
	ext.l	d2
	and.l	#$00000007,d2
	asl.l	#3,d2
	add.l	d2,d0
	move.w	d0,-$0002(a5)
	move.w	-$793e(a4),d0
	ext.l	d0
	moveq	#2,d1
	jsr	L533(pc)
	movea.l	-$0012(a5),a0
	moveq	#0,d2
	move.b	0(a0,d0.l),d2
	move.l	d2,-$000e(a5)
	btst	#0,-$793d(a4)
	bne.s	L301
	move.l	-$000e(a5),d0
	asr.l	#4,d0
	move.l	d0,-$000e(a5)
L301	move.w	#$0001,-$000a(a5)
	bra.s	L303

L302	andi.w	#$007f,-$7942(a4)
	move.w	-$7942(a4),d0
	ext.l	d0
	asl.l	#2,d0
	move.w	d0,-$0008(a5)
	move.w	-$7942(a4),-$0006(a5)
	move.w	-$0006(a5),d0
	ext.l	d0
	and.l	#$00000003,d0
	moveq	#$48,d1
	jsr	L569(pc)
	move.w	d0,-$0002(a5)
	move.w	#$0008,-$000a(a5)
	move.w	-$7942(a4),d0
	ext.l	d0
	asl.l	#2,d0
	movea.l	-$78f2(a4),a0
	move.l	0(a0,d0.l),-$000e(a5)
L303	move.w	-$0006(a5),d0
	ext.l	d0
	moveq	#$04,d1
	jsr	L533(pc)
	moveq	#$0b,d1
	jsr	L569(pc)
	move.w	d0,-$0004(a5)
	move.w	-$0004(a5),d0
	ext.l	d0
	movea.l	d0,a0
	pea	$0028(a0)
	move.w	-$0002(a5),d1
	ext.l	d1
	movea.l	d1,a1
	pea	$0020(a1)
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	move.w	-$000a(a5),d0
	ext.l	d0
	move.l	d0,-(a7)
	move.l	-$000e(a5),-(a7)
	jsr	L440(pc)
	addq.w	#8,a7
	asr	-$000a(a5)
	tst.w	-$000a(a5)
	beq.l	L308
	move.w	-$0008(a5),d0
	ext.l	d0
	and.l	#$0000000f,d0
	asl.l	#3,d0
	add.l	#$00000140,d0
	move.w	d0,-$0002(a5)
	clr.w	-$0014(a5)
	bra.s	L307

L304	move.w	-$0008(a5),d0
	addq.w	#1,-$0008(a5)
	movea.l	-$0012(a5),a0
	move.b	0(a0,d0.w),-$0015(a5)
	cmpi.b	#$20,-$0015(a5)
	bcs.s	L305
	cmpi.b	#$7e,-$0015(a5)
	bls.s	L306
L305	move.b	#$2e,-$0015(a5)
L306	move.w	-$0014(a5),d0
	lea	-$0019(a5),a0
	move.b	-$0015(a5),0(a0,d0.w)
	addq.w	#1,-$0014(a5)
L307	move.w	-$0014(a5),d0
	cmp.w	-$000a(a5),d0
	blt.s	L304
	move.w	-$0004(a5),d0
	ext.l	d0
	movea.l	d0,a0
	pea	$0028(a0)
	move.w	-$0002(a5),d1
	ext.l	d1
	move.l	d1,-(a7)
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	move.w	-$000a(a5),d0
	ext.l	d0
	move.l	d0,-(a7)
	pea	-$0019(a5)
	move.l	-$791e(a4),-(a7)
	jsr	L628(pc)
	lea	$000c(a7),a7
L308	unlk	a5
	rts	

E309	link	a5,#-$0018
	clr.l	-$000c(a5)
E310	move.l	-$000c(a5),d0
	asl.l	#2,d0
	movea.l	-$78fa(a4),a0
	clr.l	0(a0,d0.l)
	addq.l	#1,-$000c(a5)
	cmpi.l	#$00000080,-$000c(a5)
	blt.s	E310
	move.b	#1,-$7836(a4)
	clr.l	-$000c(a5)
	clr.w	-$783e(a4)
	pea	-$7842(a4)
	jsr	L222(pc)
	addq.w	#4,a7
	move.l	-$78e6(a4),-$0004(a5)
	bra.l	E320

E311	dc.b	$48,$78,0,$18
				;	pea	($0018).w
	dc.b	$48,$78,0,$38
				;	pea	($0038).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	-$0004(a5),-(a7)
	jsr	L440(pc)
	addq.w	#8,a7
	move.l	#$00000028,-$0014(a5)
	move.l	-$0004(a5),d0
	add.l	-$0014(a5),d0
	cmp.l	-$78e2(a4),d0
	ble.s	E312
	move.l	-$78e2(a4),d0
	sub.l	-$0004(a5),d0
	move.l	-$0014(a5),d1
	sub.l	d0,d1
E312	move.l	-$78fe(a4),-(a7)
	move.l	-$0014(a5),-(a7)
	move.l	-$0004(a5),-(a7)
	jsr	E354(pc)
	lea	$000c(a7),a7
	move.l	d0,-$0010(a5)
	tst.l	-$0010(a5)
	bne.l	E321
	clr.l	-$0008(a5)
	bra.l	E318

E313	cmpi.l	#$0000007f,-$000c(a5)
	bgt.l	E319
	move.l	-$0008(a5),d0
	moveq	#$09,d1
	asl.l	d1,d0
	add.l	-$78fe(a4),d0
	move.l	d0,-$0018(a5)
	cmpi.w	#$0001,$000a(a5)
	bne.s	E314
	movea.l	-$0018(a5),a0
	cmpi.l	#$00000002,(a0)
	bne.l	E317
	pea	-$7770(a4)
	movea.l	-$0018(a5),a0
	pea	$01b0(a0)
	jsr	L361(pc)
	addq.w	#8,a7
	pea	-$77e8(a4)
	pea	-$7770(a4)
	jsr	E377(pc)
	addq.w	#8,a7
	tst.l	d0
	beq.s	E317
	bra.s	E316

E314	cmpi.w	#$0002,$000a(a5)
	bne.s	E315
	movea.l	-$0018(a5),a0
	move.l	$01f4(a0),d0
	cmp.l	-$7848(a4),d0
	bne.s	E317
	bra.s	E316

E315	cmpi.w	#$0003,$000a(a5)
	bne.s	E316
	movea.l	-$0018(a5),a0
	move.l	$0004(a0),d0
	cmp.l	-$7848(a4),d0
	bne.s	E317
E316	move.l	-$0004(a5),d0
	add.l	-$0008(a5),d0
	move.l	d0,-(a7)
	move.l	-$000c(a5),-(a7)
	jsr	E349(pc)
	addq.w	#8,a7
	move.l	-$000c(a5),d0
	addq.l	#1,-$000c(a5)
	asl.l	#2,d0
	movea.l	-$78fa(a4),a0
	move.l	-$0004(a5),d1
	add.l	-$0008(a5),d1
	move.l	d1,0(a0,d0.l)
E317	addq.l	#1,-$0008(a5)
E318	move.l	-$0008(a5),d0
	cmp.l	-$0014(a5),d0
	blt.l	E313
E319	cmpi.l	#$0000007f,-$000c(a5)
	bgt.s	E321
	addi.l	#$00000028,-$0004(a5)
E320	move.l	-$0004(a5),d0
	cmp.l	-$78e2(a4),d0
	blt.l	E311
E321	jsr	L348(pc)
	move.w	-$000a(a5),-$783e(a4)
	move.l	-$78fa(a4),-$7842(a4)
	clr.w	-$783c(a4)
	clr.w	-$7942(a4)
	jsr	L295(pc)
	unlk	a5
	rts	

E322	link	a5,#-$0028
	move.l	-$78ee(a4),-$0020(a5)
	move.b	#1,-$7828(a4)
	clr.l	-$000c(a5)
	clr.w	-$7830(a4)
	cmpi.w	#$0001,$000a(a5)
	bne.s	E323
	clr.l	-(a7)
	move.l	-$791e(a4),-(a7)
	jsr	E627(pc)
	addq.w	#8,a7
	bra.s	E325

E323	cmpi.w	#$0002,$000a(a5)
	bne.s	E325
	move.l	#$00000006,-$0004(a5)
E324	move.l	-$0004(a5),d0
	asl.l	#2,d0
	movea.l	-$7902(a4),a0
	clr.l	0(a0,d0.l)
	addq.l	#1,-$0004(a5)
	cmpi.l	#$0000004e,-$0004(a5)
	blt.s	E324
	jsr	L233(pc)
E325	move.l	-$78e6(a4),-$0004(a5)
	bra.l	E340

E326	cmpi.w	#$0002,$000a(a5)
	bne.s	E327
	dc.b	$48,$78,0,$18
				;	pea	($0018).w
	dc.b	$48,$78,0,$38
				;	pea	($0038).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	-$0004(a5),-(a7)
	jsr	L440(pc)
	addq.w	#8,a7
	bra.s	E328

E327	dc.b	$48,$78,1,$8e
				;	pea	($018e).w
	dc.b	$48,$78,0,$08
				;	pea	($0008).w
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	pea	E342(pc)
	jsr	L371(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,6
				;	pea	($0006).w
	move.l	-$0004(a5),-(a7)
	jsr	L440(pc)
	addq.w	#8,a7
E328	move.l	#$00000028,-$0018(a5)
	move.l	-$0004(a5),d0
	add.l	-$0018(a5),d0
	cmp.l	-$78e2(a4),d0
	ble.s	E329
	move.l	-$78e2(a4),d0
	sub.l	-$0004(a5),d0
	move.l	-$0018(a5),d1
	sub.l	d0,d1
E329	move.l	-$78fe(a4),-(a7)
	move.l	-$0018(a5),-(a7)
	move.l	-$0004(a5),-(a7)
	jsr	E354(pc)
	lea	$000c(a7),a7
	move.l	d0,-$0014(a5)
	tst.l	-$0014(a5)
	bne.l	E341
	clr.l	-$0008(a5)
	bra.l	E338

E330	cmpi.l	#$0000007f,-$000c(a5)
	bgt.l	E339
	move.l	-$0008(a5),d0
	moveq	#$09,d1
	asl.l	d1,d0
	add.l	-$78fe(a4),d0
	move.l	d0,-$001c(a5)
	cmpi.w	#$0002,$000a(a5)
	bgt.s	E331
	movea.l	-$001c(a5),a0
	cmpi.l	#$00000002,(a0)
	bne.l	E337
	movea.l	-$001c(a5),a1
	move.l	$01f4(a1),d0
	cmp.l	-$7848(a4),d0
	bne.l	E337
E331	pea	-$7770(a4)
	movea.l	-$001c(a5),a0
	pea	$01b0(a0)
	jsr	L361(pc)
	addq.w	#8,a7
	cmpi.w	#$0002,$000a(a5)
	bne.l	E335
	move.l	-$0004(a5),d0
	add.l	-$0008(a5),d0
	move.l	d0,-$0024(a5)
	pea	-$7770(a4)
	jsr	E411(pc)
	addq.w	#4,a7
	move.l	d0,-$0028(a5)
	move.l	-$0028(a5),d0
	asl.l	#2,d0
	movea.l	-$7902(a4),a0
	tst.l	0(a0,d0.l)
	beq.s	E334
	clr.l	-$000c(a5)
E332	movea.l	-$001c(a5),a0
	tst.l	$01f0(a0)
	bne.s	E333
	move.l	-$0028(a5),d0
	asl.l	#2,d0
	movea.l	-$7902(a4),a0
	move.l	0(a0,d0.l),-$0024(a5)
	bra.s	E334

E333	move.l	-$0028(a5),d0
	asl.l	#2,d0
	movea.l	-$7902(a4),a0
	movea.l	-$001c(a5),a1
	move.l	0(a0,d0.l),d1
	cmp.l	$01f0(a1),d1
	beq.s	E334
	move.l	-$001c(a5),-(a7)
	movea.l	-$001c(a5),a0
	move.l	$01f0(a0),d0
	add.l	-$78e6(a4),d0
	move.l	d0,-(a7)
	jsr	L355(pc)
	addq.w	#8,a7
	addq.l	#1,-$000c(a5)
	cmpi.l	#$0000001e,-$000c(a5)
	blt.s	E332
E334	move.l	-$0028(a5),d0
	asl.l	#2,d0
	movea.l	-$7902(a4),a0
	move.l	-$0024(a5),0(a0,d0.l)
	move.l	-$0024(a5),-(a7)
	move.l	-$0028(a5),-(a7)
	jsr	E349(pc)
	addq.w	#8,a7
	bra.s	E336

E335	cmpi.w	#$0001,$000a(a5)
	bne.s	E336
	moveq	#$22,d1
	move.l	-$000c(a5),d0
	jsr	L538(pc)
	moveq	#$0b,d1
	jsr	L569(pc)
	movea.l	d0,a0
	pea	$0018(a0)
	moveq	#$22,d1
	move.l	-$000c(a5),d0
	jsr	L533(pc)
	move.l	#$000000a0,d1
	jsr	L569(pc)
	addq.l	#8,d0
	move.l	d0,-(a7)
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$13
				;	pea	($0013).w
	pea	-$7770(a4)
	jsr	L373(pc)
	addq.w	#8,a7
	movea.l	-$0020(a5),a0
	move.l	-$0004(a5),d0
	add.l	-$0008(a5),d0
	move.l	d0,$0002(a0)
	movea.l	-$0020(a5),a0
	move.w	#$0001,(a0)
E336	addq.l	#$6,-$0020(a5)
	addq.l	#1,-$000c(a5)
E337	addq.l	#1,-$0008(a5)
E338	move.l	-$0008(a5),d0
	cmp.l	-$0018(a5),d0
	blt.l	E330
E339	cmpi.l	#$00000088,-$000c(a5)
	bgt.s	E341
	addi.l	#$00000028,-$0004(a5)
E340	move.l	-$0004(a5),d0
	cmp.l	-$78e2(a4),d0
	blt.l	E326
E341	bsr.s	L348
	move.w	-$000a(a5),-$7830(a4)
	move.l	-$78ee(a4),-$7834(a4)
	clr.w	-$782e(a4)
	unlk	a5
	rts	

E342	dc.b	$42,$6c,$6f,$63,$6b,$3a
	dc.b	$20,0

E343	link	a5,#-$0004
	move.l	-$7902(a4),-$0004(a5)
	tst.l	-$78a6(a4)
	beq.s	E347
E344	movea.l	-$0004(a5),a0
	move.l	$000c(a0),-(a7)
	movea.l	-$7902(a4),a1
	pea	$0018(a1)
	move.l	-$78a6(a4),-(a7)
	jsr	L578(pc)
	lea	$000c(a7),a7
	movea.l	-$0004(a5),a0
	tst.l	$0010(a0)
	beq.s	E345
	movea.l	-$0004(a5),a0
	move.l	$0010(a0),-(a7)
	jsr	L188(pc)
	addq.w	#4,a7
	bra.s	E346

E345	bra.s	E347

E346	bra.s	E344

E347	unlk	a5
	rts	

L348	link	a5,#$0000
	movea.l	-$78ca(a4),a0
	clr.l	$0024(a0)
	movea.l	-$78ca(a4),a0
	move.w	#$0009,$001c(a0)
	move.l	-$78ca(a4),-(a7)
	jsr	L603(pc)
	addq.w	#4,a7
	unlk	a5
	rts	

E349	link	a5,#-$0004
	move.l	$0008(a5),d0
	and.l	#$00000003,d0
	move.w	d0,-$0002(a5)
	move.l	$0008(a5),d0
	lsr.l	#2,d0
	move.w	d0,-$0004(a5)
	move.w	-$0004(a5),d0
	muls	#$000b,d0
	movea.l	d0,a0
	pea	$0028(a0)
	move.w	-$0002(a5),d0
	muls	#$0048,d0
	movea.l	d0,a1
	pea	$0020(a1)
	move.l	-$791e(a4),-(a7)
	jsr	L624(pc)
	lea	$000c(a7),a7
	dc.b	$48,$78,0,$08
				;	pea	($0008).w
	move.l	$000c(a5),-(a7)
	jsr	L440(pc)
	addq.w	#8,a7
	unlk	a5
	rts	

	dc.b	$4e,$55,$ff,$f0,$20,$6c
	dc.b	$87,$36,$20,$2d,0,$0c
	dc.b	$72,$09,$e3,$a0,$21,$40
	dc.b	0,$24,$20,$6c,$87,$36
	dc.b	$21,$6d,0,$10,0,$28
	dc.b	$20,$6c,$87,$36,$31,$7c
	dc.b	0,3,0,$1c,$20,$6c
	dc.b	$87,$36,$20,$2d,0,$08
	dc.b	$72,$09,$e3,$a0,$21,$40
	dc.b	0,$2c,$2f,$2c,$87,$36

	jsr	L603(pc)
	addq.w	#4,a7
	movea.l	-$78ca(a4),a0
	move.b	$001f(a0),d0
	ext.w	d0
	move.w	d0,-$0002(a5)
	tst.w	-$0002(a5)
	beq.s	E350
	bra.s	E353

E350	move.w	$000a(a5),-$0004(a5)
	bra.s	E352

E351	move.w	-$0004(a5),d0
	ext.l	d0
	subq.l	#2,d0
	move.l	d0,-$0008(a5)
	moveq	#$20,d1
	move.l	-$0008(a5),d0
	jsr	L533(pc)
	addq.l	#1,d0
	move.l	d0,-$000c(a5)
	andi.l	#$0000001f,-$0008(a5)
	moveq	#1,d0
	move.l	-$0008(a5),d1
	asl.l	d1,d0
	move.l	d0,-$0010(a5)
	move.l	-$000c(a5),d0
	asl.l	#2,d0
	lea	-$7666(a4),a0
	adda.l	d0,a0
	move.l	-$0010(a5),d1
	not.l	d1
	and.l	d1,(a0)
	addq.w	#1,-$0004(a5)
E352	move.l	$000c(a5),d0
	subq.l	#1,$000c(a5)
	tst.l	d0
	bne.s	E351
E353	unlk	a5
	rts	

E354	link	a5,#-$0002
	movea.l	-$78ca(a4),a0
	move.l	$000c(a5),d0
	moveq	#$09,d1
	asl.l	d1,d0
	move.l	d0,$0024(a0)
	movea.l	-$78ca(a4),a0
	move.l	$0010(a5),$0028(a0)
	movea.l	-$78ca(a4),a0
	move.w	#$0002,$001c(a0)
	movea.l	-$78ca(a4),a0
	move.l	$0008(a5),d0
	moveq	#$09,d1
	asl.l	d1,d0
	move.l	d0,$002c(a0)
	move.l	-$78ca(a4),-(a7)
	jsr	L603(pc)
	addq.w	#4,a7
	movea.l	-$78ca(a4),a0
	move.b	$001f(a0),d0
	ext.w	d0
	move.w	d0,-$0002(a5)
	tst.w	-$0002(a5)
	move.w	-$0002(a5),d0
	ext.l	d0
	unlk	a5
	rts	

L355	link	a5,#-$0002
	movea.l	-$78ca(a4),a0
	move.l	#$00000200,$0024(a0)
	movea.l	-$78ca(a4),a0
	move.l	$000c(a5),$0028(a0)
	movea.l	-$78ca(a4),a0
	move.w	#$0002,$001c(a0)
	movea.l	-$78ca(a4),a0
	move.l	$0008(a5),d0
	moveq	#$09,d1
	asl.l	d1,d0
	move.l	d0,$002c(a0)
	move.l	-$78ca(a4),-(a7)
	jsr	L603(pc)
	addq.w	#4,a7
	movea.l	-$78ca(a4),a0
	move.b	$001f(a0),d0
	ext.w	d0
	move.w	d0,-$0002(a5)
	tst.w	-$0002(a5)
	unlk	a5
	rts	

L356	link	a5,#-$0002
	movea.l	-$78ca(a4),a0
	move.l	#$00000200,$0024(a0)
	movea.l	-$78ca(a4),a0
	move.l	$000c(a5),$0028(a0)
	movea.l	-$78ca(a4),a0
	move.w	#$0003,$001c(a0)
	movea.l	-$78ca(a4),a0
	move.l	$0008(a5),d0
	moveq	#$09,d1
	asl.l	d1,d0
	move.l	d0,$002c(a0)
	move.l	-$78ca(a4),-(a7)
	jsr	L603(pc)
	addq.w	#4,a7
	movea.l	-$78ca(a4),a0
	move.b	$001f(a0),d0
	ext.w	d0
	move.w	d0,-$0002(a5)
	tst.w	-$0002(a5)
	unlk	a5
	rts	

L357	link	a5,#-$0006
	clr.l	-$0006(a5)
	clr.w	-$0002(a5)
	bra.s	L360

L358	cmpi.w	#$0005,-$0002(a5)
	beq.s	L359
	move.w	-$0002(a5),d0
	ext.l	d0
	asl.l	#2,d0
	movea.l	$0008(a5),a0
	move.l	0(a0,d0.l),d1
	sub.l	d1,-$0006(a5)
L359	addq.w	#1,-$0002(a5)
L360	cmpi.w	#$0080,-$0002(a5)
	blt.s	L358
	move.l	-$0006(a5),d0
	unlk	a5
	rts	

L361	link	a5,#-$0006
	move.l	$000c(a5),-$0004(a5)
	movea.l	-$0004(a5),a0
	clr.b	(a0)
	tst.l	$0008(a5)
	bne.s	L363
L362	unlk	a5
	rts	

L363	movea.l	$0008(a5),a0
	addq.l	#1,$0008(a5)
	moveq	#0,d0
	move.b	(a0),d0
	move.w	d0,-$0006(a5)
	cmpi.w	#$0032,-$0006(a5)
	ble.s	L364
	move.w	#$0032,-$0006(a5)
L364	move.w	-$0006(a5),d0
	subq.w	#1,-$0006(a5)
	tst.w	d0
	beq.s	L365
	movea.l	$0008(a5),a0
	addq.l	#1,$0008(a5)
	movea.l	-$0004(a5),a1
	addq.l	#1,-$0004(a5)
	move.b	(a0),(a1)
	bra.s	L364

L365	movea.l	-$0004(a5),a0
	addq.l	#1,-$0004(a5)
	clr.b	(a0)
	bra.s	L362

L366	link	a5,#-$0002
	clr.w	-$0002(a5)
L367	movea.l	$0008(a5),a0
	addq.l	#1,$0008(a5)
	tst.b	(a0)
	beq.s	L368
	addq.w	#1,-$0002(a5)
	bra.s	L367

L368	move.w	-$0002(a5),d0
	ext.l	d0
	unlk	a5
	rts	

L369	link	a5,#-$0002
	move.l	$0008(a5),-(a7)
	bsr.s	L366
	addq.w	#4,a7
	move.w	d0,-$0002(a5)
	move.w	-$0002(a5),d0
	ext.l	d0
	move.l	d0,-(a7)
	move.l	$0008(a5),-(a7)
	move.l	-$78aa(a4),-(a7)
	jsr	L578(pc)
	lea	$000c(a7),a7
	unlk	a5
	rts	

L370	link	a5,#$0000
	movem.l	d4-d5,-(a7)
	move.l	$0008(a5),d4
	move.l	$000c(a5),d5
	pea	-$785a(a4)
	jsr	L454(pc)
	addq.w	#4,a7
	move.l	d5,-(a7)
	move.l	d4,-(a7)
	jsr	L466(pc)
	addq.w	#8,a7
	clr.l	-(a7)
	jsr	L464(pc)
	addq.w	#4,a7
	pea	-$785a(a4)
	bsr.s	L369
	addq.w	#4,a7
	movem.l	(a7)+,d4-d5
	unlk	a5
	rts	

L371	link	a5,#-$0002
	move.l	$0008(a5),-(a7)
	jsr	L366(pc)
	addq.w	#4,a7
	move.w	d0,-$0002(a5)
	cmpi.w	#$0050,-$0002(a5)
	bls.s	L372
	move.w	#$0050,-$0002(a5)
L372	moveq	#0,d0
	move.w	-$0002(a5),d0
	move.l	d0,-(a7)
	move.l	$0008(a5),-(a7)
	move.l	-$791e(a4),-(a7)
	jsr	L628(pc)
	lea	$000c(a7),a7
	unlk	a5
	rts	

L373	link	a5,#-$0002
	move.l	$0008(a5),-(a7)
	jsr	L366(pc)
	addq.w	#4,a7
	move.w	d0,-$0002(a5)
	moveq	#0,d0
	move.w	-$0002(a5),d0
	move.w	$000e(a5),d1
	ext.l	d1
	cmp.l	d1,d0
	bls.s	L374
	move.w	$000e(a5),-$0002(a5)
L374	moveq	#0,d0
	move.w	-$0002(a5),d0
	move.l	d0,-(a7)
	move.l	$0008(a5),-(a7)
	move.l	-$791e(a4),-(a7)
	jsr	L628(pc)
	lea	$000c(a7),a7
	moveq	#0,d0
	move.w	-$0002(a5),d0
	move.w	$000e(a5),d1
	ext.l	d1
	cmp.l	d1,d0
	bcc.s	L375
	move.w	$000e(a5),d0
	ext.l	d0
	moveq	#0,d1
	move.w	-$0002(a5),d1
	sub.l	d1,d0
	move.l	d0,-(a7)
	pea	-$7a12(a4)
	move.l	-$791e(a4),-(a7)
	jsr	L628(pc)
	lea	$000c(a7),a7
L375	unlk	a5
	rts	

	dc.b	$4e,$55,$ff,$fe,$3b,$6d
	dc.b	0,$12,$ff,$fe,$30,$2d
	dc.b	$ff,$fe,$b0,$6d,0,$0e
	dc.b	$6f,$06,$3b,$6d,0,$0e
	dc.b	$ff,$fe,$30,$2d,$ff,$fe
	dc.b	$48,$c0,$2f,0,$2f,$2d
	dc.b	0,8,$2f,$2c,$86,$e2

	jsr	L628(pc)
	lea	$000c(a7),a7
	move.w	-$0002(a5),d0
	cmp.w	$0012(a5),d0
	bge.s	E376
	move.w	$0012(a5),d0
	ext.l	d0
	move.w	-$0002(a5),d1
	ext.l	d1
	sub.l	d1,d0
	move.l	d0,-(a7)
	pea	-$7a12(a4)
	move.l	-$791e(a4),-(a7)
	jsr	L628(pc)
	lea	$000c(a7),a7
E376	unlk	a5
	rts	

E377	link	a5,#$0000
	movem.l	a2-a3,-(a7)
	movea.l	$0008(a5),a2
	movea.l	$000c(a5),a3
E378	tst.b	(a3)
	beq.l	E387
	cmpi.b	#$2a,(a3)
	bne.s	E384
E379	cmpi.b	#$2a,(a3)
	bne.s	E380
	addq.l	#1,a3
	bra.s	E379

E380	tst.b	(a2)
	beq.s	E381
	move.b	(a2),d0
	ext.w	d0
	ext.l	d0
	move.l	d0,-(a7)
	jsr	E477(pc)
	addq.w	#4,a7
	move.l	d0,-(a7)
	move.b	(a3),d0
	ext.w	d0
	ext.l	d0
	move.l	d0,-(a7)
	jsr	E477(pc)
	addq.w	#4,a7
	move.l	(a7)+,d1
	cmp.l	d0,d1
	beq.s	E381
	addq.l	#1,a2
	bra.s	E380

E381	tst.b	(a2)
	bne.s	E384
	tst.b	(a3)
	bne.s	E383
	moveq	#1,d0
E382	movem.l	(a7)+,a2-a3
	unlk	a5
	rts	

E383	moveq	#0,d0
	bra.s	E382

E384	cmpi.b	#$3f,(a3)
	bne.s	E385
	addq.l	#1,a3
	addq.l	#1,a2
	bra.s	E378

E385	move.b	(a3),d0
	ext.w	d0
	ext.l	d0
	move.l	d0,-(a7)
	jsr	E477(pc)
	addq.w	#4,a7
	move.l	d0,-(a7)
	move.b	(a2),d0
	ext.w	d0
	ext.l	d0
	move.l	d0,-(a7)
	jsr	E477(pc)
	addq.w	#4,a7
	move.l	(a7)+,d1
	cmp.l	d0,d1
	beq.s	E386
	moveq	#0,d0
	bra.s	E382

E386	addq.l	#1,a3
	addq.l	#1,a2
	bra.l	E378

E387	tst.b	(a2)
	bne.s	E388
	tst.b	(a3)
	bne.s	E388
	moveq	#1,d0
	bra.s	E382

E388	moveq	#0,d0
	bra.s	E382

L389	dc.b	$4a,$61,$6e,0
L390	dc.b	$46,$65,$62,0
L391	dc.b	$4d,$61,$72,0
L392	dc.b	$41,$70,$72,0
L393	dc.b	$4d,$61,$79,0
L394	dc.b	$4a,$75,$6e,0
L395	dc.b	$4a,$75,$6c,0
L396	dc.b	$41,$75,$67,0
L397	dc.b	$53,$65,$70,0
L398	dc.b	$4f,$63,$74,0
L399	dc.b	$4e,$6f,$76,0
L400	dc.b	$44,$65,$63,0

L401	link	a5,#$0000
	movem.l	d4-d6/a2,-(a7)
	movea.l	$0008(a5),a2
	move.l	#$000007ba,d4
	move.l	(a2),d6
L402	cmp.l	#$0000016e,d6
	blt.s	L405
	move.l	d4,d0
	sub.l	#$000007b8,d0
	moveq	#$04,d1
	jsr	L538(pc)
	bne.s	L403
	sub.l	#$0000016e,d6
	addq.l	#1,d4
	bra.s	L404

L403	move.l	d4,d0
	sub.l	#$000007b8,d0
	moveq	#$04,d1
	jsr	L538(pc)
	beq.s	L404
	cmp.l	#$0000016d,d6
	blt.s	L404
	sub.l	#$0000016d,d6
	addq.l	#1,d4
L404	bra.s	L402

L405	move.l	d4,d0
	sub.l	#$000007b8,d0
	moveq	#$04,d1
	jsr	L538(pc)
	bne.s	L406
	move.l	#$0000016e,d1
	move.l	d6,d0
	jsr	L538(pc)
	move.l	d0,d6
	move.b	#$1d,-$79ac(a4)
	bra.s	L407

L406	move.l	#$0000016d,d1
	move.l	d6,d0
	jsr	L538(pc)
	move.l	d0,d6
	move.b	#$1c,-$79ac(a4)
L407	moveq	#0,d5
	bra.s	L409

L408	lea	-$79ae(a4),a0
	moveq	#0,d0
	move.b	0(a0,d5.l),d0
	sub.l	d0,d6
	addq.l	#1,d5
L409	lea	-$79ae(a4),a0
	moveq	#0,d0
	move.b	0(a0,d5.l),d0
	cmp.l	d0,d6
	bhi.s	L408
	dc.b	$48,$78,0,$02
				;	pea	($0002).w
	move.l	d6,d0
	addq.l	#1,d0
	move.l	d0,-(a7)
	jsr	L465(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$2d
				;	pea	($002d).w
	jsr	L464(pc)
	addq.w	#4,a7
	move.l	d5,d0
	asl.l	#2,d0
	lea	-$79de(a4),a0
	move.l	0(a0,d0.l),-(a7)
	jsr	L456(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$2d
				;	pea	($002d).w
	jsr	L464(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$02
				;	pea	($0002).w
	move.l	d4,-(a7)
	jsr	L465(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$2d
				;	pea	($002d).w
	jsr	L464(pc)
	addq.w	#4,a7
	movem.l	(a7)+,d4-d6/a2
	unlk	a5
	rts	

L410	link	a5,#$0000
	movem.l	d4-d6/a2,-(a7)
	movea.l	$0008(a5),a2
	move.l	$0008(a2),d0
	moveq	#$32,d1
	jsr	L533(pc)
	move.l	d0,d6
	moveq	#$3c,d1
	move.l	d6,d0
	jsr	L538(pc)
	move.l	d0,d6
	move.l	$0004(a2),d5
	moveq	#$3c,d1
	move.l	d5,d0
	jsr	L533(pc)
	move.l	d0,d4
	moveq	#$3c,d1
	move.l	d5,d0
	jsr	L538(pc)
	move.l	d0,d5
	dc.b	$48,$78,0,$02
				;	pea	($0002).w
	move.l	d4,-(a7)
	jsr	L465(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$3a
				;	pea	($003a).w
	jsr	L464(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$02
				;	pea	($0002).w
	move.l	d5,-(a7)
	jsr	L465(pc)
	addq.w	#8,a7
	dc.b	$48,$78,0,$3a
				;	pea	($003a).w
	jsr	L464(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$02
				;	pea	($0002).w
	move.l	d6,-(a7)
	jsr	L465(pc)
	addq.w	#8,a7
	movem.l	(a7)+,d4-d6/a2
	unlk	a5
	rts	

E411	link	a5,#-$000e
	move.l	$0008(a5),-(a7)
	jsr	L366(pc)
	addq.w	#4,a7
	move.l	d0,-$0004(a5)
	move.l	d0,-$0008(a5)
	clr.l	-$000c(a5)
	bra.s	E414

E412	movea.l	$0008(a5),a0
	addq.l	#1,$0008(a5)
	move.b	(a0),-$000d(a5)
	cmpi.b	#$61,-$000d(a5)
	blt.s	E413
	cmpi.b	#$7a,-$000d(a5)
	bgt.s	E413
	subi.b	#$20,-$000d(a5)
E413	move.b	-$000d(a5),d0
	ext.w	d0
	ext.l	d0
	moveq	#$0d,d1
	move.l	d0,-(a7)
	move.l	-$0004(a5),d0
	jsr	L569(pc)
	move.l	(a7)+,d2
	add.l	d0,d2
	and.l	#$000007ff,d2
	move.l	d2,-$0004(a5)
	addq.l	#1,-$000c(a5)
E414	move.l	-$000c(a5),d0
	cmp.l	-$0008(a5),d0
	blt.s	E412
	moveq	#$48,d1
	move.l	-$0004(a5),d0
	jsr	L538(pc)
	addq.l	#$6,d0
	unlk	a5
	rts	

L415	move.l	$0004(a7),d0
	cmp.b	#$61,d0
	bcs.s	L416
	cmp.b	#$7a,d0
	bhi.s	L416
	add.b	#-$20,d0
L416	move.l	$0008(a7),d1
	cmp.b	#$61,d1
	bcs.s	L417
	cmp.b	#$7a,d1
	bhi.s	L417
	add.b	#-$20,d1
L417	cmp.b	d0,d1
	beq.s	L418
	clr.l	d0
	rts	

L418	moveq	#-1,d0
	rts	

	dc.b	$20,$6f,0,$04,$48,$e7
	dc.b	$f0,$e2,$2c,$6c,$86,$ce
	dc.b	$22,$6c,$86,$e2

E419	movea.l	a0,a2
	clr.l	d0
	move.b	(a0)+,d0
	beq.l	E426
	bmi.s	E422
	cmp.b	#-$80,d0
	beq.l	E425
	movea.l	a2,a0
	clr.l	d0
E420	tst.b	(a2)+
	beq.s	E421
	bmi.s	E421
	addq.w	#1,d0
	bra.s	E420

E421	movem.l	d0-d7/a0-a5,-(a7)
	jsr	-$003c(a6)
	movem.l	(a7)+,d0-d7/a0-a5
	adda.w	d0,a0
	bra.s	E419

E422	cmp.b	#-$74,d0
	bcc.s	E424
	cmp.b	#-$7b,d0
	bcc.s	E423
	cmp.b	#-$80,d0
	beq.s	E425
	cmp.b	#-$7f,d0
	beq.l	E427
	cmp.b	#-$7e,d0
	beq.l	E428
	cmp.b	#-$7d,d0
	beq.l	E429
	cmp.b	#-$7c,d0
	beq.l	E430
	bra.s	E419

E423	cmp.b	#-$7b,d0
	beq.l	E431
	cmp.b	#-$7a,d0
	beq.l	E431
	cmp.b	#-$79,d0
	beq.s	E431
	cmp.b	#-$78,d0
	beq.s	E431
	bra.l	E419

E424	cmp.b	#-$74,d0
	beq.s	E432
	cmp.b	#-$73,d0
	beq.l	E433
	cmp.b	#-$71,d0
	beq.l	E434
	cmp.b	#-$6a,d0
	beq.l	E439
	cmp.b	#-$69,d0
	beq.l	E436
	cmp.b	#-$68,d0
	beq.l	E437
	bra.l	E419

E425	clr.l	d1
	move.b	(a0)+,d0
	move.b	(a0)+,d1
	add.w	d0,d0
	add.w	d0,d0
	jsr	-$00f0(a6)
	bra.l	E419

E426	movem.l	(a7)+,d0-d3/a0-a2/a6
	rts	

E427	move.b	(a0)+,d0
	jsr	-$0156(a6)
	bra.l	E419

E428	move.b	(a0)+,d0
	jsr	-$015c(a6)
	bra.l	E419

E429	moveq	#0,d0
	jsr	-$0162(a6)
	bra.l	E419

E430	moveq	#1,d0
	jsr	-$0162(a6)
	bra.l	E419

E431	bra.l	E419

E432	clr.l	d0
	clr.l	d1
	clr.l	d2
	clr.l	d3
	move.b	(a0)+,d0
	add.w	d0,d0
	add.w	d0,d0
	move.b	(a0)+,d1
	move.b	(a0)+,d2
	add.w	d2,d2
	add.w	d2,d2
	move.b	(a0)+,d3
	movem.l	d0-d3/a0,-(a7)
	jsr	-$00f0(a6)
	move.w	d1,-(a7)
	move.w	d0,-(a7)
	move.w	d3,-(a7)
	move.w	d0,-(a7)
	move.w	d3,-(a7)
	move.w	d2,-(a7)
	move.w	d1,-(a7)
	move.w	d2,-(a7)
	movea.l	a7,a0
	moveq	#$04,d0
	jsr	-$0150(a6)
	adda.w	#$0010,a7
	movem.l	(a7)+,d0-d3/a0
	bra.l	E419

E433	clr.l	d0
	clr.l	d1
	clr.l	d2
	clr.l	d3
	move.b	(a0)+,d0
	add.w	d0,d0
	add.w	d0,d0
	move.b	(a0)+,d1
	move.b	(a0)+,d2
	add.w	d2,d2
	add.w	d2,d2
	move.b	(a0)+,d3
	movem.l	a0-a1/a6,-(a7)
	jsr	-$0132(a6)
	movem.l	(a7)+,a0-a1/a6
	bra.l	E419

E434	move.b	$0019(a1),d4
	clr.l	d0
	clr.l	d1
	clr.l	d2
	clr.l	d3
	move.b	(a0)+,d0
	add.w	d0,d0
	add.w	d0,d0
	move.b	(a0)+,d1
	move.b	(a0)+,d2
	add.w	d2,d2
	add.w	d2,d2
	move.b	(a0)+,d3
	bsr.l	E435
	movem.l	d0-d3/a0-a1/a6,-(a7)
	jsr	-$0132(a6)
	movem.l	(a7)+,d0-d3/a0-a1/a6
	bsr.s	E435
	movem.l	d0-d3/a0,-(a7)
	jsr	-$00f0(a6)
	addq.w	#1,d0
	subq.b	#1,d2
	move.w	d1,-(a7)
	move.w	d2,-(a7)
	move.w	d3,-(a7)
	move.w	d2,-(a7)
	move.w	d3,-(a7)
	move.w	d0,-(a7)
	move.w	d1,-(a7)
	move.w	d0,-(a7)
	subq.w	#1,d0
	addq.b	#1,d2
	move.w	d1,-(a7)
	move.w	d2,-(a7)
	move.w	d3,-(a7)
	move.w	d2,-(a7)
	move.w	d3,-(a7)
	move.w	d0,-(a7)
	movea.l	a7,a0
	moveq	#$07,d0
	jsr	-$0150(a6)
	adda.w	#$001c,a7
	movem.l	(a7)+,d0-d3/a0
	bsr.s	E435
	movem.l	d0-d3/a0-a1/a6,-(a7)
	move.l	d2,d0
	addq.l	#3,d2
	addq.l	#2,d1
	addq.l	#2,d3
	addq.l	#1,d0
	jsr	-$0132(a6)
	movem.l	(a7)+,d0-d3/a0-a1/a6
	movem.l	d0-d3/a0-a1/a6,-(a7)
	move.l	d3,d1
	addq.l	#2,d3
	addq.l	#3,d0
	addq.l	#1,d1
	jsr	-$0132(a6)
	movem.l	(a7)+,d0-d3/a0-a1/a6
	clr.l	d0
	move.b	d4,d0
	jsr	-$0156(a6)
	bra.l	E419

E435	movem.l	d0-d3/a1/a6,-(a7)
	clr.l	d0
	move.b	(a0)+,d0
	jsr	-$0156(a6)
	movem.l	(a7)+,d0-d3/a1/a6
	rts	

E436	bra.l	E419

E437	clr.l	d2
	clr.l	d3
	clr.l	d4
	move.b	(a0)+,d2
	move.b	(a0)+,d3
	move.b	(a0)+,d4
E438	clr.l	d0
	move.b	(a0)+,d0
	add.w	d0,d0
	add.w	d0,d0
	movem.l	d0-d4/a0,-(a7)
	move.w	d2,d1
	jsr	-$00f0(a6)
	movem.l	(a7)+,d0-d4/a0
	movem.l	d0-d4/a0,-(a7)
	move.w	d3,d1
	jsr	-$00f6(a6)
	movem.l	(a7)+,d0-d4/a0
	subq.w	#1,d4
	bne.s	E438
	bra.l	E419

E439	clr.l	d0
	clr.l	d1
	move.b	(a0)+,d0
	move.b	(a0)+,d1
	add.w	d0,d0
	add.w	d0,d0
	jsr	-$00f0(a6)
	clr.l	d0
	move.b	(a0)+,d0
	move.b	(a0)+,d1
	add.w	d0,d0
	add.w	d0,d0
	move.l	a0,-(a7)
	jsr	-$00f6(a6)
	movea.l	(a7)+,a0
	bra.l	E419

L440	move.l	$0004(a7),d0
	movem.l	d0-d7/a0-a6,-(a7)
	move.l	$0040(a7),d0
	move.l	$0044(a7),d1
	bsr.s	L441
	move.l	$0044(a7),d0
	movea.l	-$791e(a4),a1
	movea.l	-$7932(a4),a6
	jsr	-$003c(a6)
	movem.l	(a7)+,d0-d7/a0-a6
	rts	

	dc.b	$20,$2f,0,4

L441	lea	-$799a(a4),a0
	bra.s	L444

L442	move.b	d0,d2
	and.b	#$0f,d2
	add.b	#$30,d2
	cmp.b	#$39,d2
	ble.s	L443
	addq.b	#$7,d2
L443	move.b	d2,0(a0,d1.w)
	lsr.l	#4,d0
L444	dbf	d1,L442
	rts	

L445	movem.l	d0-d2/a0,-(a7)
	movea.l	$0014(a7),a0
	move.l	$0018(a7),d0
	move.l	$001c(a7),d1
	subq.w	#1,d1
L446	divu	#$000a,d0
	swap	d0
	add.b	#$30,d0
	move.b	d0,0(a0,d1.w)
	clr.w	d0
	swap	d0
	dbf	d1,L446
	movem.l	(a7)+,d0-d2/a0
	rts	

L447	move.l	$0004(a7),d0
	movem.l	d0-d7/a0-a6,-(a7)
	clr.l	d3
	bra.s	L449

L448	move.l	$0004(a7),d0
	movem.l	d0-d7/a0-a6,-(a7)
	moveq	#-1,d3
L449	bsr.s	L450
	adda.w	#$000a,a0
	move.l	$0044(a7),d0
	suba.w	d0,a0
	movea.l	-$791e(a4),a1
	movea.l	-$7932(a4),a6
	movem.l	d0/a0,-(a7)
	moveq	#1,d0
	jsr	-$0156(a6)
	moveq	#0,d0
	jsr	-$015c(a6)
	movem.l	(a7)+,d0/a0
	jsr	-$003c(a6)
	movem.l	(a7)+,d0-d7/a0-a6
	rts	

	dc.b	$20,$2f,0,$04,$76,$ff

L450	lea	-$799a(a4),a0
	moveq	#$09,d1
	bra.s	L452

L451	tst.w	d3
	bne.s	L452
	tst.w	d0
	beq.s	L453
L452	divu	#$000a,d0
	swap	d0
	add.b	#$30,d0
	move.b	d0,0(a0,d1.w)
	clr.w	d0
	swap	d0
	dbf	d1,L451
	rts	

L453	move.b	#$20,0(a0,d1.w)
	dbf	d1,L453
	rts	

L454	move.l	$0004(a7),-$79a2(a4)
	move.l	$0004(a7),-$799e(a4)
	rts	

L455	move.l	a0,-(a7)
	movea.l	-$799e(a4),a0
	clr.b	(a0)
	move.l	$0008(a7),-(a7)
	move.l	-$79a2(a4),-(a7)
	jsr	L373(pc)
	addq.w	#8,a7
	movea.l	(a7)+,a0
	rts	

	dc.b	$20,$2f,0,$04,$d0,$ac
	dc.b	$86,$5e,$20,$6c,$86,$62
	dc.b	$b0,$88,$63,$06,$10,$fc
	dc.b	0,$20,$60,$f6,$29,$48
	dc.b	$86,$62,$4e,$75

L456	movem.l	a0-a1,-(a7)
	movea.l	-$799e(a4),a0
	movea.l	$000c(a7),a1
L457	tst.b	(a1)
	beq.s	L458
	move.b	(a1)+,(a0)+
	bra.s	L457

L458	move.l	a0,-$799e(a4)
	movem.l	(a7)+,a0-a1
	rts	

L459	movem.l	d0/a0-a1,-(a7)
	movea.l	-$799e(a4),a0
	movea.l	$0010(a7),a1
	move.l	$0014(a7),d0
L460	tst.w	d0
	beq.s	L462
	tst.b	(a1)
	beq.s	L461
	move.b	(a1)+,(a0)+
	subq.w	#1,d0
	bra.s	L460

L461	tst.w	d0
	beq.s	L462
	move.b	#$20,(a0)+
	subq.w	#1,d0
	bra.s	L461

L462	move.l	a0,-$799e(a4)
	movem.l	(a7)+,d0/a0-a1
	rts	

L463	move.l	a0,-(a7)
	movea.l	-$799e(a4),a0
	move.b	#$20,(a0)+
	move.l	a0,-$799e(a4)
	movea.l	(a7)+,a0
	rts	

L464	move.l	a0,-(a7)
	movea.l	-$799e(a4),a0
	move.b	$000b(a7),(a0)+
	move.l	a0,-$799e(a4)
	movea.l	(a7)+,a0
	rts	

L465	movem.l	d0-d4/a0,-(a7)
	moveq	#-1,d3
	bra.s	L467

L466	movem.l	d0-d4/a0,-(a7)
	clr.l	d3
	moveq	#$20,d4
L467	movea.l	-$799e(a4),a0
	move.l	$001c(a7),d0
	bpl.s	L468
	moveq	#$2d,d4
	neg.l	d0
L468	move.l	$0020(a7),d1
	subq.w	#1,d1
	bra.s	L470

L469	tst.l	d0
	bne.s	L470
	tst.b	d3
	bne.s	L470
	move.b	d4,0(a0,d1.w)
	moveq	#$20,d4
	bra.s	L471

L470	divu	#$000a,d0
	swap	d0
	add.b	#$30,d0
	move.b	d0,0(a0,d1.w)
	clr.w	d0
	swap	d0
L471	dbf	d1,L469
	adda.l	$0020(a7),a0
	tst.w	d3
	bne.s	L472
	move.b	#$20,(a0)+
L472	move.l	a0,-$799e(a4)
	movem.l	(a7)+,d0-d4/a0
	rts	

L473	movem.l	d0-d3/a0,-(a7)
	movea.l	-$799e(a4),a0
	move.l	$0018(a7),d0
	move.l	$001c(a7),d1
	bra.s	L476

L474	move.b	d0,d2
	lsr.l	#4,d0
	and.b	#$0f,d2
	add.b	#$30,d2
	cmp.b	#$39,d2
	ble.s	L475
	addq.b	#$7,d2
L475	move.b	d2,0(a0,d1.w)
L476	dbf	d1,L474
	adda.l	$001c(a7),a0
	move.l	a0,-$799e(a4)
	movem.l	(a7)+,d0-d3/a0
	rts	

	dc.b	$70,0,$10,$2f,0,$07
	dc.b	$b0,$3c,0,$60,$63,$0a
	dc.b	$b0,$3c,0,$7a,$62,4
	dc.b	$90,$3c,0,$20,$4e,$75

E477	moveq	#0,d0
	move.b	$0007(a7),d0
	cmp.b	#$40,d0
	bls.s	E478
	cmp.b	#$5a,d0
	bhi.s	E478
	add.b	#$20,d0
E478	rts	

L479	bsr.s	L487
	lea	-$798a(a4),a1
	lea	-$798a(a4),a2
	cmpa.l	a1,a2
	bne.s	L481
	move.w	#$0148,d1
	bmi.s	L481
	moveq	#0,d2
L480	move.l	d2,(a1)+
	dbf	d1,L480
L481	move.l	a7,-$7986(a4)
	dc.b	$2c,$78,0,4
				;	movea.l	($0004).w,a6
	move.l	a6,-$7982(a4)
	movem.l	d0/a0,-(a7)
	btst	#$04,$0129(a6)
	beq.s	L483
	lea	L482(pc),a5
	jsr	-$001e(a6)
	bra.s	L483

L482	dc.b	$42,$a7,$f3,$5f,$4e,$73

L483	lea	L486(pc),a1
	jsr	-$0198(a6)
	move.l	d0,-$798a(a4)
	bne.s	L484
	move.l	#$00038007,d7
	jsr	-$006c(a6)
	bra.s	L485

L484	jsr	L488(pc)
L485	addq.w	#8,a7
	rts	

L486	dc.b	'dos.library',0

L487	lea	seg1+$00007ffe,a4
	rts	

L488	link	a5,#$0000
	move.l	a2,-(a7)
	pea	$00010000
	move.w	-$798e(a4),d0
	muls	#$0006,d0
	move.l	d0,-(a7)
	jsr	L602(pc)
	addq.w	#8,a7
	move.l	d0,-$797e(a4)
	bne.s	L489
	clr.l	-(a7)
	pea	$00010000
	jsr	L580(pc)
	addq.w	#8,a7
	movea.l	-$7986(a4),a7
	rts	

L489	movea.l	-$797e(a4),a0
	clr.w	$0004(a0)
	movea.l	-$797e(a4),a0
	move.w	#$0001,$0010(a0)
	movea.l	-$797e(a4),a1
	move.w	#$0001,$000a(a1)
	movea.l	-$7986(a4),a0
	move.l	-$7986(a4),d0
	sub.l	$0004(a0),d0
	addq.l	#8,d0
	move.l	d0,-$797a(a4)
	movea.l	-$797a(a4),a0
	move.l	#$4d414e58,(a0)
	clr.l	-(a7)
	jsr	L605(pc)
	addq.w	#4,a7
	movea.l	d0,a2
	tst.l	$00ac(a2)
	beq.s	L490
	move.l	$000c(a5),-(a7)
	move.l	$0008(a5),-(a7)
	move.l	a2,-(a7)
	jsr	L495(pc)
	lea	$000c(a7),a7
	move.l	#$00000001,-$7976(a4)
	movea.l	-$797e(a4),a0
	ori.w	#-$8000,$0004(a0)
	movea.l	-$797e(a4),a0
	ori.w	#-$8000,$000a(a0)
	bra.s	L492

L490	pea	$005c(a2)
	jsr	L620(pc)
	addq.w	#4,a7
	pea	$005c(a2)
	jsr	L611(pc)
	addq.w	#4,a7
	move.l	d0,-$7972(a4)
	movea.l	-$7972(a4),a0
	tst.l	$0024(a0)
	beq.s	L491
	movea.l	-$7972(a4),a0
	movea.l	$0024(a0),a1
	move.l	(a1),-(a7)
	jsr	L572(pc)
	addq.w	#4,a7
L491	move.l	-$7972(a4),-(a7)
	move.l	a2,-(a7)
	jsr	L525(pc)
	addq.w	#8,a7
	move.l	-$7972(a4),-$796e(a4)
L492	jsr	L573(pc)
	movea.l	-$797e(a4),a0
	move.l	d0,(a0)
	jsr	L577(pc)
	movea.l	-$797e(a4),a0
	move.l	d0,$0006(a0)
	beq.s	L493
	dc.b	$48,$78,3,$ed
				;	pea	($03ed).w
	pea	L494(pc)
	jsr	L575(pc)
	addq.w	#8,a7
	movea.l	-$797e(a4),a0
	move.l	d0,$000c(a0)
L493	move.l	-$796e(a4),-(a7)
	move.l	-$796a(a4),-(a7)
	jsr	L31(pc)
	addq.w	#8,a7
	clr.l	-(a7)
	jsr	L549(pc)
	addq.w	#4,a7
	movea.l	(a7)+,a2
	unlk	a5
	rts	

L494	dc.b	$2a,0

L495	link	a5,#$0000
	movem.l	d4-d5/a2-a3,-(a7)
	movea.l	$0010(a5),a2
	movea.l	$0008(a5),a0
	move.l	$00ac(a0),d0
	asl.l	#2,d0
	move.l	d0,d4
	movea.l	d4,a0
	move.l	$0010(a0),d0
	asl.l	#2,d0
	movea.l	d0,a3
	move.b	(a3),d0
	ext.w	d0
	ext.l	d0
	add.l	$000c(a5),d0
	addq.l	#2,d0
	move.l	d0,-$7966(a4)
	clr.l	-(a7)
	move.l	-$7966(a4),-(a7)
	jsr	L602(pc)
	addq.w	#8,a7
	move.l	d0,-$7962(a4)
	bne.s	L497
L496	movem.l	(a7)+,d4-d5/a2-a3
	unlk	a5
	rts	

L497	move.b	(a3),d0
	ext.w	d0
	ext.l	d0
	move.l	d0,-(a7)
	movea.l	a3,a0
	addq.l	#1,a0
	move.l	a0,-(a7)
	move.l	-$7962(a4),-(a7)
	jsr	L514(pc)
	lea	$000c(a7),a7
	pea	L513(pc)
	move.b	(a3),d0
	ext.w	d0
	ext.l	d0
	add.l	-$7962(a4),d0
	move.l	d0,-(a7)
	jsr	L523(pc)
	addq.w	#8,a7
	move.l	$000c(a5),-(a7)
	move.l	a2,-(a7)
	move.l	-$7962(a4),-(a7)
	jsr	L519(pc)
	lea	$000c(a7),a7
	clr.l	-$796a(a4)
	movea.l	-$7962(a4),a3
	movea.l	a3,a2
L498	move.b	(a3),d0
	ext.w	d0
	ext.l	d0
	move.l	d0,d5
	cmp.l	#$00000020,d0
	beq.s	L499
	cmp.l	#$00000009,d5
	beq.s	L499
	cmp.l	#$0000000c,d5
	beq.s	L499
	cmp.l	#$0000000d,d5
	beq.s	L499
	cmp.l	#$0000000a,d5
	bne.s	L500
L499	addq.l	#1,a3
	bra.s	L498

L500	cmpi.b	#$20,(a3)
	blt.l	L509
	cmpi.b	#$22,(a3)
	bne.s	L505
	addq.l	#1,a3
L501	movea.l	a3,a0
	addq.l	#1,a3
	move.b	(a0),d0
	ext.w	d0
	ext.l	d0
	move.l	d0,d5
	beq.s	L504
	movea.l	a2,a0
	addq.l	#1,a2
	move.b	d5,(a0)
	cmp.l	#$00000022,d5
	bne.s	L503
	cmpi.b	#$22,(a3)
	bne.s	L502
	addq.l	#1,a3
	bra.s	L503

L502	clr.b	-$0001(a2)
	bra.s	L504

L503	bra.s	L501

L504	bra.s	L507

L505	movea.l	a3,a0
	addq.l	#1,a3
	move.b	(a0),d0
	ext.w	d0
	ext.l	d0
	move.l	d0,d5
	beq.s	L506
	cmp.l	#$00000020,d5
	beq.s	L506
	cmp.l	#$00000009,d5
	beq.s	L506
	cmp.l	#$0000000c,d5
	beq.s	L506
	cmp.l	#$0000000d,d5
	beq.s	L506
	cmp.l	#$0000000a,d5
	beq.s	L506
	movea.l	a2,a0
	addq.l	#1,a2
	move.b	d5,(a0)
	bra.s	L505

L506	movea.l	a2,a0
	addq.l	#1,a2
	clr.b	(a0)
L507	tst.l	d5
	bne.s	L508
	subq.l	#1,a3
L508	addq.l	#1,-$796a(a4)
	bra.l	L498

L509	clr.b	(a2)
	clr.l	-(a7)
	move.l	-$796a(a4),d0
	addq.l	#1,d0
	asl.l	#2,d0
	move.l	d0,-(a7)
	jsr	L602(pc)
	addq.w	#8,a7
	move.l	d0,-$796e(a4)
	bne.s	L510
	clr.l	-$796a(a4)
	bra.l	L496

L510	moveq	#0,d5
	movea.l	-$7962(a4),a3
	bra.s	L512

L511	move.l	d5,d0
	asl.l	#2,d0
	movea.l	-$796e(a4),a0
	move.l	a3,0(a0,d0.l)
	move.l	a3,-(a7)
	jsr	L547(pc)
	addq.w	#4,a7
	addq.l	#1,d0
	adda.l	d0,a3
	addq.l	#1,d5
L512	cmp.l	-$796a(a4),d5
	blt.s	L511
	move.l	d5,d0
	asl.l	#2,d0
	movea.l	-$796e(a4),a0
	clr.l	0(a0,d0.l)
	bra.l	L496

L513	dc.b	$20,0

L514	movem.l	$0004(a7),a0-a1
	move.l	a0,d0
	move.l	$000c(a7),d1
	bra.s	L516

L515	move.b	(a1)+,(a0)+
L516	dbeq	d1,L515
	beq.s	L518
	addq.w	#1,d1
	bra.s	L518

L517	clr.b	(a0)+
L518	dbf	d1,L517
	rts	

	dc.b	$30,$3c,$7f,$ff,$60,4

L519	move.w	$000e(a7),d0
	movea.l	$0004(a7),a0
L520	tst.b	(a0)+
	bne.s	L520
	subq.w	#1,a0
	movea.l	$0008(a7),a1
	subq.w	#1,d0
L521	move.b	(a1)+,(a0)+
	dbeq	d0,L521
	beq.s	L522
	clr.b	(a0)
L522	move.l	$0004(a7),d0
	rts	

L523	movea.l	$0004(a7),a0
	move.l	a0,d0
	movea.l	$0008(a7),a1
L524	move.b	(a1)+,(a0)+
	bne.s	L524
	rts	

L525	link	a5,#$0000
	movem.l	d4-d6/a2-a3,-(a7)
	movea.l	$0008(a5),a2
	clr.l	-(a7)
	pea	L530(pc)
	jsr	L615(pc)
	addq.w	#8,a7
	move.l	d0,-$787a(a4)
	bne.s	L527
L526	movem.l	(a7)+,d4-d6/a2-a3
	unlk	a5
	rts	

L527	movea.l	$000c(a5),a0
	movea.l	$0024(a0),a1
	move.l	$0004(a1),-(a7)
	jsr	L631(pc)
	addq.w	#4,a7
	move.l	d0,d4
	beq.s	L529
	pea	L531(pc)
	movea.l	d4,a0
	move.l	$0036(a0),-(a7)
	jsr	L629(pc)
	addq.w	#8,a7
	movea.l	d0,a3
	tst.l	d0
	beq.s	L528
	dc.b	$48,$78,3,$ed
				;	pea	($03ed).w
	move.l	a3,-(a7)
	jsr	L575(pc)
	addq.w	#8,a7
	move.l	d0,d6
	beq.s	L528
	move.l	d6,d0
	asl.l	#2,d0
	move.l	d0,d5
	movea.l	d5,a0
	move.l	$0008(a0),$00a4(a2)
	move.l	d6,$009c(a2)
	dc.b	$48,$78,3,$ed
				;	pea	($03ed).w
	pea	L532(pc)
	jsr	L575(pc)
	addq.w	#8,a7
	move.l	d0,$00a0(a2)
L528	move.l	d4,-(a7)
	jsr	L630(pc)
	addq.w	#4,a7
L529	move.l	-$787a(a4),-(a7)
	jsr	L582(pc)
	addq.w	#4,a7
	clr.l	-$787a(a4)
	bra.s	L526

L530	dc.b	$69,$63,$6f,$6e,$2e,$6c
	dc.b	$69,$62,$72,$61,$72,$79
	dc.b	0
L531	dc.b	$57,$49,$4e,$44,$4f,$57
	dc.b	0
L532	dc.b	$2a,0

L533	movem.l	d1/d4,-(a7)
	clr.l	d4
	tst.l	d0
	bpl.s	L534
	neg.l	d0
	addq.w	#1,d4
L534	tst.l	d1
	bpl.s	L535
	neg.l	d1
	eori.w	#$0001,d4
L535	bsr.s	L543
L536	tst.w	d4
	beq.s	L537
	neg.l	d0
L537	movem.l	(a7)+,d1/d4
	tst.l	d0
	rts	

L538	movem.l	d1/d4,-(a7)
	clr.l	d4
	tst.l	d0
	bpl.s	L539
	neg.l	d0
	addq.w	#1,d4
L539	tst.l	d1
	bpl.s	L540
	neg.l	d1
L540	bsr.s	L543
	move.l	d1,d0
	bra.s	L536

L541	move.l	d1,-(a7)
	bsr.s	L543
	move.l	d1,d0
	move.l	(a7)+,d1
	tst.l	d0
	rts	

L542	move.l	d1,-(a7)
	bsr.s	L543
	move.l	(a7)+,d1
	tst.l	d0
	rts	

L543	movem.l	d2-d3,-(a7)
	swap	d1
	tst.w	d1
	bne.s	L544
	swap	d1
	move.w	d1,d3
	move.w	d0,d2
	clr.w	d0
	swap	d0
	divu	d3,d0
	move.l	d0,d1
	swap	d0
	move.w	d2,d1
	divu	d3,d1
	move.w	d1,d0
	clr.w	d1
	swap	d1
	movem.l	(a7)+,d2-d3
	rts	

L544	swap	d1
	move.l	d1,d3
	move.l	d0,d1
	clr.w	d1
	swap	d1
	swap	d0
	clr.w	d0
	moveq	#$0f,d2
L545	add.l	d0,d0
	addx.l	d1,d1
	cmp.l	d1,d3
	bhi.s	L546
	sub.l	d3,d1
	addq.w	#1,d0
L546	dbf	d2,L545
	movem.l	(a7)+,d2-d3
	rts	

L547	movea.l	$0004(a7),a0
	move.l	a0,d0
L548	tst.b	(a0)+
	bne.s	L548
	suba.l	d0,a0
	move.l	a0,d0
	subq.l	#1,d0
	rts	

L549	link	a5,#$0000
	tst.l	-$7876(a4)
	beq.s	L550
	movea.l	-$7876(a4),a0
	jsr	(a0)
L550	move.l	$0008(a5),-(a7)
	jsr	L551(pc)
	addq.w	#4,a7
	unlk	a5
	rts	

L551	link	a5,#-$0004
	move.l	d4,-(a7)
	move.l	$0008(a5),-$0004(a5)
	tst.l	-$797e(a4)
	beq.s	L554
	moveq	#0,d4
	bra.s	L553

L552	move.l	d4,-(a7)
	jsr	L564(pc)
	addq.w	#4,a7
	addq.l	#1,d4
L553	move.w	-$798e(a4),d0
	ext.l	d0
	cmp.l	d0,d4
	blt.s	L552
	move.w	-$798e(a4),d0
	muls	#$0006,d0
	move.l	d0,-(a7)
	move.l	-$797e(a4),-(a7)
	jsr	L608(pc)
	addq.w	#8,a7
L554	tst.l	-$7872(a4)
	beq.s	L555
	movea.l	-$7872(a4),a0
	jsr	(a0)
L555	tst.l	-$795e(a4)
	beq.s	L556
	move.l	-$795e(a4),-(a7)
	jsr	L583(pc)
	addq.w	#4,a7
L556	tst.l	-$795a(a4)
	beq.s	L557
	move.l	-$795a(a4),-(a7)
	jsr	L583(pc)
	addq.w	#4,a7
L557	tst.l	-$7956(a4)
	beq.s	L558
	move.l	-$7956(a4),-(a7)
	jsr	L583(pc)
	addq.w	#4,a7
L558	dc.b	$2c,$78,0,4
				;	movea.l	($0004).w,a6
	btst	#$04,$0129(a6)
	beq.s	L560
	move.l	a5,-(a7)
	lea	L559(pc),a5
	jsr	-$001e(a6)
	movea.l	(a7)+,a5
	bra.s	L560

L559	dc.b	$42,$a7,$f3,$5f,$4e,$73

L560	tst.l	-$7972(a4)
	bne.s	L562
	tst.l	-$7962(a4)
	beq.s	L561
	move.l	-$7966(a4),-(a7)
	move.l	-$7962(a4),-(a7)
	jsr	L608(pc)
	addq.w	#8,a7
	move.l	-$796a(a4),d0
	addq.l	#1,d0
	asl.l	#2,d0
	move.l	d0,-(a7)
	move.l	-$796e(a4),-(a7)
	jsr	L608(pc)
	addq.w	#8,a7
L561	bra.s	L563

L562	jsr	L606(pc)
	move.l	-$7972(a4),-(a7)
	jsr	L618(pc)
	addq.w	#4,a7
L563	move.l	-$0004(a5),d0
	movea.l	-$7986(a4),a7
	rts	

	dc.b	$28,$1f,$4e,$5d,$4e,$75

L564	link	a5,#$0000
	movem.l	d4-d6/a2,-(a7)
	move.l	$0008(a5),d4
	moveq	#$06,d1
	move.l	d4,d0
	jsr	L569(pc)
	movea.l	d0,a2
	adda.l	-$797e(a4),a2
	tst.l	d4
	blt.s	L565
	move.w	-$798e(a4),d0
	ext.l	d0
	cmp.l	d0,d4
	bge.s	L565
	tst.l	(a2)
	bne.s	L567
L565	move.l	#$00000002,-$7952(a4)
	moveq	#-1,d0
L566	movem.l	(a7)+,d4-d6/a2
	unlk	a5
	rts	

L567	move.w	$0004(a2),d0
	and.w	#-$8000,d0
	bne.s	L568
	move.l	(a2),-(a7)
	jsr	L571(pc)
	addq.w	#4,a7
L568	clr.l	(a2)
	moveq	#0,d0
	bra.s	L566

L569	movem.l	d1-d3,-(a7)
	move.w	d1,d2
	mulu	d0,d2
	move.l	d1,d3
	swap	d3
	mulu	d0,d3
	swap	d3
	clr.w	d3
	add.l	d3,d2
	swap	d0
	mulu	d1,d0
	swap	d0
	clr.w	d0
	add.l	d2,d0
	movem.l	(a7)+,d1-d3
	rts	

L570	jmp	L571(pc)

L571	move.l	$0004(a7),d1
	movea.l	-$798a(a4),a6
	jmp	-$0024(a6)

L572	move.l	$0004(a7),d1
	movea.l	-$798a(a4),a6
	jmp	-$007e(a6)

L573	movea.l	-$798a(a4),a6
	jmp	-$0036(a6)

E574	jmp	L575(pc)

L575	movem.l	$0004(a7),d1-d2
	movea.l	-$798a(a4),a6
	jmp	-$001e(a6)

L576	jmp	L577(pc)

L577	movea.l	-$798a(a4),a6
	jmp	-$003c(a6)

L578	jmp	L579(pc)

L579	movem.l	$0004(a7),d1-d3
	movea.l	-$798a(a4),a6
	jmp	-$0030(a6)

L580	movem.l	d7/a5,-(a7)
	movem.l	$000c(a7),d7/a5
	movea.l	-$7982(a4),a6
	jsr	-$006c(a6)
	movem.l	(a7)+,d7/a5
	rts	

L581	movea.l	$0004(a7),a1
	movea.l	-$7982(a4),a6
	jmp	-$01c2(a6)

L582	jmp	L583(pc)

L583	movea.l	$0004(a7),a1
	movea.l	-$7982(a4),a6
	jmp	-$019e(a6)

L584	link	a5,#$0000
	movem.l	d4/a2,-(a7)
	dc.b	$48,$78,$ff,$ff
				;	pea	($ffffffff).w
	jsr	L593(pc)
	addq.w	#4,a7
	move.l	d0,d4
	cmp.l	#-$00000001,d0
	bne.s	L586
	moveq	#0,d0
L585	movem.l	(a7)+,d4/a2
	unlk	a5
	rts	

L586	pea	$00010001
	dc.b	$48,$78,0,$22
				;	pea	($0022).w
	jsr	L601(pc)
	addq.w	#8,a7
	movea.l	d0,a2
	tst.l	d0
	bne.s	L587
	move.l	d4,-(a7)
	jsr	L609(pc)
	addq.w	#4,a7
	moveq	#0,d0
	bra.s	L585

L587	move.l	$0008(a5),$000a(a2)
	move.b	$000f(a5),$0009(a2)
	move.b	#$04,$0008(a2)
	clr.b	$000e(a2)
	move.b	d4,$000f(a2)
	clr.l	-(a7)
	jsr	L604(pc)
	addq.w	#4,a7
	move.l	d0,$0010(a2)
	tst.l	$0008(a5)
	beq.s	L588
	move.l	a2,-(a7)
	jsr	L592(pc)
	addq.w	#4,a7
	bra.s	L589

L588	pea	$0014(a2)
	jsr	L612(pc)
	addq.w	#4,a7
L589	move.l	a2,d0
	bra.s	L585

L590	link	a5,#$0000
	move.l	a2,-(a7)
	movea.l	$0008(a5),a2
	tst.l	$000a(a2)
	beq.s	L591
	move.l	a2,-(a7)
	jsr	L616(pc)
	addq.w	#4,a7
L591	move.b	#-1,$0008(a2)
	move.l	#-$00000001,$0014(a2)
	moveq	#0,d0
	move.b	$000f(a2),d0
	move.l	d0,-(a7)
	jsr	L609(pc)
	addq.w	#4,a7
	dc.b	$48,$78,0,$22
				;	pea	($0022).w
	move.l	a2,-(a7)
	jsr	L607(pc)
	addq.w	#8,a7
	movea.l	(a7)+,a2
	unlk	a5
	rts	

L592	movea.l	$0004(a7),a1
	movea.l	-$7982(a4),a6
	jmp	-$0162(a6)

L593	move.l	$0004(a7),d0
	movea.l	-$7982(a4),a6
	jmp	-$014a(a6)

L594	link	a5,#$0000
	move.l	a2,-(a7)
	tst.l	$0008(a5)
	bne.s	L596
	moveq	#0,d0
L595	movea.l	(a7)+,a2
	unlk	a5
	rts	

L596	pea	$00010001
	move.l	$000c(a5),-(a7)
	jsr	L601(pc)
	addq.w	#8,a7
	movea.l	d0,a2
	tst.l	d0
	bne.s	L597
	moveq	#0,d0
	bra.s	L595

L597	move.b	#$05,$0008(a2)
	move.w	$000e(a5),$0012(a2)
	move.l	$0008(a5),$000e(a2)
	move.l	a2,d0
	bra.s	L595

L598	link	a5,#$0000
	move.l	a2,-(a7)
	movea.l	$0008(a5),a2
	move.l	a2,d0
	bne.s	L600
L599	movea.l	(a7)+,a2
	unlk	a5
	rts	

L600	move.b	#-1,$0008(a2)
	move.l	#-$00000001,$0014(a2)
	move.l	#-$00000001,$0018(a2)
	moveq	#0,d0
	move.w	$0012(a2),d0
	move.l	d0,-(a7)
	move.l	a2,-(a7)
	jsr	L607(pc)
	addq.w	#8,a7
	bra.s	L599

L601	jmp	L602(pc)

L602	movem.l	$0004(a7),d0-d1
	movea.l	-$7982(a4),a6
	jmp	-$00c6(a6)

L603	movem.l	d6-d7,-(a7)
	movea.l	$000c(a7),a1
	movea.l	-$7982(a4),a6
	jsr	-$01c8(a6)
	movem.l	(a7)+,d6-d7
	rts	

L604	jmp	L605(pc)

L605	movea.l	$0004(a7),a1
	movea.l	-$7982(a4),a6
	jmp	-$0126(a6)

L606	movea.l	-$7982(a4),a6
	jmp	-$0084(a6)

L607	jmp	L608(pc)

L608	movea.l	$0004(a7),a1
	move.l	$0008(a7),d0
	movea.l	-$7982(a4),a6
	jmp	-$00d2(a6)

L609	move.l	$0004(a7),d0
	movea.l	-$7982(a4),a6
	jmp	-$0150(a6)

L610	jmp	L611(pc)

L611	movea.l	$0004(a7),a0
	movea.l	-$7982(a4),a6
	jmp	-$0174(a6)

L612	movea.l	$0004(a7),a0
	move.l	a0,(a0)
	addq.l	#4,(a0)
	clr.l	$0004(a0)
	move.l	a0,$0008(a0)
	rts	

L613	movea.l	$0004(a7),a0
	movem.l	$0008(a7),d0/a1
	move.l	$0010(a7),d1
	movea.l	-$7982(a4),a6
	jmp	-$01bc(a6)

L614	jmp	L615(pc)

L615	movea.l	-$7982(a4),a6
	movea.l	$0004(a7),a1
	move.l	$0008(a7),d0
	jmp	-$0228(a6)

L616	movea.l	$0004(a7),a1
	movea.l	-$7982(a4),a6
	jmp	-$0168(a6)

L617	jmp	L618(pc)

L618	movea.l	$0004(a7),a1
	movea.l	-$7982(a4),a6
	jmp	-$017a(a6)

L619	move.l	$0004(a7),d0
	movea.l	-$7982(a4),a6
	jmp	-$013e(a6)

L620	movea.l	$0004(a7),a0
	movea.l	-$7982(a4),a6
	jmp	-$0180(a6)

L621	movea.l	$0004(a7),a1
	movea.l	-$7932(a4),a6
	jmp	-$004e(a6)

L622	movea.l	$0004(a7),a1
	movem.l	$0008(a7),d0-d1
	movea.l	-$7932(a4),a6
	jmp	-$00f6(a6)

L623	movem.l	$0004(a7),a0-a1
	move.l	$000c(a7),d0
	movea.l	-$7932(a4),a6
	jmp	-$00c0(a6)

L624	movea.l	$0004(a7),a1
	movem.l	$0008(a7),d0-d1
	movea.l	-$7932(a4),a6
	jmp	-$00f0(a6)

L625	movea.l	$0004(a7),a1
	move.l	$0008(a7),d0
	movea.l	-$7932(a4),a6
	jmp	-$0156(a6)

L626	movea.l	$0004(a7),a1
	move.l	$0008(a7),d0
	movea.l	-$7932(a4),a6
	jmp	-$015c(a6)

E627	movea.l	$0004(a7),a1
	move.l	$0008(a7),d0
	movea.l	-$7932(a4),a6
	jmp	-$00ea(a6)

L628	movea.l	$0004(a7),a1
	movea.l	$0008(a7),a0
	move.l	$000c(a7),d0
	movea.l	-$7932(a4),a6
	move.l	d7,-(a7)
	jsr	-$003c(a6)
	move.l	(a7)+,d7
	rts	

L629	movem.l	$0004(a7),a0-a1
	movea.l	-$787a(a4),a6
	jmp	-$0060(a6)

L630	movea.l	$0004(a7),a0
	movea.l	-$787a(a4),a6
	jmp	-$005a(a6)

L631	movea.l	$0004(a7),a0
	movea.l	-$787a(a4),a6
	jmp	-$004e(a6)

L632	movea.l	$0004(a7),a0
	movea.l	-$7936(a4),a6
	jmp	-$0036(a6)

L633	movea.l	$0004(a7),a0
	movea.l	-$7936(a4),a6
	jmp	-$0042(a6)

L634	movea.l	$0004(a7),a0
	movea.l	-$7936(a4),a6
	jmp	-$0048(a6)

L635	movea.l	$0004(a7),a0
	movea.l	-$7936(a4),a6
	jmp	-$0060(a6)

L636	movea.l	$0004(a7),a0
	movea.l	-$7936(a4),a6
	jmp	-$00c6(a6)

L637	movea.l	$0004(a7),a0
	movea.l	-$7936(a4),a6
	jmp	-$00cc(a6)

E638	movem.l	$0004(a7),a0-a1
	movea.l	-$7936(a4),a6
	jmp	-$00f0(a6)

L639	movem.l	$0004(a7),a0-a1
	movea.l	-$7936(a4),a6
	jmp	-$0108(a6)

L640	movea.l	$0004(a7),a0
	movea.l	-$792e(a4),a6
	jmp	-$001e(a6)

L641	move.l	a2,-(a7)
	movem.l	$0008(a7),a0-a1
	movem.l	$0010(a7),d1/a2
	movea.l	-$7916(a4),a6
	jsr	-$0030(a6)
	movea.l	(a7)+,a2
	rts	

	dc.b	0,0


	SECTION	segment1,DATA
seg1
L642	dc.l	L1
	dc.b	0,$0b,0,0,0,5
	dc.b	$05,$5f,0,0,0,$da
	dc.b	0,0,0,0,2,$80
	dc.b	1,$90,0,2,0,$01
	dc.b	$80,$04,0,$0f
	dc.l	L642
	dc.l	L2
	dcb.b	12,0
	dc.b	2,$80,1,$90,0,$01
	dc.b	0,0,$05,$58,0,$02
	dc.b	$19
	dcb.b	21,0
	dc.b	2,$80,1,$90,2,$80
	dc.b	1,$90,0,$0f
L643	dc.b	0,1,0,0,0,4
	dc.b	0,1,0,0,0,0
	dc.l	L3
	dc.b	0,0,0,0
L644	dc.b	0,1,0,0,0,4
	dc.b	0,1,0,0,0,0
	dc.l	L4
	dc.b	0,0,0,0
L645	dc.b	0,1,0,0,0,4
	dc.b	0,1,0,0,0,0
	dc.l	L5
	dcb.b	4,0
L646	dcb.b	7,0
	dc.b	$0a,0,$aa,0,$0d,0
	dc.b	$52,0,0,0,0
	dc.l	L644
	dc.b	0,0,0,0,$4f
	dcb.b	7,0
L647	dc.l	L646
	dc.b	0,0,0,$17,0,$aa
	dc.b	0,$0d,0,$52,0,0
	dc.b	0,0
	dc.l	L645
	dc.b	0,0,0,0,$43
	dcb.b	7,0
L648	dc.l	L647
	dc.b	0,0,0,$24,0,$aa
	dc.b	0,$0d,0,$56,0,0
	dc.b	0,0
	dc.l	L643
	dc.b	0,0,0,0,$51
	dcb.b	7,0
L649	dcb.b	1,0
	dc.b	1,0,0,0,$04,0
	dc.b	1,0,0,0,0
	dc.l	L6
	dc.b	0,0,0,0
L650	dc.b	0,1,0,0,0,4
	dc.b	0,1,0,0,0,0
	dc.l	L7
	dc.b	0,0,0,0
L651	dc.b	0,1,0,0,0,4
	dc.b	0,1,0,0,0,0
	dc.l	L8
	dc.b	0,0,0,0
L652	dc.b	0,1,0,0,0,4
	dc.b	0,1,0,0,0,0
	dc.l	L9
	dc.b	0,0,0,0
L653	dc.b	0,1,0,0,0,4
	dc.b	0,1,0,0,0,0
	dc.l	L10
	dc.b	0,0,0,0
L654	dc.b	0,1,0,0,0,4
	dc.b	0,1,0,0,0,0
	dc.l	L11
	dc.b	0,0,0,0
L655	dc.b	0,1,0,0,0,4
	dc.b	0,1,0,0,0,0
	dc.l	L12
	dcb.b	4,0
L656	dcb.b	9,0
	dc.b	$c8,0,$0d,0,$52,0
	dc.b	0,0,0
	dc.l	L649
	dcb.b	12,0
L657	dc.l	L656
	dc.b	0,0,0,$0d,0,$c8
	dc.b	0,$0d,0,$52,0,0
	dc.b	0,0
	dc.l	L650
	dcb.b	12,0
L658	dc.l	L657
	dc.b	0,0,0,$1a,0,$c8
	dc.b	0,$0d,0,$52,0,0
	dc.b	0,0
	dc.l	L651
	dcb.b	12,0
L659	dc.l	L658
	dc.b	0,0,0,$27,0,$c8
	dc.b	0,$0d,0,$43,0,0
	dc.b	0,0
	dc.l	L652
	dcb.b	12,0
L660	dc.l	L659
	dc.b	0,0,0,$34,0,$c8
	dc.b	0,$0d,0,$52,0,0
	dc.b	0,0
	dc.l	L653
	dcb.b	12,0
L661	dc.l	L660
	dc.b	0,0,0,$41,0,$c8
	dc.b	0,$0d,0,$56,0,0
	dc.b	0,0
	dc.l	L654
	dc.b	0,0,0,0,$48
	dcb.b	7,0
L662	dc.l	L661
	dc.b	0,0,0,$4e,0,$c8
	dc.b	0,$0d,0,$43,0,0
	dc.b	0,0
	dc.l	L655
	dcb.b	12,0
L663	dcb.b	1,0
	dc.b	1,0,0,0,$12,0
	dc.b	1,0,0,0,0
	dc.l	L13
	dc.b	0,0,0,0
L664	dc.b	0,1,0,0,0,$12
	dc.b	0,1,0,0,0,0
	dc.l	L14
	dc.b	0,0,0,0
L665	dc.b	0,1,0,0,0,$12
	dc.b	0,1,0,0,0,0
	dc.l	L15
	dc.b	0,0,0,0
L666	dc.b	0,1,0,0,0,$12
	dc.b	0,1,0,0,0,0
	dc.l	L16
	dc.b	0,0,0,0
L667	dc.b	0,1,0,0,0,$12
	dc.b	0,1,0,0,0,0
	dc.l	L17
	dcb.b	4,0
L668	dcb.b	7,0
	dc.b	$34,0,$b4,0,$0d,0
	dc.b	$52,0,0,0,0
	dc.l	L663
	dcb.b	12,0
L669	dc.l	L668
	dc.b	0,0,0,0,0,$b4
	dc.b	0,$0d,0,$52,0,0
	dc.b	0,0
	dc.l	L664
	dcb.b	12,0
L670	dc.l	L669
	dc.b	0,0,0,$0d,0,$b4
	dc.b	0,$0d,0,$52,0,0
	dc.b	0,0
	dc.l	L665
	dcb.b	12,0
L671	dc.l	L670
	dc.b	0,0,0,$1a,0,$b4
	dc.b	0,$0d,0,$43,0,0
	dc.b	0,0
	dc.l	L666
	dcb.b	12,0
L672	dc.l	L671
	dc.b	0,0,0,$27,0,$b4
	dc.b	0,$0d,0,$43,0,0
	dc.b	0,0
	dc.l	L667
	dcb.b	12,0
L673	dcb.b	5,0
	dc.b	$72,0,0,0,$36,0
	dc.b	$0a,1,$01
	dc.l	L18
	dc.l	L672
	dcb.b	8,0
L674	dc.l	L673
	dc.b	0,$32,0,0,0,$36
	dc.b	0,$0a,1,$01
	dc.l	L19
	dc.l	L662
	dcb.b	8,0
	dc.l	L674
	dc.b	0,2,0,0,0,$26
	dc.b	0,$0a,1,$01
	dc.l	L20
	dc.l	L648
	dcb.b	8,0
L675	dc.b	1,2,0,0,0,$12
	dc.b	0,3
	dcb.b	12,0
L676	dc.b	1,2,0,0,0,$12
	dc.b	0,3,0,0,0,0
	dc.l	L21
	dc.b	0,0,0,0
L677	dc.b	1,2,0,0,0,5
	dc.b	0,3,0,0,0,0
	dc.l	L22
	dcb.b	4,0
L678	dcb.b	5,0
	dc.b	$bd,0,0,0,$bd,0
	dc.b	$31,0,0,0,$31
	dcb.b	4,0
L679	dcb.b	5,0
	dc.b	$37,0,0,0,$37,0
	dc.b	$0e,0,0,0,$0e,0
	dc.b	0,0,0
L680	dc.b	0,$09,0,$0f,0,$b2
	dc.b	0,$0f,0,$b2,0,$1b
	dc.b	0,$09,0,$1b,0,$09
	dc.b	0,$0f
L681	dc.b	0,0,0,0,3,0
	dc.b	0,5
	dc.l	L680
	dcb.b	4,0
L682	dcb.b	4,0
	dc.b	3,0,0,5
	dc.l	L678
	dc.l	L681
L683	dc.b	0,0,0,0,3,0
	dc.b	0,5
	dc.l	L679
	dcb.b	4,0
L684	dcb.b	5,0
	dc.b	$0a,0,$20,0,$32,0
	dc.b	$0c,0,0,0,$05,$10
	dc.b	$01
	dc.l	L683
	dc.b	0,0,0,0
	dc.l	L676
	dcb.b	14,0
L685	dc.l	L684
	dc.b	0,$7d,0,$20,0,$32
	dc.b	0,$0c,0,0,0,5
	dc.b	$10,$01
	dc.l	L683
	dc.b	0,0,0,0
	dc.l	L677
	dcb.b	14,0
L686	dc.l	L693
	dcb.b	7,0
	dc.b	$1c
	dcb.b	24,0
L687	dc.l	L685
	dc.b	0,$0a,0,$10,0,$aa
	dc.b	0,$0b,0,0,0,5
	dc.b	$10,4
	dcb.b	16,0
	dc.l	L686
	dcb.b	11,0
	dc.b	$14,0,$1e,0,$be,0
	dc.b	$32,0,0,0,0
	dc.l	L687
	dc.l	L682
	dc.l	L675
	dc.b	0,0,$02
	dcb.b	82,0
	dc.b	$38,0,$18,0,$06,0
	dc.b	4
	dc.l	L692
	dc.b	2,$10,1,$29,0,6
	dc.b	0,3
	dc.l	L688
	dc.b	2,$10,1,$34,0,6
	dc.b	0,3
	dc.l	L689
	dc.b	2,$10,1,$3f,0,6
	dc.b	0,3
	dc.l	L690
	dc.b	2,$10,1,$4a,0,6
	dc.b	0,3
	dc.l	L691
	dc.b	2,$10,1,$55,0,6
	dc.b	0,3
	dc.l	L692
	dcb.b	50,$20
	dc.b	0,0
	dc.l	L389
	dc.l	L390
	dc.l	L391
	dc.l	L392
	dc.l	L393
	dc.l	L394
	dc.l	L395
	dc.l	L396
	dc.l	L397
	dc.l	L398
	dc.l	L399
	dc.l	L400
	dc.b	$1f,$1c,$1f,$1e,$1f,$1e
	dc.b	$1f,$1f,$1e,$1f,$1e,$1f
	dcb.b	21,0
	dc.b	$14,0,0
	ds.b	252
L688	ds.b	4
L689	ds.b	4
L690	ds.b	4
L691	ds.b	4
L692	ds.b	110
L693	ds.b	938



	SECTION	segment2,BSS
seg2
	ds.b	4


	END

