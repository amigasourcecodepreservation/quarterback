;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*******************************************************************
*                                                                 *
*                                                                 *
*     DSM MC68000 Disassembler Version 1.0d (09/01/88).           *
*     Copyright (C) 1987, 1988 by OTG Software.                   *
*     All rights reserved.                                        *
*                                                                 *
*     Disassembly of :  format                                    *
*                                                                 *
*                                                                 *
*******************************************************************


	SECTION	segment0,CODE
seg0
	move.l	a7,SaveSP
	move.l	d0,CmdCnt		;save CLI params
	move.l	a0,CmdPtr
	movea.l	4,a6
	move.l	a6,SysBase
	ZAPA	A1
	jsr	FindTask(a6)
	movea.l	d0,a4
	tst.l	$00ac(a4)
	beq.l	FromWB
	bsr.l	OpenDos
	ZAPA	a0
	move.l	$00ac(a4),d0
	lsl.l	#2,d0
	move.l	$10(a0,d0.l),d0
	lsl.l	#2,d0
	movem.l	a2-a3,-(a7)
	lea	TokenBuffer,a2		;token buffer
	lea	TokenTable,a3		;table of token ptrs
	movea.l	d0,a0
	ZAP	d0
	move.b	(a0)+,d0
	clr.b	0(a0,d0.l)
	move.l	a0,(a3)+
	move.l	CmdCnt,d0
	movea.l	CmdPtr,a0
	lea	0(a0,d0.l),a1
L1	cmpi.b	#Space,-(a1)	;scan backwards from end till space
	bls.s	L1
	clr.b	1(a1)
L2	move.b	(a0)+,d1
	beq.s	L10		;end of string
	cmpi.b	#Space,d1
	beq.s	L2		;ignore leading spaces
	cmpi.b	#Tab,d1
	beq.s	L2		;and tabs
	move.l	a2,(a3)+	;save ptr to token in token table
	cmpi.b	#'"',d1		;leading double quote?
	beq.s	L5		;yes
	move.b	d1,(a2)+	;else store char
L3	move.b	(a0)+,d1	;and get next
	beq.s	L10		;end of string
	cmpi.b	#Space,d1
	beq.s	L4		;space ends token
	move.b	d1,(a2)+	;else store char
	bra.s	L3		;and loop

L4	clr.b	(a2)+		;terminate token
	bra.s	L2

* Come here on double quote

L5	move.b	(a0)+,d1	;get next char
	beq.s	L10		;end of string
	cmpi.b	#'"',d1		;end of quote?
	beq.s	L4		;yes...terminate token
	cmpi.b	#'*',d1		;?
	bne.s	L9		;no
	move.b	(a0)+,d1	;get next char
	cmpi.b	#'N',d1
	beq.s	L6		;found *N
	cmpi.b	#'n',d1
	bne.s	L7		;not *n
L6	moveq	#LF,d1		;store line feed
	bra.s	L9

L7	cmpi.b	#'E',d1
	beq.s	L8		;found *E
	cmpi.b	#'e',d1
	bne.s	L9		;not *e
L8	moveq	#ESC,d1		;else store ESCAPE
L9	move.b	d1,(a2)+
	bra.s	L5		;loop for end of quoted string

L10	clr.b	(a2)		;terminate token buffer
	clr.l	(a3)		;terminate token table
	move.l	#CmdPtr,d0
	sub.l	a3,d0
	not.l	d0
	lsr.l	#2,d0
	movem.l	(a7)+,a2-a3
	pea	TokenTable
	move.l	d0,-(a7)
	jsr	Input
	move.l	d0,StdIn
	jsr	Output
	move.l	d0,StdOut
	move.l	d0,StdErr
	jsr	Main
	ZAP	d0
	movea.l	SaveSP,a7
	rts			;back to CLI

FromWB:
	bsr.l	OpenDos
	bsr.l	WaitMsg
	move.l	d0,WBmsg	;save ptr to WB msg
	move.l	d0,-(a7)
	clr.l	-(a7)
	movea.l	d0,a2
	move.l	$0024(a2),d0
	beq.s	L12
	movea.l	DosBase,a6
	movea.l	d0,a0
	move.l	0(a0),d1
	jsr	CurrentDir(a6)
L12	move.l	$0020(a2),d1
	beq.s	L13
	move.l	#$000003ed,d2
	jsr	Open(a6)
	move.l	d0,StdIn
	move.l	d0,StdOut
	move.l	d0,StdErr
	beq.s	L13
	lsl.l	#2,d0
	movea.l	d0,a0
	move.l	8(a0),$00a4(a4)
L13	jsr	Main
	ZAP	d0
	bra.s	L15

L14	move.l	$0004(a7),d0	;get return code from stack
L15	movea.l	SaveSP,a7
	move.l	d0,-(a7)	;save return code
	movea.l	4,a6
	move.l	DosBase,d0
	beq.s	L16
	movea.l	d0,a1
	jsr	CloseLibrary(a6)
L16	tst.l	WBmsg
	beq.s	L17
	jsr	Forbid(a6)
	movea.l	WBmsg,a1
	jsr	ReplyMsg(a6)
L17	move.l	(a7)+,d0	;restore return code
	rts			;back to workbench

Abort:	movem.l	d7/a5-a6,-(a7)
	move.l	#$00038007,d7
	movea.l	4,a6
	jsr	Alert(a6)
	movem.l	(a7)+,d7/a5-a6
	moveq	#$64,d0
	bra.s	L15

WaitMsg:
	lea	$005c(a4),a0
	jsr	WaitPort(a6)
	lea	$005c(a4),a0
	jsr	GetMsg(a6)
	rts	

OpenDos:
	lea	L21(pc),a1
	ZAP	d0
	jsr	OpenLibrary(a6)
	move.l	d0,DosBase
	beq.s	Abort
	rts	

L21	dc.b	'dos.library',0


	SECTION	segment1,DATA
seg1
	dc.b	0,$21,0,$01
SysBase	dcb.b	4,0
DosBase	dcb.b	8,0
StdIn	dcb.b	4,$ff
StdOut	dcb.b	4,$ff
StdErr	dcb.b	4,$ff
SaveSP	dcb.b	4,0
WBmsg	dcb.b	4,0
CmdCnt	dcb.b	4,0
CmdPtr	dcb.b	4,0
TokenTable	dcb.b	128,0
TokenBuffer	dcb.b	256,0


	SECTION	segment2,CODE
seg2
Main:	movem.l	d2-d6/a2-a5,-(a7)
	move.w	$002a(a7),d3
	movea.l	$002c(a7),a2
	clr.b	d2
	movea.l	#L127,a4
	move.l	#L128,d4
	movea.l	#L274,a3
	clr.l	-(a7)
	pea	Intui.
	jsr	OpenLib
	move.l	d0,IntuitionBase
	addq.l	#8,a7
	beq.l	OpnErr
	clr.l	-(a7)
	pea	Icon.
	jsr	OpenLib
	move.l	d0,IconBase
	addq.l	#8,a7
	beq.l	OpnErr
	clr.l	-(a7)
	pea	Graph.
	jsr	OpenLib
	move.l	d0,GraphicsBase
	addq.l	#8,a7
	beq.l	OpnErr
	clr.l	-(a7)
	pea	#36
	jsr	AllocMem
	move.l	d0,NameBuf
	addq.l	#8,a7
	beq.l	OpnErr
	clr.l	-(a7)
	pea	#260
	jsr	AllocMem
	move.l	d0,BlockBuf
	addq.l	#8,a7
	beq.l	OpnErr
	movea.l	DosBase,a0
	moveq	#$21,d0
	cmp.w	$0014(a0),d0
	bls.s	L35
OpnErr	jsr	L65
	pea	#20
	jsr	L14
	addq.l	#$4,a7
L35	tst.w	d3
	beq.s	L36
	movea.l	a2,a0
	move.l	(a0),d0
	bra.s	L37

L36	ZAP	d0
L37	move.l	d0,L126
	beq.l	L49
	moveq	#$06,d0
	cmp.w	d3,d0
	bne.s	L38
	pea	NoIcon.
	movea.l	a2,a0
	moveq	#$14,d2
	adda.l	d2,a0
	move.l	(a0),-(a7)
	jsr	(a3)
	tst.l	d0
	addq.l	#8,a7
	beq.s	L38
	move.w	#$0001,NoIconFlg		;no icons
	subq.w	#1,d3
L38	moveq	#$05,d0
	cmp.w	d3,d0
	beq.s	L40
L39	movea.l	a2,a0
	move.l	(a0),-(a7)
	pea	Usage.
	jsr	DispMsg
	jsr	L65
	pea	#20
	jsr	L14
	lea	12(a7),a7		;clean up stack
L40	pea	Drive1.
	movea.l	a2,a0
	addq.l	#$4,a0
	move.l	(a0),-(a7)
	jsr	(a3)
	tst.l	d0
	addq.l	#8,a7
	beq.s	L41
	movea.l	a2,a0
	addq.l	#8,a0
	movea.l	(a0),a4
	bra.s	L42

L41	pea	Drive2.
	movea.l	a2,a0
	moveq	#12,d2
	adda.l	d2,a0
	move.l	(a0),-(a7)
	jsr	(a3)
	tst.l	d0
	addq.l	#8,a7
	beq.s	L39
	movea.l	a2,a0
	moveq	#16,d2
	adda.l	d2,a0
	movea.l	(a0),a4
L42	move.l	a4,-(a7)
	jsr	L279
	move.l	a4,-(a7)
	jsr	StgLen		;calc length
	moveq	#30,d2
	cmp.l	d0,d2		;compare against 30
	addq.l	#8,a7
	blt.s	L39
	move.l	a4,-(a7)
	jsr	L71
	tst.l	d0
	addq.l	#$4,a7
	beq.l	L39
	pea	Name1.
	movea.l	a2,a0
	addq.l	#$4,a0
	move.l	(a0),-(a7)
	jsr	(a3)
	tst.l	d0
	addq.l	#8,a7
	beq.s	L43
	movea.l	a2,a0
	addq.l	#8,a0
	movea.l	d4,a5
	move.l	(a0),(a5)
	bra.s	L44

L43	pea	Name2.
	movea.l	a2,a0
	moveq	#12,d2
	adda.l	d2,a0
	move.l	(a0),-(a7)
	jsr	(a3)
	tst.l	d0
	addq.l	#8,a7
	beq.l	L39
	movea.l	a2,a0
	moveq	#16,d2
	adda.l	d2,a0
	movea.l	d4,a5
	move.l	(a0),(a5)
L44	movea.l	d4,a5
	movea.l	(a5),a4
	bra.s	L46

L45	cmpi.b	#$3a,(a4)
	beq.l	L39
	cmpi.b	#$2f,(a4)
	beq.l	L39
	addq.l	#1,a4
L46	tst.b	(a4)
	bne.s	L45
	movea.l	d4,a5
	move.l	(a5),-(a7)
	jsr	StgLen		;calc string length
	moveq	#1,d2
	cmp.l	d0,d2
	addq.l	#$4,a7
	bgt.l	L39
	movea.l	d4,a5
	move.l	(a5),-(a7)
	jsr	StgLen		;calc string length
	moveq	#30,d2
	cmp.l	d0,d2
	addq.l	#$4,a7
	blt.l	L39
	pea	L131
	pea	Insert.
	jsr	DispMsg
	addq.l	#8,a7
L47	pea	#1
	move.l	StdIn,-(a7)
	jsr	WaitForChar
	tst.l	d0
	addq.l	#8,a7
	beq.s	L48
	jsr	L308
	bra.s	L47

L48	jsr	L308
	jsr	L76
	move.b	d0,d2
	bra.l	L62

L49	movea.l	$0024(a2),a3
	tst.l	(a3)
	beq.s	L50
	move.l	(a3),-(a7)
	jsr	UnLock
	clr.l	(a3)
	addq.l	#$4,a7
L50	move.w	$001e(a2),d3
	subq.w	#1,d3
	bne.s	L52
L51	pea	($0048).w
	pea	($0140).w
	clr.l	-(a7)
	clr.l	-(a7)
	pea	L138
	clr.l	-(a7)
	pea	L162
	clr.l	-(a7)
	jsr	AutoRequest
	jsr	L65
	pea	#20
	jsr	L14
	lea	$0024(a7),a7
L52	movea.l	#Empty.,a0
	movea.l	d4,a5
	move.l	a0,(a5)
	addq.l	#8,a3
	bra.l	L61

L53	tst.l	(a3)
	beq.l	L58
	movea.l	$0004(a3),a0
	tst.b	(a0)
	bne.s	L51
	move.l	(a3),-(a7)
	jsr	ParentDir
	move.l	d0,d5
	addq.l	#$4,a7
	beq.s	L54
	move.l	d5,-(a7)
	jsr	UnLock
	addq.l	#$4,a7
	bra.s	L51

L54	jsr	IoErr
	tst.l	d0
	bne.s	L51
	move.l	NameBuf,-(a7)
	move.l	(a3),-(a7)
	jsr	Info
	tst.l	d0
	addq.l	#8,a7
	beq.l	L60
	move.l	BlockBuf,-(a7)
	move.l	(a3),-(a7)
	jsr	Examine
	tst.l	d0
	addq.l	#8,a7
	beq.l	L60
	movea.l	DosBase,a0
	movea.l	$0022(a0),a1
	move.l	$0018(a1),d6
	lsl.l	#2,d6
	movea.l	d6,a0
	move.l	$0004(a0),d6
	lsl.l	#2,d6
	move.l	d6,(a4)
	bra.s	L56

L55	movea.l	(a4),a0
	move.l	(a0),d6
	lsl.l	#2,d6
	move.l	d6,(a4)
L56	tst.l	(a4)
	beq.s	L57
	movea.l	(a4),a0
	tst.l	$0004(a0)
	bne.s	L55
	movea.l	NameBuf,a0
	move.l	$001c(a0),d6
	lsl.l	#2,d6
	movea.l	d6,a0
	movea.l	8(a0),a0
	movea.l	(a4),a1
	cmpa.l	8(a1),a0
	bne.s	L55
L57	tst.l	(a4)
	beq.l	L51
	movea.l	(a4),a1
	move.l	$0028(a1),d6
	lsl.l	#2,d6
	movea.l	d6,a0
	addq.l	#1,a0
	move.l	a0,-(a7)
	pea	L98
	pea	Drv.
	jsr	L306
	pea	Drv.
	pea	Drive3.
	pea	L131
	jsr	L306
	movea.l	BlockBuf,a1
	pea	8(a1)
	pea	L130
	jsr	MoveStg
	pea	($0048).w
	pea	($0140).w
	clr.l	-(a7)
	clr.l	-(a7)
	pea	L134
	pea	L136
	pea	L147
	clr.l	-(a7)
	jsr	AutoRequest
	tst.l	d0
	lea	$0040(a7),a7
	beq.l	L60
	move.l	(a3),-(a7)
	jsr	UnLock
	clr.l	(a3)
	addq.l	#$4,a7
	bra.l	L59

L58	move.l	$0004(a3),-(a7)
	jsr	L279
	move.l	$0004(a3),-(a7)
	jsr	L71
	tst.l	d0
	addq.l	#8,a7
	beq.l	L51
	pea	($0048).w
	pea	($0140).w
	clr.l	-(a7)
	clr.l	-(a7)
	pea	L134
	pea	L136
	pea	L167
	clr.l	-(a7)
	jsr	AutoRequest
	tst.l	d0
	lea	$0020(a7),a7
	beq.l	L60
	pea	($0048).w
	pea	($0140).w
	clr.l	-(a7)
	clr.l	-(a7)
	pea	L134
	pea	L136
	pea	L150
	clr.l	-(a7)
	jsr	AutoRequest
	tst.l	d0
	lea	$0020(a7),a7
	beq.l	L60
L59	jsr	L76
	move.b	d0,d2
L60	addq.l	#8,a3
	subq.w	#1,d3
L61	tst.w	d3
	bhi.l	L53
	tst.l	Window
	beq.s	L62
	move.l	Window,-(a7)
	jsr	CloseWindow
	addq.l	#$4,a7
L62	jsr	L65
	tst.b	d2
	beq.s	L63
	ZAP	d2
	bra.s	L64

L63	moveq	#$14,d2
L64	move.l	d2,-(a7)
	jsr	L14
	addq.l	#$4,a7
	movem.l	(a7)+,d2-d6/a2-a5
	rts	

L65	tst.l	BlockBuf
	beq.s	L66
	pea	($0104).w
	move.l	BlockBuf,-(a7)
	jsr	FreeMem
	addq.l	#8,a7
L66	tst.l	NameBuf
	beq.s	L67
	pea	($0024).w
	move.l	NameBuf,-(a7)
	jsr	FreeMem
	addq.l	#8,a7
L67	tst.l	IconBase
	beq.s	L68
	move.l	IconBase,-(a7)
	jsr	CloseLibrary
	addq.l	#$4,a7
L68	tst.l	IntuitionBase
	beq.s	L69
	move.l	IntuitionBase,-(a7)
	jsr	CloseLibrary
	addq.l	#$4,a7
L69	tst.l	GraphicsBase
	beq.s	L70
	move.l	GraphicsBase,-(a7)
	jsr	CloseLibrary
	addq.l	#$4,a7
L70	rts	

L71	movem.l	d2-d3/a2,-(a7)
	move.l	$0010(a7),d2
	movea.l	#L127,a2
	movea.l	DosBase,a0
	movea.l	$0022(a0),a0
	move.l	$0018(a0),d3
	lsl.l	#2,d3
	movea.l	d3,a0
	move.l	$0004(a0),d0
	lsl.l	#2,d0
	move.l	d0,(a2)
	bra.l	L74

L72	movea.l	(a2),a0
	tst.l	$0004(a0)
	bne.s	L73
	movea.l	(a2),a0
	move.l	$0028(a0),d3
	lsl.l	#2,d3
	movea.l	d3,a0
	addq.l	#1,a0
	move.l	a0,-(a7)
	move.l	d2,-(a7)
	jsr	L274
	tst.l	d0
	addq.l	#8,a7
	beq.s	L73
	movea.l	(a2),a0
	move.l	$0028(a0),d3
	lsl.l	#2,d3
	movea.l	d3,a0
	addq.l	#1,a0
	move.l	a0,-(a7)
	pea	L100
	pea	Drv.
	jsr	L306
	pea	Drv.
	pea	Drive4.
	pea	L131
	jsr	L306
	moveq	#1,d0
	lea	$0018(a7),a7
	bra.s	L75

L73	movea.l	(a2),a0
	move.l	(a0),d0
	lsl.l	#2,d0
	move.l	d0,(a2)
L74	tst.l	(a2)
	bne.s	L72
	ZAP	d0
L75	movem.l	(a7)+,d2-d3/a2
	rts	

L76	movem.l	d2-d4/a2,-(a7)
	movea.l	#L126,a2
	move.l	#L102,d3
	move.l	#TryDif.,d4
L77	jsr	L209
	move.b	d0,d2
	cmpi.b	#$1c,d2
	bne.s	L78
	pea	($0048).w
	pea	($0140).w
	clr.l	-(a7)
	clr.l	-(a7)
	pea	L134
	pea	L136
	pea	L172
	clr.l	-(a7)
	jsr	AutoRequest
	tst.l	d0
	lea	$0020(a7),a7
	bne.s	L77
L78	move.b	d2,d0
	ext.w	d0
	ext.l	d0
	exg	d0,a1
	cmpa.w	#-$0001,a1
	exg	d0,a1
	blt.l	L82
	bgt.s	L79
	bra.l	L81

L79	exg	d0,a1
	cmpa.w	#$0015,a1
	exg	d0,a1
	blt.s	L80
	exg	d0,a1
	cmpa.w	#$001e,a1
	exg	d0,a1
	bgt.s	L80
	movea.l	d0,a1
	lea	-$0015(a1),a1
	move.l	a1,d0
	movea.l	d0,a1
	adda.l	d0,a1
	move.l	a1,d0
	move.w	JumpTable(d0.w),d0
	jmp	JumpTable(d0.w)

JumpTable
	DC.W	JT1-JumpTable
	DC.W	JT2-JumpTable
	DC.W	JT3-JumpTable
	DC.W	JT4-JumpTable
	DC.W	JT5-JumpTable
	DC.W	JT6-JumpTable
	DC.W	JT7-JumpTable
	DC.W	L82-JumpTable
	DC.W	JT8-JumpTable
	DC.W	JT10-JumpTable

L80	exg	d0,a1
	cmpa.w	#$007b,a1
	exg	d0,a1
	blt.l	L82
	exg	d0,a1
	cmpa.w	#$007e,a1
	exg	d0,a1
	bgt.l	L82
	movea.l	d0,a1
	lea	-$007b(a1),a1
	move.l	a1,d0
	movea.l	d0,a1
	adda.l	d0,a1
	move.l	a1,d0
	move.w	JmpTbl2(d0.w),d0
	jmp	JmpTbl2(d0.w)

JmpTbl2
	DC.W	JT10-JmpTbl2
	DC.W	JT11-JmpTbl2
	DC.W	JT13-JmpTbl2
	DC.W	JT12-JmpTbl2

L81	move.l	#OpnErr.,d4
	bra.l	L83

JT1:	MOVE.L	#NoHdr.,D3
	BRA.L	L83
JT2:	MOVE.L	#BadPreamble.,D3
	BRA.L	L83
JT3:	MOVE.L	#BadID.,D3
	BRA.L	L83
JT4:	MOVE.L	BadHdrNbr.,D3
	BRA.L	L83
JT5:	MOVE.L	#BadSecNbr.,D3
	BRA.S	L83
JT6:	MOVE.L	#TooFew.,D3
	BRA.S	L83
JT7:	MOVE.L	#BadSecHdr.,D3
	BRA.S	L83
JT8:	MOVE.L	#DskChg.,D4
	BRA.S	L83
JT9:	MOVE.L	#SeekErr.,D3
	BRA.S	L83
JT10:	MOVE.L	#NoMem.,D4
	BRA.S	L83
JT11:	MOVE.L	#NoHandler.,D4
	BRA.S	L83
JT12:	MOVE.L	#NotDisk.,D4
	BRA.S	L83
JT13:	TST.L	(A2)
	BEQ.S	L82
	PEA	#Break.
	jsr	DispMsg
	addq.l	#$4,a7
L82:	move.b	d2,d0		;error code to D0
	ext.w	d0
	ext.l	d0
	bra.l	MainExit

L83	tst.l	(a2)
	beq.s	L84
	move.l	d4,-(a7)
	move.l	d3,-(a7)
	move.l	(a2),-(a7)
	pea	Failed.
	jsr	DispMsg
	lea	$0010(a7),a7
	bra.s	L85

L84	move.l	d3,-(a7)
	pea	Tmp1.
	jsr	MoveStg
	move.l	d4,-(a7)
	pea	Tmp2.
	jsr	MoveStg
	pea	($0048).w
	pea	($0140).w
	clr.l	-(a7)
	clr.l	-(a7)
	pea	L138
	clr.l	-(a7)
	pea	L142
	clr.l	-(a7)
	jsr	AutoRequest
	lea	$0030(a7),a7		;clean up stack
L85	move.b	d2,d0
	ext.w	d0
	ext.l	d0
MainExit:
	movem.l	(a7)+,d2-d4/a2
	rts				;return from Main


	SECTION	segment3,DATA
seg3
Intui.	dc.b	'intuition.library',0
Icon.	dc.b	'icon.library',0
	dc.b	0
Graph.	dc.b	'graphics.library',0
	dc.b	0
NoIcon.	dc.b	'NOICONS',0
Usage.	dc.b	'Usage: %s DRIVE <disk> NAME <name> [NOICONS]',$0a,0
Drive1.	dc.b	'DRIVE',0
Drive2.	dc.b	'DRIVE',0
Name1.	dc.b	'NAME',0
	dc.b	0
Name2.	dc.b	'NAME',0,0
Insert.	dc.b	'Insert disk to be initialized in %s and press RETURN',0
	dc.b	0
Empty.	dc.b	'Empty',0
L98	dc.b	'%s:',0
Drive3.	dc.b	'drive %s',0,0
L100	dc.b	'%s:',0
Drive4.	dc.b	'drive %s',0,0
L102	dc.b	0,0
TryDif.	dc.b	'try a different disk',0
	dc.b	0
OpnErr.	dc.b	'cannot open device',0
	dc.b	0
NoHdr.	dc.b	'no sector header',0
	dc.b	0
BadPreamble.	dc.b	'bad sector preamble',0
BadID.		dc.b	'bad sector id',0
BadHdrNbr.	dc.b	'bad header number',0
BadSecNbr.	dc.b	'bad sector number',0
TooFew.		dc.b	'too few sectors',0
BadSecHdr.	dc.b	'bad sector header',0
DskChg.		dc.b	'disk changed',0
		dc.b	0
SeekErr.	dc.b	'seek error',0
		dc.b	0
NoMem.		dc.b	'not enough memory',0
NoHandler.	dc.b	'cannot find handler',0
NotDisk.	dc.b	'not a disk',0
		dc.b	0
Break.		dc.b	'*** BREAK',LF,0,0
Failed.		dc.b	'%s failed - %s',LF,'%s',LF,0,0

IntuitionBase	dcb.b	4,0
IconBase	dcb.b	4,0
GraphicsBase	dcb.b	4,0
NameBuf		dcb.b	4,0
BlockBuf	dcb.b	4,0
Window	dcb.b	4,0
NoIconFlg	dcb.b	2,0
L126	dcb.b	4,0
L127	dcb.b	4,0
L128	dcb.b	4,0
Drv.	dcb.b	32,0
L130	dcb.b	32,0
L131	dcb.b	38,0
Tmp1.	dcb.b	26,0
Tmp2.	dcb.b	26,0
L134	dcb.b	1,0
	dc.b	1,1,0,0,$06,0
	dc.b	$03,0,0,0,0
	dc.l	Cancel.
	dc.b	0,0,0,0
Cancel.	dc.b	'Cancel',0,0
L136	dc.b	0,1,1,0,0,6
	dc.b	0,$03,0,0,0,0
	dc.l	Cont.
	dc.b	0,0,0,0
Cont.	dc.b	'Continue',0
	dc.b	0
L138	dc.b	0,1,1,0,0,6
	dc.b	0,$03,0,0,0,0
	dc.l	OK.
	dc.b	0,0,0,0
OK.	dc.b	'Ok'0,0
L140	dc.b	0,1,1,0,0,6
	dc.b	0,$1a,0,0,0,0
	dc.l	Tmp2.
	dc.b	0,0,0,0
L141	dc.b	0,1,1,0,0,6
	dc.b	0,$0f,0,0,0,0
	dc.l	Tmp1.
	dc.l	L140
L142	dc.b	0,1,1,0,0,6
	dc.b	0,4,0,0,0,0
	dc.l	InitFailed.
	dc.l	L141
InitFailed.	dc.b	'Initialization failed',0
L144	dc.b	0,1,1,0,0,6
	dc.b	0,$1a,0,0,0,0
	dc.l	AllErased.
	dc.b	0,0,0,0
AllErased.	dc.b	'(all data will be erased) ?',0
L146	dc.b	0,1,1,0,0,6
	dc.b	0,$0f,0,0,0,0
	dc.l	L130
	dc.l	L144
L147	dc.b	0,1,1,0,0,6
	dc.b	0,4,0,0,0,0
	dc.l	OkInit.
	dc.l	L146
OkInit.	dc.b	'Ok to Initialize volume',0
L149	dc.b	0,1,1,0,0,6
	dc.b	0,$0f,0,0,0,0
	dc.l	L131
	dc.l	L144
L150	dc.b	0,1,1,0,0,6
	dc.b	0,4,0,0,0,0
	dc.l	OkInit2.
	dc.l	L149
OkInit2.	dc.b	'Ok to Initialize disk in',0
	dc.b	0
L152	dc.b	0,1,1,0,0,6
	dc.b	0,$1a,0,0,0,0
	dc.l	InUse.
	dc.b	0,0,0,0
InUse.	dc.b	'is in use',0
L154	dc.b	0,1,1,0,0,6
	dc.b	0,$0f,0,0,0,0
	dc.l	L130
	dc.l	L152
	dc.b	0,1,1,0,0,6
	dc.b	0,4,0,0,0,0
	dc.l	Warn.
	dc.l	L154
Warn.	dc.b	'Warning: disk',0
L156	dc.b	0,1,1,0,0,6
	dc.b	0,$25,0,0,0,0
	dc.l	Menu.
	dc.b	0,0,0,0
Menu.	dc.b	'from the Disk menu',0
	dc.b	0
L158	dc.b	0,1,1,0,0,6
	dc.b	0,$1a,0,0,0,0
	dc.l	Choose.
	dc.l	L156
Choose.	dc.b	'choose Initialize',0
L160	dc.b	0,1,1,0,0,6
	dc.b	0,$0f,0,0,0,0
	dc.l	SelIcon.
	dc.l	L158
SelIcon.	dc.b	'select its icon and',0
L162	dc.b	0,1,1,0,0,6
	dc.b	0,4,0,0,0,0
	dc.l	ToInit.
	dc.l	L160
ToInit.	dc.b	'To initialize a disk',0
	dc.b	0
L164	dc.b	0,1,1,0,0,6
	dc.b	0,$1a,0,0,0,0
	dc.l	L131
	dc.b	0,0,0,0
L165	dc.b	0,1,1,0,0,6
	dc.b	0,$0f,0,0,0,0
	dc.l	ToBe.
	dc.l	L164
ToBe.	dc.b	'to be initialized in',0
	dc.b	0
L167	dc.b	0,1,1,0,0,6
	dc.b	0,4,0,0,0,0
	dc.l	PlsInsert.
	dc.l	L165
PlsInsert.	dc.b	'Please insert disk',0
	dc.b	0
L169	dc.b	0,1,1,0,0,6
	dc.b	0,$1a,0,0,0,0
	dc.l	WrtProt.
	dc.b	0,0,0,0
WrtProt.	dc.b	'is write protected',0
	dc.b	0
L171	dc.b	0,1,1,0,0,6
	dc.b	0,$0f,0,0,0,0
	dc.l	L131
	dc.l	L169
L172	dc.b	0,1,1,0,0,6
	dc.b	0,4,0,0,0,0
	dc.l	Dsikin.
	dc.l	L171
Diskin.	dc.b	'Disk in',0,0,0


	SECTION	segment4,CODE
seg4

* Put disk icon on disk

AddIcons:
	movem.l	d2-d4/a2-a3,-(a7)
	clr.w	d3
	movea.l	#L189,a2
	jsr	L288
	pea	L179
	jsr	GetDiskObject
	movea.l	d0,a3
	jsr	L289
	move.w	#$0027,L191
	move.w	#$0017,L192
	move.w	#$0002,L193
	move.l	#L208,L194
	move.b	#$03,L195
	move.w	#$00b3,L181
	move.w	#$007a,L182
	move.w	#$0132,L183
	move.w	#$0046,L184
	move.b	#-1,L185
	move.b	#-1,L186
	move.l	#$020000ff,L187
	move.w	#$0001,L188
	move.w	#-$1cf0,L196
	move.w	#$0001,L197
	move.w	#$002e,L198
	move.w	#$0019,L199
	move.w	#$0005,L200
	move.w	#$0003,L201
	move.w	#$0001,L202
	exg	d4,a3
	tst.l	d4
	exg	d4,a3
	addq.l	#$4,a7
	beq.s	L175
	move.l	$0016(a3),d2
	bra.s	L176

L175	move.l	#L190,d2
L176	move.l	d2,L203
	move.b	#$05,L204
	move.l	#-$80000000,L205
	move.l	#-$80000000,L206
	move.l	#L181,L207
	pea	Drv.
	pea	L180
	pea	(a2)
	jsr	L306
	pea	(a2)
	jsr	CreateDir
	move.l	d0,d2
	lea	$0010(a7),a7
	beq.s	L177
	move.l	d2,-(a7)
	jsr	UnLock
	pea	L196
	pea	(a2)
	jsr	PutDiskObject
	move.w	d0,d3
	lea	12(a7),a7
L177	exg	d4,a3
	tst.l	d4
	exg	d4,a3
	beq.s	L178
	move.l	a3,-(a7)
	jsr	FreeDiskObject
	addq.l	#$4,a7
L178	move.w	d3,d0
	ext.l	d0
	movem.l	(a7)+,d2-d4/a2-a3
	rts	


	SECTION	segment5,DATA
seg5
L179	dc.b	'SYS:Trashcan',0
	dc.b	0
L180	dc.b	'%sTrashcan',0
	dcb.b	1,0
L181	dcb.b	2,0
L182	dcb.b	2,0
L183	dcb.b	2,0
L184	dcb.b	2,0
L185	dcb.b	1,0
L186	dcb.b	5,0
L187	dcb.b	32,0
L188	dcb.b	10,0
L189	dcb.b	38,0
L190	dcb.b	4,0
L191	dcb.b	2,0
L192	dcb.b	2,0
L193	dcb.b	2,0
L194	dcb.b	4,0
L195	dcb.b	6,0
L196	dcb.b	2,0
L197	dcb.b	10,0
L198	dcb.b	2,0
L199	dcb.b	2,0
L200	dcb.b	2,0
L201	dcb.b	2,0
L202	dcb.b	2,0
L203	dcb.b	26,0
L204	dcb.b	10,0
L205	dcb.b	4,0
L206	dcb.b	4,0
L207	dcb.b	12,0
L208	dcb.b	18,0
	dc.b	$1f,$ff,$ff,$ff,$f0,0
	dc.b	$3f,$ff,$ff,$ff,$f8
	dcb.b	7,0
	dc.b	$0f,$ff,$ff,$ff,$e0,0
	dc.b	12,$e7,$39,$ce,$60,0
	dc.b	12,$e7,$39,$ce,$60,0
	dc.b	12,$e7,$39,$ce,$60,0
	dc.b	12,$e7,$39,$ce,$60,0
	dc.b	12,$e7,$39,$ce,$60,0
	dc.b	12,$e7,$39,$ce,$60,0
	dc.b	12,$e7,$39,$ce,$60,0
	dc.b	12,$e7,$39,$ce,$60,0
	dc.b	12,$e7,$39,$ce,$60,0
	dc.b	12,$e7,$39,$ce,$60,0
	dc.b	12,$e7,$39,$ce,$60,0
	dc.b	12,$e7,$39,$ce,$60,0
	dc.b	$0e,$0f,$83,$e0,$e0,0
	dc.b	$07,$ff,$ff,$ff,$c0
	dcb.b	14,0
	dc.b	1,$ff,0,0,0,0
	dc.b	$07,1,$c0,0,0,$7f
	dc.b	$ff,$ff,$ff,$fc,0,$ff
	dc.b	$ff,$ff,$ff,$fc,0,$ff
	dc.b	$ff,$ff,$ff,$fe,0,$ff
	dc.b	$ff,$ff,$ff,$fe,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$3f
	dc.b	$ff,$ff,$ff,$f8,0,$1f
	dc.b	$ff,$ff,$ff,$f0
	dcb.b	9,0


	SECTION	segment6,CODE
seg6
L209	link	a6,#-$0054
	movem.l	d2-d7/a2-a5,-(a7)
	ZAP	d7
	movea.l	d7,a2
	ZAP	d7
	movea.l	d7,a3
	clr.l	-$0020(a6)
	clr.w	-$0012(a6)
	clr.w	-$0014(a6)
	clr.b	d4
	move.l	#L126,d6
	move.l	#Window,-8(a6)
	move.l	#DoIO,-12(a6)
	exg	d6,a5
	tst.l	(a5)
	exg	d6,a5
	bne.s	L211
	movea.l	-8(a6),a5
	tst.l	(a5)
	bne.s	L210
	jsr	L290
	movea.l	d0,a0
	movea.l	-8(a6),a5
	move.l	a0,(a5)
L210	movea.l	-8(a6),a5
	tst.l	(a5)
	bne.s	L211
	moveq	#$7b,d2
	move.l	d2,d0
	bra.l	L257

L211	pea	Drv.
	jsr	DeviceProc
	move.l	d0,-$002c(a6)
	addq.l	#$4,a7
	bne.s	L212		;open worked
	moveq	#$7c,d4
	bra.l	L246

L212	ZAP	d7
	move.w	d7,-$0012(a6)
	move.l	-$002c(a6),-(a7)
	pea	#1
	jsr	L282
	move.b	d0,d4
	seq	-$0011(a6)
	neg.b	-$0011(a6)
	addq.l	#8,a7
	beq.l	L246
	movea.l	L127,a0
	move.l	$001c(a0),d7
	lsl.l	#2,d7
	move.l	d7,d3
	movea.l	d3,a5
	move.l	8(a5),d7
	lsl.l	#2,d7
	movea.l	d7,a4
	move.l	12(a4),d1
	move.l	$0014(a4),d0
	jsr	L303
	move.l	d0,d2
	move.l	$0028(a4),-$0028(a6)
	move.l	-$0028(a6),d7
	sub.l	$0024(a4),d7
	move.l	d7,-$0028(a6)
	addq.l	#1,-$0028(a6)
	move.l	-$0028(a6),d1
	move.l	d2,d0
	jsr	L303
	move.l	d0,-$0028(a6)
	move.l	d2,-$001c(a6)
	move.l	-$001c(a6),d7
	asl.l	#$7,d7
	move.l	d7,-$001c(a6)
	move.l	d2,-$0010(a6)
	move.l	-$0010(a6),d7
	asl.l	#8,d7
	move.l	d7,-$0010(a6)
	move.l	-$0010(a6),d7
	asl.l	#1,d7
	move.l	d7,-$0010(a6)
	move.l	$0018(a4),-$0018(a6)
	move.l	-$0028(a6),d2
	subq.l	#1,d2
	move.l	-$0018(a6),d7
	add.l	d2,d7
	move.l	d7,-$0018(a6)
	move.l	-$0018(a6),d7
	asr.l	#1,d7
	move.l	d7,-$0018(a6)
	move.l	#$00010002,-(a7)
	move.l	-$0010(a6),-(a7)
	jsr	AllocMem
	movea.l	d0,a2
	clr.l	-(a7)
	clr.l	-(a7)
	jsr	L313
	move.l	d0,-$0020(a6)
	move.l	-$0020(a6),-(a7)
	jsr	L321
	movea.l	d0,a3
	exg	d7,a2
	tst.l	d7
	exg	d7,a2
	lea	$0014(a7),a7
	beq.l	L213
	tst.l	-$0020(a6)
	beq.l	L213
	exg	d7,a3
	tst.l	d7
	exg	d7,a3
	bne.s	L214
L213	moveq	#$7b,d4
	bra.l	L246

L214	ZAP	d7
	move.w	d7,-$0014(a6)
	movea.l	d3,a5
	move.l	12(a5),-(a7)
	move.l	a3,-(a7)
	movea.l	d3,a5
	move.l	(a5),-(a7)
	movea.l	d3,a5
	move.l	$0004(a5),d7
	lsl.l	#2,d7
	movea.l	d7,a0
	addq.l	#1,a0
	move.l	a0,-(a7)
	jsr	OpenDevice
	move.b	d0,d4
	seq	-$0013(a6)
	neg.b	-$0013(a6)
	lea	$0010(a7),a7
	beq.l	L246
	exg	d6,a5
	tst.l	(a5)
	exg	d6,a5
	beq.s	L215
	pea	InvCur.
	jsr	DispMsg
	addq.l	#$4,a7
L215	move.l	a2,$0028(a3)
	move.l	-$0010(a6),$0024(a3)
	move.l	$0024(a4),-$0024(a6)
	bra.l	L230

L216	move.l	-$0010(a6),d1
	move.l	-$0024(a6),d0
	jsr	L303
	move.l	d0,$002c(a3)
	clr.l	-$0004(a6)
	move.l	a2,d5
	bra.s	L218

* INIT boot block??

L217	move.l	-$0024(a6),d2
	sub.l	$0024(a4),d2
	moveq	#16,d3
	asl.l	d3,d2
	or.l	-$0004(a6),d2
	ori.l	#$444f5300,d2		;'DOS',0
	movea.l	d5,a5
	move.l	d2,(a5)
	addq.l	#1,-$0004(a6)
	addq.l	#$4,d5
L218	move.l	-$0004(a6),d7
	cmp.l	-$001c(a6),d7
	blt.s	L217
	move.l	-$0024(a6),d7
	cmp.l	$0024(a4),d7
	bne.s	L219
	move.l	#$434f5059,(a2)		;'COPY'
L219	clr.w	d5
	clr.l	-$0004(a6)
	bra.l	L229

L220	pea	($1000).w
	clr.l	-(a7)
	jsr	SetSignal
	move.l	d0,d3
	andi.l	#$00001000,d3
	addq.l	#8,a7
	beq.s	L221
	moveq	#$7d,d4
	bra.l	L246

L221	moveq	#$02,d2
	cmp.l	-$0004(a6),d2
	ble.l	L246
	tst.w	d5
	beq.s	L222
	moveq	#$02,d2
	bra.s	L223

L222	moveq	#$0b,d2
L223	move.w	d2,$001c(a3)
	move.l	a3,-(a7)
	jsr	SendIO
	tst.w	d5
	addq.l	#$4,a7
	beq.s	L224
	move.l	#Verify.,d2
	bra.s	L225

L224	move.l	#Format.,d2
L225	move.l	$0028(a4),d3
	sub.l	-$0024(a6),d3
	move.l	d3,-(a7)
	move.l	-$0024(a6),-(a7)
	move.l	d2,-(a7)
	pea	TrkCount.
	pea	-$0054(a6)
	jsr	L306
	exg	d6,a5
	tst.l	(a5)
	exg	d6,a5
	lea	$0014(a7),a7
	beq.s	L226
	pea	-$0054(a6)
	pea	Crn.
	jsr	DispMsg
	addq.l	#8,a7
	bra.s	L227

L226	pea	-$0054(a6)
	pea	($0028).w
	pea	($0014).w
	pea	($0001).w
	movea.l	-8(a6),a5
	move.l	(a5),-(a7)
	jsr	InitRoot
	lea	$0014(a7),a7
L227	move.l	a3,-(a7)
	jsr	WaitIO
	move.b	d0,d4
	addq.l	#$4,a7
	beq.s	L228
	addq.l	#1,-$0004(a6)
	clr.w	d5
L228	tst.b	d4
	bne.l	L220
	addq.w	#1,d5
L229	moveq	#1,d2
	cmp.w	d5,d2
	bge.l	L220
	addq.l	#1,-$0024(a6)
L230	move.l	-$0024(a6),d7
	cmp.l	$0028(a4),d7
	ble.l	L216
	clr.l	-$0004(a6)
	move.l	a2,d5
	bra.s	L232

L231	movea.l	d5,a5
	clr.l	(a5)+
	move.l	a5,d5
	addq.l	#1,-$0004(a6)
L232	move.l	-$0004(a6),d7
	cmp.l	-$001c(a6),d7
	blt.s	L231
	moveq	#$02,d2
	move.l	d2,(a2)
	moveq	#$48,d2
	move.l	d2,12(a2)
	moveq	#1,d2
	move.l	d2,$01fc(a2)
	pea	$01e4(a2)
	jsr	DateStamp
	pea	$01a4(a2)
	jsr	DateStamp
	movea.l	-$0018(a6),a1
	addq.l	#1,a1
	move.l	a1,$013c(a2)
	moveq	#1,d2
	move.l	d2,$0138(a2)
	move.l	L128,-(a7)
	jsr	StgLen		;calc length of string
	move.b	d0,$01b0(a2)
	move.l	L128,-(a7)
	pea	$01b1(a2)
	jsr	MoveStg
	clr.l	-$0004(a6)
	move.l	a2,d7
	addi.l	#$00000200,d7
	move.l	d7,d5
	lea	$0014(a7),a7
	bra.s	L236

L233	move.l	-$0004(a6),d2
	moveq	#$1f,d3
	and.l	d3,d2
	bne.s	L234
	move.l	a2,d7
	addi.l	#$00000400,d7
	movea.l	d7,a1
	addq.l	#$4,d5
	cmpa.l	d5,a1
	bhi.s	L234
	clr.l	$0138(a2)
	clr.l	$013c(a2)
	bra.s	L237

L234	move.l	-$0018(a6),d2
	sub.l	$0018(a4),d2
	move.l	-$0004(a6),d7
	cmp.l	d2,d7
	beq.s	L235
	move.l	-$0018(a6),d2
	addq.l	#1,d2
	sub.l	$0018(a4),d2
	move.l	-$0004(a6),d7
	cmp.l	d2,d7
	beq.s	L235
	moveq	#1,d2
	move.b	-1(a6),d3
	andi.b	#$1f,d3
	asl.l	d3,d2
	movea.l	d5,a5
	or.l	d2,(a5)
L235	addq.l	#1,-$0004(a6)
L236	move.l	-$0028(a6),d2
	sub.l	$0018(a4),d2
	move.l	-$0004(a6),d7
	cmp.l	d2,d7
	blt.s	L233
L237	moveq	#1,d7
	move.l	d7,-$0004(a6)
	move.l	a2,d7
	addi.l	#$00000200,d7
	movea.l	d7,a0
	move.l	a0,d5
	clr.l	(a0)
	bra.s	L239

L238	move.l	-$0004(a6),d0
	addq.l	#1,-$0004(a6)
	asl.l	#2,d0
	movea.l	d0,a0
	adda.l	d5,a0
	move.l	(a0),d2
	movea.l	d5,a5
	sub.l	d2,(a5)
L239	cmpi.l	#$00000080,-$0004(a6)
	blt.s	L238
	ZAP	d7
	movea.l	d7,a1
	move.l	a1,d0
	move.l	a1,$0014(a2)
	clr.l	-$0004(a6)
	move.l	a2,d5
	bra.s	L241

L240	movea.l	d5,a5
	add.l	(a5)+,d0
	move.l	a5,d5
	addq.l	#1,-$0004(a6)
L241	cmpi.l	#$00000080,-$0004(a6)
	blt.s	L240
	move.l	d0,d7
	neg.l	d7
	move.l	d7,$0014(a2)
	move.w	#$0003,$001c(a3)
	move.l	a2,$0028(a3)
	tst.l	$0138(a2)
	beq.s	L242
	move.l	#$00000400,d0
	bra.s	L243

L242	move.l	#$00000200,d0
L243	move.l	d0,$0024(a3)
	move.l	$0024(a4),d1
	move.l	-$0010(a6),d0
	jsr	L303
	movea.l	d0,a1
	move.l	-$0018(a6),d7
	asl.l	#8,d7
	asl.l	#1,d7
	movea.l	d7,a0
	adda.l	a0,a1
	move.l	a1,$002c(a3)
	move.l	a3,-(a7)
	movea.l	-12(a6),a5
	jsr	(a5)
	move.b	d0,d4
	addq.l	#$4,a7
	bne.l	L246
	move.l	#$00000200,$0024(a3)
	move.l	$0024(a4),d1
	move.l	-$0010(a6),d0
	jsr	L303
	move.l	d0,$002c(a3)
	clr.l	-$0004(a6)
	move.l	a2,d5
	bra.s	L245

* Init boot block

L244	move.l	-$0004(a6),d2
	ori.l	#$444f5300,d2		;'DOS',0
	movea.l	d5,a5
	move.l	d2,(a5)
	addq.l	#1,-$0004(a6)
	addq.l	#$4,d5
L245	cmpi.l	#$00000080,-$0004(a6)
	blt.s	L244
	move.w	#$0003,$001c(a3)
	move.l	a3,-(a7)
	movea.l	-12(a6),a5
	jsr	(a5)
	move.b	d0,d4
	addq.l	#$4,a7
	bne.l	L246
	move.w	#$0004,$001c(a3)
	move.l	a3,-(a7)
	movea.l	-12(a6),a5
	jsr	(a5)
	move.b	d0,d4
	addq.l	#$4,a7
	bne.l	L246
	move.w	#$0005,$001c(a3)
	move.l	a3,-(a7)
	movea.l	-12(a6),a5
	jsr	(a5)
	move.b	d0,d4
	addq.l	#$4,a7
	bne.l	L246
L246	tst.w	-$0012(a6)
	beq.s	L247
	move.l	-$002c(a6),-(a7)
	clr.l	-(a7)
	jsr	L282
	addq.l	#8,a7
L247	exg	d6,a5
	tst.l	(a5)
	exg	d6,a5
	beq.s	L248
	pea	VisCur.
	jsr	DispMsg
	addq.l	#$4,a7
L248	tst.b	d4
	bne.l	L252
	exg	d6,a5
	tst.l	(a5)
	exg	d6,a5
	beq.s	L249
	pea	Initing.
	pea	nLF.
	jsr	DispMsg
	addq.l	#8,a7
	bra.s	L250

L249	pea	Initing.
	pea	($0028).w
	pea	($0014).w
	pea	($0001).w
	movea.l	-8(a6),a5
	move.l	(a5),-(a7)
	jsr	InitRoot		;init root dir
	lea	$0014(a7),a7
L250	pea	($0096).w
	jsr	Delay
	tst.w	NoIconFlg	;icons?
	addq.l	#$4,a7
	bne.s	L251		;no
	jsr	AddIcons	;initialize icons
L251	exg	d6,a5
	tst.l	(a5)
	exg	d6,a5
	bne.s	L252
	pea	($00c8).w
	jsr	Delay
	addq.l	#$4,a7
L252	tst.w	-$0014(a6)
	beq.s	L253
	move.l	a3,-(a7)
	jsr	CloseDevice
	addq.l	#$4,a7
L253	exg	d7,a2
	tst.l	d7
	exg	d7,a2
	beq.s	L254
	move.l	-$0010(a6),-(a7)
	move.l	a2,-(a7)
	jsr	FreeMem
	addq.l	#8,a7
L254	exg	d7,a3
	tst.l	d7
	exg	d7,a3
	beq.s	L255
	move.l	a3,-(a7)
	jsr	L322
	addq.l	#$4,a7
L255	tst.l	-$0020(a6)
	beq.s	L256
	move.l	-$0020(a6),-(a7)
	jsr	L319
	addq.l	#$4,a7
L256	move.b	d4,d2
	ext.w	d2
	ext.l	d2
	move.l	d2,d0
L257	movem.l	(a7)+,d2-d7/a2-a5
	unlk	a6
	rts	

	dc.b	0,0


	SECTION	segment7,DATA
seg7
InvCur.		dc.b	$9b,$30,$20,$70,0,0	;make cursor invisible
TrkCount.	dc.b	'%s cyl %ld, %ld to go  ',0
Verify.		dc.b	'Verifying ',0,0
Format.		dc.b	'Formatting',0,0
Crn.		dc.b	CR,'%s',0
VisCur.		dc.b	LF,$9b,$20,$70,0,0	;make cursor visible
nLF.		dc.b	'%s',LF,0
Initing.	dc.b	'Initializing disk        ',0
		dc.b	0,0


	SECTION	segment8,CODE
seg8

* Find length of string on stack...return length in D0.

StgLen	move.l	$0004(a7),d1	;get ptr to string
	movea.l	d1,a0		;use A0 as ptr
1$:	tst.b	(a0)		;end of string?
	beq.s	2$		;yes
	addq.l	#1,a0		;no...bump ptr
	bra.s	1$		;loop

2$:	move.l	a0,d0		;calc length
	sub.l	d1,d0		;return in D0
	rts	

* Move string A0 to A1.

MoveStg:
	movea.l	$0004(a7),a1
	movea.l	8(a7),a0
1$:	move.b	(a0)+,(a1)+
	bne.s	1$
	rts	

L271	move.b	$0007(a7),d0
	cmpi.b	#$61,d0
	bcs.s	L272
	cmpi.b	#$7a,d0
	bhi.s	L272
	ZAP	d1
	move.b	d0,d1
	moveq	#$61,d0
	sub.l	d0,d1
	moveq	#$41,d0
	add.l	d0,d1
	bra.s	L273

L272	ZAP	d1
	move.b	d0,d1
L273	move.l	d1,d0
	rts	

L274	movem.l	d2-d3/a2-a3,-(a7)
	movea.l	$0014(a7),a2
	movea.l	$0018(a7),a3
	bra.s	L277

L275	tst.b	(a2)
	bne.s	L276
	moveq	#1,d0
	bra.s	L278

L276	addq.l	#1,a2
	addq.l	#1,a3
L277	ZAP	d2
	move.b	(a3),d2
	move.l	d2,-(a7)
	jsr	L271(pc)
	move.l	d0,d2
	ZAP	d3
	move.b	(a2),d3
	move.l	d3,-(a7)
	jsr	L271(pc)
	cmp.l	d0,d2
	addq.l	#8,a7
	beq.s	L275
	ZAP	d0
L278	movem.l	(a7)+,d2-d3/a2-a3
	rts	

L279	movea.l	$0004(a7),a0
L280	cmpi.b	#$3a,(a0)
	beq.s	L281
	tst.b	(a0)
	beq.s	L281
	addq.l	#1,a0
	bra.s	L280

L281	clr.b	(a0)
	rts	

L282	movem.l	d2-d6/a2-a3,-(a7)
	move.l	$0020(a7),d4
	move.l	$0024(a7),d3
	pea	($0001).w
	pea	($0044).w
	jsr	AllocMem
	movea.l	d0,a3
	exg	d6,a3
	tst.l	d6
	exg	d6,a3
	addq.l	#8,a7
	bne.s	L283
	moveq	#$7b,d2
	move.l	d2,d0
	bra.l	L287

L283	lea	$0014(a3),a0
	move.l	a0,$000a(a3)
	move.l	a3,$0014(a3)
	clr.l	-(a7)
	jsr	FindTask
	movea.l	d0,a2
	lea	$005c(a2),a0
	move.l	a0,d2
	move.l	d2,d5
	move.l	d2,$0018(a3)
	moveq	#$1f,d2
	move.l	d2,$001c(a3)
	move.l	d4,$0028(a3)
	pea	(a3)
	move.l	d3,-(a7)
	jsr	PutMsg
	movea.l	a3,a2
	move.l	d5,-(a7)
	jsr	WaitPort
	cmpa.l	d0,a2
	lea	$0010(a7),a7
	beq.s	L284
	pea	($0040).w
	pea	DosErr.
	move.l	#-$78fffffd,-(a7)
	jsr	DisplayAlert
	lea	12(a7),a7
L284	pea	(a3)
	jsr	Remove
	tst.l	$0020(a3)
	addq.l	#$4,a7
	beq.s	L285
	ZAP	d2
	bra.s	L286

L285	moveq	#$7e,d2
L286	pea	($0044).w
	move.l	a3,-(a7)
	jsr	FreeMem
	ext.w	d2
	ext.l	d2
	move.l	d2,d0
	addq.l	#8,a7
L287	movem.l	(a7)+,d2-d6/a2-a3
	rts	

L288	clr.l	-(a7)
	jsr	FindTask
	movea.l	d0,a0
	lea	$00b8(a0),a0
	move.l	(a0),L294
	moveq	#-1,d0
	move.l	d0,(a0)
	addq.l	#$4,a7
	rts	

L289	clr.l	-(a7)
	jsr	FindTask
	movea.l	d0,a0
	move.l	L294,$00b8(a0)
	addq.l	#$4,a7
	rts	

L290	link	a6,#-$0030
	move.l	d2,-(a7)
	clr.w	-$0030(a6)
	clr.w	-$002e(a6)
	move.w	#$0140,-$002c(a6)
	move.w	#$0048,-$002a(a6)
	move.b	#-1,-$0028(a6)
	move.b	#1,-$0027(a6)
	moveq	#$40,d2
	move.l	d2,-$0026(a6)
	move.l	#$00001006,-$0022(a6)
	clr.l	-$001e(a6)
	clr.l	-$001a(a6)
	move.l	#Init.,-$0016(a6)
	clr.l	-$0012(a6)
	clr.l	-$000e(a6)
	move.w	#$0028,-$000a(a6)
	move.w	#$0014,-8(a6)
	move.w	#$03e8,-$0006(a6)
	move.w	#$03e8,-$0004(a6)
	move.w	#$0001,-$0002(a6)
	pea	-$0030(a6)
	jsr	OpenWindow
	addq.l	#$4,a7
	move.l	(a7)+,d2
	unlk	a6
	rts	

* Initialize root dir and boot block

InitRoot:
	movem.l	d2-d6/a2,-(a7)
	movea.l	$001c(a7),a2
	move.l	$0020(a7),d5
	move.l	$0024(a7),d4
	move.l	$002c(a7),-(a7)
	jsr	StgLen			;calc length of string
	move.l	d0,d6
	move.l	$002c(a7),d3
	moveq	#$0a,d2
	sub.l	d2,d3
	move.l	d3,-(a7)
	move.l	d4,-(a7)
	move.l	$0032(a2),-(a7)
	jsr	Move
	move.l	d5,-(a7)
	move.l	$0032(a2),-(a7)
	jsr	SetAPen
	move.l	d6,-(a7)
	move.l	$0048(a7),-(a7)
	move.l	$0032(a2),-(a7)
	jsr	Text
	lea	$0024(a7),a7
	movem.l	(a7)+,d2-d6/a2
	rts	


	SECTION	segment9,DATA
seg9
DosErr.	dc.b	'DOS Error.  Press left mouse button to continue',0
Init.	dc.b	'Initialize',0
	dc.b	0
L294	dc.b	0,0,0,0


	SECTION	segment10,CODE
seg10
L295	movem.l	a2-a4/a6,-(a7)
	movea.l	$0014(a7),a4
	movea.l	$0018(a7),a0
	movea.l	$001c(a7),a1
	lea	L298(pc),a2
	lea	-$008c(a7),a7
	movea.l	a7,a3
	movea.l	4,a6
	jsr	RawDoFmt(a6)
	moveq	#-1,d0
L296	tst.b	(a3)+
	dbeq	d0,L296
	not.l	d0
	beq.s	L297
	move.l	d0,-(a7)
	pea	$0004(a7)
	pea	(a4)
	jsr	Write
	lea	12(a7),a7
L297	lea	$008c(a7),a7
	movem.l	(a7)+,a2-a4/a6
	rts	

L298	dc.b	$16,$c0,$4e,$75

E299	cmpi.l	#$0000ffff,d2
	bgt.s	E300
	movea.w	d1,a1
	clr.w	d1
	swap	d1
	divu	d2,d1
	move.l	d1,d0
	swap	d1
	move.w	a1,d0
	divu	d2,d0
	move.w	d0,d1
	clr.w	d0
	swap	d0
	rts	

E300	move.l	d1,d0
	clr.w	d0
	swap	d0
	swap	d1
	clr.w	d1
	movea.l	d2,a1
	moveq	#$0f,d2
E301	add.l	d1,d1
	addx.l	d0,d0
	cmpa.l	d0,a1
	bgt.s	E302
	sub.l	a1,d0
	addq.w	#1,d1
E302	dbf	d2,E301
	rts	

L303	move.l	d2,-(a7)
	move.l	d0,d2
	mulu	d1,d2
	movea.l	d2,a0
	move.l	d0,d2
	swap	d2
	mulu	d1,d2
	swap	d1
	mulu	d1,d0
	add.l	d2,d0
	swap	d0
	clr.w	d0
	adda.l	d0,a0
	move.l	a0,d0
	move.l	(a7)+,d2
	rts	

	dc.b	$2f,$02,$24,1,$22,0

	bsr.s	E299
	move.l	(a7)+,d2
	rts	

	dc.b	$2f,$02,$24,1,$22,0

	bsr.s	E299
	move.l	d1,d0
	move.l	(a7)+,d2
	rts	

	dc.b	$2f,$02,$24,1,$6c,$02
	dc.b	$44,$82,$22,0,$70,0
	dc.b	$4a,$81,$6c,4,$44,$81
	dc.b	$46,$80,$20,$40

	bsr.l	E299
	move.w	a0,d2
	beq.s	E304
	neg.l	d0
E304	move.l	(a7)+,d2
	rts	

	dc.b	$2f,$02,$20,$40,$70,0
	dc.b	$24,1,$6c,4,$44,$82
	dc.b	$46,$80,$22,$08,$6c,4
	dc.b	$44,$81,$46,$80,$20,$40

	bsr.l	E299
	move.l	a0,d2
	beq.s	E305
	neg.l	d1
E305	move.l	d1,d0
	move.l	(a7)+,d2
	rts	

	dc.b	0,0

L306	movem.l	a2-a4/a6,-(a7)
	movea.l	$0014(a7),a3
	movea.l	$0018(a7),a0
	lea	$001c(a7),a1
	lea	L307(pc),a2
	movea.l	4,a6
	jsr	-$020a(a6)
	movem.l	(a7)+,a2-a4/a6
	rts	

L307	dc.b	$16,$c0,$4e,$75


	SECTION	segment11,CODE
seg11
L308	link	a6,#-$0004
	pea	($0001).w
	pea	-$0004(a6)
	move.l	StdIn,-(a7)
	jsr	Read
	moveq	#1,d1
	cmp.l	d0,d1
	lea	12(a7),a7
	beq.s	L309
	moveq	#-1,d0
	bra.s	L310

L309	move.b	-$0004(a6),d0
	ext.w	d0
	ext.l	d0
L310	unlk	a6
	rts	

	dc.b	0,0


	SECTION	segment12,DATA
seg12


	SECTION	segment13,CODE
seg13
DispMsg:
	move.l	d2,-(a7)
	move.l	8(a7),d2
	pea	12(a7)
	move.l	d2,-(a7)
	move.l	StdOut,-(a7)
	jsr	L295
	lea	12(a7),a7
	move.l	(a7)+,d2
	rts	


	SECTION	segment14,DATA
seg14


	SECTION	segment15,CODE
seg15


	SECTION	segment16,CODE
seg16
L312	movea.l	$0004(a7),a0
	move.l	a0,(a0)
	addq.l	#$4,(a0)
	clr.l	$0004(a0)
	move.l	a0,8(a0)
	rts	

	dc.b	0,0


	SECTION	segment17,CODE
seg17
L313	movem.l	d2-d7/a2,-(a7)
	move.l	$0020(a7),d4
	move.b	$0027(a7),d3
	move.l	#-$00000001,-(a7)
	jsr	AllocSignal
	move.l	d0,d5
	move.l	d5,d6
	moveq	#-1,d2
	cmp.l	d5,d2
	addq.l	#$4,a7
	bne.s	L314
	ZAP	d0
	bra.l	L318

L314	move.l	#$00010001,-(a7)
	pea	($0022).w
	jsr	AllocMem
	movea.l	d0,a2
	exg	d7,a2
	tst.l	d7
	exg	d7,a2
	addq.l	#8,a7
	bne.s	L315
	move.l	d6,-(a7)
	jsr	FreeSignal
	ZAP	d0
	addq.l	#$4,a7
	bra.s	L318

L315	move.l	d4,$000a(a2)
	move.b	d3,$0009(a2)
	move.b	#4,8(a2)
	clr.b	$000e(a2)
	move.b	d6,$000f(a2)
	clr.l	-(a7)
	jsr	FindTask
	move.l	d0,$0010(a2)
	tst.l	d4
	addq.l	#$4,a7
	beq.s	L316
	move.l	a2,-(a7)
	jsr	AddPort
	addq.l	#$4,a7
	bra.s	L317

L316	pea	$0014(a2)
	jsr	L312
	addq.l	#$4,a7
L317	move.l	a2,d0
L318	movem.l	(a7)+,d2-d7/a2
	rts	

L319	movem.l	d2/a2,-(a7)
	movea.l	12(a7),a2
	tst.l	$000a(a2)
	beq.s	L320
	move.l	a2,-(a7)
	jsr	RemPort
	addq.l	#$4,a7
L320	move.b	#-1,8(a2)
	moveq	#-1,d2
	move.l	d2,$0014(a2)
	ZAP	d2
	move.b	$000f(a2),d2
	move.l	d2,-(a7)
	jsr	FreeSignal
	pea	($0022).w
	move.l	a2,-(a7)
	jsr	FreeMem
	lea	12(a7),a7
	movem.l	(a7)+,d2/a2
	rts	


	SECTION	segment18,DATA
seg18


	SECTION	segment19,CODE
seg19
L321	move.l	d2,-(a7)
	move.l	8(a7),d2
	pea	($0030).w
	move.l	d2,-(a7)
	jsr	L323
	addq.l	#8,a7
	move.l	(a7)+,d2
	rts	

L322	move.l	d2,-(a7)
	move.l	8(a7),d2
	move.l	d2,-(a7)
	jsr	L327
	addq.l	#$4,a7
	move.l	(a7)+,d2
	rts	


	SECTION	segment20,DATA
seg20


	SECTION	segment21,CODE
seg21
L323	movem.l	d2-d4,-(a7)
	move.l	$0010(a7),d2
	move.l	$0014(a7),d3
	tst.l	d2
	bne.s	L324
	ZAP	d0
	bra.s	L326

L324	move.l	#$00010001,-(a7)
	move.l	d3,-(a7)
	jsr	AllocMem
	movea.l	d0,a0
	exg	d4,a0
	tst.l	d4
	exg	d4,a0
	addq.l	#8,a7
	bne.s	L325
	ZAP	d0
	bra.s	L326

L325	move.b	#$05,8(a0)
	move.w	d3,$0012(a0)
	move.l	d2,$000e(a0)
	move.l	a0,d0
L326	movem.l	(a7)+,d2-d4
	rts	

L327	movem.l	d2-d3,-(a7)
	movea.l	12(a7),a0
	exg	d3,a0
	tst.l	d3
	exg	d3,a0
	beq.l	L328
	move.b	#-1,8(a0)
	moveq	#-1,d2
	move.l	d2,$0014(a0)
	moveq	#-1,d2
	move.l	d2,$0018(a0)
	ZAP	d2
	move.w	$0012(a0),d2
	move.l	d2,-(a7)
	move.l	a0,-(a7)
	jsr	FreeMem
	addq.l	#8,a7
L328	movem.l	(a7)+,d2-d3
	rts	


	SECTION	segment22,DATA
seg22


	SECTION	segment23,CODE
seg23


	SECTION	segment24,CODE
seg24
Read:	movem.l	d2-d3/a6,-(a7)
	movea.l	DosBase,a6
	movem.l	$0010(a7),d1-d3
	jsr	Read(a6)
	movem.l	(a7)+,d2-d3/a6
	rts	

	dc.b	0,0

Write:	movem.l	d2-d3/a6,-(a7)
	movea.l	DosBase,a6
	movem.l	$0010(a7),d1-d3
	jsr	Write(a6)
	movem.l	(a7)+,d2-d3/a6
	rts	

	dc.b	0,0

Input:	move.l	a6,-(a7)
	movea.l	DosBase,a6
	jsr	Input(a6)
	movea.l	(a7)+,a6
	rts	

Output:	move.l	a6,-(a7)
	movea.l	DosBase,a6
	jsr	Output(a6)
	movea.l	(a7)+,a6
	rts	

UnLock:	move.l	a6,-(a7)
	movea.l	DosBase,a6
	move.l	8(a7),d1
	jsr	UnLock(a6)
	movea.l	(a7)+,a6
	rts	

Examine:
	movem.l	d2/a6,-(a7)
	movea.l	DosBase,a6
	movem.l	12(a7),d1-d2
	jsr	Examine(a6)
	movem.l	(a7)+,d2/a6
	rts	

	dc.b	0,0

Info:	movem.l	d2/a6,-(a7)
	movea.l	DosBase,a6
	movem.l	12(a7),d1-d2
	jsr	Info(a6)
	movem.l	(a7)+,d2/a6
	rts	

	dc.b	0,0

CreateDir:
	move.l	a6,-(a7)
	movea.l	DosBase,a6
	move.l	8(a7),d1
	jsr	CreateDir(a6)
	movea.l	(a7)+,a6
	rts	

IoErr:	move.l	a6,-(a7)
	movea.l	DosBase,a6
	jsr	IoErr(a6)
	movea.l	(a7)+,a6
	rts	

DeviceProc:
	move.l	a6,-(a7)
	movea.l	DosBase,a6
	move.l	8(a7),d1
	jsr	DeviceProc(a6)
	movea.l	(a7)+,a6
	rts	

DateStamp:
	move.l	a6,-(a7)
	movea.l	DosBase,a6
	move.l	8(a7),d1
	jsr	DateStamp(a6)
	movea.l	(a7)+,a6
	rts	

Delay:	move.l	a6,-(a7)
	movea.l	DosBase,a6
	move.l	8(a7),d1
	jsr	Delay(a6)
	movea.l	(a7)+,a6
	rts	

WaitForChar:
	movem.l	d2/a6,-(a7)
	movea.l	DosBase,a6
	movem.l	12(a7),d1-d2
	jsr	WaitForChar(a6)
	movem.l	(a7)+,d2/a6
	rts	

	dc.b	0,0

ParentDir:
	move.l	a6,-(a7)
	movea.l	DosBase,a6
	move.l	8(a7),d1
	jsr	ParentDir(a6)
	movea.l	(a7)+,a6
	rts	


	SECTION	segment25,CODE
seg25
AllocMem:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movem.l	8(a7),d0-d1
	jsr	AllocMem(a6)
	movea.l	(a7)+,a6
	rts	

	dc.b	0,0

FreeMem:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movea.l	8(a7),a1
	move.l	12(a7),d0
	jsr	FreeMem(a6)
	movea.l	(a7)+,a6
	rts	

Remove:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movea.l	8(a7),a1
	jsr	Remove(a6)
	movea.l	(a7)+,a6
	rts	

FindTask:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movea.l	8(a7),a1
	jsr	FindTask(a6)
	movea.l	(a7)+,a6
	rts	

SetSignal:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movem.l	8(a7),d0-d1
	jsr	SetSignal(a6)
	movea.l	(a7)+,a6
	rts	

	dc.b	0,0

AllocSignal:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	move.l	8(a7),d0
	jsr	AllocSignal(a6)
	movea.l	(a7)+,a6
	rts	

FreeSignal:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	move.l	8(a7),d0
	jsr	FreeSignal(a6)
	movea.l	(a7)+,a6
	rts	

AddPort:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movea.l	8(a7),a1
	jsr	AddPort(a6)
	movea.l	(a7)+,a6
	rts	

RemPort:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movea.l	8(a7),a1
	jsr	RemPort(a6)
	movea.l	(a7)+,a6
	rts	

PutMsg:	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movem.l	8(a7),a0-a1
	jsr	PutMsg(a6)
	movea.l	(a7)+,a6
	rts	

	dc.b	0,0

WaitPort:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movea.l	8(a7),a0
	jsr	WaitPort(a6)
	movea.l	(a7)+,a6
	rts	

CloseLibrary:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movea.l	8(a7),a1
	jsr	CloseLibrary(a6)
	movea.l	(a7)+,a6
	rts	

OpenDevice:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movea.l	8(a7),a0
	movem.l	12(a7),d0/a1
	move.l	$0014(a7),d1
	jsr	OpenDevice(a6)
	movea.l	(a7)+,a6
	rts	

	dc.b	0,0

CloseDevice:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movea.l	8(a7),a1
	jsr	CloseDevice(a6)
	movea.l	(a7)+,a6
	rts	

DoIO:	dc.b	$2f,$0e,$2c,$79
	dc.l	SysBase
	dc.b	$22,$6f,0,$08

	jsr	DoIO(a6)
	movea.l	(a7)+,a6
	rts	

SendIO:	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movea.l	8(a7),a1
	jsr	SendIO(a6)
	movea.l	(a7)+,a6
	rts	

WaitIO:	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movea.l	8(a7),a1
	jsr	WaitIO(a6)
	movea.l	(a7)+,a6
	rts	

OpenLib:
	move.l	a6,-(a7)
	movea.l	SysBase,a6
	movea.l	8(a7),a1
	move.l	12(a7),d0
	jsr	OpenLibrary(a6)
	movea.l	(a7)+,a6
	rts	


	SECTION	segment26,CODE
seg26
Text:	move.l	a6,-(a7)
	movea.l	GraphicsBase,a6
	movea.l	8(a7),a1
	movea.l	12(a7),a0
	move.l	$0010(a7),d0
	jsr	Text(a6)
	movea.l	(a7)+,a6
	rts	

Move:	move.l	a6,-(a7)
	movea.l	GraphicsBase,a6
	movea.l	8(a7),a1
	movem.l	12(a7),d0-d1
	jsr	Move(a6)
	movea.l	(a7)+,a6
	rts	

	dc.b	0,0

SetAPen:
	move.l	a6,-(a7)
	movea.l	GraphicsBase,a6
	movea.l	8(a7),a1
	move.l	12(a7),d0
	jsr	SetAPen(a6)
	movea.l	(a7)+,a6
	rts	


	SECTION	segment27,CODE
seg27
GetDiskObject:
	move.l	a6,-(a7)
	movea.l	IconBase,a6
	movea.l	8(a7),a0
	jsr	GetDiskObject(a6)
	movea.l	(a7)+,a6
	rts	

PutDiskObject:
	move.l	a6,-(a7)
	movea.l	IconBase,a6
	movea.l	8(a7),a0
	movea.l	12(a7),a1
	jsr	PutDiskObject(a6)
	movea.l	(a7)+,a6
	rts	

FreeDiskObject:
	move.l	a6,-(a7)
	movea.l	IconBase,a6
	movea.l	8(a7),a0
	jsr	FreeDiskObject(a6)
	movea.l	(a7)+,a6
	rts	


	SECTION	segment28,CODE
seg28
CloseWindow:
	move.l	a6,-(a7)
	movea.l	IntuitionBase,a6
	movea.l	8(a7),a0
	jsr	CloseWindow(a6)
	movea.l	(a7)+,a6
	rts	

DisplayAlert:
	move.l	a6,-(a7)
	movea.l	IntuitionBase,a6
	movem.l	8(a7),d0/a0
	move.l	$0010(a7),d1
	jsr	DisplayAlert(a6)
	movea.l	(a7)+,a6
	rts	

	dc.b	0,0

OpenWindow:
	move.l	a6,-(a7)
	movea.l	IntuitionBase,a6
	movea.l	8(a7),a0
	jsr	OpenWindow(a6)
	movea.l	(a7)+,a6
	rts	

AutoRequest:
	movem.l	d2-d3/a2-a3/a6,-(a7)
	movea.l	IntuitionBase,a6
	movea.l	$0018(a7),a0
	movea.l	$001c(a7),a1
	movea.l	$0020(a7),a2
	movea.l	$0024(a7),a3
	move.l	$0028(a7),d0
	move.l	$002c(a7),d1
	move.l	$0030(a7),d2
	move.l	$0034(a7),d3
	jsr	AutoRequest(a6)
	movem.l	(a7)+,d2-d3/a2-a3/a6
	rts	


	SECTION	segment29,DATA
seg29


	END

