;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*******************************************************************
*                                                                 *
*                                                                 *
*     DSM MC68000 Disassembler Version 1.0d (09/01/88).           *
*     Copyright (C) 1987, 1988 by OTG Software.                   *
*     All rights reserved.                                        *
*                                                                 *
*     Disassembly of :  diskdoctor                                *
*                                                                 *
*                                                                 *
*******************************************************************


	SECTION	segment0,CODE
seg0
L1	movea.l	$0164(a2),a4
	moveq	#$0c,d0
	jsr	(a5)
	move.l	d1,d2
	move.l	#$00000095,d1
	lea	L1(pc),a4
	movea.l	-$0004(a4),a4
	move.l	a4,-(a7)
	move.l	d2,-(a7)
L2	adda.l	a4,a4
	adda.l	a4,a4
	move.l	a4,d0
	beq.s	L4
	move.l	$0004(a4),d0
	asl.l	#2,d0
	cmp.l	0(a4,d0.l),d1
	bge.s	L3
	move.l	0(a4,d0.l),d1
L3	movea.l	(a4),a4
	bra.s	L2

L4	move.l	d1,d6
	addi.l	#$00000032,d1
	suba.l	a0,a0
	movea.l	$0074(a2),a4
	moveq	#$0c,d0
	jsr	(a5)
	tst.l	d1
	beq.l	L12
	addi.l	#$00000032,d1
	move.l	$0070(a2),d5
	movea.l	d1,a2
	adda.l	a2,a2
	adda.l	a2,a2
	move.l	d1,d7
	movea.l	a2,a0
	move.l	d6,(a0)+
	movea.l	d6,a4
	adda.l	d7,a4
	adda.l	a4,a4
	adda.l	a4,a4
	move.l	#$474c0003,d0
L5	move.l	d0,(a0)+
	addq.l	#2,d0
	cmpa.l	a4,a0
	ble.s	L5
	suba.l	a0,a0
	move.l	$000c(a7),d0
	lea	$0010(a7),a1
	suba.l	d0,a1
	move.l	#-$00000001,$0004(a1)
	move.l	a1,d1
	subi.l	#$000000a0,d0
	add.l	d1,d0
	move.l	d0,$0008(a1)
	asr.l	#2,d1
	move.l	d1,$0030(a2)
	move.l	(a7)+,d7
	move.l	d7,d6
	asl.l	#2,d6
	add.l	0(a0,d6.l),d7
	asl.l	#2,d7
L6	addq.l	#4,d6
	cmp.l	d6,d7
	blt.s	L7
	move.l	0(a0,d6.l),d1
	movea.l	d5,a4
	moveq	#$0c,d0
	jsr	(a5)
	tst.l	d1
	beq.l	L13
	bra.s	L6

L7	move.l	(a7)+,d1
	movea.l	d5,a4
	moveq	#$0c,d0
	jsr	(a5)
	tst.l	d1
	beq.s	L14
	movea.l	$0218(a2),a4
	moveq	#$0c,d0
	jsr	(a5)
	asl.l	#2,d1
	movea.l	d1,a3
	move.l	a3,-(a7)
	beq.s	L9
	lea	$0218(a2),a4
	moveq	#$0f,d0
L8	move.l	(a3)+,(a4)+
	dbf	d0,L8
L9	lea	L15(pc),a4
	move.l	a4,$0008(a2)
	movea.l	$0004(a2),a4
	moveq	#$20,d0
	moveq	#0,d1
	jsr	(a5)
	moveq	#0,d0
	move.l	d0,d7
	move.l	(a7)+,d1
	beq.s	L11
	movea.l	d1,a3
	lea	$0218(a2),a4
	moveq	#$0f,d0
L10	move.l	(a4)+,(a3)+
	dbf	d0,L10
L11	move.l	a2,d1
	asr.l	#2,d1
	subi.l	#$00000032,d1
	movea.l	$0078(a2),a4
	moveq	#$0c,d0
	jsr	(a5)
	move.l	d7,d0
	rts	

L12	tst.l	(a7)+
L13	tst.l	(a7)+
L14	moveq	#-1,d0
	rts	

L15	dc.b	$20,1,$60,$ca,0,0


	SECTION	segment1,CODE
seg1
	dc.b	0,0,6,$30,0,0
	dc.b	$30,$39,$11
	dc.b	'Doctor		',0
	dc.b	0,$07
	dc.b	'extractt'
	dc.b	$0c,$d4,$89,$e4,$8a,$23
	dc.b	$42,0,8,$78,1,$26
	dc.b	1,$74,$3a,$22,$29,0
	dc.b	8,$70,$38,$28,$6a,1
	dc.b	$ac

	jsr	(a5)
	move.l	d1,$002c(a1)
	move.l	$0008(a1),d1
	moveq	#$3c,d0
	movea.l	$01f0(a2),a4
	jsr	(a5)
	move.l	d1,$0030(a1)
	tst.l	d1
	bne.l	E16
	move.l	(a1),d2
	lea	$0134(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	moveq	#$48,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
	moveq	#$14,d1
	moveq	#$48,d0
	movea.l	$0008(a2),a4
	jsr	(a5)
E16	move.l	$0030(a1),d1
	lsl.l	#2,d1
	move.l	$1c(a0,d1.l),$0034(a1)
	move.l	$0034(a1),d2
	lsl.l	#2,d2
	move.l	0(a0,d2.l),$02a0(a2)
	move.l	$04(a0,d2.l),$02a4(a2)
	move.l	$08(a0,d2.l),$0038(a1)
	move.l	$0c(a0,d2.l),$02a8(a2)
	move.l	$0038(a1),d3
	lsl.l	#2,d3
	move.l	$18(a0,d3.l),$026c(a2)
	move.l	$24(a0,d3.l),$0270(a2)
	move.l	$28(a0,d3.l),$0274(a2)
	move.l	$0c(a0,d3.l),$0278(a2)
	move.l	$14(a0,d3.l),$027c(a2)
	move.l	$027c(a2),d2
	move.l	$0278(a2),d1
	jsr	$0010(a5)
	move.l	d1,$0280(a2)
	move.l	$0270(a2),d2
	move.l	$0274(a2),d3
	sub.l	d2,d3
	addq.l	#1,d3
	move.l	d1,d2
	move.l	d3,d1
	jsr	$0010(a5)
	move.l	d1,$0284(a2)
	move.l	#$00000080,d2
	move.l	$027c(a2),d1
	jsr	$0010(a5)
	move.l	d1,$0288(a2)
	move.l	d1,d2
	moveq	#4,d1
	jsr	$0010(a5)
	move.l	d1,$028c(a2)
	move.l	$026c(a2),d2
	move.l	$0284(a2),d3
	sub.l	d2,d3
	move.l	d3,$0004(a1)
	moveq	#$20,d2
	move.l	d3,d1
	jsr	$0012(a5)
	move.l	d1,$0294(a2)
	moveq	#$20,d2
	move.l	$0004(a1),d1
	jsr	$0012(a5)
	move.l	d2,$0298(a2)
	move.l	$0284(a2),d1
	subq.l	#1,d1
	add.l	$026c(a2),d1
	moveq	#2,d2
	jsr	$0012(a5)
	move.l	d1,$029c(a2)
	move.l	$0270(a2),d2
	move.l	$0280(a2),d1
	jsr	$0010(a5)
	move.l	d1,$02ac(a2)
	jmp	(a6)

	dc.b	$12
	dc.b	'Unknown device %S'
	dc.b	$0a,0,7,$65,$72,$72
	dc.b	$6f,$72,$20,$20,$24,1
	dc.b	$47,$ec,0,$20,$22,$0b
	dc.b	$e4,$89,$70,$10,$28,$6a
	dc.b	1,$28

	jsr	(a5)
	moveq	#$14,d1
	moveq	#$10,d0
	lea	$0064(a4),a4
	jsr	(a5)
	jmp	(a6)

	dc.b	$4e,$71,$0a,$45,$72,$72
	dc.b	$6f,$72,$3a,$20,$25,$53
	dc.b	$0a,0,7,$66,$72,$65
	dc.b	$65,$73,$70,$20,$22,$2a
	dc.b	2,$58,$70,$0c,$28,$6a
	dc.b	0,$78

	jsr	(a5)
	move.l	$0260(a2),d1
	moveq	#$0c,d0
	movea.l	$0078(a2),a4
	jsr	(a5)
	move.l	$0264(a2),d1
	moveq	#$0c,d0
	movea.l	$0078(a2),a4
	jsr	(a5)
	jmp	(a6)

	dc.b	$4e,$71,$07
	dc.b	'exit	p'
	dc.b	$10,$49,$ec,$ff,$d0

	jsr	(a5)
	moveq	#$10,d0
	lea	$1340(a4),a4
	jsr	(a5)
	move.l	$025c(a2),d1
	moveq	#$10,d0
	movea.l	$0080(a2),a4
	jsr	(a5)
	clr.l	$0024(a1)
	move.l	$001c(a1),d4
	moveq	#$1f,d3
	move.l	$0268(a2),d2
	move.l	$0010(a1),d1
	moveq	#$10,d0
	movea.l	$00c0(a2),a4
	jsr	(a5)
	move.l	(a1),d1
	moveq	#$10,d0
	movea.l	$0008(a2),a4
	jsr	(a5)
	jmp	(a6)

	dc.b	$4e,$71,$07
	dc.b	'start	r'
	dc.b	4,$d2,$89,$e4,$89,$22
	dc.b	$81,$24,$3c,0,0,0
	dc.b	$d4,$d4,$89,$e4,$8a,$23
	dc.b	$42,0,$d0,$42,$a9,1
	dc.b	$0c,$42,$a9,1,$10,$4a
	dc.b	$aa,2,$44,$67,0,0
	dc.b	$24,$47,$ec,$0c,$8c,$22
	dc.b	$0b,$e4,$89,$20,$3c,0
	dc.b	0,1,$20,$28,$6a,1
	dc.b	$24

	jsr	(a5)
	moveq	#$14,d1
	move.l	#$00000120,d0
	movea.l	$0008(a2),a4
	jsr	(a5)
	move.l	$00d0(a1),$025c(a2)
	moveq	#$32,d3
	move.l	(a1),d2
	lea	$0cbc(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000120,d0
	movea.l	$0138(a2),a4
	jsr	(a5)
	tst.l	d1
	bne.l	E17
	lea	$0cc4(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000120,d0
	movea.l	$0124(a2),a4
	jsr	(a5)
	moveq	#$14,d1
	move.l	#$00000120,d0
	movea.l	$0008(a2),a4
	jsr	(a5)
E17	move.l	(a1),d1
	lsl.l	#2,d1
	move.l	0(a0,d1.l),d1
	move.l	#$00000120,d0
	lea	-$0200(a4),a4
	jsr	(a5)
	moveq	#2,d2
	move.l	$0288(a2),d1
	move.l	#$00000120,d0
	movea.l	$004c(a2),a4
	jsr	(a5)
	move.l	d1,$0258(a2)
	move.l	$0284(a2),d1
	move.l	#$00000120,d0
	movea.l	$0074(a2),a4
	jsr	(a5)
	move.l	d1,$0260(a2)
	move.l	$0284(a2),d1
	move.l	#$00000120,d0
	movea.l	$0074(a2),a4
	jsr	(a5)
	move.l	d1,$0264(a2)
	tst.l	$0258(a2)
	beq.l	E18
	tst.l	$0260(a2)
	beq.l	E18
	tst.l	d1
	bne.l	E19
E18	move.l	#$00000120,d0
	lea	-$007c(a4),a4
	jsr	(a5)
	lea	$0cf8(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000120,d0
	movea.l	$0124(a2),a4
	jsr	(a5)
	moveq	#$0a,d1
	move.l	#$00000120,d0
	movea.l	$0008(a2),a4
	jsr	(a5)
E19	move.l	(a1),d1
	lsl.l	#2,d1
	move.l	0(a0,d1.l),d1
	move.l	#$00000120,d0
	movea.l	$0198(a2),a4
	jsr	(a5)
	move.l	d1,$0268(a2)
	tst.l	d1
	bne.l	E20
	move.l	(a1),d2
	lsl.l	#2,d2
	lea	$0d0c(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	0(a0,d2.l),d2
	move.l	#$00000120,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
	move.l	#$00000120,d0
	lea	-$007c(a4),a4
	jsr	(a5)
	moveq	#$14,d1
	move.l	#$00000120,d0
	movea.l	$0008(a2),a4
	jsr	(a5)
E20	move.l	$02a8(a2),d4
	move.l	$02a0(a2),d3
	move.l	$02a4(a2),d2
	move.l	$025c(a2),d1
	move.l	#$00000120,d0
	movea.l	$007c(a2),a4
	jsr	(a5)
	tst.l	d1
	bne.l	E21
	lea	$0d24(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000120,d0
	movea.l	$0124(a2),a4
	jsr	(a5)
	move.l	#$00000120,d0
	lea	-$007c(a4),a4
	jsr	(a5)
	moveq	#$14,d1
	move.l	#$00000120,d0
	movea.l	$0008(a2),a4
	jsr	(a5)
E21	lea	$0d40(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000120,d0
	movea.l	$0124(a2),a4
	jsr	(a5)
	move.l	#$00000120,d0
	lea	$12f4(a4),a4
	jsr	(a5)
	moveq	#-1,d1
	move.l	d1,$0134(a1)
	move.l	$012c(a1),d4
	moveq	#$1f,d3
	move.l	$0268(a2),d2
	move.l	$0120(a1),d1
	move.l	#$00000120,d0
	movea.l	$00c0(a2),a4
	jsr	(a5)
	lea	$0d54(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000120,d0
	movea.l	$0124(a2),a4
	jsr	(a5)
E22	move.l	#$00000120,d0
	movea.l	$00d8(a2),a4
	jsr	(a5)
	move.l	d1,$0114(a1)
	moveq	#-1,d2
	cmp.l	d1,d2
	bne.l	E23
	lea	$0d84(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000124,d0
	lea	-$00b0(a4),a4
	jsr	(a5)
E23	moveq	#$0a,d1
	cmp.l	$0114(a1),d1
	beq.l	E24
	bra.s	E22

E24	moveq	#$0f,d1
	move.l	#$00000120,d0
	lea	$12bc(a4),a4
	jsr	(a5)
	tst.l	d1
	bne.l	E25
	lea	$0d9c(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000120,d0
	lea	-$00b0(a4),a4
	jsr	(a5)
E25	move.l	$025c(a2),d1
	lsl.l	#2,d1
	tst.l	$20(a0,d1.l)
	beq.l	E26
	lea	$0db4(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000120,d0
	lea	-$00b0(a4),a4
	jsr	(a5)
E26	move.l	$0284(a2),d1
	subq.l	#1,d1
	move.l	d1,$0114(a1)
	moveq	#0,d1
E27	move.l	d1,$0118(a1)
	cmp.l	$0114(a1),d1
	bgt.l	E28
	add.l	$0260(a2),d1
	lsl.l	#2,d1
	clr.l	0(a0,d1.l)
	move.l	$0264(a2),d1
	add.l	$0118(a1),d1
	lsl.l	#2,d1
	clr.l	0(a0,d1.l)
	moveq	#1,d1
	add.l	$0118(a1),d1
	bra.s	E27

E28	moveq	#2,d1
	move.l	#$00000120,d0
	lea	$1364(a4),a4
	jsr	(a5)
	move.l	$0284(a2),d1
	subq.l	#1,d1
	move.l	d1,$0114(a1)
	move.l	$026c(a2),d1
E29	move.l	d1,$0118(a1)
	cmp.l	$0114(a1),d1
	bgt.l	E34
	add.l	$0260(a2),d1
	lsl.l	#2,d1
	move.l	0(a0,d1.l),d2
	tst.l	d2
	bge.s	E30
	neg.l	d2
E30	move.l	d2,$011c(a1)
	tst.l	d2
	beq.l	E33
	tst.l	d2
	ble.l	E31
	cmp.l	$0284(a2),d2
	blt.l	E32
E31	move.l	$011c(a1),d3
	move.l	$0118(a1),d2
	lea	$0dd0(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$0000012c,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
	move.l	$0260(a2),d1
	add.l	$0118(a1),d1
	lsl.l	#2,d1
	clr.l	0(a0,d1.l)
	bra.l	E33

E32	move.l	$0260(a2),d1
	add.l	$011c(a1),d1
	lsl.l	#2,d1
	tst.l	0(a0,d1.l)
	bne.l	E33
	move.l	$011c(a1),d1
	neg.l	d1
	move.l	$0260(a2),d2
	add.l	$0118(a1),d2
	lsl.l	#2,d2
	move.l	d1,0(a0,d2.l)
	addq.l	#1,$0110(a1)
E33	moveq	#1,d1
	add.l	$0118(a1),d1
	bra.l	E29

E34	move.l	$0264(a2),d1
	lsl.l	#2,d1
	tst.l	0(a0,d1.l)
	bne.l	E37
	move.l	$0288(a2),d2
	subq.l	#1,d2
	move.l	d2,$0114(a1)
	moveq	#0,d1
E35	move.l	d1,$0118(a1)
	cmp.l	$0114(a1),d1
	bgt.l	E36
	move.l	#$444f5300,d2
	or.l	d1,d2
	add.l	$0258(a2),d1
	lsl.l	#2,d1
	move.l	d2,0(a0,d1.l)
	moveq	#1,d1
	add.l	$0118(a1),d1
	bra.s	E35

E36	lea	$0dfc(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000120,d0
	movea.l	$0124(a2),a4
	jsr	(a5)
	move.l	$02ac(a2),d1
	moveq	#9,d2
	lsl.l	d2,d1
	move.l	d1,d4
	move.l	$028c(a2),d3
	move.l	$0258(a2),d2
	moveq	#$0b,d1
	move.l	#$00000120,d0
	lea	$12bc(a4),a4
	jsr	(a5)
	move.l	d1,$010c(a1)
	tst.l	d1
	bne.l	E37
	lea	$0e30(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000120,d0
	movea.l	$0124(a2),a4
	jsr	(a5)
	moveq	#$14,d1
	move.l	#$00000120,d0
	lea	-$004c(a4),a4
	jsr	(a5)
E37	move.l	$0264(a2),d1
	add.l	$029c(a2),d1
	lsl.l	#2,d1
	tst.l	0(a0,d1.l)
	beq.l	E40
	move.l	$029c(a2),d3
	move.l	$0258(a2),d2
	moveq	#2,d1
	move.l	#$00000120,d0
	lea	$1670(a4),a4
	jsr	(a5)
	moveq	#0,d1
E38	move.l	d1,$0114(a1)
	moveq	#$47,d2
	cmp.l	d2,d1
	bgt.l	E39
	moveq	#6,d3
	add.l	d1,d3
	add.l	$0258(a2),d3
	lsl.l	#2,d3
	clr.l	0(a0,d3.l)
	moveq	#1,d1
	add.l	$0114(a1),d1
	bra.s	E38

E39	bra.l	E45

E40	lea	$0e58(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	d1,$0114(a1)
	move.l	$029c(a2),d3
	move.l	$0258(a2),d2
	moveq	#3,d1
	move.l	#$00000124,d0
	lea	$1670(a4),a4
	jsr	(a5)
	move.l	d1,$010c(a1)
	tst.l	d1
	bne.l	E41
	lea	$0e60(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000124,d0
	movea.l	$0124(a2),a4
	jsr	(a5)
	move.l	$029c(a2),d1
	add.l	$02ac(a2),d1
	moveq	#9,d2
	lsl.l	d2,d1
	move.l	d1,d4
	move.l	$028c(a2),d3
	move.l	$0258(a2),d2
	moveq	#$0b,d1
	move.l	#$00000124,d0
	lea	$12bc(a4),a4
	jsr	(a5)
	move.l	d1,$010c(a1)
	tst.l	d1
	bne.l	E41
	lea	$0e94(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000124,d0
	movea.l	$0124(a2),a4
	jsr	(a5)
	moveq	#$14,d1
	move.l	#$00000124,d0
	lea	-$004c(a4),a4
	jsr	(a5)
E41	move.l	$0288(a2),$0118(a1)
	moveq	#0,d1
E42	move.l	d1,$011c(a1)
	cmp.l	$0118(a1),d1
	bgt.l	E43
	add.l	$0258(a2),d1
	lsl.l	#2,d1
	clr.l	0(a0,d1.l)
	moveq	#1,d1
	add.l	$011c(a1),d1
	bra.s	E42

E43	move.l	$0258(a2),d1
	lsl.l	#2,d1
	moveq	#2,d2
	move.l	d2,0(a0,d1.l)
	move.l	$0258(a2),d1
	lsl.l	#2,d1
	moveq	#$48,d3
	move.l	d3,$0c(a0,d1.l)
	moveq	#$7f,d1
	add.l	$0258(a2),d1
	lsl.l	#2,d1
	moveq	#1,d4
	move.l	d4,0(a0,d1.l)
	moveq	#$79,d1
	add.l	$0258(a2),d1
	move.l	#$00000124,d0
	movea.l	$0158(a2),a4
	jsr	(a5)
	moveq	#$69,d1
	add.l	$0258(a2),d1
	move.l	#$00000124,d0
	movea.l	$0158(a2),a4
	jsr	(a5)
	move.l	$0114(a1),d1
	lsl.l	#2,d1
	moveq	#0,d2
	move.b	0(a0,d1.l),d2
	move.l	d2,$0118(a1)
	moveq	#0,d1
E44	move.l	d1,$011c(a1)
	cmp.l	$0118(a1),d1
	bgt.l	E45
	move.l	$0114(a1),d2
	lsl.l	#2,d2
	add.l	d1,d2
	moveq	#0,d3
	move.b	0(a0,d2.l),d3
	moveq	#$6c,d2
	add.l	$0258(a2),d2
	lsl.l	#2,d2
	add.l	d1,d2
	move.b	d3,0(a0,d2.l)
	moveq	#1,d1
	add.l	$011c(a1),d1
	bra.s	E44

E45	move.l	$0284(a2),d1
	subq.l	#1,d1
	move.l	d1,$0114(a1)
	moveq	#0,d1
E46	move.l	d1,$0118(a1)
	cmp.l	$0114(a1),d1
	bgt.l	E48
	add.l	$0260(a2),d1
	lsl.l	#2,d1
	tst.l	0(a0,d1.l)
	bge.l	E47
	move.l	$029c(a2),d2
	move.l	$0118(a1),d1
	move.l	#$00000128,d0
	lea	$1064(a4),a4
	jsr	(a5)
E47	moveq	#1,d1
	add.l	$0118(a1),d1
	bra.s	E46

E48	move.l	$0260(a2),d1
	add.l	$029c(a2),d1
	lsl.l	#2,d1
	clr.l	0(a0,d1.l)
	moveq	#$4e,d1
	add.l	$0258(a2),d1
	lsl.l	#2,d1
	clr.l	0(a0,d1.l)
	move.l	$029c(a2),d2
	move.l	$0258(a2),d1
	move.l	#$00000120,d0
	lea	$1280(a4),a4
	jsr	(a5)
	move.l	d1,$010c(a1)
	tst.l	d1
	bne.l	E49
	lea	$0ebc(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000120,d0
	movea.l	$0124(a2),a4
	jsr	(a5)
	moveq	#$14,d1
	move.l	#$00000120,d0
	lea	-$004c(a4),a4
	jsr	(a5)
E49	move.l	$0284(a2),d1
	subq.l	#1,d1
	move.l	d1,$0114(a1)
	move.l	$026c(a2),d1
E50	move.l	d1,$0118(a1)
	cmp.l	$0114(a1),d1
	bgt.l	E72
	add.l	$0264(a2),d1
	lsl.l	#2,d1
	tst.l	0(a0,d1.l)
	beq.l	E71
	move.l	#$00000080,d1
	add.l	$0258(a2),d1
	move.l	d1,$011c(a1)
	move.l	#$0000012c,d0
	lea	$1324(a4),a4
	jsr	(a5)
	bra.l	E70

E51	move.l	$0258(a2),d2
	move.l	$0118(a1),d1
	move.l	#$0000012c,d0
	lea	$15fc(a4),a4
	jsr	(a5)
	move.l	d1,$0120(a1)
	move.l	$0258(a2),d2
	lsl.l	#2,d2
	move.l	$08(a0,d2.l),$0124(a1)
	move.l	$0124(a1),$0128(a1)
	move.l	$0258(a2),$012c(a1)
	clr.l	$0134(a1)
	tst.l	d1
	bne.l	E58
	bra.l	E71

E52	moveq	#$48,d1
	cmp.l	$0124(a1),d1
	bge.l	E53
	move.l	d1,$0128(a1)
E53	moveq	#$48,d1
	sub.l	d1,$0124(a1)
	moveq	#$4d,d2
	sub.l	$0128(a1),d2
	addq.l	#1,d2
	move.l	d2,d1
E54	move.l	d1,$0138(a1)
	moveq	#$4d,d2
	cmp.l	d2,d1
	bgt.l	E56
	add.l	$012c(a1),d1
	lsl.l	#2,d1
	move.l	0(a0,d1.l),$013c(a1)
	move.l	$0264(a2),d1
	add.l	$013c(a1),d1
	lsl.l	#2,d1
	moveq	#8,d3
	cmp.l	0(a0,d1.l),d3
	beq.l	E55
	moveq	#-1,d1
	move.l	d1,$0134(a1)
E55	move.l	$0264(a2),d1
	add.l	$013c(a1),d1
	lsl.l	#2,d1
	moveq	#-1,d2
	move.l	d2,0(a0,d1.l)
	moveq	#1,d1
	add.l	$0138(a1),d1
	bra.s	E54

E56	moveq	#$7e,d1
	add.l	$011c(a1),d1
	lsl.l	#2,d1
	move.l	0(a0,d1.l),$0130(a1)
	tst.l	$0130(a1)
	beq.l	E59
	move.l	$0264(a2),d1
	add.l	$0130(a1),d1
	lsl.l	#2,d1
	moveq	#$10,d2
	cmp.l	0(a0,d1.l),d2
	beq.l	E57
	moveq	#-1,d1
	move.l	d1,$0134(a1)
	bra.l	E59

E57	move.l	$011c(a1),d2
	move.l	$0130(a1),d1
	move.l	#$00000144,d0
	lea	$15fc(a4),a4
	jsr	(a5)
	tst.l	d1
	beq.l	E71
	move.l	$0124(a1),$0128(a1)
	move.l	$011c(a1),$012c(a1)
E58	tst.l	$0124(a1)
	bgt.l	E52
E59	tst.l	$0134(a1)
	beq.l	E60
	moveq	#$6c,d1
	add.l	$0258(a2),d1
	move.l	d1,d2
	lea	$0ee0(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000144,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
	move.l	$0264(a2),d1
	add.l	$0118(a1),d1
	lsl.l	#2,d1
	move.l	#$00000400,0(a0,d1.l)
E60	bra.l	E71

E61	move.l	$0258(a2),d2
	move.l	$0118(a1),d1
	move.l	#$0000012c,d0
	lea	$15fc(a4),a4
	jsr	(a5)
	move.l	d1,$0120(a1)
	tst.l	d1
	beq.l	E71
	moveq	#6,d1
E62	move.l	d1,$0124(a1)
	moveq	#$4d,d2
	cmp.l	d2,d1
	bgt.l	E69
	add.l	$0258(a2),d1
	lsl.l	#2,d1
	move.l	0(a0,d1.l),$0128(a1)
	move.l	$0258(a2),d1
	add.l	$0124(a1),d1
	move.l	d1,$012c(a1)
	move.l	$0258(a2),$0130(a1)
	move.l	$0118(a1),$0134(a1)
	bra.l	E67

E63	move.l	$0260(a2),d1
	add.l	$0128(a1),d1
	lsl.l	#2,d1
	clr.l	0(a0,d1.l)
	move.l	$0264(a2),d1
	add.l	$0128(a1),d1
	lsl.l	#2,d1
	moveq	#-3,d2
	cmp.l	0(a0,d1.l),d2
	beq.l	E65
	move.l	$0264(a2),d1
	add.l	$0128(a1),d1
	lsl.l	#2,d1
	moveq	#2,d3
	cmp.l	0(a0,d1.l),d3
	beq.l	E65
	move.l	$0264(a2),d1
	add.l	$0128(a1),d1
	lsl.l	#2,d1
	cmpi.l	#$00000400,0(a0,d1.l)
	beq.l	E65
	moveq	#$6c,d1
	add.l	$0258(a2),d1
	move.l	d1,d2
	lea	$0f0c(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000144,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
	move.l	$012c(a1),d1
	lsl.l	#2,d1
	clr.l	0(a0,d1.l)
	move.l	$0134(a1),d2
	move.l	$0130(a1),d1
	move.l	#$00000144,d0
	lea	$1280(a4),a4
	jsr	(a5)
	tst.l	d1
	bne.l	E64
	move.l	$0134(a1),d2
	lea	$0f58(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000144,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
E64	bra.l	E68

E65	move.l	$011c(a1),d2
	move.l	$0128(a1),d1
	move.l	#$00000144,d0
	lea	$15fc(a4),a4
	jsr	(a5)
	tst.l	d1
	beq.l	E68
	moveq	#$7c,d1
	add.l	$011c(a1),d1
	move.l	d1,$012c(a1)
	move.l	$0128(a1),$0134(a1)
	move.l	d1,d2
	lsl.l	#2,d2
	move.l	0(a0,d2.l),$0128(a1)
	move.l	$011c(a1),$0130(a1)
	move.l	$0128(a1),d3
	cmp.l	$0134(a1),d3
	bne.l	E67
	moveq	#$6c,d4
	add.l	$0130(a1),d4
	move.l	d4,d2
	lea	$0f74(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000144,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
	move.l	$012c(a1),d1
	lsl.l	#2,d1
	clr.l	0(a0,d1.l)
	move.l	$0134(a1),d2
	move.l	$0130(a1),d1
	move.l	#$00000144,d0
	lea	$1280(a4),a4
	jsr	(a5)
	tst.l	d1
	bne.l	E66
	move.l	$0134(a1),d2
	lea	$0f98(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000144,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
E66	bra.l	E68

E67	tst.l	$0128(a1)
	bne.l	E63
E68	moveq	#1,d1
	add.l	$0124(a1),d1
	bra.l	E62

E69	bra.l	E71

E70	move.l	$0264(a2),d1
	add.l	$0118(a1),d1
	lsl.l	#2,d1
	move.l	0(a0,d1.l),d1
	moveq	#-3,d2
	cmp.l	d2,d1
	beq.l	E51
	moveq	#1,d3
	cmp.l	d3,d1
	beq.l	E61
	moveq	#2,d4
	cmp.l	d4,d1
	beq.l	E61
	bra.l	E71

E71	moveq	#1,d1
	add.l	$0118(a1),d1
	bra.l	E50

E72	move.l	$0284(a2),d1
	subq.l	#1,d1
	move.l	d1,$0114(a1)
	move.l	$026c(a2),d1
E73	move.l	d1,$0118(a1)
	cmp.l	$0114(a1),d1
	bgt.l	E83
	add.l	$0264(a2),d1
	lsl.l	#2,d1
	moveq	#2,d2
	cmp.l	0(a0,d1.l),d2
	beq.l	E74
	move.l	$0264(a2),d1
	add.l	$0118(a1),d1
	lsl.l	#2,d1
	moveq	#1,d3
	cmp.l	0(a0,d1.l),d3
	bne.l	E82
E74	move.l	#$00000080,d1
	add.l	$0258(a2),d1
	move.l	d1,$011c(a1)
	move.l	$0258(a2),d2
	move.l	$0118(a1),d1
	move.l	#$0000012c,d0
	lea	$15fc(a4),a4
	jsr	(a5)
	move.l	d1,$0120(a1)
	moveq	#-1,d2
	move.l	d2,$0124(a1)
	clr.l	$0128(a1)
	move.l	#$00000138,d0
	lea	$1324(a4),a4
	jsr	(a5)
	tst.l	$0120(a1)
	beq.l	E82
	moveq	#6,d1
E75	move.l	d1,$012c(a1)
	moveq	#$4d,d2
	cmp.l	d2,d1
	bgt.l	E82
	add.l	$0258(a2),d1
	lsl.l	#2,d1
	move.l	0(a0,d1.l),$0130(a1)
	move.l	$0258(a2),d1
	add.l	$012c(a1),d1
	move.l	d1,$0134(a1)
	move.l	$0258(a2),$0138(a1)
	move.l	$0118(a1),$013c(a1)
	bra.l	E80

E76	move.l	$0260(a2),d1
	add.l	$0130(a1),d1
	lsl.l	#2,d1
	clr.l	0(a0,d1.l)
	move.l	$0264(a2),d1
	add.l	$0130(a1),d1
	lsl.l	#2,d1
	cmpi.l	#$00000400,0(a0,d1.l)
	bne.l	E79
	tst.l	$0124(a1)
	beq.l	E78
	moveq	#$6c,d1
	add.l	$0258(a2),d1
	move.l	d1,d2
	lea	$0fb4(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$0000014c,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
	moveq	#$32,d3
	move.l	(a1),d2
	lea	$0fdc(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$0000014c,d0
	movea.l	$0138(a2),a4
	jsr	(a5)
	moveq	#0,d2
	cmp.l	d2,d1
	beq.s	E77
	not.l	d2
E77	move.l	d2,$0128(a1)
	clr.l	$0124(a1)
E78	tst.l	$0128(a1)
	beq.l	E79
	move.l	$0134(a1),d1
	lsl.l	#2,d1
	clr.l	0(a0,d1.l)
	move.l	$013c(a1),d2
	move.l	$0138(a1),d1
	move.l	#$0000014c,d0
	lea	$1280(a4),a4
	jsr	(a5)
	tst.l	d1
	bne.l	E79
	move.l	$013c(a1),d2
	lea	$0fe4(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$0000014c,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
E79	move.l	$011c(a1),d2
	move.l	$0130(a1),d1
	move.l	#$0000014c,d0
	lea	$15fc(a4),a4
	jsr	(a5)
	tst.l	d1
	beq.l	E81
	moveq	#$7c,d1
	add.l	$011c(a1),d1
	move.l	d1,$0134(a1)
	move.l	$0130(a1),$013c(a1)
	move.l	d1,d2
	lsl.l	#2,d2
	move.l	0(a0,d2.l),$0130(a1)
	move.l	$011c(a1),$0138(a1)
E80	tst.l	$0130(a1)
	bne.l	E76
E81	moveq	#1,d1
	add.l	$012c(a1),d1
	bra.l	E75

E82	moveq	#1,d1
	add.l	$0118(a1),d1
	bra.l	E73

E83	move.l	$0284(a2),d1
	subq.l	#1,d1
	move.l	d1,$0114(a1)
	moveq	#0,d1
E84	move.l	d1,$0118(a1)
	cmp.l	$0114(a1),d1
	bgt.l	E86
	add.l	$0260(a2),d1
	lsl.l	#2,d1
	tst.l	0(a0,d1.l)
	ble.l	E85
	move.l	$0264(a2),d1
	add.l	$0118(a1),d1
	lsl.l	#2,d1
	cmpi.l	#$00000400,0(a0,d1.l)
	beq.l	E85
	move.l	$0260(a2),d1
	add.l	$0118(a1),d1
	lsl.l	#2,d1
	move.l	0(a0,d1.l),$011c(a1)
	move.l	#$0000012c,d0
	lea	$1324(a4),a4
	jsr	(a5)
	move.l	$0258(a2),d2
	move.l	$011c(a1),d1
	move.l	#$0000012c,d0
	lea	$15fc(a4),a4
	jsr	(a5)
	tst.l	d1
	beq.l	E85
	move.l	$011c(a1),d2
	move.l	$0118(a1),d1
	move.l	#$0000012c,d0
	lea	$1064(a4),a4
	jsr	(a5)
	move.l	$011c(a1),d2
	move.l	$0258(a2),d1
	move.l	#$0000012c,d0
	lea	$1280(a4),a4
	jsr	(a5)
	tst.l	d1
	bne.l	E85
	move.l	$011c(a1),d2
	lea	$1000(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$0000012c,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
E85	moveq	#1,d1
	add.l	$0118(a1),d1
	bra.l	E84

E86	moveq	#4,d1
	move.l	#$00000120,d0
	lea	$12bc(a4),a4
	jsr	(a5)
	moveq	#5,d1
	move.l	#$00000120,d0
	lea	$12bc(a4),a4
	jsr	(a5)
	lea	$101c(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	#$00000120,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
	moveq	#0,d1
	move.l	#$00000120,d0
	lea	-$004c(a4),a4
	jsr	(a5)
	jmp	(a6)

	dc.b	'Nq,Disk Doctor cannot be run in the background'
	dc.b	$0a,0,0,0,7,$44
	dc.b	$52,$49,$56,$45,$2f,$41
	dc.b	$30,$55,$73,$61,$67,$65
	dc.b	$3a,$20,$44,$49,$53,$4b
	dc.b	$44,$4f,$43,$54,$4f,$52
	dc.b	$20,$5b,$44,$52,$49,$56
	dc.b	$45,$20,$7b,$44,$46,$30
	dc.b	$3a,$7c,$44,$46,$31,$3a
	dc.b	$7c,$44,$46,$32,$3a,$7c
	dc.b	$44,$46,$33,$3a,$7d,$5d
	dc.b	$0a,0,0,0,$12
	dc.b	'Not enough memory'
	dc.b	$0a,0,$14
	dc.b	'Device %s not found'
	dc.b	$0a,0,0,0,$1b
	dc.b	'Unable to open disk device'
	dc.b	$0a,$11
	dc.b	'Disk Doctor V1.3'
	dc.b	$0a,0,0
	dc.b	'-Insert disk to be corrected and press RETURN',0
	dc.b	0,0,$17
	dc.b	'Unexpected end of file'
	dc.b	$0a,$15
	dc.b	'Unable to access disk'
	dc.b	0
	dc.b	0,$1a
	dc.b	'Disk must be write enabled',0
	dc.b	'(Parent of key %N is %N which is invalid'
	dc.b	$0a,0,0,0
	dc.b	'1Unable to read disk type - formatting track zero'
	dc.b	$0a,0,0
	dc.b	'%Track zero failed to format - Sorry!'
	dc.b	$0a,0,0,$07
	dc.b	'Lazarus0Unable to write to root - formatting root track'
	dc.b	$0a,0,0,0
	dc.b	'%Root track failed to format - Sorry!'
	dc.b	$0a,0,0
	dc.b	'!Cannot write root block - Sorry!'
	dc.b	$0a,0,0
	dc.b	'*Warning: File %S contains unreadable data'
	dc.b	$0a,0
	dc.b	'HATTENTION: Some file in directory %S is unreadable and has bee'
	dc.b	'n deleted'
	dc.b	$0a,0,0,0,$19
	dc.b	'Failed to rewrite key %N'
	dc.b	$0a,0,0
	dc.b	'"Warning: Loop detected at file %S'
	dc.b	$0a,0,$19
	dc.b	'Failed to rewrite key %N'
	dc.b	$0a,0,0
	dc.b	'''Delete corrupt files in directory %S? ',0
	dc.b	7,$59,$3d,$59,$45,$53
	dc.b	$2f,$53,$19
	dc.b	'Failed to rewrite key %N'
	dc.b	$0a,0,0,$19
	dc.b	'Failed to rewrite key %N'
	dc.b	$0a,0,0
	dc.b	'=Now copy files required to a new disk and reformat this disk'
	dc.b	$0a,0,0,7,$69,$6e
	dc.b	$73,$65,$72,$74,$20,$26
	dc.b	$3c,0,0,0,$80,$d6
	dc.b	$aa,2,$58,$23,$43,0
	dc.b	8,$28,$3c,0,0,0
	dc.b	$80,$d8,$83,$23,$44,0
	dc.b	$0c,$23,$6a,2,$58,0
	dc.b	$10,$7a,$6c,$da,$83,$23
	dc.b	$45,0,$14,$26,1,$24
	dc.b	$29,0,8,$72,2,$70
	dc.b	$28,$49,$ec,6,$0c

	jsr	(a5)
	move.l	d1,$001c(a1)
	tst.l	d1
	bne.l	E87
	move.l	(a1),d2
	lea	$0138(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	moveq	#$2c,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
	jmp	(a6)

E87	moveq	#$7d,d1
	add.l	$0008(a1),d1
	lsl.l	#2,d1
	lea	$0150(a4),a3
	move.l	a3,d2
	lsr.l	#2,d2
	move.l	d2,$002c(a1)
	move.l	$0004(a1),d3
	cmp.l	0(a0,d1.l),d3
	bne.l	E88
	lea	$015c(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	bra.l	E89

E88	lea	$0168(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
E89	moveq	#$7f,d2
	add.l	$0008(a1),d2
	lsl.l	#2,d2
	move.l	d1,$0030(a1)
	moveq	#-3,d3
	cmp.l	0(a0,d2.l),d3
	bne.l	E90
	lea	$0174(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	bra.l	E91

E90	lea	$017c(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
E91	move.l	$0014(a1),d4
	move.l	d1,d3
	move.l	$0030(a1),d2
	move.l	$002c(a1),d1
	moveq	#$2c,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
	move.l	$0014(a1),d1
	moveq	#$2c,d0
	lea	$01a4(a4),a4
	jsr	(a5)
	move.l	d1,$0018(a1)
	moveq	#$7d,d2
	add.l	$0008(a1),d2
	lsl.l	#2,d2
	move.l	$0004(a1),0(a0,d2.l)
	move.l	$0010(a1),d1
	add.l	$0018(a1),d1
	lsl.l	#2,d1
	moveq	#$7c,d2
	add.l	$0008(a1),d2
	lsl.l	#2,d2
	move.l	0(a0,d1.l),0(a0,d2.l)
	move.l	$0010(a1),d1
	add.l	$0018(a1),d1
	lsl.l	#2,d1
	move.l	(a1),0(a0,d1.l)
	move.l	(a1),d2
	move.l	$0008(a1),d1
	moveq	#$2c,d0
	lea	$021c(a4),a4
	jsr	(a5)
	tst.l	d1
	bne.l	E92
	move.l	(a1),d2
	lea	$0184(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	moveq	#$2c,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
E92	jmp	(a6)

	dc.b	$4e,$71,$16
	dc.b	'Failed to read key %N'
	dc.b	$0a,0,9,$25,$53,$20
	dc.b	$25,$53,$20,$25,$53,$0a
	dc.b	0,0,$09
	dc.b	'Replacing',0
	dc.b	0,$09
	dc.b	'Inserting',0
	dc.b	0,4,$66,$69,$6c,$65
	dc.b	0,0,0,4,$64,$69
	dc.b	$72,$20,0,0,0,$17
	dc.b	'Failed to write key %'
	dc.b	'N'
	dc.b	$0a,7,$68,$61,$73,$68
	dc.b	$76

	bsr.s	E95
	move.l	d1,d2
	lsl.l	#2,d2
	moveq	#0,d3
	move.b	0(a0,d2.l),d3
	move.l	d3,$0004(a1)
	move.l	d3,$0008(a1)
	moveq	#1,d1
E93	move.l	d1,$000c(a1)
	cmp.l	$0008(a1),d1
	bgt.l	E94
	move.l	$0004(a1),d2
	moveq	#$0d,d1
	jsr	$0010(a5)
	move.l	(a1),d2
	lsl.l	#2,d2
	add.l	$000c(a1),d2
	moveq	#0,d3
	move.b	0(a0,d2.l),d3
	move.l	d1,$0010(a1)
	move.l	d3,d1
	moveq	#$20,d0
	movea.l	$012c(a2),a4
	jsr	(a5)
	add.l	$0010(a1),d1
	andi.l	#$000007ff,d1
	move.l	d1,$0004(a1)
	moveq	#1,d2
	add.l	$000c(a1),d2
	move.l	d2,d1
	bra.s	E93

E94	moveq	#$48,d2
	move.l	$0004(a1),d1
	jsr	$0012(a5)
	addq.l	#$6,d2
	move.l	d2,d1
E95	jmp	(a6)

	dc.b	$4e,$71,7,$70,$75,$74
	dc.b	$62,$6c,$6f,$63,$26,1
	dc.b	$e5,$8b,$23,$70,$38,$14
	dc.b	0,8,$70,$18,$49,$ec
	dc.b	3,$48

	jsr	(a5)
	move.l	$0008(a1),d2
	sub.l	d1,d2
	move.l	(a1),d1
	lsl.l	#2,d1
	move.l	d2,$14(a0,d1.l)
	move.l	$0004(a1),d3
	move.l	(a1),d2
	moveq	#3,d1
	moveq	#$14,d0
	lea	$03f0(a4),a4
	jsr	(a5)
	jmp	(a6)

	dc.b	$4e,$71,7,$44,$6f,$44
	dc.b	$69,$73,$63,$20,$23,$44
	dc.b	0,$2c,$28,3,$26,$02
	dc.b	$24,1,$22,$2a,2,$5c
	dc.b	$70,$1c,$28,$6a,0,$18

	jsr	(a5)
	move.l	$025c(a2),d1
	moveq	#$1c,d0
	movea.l	$0054(a2),a4
	jsr	(a5)
	moveq	#0,d2
	cmp.l	d2,d1
	bne.s	E96
	not.l	d2
E96	move.l	d2,d1
	jmp	(a6)

	dc.b	$4e,$71,$07
	dc.b	'DiscFlur'
	dc.b	4,$70,$0c,$49,$ec,$ff
	dc.b	$c8

	jsr	(a5)
	moveq	#5,d1
	moveq	#$0c,d0
	lea	-$0038(a4),a4
	jsr	(a5)
	moveq	#0,d3
	move.l	$0010(a1),d2
	moveq	#9,d1
	moveq	#$0c,d0
	lea	-$0038(a4),a4
	jsr	(a5)
	jmp	(a6)

	dc.b	$4e,$71,$07
	dc.b	'checkbrr'
	dc.b	1,$70,$0c,$28,$6a,0
	dc.b	$94

	jsr	(a5)
	tst.l	d1
	beq.l	E97
	lea	$002c(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	moveq	#$0c,d0
	movea.l	$0124(a2),a4
	jsr	(a5)
	moveq	#$14,d1
	moveq	#$0c,d0
	lea	-$1370(a4),a4
	jsr	(a5)
E97	jmp	(a6)

	dc.b	$0a,$2a,$2a,$2a
	dc.b	' BREAK'
	dc.b	$0a,0,$07
	dc.b	'DoPass #j'
	dc.b	2,$74,0,4,$22,$2a
	dc.b	2,$70

E98	move.l	d1,$0008(a1)
	cmp.l	$0004(a1),d1
	bgt.l	E113
	move.l	$0278(a2),d2
	subq.l	#1,d2
	move.l	d2,$000c(a1)
	moveq	#0,d1
E99	move.l	d1,$0010(a1)
	cmp.l	$000c(a1),d1
	bgt.l	E112
	moveq	#$20,d0
	lea	-$0040(a4),a4
	jsr	(a5)
	tst.l	$0010(a1)
	bne.l	E100
	move.l	$0008(a1),d2
	lea	$0210(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	moveq	#$20,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
E100	move.l	(a1),$0020(a1)
	move.l	$0258(a2),$0024(a1)
	move.l	$028c(a2),$0028(a1)
	move.l	$0008(a1),d2
	move.l	$0278(a2),d1
	jsr	$0010(a5)
	add.l	$0010(a1),d1
	move.l	$028c(a2),d2
	jsr	$0010(a5)
	move.l	d1,d4
	move.l	$0028(a1),d3
	move.l	$0024(a1),d2
	move.l	$0020(a1),d1
	moveq	#$20,d0
	lea	-$00a8(a4),a4
	jsr	(a5)
	tst.l	d1
	bne.l	E101
	move.l	$0010(a1),d3
	move.l	$0008(a1),d2
	lea	$0228(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	moveq	#$20,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
	bra.l	E111

E101	move.l	$027c(a2),d1
	subq.l	#1,d1
	move.l	d1,$0014(a1)
	moveq	#0,d1
E102	move.l	d1,$0018(a1)
	cmp.l	$0014(a1),d1
	bgt.l	E111
	move.l	d1,d2
	move.l	#$00000080,d1
	jsr	$0010(a5)
	add.l	$0258(a2),d1
	move.l	d1,$001c(a1)
	moveq	#$7f,d2
	add.l	d1,d2
	lsl.l	#2,d2
	move.l	0(a0,d2.l),$0020(a1)
	move.l	$0270(a2),d2
	move.l	$0008(a1),d3
	sub.l	d2,d3
	move.l	$0278(a2),d2
	move.l	d3,d1
	jsr	$0010(a5)
	add.l	$0010(a1),d1
	move.l	$027c(a2),d2
	jsr	$0010(a5)
	add.l	$0018(a1),d1
	move.l	d1,$0024(a1)
	cmp.l	$026c(a2),d1
	bge.l	E103
	add.l	$0264(a2),d1
	lsl.l	#2,d1
	moveq	#-1,d2
	move.l	d2,0(a0,d1.l)
	bra.l	E110

E103	move.l	$001c(a1),d1
	moveq	#$34,d0
	lea	$0264(a4),a4
	jsr	(a5)
	tst.l	d1
	bne.l	E110
	move.l	$001c(a1),d1
	lsl.l	#2,d1
	moveq	#2,d2
	cmp.l	0(a0,d1.l),d2
	bne.l	E108
	moveq	#$7d,d3
	add.l	$001c(a1),d3
	lsl.l	#2,d3
	move.l	0(a0,d3.l),$002c(a1)
	bra.l	E106

E104	lea	$024c(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	d1,$0028(a1)
	bra.l	E107

E105	lea	$0254(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	move.l	d1,$0028(a1)
	bra.l	E107

E106	move.l	$0020(a1),d1
	moveq	#-3,d2
	cmp.l	d2,d1
	beq.s	E104
	moveq	#1,d3
	cmp.l	d3,d1
	beq.s	E105
	moveq	#2,d4
	cmp.l	d4,d1
	beq.s	E105
	bra.l	E107

E107	move.l	$0260(a2),d1
	add.l	$0024(a1),d1
	lsl.l	#2,d1
	move.l	$002c(a1),0(a0,d1.l)
	move.l	$0264(a2),d1
	add.l	$0024(a1),d1
	lsl.l	#2,d1
	move.l	$0020(a1),0(a0,d1.l)
E108	move.l	$001c(a1),d1
	lsl.l	#2,d1
	moveq	#$10,d2
	cmp.l	0(a0,d1.l),d2
	beq.l	E109
	moveq	#8,d3
	cmp.l	0(a0,d1.l),d3
	bne.l	E110
E109	move.l	$001c(a1),d1
	lsl.l	#2,d1
	move.l	$0264(a2),d2
	add.l	$0024(a1),d2
	lsl.l	#2,d2
	move.l	0(a0,d1.l),0(a0,d2.l)
E110	moveq	#1,d1
	add.l	$0018(a1),d1
	bra.l	E102

E111	moveq	#1,d1
	add.l	$0010(a1),d1
	bra.l	E99

E112	moveq	#1,d1
	add.l	$0008(a1),d1
	bra.l	E98

E113	moveq	#$10,d0
	movea.l	$0110(a2),a4
	jsr	(a5)
	jmp	(a6)

	dc.b	$15
	dc.b	'Reading cylinder %I2'
	dc.b	$0d,0,0
	dc.b	' Hard error Track %I2 Surface %N'
	dc.b	$0a,0,0,0,4,$46
	dc.b	$69,$6c,$65,0,0,0
	dc.b	4,$44,$69,$72,$20,0
	dc.b	0,0,$07
	dc.b	'checksuB'
	dc.b	$a9,0,4,$72,0,$23
	dc.b	$41,0,8,$74,$7f,$b2
	dc.b	$82,$6e,0,0,$16,$d2
	dc.b	$91,$e5,$89,$26,$30,$18
	dc.b	0,$d7,$a9,0,4,$72
	dc.b	1,$d2,$a9,0,8,$60
	dc.b	$e0,$22,$29,0,4,$4e
	dc.b	$d6,7,$67,$65,$74,$62
	dc.b	$6c,$6f,$63,$26,1,$72
	dc.b	2,$70,$14,$49,$ec,0
	dc.b	$74

	jsr	(a5)
	move.l	d1,$0008(a1)
	tst.l	d1
	beq.l	E114
	move.l	$0004(a1),d1
	moveq	#$18,d0
	lea	-$0034(a4),a4
	jsr	(a5)
	tst.l	d1
	bne.l	E114
	move.l	$0004(a1),d1
	lsl.l	#2,d1
	moveq	#2,d2
	cmp.l	0(a0,d1.l),d2
	beq.l	E115
E114	move.l	(a1),d2
	lea	$0054(a4),a3
	move.l	a3,d1
	lsr.l	#2,d1
	moveq	#$18,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
	moveq	#0,d1
	jmp	(a6)

E115	moveq	#-1,d1
	jmp	(a6)

	dc.b	$4e,$71,$16
	dc.b	'Key %N now unreadable'
	dc.b	$0a,0,7,$44,$69,$73
	dc.b	$63,$49,$4f,$20,$28,$2a
	dc.b	2,$ac,$d8,$83,$7a,$09
	dc.b	$eb,$ac,$26,$3c,0,0
	dc.b	2,0,$70,$18,$49,$ec
	dc.b	$fc,$4c

	jsr	(a5)
	jmp	(a6)

	dc.b	$4e,$71
	dcb.b	7,0
	dc.b	1,0,0,2,$24,0
	dc.b	0,0,$ab


	END

