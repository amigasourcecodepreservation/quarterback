;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*******************************************************************
*                                                                 *
*                                                                 *
*     DSM MC68000 Disassembler Version 1.0d (09/01/88).           *
*     Copyright (C) 1987, 1988 by OTG Software.                   *
*     All rights reserved.                                        *
*                                                                 *
*     Disassembly of :  ren                                       *
*                                                                 *
*                                                                 *
*******************************************************************


	SECTION	segment0,CODE
seg0
L1	movea.l	$0164(a2),a4
	moveq	#$0c,d0
	jsr	(a5)
	move.l	d1,d2
	move.l	#$00000095,d1
	lea	L1(pc),a4
	movea.l	-$0004(a4),a4
	move.l	a4,-(a7)
	move.l	d2,-(a7)
L2	adda.l	a4,a4
	adda.l	a4,a4
	move.l	a4,d0
	beq.s	L4
	move.l	$0004(a4),d0
	asl.l	#$2,d0
	cmp.l	$00(a4,d0.l),d1
	bge.s	L3
	move.l	$00(a4,d0.l),d1
L3	movea.l	(a4),a4
	bra.s	L2

L4	move.l	d1,d6
	addi.l	#$00000032,d1
	suba.l	a0,a0
	movea.l	$0074(a2),a4
	moveq	#$0c,d0
	jsr	(a5)
	tst.l	d1
	beq.l	L12
	addi.l	#$00000032,d1
	move.l	$0070(a2),d5
	movea.l	d1,a2
	adda.l	a2,a2
	adda.l	a2,a2
	move.l	d1,d7
	movea.l	a2,a0
	move.l	d6,(a0)+
	movea.l	d6,a4
	adda.l	d7,a4
	adda.l	a4,a4
	adda.l	a4,a4
	move.l	#$474c0003,d0
L5	move.l	d0,(a0)+
	addq.l	#$2,d0
	cmpa.l	a4,a0
	ble.s	L5
	suba.l	a0,a0
	move.l	$000c(a7),d0
	lea	$0010(a7),a1
	suba.l	d0,a1
	move.l	#-$00000001,$0004(a1)
	move.l	a1,d1
	subi.l	#$000000a0,d0
	add.l	d1,d0
	move.l	d0,$0008(a1)
	asr.l	#$2,d1
	move.l	d1,$0030(a2)
	move.l	(a7)+,d7
	move.l	d7,d6
	asl.l	#$2,d6
	add.l	$00(a0,d6.l),d7
	asl.l	#$2,d7
L6	addq.l	#$4,d6
	cmp.l	d6,d7
	blt.s	L7
	move.l	$00(a0,d6.l),d1
	movea.l	d5,a4
	moveq	#$0c,d0
	jsr	(a5)
	tst.l	d1
	beq.l	L13
	bra.s	L6

L7	move.l	(a7)+,d1
	movea.l	d5,a4
	moveq	#$0c,d0
	jsr	(a5)
	tst.l	d1
	beq.s	L14
	movea.l	$0218(a2),a4
	moveq	#$0c,d0
	jsr	(a5)
	asl.l	#$2,d1
	movea.l	d1,a3
	move.l	a3,-(a7)
	beq.s	L9
	lea	$0218(a2),a4
	moveq	#$0f,d0
L8	move.l	(a3)+,(a4)+
	dbf	d0,L8
L9	lea	L15(pc),a4
	move.l	a4,$0008(a2)
	movea.l	$0004(a2),a4
	moveq	#$20,d0
	moveq	#0,d1
	jsr	(a5)
	moveq	#0,d0
	move.l	d0,d7
	move.l	(a7)+,d1
	beq.s	L11
	movea.l	d1,a3
	lea	$0218(a2),a4
	moveq	#$0f,d0
L10	move.l	(a4)+,(a3)+
	dbf	d0,L10
L11	move.l	a2,d1
	asr.l	#$2,d1
	subi.l	#$00000032,d1
	movea.l	$0078(a2),a4
	moveq	#$0c,d0
	jsr	(a5)
	move.l	d7,d0
	rts	

L12	tst.l	(a7)+
L13	tst.l	(a7)+
L14	moveq	#-1,d0
	rts	

L15	dc.b	$20,1,$60,$ca,0,0


	SECTION	segment1,CODE
seg1
	dc.b	0,0,0,$4a,0,0
	dc.b	$30,$39,$11
	dc.b	'RENAME		',0
	dc.b	0,$07
	dc.b	'start	r'
	dc.b	$04,$d2,$89,$e4,$89,$22
	dc.b	$81,$76,$32,$24,1,$47
	dc.b	$ec,0,$c0,$22,$0b,$e4
	dc.b	$89,$20,$3c,0,0,0
	dc.b	$dc,$28,$6a,1,$38

	jsr	(a5)
	tst.l	d1
	bne.l	E16
	lea	$00d0(a4),a3
	move.l	a3,d1
	lsr.l	#$2,d1
	move.l	#$000000dc,d0
	movea.l	$0124(a2),a4
	jsr	(a5)
	moveq	#$14,d1
	move.l	#$000000dc,d0
	movea.l	$0008(a2),a4
	jsr	(a5)
E16	move.l	(a1),d1
	lsl.l	#$2,d1
	move.l	d1,d2
	move.l	$04(a0,d2.l),d2
	move.l	$00(a0,d1.l),d1
	move.l	#$000000dc,d0
	movea.l	$016c(a2),a4
	jsr	(a5)
	tst.l	d1
	bne.l	E17
	moveq	#0,d1
	move.l	#$000000dc,d0
	movea.l	$0028(a2),a4
	jsr	(a5)
	move.l	d1,$00d0(a1)
	move.l	(a1),d2
	lsl.l	#$2,d2
	move.l	d2,d3
	lea	$00dc(a4),a3
	move.l	a3,d1
	lsr.l	#$2,d1
	move.l	$04(a0,d3.l),d3
	move.l	$00(a0,d2.l),d2
	move.l	#$000000e0,d0
	movea.l	$0128(a2),a4
	jsr	(a5)
	move.l	$00d0(a1),d2
	moveq	#-1,d1
	move.l	#$000000e0,d0
	movea.l	$0028(a2),a4
	jsr	(a5)
	moveq	#$14,d1
	move.l	#$000000e0,d0
	movea.l	$0008(a2),a4
	jsr	(a5)
E17	jmp	(a6)

	dc.b	$4e,$71,$0e,$46,$52,$4f
	dc.b	$4d,$2f,$41,$2c,$54,$4f
	dc.b	$3d,$41,$53,$2f,$41,0
	dc.b	$09
	dc.b	'Bad args'
	dc.b	$0a,0,0,$16
	dc.b	'Can''t rename %s as %'
	dc.b	's'
	dc.b	$0a
	dcb.b	8,0
	dc.b	1,0,0,0,$24,0
	dc.b	0,0,$5b


	END

