;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*******************************************************************
*                                                                 *
*                                                                 *
*     DSM MC68000 Disassembler Version 1.0d (09/01/88).           *
*     Copyright (C) 1987, 1988 by OTG Software.                   *
*     All rights reserved.                                        *
*                                                                 *
*     Disassembly of :  info                                      *
*                                                                 *
*                                                                 *
*******************************************************************


         SECTION  segment0,CODE
seg0
L1       movea.l  $0164(a2),a4
         moveq    #$0c,d0
         jsr      (a5)
         move.l   d1,d2
         move.l   #$00000095,d1
         lea      L1(pc),a4
         movea.l  -$0004(a4),a4
         move.l   a4,-(a7)
         move.l   d2,-(a7)
L2       adda.l   a4,a4
         adda.l   a4,a4
         move.l   a4,d0
         beq.s    L4
         move.l   $0004(a4),d0
         asl.l    #$2,d0
         cmp.l    $00(a4,d0.l),d1
         bge.s    L3
         move.l   $00(a4,d0.l),d1
L3       movea.l  (a4),a4
         bra.s    L2

L4       move.l   d1,d6
         addi.l   #$00000032,d1
         suba.l   a0,a0
         movea.l  $0074(a2),a4
         moveq    #$0c,d0
         jsr      (a5)
         tst.l    d1
         beq.l    L12
         addi.l   #$00000032,d1
         move.l   $0070(a2),d5
         movea.l  d1,a2
         adda.l   a2,a2
         adda.l   a2,a2
         move.l   d1,d7
         movea.l  a2,a0
         move.l   d6,(a0)+
         movea.l  d6,a4
         adda.l   d7,a4
         adda.l   a4,a4
         adda.l   a4,a4
         move.l   #$474c0003,d0
L5       move.l   d0,(a0)+
         addq.l   #$2,d0
         cmpa.l   a4,a0
         ble.s    L5
         suba.l   a0,a0
         move.l   $000c(a7),d0
         lea      $0010(a7),a1
         suba.l   d0,a1
         move.l   #-$00000001,$0004(a1)
         move.l   a1,d1
         subi.l   #$000000a0,d0
         add.l    d1,d0
         move.l   d0,$0008(a1)
         asr.l    #$2,d1
         move.l   d1,$0030(a2)
         move.l   (a7)+,d7
         move.l   d7,d6
         asl.l    #$2,d6
         add.l    $00(a0,d6.l),d7
         asl.l    #$2,d7
L6       addq.l   #$4,d6
         cmp.l    d6,d7
         blt.s    L7
         move.l   $00(a0,d6.l),d1
         movea.l  d5,a4
         moveq    #$0c,d0
         jsr      (a5)
         tst.l    d1
         beq.l    L13
         bra.s    L6

L7       move.l   (a7)+,d1
         movea.l  d5,a4
         moveq    #$0c,d0
         jsr      (a5)
         tst.l    d1
         beq.s    L14
         movea.l  $0218(a2),a4
         moveq    #$0c,d0
         jsr      (a5)
         asl.l    #$2,d1
         movea.l  d1,a3
         move.l   a3,-(a7)
         beq.s    L9
         lea      $0218(a2),a4
         moveq    #$0f,d0
L8       move.l   (a3)+,(a4)+
         dbf      d0,L8
L9       lea      L15(pc),a4
         move.l   a4,$0008(a2)
         movea.l  $0004(a2),a4
         moveq    #$20,d0
         moveq    #$00,d1
         jsr      (a5)
         moveq    #$00,d0
         move.l   d0,d7
         move.l   (a7)+,d1
         beq.s    L11
         movea.l  d1,a3
         lea      $0218(a2),a4
         moveq    #$0f,d0
L10      move.l   (a4)+,(a3)+
         dbf      d0,L10
L11      move.l   a2,d1
         asr.l    #$2,d1
         subi.l   #$00000032,d1
         movea.l  $0078(a2),a4
         moveq    #$0c,d0
         jsr      (a5)
         move.l   d7,d0
         rts      

L12      tst.l    (a7)+
L13      tst.l    (a7)+
L14      moveq    #-$01,d0
         rts      

L15      dc.b     $20,$01,$60,$ca,$00,$00


         SECTION  segment1,CODE
seg1
         dc.b     $00,$00,$01,$4f,$00,$00
         dc.b     $30,$39,$11
         dc.b     'INFO             ',$00
         dc.b     $00,$07
         dc.b     'printint'
         dc.b     $08,$d4,$89,$e4,$8a,$23
         dc.b     $42,$00,$04,$26,$01,$e5
         dc.b     $8b,$23,$70,$38,$08,$00
         dc.b     $44,$4a,$a9,$00,$44,$67
         dc.b     $00,$02,$9c,$23,$42,$00
         dc.b     $68,$28,$29,$00,$60,$76
         dc.b     $19,$24,$29,$00,$44,$72
         dc.b     $ff,$70,$54,$28,$6a,$00
         dc.b     $c0

         jsr      (a5)
         tst.l    d1
         beq.l    E33
         move.l   (a1),d1
         lsl.l    #$2,d1
         move.l   $28(a0,d1.l),d2
         lea      $02b8(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$54,d0
         movea.l  $0128(a2),a4
         jsr      (a5)
         move.l   $0004(a1),d1
         lsl.l    #$2,d1
         moveq    #$02,d2
         add.l    $0c(a0,d1.l),d2
         move.l   d2,$002c(a1)
         moveq    #$02,d2
         move.l   $002c(a1),d1
         jsr      $0012(a5)
         move.l   d1,$0040(a1)
         move.l   $0004(a1),d2
         lsl.l    #$2,d2
         move.l   $18(a0,d2.l),$0038(a1)
         move.l   $1c(a0,d2.l),$003c(a1)
         bra.l    E31

E16      cmpi.l   #$00002710,$0040(a1)
         ble.l    E17
         lea      $02c0(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         move.l   d1,$0054(a1)
         move.l   #$000003e8,d2
         move.l   $0040(a1),d1
         jsr      $0012(a5)
         move.l   d1,d2
         move.l   $0054(a1),d1
         moveq    #$54,d0
         movea.l  $0128(a2),a4
         jsr      (a5)
         bra.l    E19

E17      cmpi.l   #$000003e8,$0040(a1)
         ble.l    E18
         lea      $02c8(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         move.l   d1,$0054(a1)
         move.l   #$000003e8,d2
         move.l   $0040(a1),d1
         jsr      $0012(a5)
         move.l   d1,$0058(a1)
         move.l   #$000003e8,d2
         move.l   $0040(a1),d1
         jsr      $0012(a5)
         move.l   d2,$005c(a1)
         moveq    #$64,d2
         move.l   $005c(a1),d1
         jsr      $0012(a5)
         move.l   d1,d3
         move.l   $0058(a1),d2
         move.l   $0054(a1),d1
         moveq    #$54,d0
         movea.l  $0128(a2),a4
         jsr      (a5)
         bra.l    E19

E18      move.l   $0040(a1),d2
         lea      $02d4(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$54,d0
         movea.l  $0128(a2),a4
         jsr      (a5)
E19      move.l   $0004(a1),d1
         lsl.l    #$2,d1
         move.l   $08(a0,d1.l),$0030(a1)
         move.l   $10(a0,d1.l),$0034(a1)
         subq.l   #$2,$002c(a1)
         moveq    #$51,d2
         cmp.l    $0030(a1),d2
         bne.l    E20
         lea      $02dc(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$54,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         bra.l    E21

E20      move.l   $0034(a1),d1
         move.l   $002c(a1),d2
         sub.l    d1,d2
         move.l   d2,$005c(a1)
         lea      $02f4(a4),a3
         move.l   a3,d3
         lsr.l    #$2,d3
         move.l   d3,$0054(a1)
         move.l   d1,$0058(a1)
         move.l   d1,d2
         moveq    #$64,d1
         jsr      $0010(a5)
         move.l   $002c(a1),d2
         jsr      $0012(a5)
         move.l   d1,d4
         move.l   $005c(a1),d3
         move.l   $0058(a1),d2
         move.l   $0054(a1),d1
         moveq    #$54,d0
         movea.l  $0128(a2),a4
         jsr      (a5)
E21      move.l   $0004(a1),d1
         lsl.l    #$2,d1
         move.l   $00(a0,d1.l),d2
         lea      $0300(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$54,d0
         movea.l  $0128(a2),a4
         jsr      (a5)
         lea      $0308(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         move.l   d1,$0054(a1)
         moveq    #$52,d2
         cmp.l    $0030(a1),d2
         bne.l    E22
         lea      $0310(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         bra.l    E24

E22      moveq    #$50,d1
         cmp.l    $0030(a1),d1
         bne.l    E23
         lea      $031c(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         bra.l    E24

E23      lea      $0328(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
E24      move.l   d1,$0058(a1)
         tst.l    $003c(a1)
         bne.l    E25
         lea      $0334(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         bra.l    E26

E25      move.l   $003c(a1),d1
         lsl.l    #$2,d1
         move.l   $28(a0,d1.l),d1
E26      move.l   d1,d3
         move.l   $0058(a1),d2
         move.l   $0054(a1),d1
         moveq    #$54,d0
         movea.l  $0128(a2),a4
         jsr      (a5)
         bra.l    E32

E27      lea      $0338(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$54,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         bra.l    E32

E28      lea      $0348(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$54,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         bra.l    E32

E29      lea      $0358(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$54,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         bra.l    E32

E30      lea      $0368(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$54,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         bra.l    E32

E31      move.l   $0038(a1),d1
         moveq    #-$01,d2
         cmp.l    d2,d1
         beq.s    E28
         cmpi.l   #$42414400,d1
         beq.s    E29
         cmpi.l   #$444f5300,d1
         beq.l    E16
         cmpi.l   #$4b49434b,d1
         beq.s    E27
         bra.s    E30

E32      moveq    #$54,d0
         movea.l  $0110(a2),a4
         jsr      (a5)
E33      jmp      (a6)

         dc.b     $04,$25,$53,$3a,$20,$00
         dc.b     $00,$00,$04,$25,$49,$33
         dc.b     $4d,$00,$00,$00,$08,$25
         dc.b     $49,$31,$2e,$25,$49,$31
         dc.b     $4d,$00,$00,$00,$04,$25
         dc.b     $49,$33,$4b,$00,$00,$00
         dc.b     $15
         dcb.b    21,$20
         dc.b     $00,$00,$0b,$25,$49,$38
         dc.b     $25,$49,$38,$25,$49,$34
         dc.b     $25,$25,$05,$25,$49,$34
         dc.b     $20,$20,$00,$00,$06,$25
         dc.b     $54,$41,$20,$25,$53,$00
         dc.b     $0a
         dc.b     'Read/Write',$00
         dc.b     $09
         dc.b     'Read Only',$00
         dc.b     $00,$0a
         dc.b     'Validating',$00
         dc.b     $00,$00,$00,$00,$0e
         dc.b     'Kickstart disk',$00
         dc.b     $0f
         dc.b     'No disk present'
         dc.b     $0f
         dc.b     'Unreadable Disk'
         dc.b     $0e
         dc.b     'Not a DOS disk',$00
         dc.b     $07
         dc.b     'start  r'
         dc.b     $04,$d2,$89,$e4,$89,$22
         dc.b     $81,$70,$3c,$28,$6a,$00
         dc.b     $9c

         jsr      (a5)
         lsl.l    #$2,d1
         move.l   $18(a0,d1.l),d2
         lsl.l    #$2,d2
         move.l   $04(a0,d2.l),$0030(a1)
         move.l   $0030(a1),$0034(a1)
         moveq    #-$01,d1
         move.l   d1,$0038(a1)
         moveq    #$0a,d3
         move.l   (a1),d2
         lea      $0110(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$48,d0
         movea.l  $0138(a2),a4
         jsr      (a5)
         lea      $0118(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$48,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         lea      $012c(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$48,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         bra.l    E36

E34      move.l   $0034(a1),d1
         lsl.l    #$2,d1
         tst.l    $04(a0,d1.l)
         bne.l    E35
         move.l   $0034(a1),d1
         moveq    #$48,d0
         lea      -$0380(a4),a4
         jsr      (a5)
E35      move.l   $0034(a1),d1
         lsl.l    #$2,d1
         move.l   $00(a0,d1.l),$0034(a1)
E36      tst.l    $0034(a1)
         bne.s    E34
         move.l   $0030(a1),$0034(a1)
         bra.l    E41

E37      move.l   $0034(a1),d1
         lsl.l    #$2,d1
         moveq    #$02,d2
         cmp.l    $04(a0,d1.l),d2
         bne.l    E40
         tst.l    $0038(a1)
         beq.l    E38
         lea      $0164(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$48,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         clr.l    $0038(a1)
E38      move.l   $0034(a1),d1
         lsl.l    #$2,d1
         move.l   $28(a0,d1.l),d1
         moveq    #$48,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         move.l   $0034(a1),d1
         lsl.l    #$2,d1
         tst.l    $08(a0,d1.l)
         beq.l    E39
         lea      $017c(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$48,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
E39      moveq    #$48,d0
         movea.l  $0110(a2),a4
         jsr      (a5)
E40      move.l   $0034(a1),d1
         lsl.l    #$2,d1
         move.l   $00(a0,d1.l),$0034(a1)
E41      tst.l    $0034(a1)
         bne.s    E37
         jmp      (a6)

         dc.b     $04,$6e,$6f,$6e,$65,$00
         dc.b     $00,$00,$10,$0a
         dc.b     'Mounted disks:'
         dc.b     $0a,$00,$00,$00
         dc.b     '4Unit Size    Used   '
         dc.b     ' Free Full Errs   Sta'
         dc.b     'tus   Name'
         dc.b     $0a,$00,$00,$00,$14,$0a
         dc.b     'Volumes available:'
         dc.b     $0a,$00,$00,$00,$0a,$20
         dc.b     $5b,$4d,$6f,$75,$6e,$74
         dc.b     $65,$64,$5d
         dcb.b    8,$00
         dc.b     $01,$00,$00,$03,$a4,$00
         dc.b     $00,$00,$4e


         END

