;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*******************************************************************
*                                                                 *
*                                                                 *
*     DSM MC68000 Disassembler Version 1.0d (09/01/88).           *
*     Copyright (C) 1987, 1988 by OTG Software.                   *
*     All rights reserved.                                        *
*                                                                 *
*     Disassembly of :  del                                       *
*                                                                 *
*                                                                 *
*******************************************************************


         SECTION  segment0,CODE
seg0
L1       movea.l  $0164(a2),a4
         moveq    #$0c,d0
         jsr      (a5)
         move.l   d1,d2
         move.l   #$00000095,d1
         lea      L1(pc),a4
         movea.l  -$0004(a4),a4
         move.l   a4,-(a7)
         move.l   d2,-(a7)
L2       adda.l   a4,a4
         adda.l   a4,a4
         move.l   a4,d0
         beq.s    L4
         move.l   $0004(a4),d0
         asl.l    #$2,d0
         cmp.l    $00(a4,d0.l),d1
         bge.s    L3
         move.l   $00(a4,d0.l),d1
L3       movea.l  (a4),a4
         bra.s    L2

L4       move.l   d1,d6
         addi.l   #$00000032,d1
         suba.l   a0,a0
         movea.l  $0074(a2),a4
         moveq    #$0c,d0
         jsr      (a5)
         tst.l    d1
         beq.l    L12
         addi.l   #$00000032,d1
         move.l   $0070(a2),d5
         movea.l  d1,a2
         adda.l   a2,a2
         adda.l   a2,a2
         move.l   d1,d7
         movea.l  a2,a0
         move.l   d6,(a0)+
         movea.l  d6,a4
         adda.l   d7,a4
         adda.l   a4,a4
         adda.l   a4,a4
         move.l   #$474c0003,d0
L5       move.l   d0,(a0)+
         addq.l   #$2,d0
         cmpa.l   a4,a0
         ble.s    L5
         suba.l   a0,a0
         move.l   $000c(a7),d0
         lea      $0010(a7),a1
         suba.l   d0,a1
         move.l   #-$00000001,$0004(a1)
         move.l   a1,d1
         subi.l   #$000000a0,d0
         add.l    d1,d0
         move.l   d0,$0008(a1)
         asr.l    #$2,d1
         move.l   d1,$0030(a2)
         move.l   (a7)+,d7
         move.l   d7,d6
         asl.l    #$2,d6
         add.l    $00(a0,d6.l),d7
         asl.l    #$2,d7
L6       addq.l   #$4,d6
         cmp.l    d6,d7
         blt.s    L7
         move.l   $00(a0,d6.l),d1
         movea.l  d5,a4
         moveq    #$0c,d0
         jsr      (a5)
         tst.l    d1
         beq.l    L13
         bra.s    L6

L7       move.l   (a7)+,d1
         movea.l  d5,a4
         moveq    #$0c,d0
         jsr      (a5)
         tst.l    d1
         beq.s    L14
         movea.l  $0218(a2),a4
         moveq    #$0c,d0
         jsr      (a5)
         asl.l    #$2,d1
         movea.l  d1,a3
         move.l   a3,-(a7)
         beq.s    L9
         lea      $0218(a2),a4
         moveq    #$0f,d0
L8       move.l   (a3)+,(a4)+
         dbf      d0,L8
L9       lea      L15(pc),a4
         move.l   a4,$0008(a2)
         movea.l  $0004(a2),a4
         moveq    #$20,d0
         moveq    #$00,d1
         jsr      (a5)
         moveq    #$00,d0
         move.l   d0,d7
         move.l   (a7)+,d1
         beq.s    L11
         movea.l  d1,a3
         lea      $0218(a2),a4
         moveq    #$0f,d0
L10      move.l   (a4)+,(a3)+
         dbf      d0,L10
L11      move.l   a2,d1
         asr.l    #$2,d1
         subi.l   #$00000032,d1
         movea.l  $0078(a2),a4
         moveq    #$0c,d0
         jsr      (a5)
         move.l   d7,d0
         rts      

L12      tst.l    (a7)+
L13      tst.l    (a7)+
L14      moveq    #-$01,d0
         rts      

L15      dc.b     $20,$01,$60,$ca,$00,$00


         SECTION  segment1,CODE
seg1
         dc.b     $00,$00,$05,$48,$00,$00
         dc.b     $30,$39,$11
         dc.b     'DELETE           ',$00
         dc.b     $00,$07
         dc.b     'match  x'
         dc.b     $10,$d8,$89,$e4,$8c,$23
         dc.b     $44,$00,$0c,$42,$a9,$02
         dc.b     $14,$29,$44,$02,$18,$42
         dc.b     $ac,$02,$1c,$42,$ac,$02
         dc.b     $20,$72,$01,$20,$3c,$00
         dc.b     $00,$02,$24,$49,$ec,$02
         dc.b     $44,$4e,$95,$22,$29,$00
         dc.b     $04,$e5,$89,$74,$00,$14
         dc.b     $30,$18,$00,$4a,$82,$67
         dc.b     $00,$00,$16,$74,$00,$14
         dc.b     $30,$18,$00,$22,$02,$20
         dc.b     $3c,$00,$00,$02,$24,$49
         dc.b     $ec,$02,$44,$4e,$95,$72
         dc.b     $01,$23,$41,$02,$18,$60
         dc.b     $00,$00,$b2,$22,$2c,$02
         dc.b     $18,$d2,$a9,$02,$18,$e5
         dc.b     $89,$23,$70,$18,$00,$02
         dc.b     $1c,$22,$11,$e5,$89,$d2
         dc.b     $a9,$02,$1c,$74,$00,$14
         dc.b     $30,$18,$00,$22,$29,$00
         dc.b     $04,$e5,$89,$d2,$a9,$02
         dc.b     $1c,$76,$00,$16,$30,$18
         dc.b     $00,$23,$42,$02,$20,$23
         dc.b     $43,$02,$24,$60,$00,$00
         dc.b     $56,$72,$01,$d2,$a9,$02
         dc.b     $1c,$20,$3c,$00,$00,$02
         dc.b     $34,$49,$ec,$02,$44

         jsr      (a5)
         move.l   $0224(a1),d1
         move.l   #$00000234,d0
         lea      $0244(a4),a4
         jsr      (a5)
         bra.l    E17

         dc.b     $72,$01,$d2,$a9,$02,$1c
         dc.b     $20,$3c,$00,$00,$02,$34
         dc.b     $49,$ec,$02,$44

         jsr      (a5)
         tst.l    $0224(a1)
         beq.l    E16
         move.l   $0224(a1),d1
         move.l   #$00000234,d0
         lea      $0244(a4),a4
         jsr      (a5)
E16      bra.l    E17

         dc.b     $22,$29,$02,$20,$74,$23
         dc.b     $b2,$82,$67,$a2,$76,$25
         dc.b     $b2,$83,$67,$ae,$78,$28
         dc.b     $b2,$84,$67,$bc,$7a,$7c
         dc.b     $b2,$85,$67,$b6,$60,$b0
E17      dc.b     $52,$a9,$02,$18,$22,$2c
         dc.b     $02,$1c,$b2,$a9,$02,$18
         dc.b     $6c,$00,$ff,$46,$24,$29
         dc.b     $00,$08,$e5,$8a,$76,$00
         dc.b     $16,$30,$28,$00,$b6,$a9
         dc.b     $02,$14,$6e,$00,$00,$08
         dc.b     $22,$2c,$02,$20,$4e,$d6
         dc.b     $4a,$ac,$02,$1c,$66,$00
         dc.b     $00,$06,$72,$00,$4e,$d6
         dc.b     $52,$a9,$02,$14,$22,$29
         dc.b     $00,$08,$e5,$89,$d2,$a9
         dc.b     $02,$14,$74,$00,$14,$30
         dc.b     $18,$00,$29,$42,$02,$2c
         dc.b     $23,$6c,$02,$1c,$02,$18
         dc.b     $42,$ac,$02,$1c,$42,$ac
         dc.b     $02,$20,$23,$69,$02,$18
         dc.b     $02,$1c,$72,$01,$23,$41
         dc.b     $02,$20,$b2,$a9,$02,$1c
         dc.b     $6e,$00,$00,$a0,$d2,$ac
         dc.b     $02,$18,$e5,$89,$23,$70
         dc.b     $18,$00,$02,$24,$22,$11
         dc.b     $e5,$89,$d2,$a9,$02,$24
         dc.b     $74,$00,$14,$30,$18,$00
         dc.b     $23,$42,$02,$28,$60,$00
         dc.b     $00,$4a,$60,$00,$00,$70
         dc.b     $72,$01,$d2,$a9,$02,$24
         dc.b     $24,$11,$e5,$8a,$d4,$81
         dc.b     $72,$00,$12,$30,$28,$00
         dc.b     $23,$41,$02,$28,$22,$2c
         dc.b     $02,$2c,$b2,$a9,$02,$28
         dc.b     $66,$00,$00,$4e,$22,$29
         dc.b     $00,$04,$e5,$89,$d2,$a9
         dc.b     $02,$24,$74,$00,$14,$30
         dc.b     $18,$00,$22,$02,$20,$3c
         dc.b     $00,$00,$02,$38,$49,$ec
         dc.b     $02,$44

         jsr      (a5)
         bra.l    E18

         dc.b     $22,$29,$02,$28,$74,$23
         dc.b     $b2,$82,$67,$ae,$76,$25
         dc.b     $b2,$83,$67,$a8,$78,$27
         dc.b     $b2,$84,$67,$a6,$7a,$28
         dc.b     $b2,$85,$67,$9c,$7c,$3f
         dc.b     $b2,$86,$67,$bc,$7e,$7c
         dc.b     $b2,$87,$67,$90,$60,$a8
E18      dc.b     $72,$01,$d2,$a9,$02,$20
         dc.b     $60,$00,$ff,$58,$60,$00
         dc.b     $fe,$3a,$4e,$71
         dcb.b    36,$00
         dc.b     $07
         dc.b     'put    J'
         dc.b     $81,$66,$00,$00,$0c,$74
         dc.b     $ff,$29,$42,$ff,$dc,$60
         dc.b     $00,$00,$40,$23,$6c,$ff
         dc.b     $d8,$00,$04,$72,$01,$23
         dc.b     $41,$00,$08,$b2,$a9,$00
         dc.b     $04,$6e,$00,$00,$1a,$d2
         dc.b     $ac,$ff,$d4,$e5,$89,$24
         dc.b     $11,$b4,$b0,$18,$00,$67
         dc.b     $00,$00,$1c,$72,$01,$d2
         dc.b     $a9,$00,$08,$60,$dc,$52
         dc.b     $ac,$ff,$d8,$22,$2c,$ff
         dc.b     $d4,$d2,$ac,$ff,$d8,$e5
         dc.b     $89,$21,$91,$18,$00,$4e
         dc.b     $d6,$07,$72,$63,$68,$20
         dc.b     $20,$20,$20,$22,$2c,$ff
         dc.b     $98,$b2,$ac,$ff,$94,$6e
         dc.b     $00,$00,$0c,$74,$ff,$29
         dc.b     $42,$ff,$90,$60,$00,$00
         dc.b     $1a,$52,$ac,$ff,$94,$22
         dc.b     $2c,$ff,$88,$e5,$89,$d2
         dc.b     $ac,$ff,$94,$74,$00,$14
         dc.b     $30,$18,$00,$29,$42,$ff
         dc.b     $90,$4e,$d6,$07
         dc.b     'nextiter'''
         dc.b     $b2,$ac,$ff,$58,$66,$00
         dc.b     $00,$0a,$70,$0c,$49,$ec
         dc.b     $ff,$c8

         jsr      (a5)
         moveq    #$0c,d0
         lea      -$0038(a4),a4
         jsr      (a5)
         jmp      (a6)

         dc.b     $07
         dc.b     'prim   "'
         dc.b     $ac,$ff,$38,$23,$6c,$ff
         dc.b     $34,$00,$04,$70,$14,$49
         dc.b     $ec,$ff,$dc

         jsr      (a5)
         bra.l    E20

         dc.b     $72,$ff,$29,$41,$ff,$40
         dc.b     $22,$11,$4e,$d6,$70,$20
         dc.b     $49,$ec,$00,$00

         jsr      (a5)
         move.l   (a1),d2
         moveq    #$14,d0
         lea      $0114(a4),a4
         jsr      (a5)
         move.l   (a1),d1
         jmp      (a6)

         dc.b     $22,$11,$70,$14,$49,$ec
         dc.b     $00,$8c

         jsr      (a5)
         move.l   d1,(a1)
         moveq    #$29,d2
         cmp.l    -$00cc(a4),d2
         beq.l    E19
         moveq    #-$01,d3
         move.l   d3,-$00c0(a4)
E19      moveq    #$14,d0
         lea      -$0024(a4),a4
         jsr      (a5)
         move.l   (a1),d1
         jmp      (a6)

E20      dc.b     $22,$29,$00,$04,$74,$ff
         dc.b     $b2,$82,$67,$ae,$76,$23
         dc.b     $b2,$83,$67,$b2,$78,$28
         dc.b     $b2,$84,$67,$c2,$7a,$29
         dc.b     $b2,$85,$67,$9c,$7c,$7c
         dc.b     $b2,$86,$67,$96,$60,$9a
         dc.b     $4e,$71,$07
         dc.b     'exp    B'
         dc.b     $a9,$00,$04
E21      dc.b     $70,$14,$49,$ec,$ff,$74

         jsr      (a5)
         move.l   d1,$0008(a1)
         moveq    #$7c,d2
         cmp.l    -$0158(a4),d2
         beq.l    E22
         moveq    #$29,d3
         cmp.l    -$0158(a4),d3
         beq.l    E22
         moveq    #-$01,d4
         cmp.l    -$0158(a4),d4
         bne.l    E24
E22      move.l   $0008(a1),d2
         move.l   $0004(a1),d1
         moveq    #$18,d0
         lea      $00c0(a4),a4
         jsr      (a5)
         move.l   d1,$0004(a1)
         moveq    #$7c,d2
         cmp.l    -$0158(a4),d2
         beq.l    E23
         jmp      (a6)

E23      dc.b     $22,$2c,$fe,$a4,$e5,$89
         dc.b     $d2,$91,$24,$2c,$fe,$ac
         dc.b     $11,$82,$18,$00,$22,$ac
         dc.b     $fe,$ac,$70,$18,$49,$ec
         dc.b     $ff,$50

         jsr      (a5)
         bra.l    E25

E24      dc.b     $24,$2c,$fe,$ac,$22,$29
         dc.b     $00,$08,$70,$18,$49,$ec
         dc.b     $00,$88

         jsr      (a5)
E25      bra.s    E21

         dc.b     $07,$73,$65,$74,$65,$78
         dc.b     $69,$74,$60,$00,$00,$26
         dc.b     $22,$2c,$fe,$1c,$e5,$89
         dc.b     $d2,$91,$74,$00,$14,$30
         dc.b     $18,$00,$23,$42,$00,$08
         dc.b     $22,$2c,$fe,$1c,$e5,$89
         dc.b     $d2,$91,$11,$a9,$00,$07
         dc.b     $18,$00,$22,$a9,$00,$08
         dc.b     $4a,$91,$66,$d8,$4e,$d6
         dc.b     $4e,$71,$07
         dc.b     'join   #A',$00
         dc.b     $08,$4a,$81,$66,$00,$00
         dc.b     $06,$22,$02,$4e,$d6,$60
         dc.b     $00,$00,$12,$22,$2c,$fd
         dc.b     $e4,$e5,$89,$d2,$91,$74
         dc.b     $00,$14,$30,$18,$00,$22
         dc.b     $82,$22,$2c,$fd,$e4,$e5
         dc.b     $89,$d2,$91,$74,$00,$14
         dc.b     $30,$18,$00,$4a,$82,$66
         dc.b     $de,$22,$2c,$fd,$e4,$e5
         dc.b     $89,$d2,$91,$11,$a9,$00
         dc.b     $07,$18,$00,$22,$29,$00
         dc.b     $08,$4e,$d6,$07,$63,$6d
         dc.b     $70,$6c,$70

         bsr.s    E28
         move.l   d1,-$0270(a4)
         move.l   d2,-$026c(a4)
         clr.l    -$0264(a4)
         move.l   d1,d3
         lsl.l    #$2,d3
         moveq    #$00,d4
         move.b   $00(a0,d3.l),d4
         move.l   d4,-$0260(a4)
         clr.l    -$025c(a4)
         move.l   d4,$0008(a1)
         moveq    #$00,d1
E26      move.l   d1,$000c(a1)
         cmp.l    $0008(a1),d1
         bgt.l    E27
         move.l   -$026c(a4),d2
         lsl.l    #$2,d2
         add.l    d1,d2
         moveq    #$00,d3
         move.b   d3,$00(a0,d2.l)
         moveq    #$01,d1
         add.l    $000c(a1),d1
         bra.s    E26

E27      moveq    #$14,d0
         lea      -$01f8(a4),a4
         jsr      (a5)
         moveq    #$00,d1
         moveq    #$20,d0
         lea      -$0110(a4),a4
         jsr      (a5)
         moveq    #$00,d2
         moveq    #$14,d0
         lea      -$0088(a4),a4
         jsr      (a5)
         move.l   -$025c(a4),d1
         not.l    d1
         jmp      (a6)

         dc.b     $4e,$71,$07
         dc.b     'concat '

E28      move.l   d1,d3
         lsl.l    #$2,d3
         moveq    #$00,d4
         move.b   $00(a0,d3.l),d4
         move.l   d2,d5
         lsl.l    #$2,d5
         moveq    #$00,d6
         move.b   $00(a0,d5.l),d6
         move.l   d4,$0008(a1)
         move.l   d6,$000c(a1)
         move.l   d6,$0010(a1)
         moveq    #$01,d1
E29      move.l   d1,$0014(a1)
         cmp.l    $0010(a1),d1
         bgt.l    E30
         move.l   $0004(a1),d2
         lsl.l    #$2,d2
         add.l    d1,d2
         moveq    #$00,d3
         move.b   $00(a0,d2.l),d3
         add.l    $0008(a1),d1
         move.l   (a1),d2
         lsl.l    #$2,d2
         add.l    d1,d2
         move.b   d3,$00(a0,d2.l)
         moveq    #$01,d1
         add.l    $0014(a1),d1
         bra.s    E29

E30      move.l   $0008(a1),d1
         add.l    $000c(a1),d1
         move.l   (a1),d2
         lsl.l    #$2,d2
         move.b   d1,$00(a0,d2.l)
         jmp      (a6)

         dc.b     $07,$6d,$61,$6b,$65,$63
         dc.b     $61,$70,$26,$01,$e5,$8b
         dc.b     $78,$00,$18,$30,$38,$00
         dc.b     $2a,$02,$e5,$8d,$11,$84
         dc.b     $58,$00,$22,$11,$e5,$89
         dc.b     $74,$00,$14,$30,$18,$00
         dc.b     $23,$42,$00,$08,$72,$01
         dc.b     $23,$41,$00,$0c,$b2,$a9
         dc.b     $00,$08,$6e,$00,$00,$2e
         dc.b     $24,$11,$e5,$8a,$d4,$81
         dc.b     $76,$00,$16,$30,$28,$00
         dc.b     $22,$03,$70,$1c,$28,$6a
         dc.b     $01,$2c,$4e,$95,$24,$29
         dc.b     $00,$04,$e5,$8a,$d4,$a9
         dc.b     $00,$0c,$11,$81,$28,$00
         dc.b     $72,$01,$d2,$a9,$00,$0c
         dc.b     $60,$c8,$4e,$d6,$07,$66
         dc.b     $69,$6e,$64,$6e,$65,$78
         dc.b     $60,$00,$00,$3c
E31      dc.b     $72,$10,$d2,$89,$e4,$89
         dc.b     $23,$41,$00,$0c,$74,$02
         dc.b     $d4,$a9,$00,$08,$22,$02
         dc.b     $24,$29,$00,$0c,$70,$40
         dc.b     $49,$ec,$ff,$9c

         jsr      (a5)
         move.l   $000c(a1),d3
         move.l   $0004(a1),d2
         move.l   (a1),d1
         moveq    #$40,d0
         lea      -$05d8(a4),a4
         jsr      (a5)
         tst.l    d1
         beq.l    E32
         moveq    #-$01,d1
         jmp      (a6)

E32      dc.b     $22,$29,$00,$08,$70,$18
         dc.b     $49,$ec,$02,$c4

         jsr      (a5)
         tst.l    d1
         bne.s    E31
         moveq    #$00,d1
         jmp      (a6)

         dc.b     $4e,$71,$07
         dc.b     'addlockr'
         dc.b     $01,$70,$10,$28,$6a,$00
         dc.b     $74

         jsr      (a5)
         move.l   d1,$0004(a1)
         tst.l    d1
         bne.l    E33
         lea      $0050(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$14,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         moveq    #$14,d1
         move.l   d1,d0
         movea.l  $0150(a2),a4
         jsr      (a5)
         bra.l    E34

E33      dc.b     $22,$29,$00,$04,$e5,$89
         dc.b     $21,$aa,$02,$74,$18,$00
         dc.b     $22,$29,$00,$04,$e5,$89
         dc.b     $21,$91,$18,$04,$25,$69
         dc.b     $00,$04,$02,$74

E34      jmp      (a6)

         dc.b     $12,$0a
         dc.b     'Out of Workspace'
         dc.b     $0a,$00,$07,$66,$72,$65
         dc.b     $65,$6c,$6f,$63,$24,$3c
         dc.b     $00,$00,$02,$74,$d4,$8a
         dc.b     $e4,$8a,$23,$42,$00,$04
         dc.b     $23,$6a,$02,$74,$00,$08
         dc.b     $60,$00,$00,$4e,$22,$29
         dc.b     $00,$08,$e5,$89,$24,$11
         dc.b     $b4,$b0,$18,$04,$66,$00
         dc.b     $00,$2c,$22,$02,$70,$18
         dc.b     $28,$6a,$01,$b4

         jsr      (a5)
         move.l   $0008(a1),d1
         lsl.l    #$2,d1
         move.l   $0004(a1),d2
         lsl.l    #$2,d2
         move.l   $00(a0,d1.l),$00(a0,d2.l)
         move.l   $0008(a1),d1
         moveq    #$18,d0
         movea.l  $0078(a2),a4
         jsr      (a5)
         jmp      (a6)

         dc.b     $23,$69,$00,$08,$00,$04
         dc.b     $22,$29,$00,$08,$e5,$89
         dc.b     $23,$70,$18,$00,$00,$08
         dc.b     $4a,$a9,$00,$08,$66,$ae
         dc.b     $4e,$d6,$07
         dc.b     'getlockp'
         dc.b     $10,$28,$6a,$01,$b0

         jsr      (a5)
         move.l   d1,$0004(a1)
         tst.l    d1
         bne.l    E35
         move.l   (a1),d2
         lea      $0044(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$14,d0
         lea      $02f0(a4),a4
         jsr      (a5)
         moveq    #$14,d1
         move.l   d1,d0
         movea.l  $0150(a2),a4
         jsr      (a5)
         bra.l    E36

E35      move.l   $0004(a1),d1
         moveq    #$14,d0
         lea      -$00e0(a4),a4
         jsr      (a5)
E36      move.l   $0004(a1),d1
         jmp      (a6)

         dc.b     '%Could not get inform'
         dc.b     'ation for "%S" - ',$00
         dc.b     $00,$07
         dc.b     'examinep'
         dc.b     $14,$49,$ec,$00,$4c

         jsr      (a5)
         tst.l    d1
         bne.l    E37
         lea      $002c(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$14,d0
         lea      $027c(a4),a4
         jsr      (a5)
         moveq    #$14,d1
         move.l   d1,d0
         movea.l  $0150(a2),a4
         jsr      (a5)
E37      jmp      (a6)

         dc.b     $4e,$71,$17
         dc.b     'Could not examine dis'
         dc.b     'c'
         dc.b     $0a,$07
         dc.b     'exobjec'

E38      move.l   (a1),d1
         lsl.l    #$2,d1
         move.l   (a1),$0028(a1)
         move.l   $0004(a1),$002c(a1)
         move.l   $0020(a1),d4
         moveq    #$17,d3
         move.l   $0c(a0,d1.l),d2
         moveq    #-$01,d1
         moveq    #$14,d0
         movea.l  $00c0(a2),a4
         jsr      (a5)
         move.l   d1,$0008(a1)
         tst.l    d1
         bne.l    E39
         move.l   (a1),d2
         lsl.l    #$2,d2
         move.l   $0c(a0,d2.l),d3
         move.l   (a1),d2
         moveq    #$02,d1
         moveq    #$18,d0
         movea.l  $01c8(a2),a4
         jsr      (a5)
         tst.l    d1
         beq.l    E40
E39      move.l   $0008(a1),d1
         jmp      (a6)

E40      bra.s    E38

         dc.b     $4e,$71,$07,$73,$61,$76
         dc.b     $65,$69,$6e,$66,$72,$00
         dc.b     $23,$41,$00,$04,$74,$38
         dc.b     $b2,$82,$6e,$00,$00,$1e
         dc.b     $d2,$91,$e5,$89,$26,$2a
         dc.b     $02,$68,$d6,$a9,$00,$04
         dc.b     $e5,$8b,$21,$b0,$18,$00
         dc.b     $38,$00,$72,$01,$d2,$a9
         dc.b     $00,$04,$60,$d8,$22,$2a
         dc.b     $02,$68,$70,$10,$49,$ec
         dc.b     $00,$70

         jsr      (a5)
         move.l   d1,$0270(a2)
         moveq    #$00,d1
E41      move.l   d1,$0004(a1)
         moveq    #$38,d2
         cmp.l    d2,d1
         bgt.l    E42
         add.l    (a1),d1
         lsl.l    #$2,d1
         move.l   $026c(a2),d3
         add.l    $0004(a1),d3
         lsl.l    #$2,d3
         move.l   $00(a0,d1.l),$00(a0,d3.l)
         moveq    #$01,d1
         add.l    $0004(a1),d1
         bra.s    E41

E42      jmp      (a6)

         dc.b     $4e,$71,$07,$65,$78,$6e
         dc.b     $65,$78,$74,$20,$72,$00
         dc.b     $23,$41,$00,$04,$74,$38
         dc.b     $b2,$82,$6e,$00,$00,$2a
         dc.b     $d2,$91,$e5,$89,$26,$2a
         dc.b     $02,$6c,$d6,$a9,$00,$04
         dc.b     $e5,$8b,$28,$30,$38,$00
         dc.b     $b8,$b0,$18,$00,$67,$00
         dc.b     $00,$08,$72,$00,$60,$00
         dc.b     $00,$0c,$72,$01,$d2,$a9
         dc.b     $00,$04,$60,$cc,$72,$ff
         dc.b     $4a,$81,$67,$00,$00,$32
         dc.b     $72,$00,$23,$41,$00,$04
         dc.b     $74,$38,$b2,$82,$6e,$00
         dc.b     $00,$1e,$d2,$aa,$02,$68
         dc.b     $e5,$89,$26,$11,$d6,$a9
         dc.b     $00,$04,$e5,$8b,$21,$b0
         dc.b     $18,$00,$38,$00,$72,$01
         dc.b     $d2,$a9,$00,$04,$60,$d8
         dc.b     $22,$2a,$02,$70,$4e,$d6

E43      moveq    #$00,d1
         moveq    #$10,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
         move.l   d1,$0004(a1)
         move.l   d1,d2
         lsl.l    #$2,d2
         move.l   $0c(a0,d2.l),$0008(a1)
         move.l   d1,$002c(a1)
         move.l   (a1),$0030(a1)
         move.l   $0024(a1),d4
         moveq    #$18,d3
         move.l   $0008(a1),d2
         moveq    #-$01,d1
         move.l   d3,d0
         movea.l  $00c0(a2),a4
         jsr      (a5)
         move.l   d1,$000c(a1)
         tst.l    d1
         bne.l    E44
         move.l   $0008(a1),d3
         move.l   $0004(a1),d2
         moveq    #$02,d1
         moveq    #$1c,d0
         movea.l  $01c8(a2),a4
         jsr      (a5)
         tst.l    d1
         beq.l    E45
E44      move.l   $000c(a1),d1
         jmp      (a6)

E45      bra.s    E43

         dc.b     $07,$69,$73,$70

         bsr.s    E51
         move.l   -(a0),d0
         tst.l    d1
         bne.l    E46
         moveq    #$00,d1
         jmp      (a6)

E46      move.l   (a1),d1
         lsl.l    #$2,d1
         moveq    #$00,d2
         move.b   $00(a0,d1.l),d2
         tst.l    d2
         bne.l    E47
         moveq    #$00,d1
         jmp      (a6)

E47      move.l   (a1),d1
         lsl.l    #$2,d1
         moveq    #$00,d2
         move.b   $00(a0,d1.l),d2
         move.l   d2,$0004(a1)
         moveq    #$01,d1
E48      move.l   d1,$0008(a1)
         cmp.l    $0004(a1),d1
         ble.l    E50
         bra.l    E53

E49      moveq    #-$01,d1
         jmp      (a6)

E50      move.l   (a1),d1
         lsl.l    #$2,d1
         add.l    $0008(a1),d1
         moveq    #$00,d2
         move.b   $00(a0,d1.l),d2
         move.l   d2,d1
         moveq    #$23,d2
         cmp.l    d2,d1
         beq.s    E49
         moveq    #$25,d3
         cmp.l    d3,d1
         beq.s    E49
         moveq    #$27,d4
         cmp.l    d4,d1
         beq.s    E49
         moveq    #$28,d5
         cmp.l    d5,d1
         beq.s    E49
         moveq    #$29,d6
         cmp.l    d6,d1
         beq.s    E49
         moveq    #$3f,d7
E51      cmp.l    d7,d1
         beq.s    E49
         moveq    #$7c,d0
         cmp.l    d0,d1
         beq.s    E49
         bra.l    E52

E52      moveq    #$01,d1
         add.l    $0008(a1),d1
         bra.s    E48

E53      moveq    #$00,d1
         jmp      (a6)

         dc.b     $07
         dc.b     'myfaultr',$00
         dc.b     'p'
         dc.b     $14,$28,$6a,$00,$28

         jsr      (a5)
         move.l   d1,$0008(a1)
         move.l   $0004(a1),d2
         move.l   (a1),d1
         moveq    #$18,d0
         movea.l  $0128(a2),a4
         jsr      (a5)
         move.l   $0008(a1),d1
         moveq    #$18,d0
         movea.l  $01a0(a2),a4
         jsr      (a5)
         jmp      (a6)

         dc.b     $4e,$71,$07
         dc.b     'delete.p'
         dc.b     $14,$28,$6a,$01,$68

         jsr      (a5)
         tst.l    d1
         bne.l    E54
         moveq    #$00,d1
         moveq    #$14,d0
         movea.l  $0028(a2),a4
         jsr      (a5)
         move.l   d1,$0008(a1)
         lea      $0068(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$18,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         move.l   $0008(a1),d1
         moveq    #$18,d0
         movea.l  $01a0(a2),a4
         jsr      (a5)
         move.l   $0008(a1),d2
         moveq    #-$01,d1
         moveq    #$18,d0
         movea.l  $0028(a2),a4
         jsr      (a5)
         move.l   $0260(a2),d1
         jmp      (a6)

E54      tst.l    $0004(a1)
         beq.l    E55
         lea      $007c(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$14,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
E55      moveq    #-$01,d1
         jmp      (a6)

         dc.b     $10
         dc.b     '  Not Deleted - ',$00
         dc.b     $00,$00,$0a
         dc.b     '  Deleted'
         dc.b     $0a,$00,$07,$69,$74,$65
         dc.b     $6d,$64,$65,$6c,$26,$2a
         dc.b     $02,$58,$86,$aa,$02,$60
         dc.b     $28,$2a,$02,$64,$46,$84
         dc.b     $c6,$84,$23,$43,$00,$08
         dc.b     $78,$02,$d8,$81,$23,$44
         dc.b     $00,$0c,$4a,$83,$67,$00
         dc.b     $00,$56,$2a,$02,$e5,$8d
         dc.b     $7c,$00,$1c,$30,$58,$00
         dc.b     $da,$86,$7c,$00,$1c,$30
         dc.b     $58,$00,$47,$ec,$00,$94
         dc.b     $2a,$0b,$e4,$8d,$23,$45
         dc.b     $00,$1c,$23,$42,$00,$20
         dc.b     $7e,$3a,$bc,$87,$66,$00
         dc.b     $00,$0e,$47,$ec,$00,$9c
         dc.b     $22,$0b,$e4,$89,$60,$00
         dc.b     $00,$0a,$47,$ec,$00,$a0
         dc.b     $22,$0b,$e4,$89,$28,$29
         dc.b     $00,$0c,$26,$01,$24,$29
         dc.b     $00,$20,$22,$29,$00,$1c
         dc.b     $70,$1c,$28,$6a,$01,$28

         jsr      (a5)
         move.l   (a1),d1
         moveq    #$1c,d0
         lea      -$029c(a4),a4
         jsr      (a5)
         move.l   $0008(a1),d2
         move.l   $000c(a1),d1
         moveq    #$1c,d0
         lea      -$0090(a4),a4
         jsr      (a5)
         jmp      (a6)

         dc.b     $4e,$71,$06,$25,$53,$25
         dc.b     $53,$25,$53,$00,$00,$00
         dc.b     $00,$00,$01,$2f,$00,$00
         dc.b     $07,$68

         bsr.s    E58
         bcc.s    E58
         bcs.s    E56
         move.l   d3,d4
         lsl.l    #$2,d4
         move.l   $0c(a0,d4.l),$000c(a1)
         moveq    #-$01,d5
         move.l   d5,$0010(a1)
         move.l   $000c(a1),d6
         lsl.l    #$2,d6
         moveq    #$00,d7
         move.b   $00(a0,d6.l),d7
         move.l   d7,$0014(a1)
         moveq    #$00,d1
         moveq    #$24,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
         move.l   d1,$0018(a1)
E56      move.l   (a1),d2
         moveq    #-$01,d1
         moveq    #$28,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
         move.l   $000c(a1),d1
         lsl.l    #$2,d1
         add.l    $0014(a1),d1
         moveq    #$00,d2
         move.b   $00(a0,d1.l),d2
         moveq    #$3a,d1
         cmp.l    d1,d2
         beq.l    E57
         lea      $00c8(a4),a3
         move.l   a3,d2
         lsr.l    #$2,d2
         move.l   $000c(a1),d1
         moveq    #$28,d0
         lea      -$066c(a4),a4
         jsr      (a5)
E57      moveq    #$02,d1
         add.l    $0004(a1),d1
         move.l   d1,d2
         move.l   $000c(a1),d1
         moveq    #$28,d0
         lea      -$066c(a4),a4
         jsr      (a5)
         bra.l    E60

E58      EQU      seg1+$00000c02
E59      move.l   $0004(a1),d2
         move.l   $0008(a1),d1
         moveq    #$28,d0
         lea      $00d4(a4),a4
         jsr      (a5)
         move.l   d1,$0010(a1)
         tst.l    d1
         beq.l    E61
E60      move.l   $0004(a1),d1
         moveq    #$28,d0
         lea      -$02d8(a4),a4
         jsr      (a5)
         tst.l    d1
         bne.s    E59
E61      move.l   $000c(a1),d1
         lsl.l    #$2,d1
         move.b   $0017(a1),$00(a0,d1.l)
         move.l   $0018(a1),d2
         moveq    #-$01,d1
         moveq    #$28,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
         move.l   $0010(a1),d1
         jmp      (a6)

         dc.b     $01,$2f,$00,$00,$07
         dc.b     'processv'
         dc.b     $ff,$23,$43,$00,$08,$78
         dc.b     $14,$d8,$89,$e4,$8c,$23
         dc.b     $44,$00,$10,$72,$01,$20
         dc.b     $3c,$00,$00,$01,$08,$28
         dc.b     $6a,$00,$94

         jsr      (a5)
         tst.l    d1
         beq.l    E62
         lea      $00d0(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         move.l   #$00000108,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         moveq    #$00,d1
         jmp      (a6)

E62      moveq    #$02,d1
         add.l    $0004(a1),d1
         move.l   #$00000108,d0
         lea      -$0534(a4),a4
         jsr      (a5)
         move.l   d1,$00f8(a1)
         move.l   $0010(a1),d2
         move.l   #$00000108,d0
         lea      -$04c0(a4),a4
         jsr      (a5)
         tst.l    $0258(a2)
         beq.l    E63
         move.l   $0010(a1),d1
         lsl.l    #$2,d1
         moveq    #$02,d2
         cmp.l    $78(a0,d1.l),d2
         bne.l    E63
         move.l   (a1),d3
         move.l   $0010(a1),d2
         move.l   $00f8(a1),d1
         move.l   #$00000108,d0
         lea      -$00d4(a4),a4
         jsr      (a5)
         move.l   d1,$0008(a1)
E63      move.l   $00f8(a1),d1
         move.l   #$00000108,d0
         lea      -$05a8(a4),a4
         jsr      (a5)
         tst.l    $0008(a1)
         beq.l    E64
         move.l   (a1),d1
         lsl.l    #$2,d1
         move.l   $0c(a0,d1.l),d2
         move.l   $0004(a1),d1
         move.l   #$00000108,d0
         lea      -$0180(a4),a4
         jsr      (a5)
         move.l   d1,$0008(a1)
E64      move.l   $0008(a1),d1
         jmp      (a6)

         dc.b     $4e,$71,$0a,$2a,$2a,$2a
         dc.b     ' BREAK'
         dc.b     $0a,$00,$07
         dc.b     'nextfilt'
         dc.b     $08,$d4,$89,$e4,$8a,$23
         dc.b     $42,$00,$04,$76,$30,$d6
         dc.b     $89,$e4,$8b,$23,$43,$00
         dc.b     $2c,$78,$58,$d8,$89,$e4
         dc.b     $8c,$23,$44,$00,$54,$42
         dc.b     $a9,$01,$3c,$42,$a9,$01
         dc.b     $40,$42,$a9,$01,$44,$42
         dc.b     $a9,$01,$48,$7a,$ff,$23
         dc.b     $45,$01,$4c,$2c,$01,$e5
         dc.b     $8e,$23,$70,$68,$08,$01
         dc.b     $50,$72,$00,$20,$3c,$00
         dc.b     $00,$01,$6c,$28,$6a,$00
         dc.b     $cc

         jsr      (a5)
         move.l   $0054(a1),d2
         move.l   #$00000160,d0
         lea      -$05a4(a4),a4
         jsr      (a5)
         move.l   (a1),d1
         lsl.l    #$2,d1
         move.l   $0150(a1),d4
         move.l   $04(a0,d1.l),d3
         moveq    #$2f,d2
         move.l   $0004(a1),d1
         move.l   #$00000160,d0
         movea.l  $01ac(a2),a4
         jsr      (a5)
         move.l   d1,$0150(a1)
         tst.l    d1
         bne.l    E65
         moveq    #-$01,d2
         move.l   d2,$0140(a1)
E65      move.l   $0004(a1),d1
         move.l   #$00000160,d0
         lea      -$03bc(a4),a4
         jsr      (a5)
         move.l   d1,$0148(a1)
         move.l   $0004(a1),d2
         move.l   d2,d1
         move.l   #$00000160,d0
         lea      -$07b8(a4),a4
         jsr      (a5)
         move.l   $002c(a1),d2
         move.l   $0004(a1),d1
         move.l   #$00000160,d0
         lea      -$0898(a4),a4
         jsr      (a5)
E66      tst.l    $0144(a1)
         beq.l    E67
         moveq    #-$01,d1
         jmp      (a6)

E67      tst.l    $0148(a1)
         beq.l    E68
         move.l   $0054(a1),d3
         move.l   $002c(a1),d2
         move.l   $0004(a1),d1
         move.l   #$00000160,d0
         lea      -$0754(a4),a4
         jsr      (a5)
         move.l   d1,$013c(a1)
         bra.l    E70

E68      tst.l    $013c(a1)
         beq.l    E69
         clr.l    $013c(a1)
         bra.l    E70

E69      move.l   $0004(a1),d1
         move.l   #$00000160,d0
         movea.l  $01b0(a2),a4
         jsr      (a5)
         move.l   d1,$013c(a1)
         tst.l    d1
         beq.l    E70
         move.l   #$00000160,d0
         lea      -$06f8(a4),a4
         jsr      (a5)
         move.l   $0054(a1),d2
         move.l   $013c(a1),d1
         move.l   #$00000160,d0
         lea      -$05a4(a4),a4
         jsr      (a5)
         move.l   $013c(a1),d1
         move.l   #$00000160,d0
         lea      -$068c(a4),a4
         jsr      (a5)
E70      tst.l    $013c(a1)
         bne.l    E71
         moveq    #$00,d1
         jmp      (a6)

E71      tst.l    $0140(a1)
         beq.l    E74
         move.l   (a1),d1
         lsl.l    #$2,d1
         move.l   $0c(a0,d1.l),$0154(a1)
         move.l   $014c(a1),d2
         moveq    #-$01,d1
         move.l   #$00000164,d0
         movea.l  $0028(a2),a4
         jsr      (a5)
         tst.l    $0154(a1)
         beq.l    E72
         move.l   $0154(a1),d1
         lsl.l    #$2,d1
         moveq    #$00,d2
         move.b   $00(a0,d1.l),d2
         add.l    d2,d1
         moveq    #$00,d2
         move.b   $00(a0,d1.l),d2
         moveq    #$3a,d1
         cmp.l    d1,d2
         beq.l    E72
         moveq    #$00,d1
         move.l   #$00000164,d0
         movea.l  $0028(a2),a4
         jsr      (a5)
         tst.l    d1
         beq.l    E72
         move.l   $0154(a1),d1
         lsl.l    #$2,d1
         moveq    #$00,d2
         move.b   $00(a0,d1.l),d2
         subq.l   #$1,d2
         move.b   d2,$00(a0,d1.l)
E72      move.l   $0148(a1),$0260(a2)
         move.l   $0054(a1),d2
         move.l   (a1),d1
         move.l   #$00000164,d0
         lea      -$00e4(a4),a4
         jsr      (a5)
         tst.l    d1
         bne.l    E73
         moveq    #-$01,d1
         move.l   d1,$0144(a1)
E73      clr.l    $014c(a1)
         bra.l    E75

E74      move.l   $0054(a1),d1
         lsl.l    #$2,d1
         moveq    #$02,d2
         cmp.l    $78(a0,d1.l),d2
         bne.l    E75
         moveq    #$00,d1
         move.l   #$00000160,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
         move.l   d1,$0154(a1)
         move.l   (a1),d2
         lsl.l    #$2,d2
         move.l   $0c(a0,d2.l),d3
         lsl.l    #$2,d3
         moveq    #$00,d4
         move.b   $00(a0,d3.l),d4
         move.l   d4,$0158(a1)
         moveq    #$02,d3
         add.l    $0054(a1),d3
         move.l   $0c(a0,d2.l),d1
         move.l   d3,d2
         move.l   #$00000168,d0
         lea      -$0824(a4),a4
         jsr      (a5)
         moveq    #$02,d1
         add.l    $0054(a1),d1
         move.l   #$00000178,d0
         lea      -$0618(a4),a4
         jsr      (a5)
         move.l   d1,d2
         moveq    #-$01,d1
         move.l   #$00000168,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
         move.l   (a1),d1
         lsl.l    #$2,d1
         move.l   $0150(a1),$08(a0,d1.l)
         move.l   (a1),d1
         lsl.l    #$2,d1
         lea      $02e0(a4),a3
         move.l   a3,d2
         lsr.l    #$2,d2
         move.l   $0c(a0,d1.l),d1
         move.l   #$00000168,d0
         lea      -$0824(a4),a4
         jsr      (a5)
         move.l   (a1),d1
         move.l   #$00000168,d0
         lea      $0000(a4),a4
         jsr      (a5)
         move.l   d1,$0144(a1)
         move.l   (a1),d2
         lsl.l    #$2,d2
         move.l   $0c(a0,d2.l),d3
         lsl.l    #$2,d3
         move.b   $015b(a1),$00(a0,d3.l)
         moveq    #$00,d1
         move.l   #$00000174,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
         move.l   #$00000168,d0
         lea      -$068c(a4),a4
         jsr      (a5)
         move.l   $0154(a1),d2
         moveq    #-$01,d1
         move.l   #$00000168,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
E75      bra.l    E66

         dc.b     $01,$2f,$00,$00,$07
         dc.b     'start  r'
         dc.b     $04,$d2,$89,$e4,$89,$22
         dc.b     $81,$24,$3c,$00,$00,$00
         dc.b     $98,$d4,$89,$e4,$8a,$23
         dc.b     $42,$00,$94,$26,$3c,$00
         dc.b     $00,$00,$ac,$d6,$89,$e4
         dc.b     $8b,$23,$43,$00,$a8,$42
         dc.b     $a9,$01,$b0,$42,$a9,$01
         dc.b     $b4,$28,$3c,$00,$00,$01
         dc.b     $bc,$d8,$89,$e4,$8c,$23
         dc.b     $44,$01,$b8,$2a,$3c,$00
         dc.b     $00,$02,$a4,$da,$89,$e4
         dc.b     $8d,$23,$45,$02,$a0,$76
         dc.b     $23,$24,$01,$47,$ec,$02
         dc.b     $cc,$22,$0b,$e4,$89,$20
         dc.b     $3c,$00,$00,$03,$94,$28
         dc.b     $6a,$01,$38

         jsr      (a5)
         tst.l    d1
         bne.l    E76
         lea      $02e8(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         move.l   #$00000394,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         moveq    #$14,d1
         move.l   #$00000394,d0
         movea.l  $0008(a2),a4
         jsr      (a5)
E76      move.l   (a1),d1
         lsl.l    #$2,d1
         move.l   $24(a0,d1.l),$0258(a2)
         move.l   $28(a0,d1.l),$0264(a2)
         clr.l    $025c(a2)
         clr.l    $0274(a2)
         moveq    #$00,d1
         move.l   #$00000394,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
         move.l   d1,$0278(a2)
         move.l   $01b8(a1),$026c(a2)
         move.l   $02a0(a1),$0268(a2)
         moveq    #-$01,d2
         move.l   d2,$0270(a2)
         moveq    #$00,d1
E77      move.l   d1,$0388(a1)
         moveq    #$38,d2
         cmp.l    d2,d1
         bgt.l    E78
         add.l    $026c(a2),d1
         lsl.l    #$2,d1
         clr.l    $00(a0,d1.l)
         moveq    #$01,d1
         add.l    $0388(a1),d1
         bra.s    E77

E78      clr.l    $0260(a2)
         moveq    #$00,d1
         move.l   #$00000394,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
         tst.l    d1
         bne.l    E79
         lea      $02f8(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         move.l   #$00000394,d0
         lea      -$0904(a4),a4
         jsr      (a5)
         move.l   d1,$01b0(a1)
         move.l   d1,d2
         moveq    #-$01,d1
         move.l   #$00000394,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
E79      move.l   (a1),d1
         add.l    $01b4(a1),d1
         lsl.l    #$2,d1
         tst.l    $00(a0,d1.l)
         bne.l    E81
         tst.l    $01b4(a1)
         bne.l    E80
         lea      $02fc(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         move.l   #$00000394,d0
         movea.l  $0124(a2),a4
         jsr      (a5)
         moveq    #$14,d1
         move.l   d1,$025c(a2)
E80      bra.l    E86

E81      move.l   $00a8(a1),d1
         lsl.l    #$2,d1
         moveq    #$00,d2
         move.b   d2,$00(a0,d1.l)
         move.l   $0094(a1),d1
         lsl.l    #$2,d1
         move.l   $00a8(a1),$0c(a0,d1.l)
         move.l   (a1),d1
         add.l    $01b4(a1),d1
         lsl.l    #$2,d1
         move.l   $0094(a1),d3
         lsl.l    #$2,d3
         move.l   $00(a0,d1.l),$04(a0,d3.l)
         move.l   $0094(a1),d1
         lsl.l    #$2,d1
         move.l   d2,$00(a0,d1.l)
         move.l   $0094(a1),d1
         lsl.l    #$2,d1
         moveq    #$01,d3
         move.l   d3,$08(a0,d1.l)
         move.l   (a1),d1
         add.l    $01b4(a1),d1
         lsl.l    #$2,d1
         move.l   $00(a0,d1.l),d1
         move.l   #$00000394,d0
         lea      -$06a8(a4),a4
         jsr      (a5)
         tst.l    d1
         beq.l    E82
         move.l   (a1),d1
         add.l    $01b4(a1),d1
         lsl.l    #$2,d1
         move.l   $00(a0,d1.l),d2
         move.l   $0094(a1),d1
         move.l   #$00000394,d0
         lea      $0370(a4),a4
         jsr      (a5)
         bra.l    E84

E82      move.l   (a1),d1
         add.l    $01b4(a1),d1
         lsl.l    #$2,d1
         move.l   $00(a0,d1.l),$0388(a1)
         move.l   #$00000390,d1
         add.l    a1,d1
         lsr.l    #$2,d1
         move.l   d1,$038c(a1)
         move.l   $0388(a1),d1
         move.l   #$00000480,d0
         lea      -$0904(a4),a4
         jsr      (a5)
         move.l   d1,$0474(a1)
         move.l   $038c(a1),d2
         move.l   #$00000484,d0
         lea      -$0890(a4),a4
         jsr      (a5)
         move.l   $0474(a1),d1
         move.l   #$00000484,d0
         lea      -$0978(a4),a4
         jsr      (a5)
         tst.l    $0258(a2)
         beq.l    E83
         move.l   $038c(a1),d1
         lsl.l    #$2,d1
         moveq    #$02,d2
         cmp.l    $78(a0,d1.l),d2
         bne.l    E83
         move.l   $0388(a1),d2
         move.l   $0094(a1),d1
         move.l   #$00000484,d0
         lea      $0370(a4),a4
         jsr      (a5)
         bra.l    E84

E83      moveq    #$00,d2
         move.l   $0388(a1),d1
         move.l   #$00000484,d0
         lea      -$05e0(a4),a4
         jsr      (a5)
E84      move.l   $0094(a1),d1
         lsl.l    #$2,d1
         tst.l    $00(a0,d1.l)
         beq.l    E85
         moveq    #$00,d1
         move.l   #$000003a0,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
         move.l   #$00000394,d0
         lea      -$0978(a4),a4
         jsr      (a5)
         move.l   $0094(a1),d1
         lsl.l    #$2,d1
         move.l   $00(a0,d1.l),d2
         moveq    #-$01,d1
         move.l   #$00000394,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
E85      addq.l   #$1,$01b4(a1)
         moveq    #$09,d1
         cmp.l    $01b4(a1),d1
         bne.l    E79
E86      move.l   $025c(a2),d1
         move.l   #$00000394,d0
         movea.l  $0150(a2),a4
         jsr      (a5)
         jmp      (a6)

         dc.b     $18
         dcb.b    9,$2c
         dc.b     $41,$4c,$4c,$2f,$53,$2c
         dc.b     $51,$3d,$51,$55,$49,$45
         dc.b     $54,$2f,$53,$00,$00,$00
         dc.b     $0e
         dc.b     'Bad arguments'
         dc.b     $0a,$00,$01,$3a,$00,$00
         dc.b     $12
         dc.b     'No file to delete'
         dc.b     $0a,$00,$07,$74,$69,$64
         dc.b     $79,$75,$70,$20,$60,$00
         dc.b     $00,$2c

E87      move.l   $0274(a2),d1
         lsl.l    #$2,d1
         move.l   $00(a0,d1.l),$0004(a1)
         move.l   $04(a0,d1.l),d1
         moveq    #$14,d0
         movea.l  $01b4(a2),a4
         jsr      (a5)
         move.l   $0274(a2),d1
         moveq    #$14,d0
         movea.l  $0078(a2),a4
         jsr      (a5)
         move.l   $0004(a1),$0274(a2)
         tst.l    $0274(a2)
         bne.s    E87
         move.l   $0278(a2),d2
         moveq    #-$01,d1
         moveq    #$10,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
         move.l   (a1),d1
         moveq    #$10,d0
         movea.l  $0008(a2),a4
         jsr      (a5)
         jmp      (a6)

         dc.b     $4e,$71,$07
         dc.b     'start.nv'
         dc.b     $0c,$d6,$89,$e4,$8b,$23
         dc.b     $43,$00,$08,$42,$a9,$00
         dc.b     $30,$28,$01,$e5,$8c,$23
         dc.b     $70,$48,$0c,$00,$34,$78
         dc.b     $01,$26,$02,$74,$3a,$22
         dc.b     $29,$00,$08,$70,$44,$28
         dc.b     $6a,$01,$ac

         jsr      (a5)
         move.l   d1,$0030(a1)
         tst.l    d1
         beq.l    E89
         lea      $012c(a4),a3
         move.l   a3,d2
         lsr.l    #$2,d2
         move.l   $0008(a1),d1
         moveq    #$48,d0
         lea      -$0e80(a4),a4
         jsr      (a5)
         move.l   $0008(a1),d1
         lsl.l    #$2,d1
         moveq    #$00,d2
         move.b   $00(a0,d1.l),d2
         move.l   $0004(a1),d3
         lsl.l    #$2,d3
         moveq    #$00,d4
         move.b   $00(a0,d3.l),d4
         cmp.l    d4,d2
         bne.l    E88
         move.l   $0004(a1),d2
         lea      $0130(a4),a3
         move.l   a3,d1
         lsr.l    #$2,d1
         moveq    #$48,d0
         movea.l  $0128(a2),a4
         jsr      (a5)
         moveq    #$14,d1
         move.l   d1,$025c(a2)
         jmp      (a6)

E88      move.l   $0008(a1),d2
         move.l   $0034(a1),d1
         moveq    #$48,d0
         lea      -$0e80(a4),a4
         jsr      (a5)
         move.l   $0008(a1),d1
         moveq    #$48,d0
         lea      -$0c74(a4),a4
         jsr      (a5)
         move.l   d1,$0038(a1)
         move.l   (a1),d2
         lsl.l    #$2,d2
         move.l   $0030(a1),$08(a0,d2.l)
         moveq    #$00,d1
         moveq    #$48,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
         move.l   (a1),d2
         lsl.l    #$2,d2
         move.l   d1,$00(a0,d2.l)
         move.l   $0038(a1),d2
         moveq    #-$01,d1
         moveq    #$48,d0
         movea.l  $00cc(a2),a4
         jsr      (a5)
E89      move.l   $0034(a1),d1
         lsl.l    #$2,d1
         moveq    #$00,d2
         move.b   $00(a0,d1.l),d2
         tst.l    d2
         bne.l    E90
         move.l   $021c(a2),d2
         move.l   $0034(a1),d1
         moveq    #$44,d0
         lea      -$0e80(a4),a4
         jsr      (a5)
         move.l   $0034(a1),d1
         lsl.l    #$2,d1
         moveq    #$00,d2
         move.b   $00(a0,d1.l),d2
         add.l    d2,d1
         moveq    #$00,d2
         move.b   $00(a0,d1.l),d2
         moveq    #$3a,d1
         cmp.l    d1,d2
         beq.l    E90
         lea      $0158(a4),a3
         move.l   a3,d2
         lsr.l    #$2,d2
         move.l   $0034(a1),d1
         moveq    #$44,d0
         lea      -$0e80(a4),a4
         jsr      (a5)
E90      move.l   (a1),d1
         moveq    #$44,d0
         lea      -$065c(a4),a4
         jsr      (a5)
         jmp      (a6)

         dc.b     $4e,$71,$01,$3a,$00,$00
         dc.b     '%%S is a device and c'
         dc.b     'annot be deleted'
         dc.b     $0a,$00,$00,$01,$2f
         dcb.b    9,$00
         dc.b     $01,$00,$00,$10,$3c,$00
         dc.b     $00,$00,$54,$00,$00,$13
         dc.b     $54,$00,$00,$00,$9e


         END

