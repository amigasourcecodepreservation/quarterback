;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;

* These text strings appear inside gadgets, and therefore, are very limited
* in space.  Please try to limit the translated text as much as possible.


AbtTxt	GTEXT	BLACK,0,JAM1,68,2,'ABORT',0

RootTxt	GTEXT	WHITE,BLACK,JAM2,17,2,'ROOT',0

ParTxt	GTEXT	WHITE,BLACK,JAM2,10,2,'PARENT',0

ResCTxt	GTEXT	WHITE,BLACK,JAM2,4,2,<'Restore files to'>,ResC1
ResC1	GTEXT	WHITE,BLACK,JAM2,12,12,<'current volume'>,0

ResDTxt	GTEXT	WHITE,BLACK,JAM2,16,2,<'Copy files to'>,ResD1
ResD1	GTEXT	WHITE,BLACK,JAM2,4,12,<'different volume'>,0

DSELTXT	TEXTZ	'Exclude'
SELTXT	TEXTZ	'Include'


CAN1TXT	GTEXT	BLACK,WHITE,JAM1,14,2,'Cancel',0
PWTXT	GTEXT	BLACK,WHITE,JAM1,8,2,<'Proceed'>,0
OFSTXT	GTEXT	BLACK,WHITE,JAM1,4,2,<'Old File System'>,0
FFSTXT	GTEXT	BLACK,WHITE,JAM1,4,2,<'New File System'>,0
NEGTXT	GTEXT	ORANGE,WHITE,JAM2,8,3,<'   NO  '>,0
POSTXT	GTEXT	BLACK,WHITE,JAM2,8,3,<'  YES '>,0
OKAYTXT	GTEXT	BLACK,WHITE,JAM2,24,3,<'Okay'>,0
OKTXT	GTEXT	BLACK,WHITE,JAM2,8,3,<'Okay'>,0
SKIPTXT	GTEXT	BLACK,WHITE,JAM1,2,2,'Next File',0
REPTXT	GTEXT	BLACK,WHITE,JAM1,6,2,<'Replace'>,0
SALVTXT	GTEXT	BLACK,WHITE,JAM1,8,2,<'Salvage'>,0
RETRYTXT
	GTEXT	BLACK,WHITE,JAM1,16,2,<'Retry'>,0

CTXT	TEXTZ	'CANCEL'
PTXT	TEXTZ	'PROCEED'
PZTXT	TEXTZ	'PAUSE'
CONTXT	TEXTZ	'CONTINUE'

