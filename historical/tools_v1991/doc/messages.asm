;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*							*
*	QB TOOLS language-dependent text messages	*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1990 Central Coast Software	*
*	424 Vista Avenue, Golden, CO 80401		*
*********************************************************

	SECTION	MESSAGES,DATA

	XDEF	MONTHS
	XDEF	RunErr.
	XDEF	ArexErr.
	XDEF	ALibErr.

TXT	MACRO	;label, text string
	XDEF	\1
\1	DC.B	\2,0
	ENDM

TEXTZ	MACRO
	DC.B	\1,0
	ENDM

MONTHS	TEXTZ	'JAN'
	TEXTZ	'FEB'
	TEXTZ	'MAR'
	TEXTZ	'APR'
	TEXTZ	'MAY'
	TEXTZ	'JUN'
	TEXTZ	'JUL'
	TEXTZ	'AUG'
	TEXTZ	'SEP'
	TEXTZ	'OCT'
	TEXTZ	'NOV'
	TEXTZ	'DEC'

*******************************************************************
* These messages may appear if Tools is started from CLI or AREXX.

RunErr.		DC.B	17,'Usage: TOOLS -R',13,10
ArexErr.	DC.B	26,'Error opening Arexx port',13,10
ALibErr.	DC.B	24,'Rexx library not found',13,10

*******************************************************************
* These are Tools main menu or submenu text strings.
* Lengths greater than 55 chars will require moving gadgets on the screen
* to make room.  This can be done, but makes the translated version
* more difficult to maintain.

 TXT Fun1.,<'Display volume statistics.'>
 TXT Fun3.,<'Go to Volume Reorganization Menu.'>
 TXT Fun4.,<'Restore deleted/lost files and drawers.'> 
 TXT Fun5.,<'Go to Volume Repair Menu.'>
 TXT Fun6.,<'Format volume.'>
 TXT Fun7.,<'Unformat volume.'>

 TXT Fun10.,<'Find unreadable blocks; mark them "out of service".'>
 TXT Fun11.,<'Find (but don''t repair) bad files and drawers.'>
 TXT Fun12.,<'Find and Repair bad files and drawers.'>

 TXT Fun21.,<'Attempt to repair bad volume.'>
 TXT Fun22.,<'Restore files to a different volume.'>
 TXT Fun23.,<'Format bad volume.'>

 TXT Fun30.,<'Display volume space fragmentation.'>
 TXT Fun31.,<'Count fragmented files, but don''t reorganize.'>
 TXT Fun32.,<'Reorganize volume and defragment files.'>

 TXT RetMenu.,<'Return to Main Menu.'>

 TXT MainMenu.,<'MAIN MENU'>
 TXT BadMenu.,<'BAD VOLUME MENU'>
 TXT ReorgMenu.,<'VOLUME REORGANIZATION MENU'>
 TXT RepairMenu.,<'VOLUME REPAIR MENU'>
 
**********************************************************************

 TXT DiffDrv.,<'Select a different drive.'>
 TXT DiffVol.,<'Select a different volume.'>

**********************************************************************
* These text strings appear on the volume statistics window:

 TXT ParamTitle1.,<'Statistics for volume '>
 TXT ParamTitle2.,<' on disk drive '>
 TXT ParamTitle3.,<'Statistics for disk drive '>
 TXT Parm1.,<'Percent full:          '>
 TXT Parm2.,<'Volume size in KB:     '>
 TXT Parm2M.,<'Volume size in MB:     '>
 TXT Parm3.,<'Volume size in blocks: '>
 TXT Parm4.,<'Free space in KB:      '>
 TXT Parm4M.,<'Free space in MB:      '>
 TXT Parm5.,<'Free space in blocks:  '>
 TXT Parm6.,<'Low cylinder:          '>
 TXT Parm7.,<'High cylinder:         '>
 TXT Parm8.,<'Number of cylinders:   '>
 TXT Parm9.,<'Number of surfaces:    '>
 TXT Parm10.,<'Blocks per track:      '>
 TXT Parm11.,<'Reserved blocks:       '>
 TXT Parm19.,<'Preallocated blocks:   '>
 TXT Parm12.,<'Maxtransfer (KB):      '>
 TXT Parm13.,<'Device driver name:    '>
 TXT Parm14.,<'Filing system:         '>
 TXT Parm15.,<'Volume status:         '>
 TXT Parm16.,<'Block size (bytes):    '>
 TXT Parm17.,<'Known bad blocks:      '>
 TXT Parm18.,<'Memory type:           '>

 TXT Stat1.,<'No disk loaded'>
 TXT Stat2.,<'Not an AmigaDOS volume'>
 TXT Stat3.,<'Valid AmigaDOS volume; write protected'>
 TXT Stat4.,<'Not valid for writing'>
 TXT Stat5.,<'Valid AmigaDOS volume'>

 TXT Fast.,<'New'>
 TXT Old.,<'Old'>
 TXT MTChip.,<'Chip'>
 TXT MTFast.,<'Fast'>
 TXT MTPublic.,<'Public'>
 TXT Period.,<'.'>
 TXT Ques.,<'??'>

**********************************************************************
* These text strings appear in error requesters (upper left corner)
* or on the screen in various places.  Generally, the length of these
* strings is not critical, but try to keep the translated text as short
* as possible!

 TXT Unnamed.,<'Unnamed'>
 TXT VolLst.,<'Here are the AmigaDOS volumes currently available.'>
 TXT ClkVol.,<'Click on the name of the volume you wish to process.'>
 TXT DrvLst.,<'Here are the disk drives currently available.'>
 TXT ClkDrv.,<'Click on the name of the disk drive you wish to process.'>
 TXT NoDev.,<' is not available.'>
 TXT DrvOpnErr.,<'Unable to open device @'>
 TXT MaxBlkErr.,<'Discrepancy in volume size in blocks.'>
 TXT FunVol.,<'Volume selected: @'>
 TXT FunDrv.,<'Disk drive selected: @'>

 TXT BlkScanTitle.,<'Searching volume @ for unreadable blocks.'>
 TXT BitmapTitle.,<'Space usage map for volume @'>
 TXT Rest1.,<'Building catalog of active and deleted files.'>
 TXT Rest2.,<'Restoring selected files to volume @.'>
 TXT Copy1.,<'Copying selected files from volume @.'>

 TXT Analyze.,<'Checking fragmentation and integrity of volume @'> 
 TXT Defrag.,<'Reorganizing and defragmenting volume @'>
 TXT PrepReorg.,<'Preparing to reorganize volume @'>

 TXT RexFragCnt.,<'Found % fragmented files.'>

 TXT NoParms.,<'Cannot display volume parameters.'>

 TXT BMInvalid.,<'Bitmap is invalid.'>
 TXT BMRdErr.,<'Disk error reading bitmap.'>
 TXT BMWrtErr.,<'Error writing bitmap.'>
 TXT CtrlRngErr.,<'Control key out of range.'>
 TXT CtrlNoCache.,<'No cache for control block.'>
 TXT CtrlCkSum.,<'Control block checksum error.'>
 TXT CtrlRdErr.,<'Disk error reading control block.'>
 TXT DataRdErr.,<'Disk error reading data block.'>
 TXT RootRdErr.,<'Disk error reading root block.'>
 TXT RootWtErr.,<'Error writing root block.'>
 TXT RootGarb.,<'Root drawer damaged'>
 TXT RootErr.,<'Root drawer damaged - files inaccessible'>
 TXT NoMemErr.,<'Not enough available memory.'>
 TXT BMCkSum.,<'Bitmap checksum error.'>
 TXT Frag.,<'Unused space is broken into % fragments.   '>
 TXT NoFrag.,<'Unused space is not fragmented.'>
 TXT NoFree.,<'This volume is completely full.'>
 TXT RexBlkErr.,<'Found % new unreadable blocks.'>

 TXT ActFil.,<'Active files:    '>
 TXT ActDir.,<'Active drawers:  '>
 TXT DelFil.,<'Deleted files:   '>
 TXT DelDir.,<'Deleted drawers: '>

 TXT NoFilesSel.,<'No files selected to restore!'>
 TXT BBReadErr.,<'Error reading Bad.Blocks file.'>
 TXT BBWriteErr.,<'Error writing Bad.Blocks file.'>
 TXT TooManyBad.,<'Too many bad blocks- reformat volume'>
 TXT DiskFull.,<'Volume full.'>
 TXT OpenErr.,<'Can''t copy file to destination.'>
 TXT CantSub.,<'Can''t create new drawer on volume.'>

 TXT BadPath.,<'Can''t find volume '>

 TXT RestFile.,<'Restoring file: '>
 TXT CopyFile.,<'Copying file: '>

 TXT NoDisk.,<'No disk loaded in drive @.'>
 TXT DeleteErr.,<'Error deleting file.'>

 TXT InvFilNam.,<'Illegal character in volume name.'>
 TXT NameTooLong.,<'Volume name limited to 30 characters.'>
 TXT LgFree.,<' unused '>
 TXT LgUsed.,<'  used  '>

 TXT BadScanFind.,<'Searching for bad files and drawers on volume @.'>
 TXT BadScanFix.,<'Repairing bad files and drawers on volume @.'>
 TXT BadCtrl.,<'Invalid control block'>
 TXT NotSafe.,<'Disk too full; Unable to save old format.'>

 TXT ProcDir.,<'Processing drawer: '>
 TXT ProcFile.,<'Processing file:   '>

 TXT FmtVol.,<'Formatting volume @.'>
 TXT UnFmtVol.,<'Restoring previously saved format to volume @.'>
 TXT FmtErr.,<'Error formatting volume.'>
 TXT FmtFlop.,<'Formatting floppy disk volume @.'>

 TXT RepairRpt.,<'Quarterback Tools Test Report for @, %'>
 TXT RepairName.,<'@.QBTR.%'>
 TXT NotFlop.,<' is not a floppy drive.'>

 TXT BadCnt.,<'Unreadable blocks found: '>

 TXT PartSizeErr.,<'Partition size too large for QB Tools!'>
 TXT Proc.,<'Processing: '>

*********************************************************************
* Fatal errors.  These should never appear.

 TXT FEGetCache.,<'FE: out of cache buffers'>
 TXT Fatal1.,<'Fatal error 1, dir already in list'>
 TXT Fatal2.,<'Fatal error 2, remapping remap key'>

*********************************************************************
* These strings appear in the catalog window after Tools scans the
* volume for lost or deleted files to be copied or restored.

 TXT CTxt1.,<'Click on file name to'>
 TXT CTxt2.,<'select for restore.'>
 TXT CTxt3.,<'Click on drawer name'>
 TXT CTxt4.,<'to examine contents.'>

 TXT CFil.,<'Files selected:'>
 TXT CAct.,<'Active:  '>
 TXT CDel.,<'Deleted: '>

 TXT FSel.,<'Selected to restore:'>
 TXT OutOf.,<' of '>
 TXT CatFor.,<'Catalog for: '>
 TXT SubDir.,<'      (Drawer)         '>

*************************************************************************
* The next 2 test strings appear as the first line of the requester
* which appears when Tools finds a problem during REPAIR:
* (43 CHARS MAX)

 TXT MajorErr.,<'** A VERY SERIOUS PROBLEM HAS BEEN FOUND **'>
 TXT MinorErr.,<'     A Minor Problem Has Been Found'>

* These next 4 lines are part of the REPAIR problem requester

 TXT Drawer.,<'Drawer: '>
 TXT Entry.,<'Entry:  '>
 TXT Error.,<'Error:  '>
 TXT Action.,<'Action: '>

* One of these words is inserted at the "%" of the error strings in the
* module ERRORS.ASM:

 TXT FilHdr.,<'File '>
 TXT DirBlk.,<'Drawer '>

*************************************************************************
* The following is the repair summary which Tools displays (and may
* also print) following a REPAIR operation:

 TXT TestRes.,<'Test results for volume '>
 TXT SerProb.,<'Serious problems found:   '>
 TXT MinProb.,<'Minor problems found:     '>
 TXT TotFiles.,<'Total files processed:    '>
 TXT BadFiles.,<'Bad files deleted:        '>
 TXT TotDrawers.,<'Total drawers processed:  '>
 TXT BadDrawers.,<'Bad drawers detected:     '>
 TXT BadBlocks.,<'Unreadable blocks found:  '>
 TXT VolStatus.,<'Volume status: '>
 TXT VolNoProb.,<'All files appear to be good.'>
 TXT VolLostFiles.,<'Major problems corrected; volume usable.'>
 TXT VolBad1.,<'Minor problems; use Find and Repair to fix'>
 TXT VolBad2.,<'Volume bad; RESTORE files to different volume'>
 TXT VolBad3.,<'Major problems; use Find and Repair to fix'>
 TXT VolFixed.,<'Minor problems corrected; volume usable'>

***********************************************************************
* The following are complete requester text strings into which the volume
* name may be inserted at the "@", numbers of up to five digits may
* be inserted at the "%", and a file name may be inserted at the "$".
* Maximum size is 43 characters INCLUDING the inserted volume or file
* name or number.

 TXT BBExist1.,<'Volume @'>
 TXT BBExist2.,<'has % blocks already known to be unreadable.'>
 TXT BBExist3.,<'Do you want to recheck these known bad'>
 TXT BBExist4.,<'blocks?'>

 TXT NoBlkEr1.,<'There are no unreadable blocks on volume'>
 TXT NoBlkEr2.,<'@'>

 TXT BlkEr1.,<'Found % unreadable blocks on volume'>
 TXT BlkEr2.,<'@.'>
 TXT BlkEr3.,<'Do you want to mark these blocks "bad"'>
 TXT BlkEr3a.,<'to avoid using them in a file?'>


 TXT BlkEr4.,<'Found % unreadable blocks, of which ^'>
 TXT BlkEr5.,<'were already marked "bad". Do you'>
 TXT BlkEr6.,<'want to mark the remaining blocks'>
 TXT BlkEr7.,<'"bad" to avoid using them?'>

 TXT BlkEr8.,<'Found no new unreadable blocks on volume'>
 TXT BlkEr9.,<'@,'>
 TXT BlkEr10.,<'which has % known unreadable blocks.'>

 TXT BlkWrn1.,<'DANGER - % bad blocks may be part of'>
 TXT BlkWrn2.,<'active files. Use FIND mode to identify'>
 TXT BlkWrn3.,<'files, RESTORE files to another volume,'>
 TXT BlkWrn4.,<'delete them, and then rerun this test.'>

 TXT FragFiles1.,<'Fragmentation check results for volume'>
 TXT FragFiles2.,<'@'>
 TXT FragFiles3.,<'Fragmented files: % out of ^'>
 TXT FragFiles4.,<'Volume free space fragments: '>

 TXT CpyWrn1.,<'CAUTION - Repairing a bad volume may cause'>
 TXT CpyWrn2.,<'loss of files. You should copy critical files'>
 TXT CpyWrn3.,<'to a different volume before attempting to'>
 TXT CpyWrn4.,<'repair this volume. DO YOU WANT TO COPY FILES'>
 TXT CpyWrn5.,<'FROM THIS VOLUME TO A DIFFERENT VOLUME?'>

 TXT NoDelEr1.,<'No deleted files were found on volume'>
 TXT NoDelEr2.,<'@'>

 TXT DelNam1.,<'Unable to find a deleted file named'>
 TXT DelNam2.,<'$.'

 TXT ResNam1.,<'Enter the name of the file to be restored, or'>
 TXT ResNam2.,<'just press RETURN to find all deleted files.'>
 TXT ResNam3.,<'Deleted drawers will be restored as necessary'>
 TXT ResNam4.,<'when deleted files are restored.'>

 TXT ResDup1.,<'This file already exists:'>
 TXT ResDup2.,<'Enter a different name for this file and press'>
 TXT ResDup3.,<'RETURN, or click REPLACE to overwrite the'>
 TXT ResDup4.,<'existing file, or SKIP to bypass this file.'>

 TXT ResDir1.,<'This drawer already exists:'>
 TXT ResDir2.,<'Enter a different name for the drawer to be'>
 TXT ResDir3.,<'restored, and press RETURN to proceed.'>

 TXT Damag1.,<'This file is damaged and cannot be restored:'>
 TXT Damag2.,<'$'>
 TXT Damag3.,<'Use COPY FILES TO ANOTHER VOLUME to salvage'>
 TXT Damag4.,<'as much of the damaged file as possible.'>

 TXT CpyErr1.,<'Error reading parts of this file:'>
 TXT CpyErr2.,<'$'>
 TXT CpyErr3.,<'Click SALVAGE to copy as much as possible or'>
 TXT CpyErr4.,<'NEXT FILE to skip this file and continue.'>

 TXT CpyErr5.,<'Error writing $'>
 TXT CpyErr6.,<'to volume %.'>
 TXT CpyErr7.,<'Click CANCEL to return to catalog. Try again'>
 TXT CpyErr8.,<'with a different destination volume.'>

 TXT DestVol1.,<'Enter the name of the destination volume'>
 TXT DestVol2.,<'(and optional path) to which selected files'>
 TXT DestVol3.,<'will be copied:'>

 TXT WrtProt1.,<'Volume @ is'>
 TXT WrtProt2.,<'write protected. Enable writing and'>
 TXT WrtProt3.,<'select RETRY to proceed.'>

 TXT Lost1.,<'Found % file(s) belonging to ^ lost'>
 TXT Lost2.,<'drawer(s). The lost drawer(s) cannot be'>
 TXT Lost3.,<'restored. The files will appear in the'>
 TXT Lost4.,<'root drawer of the catalog.'>

 TXT FmtNam1.,<'Enter a new name for the volume, or just'>
 TXT FmtNam2.,<'press RETURN to keep the old name.'>

 TXT RUSur1.,<'Last warning - You are about to format a valid'>
 TXT RUSur2.,<'AmigaDOS volume. Do you want to preserve the'>
 TXT RUSur3.,<'existing contents so you can "Unformat" the'>
 TXT RUSur4.,<'volume at a later time, if necessary?'>

 TXT UnFmt1.,<'WARNING - UNFORMATTING THIS VOLUME CAN CAUSE'>
 TXT UnFmt2.,<'LOST OR DAMAGED FILES IF ANY FILES WERE ADDED'>
 TXT UnFmt3.,<'OR DELETED AFTER THE STRUCTURE OF THIS VOLUME'>
 TXT UnFmt4.,<'WAS LAST PRESERVED. ARE YOU ABSOLUTELY SURE'>
 TXT UnFmt5.,<'YOU WANT TO ATTEMPT TO UNFORMAT THIS VOLUME?'>

 TXT FilSys1.,<'Select the type of filing system you wish'>
 TXT FilSys2.,<'to use with this volume. Select New (FAST)'>
 TXT FilSys3.,<'filing system if you are using Workbench 2.0,'>
 TXT FilSys4.,<'but the Old filing system works with ALL'>
 TXT FilSys5.,<'versions of Workbench.'>

 TXT NoOld1.,<'The file and drawer information for volume'>
 TXT NoOld2.,<'@ has not been'>
 TXT NoOld3.,<'saved. Do you want to search for existing'>
 TXT NoOld4.,<'files and drawers and try to restore them?'>

 TXT WrnBk1.,<'WARNING - Volume reorganization can cause'>
 TXT WrnBk2.,<'lost or scrambled files. Have you backed up'>
 TXT WrnBk3.,<'critical files? If not, click on NO and'>
 TXT WrnBk4.,<'perform a full backup before reorganization.'>

 TXT RegOK1.,<'Reorganization was successful. To ensure the'>
 TXT RegOK2.,<'integrity of the volume and to remove any'>
 TXT RegOK3.,<'resident references to the old organization,'>
 TXT RegOK4.,<'you should reboot your Amiga immediately.'>

 TXT Maybe1.,<'WARNING - A potential problem has been found'>
 TXT Maybe2.,<'which may or may not affect reorganization.'>
 TXT Maybe3.,<'Select PROCEED to reorganize, or select CANCEL'>
 TXT Maybe4.,<'to exit, then use FIND AND REPAIR to verify'>
 TXT Maybe5.,<'and correct the problem, then reorganize.'>

 TXT RegEr1.,<'WARNING - Volume reorganization has failed.'>
 TXT RegEr2.,<'Some files may be lost or scrambled. Copy'>
 TXT RegEr3.,<'files to a different volume, then FORMAT this'>
 TXT RegEr4.,<'volume and restore files from backup.'>

 TXT CntRg1.,<'Volume contains known unreadable blocks,'>
 TXT CntRg2.,<'and cannot be reorganized.'>

 TXT RegCn1.,<'WARNING - If you CANCEL reorganization'>
 TXT RegCn2.,<'before it is complete, some files may be'>
 TXT RegCn3.,<'lost or scrambled. Do you really want to'>
 TXT RegCn4.,<'cancel reorganization?'>

 TXT ScRdEr1.,<'An unreadable block has been found on the'>
 TXT ScRdEr2.,<'volume. The reorganization cannot proceed.'>
 TXT ScRdEr3.,<'Select FIND UNREADABLE BLOCKS to repair,'>
 TXT ScRdEr4.,<'then try reorganization again.'>

 TXT FragErr1.,<'WARNING - Critical errors were found on volume'>
 TXT FragErr2.,<'@ which must be'>
 TXT FragErr3.,<'repaired before reorganization can proceed.'>
 TXT FragErr4.,<'Select VOLUME REPAIR MENU to find and repair'>
 TXT FragErr5.,<'volume problems.'>

 TXT ScBkEr1.,<'Volume has a critical error which must be'>
 TXT ScBkEr2.,<'repaired before reorganization can proceed.'>
 TXT ScBkEr3.,<'Select FIND AND REPAIR to correct problem,'>
 TXT ScBkEr4.,<'then try reorganization again.'>

 TXT RptNam1.,<'Enter the device:path for the drawer where'>
 TXT RptNam2.,<'you want the Error Report to be stored.  You'>
 TXT RptNam3.,<'cannot send the report to the volume selected'>
 TXT RptNam4.,<'for testing.  Press RETURN to proceed.'>

 TXT NoPrt1.,<'Something is wrong with the printer.'>
 TXT NoPrt2.,<'Correct the problem and click on PROCEED'>
 TXT NoPrt3.,<'to retry or click on CANCEL to abort.'>

 TXT NoRpt1.,<'Unable to create report file using the'>
 TXT NoRpt2.,<'device:path you specified.  Click on'>
 TXT NoRpt3.,<'PROCEED to enter a new device:path or'>
 TXT NoRpt4.,<'click on CANCEL to abort.'>

 TXT RootFail1.,<'The root block is unreadable, making all files'>
 TXT RootFail2.,<'"inaccessible".  Try to restore the lost files'>
 TXT RootFail3.,<'to a different volume using RESTORE DELETED'>
 TXT RootFail4.,<'FILES; then try to reformat this volume.'>

 TXT FmtRes1.,<'A previously-saved format has been restored'>
 TXT FmtRes2.,<'to volume @.'>
 TXT FmtRes3.,<'Do you want to verify the integrity of the'>
 TXT FmtRes4.,<'files on this volume before using it?'>

 TXT NoFmt1.,<'Quarterback Tools cannot format the selected'>
 TXT NoFmt2.,<'volume. You must format the volume using a'>
 TXT NoFmt3.,<'"low-level" formatting program, which will'>
 TXT NoFmt4.,<'erase any useful data on the volume.'>

 TXT FrBlks1.,<'Volume @ is too'>
 TXT FrBlks2.,<'full to reorganize. Tools needs % free'>
 TXT FrBlks3.,<'blocks on this volume. Delete one or more'>
 TXT FrBlks4.,<'unneeded files and try again.'>

 TXT BadFlop1.,<'Diskette is defective and cannot be'>
 TXT BadFlop2.,<'initialized. Replace diskette and select'>
 TXT BadFlop3.,<'RETRY to initialize another diskette.'>

 TXT FlpDrv1.,<'Load the diskette you wish to initialize'>
 TXT FlpDrv2.,<'into a floppy drive and enter the name of'
 TXT FlpDrv3.,<'floppy drive here. Press RETURN to proceed.'>

 TXT ErFlp1.,<'Do you want to fully rewrite the entire'>
 TXT ErFlp2.,<'floppy disk, erasing all previous contents?'>

	END
