;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
* These text strings appear if Tools finds a problem while trying to
* REPAIR a volume.  The first list describes the type of problem found, 
* and the second list describes the action to be taken to repair the
* problem.

* These strings are critical in length, since they MUST fit into a single
* text line of a requester in the center of the screen.

* The "%" in a message means that the word "file" or "drawer" will be
* inserted, as appropriate.  The resulting text string MUST NOT EXCEED 40
* characters maximum.

ErrList:
	ETXT 0,<'%hdr contains extraneous data'>
	ETXT 1,<'Drawer entry key out of range'>
	ETXT 2,<'Drawer entry checksum error'>
	ETXT 3,<'Drawer entry bad header type'>
	ETXT 4,<'Drawer entry claims to be root block'>
	ETXT 5,<'%cross-linked with another file'>
	ETXT 6,<'%header own key mismatch'>
	ETXT 7,<'Disk error reading drawer entry'>
	ETXT 8,<'%hash chain link key out of range'>
	ETXT 9,<'%parent key mismatch'>
	ETXT 10,<'%linked into wrong hash chain'>
	ETXT 11,<'Hash chain entry not properly sorted'>
	ETXT 12,<'%header invalid name'>
	ETXT 13,<'%header invalid comment'>
	ETXT 14,<'%header invalid date/time stamp'>
	ETXT 15,<'%first data block mismatch'>
	ETXT 16,<'%incorrect block count'>
	ETXT 17,<'%data key out of range'>
	ETXT 18,<'Disk error reading %data block'>
	ETXT 19,<'%data block cross-linked'>
	ETXT 20,<'Extension %hdr key out of range'>
	ETXT 21,<'%file size error'>
	ETXT 22,<'Disk error reading extension %header'>
	ETXT 23,<'Extension %header checksum error'>
	ETXT 24,<'%data block header error'>
	ETXT 25,<'Bad extension %header type'>
	ETXT 26,<'Ext %header own key mismatch'>
	ETXT 27,<'Ext %hdr parent key mismatch'>
	ETXT 28,<'Ext %header cross-linked'>
	ETXT 29,<'Disk error reading root drawer'>
	ETXT 30,<'Root drawer contents corrupted'>
	ETXT 31,<'Bad bitmap key'>
	ETXT 32,<'Disk error reading bitmap'>
	ETXT 33,<'Bitmap checksum error'>
	ETXT 34,<'Bad bitmap extension key'>
	ETXT 35,<'Disk error reading bitmap extension'>
	ETXT 36,<'Extraneous bitmap key'>
	ETXT 37,<'Extraneous bitmap extension key'>
	ETXT 38,<'%hash chain key cross-linked'>
	ETXT 39,<'Bitmap contents incorrect'>


ActList:
	ATXT 0,<'None - use "Repair" mode to fix'>
	ATXT 1,<'Bad entry will be deleted.'>
	ATXT 2,<'Checksum will be recalculated'>
	ATXT 3,<'Error will be corrected'>
	ATXT 4,<'Will be properly relinked'>
	ATXT 5,<'Entries will be sorted'>
	ATXT 6,<'Name replaced with: %'>
	ATXT 7,<'Comment will be deleted'>
	ATXT 8,<'Will be set to current date/time'>
	ATXT 9,<'File will be deleted'>
	ATXT 10,<'File will be deleted'>
	ATXT 11,<'None - all files inaccessible'>
	ATXT 12,<'New key allocated - error corrected'>
	ATXT 13,<'Hash chain deleted - some data lost'>
	ATXT 14,<'Data key deleted - some data lost'>
	ATXT 15,<'Bitmap reconstructed'>

	END
