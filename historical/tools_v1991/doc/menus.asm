;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*							*
*		QB TOOLS menus				*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1990 Central Coast Software	*
*********************************************************

* TEXT STRINGS USED IN MENUS

PROJ_TXT	PTEXTZ	<'  PROJECT '>
DEV_TXT		PTEXTZ	<'  DISPLAY'>
INHIBIT_TXT	PTEXTZ	<' AmigaDOS'>
SCNMODE_TXT	PTEXTZ	<' SCREEN MODE '>
RPT_TXT		PTEXTZ	<' ERROR REPORT'>
MEMORY_TXT	PTEXTZ	<' MEMORY USEAGE'>
MODE_TXT	PTEXTZ	<' OPTIMIZE FOR'>
INC_TXT		PTEXTZ	<' INCLUDE'>
EXC_TXT		PTEXTZ	<' EXCLUDE'>
FMT_TXT		PTEXTZ	<' INITIALIZE'>

ABOUT_TXT	MTXT	<'About QB Tools'>
CACHE_TXT	MTXT	<'Set Memory Use'>
SAVPRM_TXT	MTXT	<'Save Options  '>
LODPRM_TXT	MTXT	<'Load Options  '>
QUIT_TXT	MTXT	<'Exit QB Tools '>

VOL_TXT		MTXT	<'Volumes     '>
DRV_TXT		MTXT	<'Disk Drives '>

INTERACT_TXT	MTXT	<'  Interactive'>
INFORM_TXT	MTXT	<'  Informative'>
QUIET_TXT	MTXT	<'  Quiet      '>

NONE_TXT	MTXT	<'  No report      '>
DISK_TXT	MTXT	<'  Save to disk   '>
PRINT_TXT	MTXT	<'  Send to printer'>

MLOW_TXT	MTXT	<'  Low (slow)     '>
MNORM_TXT	MTXT	<'  Medium (faster)'>
MHOG_TXT	MTXT	<'  All (fastest)  '>

WB_TXT		MTXT	<'  Workbench'>
CLI_TXT		MTXT	<'  CLI      '>

ALL_TXT		MTXT	<' All files    '>
ACT_TXT		MTXT	<' Active files '>
DEL_TXT		MTXT	<' Deleted files'>

ENABLE_TXT	MTXT	<'  Enable'>
DISABLE_TXT	MTXT	<'  Disable'>

FMTFLOP_TXT	MTXT	<'Init Floppy Disk'>

	END
