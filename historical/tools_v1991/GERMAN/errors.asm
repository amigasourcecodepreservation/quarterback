;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*							*
*	QB TOOLS REPAIR error messages and routines	*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1990 Central Coast Software	*
*	424 Vista Avenue, Golden, CO 80401		*
*********************************************************

	XDEF	ErrorTable,ErrList
	XDEF	ActionTable,ActList

* In these macros, the first param is an error number, which is ignored.
* It exists only as an aid of the programmer, who must refer to them by
* number.

ETXT	MACRO	;text string
	SECTION	ErrTxt,DATA
Err\@	DC.B	\2,0
	SECTION	ErrTbl,DATA
	DC.W	Err\@-ErrList
	ENDM

ATXT	MACRO	;text string
	SECTION	ActTxt,DATA
Act\@	DC.B	\2,0
	SECTION	ActTbl,DATA
	DC.W	Act\@-ActList
	ENDM

TEXTZ	MACRO	;label, text string
	XDEF	\1
\1	DC.B	\2,0
	ENDM


* These error messages are placed in a report.  To reduce code space, they
* are identified by number in a scheme which groups them by severity.
* Errors from 1 to 127 are minor errors, while -1 to -127 are major errors.
* Error reporting is invoked by the REPORT macro, which sets the error
* code and calls the RptErr routine.  Action taken text goes with each
* error msg.  Again, to reduce code space, actions are numbered, from 1 to 
* 127. Of course, if the RepairFlg=0 the action taken is always NONE.

	SECTION	ErrTbl,DATA	

ErrorTable:

	SECTION	ErrTxt,DATA

ErrList:
;	ETXT 0,<'%hdr contains extraneous data'>
;	ETXT 1,<'Drawer entry key out of range'>
;	ETXT 2,<'Drawer entry checksum error'>
;	ETXT 3,<'Drawer entry bad header type'>
;	ETXT 4,<'Drawer entry claims to be root block'>
;	ETXT 5,<'%cross-linked with another file'>
;	ETXT 6,<'%header own key mismatch'>
;	ETXT 7,<'Disk error reading drawer entry'>
;	ETXT 8,<'%hash chain link key out of range'>
;	ETXT 9,<'%parent key mismatch'>
;	ETXT 10,<'%linked into wrong hash chain'>
;	ETXT 11,<'Hash chain entry not properly sorted'>
;	ETXT 12,<'%header invalid name'>
;	ETXT 13,<'%header invalid comment'>
;	ETXT 14,<'%header invalid date/time stamp'>
;	ETXT 15,<'%first data block mismatch'>
;	ETXT 16,<'%incorrect block count'>
;	ETXT 17,<'%data key out of range'>
;	ETXT 18,<'Disk error reading %data block'>
;	ETXT 19,<'%data block cross-linked'>
;	ETXT 20,<'Extension %hdr key out of range'>
;	ETXT 21,<'%file size error'>
;	ETXT 22,<'Disk error reading extension %header'>
;	ETXT 23,<'Extension %header checksum error'>
;	ETXT 24,<'%data block header error'>
;	ETXT 25,<'Bad extension %header type'>
;	ETXT 26,<'Ext %header own key mismatch'>
;	ETXT 27,<'Ext %hdr parent key mismatch'>
;	ETXT 28,<'Ext %header cross-linked'>
;	ETXT 29,<'Disk error reading root drawer'>
;	ETXT 30,<'Root drawer contents corrupted'>
;	ETXT 31,<'Bad bitmap key'>
;	ETXT 32,<'Disk error reading bitmap'>
;	ETXT 33,<'Bitmap checksum error'>
;	ETXT 34,<'Bad bitmap extension key'>
;	ETXT 35,<'Disk error reading bitmap extension'>
;	ETXT 36,<'Extraneous bitmap key'>
;	ETXT 37,<'Extraneous bitmap extension key'>
;	ETXT 38,<'%hash chain key cross-linked'>
;	ETXT 39,<'Bitmap contents incorrect'>

	ETXT 0,<'%hdr enth�lt besondere Daten'>
	ETXT 1,<'Schl�ssel des Verzeichniseintrages au�erhalb des g�ltigen Bereiches'>
	ETXT 2,<'Checksummenfehler des Verzeichniseintrages'>
	ETXT 3,<'"Bad header type" bei Verzeichniseintrag'>
	ETXT 4,<'Verzeichniseintrag zeigt auf den Root-Block'>
	ETXT 5,<'%Zeigereintrag auf eine andere Datei'>
	ETXT 6,<'%Schl�ssel des Dateikopfs nicht korrekt'>
	ETXT 7,<'Fehler bei Lesen des Verzeichniseintrages'>
	ETXT 8,<'%Eintrag in der Hash-Liste nicht g�ltig'>
	ETXT 9,<'%Falscher "Parent key"'>
	ETXT 10,<'%wurde in falsche Hash-Liste eingetragen'>
	ETXT 11,<'Hash-Listeneintrag ungen�gend sortiert'>
	ETXT 12,<'%ung�ltiger Header-Name'>
	ETXT 13,<'%ung�ltiger Header-Kommentar'>
	ETXT 14,<'%ung�ltiger Header-Zeit/Datum-Eintrag'>
	ETXT 15,<'%Fehlerhafter "First data block"'>
	ETXT 16,<'%falsche Block-Nummer'>
	ETXT 17,<'%ung�ltiger Datenschl�ssel'>
	ETXT 18,<'Fehler bei Lesen des %Data-Blockes'>
	ETXT 19,<'%Datenblock verkn�pft'>
	ETXT 20,<'Extension %hdr Schl�ssel nicht g�ltig'>
	ETXT 21,<'%Fehler bei Datei-Gr��e'>
	ETXT 22,<'Fehler bei Lesen des Extension %Kopfes'>
	ETXT 23,<'Checksummenfehler bei Extension %header'>
	ETXT 24,<'%Ung�ltiger Datenblock'>
	ETXT 25,<'Falscher Extension %header-Typ'>
	ETXT 26,<'Ext %Headereigener Schl�ssel falsch'>
	ETXT 27,<'Ext %Header - �bergeordneter Schl�ssel falsch'>
	ETXT 28,<'Ext %Header verkn�pft'>
	ETXT 29,<'Fehler bei Lesen des Root-Verzeichnisses'>
	ETXT 30,<'Inhalt des Root-Verzeichnisses zerst�rt'>
	ETXT 31,<'Fehlerhafter Bitmap-Schl�ssel'>
	ETXT 32,<'Fehler bei Lesen der Bitmap'>
	ETXT 33,<'Checksummenfehler der Bitmap'>
	ETXT 34,<'Fehlerhafter Bitmap-Erweiterungs-Schl�ssel'>
	ETXT 35,<'Fehler bei Lesen der Bitmap-Erweiterung'>
	ETXT 36,<'Beonderer Bitmap-Schl�ssel'>
	ETXT 37,<'Besonderer Bitmap-Erweiterungs-Schl�ssel'>
	ETXT 38,<'%Hash-Listen-Eintrag verkn�pft'>
	ETXT 39,<'Falscher Inhalt der Bitmap'>

	SECTION ErrTbl,DATA

MaxErr	EQU	*-ErrorTable/2

***********************************************************************

	SECTION	ActTbl,DATA

ActionTable:

	SECTION	ActTxt,DATA

ActList:
;	ATXT 0,<'None - use "Repair" mode to fix'>
;	ATXT 1,<'Bad entry will be deleted.'>
;	ATXT 2,<'Checksum will be recalculated'>
;	ATXT 3,<'Error will be corrected'>
;	ATXT 4,<'Will be properly relinked'>
;	ATXT 5,<'Entries will be sorted'>
;	ATXT 6,<'Name replaced with: %'>
;	ATXT 7,<'Comment will be deleted'>
;	ATXT 8,<'Will be set to current date/time'>
;	ATXT 9,<'File will be deleted'>
;	ATXT 10,<'File will be deleted'>
;	ATXT 11,<'None - all files inaccessible'>
;	ATXT 12,<'New key allocated - error corrected'>
;	ATXT 13,<'Hash chain deleted - some data lost'>
;	ATXT 14,<'Data key deleted - some data lost'>
;	ATXT 15,<'Bitmap reconstructed'>

	ATXT 0,<'Nichts - benutze Reparatur-Modus zum Beheben'>
	ATXT 1,<'Falscher Eintrag wird gel�scht.'>
	ATXT 2,<'Checksumme wird berechnet'>
	ATXT 3,<'Fehler wird korrigiert'>
	ATXT 4,<'wurde neu eingetragen'>
	ATXT 5,<'Eintr�ge werden sortiert'>
	ATXT 6,<'Name wurde ersetzt durch: %'>
	ATXT 7,<'Kommentar wird gel�scht'>
	ATXT 8,<'Wird auf aktuelle Zeit/Datum gesetzt'>
	ATXT 9,<'Datei wird gel�scht'>
	ATXT 10,<'Datei wird gel�scht'>
	ATXT 11,<'Nichts - keine Dateien erreichbar'>
	ATXT 12,<'New key allocated - error corrected'>
	ATXT 13,<'Hash-Liste gel�scht - einige Daten sind verlorengegangen'>
	ATXT 14,<'Daten-Schl�ssel gel�scht - einige Daten sind verlorengegangen'>
	ATXT 15,<'Bitmap neu erstellt'>

	SECTION ActTbl,DATA

MaxAct	EQU	*-ActionTable/2

	END
