;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*							*
*		QB TOOLS menus				*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1990 Central Coast Software	*
*********************************************************


	INCLUDE	"vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"

	SECTION	MENUS,DATA

	XDEF	ToolsMenu
	XDEF	ReorgMenu
	XDEF	RestoreMenu
	XDEF	RepairMenu
	XDEF	FunctionMenu

	XDEF	SCNMODE_ITEMS
	XDEF	RPT_ITEMS
	XDEF	MEMORY_ITEMS
	XDEF	MODE_ITEMS

	XREF	T80

* MENU MACROS

MENU	MACRO		;WID,HT,FLG,TEXT,ITM,0
	IFEQ	NARG-5
	DC.L	*+30
	ENDC
	IFEQ	NARG-6
	DC.L	0
	ENDC
	DC.W	MNUPOS,0,\1,\2,\3
	DC.L	\4,\5
	DC.W	0,0,0,0
MNUPOS	SET	MNUPOS+\1
	ENDM

MNUITM	MACRO		;LE,WID,FLG,CHAR,TEXT,XOR,SUBITM,ROUTINE,0
	XREF	\8
	IFEQ	NARG-8
	DC.L	*+38
	ENDC
	IFEQ	NARG-9
	DC.L	0
	ENDC
	DC.W	\1,ITEMPOS,\2,MNUHT
	DC.W	\3
	DC.L	\6,\5,0
	DC.B	\4,0
	DC.L	\7
	DC.W	0
	DC.L	\8
ITEMPOS	SET	ITEMPOS+MNUHT
	ENDM

ITEXT	MACRO	;FPEN,BPEN,MODE,RLE,RTE,FONT,STG,NXT
	DC.B	\1,\2,\3,0	;FPEN, BPEN, MODE, FILL
	DC.W	\4,\5		;REL LE, REL TE
	DC.L	\6,\7,\8	;FONT PTR, STG PTR, NXT PTR
	ENDM

MTXT	MACRO	;STG
	ITEXT	0,1,JAM2,1,1,T80,IT\@,0
IT\@	DC.B	\1,0
	CNOP	0,2
	ENDM

PTEXTZ	MACRO
	DC.B	\1,0
	CNOP	0,2
	ENDM

* MENU DEFINITIONS

MNUWID		EQU	100
MNUHT		EQU	11
IF_NORM		EQU	$52
IF_COMS		EQU	$56
IF_TCHECK	EQU	$5B
IF_CHECK 	EQU	$5B

* These items define the menu bar:

MNUPOS	SET	0

ToolsMenu	;WID,HT,FLG,TEXT,ITM,0
	MENU	100,0,1,PROJ_TXT,PROJ_ITEMS
;;	MENU	128,0,1,MEMORY_TXT,MEMORY_ITEMS
	MENU	100,0,1,DEV_TXT,DEV_ITEMS,0

MNUPOS	SET	0

FunctionMenu
	MENU	100,0,1,PROJ_TXT,PROJ_ITEMS
	MENU	128,0,1,MEMORY_TXT,MEMORY_ITEMS
	MENU	100,0,1,INHIBIT_TXT,INHIBIT_ITEMS,0

MNUPOS	SET	0

RepairMenu
	MENU	100,0,1,PROJ_TXT,PROJ_ITEMS
	MENU	116,0,1,SCNMODE_TXT,SCNMODE_ITEMS
	MENU	116,0,1,RPT_TXT,RPT_ITEMS,0

MNUPOS	SET	0

ReorgMenu
	MENU	100,0,1,PROJ_TXT,PROJ_ITEMS
	MENU	116,0,1,SCNMODE_TXT,SCNMODE_ITEMS
	MENU	116,0,1,MODE_TXT,MODE_ITEMS,0

MNUPOS	SET	0

RestoreMenu
	MENU	100,0,1,PROJ_TXT,PROJ_ITEMS
	MENU	100,0,1,INC_TXT,INC_ITEMS
	MENU	100,0,1,EXC_TXT,EXC_ITEMS
	MENU	100,0,1,FMT_TXT,FMT_ITEMS,0


* MENU ITEM DEFINITIONS

		;LE,WID,FLG,CHAR,TEXT,XOR,SUBITM,ROUTINE,0

ITEMPOS	SET	0

PROJ_ITEMS
	MNUITM	0,128,IF_NORM,0,ABOUT_TXT,0,0,AboutTools
	MNUITM	0,128,IF_NORM,0,SAVPRM_TXT,0,0,SaveParams
	MNUITM	0,128,IF_NORM,0,LODPRM_TXT,0,0,LoadParams
	MNUITM	0,128,IF_NORM,0,QUIT_TXT,0,0,QuitTools,0

ITEMPOS	SET	0

DEV_ITEMS
	MNUITM	0,112,IF_NORM,0,VOL_TXT,0,0,VolMnu
	MNUITM	0,112,IF_NORM,0,DRV_TXT,0,0,DrvMnu,0

ITEMPOS	SET	0

SCNMODE_ITEMS
	MNUITM	0,120,IF_CHECK,0,QUIET_TXT,$FE,0,ScnQuiet
	MNUITM	0,120,IF_CHECK,0,INFORM_TXT,$FD,0,ScnInform
	MNUITM	0,120,IF_CHECK+$100,0,INTERACT_TXT,$FB,0,ScnInteract,0


ITEMPOS	SET	0

RPT_ITEMS
	MNUITM	0,152,IF_CHECK+$100,0,NONE_TXT,$FE,0,RptNone
	MNUITM	0,152,IF_CHECK,0,DISK_TXT,$FD,0,RptDisk
	MNUITM	0,152,IF_CHECK,0,PRINT_TXT,$FB,0,RptPtr,0

ITEMPOS	SET	0

MEMORY_ITEMS
	MNUITM	0,152,IF_CHECK,0,MLOW_TXT,$FE,0,LowMem
	MNUITM	0,152,IF_CHECK+$100,0,MNORM_TXT,$FD,0,NormMem
	MNUITM	0,152,IF_CHECK,0,MHOG_TXT,$FB,0,HogMem,0

ITEMPOS	SET	0

MODE_ITEMS
	MNUITM	0,100,IF_CHECK,0,CLI_TXT,$FE,0,ModeCLI
	MNUITM	0,100,IF_CHECK+$100,0,WB_TXT,$FD,0,ModeWB,0

ITEMPOS	SET	0

INC_ITEMS
	MNUITM	0,100,IF_NORM,0,ALL_TXT,0,0,IncAll
	MNUITM	0,100,IF_NORM,0,ACT_TXT,0,0,IncAct
	MNUITM	0,100,IF_NORM,0,DEL_TXT,0,0,IncDel,0

ITEMPOS	SET	0

EXC_ITEMS
	MNUITM	0,100,IF_NORM,0,ALL_TXT,0,0,ExcAll
	MNUITM	0,100,IF_NORM,0,ACT_TXT,0,0,ExcAct
	MNUITM	0,100,IF_NORM,0,DEL_TXT,0,0,ExcDel,0

ITEMPOS	SET	0

FMT_ITEMS
	MNUITM	0,152,IF_NORM,0,FMTFLOP_TXT,0,0,FmtFlop,0

ITEMPOS	SET	0

INHIBIT_ITEMS
	MNUITM	0,152,IF_CHECK+$100,0,ENABLE_TXT,$FE,0,ADOSon
	MNUITM	0,152,IF_CHECK,0,DISABLE_TXT,$FD,0,ADOSoff,0

* TEXT STRINGS USED IN MENUS

;PROJ_TXT	PTEXTZ	<'  PROJECT '>
;DEV_TXT		PTEXTZ	<'  DISPLAY'>
;INHIBIT_TXT	PTEXTZ	<' AmigaDOS'>
;SCNMODE_TXT	PTEXTZ	<' SCREEN MODE '>
;RPT_TXT		PTEXTZ	<' ERROR REPORT'>
;MEMORY_TXT	PTEXTZ	<' MEMORY USEAGE'>
;MODE_TXT	PTEXTZ	<' OPTIMIZE FOR'>
;INC_TXT		PTEXTZ	<' INCLUDE'>
;EXC_TXT		PTEXTZ	<' EXCLUDE'>
;FMT_TXT		PTEXTZ	<' INITIALIZE'>

;ABOUT_TXT	MTXT	<'About QB Tools'>
;CACHE_TXT	MTXT	<'Set Memory Use'>
;SAVPRM_TXT	MTXT	<'Save Options  '>
;LODPRM_TXT	MTXT	<'Load Options  '>
;QUIT_TXT	MTXT	<'Exit QB Tools '>

;VOL_TXT		MTXT	<'Volumes     '>
;DRV_TXT		MTXT	<'Disk Drives '>

;INTERACT_TXT	MTXT	<'  Interactive'>
;INFORM_TXT	MTXT	<'  Informative'>
;QUIET_TXT	MTXT	<'  Quiet      '>

;NONE_TXT	MTXT	<'  No report      '>
;DISK_TXT	MTXT	<'  Save to disk   '>
;PRINT_TXT	MTXT	<'  Send to printer'>

;MLOW_TXT	MTXT	<'  Low (slow)     '>
;MNORM_TXT	MTXT	<'  Medium (faster)'>
;MHOG_TXT	MTXT	<'  All (fastest)  '>

;WB_TXT		MTXT	<'  Workbench'>
;CLI_TXT		MTXT	<'  CLI      '>

;ALL_TXT		MTXT	<' All files    '>
;ACT_TXT		MTXT	<' Active files '>
;DEL_TXT		MTXT	<' Deleted files'>

;ENABLE_TXT	MTXT	<'  Enable'>
;DISABLE_TXT	MTXT	<'  Disable'>

;FMTFLOP_TXT	MTXT	<'Init Floppy Disk'>

PROJ_TXT	PTEXTZ	<'  PROJEkT '>
DEV_TXT		PTEXTZ	<'  ANZEIGE'>
INHIBIT_TXT	PTEXTZ	<' AmigaDOS'>
SCNMODE_TXT	PTEXTZ	<' BILDSCHIRM-MODUS'>
RPT_TXT		PTEXTZ	<' FEHLERREPORT'>
MEMORY_TXT	PTEXTZ	<' SPEICHERBENUTZUNG'>
MODE_TXT	PTEXTZ	<' OPTIMIERE F�R'>
INC_TXT		PTEXTZ	<' INKLUSIVE'>
EXC_TXT		PTEXTZ	<' EXKLUSIVE'>
FMT_TXT		PTEXTZ	<' INITIALISIEREN'>

ABOUT_TXT	MTXT	<'�ber QB Tools'>
CACHE_TXT	MTXT	<'Speicherbenutzung �ndern'>
SAVPRM_TXT	MTXT	<'Einstellungen Speichern  '>
LODPRM_TXT	MTXT	<'Einstellungen Laden  '>
QUIT_TXT	MTXT	<'QB Tools verlassen'>

VOL_TXT		MTXT	<'Laufwerke   '>
DRV_TXT		MTXT	<'Disk-Laufwerke'>

INTERACT_TXT	MTXT	<'  Interaktiv'>
INFORM_TXT	MTXT	<'  Informativ'>
QUIET_TXT	MTXT	<'  Ruhe    '>

NONE_TXT	MTXT	<'  Kein Report    '>
DISK_TXT	MTXT	<'  Auf Disk speichern'>
PRINT_TXT	MTXT	<'  Auf Drucker ausgeben'>

MLOW_TXT	MTXT	<'  Wenig (langsam)     '>
MNORM_TXT	MTXT	<'  Mittel (schneller)'>
MHOG_TXT	MTXT	<'  Alles (am schnellsten)  '>

WB_TXT		MTXT	<'  Workbench'>
CLI_TXT		MTXT	<'  CLI      '>

ALL_TXT		MTXT	<' Alle Dateien    '>
ACT_TXT		MTXT	<' Aktive Dateien '>
DEL_TXT		MTXT	<' Gel�schte Dateien'>

ENABLE_TXT	MTXT	<'  Erlauben'>
DISABLE_TXT	MTXT	<'  Verbieten'>

FMTFLOP_TXT	MTXT	<'Initialisiere Floppy Disk'>

	END
