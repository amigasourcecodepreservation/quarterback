;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*							*
*	Quarterback language-dependent text messages	*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1987 Central Coast Software	*
*	    424 Vista Ave, Golden, CO 80401		*
*	     All rights reserved, worldwide		*
*********************************************************

	INCLUDE	"vd0:MACROS.ASM"

	SECTION	MESSAGES,DATA

	XDEF	MONTHS
	XDEF	Restfm,Backto
	XDEF	ResOpt,ResExist,ResOvr,ResSub,ResRpt
	XDEF	BakOpt,BakArc,BakOvr,BakSub,BakRpt,BakChk
	XDEF	Slow.,ResTest
	XDEF	ChgBeep,BDev1Txt.,BDev2Txt.,RDev1Txt.,RDev2Txt.
	XDEF	SelFrom
	XDEF	FSel.,OutOf.,Bytes.,Disks.
	XDEF	Restto.,Backfm.
	XDEF	Status,Status.
	XDEF	StRest.
	XDEF	StBack.
	XDEF	StTest.
	XDEF	Disk.
	XDEF	File.
	XDEF	DevNS
	XDEF	DevRdy
	XDEF	DevAct
	XDEF	DevNRdy
	XDEF	PlsLoad
	XDEF	PlsRem
	XDEF	GoodBak,GoodRst,GoodTst,GdBakArc
	XDEF	AbortMsg
	XDEF	NoBrDev.,BadVol.,BadDir.,NotDir.
	XDEF	NoMemory
	XDEF	SubDir.
	XDEF	IncNam.,ExcNam.
	XDEF	Wild.
	XDEF	IncDat.
	XDEF	ExcDat.
	XDEF	Format.
	XDEF	BldCat.,FilCnt.
	XDEF	CatFor.
	XDEF	Legnd.,LgInc.,LgExc.
	XDEF	DiskBad.
	XDEF	LoadN1.,LoadN2.
	XDEF	LoadL1.,LoadL2.
	XDEF	NoPrt.,NoRpt.
	XDEF	BackRpt.,RestRpt.,CatRpt.
	XDEF	BRptDev.,RRptDev.
	XDEF	Page.
	XDEF	PrtRpt.,PrtCat.
	XDEF	DevOpnErr.
	XDEF	CantSub.
	XDEF	WrtPrt1.,WrtPrt2.,WrtPrt3.
	XDEF	MiscEr1.,MiscEr2.,MiscEr3.
	XDEF	ADOS1.,ADOS2.,ADOS3.
	XDEF	NotQB1.,NotQB2.,NotQB3.
	XDEF	BSEQ1.,BSEQ2.,BSEQ3.
	XDEF	BSET1.,BSET2.,BSET3.
	XDEF	BDSK1.,BDSK2.,BDSK3.
	XDEF	RdEr1.,RdEr2.,RdEr3.
	XDEF	CatEr1.,CatEr2.,CatEr3.
	XDEF	Dup1.,Dup2.
	XDEF	RecovErr.
	XDEF	With.,Backup.,Restore.,Date.,Arc.,UCInc.,UCExc.,All.
	XDEF	NoBatch.
	XDEF	BadSyntax.
	XDEF	ArcWP1.,ArcWP2.,ArcWP3.
	XDEF	ReadErr.
	XDEF	CantBak.,CantRes.
	XDEF	NoneSel.
	XDEF	RPTxt.
	XDEF	ResArc
	XDEF	NoDev.
	XDEF	SameDev.
	XDEF	MixedDev.
	XDEF	NoDFn.
	XDEF	Err.
	XDEF	CatError
	XDEF	CantFindAltCat
	XDEF	DirBlk.
	XDEF	WrtErrRest.,DeviceFull.
	XDEF	FileRdPrt.,FileWrtPrt.
	XDEF	FileDelPrt.,FileInUse.
	XDEF	WrtErrCode.
	XDEF	NotFSD.
	XDEF	BakVol.,ResVol.
	XDEF	VolLst.
	XDEF	AllRes.
	XDEF	Files.
	XDEF	NoCat.,For.

MONTHS	TEXTZ	'JAN'
	TEXTZ	'FEB'
	TEXTZ	'MAR'
	TEXTZ	'APR'
	TEXTZ	'MAJ'
	TEXTZ	'JUN'
	TEXTZ	'JUL'
	TEXTZ	'AUG'
	TEXTZ	'SEP'
	TEXTZ	'OKT'
	TEXTZ	'NOV'
	TEXTZ	'DEC'

;These text strings appear inside various gadgets:

Backto	;TEXTZ	<'BACKUP FILES'>
	TEXTZ	<'S�KERHETSKOPIERA'>

Restfm	;TEXTZ	<'RESTORE FILES'>
	TEXTZ	<'�TERST�LL'>

; These text strings appear on the device selection screen:

VolLst.	;TEXTZ	<'Here are the hard disk volumes (drives or partitions) on your Amiga.'>
	TEXTZ	<'H�r �r de h�rddiskar (enheter och partitioner) som finns p� din Amiga.'>

BakVol.	;TEXTZ	<'Click on the name of the volume you wish to backup from.'>
	TEXTZ	<'Klicka p� den h�rddisk du vill s�kerhetskopiera fr�n.'>

ResVol.	;TEXTZ	<'Click on the name of the volume you wish to restore to.'>
	TEXTZ	<'Klicka p� den h�rddisk du vill �terst�lla till.'>

Backfm.	;TEXTZ	<'Backup files from this hard disk volume:'>
	TEXTZ	<'S�kerhetskopiera filer fr�n denna h�rddisk:'>

Restto.	;TEXTZ	<'Restore files to this hard disk volume:'>
	TEXTZ	<'�terst�ll filer till denna h�rddisk:'>

BDev1Txt.
	;TEXTZ	<'Backup files to this primary device:'>
	TEXTZ	<'S�kerhetskopiera filer till denna utg�ngsenhet:'>

BDev2Txt.
	;TEXTZ	<'Backup files to this alternate device:'>
	TEXTZ	<'S�kerhetskopiera filer till denna alternerande enhet:'>

RDev1Txt.
	;TEXTZ	<'Restore files from this primary device:'>
	TEXTZ	<'�terst�ll filer fr�n denna utg�ngsenhet:'>

RDev2Txt. 
	;TEXTZ	<'Restore files from this alternate device:'>
	TEXTZ	<'�terst�ll filer fr�n denna alternerande enhet:'>


;These are requester strings which may appear after device selection:

NoBrDev. ;TEXTZ	<'Please select a hard disk volume to backup/restore.'>
	TEXTZ	<'V�lj en h�rddisk att s�kerhetskopiera/�terst�lla.'>

BadVol. ;TEXTZ	<'Can''t find hard disk volume you entered.'>
	TEXTZ	<'Kan inte hitta den beg�rda h�rddisken.'>

BadDir. ;TEXTZ	<'Can''t find the specified directory on that volume.'>
	TEXTZ	<'Kan inte hitta det angivna biblioteket p� h�rddisken.'>

NotDir.	;TEXTZ	<'Enter hard disk volume name, not a file name.'>
	TEXTZ	<'Mata in h�rddiskens namn, inte ett filnamn.'>

NoDev.	;TEXTZ	<'You must specify a primary device.'>
	TEXTZ	<'Du m�ste ange utg�ngsenhet.'>

MixedDev. ;TEXTZ <'Primary and alternate devices must be of the same type.'>
	TEXTZ	<'Utg�ngs- och alternerande enheter m�ste vara av samma typ.'>

SameDev.  ;TEXTZ <'Alternate device cannot be same as primary device.'>
	TEXTZ	<'Alternerande enhet f�r inte vara samma som utg�ngsenhet.'>

NoDFn.	  ;TEXTZ	<'@ is not available.'>
	TEXTZ	<'@ �r inte tillg�nglig.'>

NotFSD.	  ;TEXTZ	<' is not a file storage device.'>
	TEXTZ	<'@ �r inte en filorienterad enhet.'>

NoMemory ;TEXTZ	<'Out of memory ... can''t perform your request.'>
	TEXTZ	<'Ej nog minne ... kan ej utf�ra operationen.'>


; These text strings appear while building the catalog:

BldCat.	;TEXTZ	<'Building catalog of files ... please wait!'>
	TEXTZ	<'Bygger register �ver filerna ... v�nta!'>

FilCnt.	;TEXTZ	<'Files found:     '>
	TEXTZ	<'Filer funna:     '>

; These requester strings may appear during restore:

CatError ;TEXTZ	<'Unable to read backup catalog.'>
	TEXTZ	<'Kan ej l�sa �terst�llningsregister.'>

CantFindAltCat 
	;TEXTZ	<'Unable to find alternate catalog.'>
	TEXTZ	<'Kan ej hitta alternerande register.'>


;These text strings appear on the catalog screen:

FSel.	;TEXTZ	<'Files included:'>
	TEXTZ	<'Filer som ing�r:'>

OutOf.	;TEXTZ	<' of '>
	TEXTZ	<' av '>

Bytes.	;TEXTZ	<'Bytes: '>
	TEXTZ	<'Tecken: '>

Disks.	;TEXTZ	<'Volumes: '>
	TEXTZ	<'Disketter: '>

SubDir.	;TEXTZ	<'  (Subdirectory)       '>
	TEXTZ	<'  (Underbibliotek)     '>

SelFrom	;TEXTZ	<'Commands apply to:'>
	TEXTZ	<'�tg�rderna g�ller:'>

CatFor.	;TEXTZ	<'Catalog for: '>
	TEXTZ	<'Register �ver: '>

Legnd.	;TEXTZ	<'Legend: '>
	TEXTZ	<'M�rkning: '>

LgInc.	;TEXTZ	<' included '>
	TEXTZ	<'  ing�r   '>

LgExc.	;TEXTZ	<' excluded '>
	TEXTZ	<' ing�r ej '>


;These are requester text strings which may appear during selection of files on
;the catalog screen:

IncNam. ;TEXTZ	<'Enter name of file to be included.'>
	TEXTZ	<'Ange namn p� fil som skall ing�.'>

ExcNam. ;TEXTZ	<'Enter name of file to be excluded.'>
	TEXTZ	<'Ange namn p� fil som ej skall ing�.'>

Wild.	;TEXTZ	<'Use wildcards ''?'' or ''#?'' for groups.'>
	TEXTZ	<'Anv�nd jokrar ''?'' och ''#'' f�r flera.'>

IncDat. ;TEXTZ	<'Include files which changed on or after'>
	TEXTZ	<'L�t ing� filer �ndrade p� eller efter'>

ExcDat. ;TEXTZ	<'Exclude files which haven''t changed since'>
	TEXTZ	<'Uteslut filer som inte �ndrats sedan'>

Format.	;TEXTZ	<'Date format: 31-DEC-87'>
	TEXTZ	<'Datumformat: 31-DEC-87'>


; These text strings appear on the backup/restore options screen:

ResOpt	;TEXTZ	<'SELECT RESTORE OPTIONS'>
	TEXTZ	<'ANGE �TERST�LLNINGSALTERNATIV'>

BakOpt	;TEXTZ	<'SELECT BACKUP OPTIONS'>
	TEXTZ	<'ANGE S�KERHETSKOPIERINGSALTERNATIV'>

ResOvr	;TEXTZ	<'Overwrite existing files:'>
	TEXTZ	<'Skriv �ver existerande filer:'>

ResExist ;TEXTZ	<'Restore only files which already exist:'>
	TEXTZ	<'�terst�ll bara filer som redan finns:'>

BakArc	;TEXTZ	<'Set archive flag on backed-up files:'>
	TEXTZ	<'S�tt arkivflagga p� kopierade filer:'>

ResArc	;TEXTZ	<'Set archive bit for restored files:'>
	TEXTZ	<'S�tt arkivflagga p� �terst�llda filer:'>

BakOvr	;TEXTZ	<'Overwrite AmigaDOS format on floppies:'>
	TEXTZ	<'Skriv �ver AmigaDOSformaterade disketter:'>

BakChk	;TEXTZ	<'Read data after write:'>
	TEXTZ	<'Kontroll�s data efter skrivning:'>

Slow.	;TEXTZ	<'Slow speed, reduced memory: '>
	;TEXTZ	<'L�g hastighet, beg�nsat minne:'>
	TEXTZ	<'L�g hastighet, minskat minnesbehov:'>

ResTest	;TEXTZ	<'Read backup volumes, don''t restore files:'>
	TEXTZ	<'L�s s�kerhetskopior, �terst�ll inga filer:'>

ChgBeep	;TEXTZ	<'Beep for QB volume change: '>
	TEXTZ	<'Pip vid QB diskettbyte:'>

BakRpt	;TEXTZ	<'Send archive report to:'>
	TEXTZ	<'Skicka arkivrapport till:'>

ResRpt	;TEXTZ	<'Send restoration report to:'>
	TEXTZ	<'Skicka �terst�llningsrapport till:'>

BakSub	;TEXTZ	<'Backup full subdirectory structure:'>
	TEXTZ	<'Kopiera hela biblioteksstrukturen:'>

ResSub	;TEXTZ	<'Restore full subdirectory structure:'>
	TEXTZ	<'�terst�ll hela biblioteksstrukturen:'>


; This requester appears if you select DISK for the report:

RPTxt.	;TEXTZ	<'Enter device:path/name for report:'>
	TEXTZ	<'Ange enhet:bibliotek/namn f�r rapport:'>


;This requester text appears if the disk report path is bad:

NoPrt.	;TEXTZ	<'Can''t open printer for report...retry?'>
	TEXTZ	<'Kan inte �ppna skrivaren f�r utskrift...nytt f�rs�k?'>

NoRpt.	;TEXTZ	<'Can''t open file for report...retry?'>
	TEXTZ	<'Kan inte �ppna fil f�r skrivning...nytt f�rs�k?'>

; These text strings appear on the backup/restore status screen:


Status	;TEXTZ	<'S T A T U S'>
	TEXTZ	<'S T A T U S'>

StRest.	;TEXTZ	<'Restoring to '>
	TEXTZ	<'�terst�ller till '>

StBack.	;TEXTZ	<'Backing up '>
	TEXTZ	<'S�kerhetskopierar '>

StTest.	;TEXTZ	<'Testing backup volumes'>
	TEXTZ	<'Testar s�kerhetskopior'>

Disk.	;TEXTZ	<'Volume '>
	TEXTZ	<'Diskett '>

File.	;TEXTZ	<'File '>
	TEXTZ	<'Fil '>

DevNS	;TEXTZ	<'No second drive selected'>
	TEXTZ	<'Ingen enhet vald'>

Status.	;TEXTZ	<' status:'>
	TEXTZ	<' status:'>

DevRdy	;TEXTZ	<'ready    '>
	TEXTZ	<'klar     '>

DevAct	;TEXTZ	<'active   '>
	TEXTZ 	<'aktiv    '>

DevNRdy	;TEXTZ	<'not ready'>
	TEXTZ 	<'ej klar  '>

PlsLoad	;TEXTZ	<'Please load volume '>
	TEXTZ	<'Mata in diskett %'>

PlsRem	;TEXTZ	<'Please remove volume '>
	TEXTZ	<'Ta bort diskett '>

GoodBak	;TEXTZ	<'File backup completed.'>
	TEXTZ	<'S�kerhetskopiering genomf�rd.'>

GoodRst	;TEXTZ	<'File restore completed.'>
	TEXTZ	<'�terst�llning av filer genomf�rd.'>

GoodTst	;TEXTZ	<'Volume testing completed.'>
	TEXTZ	<'Test av disketter genomf�rd.'>

GdBakArc ;TEXTZ	<'Backup successful -- setting archive bits.'>
	TEXTZ	<'S�kerhetskopiering framg�ngsrik -- s�tter arkivflaggor.'>

AbortMsg ;TEXTZ	<'WARNING ---- Backup/restore aborted ---- WARNING'>
	TEXTZ	<'S�kerhetskopiering/�terst�llning avbruten'>


;These requester messages may appear during the backup/restore process:

WrtPrt1. ;TEXTZ	<'WARNING - Volume in drive@ is write'>
	TEXTZ	<'VARNING - Disketten i enhet@ �r skriv-'>
WrtPrt2. ;TEXTZ	<'protected.  Enable writing or replace'>
	TEXTZ	<'skyddad. Ta bort skrivskyddet eller byt'>
WrtPrt3. ;TEXTZ	<'volume.  PROCEED to retry.'>
	TEXTZ	<'volym. FORTS�TT f�r nytt f�rs�k.'>

MiscEr1. ;TEXTZ	<'WARNING - Error writing to drive@.'>
	TEXTZ	<'VARNING - Skrivfel p� enhet@. Byt'>
MiscEr2. ;TEXTZ	<'Replace volume and PROCEED.  New'>
	TEXTZ	<'diskett och FORTS�TT. Den nya disketten'>
MiscEr3. ;TEXTZ	<'volume replaces old volume in sequence.'>
	TEXTZ	<'ers�tter den gamla.'>

ADOS1.	;TEXTZ	<'WARNING - Volume in drive@ contains'>
	TEXTZ	<'VARNING - Disketten i enhet@ inneh�ller'>
ADOS2.	;TEXTZ	<'AmigaDOS files.  Replace volume and '>
	TEXTZ	<'AmigaDOS-filer. Byt diskett och FORTS�TT'>
ADOS3.	;TEXTZ	<'PROCEED to avoid losing these files.'>
	TEXTZ	<'f�r att undvika f�rlust av dessa filer.'>

BSET1.	;TEXTZ	<'Backup volume in drive@ has a different'>
	TEXTZ	<'S�kerhetskopian i enhet@ �r m�rkt med ett'>
BSET2.	;TEXTZ	<'backup date/time stamp.  It does not belong'>
	TEXTZ	<'annat datum/tid. Den h�r inte till den kopie-'>
BSET3.	;TEXTZ	<'to the backup set you are restoring.'>
	TEXTZ	<'omg�ng du �terst�ller.'>

BSEQ1.	;TEXTZ	<'Volume % in drive@ is not the volume'>  
	TEXTZ	<'Diskett % i enhet@ �r inte den diskett'>
BSEQ2.	;TEXTZ	<'needed next.  Please remove it and'>
	TEXTZ	<'som beh�vs nu. Byt den och FORTS�TT,'>
BSEQ3.	;TEXTZ	<'PROCEED, or SKIP to bypass, or CANCEL.'>
	TEXTZ	<'HOPPA �VER, eller AVBRYT.'>

NotQB1.	;TEXTZ	<'Volume in drive@ is not a Quarterback'>
	TEXTZ	<'Disketten i enhet@ �r inte en Quarterback'>
NotQB2.	;TEXTZ	<'backup volume.  Load correct volume and'>
	TEXTZ	<'s�kerhetskopia. Mata in r�tt diskett och'>
NotQB3.	;TEXTZ	<'PROCEED, or SKIP to bypass, or CANCEL.'>
	TEXTZ	<'FORTS�TT, HOPPA �VER, eller AVBRYT.'>

ArcWP1.	;TEXTZ	<'Can''t update archive bits; drive is'>
	TEXTZ	<'Kan inte s�tta arkivflaggorna; enheten'>
ArcWP2.	;TEXTZ	<'write protected.  Enable writing and'>
	TEXTZ	<'�r skrivskyddad. Ta bort skrivskyddet'>
ArcWP3.	;TEXTZ	<'select PROCEED to retry.'>
	TEXTZ	<'och v�lj FORTS�TT f�r nytt f�rs�k.'>

RdEr1.	;TEXTZ	<'Read error on track 0 of volume in'>
	TEXTZ	<'L�sfel p� sp�r 0 p� disketten i enhet@.'>
RdEr2.	;TEXTZ	<'drive@.  Can''t restore from bad volume.'>
	TEXTZ	<'Kan inte �terst�lla fr�n en d�lig enhet.'>
RdEr3.	;TEXTZ	<'Select SKIP to proceed with next volume.'>
	TEXTZ	<'HOPPA �VER fors�tter med n�sta diskett.'>

CatEr1.	;TEXTZ	<'Error reading catalog from first volume.'>
	TEXTZ	<'Fel vid l�sning av bibliotek fr�n f�rsta'>
CatEr2.	;TEXTZ	<'Select SKIP to switch to alternate'>
	TEXTZ	<'disketten. HOPPA �VER v�ljer alternerande'>
CatEr3.	;TEXTZ	<'catalog on last backup volume.'>
	TEXTZ	<'biliotek p� sista disketten.'>

BDSK1.	;TEXTZ	<'WARNING - Volume in drive@ is unusable.'>
	TEXTZ	<'Disketten i enhet@ �r oanv�ndbar. Byt'>
BDSK2.	;TEXTZ	<'Replace bad volume and PROCEED.  New'>
	TEXTZ	<'denna och FORTS�TT. Den nya volymen'>
BDSK3.	;TEXTZ	<'volume replaces old volume in sequence.'>
	TEXTZ	<'ers�tter den gamla.'>

Dup1.	;TEXTZ	<'This file already exists:'>
	TEXTZ	<'Denna fil finns redan:'>
Dup2.	;TEXTZ	<'Do you want to replace it?'>
	TEXTZ	<'Vill du ers�tta den?'>

RecovErr.
	;TEXTZ	<'Bad volume recovery error...aborting'>
	TEXTZ	<'Problem med disketten...avbryter'>

ReadErr. ;TEXTZ	<'Read error on active volume'>
	TEXTZ	<'L�sfel p� aktiv diskett'>

CantRes. ;TEXTZ	<'Unable to restore this file:'>
	TEXTZ	<'Kan ej �terst�lla denna fil:'>

CantBak. ;TEXTZ	<'Unable to backup this file:'>
	TEXTZ	<'Kan ej s�kerhetskopiera denna fil:'>

NoneSel. ;TEXTZ	<'No files selected to process.'>
	TEXTZ	<'Inga filer angivna f�r bearbetning.'>

DiskBad. ;TEXTZ	<'Hard disk directory corrupted...continuing'>
	TEXTZ	<'H�rddisksbibliotek ol�sbart...forts�tter.'>

WrtErrRest.
	;TEXTZ	<'Write error while restoring file:'>
	TEXTZ	<'Skrivfel under �terst�llning av:'>

FileWrtPrt.
	;TEXTZ	<'Existing file is write protected.'>
	TEXTZ	<'Existerande fil skrivskyddad.'>

FileRdPrt.
	;TEXTZ	<'File is read protected.'>
	TEXTZ	<'Filen skyddad fr�n l�sning.'>

FileInUse.
	;TEXTZ	<'File is in use.'>
	TEXTZ	<'Existerande fil anv�nds.'>

FileDelPrt.
	;TEXTZ	<'File is protected from deletion.'>
	TEXTZ	<'Existerande fil skyddad fr�n borttagning.'>

DeviceFull.
	;TEXTZ	<'Device is full.'>
	TEXTZ	<'Enheten �r full.'>

WrtErrCode.
	;TEXTZ	<'Write error code: '>
	TEXTZ	<'Felkod vid skrivfel:'>


;These requester strings may appear at various times during a restore:

LoadN1.	;TEXTZ	<'Load volume % from the backup set'>
	TEXTZ	<'Mata in diskett % fr�n den kopieomg�ng'>
LoadN2.	;TEXTZ	<'you wish to restore into drive@'>
	TEXTZ	<'du vill �terst�lla i enhet@.'>

LoadL1.	;TEXTZ	<'Load the LAST volume from the backup set'>
	TEXTZ	<'Mata in sista disketten fr�n den kopie-'>
LoadL2.	;TEXTZ	<'you wish to restore into drive@'>
	TEXTZ	<'omg�ng du vill �terst�lla i enhet@.'>

DevOpnErr. ;TEXTZ <'Unable to open device@'>
	TEXTZ	<'Kan inte �ppna enhet@'>

CantSub. ;TEXTZ	<'Cannot create subdirectory'>
	TEXTZ	<'Kan inte skapa underbibliotek'>


;This message appears while reports are being produced:

NoCat.	;TEXTZ	<'There is no catalog to print.'>
	TEXTZ	<'Har ingen bibliotekslista att skriva.'>

PrtRpt.	;TEXTZ	<'Producing backup/restore report...please wait.'>
	TEXTZ	<'Skapar kopierings/�terst�llningsrapport...v�nta!'>

PrtCat.	;TEXTZ	<'Printing Quarterback catalog...please wait.'>
	TEXTZ	<'Skriver Quarterback bibliotekslista...v�nta.'>

;These text strings appear on backup/restore reports:

BackRpt. ;TEXTZ	<'Archive Report'>
	TEXTZ	<'Arkivrapport'>

RestRpt. ;TEXTZ	<'Restoration Report'>
	TEXTZ	<'�terst�llningsrapport'>

CatRpt.	;TEXTZ	<'Quarterback Catalog'>
	TEXTZ	<'Quarterback Bibliotekslista'>

BRptDev. ;TEXTZ	<'% files backed up from@'>
	TEXTZ	<'% filer s�kerhetskopierade fr�n@'>

RRptDev. ;TEXTZ	<'% files restored to@'>
	TEXTZ	<'% filer �terst�llda till@'>

Page.	;TEXTZ	<'Page '>
	TEXTZ	<'Sida '>

Err.	;TEXTZ	<' ** Error **'>
	TEXTZ	<'*** Fel ***'>


;These are general requester messages:

DirBlk.	;TEXTZ	<'Directory blocks not released.'>
	TEXTZ	<'Biblioteksblocken ej sl�ppta.'>

NoBatch. ;TEXTZ	<'I can''t find the command file.'>
	TEXTZ	<'Kan ej hitta kommandofilen.'>

BadSyntax.
	;TEXTZ	<'Command file error...See manual.'>
	TEXTZ	<'Kommandofilsfel...Se handboken.'>


;These messages are used to decode CLI command line options or command files:


With.	;TEXTZ	'WITH'
	TEXTZ	'MED'

Backup.	;TEXTZ	'BACKUP'
	TEXTZ	'S�KERHETSKOPIERA'

Restore. ;TEXTZ	'RESTORE'
	TEXTZ	'�TERST�LL'

All.	;TEXTZ	'ALL'
	TEXTZ	'ALLA'

Date.	;TEXTZ	'DATE'
	TEXTZ	'DATUM'

UCInc.	;TEXTZ	'INCLUDE'
	TEXTZ	'INKLUDERA'

UCExc.	;TEXTZ	'EXCLUDE'
	TEXTZ	'UTESLUT'

Arc.	;TEXTZ	'ARCHIVE'
	TEXTZ	'ARKIV'

For.	;TEXTZ	<' for '>
	TEXTZ	<' �ver '>

Files.	;TEXTZ	<'Files: '>
	TEXTZ	<'Filer: '>

AllRes.	;TEXTZ	<'All Rights Reserved'>
	TEXTZ	<'ALLA R�TTIGHETER F�RBEH�LLES'>

	END
