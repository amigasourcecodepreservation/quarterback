


These text strings appear in the Project menu for Quarterback.  George
forgot to list them in our previous fax to you.

'About Quarterback' - Displays the version and copyright message
'Save Options     ' - same as V2.3
'Print Catalog    ' - Causes a catalog to be printed on the printer
'Open New CLI     ' - same as V2.3
'Exit Quarterback ' - same as V2.3
___________________________________________________________________________
[the end]
