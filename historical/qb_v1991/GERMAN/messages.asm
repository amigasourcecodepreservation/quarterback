;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*							*
*	Quarterback language-dependent text messages	*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1987 Central Coast Software	*
*	    268 Bowie Dr, Los Osos, CA 93402		*
*	     All rights reserved, worldwide		*
*********************************************************

	INCLUDE	"vd0:MACROS.ASM"

	SECTION	MESSAGES,DATA

;;	XDEF	CP1.,CP2.,CP3.,CP4.,CP5.,CP6.
	XDEF	MONTHS
	XDEF	Restfm,Backto
	XDEF	ResOpt,ResExist,ResOvr,ResSub,ResRpt
	XDEF	BakOpt,BakArc,BakOvr,BakSub,BakRpt,BakChk
	XDEF	Slow.,ResTest
	XDEF	ChgBeep,BDev1Txt.,BDev2Txt.,RDev1Txt.,RDev2Txt.
	XDEF	SelFrom
	XDEF	FSel.,OutOf.,Bytes.,Disks.
	XDEF	Restto.,Backfm.
;	XDEF	Device.
	XDEF	Status,Status.
	XDEF	StRest.
	XDEF	StBack.
	XDEF	StTest.
	XDEF	Disk.
	XDEF	File.
	XDEF	DevNS
	XDEF	DevRdy
	XDEF	DevAct
	XDEF	DevNRdy
	XDEF	PlsLoad
	XDEF	PlsRem
	XDEF	GoodBak,GoodRst,GoodTst,GdBakArc
	XDEF	AbortMsg
;	XDEF	BadVol.,NotDir.
	XDEF	NoBrDev.,BadVol.,BadDir.,NotDir.
	XDEF	NoMemory
	XDEF	SubDir.
;	XDEF	EntNam.
	XDEF	IncNam1.,ExcNam1.
	XDEF	IncNam2.,ExcNam2.
	XDEF	Wild1.,Wild2.
	XDEF	IncDat1.,IncDat2.
	XDEF	ExcDat1.,ExcDat2.
;	XDEF	Onafter.
;	XDEF	Before.
	XDEF	Format.
	XDEF	BldCat.,FilCnt.
	XDEF	CatFor.
	XDEF	Legnd.,LgInc.,LgExc.
	XDEF	DiskBad.
	XDEF	LoadN1.,LoadN2.
	XDEF	LoadL1.,LoadL2.
	XDEF	NoPrt.,NoRpt.
	XDEF	BackRpt.,RestRpt.,CatRpt.
	XDEF	BRptDev.,RRptDev.
	XDEF	Page.
	XDEF	PrtRpt.,PrtCat.
	XDEF	DevOpnErr.
	XDEF	CantSub.
	XDEF	WrtPrt1.,WrtPrt2.,WrtPrt3.
	XDEF	MiscEr1.,MiscEr2.,MiscEr3.
	XDEF	ADOS1.,ADOS2.,ADOS3.
	XDEF	NotQB1.,NotQB2.,NotQB3.
	XDEF	BSEQ1.,BSEQ2.,BSEQ3.
	XDEF	BSET1.,BSET2.,BSET3.
	XDEF	BDSK1.,BDSK2.,BDSK3.
	XDEF	RdEr1.,RdEr2.,RdEr3.
	XDEF	CatEr1.,CatEr2.,CatEr3.
	XDEF	Dup1.,Dup2.
	XDEF	RecovErr.
	XDEF	With.,Backup.,Restore.,Date.,Arc.,UCInc.,UCExc.,All.
	XDEF	NoBatch.
	XDEF	BadSyntax.
;;	XDEF	ArcProt.
	XDEF	ArcWP1.,ArcWP2.,ArcWP3.
	XDEF	ReadErr.
	XDEF	CantBak.,CantRes.
	XDEF	NoneSel.
	XDEF	RPTxt.
	XDEF	ResArc
	XDEF	NoDev.
;	XDEF	UnkDev.
;	XDEF	MListErr.
	XDEF	SameDev.
	XDEF	MixedDev.
;	XDEF	DevMLErr.
	XDEF	NoDFn.
	XDEF	Err.
;	XDEF	Warning.
	XDEF	CatError
	XDEF	CantFindAltCat
	XDEF	DirBlk.
	XDEF	WrtErrRest.,DeviceFull.
	XDEF	FileRdPrt.,FileWrtPrt.
	XDEF	FileDelPrt.,FileInUse.
	XDEF	WrtErrCode.
	XDEF	NotFSD.
	XDEF	BakVol.,ResVol.
	XDEF	VolLst.
	XDEF	AllRes.
	XDEF	Files.
	XDEF	NoCat.,For.

MONTHS	TEXTZ	'JAN'
	TEXTZ	'FEB'
	TEXTZ	'MAR'
	TEXTZ	'APR'
	TEXTZ	'MAY'
	TEXTZ	'JUN'
	TEXTZ	'JUL'
	TEXTZ	'AUG'
	TEXTZ	'SEP'
	TEXTZ	'OCT'
	TEXTZ	'NOV'
	TEXTZ	'DEC'

;These text strings appear inside various gadgets:

Backto	;TEXTZ	<'BACKUP FILES'>
	TEXTZ	<'Datei sichern'>

Restfm	;TEXTZ	<'RESTORE FILES'>
	TEXTZ	<'Datei laden'>

; These text strings appear on the device selection screen:

VolLst.	;TEXTZ	<'Here are the hard disk volumes (drives or partitions) on your Amiga.'>
	TEXTZ	<'Folgende Festplatten (Laufwerk oder Partitionen) sind verf�gbar.'>

BakVol.	;TEXTZ	<'Click on the name of the volume you wish to backup from.'>
	DC.B	'Klicken Sie den Namen des Laufwerks an, '
	TEXTZ	<'von dem Sie Dateien sichern m�chten.'>

ResVol.	;TEXTZ	<'Click on the name of the volume you wish to restore to.'>
	DC.B	'Klicken Sie den Namen des Laufwerkes an, '
	TEXTZ	<'auf das Sie zur�ckschreiben m�chten.'>

Backfm.	;TEXTZ	<'Backup files from this hard disk volume:'>
	TEXTZ	<'Dateien von dieser Festplatte sichern:'>

Restto.	;TEXTZ	<'Restore files to this hard disk volume:'>
	TEXTZ	<'Dateien auf diese Festplatte zur�ckschreiben:'>

BDev1Txt.
	;TEXTZ	<'Backup files from hard disk to this primary device:'>
	;TEXTZ	<'Sichere Dateien der Festplatte auf Laufwerk:'>
	TEXTZ	<'Sichere Dateien auf Laufwerk:'>

BDev2Txt.
	;TEXTZ	<'Backup files from hard disk to this alternate device:'>
	;TEXTZ	<'Sichere Dateien der Festplatte abwechselnd auf Laufwerk:'>
	TEXTZ	<'Sichere Dateien abwechselnd auf Laufwerk:'>

RDev1Txt.
	;TEXTZ	<'Restore files to hard disk from this primary device:'>
	;TEXTZ	<'Laden der Dateien auf Festplatte von Laufwerk:'>
	TEXTZ	<'Laden der Dateien von Laufwerk:'>

RDev2Txt. 
	;TEXTZ	<'Restore files to hard disk from this alternate device:'>
	;TEXTZ 	<'Laden der Dateien auf Festplatte im Wechsel von Laufwerk:'>
	TEXTZ 	<'Laden der Dateien im Wechsel von Laufwerk:'>


;These are requester strings which may appear after device selection:

NoBrDev. ;TEXTZ	<'Please select a hard disk volume to backup/restore.'>
	TEXTZ	<'Bitte geben Sie den Festplattennamen zum sichern/zur�ckschreiben an.'>

BadVol. ;TEXTZ	<'Can''t find hard disk volume you entered.'>
	TEXTZ	<'Die angegebene Festplatte ist nicht vorhanden.'>

BadDir. ;TEXTZ	<'Can''t find the specified directory on that volume.'>
	TEXTZ	<'Das angegebene Verzeichnis existiert in diesem Laufwerk nicht.'>

NotDir.	;TEXTZ	<'Enter hard disk volume name, not a file name.'>
	TEXTZ	<'Bitte geben Sie einen Festplattennamen an, keinen Dateinamen.'>

NoDev.	;TEXTZ	<'You must specify a primary device.'>
	TEXTZ	<'Bitte geben Sie das erste Laufwerk an.'>

MixedDev. ;TEXTZ <'Primary and alternate devices must be of the same type.'>
	TEXTZ	<'Wechselweise benutzte Laufwerke m�ssen von dem gleichen Typ sein.'>

SameDev.  ;TEXTZ	<'Alternate device cannot be same as primary device.'>
	TEXTZ	<'Das zweite Laufwerk darf nicht gleich dem Ersten sein.'>

NoDFn.	  ;TEXTZ	<'@ is not available.'>
	TEXTZ	<'@ ist nicht verf�gbar.'>

NotFSD.	  ;TEXTZ	<' is not a file storage device.'>
	TEXTZ	<'Auf@ k�nnen keine Daten gesichert werden.'>

;;NoDev.	;TEXTZ	<'You must specify a primary device.'>
;;	TEXTZ	<'Sie m�ssen ein Laufwerk bestimmen.'>

;;MixedDev.
;;	;TEXTZ	<'Primary and alternate devices must be of the same type.'>
;;	TEXTZ	<'Wechselweisse benutzte Laufwerke m�ssen gleich sein.'>

;;SameDev. ;TEXTZ	<'Alternate device cannot be same as primary device.'>
;;	TEXTZ	<'Wechselseitiges Laufwerk kann nicht gleich Quellaufwerk sein.'>

;;NoDFn.	;TEXTZ	<' is not available.'>
;;	TEXTZ	<' nicht vorhanden.'>

;;BAD_DIR. ;TEXTZ	<'Hard disk device name or subdirectory path error.'>
;;	TEXTZ	<'Bezeichnung der Festplatte oder Unterverzeichnis falsch.'>

;;NotDir.	;TEXTZ	<'Enter a hard disk device name, not a file name.'>
;;	TEXTZ	<'Name der Festplatte angeben, keinen Dateinamen.'>

NoMemory ;TEXTZ	<'Out of memory ... can''t perform your request.'>
	TEXTZ	<'Nicht gen�gend Speicher, Befehl nicht ausf�hrbar.'>


; These text strings appear while building the catalog:

BldCat.	;TEXTZ	<'Building catalog of files ... please wait!'>
	TEXTZ	<'Erstelle Katalog der Dateien... bitte warten'>

FilCnt.	;TEXTZ	<'Files found:     '>
	TEXTZ	<'Gefundene Dateien:     '>

; These requester strings may appear during restore:

CatError ;TEXTZ	<'Unable to read backup catalog.'>
	TEXTZ	<'Katalog zur Sicherung nicht zu lesen.'>

CantFindAltCat 
	;TEXTZ	<'Unable to find alternate catalog.'>
	TEXTZ	<'Folge Katalog nicht vorhanden.'>


;These text strings appear on the catalog screen:

FSel.	;TEXTZ	<'Files included:'>
	TEXTZ	<'vorhandene Dateien:'>

OutOf.	;TEXTZ	<' of '>
	TEXTZ	<' von '>

Bytes.	;TEXTZ	<'Bytes: '>
	TEXTZ	<'Bytes: '>

Disks.	;TEXTZ	<'Volumes: '>
	TEXTZ	<'Disketten: '>

SubDir.	;TEXTZ	<'  (Subdirectory)       '>
	TEXTZ	<'  Unterverzeichnis     '>

SelFrom	;TEXTZ	<'Commands apply to:'>
	TEXTZ	<'Befehl g�ltig auf:'>

CatFor.	;TEXTZ	<'Catalog for: '>
	TEXTZ	<'Katalog f�r: '>

Legnd.	;TEXTZ	<'Legend: '>
	TEXTZ	<'Text:   '>

LgInc.	;TEXTZ	<' included '>
	TEXTZ	<'inklusive '>

LgExc.	;TEXTZ	<' excluded '>
	TEXTZ	<' exklusive '>


;These are requester text strings which may appear during selection of files on
;the catalog screen:

IncNam1. ;TEXTZ	<'Enter name of file to be included.'>
	TEXTZ	<'Geben Sie Namen der gew�nschten Datei an.'>
IncNam2. TEXTZ	<' '>

ExcNam1. ;TEXTZ	<'Enter name of file to be excluded.'>
	TEXTZ	<'Geben Sie den Namen der nicht ben�tigten'>
ExcNam2. TEXTZ	<'Datei an.'>

Wild1.	;TEXTZ	<'Use wildcards ''?'' or ''#?'' for groups.'>
	TEXTZ	<'F�r mehrere Dateien ''?'' oder ''#'''>
Wild2.	TEXTZ	<'als Joker benutzan.'>

IncDat1. ;TEXTZ	<'Include files which changed on or after'>
	TEXTZ	<'Inklusive der Dateien die am oder nach'>
IncDat2. TEXTZ	<'Datum ge�ndert werden'>

ExcDat1. ;TEXTZ	<'Exclude files which haven''t changed since'>
	TEXTZ	<'Au�er Dateien dei seit'>
ExcDat2. TEXTZ	<'nicht mehr ver�ndert wurden'>

Format.	;TEXTZ	<'Date format: 31-DEC-87'>
	TEXTZ	<'Datum format: 31-DEC-87'>


; These text strings appear on the backup/restore options screen:

ResOpt	;TEXTZ	<'SELECT RESTORE OPTIONS'>
	TEXTZ	<'Optionen zum R�ckspeichern bestimmen'>

BakOpt	;TEXTZ	<'SELECT BACKUP OPTIONS'>
	TEXTZ	<'Optionen zum Speichern bestimmen'>

ResOvr	;TEXTZ	<'Overwrite existing files:'>
	TEXTZ	<'Vorhandene Dateien �berschreiben'>

ResExist ;TEXTZ	<'Restore only files which already exist:'>
	TEXTZ	<'Nur vorhandene Dateien r�ckspeichern'>

BakArc	;TEXTZ	<'Set archive flag on backed-up files:'>
	TEXTZ	<'Setze Archiv-Flag auf gesicherte Dateien'>

ResArc	;TEXTZ	<'Set archive bit for restored files:'>
	TEXTZ	<'Setze Archiv-Flag bei R�cksicherung'>

BakOvr	;TEXTZ	<'Overwrite AmigaDOS format on floppies:'>
	TEXTZ	<'�berschreibe formatierte DOS Disketten'>

BakChk	;TEXTZ	<'Read data after write:'>
	TEXTZ	<'Daten nach dem Schreiben lesen'>

Slow.	;TEXTZ	<'Slow speed, reduced memory: '>
	TEXTZ	<'Langsamer, ben�tigt weniger Speicher'>

ResTest	;TEXTZ	<'Read backup volumes, don''t restore files:'>
	TEXTZ	<'Nur lesen, nicht zur�ckschreiben:'>

ChgBeep	;TEXTZ	<'Beep for QB volume change: '>
	TEXTZ	<'Signal bei Diskettenwechsel'>

BakRpt	;TEXTZ	<'Send archive report to:'>
	TEXTZ	<'Schicke Archivierungsreport an'>

ResRpt	;TEXTZ	<'Send restoration report to:'>
	TEXTZ	<'Schicke R�ckspeicherreport an'>

BakSub	;TEXTZ	<'Backup full subdirectory structure:'>
	TEXTZ	<'Sichere gesamte Verzeichnisstruktur'>

ResSub	;TEXTZ	<'Restore full subdirectory structure:'>
	TEXTZ	<'Gesamte Verzeichnisstruktur erneut laden'>


; This requester appears if you select DISK for the report:

RPTxt.	;TEXTZ	<'Enter device:path/name for report:'>
	TEXTZ	<'Laufwerk und Pfad/Name f�r Report:'>


;This requester text appears if the disk report path is bad:

NoPrt.	;TEXTZ	<'Can''t open printer for report...retry?'>
	TEXTZ	<'Drucker nicht erreichbar...Nocheinmal?'>

NoRpt.	;TEXTZ	<'Can''t open file for report...retry?'>
	TEXTZ	<'Datei kann nicht ge�ffnet werden...Nocheinmal?'>

; These text strings appear on the backup/restore status screen:


Status	;TEXTZ	<'S T A T U S'>
	TEXTZ	<'S T A T U S'>

StRest.	;TEXTZ	<'Restoring to '>
	TEXTZ	<'R�ckspeichern auf '>

StBack.	;TEXTZ	<'Backing up '>
	TEXTZ	<'Speichern auf '>

StTest.	;TEXTZ	<'Testing backup volumes'>
	TEXTZ	<'Die Sicherungsdisks werden �berpr�ft'>

Disk.	;TEXTZ	<'Volume '>
	TEXTZ	<'Ausgabe '>

File.	;TEXTZ	<'File '>
	TEXTZ	<'Datei '>

DevNS	;TEXTZ	<'No second drive selected'>
;	TEXTZ	<'Kein zweites Laufwerk vorgesehen'>
	TEXTZ	<'Zweites Laufwerk fehlt'>

Status.	;TEXTZ	<' status:'>
	TEXTZ	<' Status:'>

DevRdy	;TEXTZ	<'ready    '>
	TEXTZ	<'bereit      '>

DevAct	;TEXTZ	<'active   '>
	TEXTZ 	<'aktiv       '>

DevNRdy	;TEXTZ	<'not ready'>
	TEXTZ 	<'nicht bereit'>

PlsLoad	;TEXTZ	<'Please load volume '>
	TEXTZ	<'Bitte Diskette % laden'>

PlsRem	;TEXTZ	<'Please remove volume '>
	TEXTZ	<'Diskette entnehmen    '>

GoodBak	;TEXTZ	<'File backup completed.'>
	TEXTZ	<'Sichern der Dateien beendet.'>

GoodRst	;TEXTZ	<'File restore completed.'>
	TEXTZ	<'R�ckspeichern beendet.'>

GoodTst	;TEXTZ	<'Volume testing completed.'>
	TEXTZ	<'Disk�berpr�fung beendet'>

;;GoodBak	;TEXTZ	<'Backup successfully completed.'>
;;	TEXTZ	<'Sicherung erfolgreich abgeschlossen.'>

;;GoodRst	;TEXTZ	<'Restore successfully completed.'>
;;	TEXTZ	<'R�ckspeichern erfolgreich abgeschlossen.'>

GdBakArc ;TEXTZ	<'Backup successful -- setting archive bits.'>
	;TEXTZ	<'Sicherung erfolgreich -- setze Archivbits.'>
	TEXTZ	<'Sicherung erfolgreich -- setze Archiv Bit''s.'>

AbortMsg ;TEXTZ	<'WARNING ---- Backup/restore aborted ---- WARNING'>
	TEXTZ	<'  !!!  Sicherung/R�ckspeichern abgerochen  !!!  '>


;These requester messages may appear during the backup/restore process:

WrtPrt1. ;TEXTZ	<'WARNING - Volume in drive@ is write'>
	;TEXTZ	<'Achtung - Disk in Laufwerk@ schreibgesch�tzt.'>
	TEXTZ	<'WARNUNG - Disk in Laufwerk@ schreibgesch�tzt.'>
WrtPrt2. ;TEXTZ	<'protected.  Enable writing or replace'>
	;TEXTZ	<'Schreibschultz �ffnen oder Disk wechseln'>
	TEXTZ	<'Screibschutz entfernen oder Disk wechseln.'>
WrtPrt3. ;TEXTZ	<'volume.  PROCEED to retry.'>
	;TEXTZ	<'Vorgang wiederholen..'>
	TEXTZ	<'Vorgang wiederholen ...'>

MiscEr1. ;TEXTZ	<'WARNING - Error writing to drive@.'>
	;TEXTZ	<'ACHTUNG - Fehler beim Schreiben Laufwerk@'>
	TEXTZ	<'WARNUNG - Schreibfehler auf Laufwerk@.'>
MiscEr2. ;TEXTZ	<'Replace volume and PROCEED.  New'>
	;TEXTZ	<'Bitte Diskette austauschen. Die neue Diskette'>
	TEXTZ	<'Disk ersetzen. Die neue Disk'>
MiscEr3. ;TEXTZ	<'volume replaces old volume in sequence.'>
	;TEXTZ	<'ersetzt diese in der Sicherungssequenz.'>
	TEXTZ	<'ersetze die defekte in der Reihenfolge.'>

ADOS1.	;TEXTZ	<'WARNING - Volume in drive@ contains'>
	;TEXTZ	<'Achtung - Dateien auf Disk in Laufwerk@'>
	TEXTZ	<'WARNUNG - Disk in Laufwerk@ enth�lt'>
ADOS2.	;TEXTZ	<'AmigaDOS files.  Replace volume and '>
	;TEXTZ	<'im AmigaDOS. Zum Weitermachen Disk'>
	TEXTZ	<'AmigaDOS Dateien. Disk ersetzen, um'>
ADOS3.	;TEXTZ	<'PROCEED to avoid losing these files.'>
	;TEXTZ	<'wechseln, sonst Verlust der Dateien.'>
	TEXTZ	<'die Dateien nicht zu zerst�ren'>

BSET1.	;TEXTZ	<'Backup volume in drive@ has a different'>
	TEXTZ	<'Sicherungsdisk in Laufwerk@ enth�lt falsches'>
BSET2.	;TEXTZ	<'backup date/time stamp.  It does not belong'>
	TEXTZ	<'Datum/Zeit. Diese Disk geh�rt nicht zu dem'>
BSET3.	;TEXTZ	<'to the backup set you are restoring.'>
	TEXTZ	<'Satz Sicherungskopien.'>

BSEQ1.	;TEXTZ	<'Volume % in drive@ is not the volume'>  
	TEXTZ	<'Falsche Disk % in Laufwerk@. Richtige'>
BSEQ2.	;TEXTZ	<'needed next.  Please remove it and'>
	TEXTZ	<'Disk einlegen, �berspringen oder beenden.'>
BSEQ3.	;TEXTZ	<'PROCEED, or SKIP to bypass, or CANCEL.'>
	DC.B	0

NotQB1.	;TEXTZ	<'Volume in drive@ is not a Quarterback'>
	TEXTZ	<'Disk in Laufwerk@ ist keine Quarterback'>
NotQB2.	;TEXTZ	<'backup volume.  Load correct volume and'>
	TEXTZ	<'Sicherungsdisk. Richtige Disk einlegen,'>
NotQB3.	;TEXTZ	<'PROCEED, or SKIP to bypass, or CANCEL.'>
	TEXTZ	<'�berspringen oder beenden.'>

;;BSEQ1.	;TEXTZ	<'WARNING - Volume in drive@ is out of'>
;;	TEXTZ	<'Achtung - Falsche Reihenfolge der Disketten'>
;;BSEQ2.	;TEXTZ	<'sequence.  Load correct volume and'>
;;	TEXTZ	<'in Laufwerk@. Richtige Reihenfolge'>
;;BSEQ3.	;TEXTZ	<'PROCEED, or SKIP to bypass, or CANCEL.'>
;;	TEXTZ	<'herstellen, �berspringen oder beenden.'>

ArcWP1.	;TEXTZ	<'Can''t update archive bits; drive is'>
	TEXTZ	<'Die Archiv Bit''s k�nnen nicht gesetzt'>
ArcWP2.	;TEXTZ	<'write protected.  Enable writing and'>
	TEXTZ	<'werden; Laufwerk ist schreibgesch�tzt.'>
ArcWP3.	;TEXTZ	<'select PROCEED to retry.'>
	TEXTZ	<'Schreibschutz entfernen und fortfahren.'>

RdEr1.	;TEXTZ	<'Read error on track 0 of volume in'>
	TEXTZ	<'Lesefehler auf Spur Oder Disk in Laufwerk'>
RdEr2.	;TEXTZ	<'drive@.  Can''t restore from bad volume.'>
	TEXTZ	<'@. R�ckspeichern nicht m�glich. �ber-'>
RdEr3.	;TEXTZ	<'Select SKIP to proceed with next volume.'>
	TEXTZ	<'springen und mit n�chster Disk fortfahren.'>

CatEr1.	;TEXTZ	<'Error reading catalog from first volume.'>
	TEXTZ	<'Der Dateikatalog kann nicht von der ersten'>
CatEr2.	;TEXTZ	<'Select SKIP to switch to alternate'>
	TEXTZ	<'Disk gelesen werden. SPRUNG anw�hlen, um'>
CatEr3.	;TEXTZ	<'catalog on last backup volume.'>
	TEXTZ	<'den Katalog auf der letzten Disk zu lesen.'>

BDSK1.	;TEXTZ	<'WARNING - Volume in drive@ is unusable.'>
	;TEXTZ	<'Achtung - Disk in Laufwerk@ nicht'>
	TEXTZ	<'Sicherungdisk in Laufwerk@ ist un-'>
BDSK2.	;TEXTZ	<'Replace bad volume and PROCEED.  New'>
	;TEXTZ	<'verwendbar. Disk wechseln und fortfahren.'>
	TEXTZ	<'brauchbar. Disk ersetzen. Neue Disk'>
BDSK3.	;TEXTZ	<'volume replaces old volume in sequence.'>
	;TEXTZ	<'Neue Disk ersetzt alte Disk in Reihenfolge.'>
	TEXTZ	<'ersetze defekte in der Reihenfolge.'>

Dup1.	;TEXTZ	<'This file already exists:'>
	TEXTZ	<'Datei ist bereits vorhanden:'>
Dup2.	;TEXTZ	<'Do you want to replace it?'>
	TEXTZ	<'Soll sie ersetzt werden?'>

RecovErr.
	;TEXTZ	<'Bad volume recovery error...aborting'>
	TEXTZ	<'Weiterhin Fehlermeldung... Abbruch'>

ReadErr. ;TEXTZ	<'Read error on active volume'>
	TEXTZ	<'Lesefehler auf aktiver Disk'>

CantRes. ;TEXTZ	<'Unable to restore this file:'>
	TEXTZ	<'Datei kann nicht r�ckgespeichert werden:'>

CantBak. ;TEXTZ	<'Unable to backup this file:'>
	TEXTZ	<'Datei kann nicht gesichert werden:'>

NoneSel. ;TEXTZ	<'No files selected to process.'>
	TEXTZ	<'Keine Datei zum Fortfahren bestimmt.'>

DiskBad. ;TEXTZ	<'Hard disk directory corrupted...continuing'>
	TEXTZ	<'Defektes Festplatten-Verzeichnis... fahre fort'>

;;ArcProt. ;TEXTZ	<'Can''t update archive bits...write protected.'>
;;	TEXTZ	<'Erneuerung der Archive-Bits nicht zu setzen... Schreibschutz.'>

WrtErrRest.
	;TEXTZ	<'Write error while restoring file:'>
	;TEXTZ	<'Schreibfehler beim Wiedereinsetzen von Dateien:'>
	TEXTZ	<'Schreibfehler beim R�ckspeichern:'>

FileWrtPrt.
	;TEXTZ	<'File is write protected.'>
	;TEXTZ	<'Datei ist schreibgesch�tzt.'>
	TEXTZ	<'Datei ist schreibgesch�tzt.'>

FileRdPrt.
	;TEXTZ	<'File is read protected.'>
	;TEXTZ	<'Datei ist lesegesch�tzt.'>
	TEXTZ	<'Datei ist lesegesch�tzt.'>

FileInUse.
	;TEXTZ	<'File is in use.'>
	;TEXTZ	<'Datei wird bearbeitet.'>
	TEXTZ	<'Datei ist in Gebrauch.'>

FileDelPrt.
	;TEXTZ	<'File is delete protected.'>
	;TEXTZ	<'Datei ist l�schgesch�tzt.'>
	TEXTZ	<'Datei ist vor dem L�schen gesch�tzt.'>

DeviceFull.
	;TEXTZ	<'Device is full.'>
	;TEXTZ	<'Diskette ist voll.'>
	TEXTZ	<'Device ist voll.'>

WrtErrCode.
	;TEXTZ	<'Write error code: '>
	;TEXTZ	<'Schreibfehler Kode:'>
	TEXTZ	<'Schreibfehler Nr.:'>


;These requester strings may appear at various times during a restore:

LoadN1.	;TEXTZ	<'Load volume % from the backup set'>
	;TEXTZ	<'Lade Disk % der Sicherungskopien'>
	TEXTZ	<'Lege Disk % der Sicherungskopien'>
LoadN2.	;TEXTZ	<'you wish to restore into drive@'>
	;TEXTZ	<'zum R�ckspeichern in Laufwerk@'>
	TEXTZ	<'zum R�ckspeichern in Laufwerk@.'>

LoadL1.	;TEXTZ	<'Load the LAST volume from the backup set'>
	;TEXTZ	<'Lade die letze Disk der Sicherungskopien'>
	TEXTZ	<'Lege die LETZTE Disk der Sicherungs-'>
LoadL2.	;TEXTZ	<'you wish to restore into drive@'>
	;TEXTZ	<'die r�ckgespeichert werden in Laufwerk@'>
	TEXTZ	<'kopien in Laufwerk@.'>

DevOpnErr. ;TEXTZ <'Unable to open device@'>
	;TEXTZ	<'Ger�t@ kann nicht angesprochen werden'>
	TEXTZ	<'Device@ kann nicht ge�ffnet werden.'>

CantSub. ;TEXTZ	<'Cannot create subdirectory'>
	;TEXTZ	<'Unterverzeichnis kann nicht erstellt werden'>
	TEXTZ	<'Unterverzeichnis kann nicht erstellt werden.'>


;This message appears while reports are being produced:

NoCat.	;TEXTZ	<'There is no catalog to print.'>
	TEXTZ	<'Kein Sicherungsreport vorhanden.'>

PrtRpt.	;TEXTZ	<'Producing backup/restore report...please wait.'>
	TEXTZ	<'Erstelle Sicherungs/R�ckspeicher Report.  Bitte warten!'>

PrtCat.	;TEXTZ	<'Printing Quarterback catalog...please wait.'>
	TEXTZ	<'Drucke Quarterback Sicherungsreport...bite warten.'>

;These text strings appear on backup/restore reports:

BackRpt. ;TEXTZ	<'Archive Report'>
	TEXTZ	<'Archivierungs Report'>

RestRpt. ;TEXTZ	<'Restoration Report'>
	TEXTZ	<'Report der Wiederherstellung'>

CatRpt.	;TEXTZ	<'Quarterback Catalog'>
	TEXTZ	<'Quarterback Sicherungsreport'>

BRptDev. ;TEXTZ	<'% files backed up from@'>
	;TEXTZ	<'% Dateien von Laufwerk@ gespeichert'>
	TEXTZ	<'% Dateien wurden von@ gesichert.'>

RRptDev. ;TEXTZ	<'% files restored to@'>
	;TEXTZ	<'% Dateien zu Laufwerk@ zur�ckgespeichert'>
	TEXTZ	<'% Dateien wurden auf@ zar�ckgeschreiben'>

Page.	;TEXTZ	<'Page '>
	TEXTZ	<'Seite '>

Err.	;TEXTZ	<' ** Error **'>
	TEXTZ	<' ** Fehler **'>


;These are general requester messages:

DirBlk.	;TEXTZ	<'Directory blocks not released.'>
	TEXTZ	<'Biblioteksblocken ej sl�ppta.'>

NoBatch. ;TEXTZ	<'I can''t find the command file.'>
	TEXTZ	<'Dieser Befehl ist nicht zur Verf�gung.'>

BadSyntax.
	;TEXTZ	<'Command file error...See manual.'>
	TEXTZ	<'Befehlsdatei Fehler... Siehe Handbuch.'>


;These messages are used to decode CLI command line options or command files:


With.	;TEXTZ	'WITH'
	TEXTZ	'MIT'

Backup.	;TEXTZ	'BACKUP'
	TEXTZ	'ABSPEICHERN'

Restore. ;TEXTZ	'RESTORE'
	TEXTZ	'WIEDEREINSETZEN'

All.	;TEXTZ	'ALL'
	TEXTZ	'ALLES'

Date.	;TEXTZ	'DATE'
	TEXTZ	'DATUM'

UCInc.	;TEXTZ	'INCLUDE'
	TEXTZ	'INKLUSIZE'

UCExc.	;TEXTZ	'EXCLUDE'
	TEXTZ	'EXKLUSIVE'

Arc.	;TEXTZ	'ARCHIVE'
	TEXTZ	'ARCHIV'

For.	;TEXTZ	<' for '>
	TEXTZ	<' f�r '>

Files.	;TEXTZ	<'Files: '>
	TEXTZ	<'Dateien: '>

AllRes.	TEXTZ	<'All Rights Reserved'>

	END
