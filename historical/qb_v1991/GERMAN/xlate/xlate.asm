;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
Here is the full set of Quarterback text message strings, as they appear
on the screen.  Where Quarterback inserts text into the message, the part
inserted by Quarterback is indicated in square brackets [].  For example,
where Quarterback inserts a device name, you will see [device].  Where
Quarterback inserts a number, you will see [n].  In some cases the number
inserted by Quarterback can be up to 4 digits.

Where text appears inside a gadget, we must try hard to keep the translated
words the same size as the original English words, please!

These text strings appear inside various Quarterback gadgets:

'BACKUP FILES'
'RESTORE FILES'
'CANCEL'
'PROCEED'
'ABORT'


These text strings appear inside gadgets associated with various
requesters:

'Cancel'
'Proceed'
'Skip'
' NO  '
'  YES '
' Okay '


These text strings appear on the device selection screen:

'Enter hard disk device name and optional subdirectory path to restore to:'

'Enter hard disk device name and optional subdirectory path to backup from:'

'Backup files from hard disk to this primary device:'

'Backup files from hard disk to this alternate device:'

'Restore files to hard disk from this primary device:'

'Restore files to hard disk from this alternate device:'


These are requester text strings which may appear after device selection:

'You must specify a primary device.'

'Device not found in Devs:Mountlist.'

'Device Mountlist entry not complete.'

'Primary and alternate devices must be the same type.'

'Error processing Devs:Mountlist.'

'Drive DF[n] is not available.'

'Hard disk device name or subdirectory path error.'

'Enter a hard disk device name, not a file name.'

'Out of memory ... can't perform your request.'


These text strings appear while Quarterback builds the catalog:

'Building catalog of files ... please wait!'

'Files found: [n]'


These requester text strings may appear while loading the catalog
from the backup disks during a restore:

'Unable to read backup catalog.'

'Unable to find alternate catalog.'


These text messages appear on the catalog screen:

'Files included:'

'[n] of [n]'

'Bytes: [n]'

'Volumes: [n]'

'  (Subdirectory)       '

'Commands apply to:'

'Catalog for: [device:path]'

'Legend: included excluded'


These are gadget texts for the catalog screen:

'ROOT'
'PARENT'

'Current DIR'
'and SubDIRs'

'Current DIR'
'   Only    '

'Exclude'
'  All  '

'Include'
'  All  '

'Exclude'
'by Date'

'Include'
'by Date'

'Exclude'
'by Name'

'Include'
'by Name'

'Exclude'
'Archive'

'Include'
'Archive'


These are requester text strings which may appear during selection of
files on the catalog screen:

'Enter name of file to be included.'
'Use wildcards '?' or '#?' for groups.'

'Enter name of file to be excluded.'
'Use wildcards '?' or '#?' for groups.'

'Include files which changed on or after [date]'
'Date format: 31-DEC-87'

'Exclude files which haven't changed since [date]'
'Date format: 31-DEC-87'


These text strings appear on the backup/restore options screen:

'SELECT RESTORE OPTIONS'

'SELECT BACKUP OPTIONS'

'Overwrite existing files:'

'Restore only files which already exist:'

'Set archive flag on backed-up files:'

'Set archive bit for restored files:'

'Overwrite AmigaDOS format on floppies:'

'Read data after write:'

'Beep for QB volume change: '

'Send archive report to:'

'Send restoration report to:'

'Backup full subdirectory structure:'

'Restore full subdirectory structure:'


This requester text appears if you select DISK for the report:

'Enter device:path/name for report:'


This requester text appears if the disk report path is bad:

'Can't open device for report...retry?'


These are gadget strings for the options screens:

'YES'
'NO'

'QUIET'
'BEEP'
'FLASH'

'NONE'
'DISK'
'PRT:'


These text strings appear on the backup/restore status screen:

'S T A T U S'

'Restoring to [device]'

'Backing up [device]'

'Volume [n] of [n]'

'File [device:path/filename]'

'No second drive selected'

'[device] status: ready    '

'[device] status: active   '

'[device] status: not ready'

'Please load volume [n]'

'Please remove volume [n]'

'Backup successfully completed.'

'Restore successfully completed.'

'WARNING ---- Backup/restore aborted ---- WARNING'


This string appears in a gadget on the status screen inside the 
status box when the backup/restore device is not a floppy drive:

'Volume Changed'


These requester text messages may appear during the backup/restore process:

'WARNING - Volume in drive [drive] is write'
'protected.  Enable writing or replace'
'volume.  PROCEED to retry.'

'WARNING - Volume in drive [drive] has a'
'write error.  Replace volume and'
'PROCEED or CANCEL.'

'WARNING - Volume in drive [drive] contains'
'AmigaDOS files.  Replace volume and '
'PROCEED to avoid losing these files.'

'WARNING - Volume in drive [drive] is out of'
'sequence.  Load correct volume and'
'PROCEED, or SKIP to bypass, or CANCEL.'

'WARNING - Volume in drive [drive] is not a'
'Quarterback volume.  Load correct volume;'
'PROCEED, or SKIP to bypass, or CANCEL.'

'WARNING - Volume in drive [drive] is unusable.'
'Replace bad volume and PROCEED.  New'
'volume replaces old volume in sequence.'

'This file already exists: [filename]
'Do you want to replace it?'

'Bad volume recovery error...aborting'

'Read error on active volume'

'Unable to restore this file: [filename]'

'Unable to backup this file: [filename]'

'No files selected to process.'

'Hard disk directory corrupted...continuing'

'Can't update archive bits...write protected.'

'Write error while restoring file:'

'File is write protected.'

'File is read protected.'

'File is in use.'

'File is delete protected.'

'Device is full.'

'Write error code:'

These requester text strings may appear at various times during a
restore:

'Load volume [n] from the backup set'
'you wish to restore into drive [drive]'

'Load the LAST volume from the backup set'
'you wish to restore into drive [drive]'

'Unable to open device [device]'

'Cannot create subdirectory'


This message appears while reports are being produced:

'Producing backup/restore report...please wait.'


These text strings appear on the backup/restore reports:

'Archive Report'

'Restoration Report'

'[n] files backed up from [device]'

'[n] files restored to [device]'

'Page [n]'

' ** Error **'


These are general requester messages:

'Directory blocks not released.'

'I can't find the command file.'

'Command file error...See manual.'


These messages are used to decode CLI command line options or command
files:

'WITH'
'BACKUP'
'RESTORE'
'ALL'
'DATE'
'INCLUDE'
'EXCLUDE'
'ARCHIVE'

[The end]

