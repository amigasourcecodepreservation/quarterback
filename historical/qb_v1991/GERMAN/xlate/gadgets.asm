;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
These text strings appear inside gadgets throughout Quarterback.  Since
gadget size is always predefined, we must try hard to keep the translated
words the same size as the original English words.

General gadget text:

'CANCEL'
'PROCEED'
'ABORT'

These are gadget texts for the options screens:

'YES'
'NO'

'QUIET'
'BEEP'
'FLASH'

'NONE'
'DISK'
'PRT:'

These are gadget texts for the catalog screen:

'ROOT'
'PARENT'

'Current DIR'
'and SubDIRs'

'Current DIR'
'   Only    '

'Exclude'
'  All  '

'Include'
'  All  '

'Exclude'
'by Date'

'Include'
'by Date'

'Exclude'
'by Name'

'Include'
'by Name'

'Exclude'
'Archive'

'Include'
'Archive'

This string appears in a gadget on the status screen inside the 
status box when the backup/restore device is not a floppy drive:

'Volume Changed'

These text strings appear inside gadgets associated with requesters:

'Cancel'
'Proceed'
'Skip'
' NO  '
'  YES '
' Okay '

