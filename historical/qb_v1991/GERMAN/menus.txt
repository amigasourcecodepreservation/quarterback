


These text strings appear in the Project menu for Quarterback.  George
forgot to list them in our previous fax to you.

'About Quarterback' - Displays the version and copyright message
'Save Options     ' - same as V2.3
'Print Catalog    ' - Causes a catalog to be printed on the printer
'Open New CLI     ' - same as V2.3
'Exit Quarterback ' - same as V2.3
___________________________________________________________________________

These text strings appear on the device selection screen.  The German
translation you sent to us is too long for the space provided.  
Please, if possible, translate the text to a shorter German version.

'Click on the name of the volume you wish to backup from.

'Klicken Sie den Namen des Laufwerks an,'
'von dem Sie Dateien sichern m�chten.'
                                    
This message just barely fits the space.  You can leave it as is, unless
you want to change it to be similar to the changes you may make to the 
message below.

___________________________________________________________________________

'Click on the name of the volume you wish to restore to.'

'Klicken Sie den Namen des Laufwerkes an,' 
'auf das Sie Dateien zur�kschreiben m�chten.'

This message is too long here.......^
____________________________________________________________________________

The error message for failure to open an output device for the archive report
has been expanded to two messages: one for the printer and one for a disk
file:

'Can't open printer for report...retry?'

'Can't open file for report...retry?'

____________________________________________________________________________
[the end]
