;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*							*
*	QUARTERBACK directory handling routines		*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1987 Central Coast Software	*
*	    268 Bowie Dr, Los Osos, CA 93402		*
*	     All rights reserved, worldwide		*
*********************************************************


	INCLUDE	"vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"vd0:BOXES.ASM"

	XREF	SavePC

	XDEF	BuildDir,InitList
	XDEF	InitDirectory,CloseDirectory
	XDEF	NextFile,UpdateNumbers,UpdateWindow,UpdatePath
	XDEF	UpdateScrollBar
	XDEF	Root,Parent,SAll,DAll,SNam,DNam,SDat,DDat,SArc,DArc
	XDEF	IncAll,ExcAll,IncArc,ExcArc
	XDEF	UpArrow,DnArrow,PropHit,FileGadHit
	XDEF	ScrollWindow
	XDEF	InitScan,GetNextFile
	XDEF	ReadDir
	XDEF	ConvertDosDate,ConvertDosTime
	XDEF	InitPath,AppendPath,TruncatePath
;;	XDEF	CurrentPath
;;	XDEF	ChgOrigDir
	XDEF	MovePad
	XDEF	ValidateDate,DateScan,NameScan
	XDEF	RecalcNumbers

	XDEF	TotFiles,SelFiles
	XDEF	TotVols,SelVols
	XDEF	TotBytes,SelBytes
	XDEF	AbsolutePos
	XDEF	CatSize
	XDEF	DisksPos,BytesPos,FilTotPos
	XDEF	UpdWinFlg
	XDEF	CurLevel,RootLevel
	XDEF	FILE_INFO,FIB_TYPE
	XDEF	Path.
;;	XDEF	OriginalDir
	XDEF	IncFlg

	XREF	SysBase,DosBase,GraphicsBase,IntuitionBase
	XREF	WINDOW,FontPtr
	XREF	MLoop,GAD_PTR,MSG_CLASS,CDASFlg
;	XREF	INameTxt,ENameTxt,IDateTxt,EDateTxt

	XREF	StripAppend,ClearBox,REFRESH

	XREF	OpenRequester,CloseRequester,WaitRequester

	XREF	FileBox,DirBox,DatBox,NamBox
	XREF	PropPot,PropGad,ReqDateGad,ReqNameGad
	XREF	IDatTxt,EDatTxt,INamTxt,ENamTxt

	XREF	TMP.,TMP2.,BRDevPathBuf,DatBuf,NamBuf
	XREF	FileGList
	XREF	DirBlk.
	XREF	SubDir.
;	XREF	EntNam.
;	XREF	IncNam.,ExcNam.
;	XREF	Inc.,Exc.
	XREF	CatFor.
	XREF	DiskBad.
	XREF	DirErr.,CantSub.
	XREF	FilCnt.
	XREF	NoMemory
	XREF	MONTHS

	XREF	RSubFlg,TestFlg,Mode,EmptyFlg

	XREF	GetChar,GetWord,GetLong,ProcessMsg
	XREF	ActiveParams,Dev0Params,ActiveDev,ActiveDsk
	XREF	OpenFloppies,CloseFloppies,MotorOff
	XREF	RestDateProt

	XREF	FirstDskFlg,CurFib
	XREF	FillCnt,AbortFlg
	XREF	VolumeSize
	XREF	REQGAD,REQTXT,DT_TXT

DirBlockSize	EQU	2000	;size of directory block
MaxFileGads	EQU	20	;max files 
FontHt		EQU	8
DirTxtLen	EQU	52	;chars in directory text line
CatEntSize	EQU	14 ;size=4 date=2 min=2 sec=2 filcnt=2 flags=1 prot=1
FileNameLen	EQU	30
FileNoteLen	EQU	79

ScrollWindow:
	IFLEIW	MaxEnt,#MaxFileGads,9$,L ;nothing to scroll
	MOVE.W	MaxEnt,D1
	SUB.W	#MaxFileGads,D1		;calc based on top of window
	MOVE.W	D1,D0
	MULU	PropPot,D0
	DIVU	#$FFFF,D0
;	DECW	D1			;fudge factor
	IFLEW	D0,D1,13$		;valid pos
	MOVE.W	D1,D0
13$:	MOVE.W	D0,D6			;new pos
	MOVE.W	CurEnt,D7		;old pos to D7
	IFEQW	D0,D7,9$		;no change in pos...ignore it
	BCS.S	3$			;new>old...
	SUB.W	D0,D7			;new<old...how far?
	IFGTIW	D7,#5,5$		;more than 5
11$:	BSR	NextUp
	ERROR	9$
	IFGTW	CurEnt,D6,11$		;loop till we'e there
	BRA.S	9$
3$:	SUB.W	D7,D0			;how far?
	IFGTIW	D0,#5,5$		;more than 5
12$:	BSR	NextDown
	ERROR	9$
	IFLTW	CurEnt,D6,12$		;loop till we're there
	BRA.S	9$
5$:	MOVE.L	CurLevel,A0
	MOVE.L	dl_ChildPtr(A0),A1	;this is where we start counting
	BEQ.S	9$			;nothing to search
	MOVE.W	D6,CurEnt
	MOVE.W	D6,D1			;looking for this entry
	BEQ.S	8$			;first entry
6$:	MOVE.L	df_Next(A1),A1		;try the next one
	IFZL	A1,9$			;end of list...corrupted?
	DECW	D1			;when it goes to 0, we've found it
	BNE.S	6$			;not end of list...keep checking
8$:	MOVE.L	A1,dl_CurFib(A0)	;make this one the top item
	BSR	UpdWinNoClear		;and redisplay window
9$:	CLR.B	UpdWinFlg
	RTS

PropHit:
	MOVE.L	WINDOW,A0
	MOVE.L	wd_IDCMPFlags(A0),D0
;	MOVE.L	#CLOSEWINDOW+GADGETUP+GADGETDOWN,D0
	BCLR	#4,D0			;disable mousemove
	IFEQIL	MSG_CLASS,#GADGETUP,1$	;turn off mouse
;	MOVE.L	#CLOSEWINDOW+GADGETUP+GADGETDOWN+MOUSEMOVE,D0
	BSET	#4,D0			;enable mousemove
	SETF	UpdWinFlg
1$:	CALLSYS	ModifyIDCMP,IntuitionBase ;enable/disable the mouse
	JMP	MLoop

FileGadHit:
	MOVE.L	GAD_PTR,A0		;find this gadget
	MOVE.W	gg_GadgetID(A0),D0	;range: 1-20
	MOVE.W	D0,D1			;save the level
	MOVE.L	CurLevel,A0
	MOVE.L	dl_CurFib(A0),A1	;this is where we start counting
	BRA.S	2$			;dont link fwd on first pass
1$:	MOVE.L	df_Next(A1),A1		;try the next one
2$:	IFZL	A1,9$			;end of list...clicked on inactive
	DECW	D1			;when it goes to 0, we've found it
	BNE.S	1$			;not end of list...keep checking
3$:	BTST	#7,df_Flags(A1)		;a directory?
	BNE.S	4$			;yes...going to next level
	BCHG	#6,df_Flags(A1)		;else reverse selected state
	MOVE.L	A1,A0			;and update this line's color
	BSR	DispFileInfo		;line in D0
	BRA.S	9$
4$:;	MOVE.L	A1,dl_CurFib(A0)	;start here next time
	MOVE.W	CurEnt,dl_CurEnt(A0)	;save prop body value
	MOVE.L	df_SubLevel(A1),A0	;this becomes current level
	MOVE.L	A0,CurLevel
	MOVE.L	dl_ChildPtr(A0),dl_CurFib(A0) ;start at first file
	CLR.W	CurEnt			;beginning of list
	LEA	df_Name(A1),A0
	BSR	AppendPath
	BSR	UpdatePath
	BSR	UpdateWindow
	BSR	CountMaxEnt
	BSR	UpdateScrollBar
9$:	BSR	UpdateNumbers
	JMP	MLoop

* Scrolling arrows

UpArrow:
	IFEQL	MSG_CLASS,#GADGETUP,9$	;ignore up
	BSR	NextUp
	BSR	UpdateScrollBar
	BSR	ScrollDelay
	MOVE.L	GAD_PTR,A0
	BTST	#7,gg_Flags+1(A0)	;gadget still selected?
	BNE.S	UpArrow			;yes
9$:	JMP	MLoop			;else bail out

NextUp:
	MOVE.L	CurLevel,A1
	MOVE.L	dl_ChildPtr(A1),A0
	MOVE.L	dl_CurFib(A1),A2
	IFEQL	A0,A2,9$		;already at top...can't do more
1$:	MOVE.L	A0,A3			;save ptr to 'previous'
	MOVE.L	df_Next(A0),A0
	IFNEL	A0,A2,1$		;loop till we find the current one
	MOVE.L	A3,dl_CurFib(A1)	;and make this one current top
	BSR	ScrollDown		;else make room at top
	MOVEQ	#1,D0			;display at line 1
	MOVE.L	A3,A0
	BSR	DispFileInfo		;fill it in
	IFZW	CurEnt,8$
	DECW	CurEnt
8$:	RTS
9$:	CLR.W	CurEnt
	STC
	RTS

DnArrow:
	IFEQL	MSG_CLASS,#GADGETUP,9$	;ignore up
	BSR	NextDown
	BSR	UpdateScrollBar
	BSR	ScrollDelay
	MOVE.L	GAD_PTR,A0
	BTST	#7,gg_Flags+1(A0)	;gadget still selected?
	BNE.S	DnArrow			;yes
9$:	JMP	MLoop			;else bail out

NextDown:
	MOVE.L	CurLevel,A1		;this level
	MOVE.L	dl_CurFib(A1),A2	;get current top entry
	MOVE.L	A2,A0
	ZAP	D0			;start at level 1
1$:	MOVE.L	df_Next(A0),A0		;else get link
2$:	IFZL	A0,9$			;end of list before line 20
	INCW	D0
	IFLTIW	D0,#MaxFileGads,1$	;loop till we find last one
	MOVE.L	A0,A3			;save ptr to new last line
	MOVE.L	df_Next(A2),dl_CurFib(A1) ;make next entry the top item
	BSR	ScrollUp		;make room at bottom
	MOVEQ	#MaxFileGads,D0		;goes on line 20
	MOVE.L	A3,A0
	BSR	DispFileInfo
	INCW	CurEnt
;	BSR	UpdateScrollBar
	RTS
9$:	STC
	RTS

* Waits for a while during scrolling, to prevent scrolling too fast.

ScrollDelay:
	MOVEQ	#4,D1
	CALLSYS	Delay,DosBase
	RTS

InitDirectory:
	LEA	RootLevel,A0
	MOVE.L	A0,CurLevel		;point to base level
	MOVE.L	dl_ChildPtr(A0),dl_CurFib(A0)
	CLR.W	CurEnt
	BSR	CountMaxEnt
	BSR	InitPath
	RTS

Root:	LEA	RootLevel,A4		;reset to top level
	IFEQL	CurLevel,A4,Pex,L	;already there
	MOVE.L	A4,CurLevel
	BSR	InitPath
	BRA.S	PCom

Parent:	MOVE.L	CurLevel,A4		;move back to prev level
	MOVE.L	dl_ParLevel(A4),A4
	IFZL	A4,Pex			;already at root level...stop here
	MOVE.L	A4,CurLevel		;this is our current level
	BSR	TruncatePath

PCom:
;	MOVE.L	dl_ChildPtr(A4),dl_CurFib(A4) ;start at first name in list
	MOVE.W	dl_CurEnt(A4),CurEnt
;	CLR.W	CurEnt
	BSR	CountMaxEnt
	BSR	UpdatePath
	BSR	UpdateWindow
	BSR	UpdateScrollBar
Pex:	JMP	MLoop

InitPath:
	MOVE.	BRDevPathBuf,Path.
	IFLCEQ.	Path.,#':',1$
	IFEQIB	D0,#'/',1$
	ACHAR.	#'/',Path.
1$:	RTS

* Appends name pointed to by A0 to Path.  Limits Path. to PathSize.
* On restore, if full path NOT being restored, does nothing.

AppendPath:
	IFZB	Mode,3$			;backup
	IFNZB	RSubFlg,3$		;restoring full path
	RTS
3$:	MOVE.W	#PathSize-3,D1		;max path with room for '/' and null
	LEA	Path.,A1
1$:	TST.B	(A1)
	BEQ.S	2$			;end of Path.?
	INCL	A1			;no
	DBF	D1,1$			;keep searching
	RTS				;oops...already too long
2$:	MOVE.B	(A0)+,(A1)+		;add a new char
	DBEQ	D1,2$			;loop till end of new string or limit
	DECL	A1
	MOVE.B	#'/',(A1)+		;trailing slash
	CLR.B	(A1)			;null term
	RTS

* Deletes last subdir name from Path.
* On restore, if full path NOT being restored, does nothing.

TruncatePath:
	IFZB	Mode,5$			;backup
	IFNZB	RSubFlg,5$		;restoring full path
	RTS
5$:	MOVE.L	A1,-(A7)
	LEA	Path.,A0		;point to path
	MOVE.L	A0,A1
1$:	TST.B	(A0)+			;find the end of it
	BNE.S	1$
	DECL	A0
	DECL	A0
2$:	MOVE.B	-(A0),D0		;get previous char
	IFEQIB	D0,#':',3$		;thats it...
	IFNEIB	D0,#'/',2$		;no...loop
3$:	IFLEL	A0,A1,4$		;out of range of PATH.
	CLR.B	1(A0)			;else terminate string here
4$:	MOVE.L	(A7)+,A1
	RTS
	
* Redisplays the subdirectory path.

UpdatePath:
	LEA	DirBox,A0		;first clear the old path out of box
	BSR	ClearBox
	MOVE.	Path.,TMP.
	LEA	TMP.,A0
	CLR.B	42(A0)			;truncate to safe length
	DispMsg	#Dir_LE+4,#Dir_TE+2,CatFor.,WHITE
	DispCur	TMP.,ORANGE
	RTS

CountMaxEnt:
	MOVE.L	CurLevel,A1
	MOVE.L	dl_ChildPtr(A1),A0
	ZAP	D0
1$:	IFZL	A0,9$
	INCW	D0
	MOVE.L	df_Next(A0),A0
	BRA.S	1$
9$:	MOVE.W	D0,MaxEnt
	RTS

UpdateScrollBar:
	PUSH	D2-D5/A2
	MOVEQ	#-1,D1
	MOVE.W	D1,D4			;default body
	ZAP	D2			;default pot
	MOVEQ	#MaxFileGads,D0
	IFLEW	MaxEnt,D0,1$		;entire list on screen
;	IFGEW	D0,D2,1$		;entire text on screen
;	ZAP	D2
	MOVE.W	MaxEnt,D2
	MULU	D1,D0
	DIVU	D2,D0
	MOVE.W	D0,D4			;new slider size
	ZAP	D0
	MOVE.W	CurEnt,D0
	SUB.W	#MaxFileGads,D2
	MULU	D1,D0
	DIVU	D2,D0
	MOVE.W	D0,D2
1$:	LEA	PropGad,A0
	MOVEQ	#5,D0
	ZAPA	A2
	MOVEQ	#1,D5
	MOVE.L	WINDOW,A1
	CALLSYS	NewModifyProp,IntuitionBase
	POP	D2-D5/A2
	RTS

UpdateWindow:
	LEA	FileBox,A0
	BSR	ClearBox	
UpdWinNoClear:
	PUSH	D2/A3
	MOVE.L	CurLevel,A3		;current directory level ptr
	MOVE.L	dl_CurFib(A3),A3	;first file entry in window
	ZAP	D2			;line counter
1$:	INCW	D2
	IFGTIW	D2,#MaxFileGads,2$	;filled all lines
	MOVE.L	A3,A0			;display this entry
	IFZL	A0,2$			;no more files
	MOVE.L	(A0),A3			;set up link to next entry
	MOVE.L	D2,D0			;on this line of file window
	BSR	DispFileInfo		;display one line of file info
	BRA.S	1$
2$:	POP	D2/A3
	RTS

* Builds and displays one line of directory.  Dir line number (1-20) in D0.
* Pointer to CurFib in A0.

DispFileInfo:
	PUSH	D2-D5/A2-A5
	MOVE.L	D0,D4			;save calling params
	MOVE.L	A0,A4
	MOVE.L	WINDOW,A5
	MOVE.L	wd_RPort(A5),A5
	MOVEQ	#JAM2,D0
	MOVE.L	A5,A1
	CALLSYS	SetDrMd,GraphicsBase
	MOVE.L	FontPtr,A0
	MOVE.L	A5,A1
	CALLSYS	SetFont			;force Topaz 80
;;	MOVEQ	#ORANGE,D2		;dir colors
;;	MOVEQ	#BLACK,D3
;;	BTST	#7,df_Flags(A4)		;dir?
;;	BNE.S	1$			;yes
	MOVEQ	#ORANGE,D3
	MOVEQ	#BLACK,D2
;;	EXG	D2,D3			;sel file colors
	BTST	#6,df_Flags(A4)		;item selected?
	BNE.S	1$			;yes
	MOVEQ	#WHITE,D2		;else mark it unselected
	MOVEQ	#BLACK,D3
1$:	MOVE.L	D2,D0
	MOVE.L	A5,A1
	CALLSYS	SetAPen
	MOVE.L	D3,D0
	MOVE.L	A5,A1
	CALLSYS	SetBPen
	DECW	D4			;change to 0 origin
	MULU	#FontHt,D4		;figure out which line we are on
	ADD.W	#Files_TE+8,D4		;font offset + border
	MOVE.W	D4,D1			;y pos
	MOVE.W	#Files_LE+4,D0		;x pos
	MOVE.L	A5,A1
	CALLSYS	Move			;set x,y pos to proper line
	LEA	df_Name(A4),A0		;name of dir or file
	BSR	MovePad			;move to TMP. and pad to 30 chars
	MOVE.L	A1,A3			;save ptr to end of name string
	BTST	#7,df_Flags(A4)		;file?
	BEQ.S	2$			;yes
	APPEND.	SubDir.,TMP.		;no...report as subdirectory
	BRA.S	3$
2$:	STR.	L,df_Size(A4),TMP2.,#32,#8 ;convert size to ASCII, blank fill
	ACHAR.	#32,TMP2.		;add a trailing space

* This kludge puts highest byte of size, if non-blank, over last char of name.
* Only clobbers last name char if file size >9,999,999.

	LEA	TMP2.,A0
	MOVE.B	(A0)+,D0		;get 1st char of size
	IFEQIB	D0,#SPACE,4$		;it is space...ignore it
	MOVE.B	D0,-(A3)		;move 1st size char over last name char
4$:	APPEND.	A0,TMP.
	ZAP	D0
	MOVE.W	df_Date(A4),D0		;GET DAYS SINCE JAN 1, 1978
	BSR	ConvertDosDate		;add the date in dd-mmm-yy format
	ZAP	D0
	MOVE.W	df_Time(A4),D0		;minutes of day
	BSR	ConvertDosTime		;add the time in HH:MM
3$:	LEN.	TMP.			;load A0, set D0 to length
	MOVE.L	A5,A1
	CALLSYS	Text			;send size/date/time to that line
	MOVEQ	#BLACK,D0		;restore background color
	MOVE.L	A5,A1
	CALLSYS	SetBPen
	POP	D2-D5/A2-A5
	RTS

* Calculates day, month, and year from AmigaDOS date stamp in D0.  
* Stores in result i TMP.  FROM EDN OCT 17, 1985 P. 168

ConvertDosDate:
	PUSH	D2
	ADDI.L	#28431,D0		;MAGIC NUMBER CONVERTS TO MARCH 1, 1900
	MULU	#4,D0
	SUBQ.L	#1,D0			;K2=4*K-1
	MOVE.L	D0,D2
	DIVU	#1461,D2		;Y=INT(K2/1461)
	MOVE.L	D2,D1
	SWAP	D1			;D=INT (K2 MOD 1461)
	ADDQ.W	#4,D1
	LSR.W	#2,D1			;D=INT ((D+4)/4)
	MULU	#5,D1
	SUBQ.L	#3,D1
	DIVU	#153,D1	 		;M=INT ((5*D-3)/153)
	MOVE.W	D1,D0
	SWAP	D1
	EXT.L	D1			;D=INT ((5*D-3) MOD 153)
	ADDQ.L	#5,D1
	DIVU	#5,D1			;D=INT ((D+5)/5)
	CMPI.W	#10,D0
	BLT.S	1$
	SUBI.W	#9,D0			;M=M-9
	ADDQ.W	#1,D2			;Y=Y+1
	BRA.S	2$
1$:	ADDQ.W	#3,D0
; month in D0, day in D1, year in D2 at this point
2$:	PUSH	D0			;save month
	STR.	W,D1,TMP2.,#32,#2	;convert day
	APPEND.	TMP2.,TMP.
	ACHAR.	#'-',TMP.
	POP	D0
	DECW	D0
	LSL.W	#2,D0			;change into long index
;;	LEA	MONTHS(D0.W),A0		;index month names
	LEA	MONTHS,A0
	ADDA.W	D0,A0
	APPEND.	A0,TMP.
	ACHAR.	#'-',TMP.
	STR.	W,D2,TMP2.,#'0',#2
	APPEND.	TMP2.,TMP.
	ACHAR.	#32,TMP.		;add a trailing space
	POP	D2
	RTS

* Appends AmigaDOS time in D0in minutes since midnight to HH:MM.  
* Appends to TMP.

ConvertDosTime:
	PUSH	D2
	MOVE.L	D0,D2
	DIVU	#60,D2			;hours in low word, mins in high word
	STR.	W,D2,TMP2.,#32,#2	;convert hours
	APPEND.	TMP2.,TMP.
	ACHAR.	#':',TMP.		;format HH:MM
	SWAP	D2
	STR.	W,D2,TMP2.,#'0',#2	;minutes
	APPEND.	TMP2.,TMP.
	POP	D2
	RTS

* Moves file or dir name to TMP. and pads to 30 chars.  Ptr to name stg in A0.

MovePad:
	MOVEQ	#SPACE,D2
	MOVEQ	#29,D1
	LEA	TMP.,A1
1$:	MOVE.B	(A0)+,D0
	BEQ.S	2$
	MOVE.B	D0,(A1)+
	DBF	D1,1$			;MOVE UP TO 30 CHARS
	BRA.S	9$
2$:	MOVE.B	D2,(A1)+
	DBF	D1,2$
9$:	CLR.B	(A1)			;terminate properly
	RTS

* Scrolls the entire window one row up.

ScrollUp:
	MOVEQ	#FontHt,D1		;delta y
	BRA.S	ScrollCom
* Scrolls the entire window one row down.

ScrollDown:
	MOVEQ	#FontHt,D1		;delta y
	NEG.L	D1			;move away from 0,0
ScrollCom:
	MOVE.L	#Files_TE+1,D3		;miny
	MOVE.L	#Files_TE+Files_HT-4,D5	;maxy
	ZAP	D0			;delta x	
	MOVE.L	#Files_LE+2,D2		;minx
	MOVE.L	#Files_LE+Files_WD-3,D4	;maxx
	MOVE.L	WINDOW,A0
	MOVE.L	wd_RPort(A0),A1
	CALLSYS	ScrollRaster,GraphicsBase
	RTS

* Builds the catalog of files from the selected device:path.

BuildDir:
;	DispMsg	#FCnt_LE,#FCnt_TE,FilCnt.,WHITE
;	LEN.	FilCnt.			;set D0 to length
;	ASL	#3,D0			;multiply by 8
;	ADD.W	#FCnt_LE,D0		;offset to correct position
;	MOVE.W	D0,FilCntPos		;display count here
	DispCent #320,#FCnt_TE,FilCnt.,WHITE
	LEA	DT_TXT,A1
	MOVE.W	it_LeftEdge(A1),D0
	SUB.W	#4*8,D0			;back up to where numbers go
	MOVE.W	D0,FilCntPos 		;here is where count goes
	CLR.W	TotFiles
	CLR.B	AbortFlg
	MOVE.L	#BaseDirBlock,CurDBBase
	BSR	AllocateDirBlock	;allocate the first dir block
	ERROR	BuildNoMem		;oops...no memory
	LEA	RootLevel,A2
	MOVE.L	A2,CurLevel
	MOVE.L	A2,A1
	BSR	ZapLevelBlk
	BSR	InitPath		;initialize path
	BSR	PathLock
;;	BSR	CurrentPath		;get a lock on initial path
;;	MOVE.L	D0,OriginalDir

* read files and subdirs at a given level

ScanLevel:
	MOVE.L	CurLevel,A2
	MOVE.L	dl_DirLock(A2),D1
	MOVE.L	#FILE_INFO,D2
	CALLSYS	Examine			;take a look at directory
	TST.L	D0
	BEQ	DiskBadUn		;examine failed...cant find the path
	TST.L	FIB_TYPE		;make sure it is a dir
	BMI	DiskBadUn		;no...it is a file
	BSR	ScanDir			;else build entries for this dir
	ERROR	BuildErr		;scan error or op abort
	MOVE.L	CurLevel,A2
	LEA	dl_ChildPtr(A2),A1
	MOVE.L	A1,dl_CurFib(A2) 	;start at beg of list

NextEntry:
	MOVE.L	dl_CurFib(A2),A1	;work the list
	MOVE.L	df_Next(A1),A1		;advance to next entry in list
	IFZL	A1,PrevLevel,L		;end of list...back up a level
	BTST	#7,df_Flags(A1)		;dir?
	BEQ	PrevLevel		;no...a file...stop scan at this level
	MOVE.L	A1,dl_CurFib(A2)	;save ptr to dir entry
	LEA	df_Name(A1),A3		;name of dir
	IFGEIL	CurDBCnt,#dl_SIZEOF,1$	;still room in buffer
	BSR	AllocateDirBlock	;else get another
	ERROR	BuildErr
1$:	MOVE.L	CurDBPtr,A1
	MOVEQ	#dl_SIZEOF,D0		;account for size of level block
	ADD.L	D0,CurDBPtr
	SUB.L	D0,CurDBCnt
	BSR	ZapLevelBlk
	MOVE.L	dl_CurFib(A2),A0	;set ptr to sublevel
	MOVE.L	A1,df_SubLevel(A0)	;point to subdir level block
	MOVE.L	CurLevel,dl_ParLevel(A1);linkage to parent level
	MOVE.L	A1,CurLevel		;this will soon be the current level
;;	MOVE.L	dl_DirLock(A2),D1	;unlock the old current dir
;;	BEQ.S	2$
;;	CALLSYS	UnLock,DosBase
2$:;;	CLR.L	dl_DirLock(A2)
	MOVE.L	CurLevel,A2		;ptr to new current level
	LEA	dl_ChildPtr(A2),A1
	MOVE.L	A1,dl_CurFib(A2)
	MOVE.L	A3,A0
	BSR	AppendPath		;add new subdir to existing path
	BSR	PathLock
;;	BSR	CurrentPath		;make the new subdir current
	ERROR	NextEntry		;cant find dir...skip it
	BRA	ScanLevel		;now check out this level

PrevLevel:
	MOVE.L	dl_DirLock(A2),D1	;else unlock the current level
	BEQ.S	1$			;no lock
	CALLSYS	UnLock,DosBase
1$:	CLR.L	dl_DirLock(A2)
	MOVE.L	dl_ParLevel(A2),A2
	IFZL	A2,9$,L			;completed root...stop now
	MOVE.L	A2,CurLevel		;backup one level to parent
	BSR	TruncatePath		;subtract current subdir from path
;;	BSR	PathLock
;;	BSR	CurrentPath		;make parent dir current
	BRA	NextEntry		;continue with next entry	

9$:;;	BSR	ChgOrigDir		;;;
	CLC
	RTS

DiskBadUn:
	MOVE.L	dl_DirLock(A2),D1
	BEQ.S	DiskBad
	CALLSYS	UnLock,DosBase		;"please release me, let me go..."
DiskBad:
	DispErr	DiskBad.		;disk corrupted...
	BRA	PrevLevel		;and attempt to continue at prev level

* Build entries of both types for current subdir.

ScanDir:
	BSR	ProcessMsg		;check for op action
	IFZB	AbortFlg,7$		;don't stop
	STC
	RTS

7$:	MOVE.L	CurLevel,A2
	MOVE.L	dl_DirLock(A2),D1
	MOVE.L	#FILE_INFO,D2
	CALLSYS	ExNext,DosBase		;get next entry
	IFNZL	D0,8$			;there is one...keep going
	RTS

* Got an entry...build Fib block

8$:	IFEQIW	FIB_NAME,#$2A00,7$	;ignore illegal name: *
	IFGEIL	CurDBCnt,#df_SIZEOF,1$	;have enuf room for this entry
	BSR	AllocateDirBlock	;else get another block
	RTSERR				;oops...can't get any
1$:	MOVE.L	CurDBPtr,A1		;point to new block
	MOVE.L	A1,dl_CurFib(A2)	;this fib is now current
	CLR.L	(A1)+			;no linkage yet
	MOVE.L	FIB_SIZE,(A1)+		;copy file size
	MOVE.L	FIB_DATE,D0
	MOVE.W	D0,(A1)+
	MOVE.L	FIB_TIME,D0
	MOVE.W	D0,(A1)+
	MOVE.L	FIB_TICK,D0
	MOVE.W	D0,(A1)+
	ZAP	D0
	MOVE.W	D0,(A1)+		;leave room for df_FilCnt
	MOVE.L	FIB_PROT,D0
	MOVE.B	D0,(A1)+
	MOVEQ	#$40,D0			;everything is selected
	TST.L	FIB_TYPE		;a file?
	BMI.S	5$			;yes
	BSET	#7,D0			;else show it as dir
5$:	MOVE.B	D0,(A1)+		;store flags
	LEA	FIB_NAME,A0
	MOVEQ	#FileNameLen-1,D0	;limit for file names
	BSR	LimitMove
;	MOVE.	A0,A1			;copy file name into fib
	LEA	FIB_COMM,A0
	MOVEQ	#FileNoteLen-1,D0	;limit for file notes
	BSR	LimitMove
;	MOVE.	A0,A1			;add possible comments after
	MOVE.L	A1,D0
	INCL	D0
	ANDI.L	#$FFFFFFFE,D0
	MOVE.L	D0,D1
	SUB.L	CurDBPtr,D1		;get size of this fib
	MOVE.L	D0,CurDBPtr		;allign ptr properly
	SUB.L	D1,CurDBCnt		;add to size of block
	IFNZL	dl_ChildPtr(A2),2$	;child already set
	MOVE.L	dl_CurFib(A2),dl_ChildPtr(A2)	;else set it now
	BRA.S	3$			;no sort needed...1st entry at level
2$:	BSR	SortInsert		;else insert into list
3$:	TST.L	FIB_TYPE		;dir?
	BPL	ScanDir			;yes...go on
	INCW	TotFiles		;else count the file
	BSR	DispNbrFiles		;update the file count
	BRA	ScanDir			;loop

BuildErr:
	MOVE.L	CurLevel,A4
	MOVE.L	dl_DirLock(A4),D1
	BEQ.S	1$
	CALLSYS	UnLock,DosBase
1$:;;	BSR	ChgOrigDir
CloseDirectory:
BuildNoMem:
	BSR	FreeDirBlocks		;release memory
	STC				;error return
	RTS

* Moves null-terminated string A0 into buffer A1 up to number of chars in D0.
* Inserts null if limit reached.

LimitMove:
	MOVE.B	(A0)+,(A1)+
	DBEQ	D0,LimitMove
	BEQ.S	9$			;string ended ok
	CLR.B	(A1)+			;put in a null if limit reached
9$:	RTS


DispNbrFiles:
	STR.	W,TotFiles,TMP2.,#32,#5
	DispMsg	FilCntPos,#FCnt_TE,TMP2.,ORANGE,BLUE
	RTS

* Reads directory from first disk of set...
* Returns CY=1 if unable to load directory.

ReadDir:
	MOVE.L	#BaseDirBlock,CurDBBase
	BSR	AllocateDirBlock	;allocate the first dir block
	ERROR	ReadDirErr		;oops...no memory
	CLR.L	FillCnt			;nothing in buffer
	MOVE.W	#1,ActiveDev		;force switch to DF0:
	LEA	Dev0Params,A0
	BSR	GetWord			;get number of root level entries
	ERROR	ReadDirErr
	LEA	RootLevel,A4
	MOVE.L	A4,CurLevel		;start at top level
	MOVE.L	A4,A1
	BSR	ZapLevelBlk
	MOVE.W	D0,dl_FilCnt(A4)	;root level file count
	CLR.W	TotFiles
	CLR.L	TotBytes

RNxtEntry:
	IFZW	dl_FilCnt(A4),RPrvLevel,L ;no more at this level...back up
	BSR	ReadEntry		;read next entry from disk
	ERROR	ReadDirErr
	IFNZL	dl_ChildPtr(A4),1$	;this level already active
	MOVE.L	A0,dl_ChildPtr(A4)	;else init this level
	BRA.S	2$
1$:	MOVE.L	dl_CurFib(A4),A1	;else get ptr to previous entry
	MOVE.L	A0,df_Next(A1)		;fix up linkage to previous
2$:	MOVE.L	A0,dl_CurFib(A4)	;and make this one current
	DECW	dl_FilCnt(A4)		;one less to go
	BTST	#7,df_Flags(A0)		;dir?
	BNE.S	RNxtLevel		;yes...down a level
	MOVE.L	df_Size(A0),D0
	ADD.L	D0,TotBytes		;else add size of this file
	INCW	TotFiles
	BRA.S	RNxtEntry
	
RNxtLevel:
	IFGEIL	CurDBCnt,#dl_SIZEOF,1$	;still room
	BSR	AllocateDirBlock
	ERROR	ReadDirErr
1$:	MOVE.L	CurDBPtr,A1
	BSR	ZapLevelBlk
	MOVE.L	dl_CurFib(A4),A0	;ptr parent of new level
	MOVE.L	A1,df_SubLevel(A0)	;set up linkage
	MOVE.L	A4,dl_ParLevel(A1)	;ptr to parent level block
	MOVE.W	df_FilCnt(A0),dl_FilCnt(A1) ;number of files at this level
	MOVE.L	A1,A4
	MOVE.L	A4,CurLevel		;new level now active
	MOVEQ	#dl_SIZEOF,D0
	ADD.L	D0,CurDBPtr
	SUB.L	D0,CurDBCnt
	BRA	RNxtEntry		;now process this level

RPrvLevel:
	MOVE.L	dl_ParLevel(A4),A4
	MOVE.L	A4,CurLevel
	BEQ.S	9$			;finished with root level
	MOVE.L	dl_CurFib(A4),A0
	BRA	RNxtEntry
9$:	BSR	MotorOff		;turn off the drive
	RTS

ReadDirErr:
	BSR	MotorOff
	STC
	RTS

* Reads next entry from disk and builds FIB entry, which is returned in A0.
* Current level in A4

ReadEntry:
	PUSH	D2
	IFGEIL	CurDBCnt,#df_SIZEOF,1$	;have enuf room
	BSR	AllocateDirBlock	;else get another block
	ERROR	9$
1$:	MOVE.L	CurDBPtr,A2		;point to new block
	MOVE.L	A2,A3
	CLR.L	(A2)+			;no linkage yet
	BSR	GetLong			;get size in bytes
	ERROR	9$
	MOVE.L	D0,(A2)+
	BSR	GetWord			;date
	ERROR	9$
	MOVE.W	D0,(A2)+
	BSR	GetWord			;time
	ERROR	9$
	MOVE.W	D0,(A2)+
	BSR	GetWord			;ticks
	ERROR	9$
	MOVE.W	D0,(A2)+
	BSR	GetWord			;file count
	ERROR	9$
	MOVE.W	D0,(A2)+
	BSR	GetChar			;prot bits
	ERROR	9$
	MOVE.B	D0,(A2)+
	BSR	GetChar			;flags
	ERROR	9$
	MOVE.B	D0,(A2)+
	MOVEQ	#FileNameLen-1,D2
2$:	BSR	GetChar			;get char of name
	ERROR	9$
	MOVE.B	D0,(A2)+		;store it
	BEQ.S	4$			;end of name
	DBF	D2,2$			;loop till end
	CLR.B	(A2)+			;null-terminate it
21$:	BSR	GetChar			;find the null
	ERROR	9$
	IFNZB	D0,21$
4$:	MOVEQ	#FileNoteLen-1,D2
3$:	BSR	GetChar			;get char of comments
	ERROR	9$
	MOVE.B	D0,(A2)+
	BEQ.S	5$			;loop for all of comments
	DBF	D2,3$			;loop till end
	CLR.B	(A2)+			;null-terminate it
31$:	BSR	GetChar			;find the null
	ERROR	9$
	IFNZB	D0,31$
5$:	MOVE.L	A3,A0			;restore ptr to this one
	MOVE.L	A2,D0
	INCL	D0
	ANDI.L	#$FFFFFFFE,D0
	MOVE.L	D0,D1
	SUB.L	A0,D1			;get size of this FIB
	MOVE.L	D0,CurDBPtr		;align ptr properly
	SUB.L	D1,CurDBCnt
9$:	POP	D2
	RTS

* Inserts new FIB into chain at proper place, depending upon name.  Dirs
* sort ahead of files.  A2 points to level block.  New FIB in dl_CurFib.

SortInsert:
	LEA	dl_ChildPtr(A2),A1	;point to "head" of chain
	MOVE.L	dl_CurFib(A2),A0	;points to new FIB
	BTST	#7,df_Flags(A0)		;is new FIB a dir?
	BEQ.S	2$			;no...a file
1$:	MOVE.L	(A1),A3			;got a dir...get (first) next entry
	IFZL	A3,5$			;end of chain...add new FIB to end
	BTST	#7,df_Flags(A3)		;is it a dir?
	BEQ.S	4$			;no...dir goes in front
	BSR	CompareNames		;else check sort order
	BCS.S	4$			;put new FIB in front
	LEA	df_Next(A3),A1		;else advance to next entry
	BRA.S	1$
2$:	MOVE.L	(A1),A3			;a file...get (first) next entry
	IFZL	A3,5$			;end of chain...add new FIB to end
	BTST	#7,df_Flags(A3)		;is it a dir?
	BNE.S	3$			;yes...files go after dirs
	BSR	CompareNames		;else check sort order
	BCS.S	4$			;put new FIB here
3$:	LEA	df_Next(A3),A1		;advance to next entry
	BRA.S	2$
4$:	MOVE.L	A3,df_Next(A0)		;new one points to old one
5$:	MOVE.L	A0,(A1)			;and correct previous linkage
	RTS

* Compares names of new block (FIB_NAME) to current block (A3).  Returns
* CY=1 if new item sorts ahad of current item.  CY=0 otherwise.

CompareNames:
	PUSH	A0/A3
	LEA	FIB_NAME,A0		;get pointers to name strings
	LEA	df_Name(A3),A3	
	MOVEQ	#$5F,D7			;lc mask
1$:	MOVE.B	(A0)+,D0		;get a byte of new name
	BEQ.S	9$			;new item ended...sort ahead
	AND.B	D7,D0			;make uppercase
	MOVE.B	(A3)+,D1		;get a byte of current name
	AND.B	D7,D1			;make upper case
	IFEQB	D1,D0,1$		;loop if equal
	POP	A0/A3			;else return with CY set properly
	RTS
9$:	POP	A0/A3
	STC				;short return
	RTS

* Allocates memory for directory blocks.

AllocateDirBlock:
	MOVE.L	#DirBlockSize,D0
	ZAP	D1			;any kind of memory
	PUSH	A6
	CALLSYS	AllocMem,SysBase
	POP	A6
	MOVE.L	CurDBBase,A0		;ptr to base of current dir block
	MOVE.L	D0,(A0)			;set up link to new dir block
	BEQ.S	9$			;oops...no memory...bad awful
	INCB	DirBlkCnt		;one more
	MOVE.L	D0,A0
	MOVE.L	A0,CurDBBase
	CLR.L	(A0)+			;no fwd link yet...
	MOVE.L	A0,CurDBPtr		;load dir block here
	MOVE.L	#DirBlockSize-4,CurDBCnt
	RTS
9$:	DispErr	NoMemory
	STC
	RTS

FreeDirBlocks:
	CLR.W	TotFiles		;no files to process
	MOVE.L	BaseDirBlock,A2		;free the chain of dir blocks
1$:	IFZL	A2,2$			;nothing left in chain
	MOVE.L	A2,A1
	MOVE.L	(A2),A2			;pick up fwd link BEFORE releasing...
	MOVE.L	#DirBlockSize,D0
	CALLSYS	FreeMem,SysBase
	DECB	DirBlkCnt
	BRA.S	1$
2$:	MOVE.L	A2,BaseDirBlock		;show chain released
	IFZB	DirBlkCnt,9$
	DispErr	DirBlk.			;dir blocks not all released
9$:	LEA	RootLevel,A1
	BSR.S	ZapLevelBlk
	RTS

ZapLevelBlk:
	PUSH	A1
	CLR.L	(A1)+
	CLR.L	(A1)+
	CLR.L	(A1)+
	CLR.L	(A1)+
	CLR.L	(A1)+
	CLR.L	(A1)+
	POP	A1
	RTS

* Resets dir list

InitList:
	LEA	RootLevel,A2		;prepare to process the list
	LEA	dl_ChildPtr(A2),A1
	MOVE.L	A1,dl_CurFib(A2)	;tricky...this kicks off root level
	SETF	LevelFlg		;so as not to add size on first
	MOVE.L	A2,CurLevel
	BSR	InitPath		;initialize path
;;	BSR	CurrentPath		;make "root" directory current
;;	MOVE.L	D0,OriginalDir		;save original dir
	MOVE.L	CatSize,D0		;total catalog size
	ADDQ.L	#2,D0			;account for root file count
	MOVE.L	D0,AbsolutePos		;absolute byte position
	RTS

* Returns ptr to FIB of the next selected file to backup or restore in A0, 
* or CY=1 if no more entries to process.

NextFile:
	MOVE.L	CurLevel,A2
	MOVE.L	dl_CurFib(A2),A1
1$:	IFNZB	LevelFlg,16$		;level block...dont add size
	BTST	#7,df_Flags(A1)		;dir?
	BNE.S	16$			;yes...also dont add
	MOVE.L	df_Size(A1),D0
	ADD.L	D0,AbsolutePos		;add size of current file
16$:	CLR.B	LevelFlg
	MOVE.L	df_Next(A1),A1		;get ptr to next FIB at this level
	IFNZL	A1,2$			;there is another entry at this level
;;	MOVE.L	dl_DirLock(A2),D1	;else unlock the current directory
;;	BEQ.S	6$			;oops...no lock
;;	CALLSYS	UnLock,DosBase
;;	CLR.L	dl_DirLock(A2)
;;6$:
	MOVE.L	dl_ParLevel(A2),A2
	MOVE.L	A2,CurLevel		;backup one level to parent dir
	IFZL	A2,9$,L			;completed root...stop now
	BSR	TruncatePath		;delete current dir from path

* This code updates the original date/time stamp on a directory entry AFTER 
* restoring file(s) to it.  ADOS updates the directory date/time stamp to the
* current date/time with every write to the directory, so if we want the
* directory to have its ORIGINAL date/time stamp, we must update it AFTER
* we have restored all files to the directory.

	IFZB	Mode,18$		;backup...dont update dir date/time
	MOVE.L	dl_CurFib(A2),CurFib
	BSR	RestDateProt		;restore dir date/time after writing
18$:;;	BSR	CurrentPath		;lock parent dir and make current dir
	MOVE.L	dl_CurFib(A2),A1	;ptr to last FIB at this level
	BRA.S	16$			;proceed at parent level, dont add size

* Come here to check out the new current entry at this level.

2$:	MOVE.L	A1,dl_CurFib(A2)	;save ptr to this entry
	MOVE.L	A1,CurFib

* I not sure why this next step is necessary, but it probably handles the
* case where a bad disk is detected.  In this case we may have to restart 
* part of the way through a different directory, which may not be locked.

;;	IFNZL	dl_DirLock(A2),11$	;already have a lock on this directory?
;;	BSR	CurrentPath		;no...lock dir and make it current
	MOVE.L	dl_CurFib(A2),A1
;;11$:
	BTST	#7,df_Flags(A1)		;is this entry a subdirectory?
	BEQ	3$			;no...a file
	LEA	df_Name(A1),A3		;ptr to name of subdirectory
	BTST	#6,df_Flags(A1)		;subdir selected?
;;;;	BRA.S	19$			;this patch restores empty subdirs 
	BNE.S	19$			;yes...
	IFNZB	EmptyFlg,19$		;restoring empty subdir
	BSR	ScanUnselDir		;else scan the unselected dir for position
	BRA	NextFile		;and continue at this level

19$:	IFZB	Mode,15$		;backup, subdir must already exist
	IFZB	RSubFlg,15$		;restore, but not restoring full path
	IFNZB	TestFlg,15$		;test mode, don't create Subdirs

* Come here on restore only, when we reach a lower subdir level and we are 
* restoring the full subdirectory structure.  Must first find out if subdir
* already exists.  If not, we must create it.

20$:;;	MOVE.L	A3,D1
	MOVE.	Path.,TMP.
	MOVE.L	A3,A0
	APPEND.	A0,TMP.
	MOVE.L	#TMP.,D1
	MOVEQ	#ACCESS_READ,D2		;try to find the subdir
	CALLSYS	Lock,DosBase		;does subdir exist?
	MOVE.L	D0,D1
	BNE.S	5$			;yes...no need to create it
;;	MOVE.L	A3,D1
	MOVE.L	#TMP.,D1
	CALLSYS	CreateDir		;else try to create missing subdir
	MOVE.L	D0,D1			;successful?
	BEQ.S	7$			;no...bail out
5$:	CALLSYS	UnLock			;unlock the subdir

* We get here when we must switch to a lower subdirectory level.  On backup
* the subdirectory is assumed to exist.  On restore either it already existed
* or we just created it.

15$:	MOVE.L	CurLevel,A2
;;	MOVE.L	dl_DirLock(A2),D1	;first unlock the current directory
;;	BEQ.S	17$			;oops...no lock
;;	CALLSYS	UnLock,DosBase
;;	CLR.L	dl_DirLock(A2)
;;17$:
	MOVE.L	dl_CurFib(A2),A1
	MOVE.L	df_SubLevel(A1),A2	;switch to lower level
	MOVE.L	A2,CurLevel		;this is now the current level
	LEA	dl_ChildPtr(A2),A1
	MOVE.L	A1,dl_CurFib(A2)
	SETF	LevelFlg		;dont add size
	MOVE.L	A3,A0
	BSR	AppendPath		;add new subdir name to path
;;	BSR	CurrentPath		;lock subdir and make it current dir
;;	ERROR	6$			;on error, back up 1 level
	BRA	NextFile		;now scan this new level

* Come here if next item at current level is a file.

3$:	BTST	#6,df_Flags(A1)		;file selected?
	BEQ	1$			;no...forget it...try next entry
	MOVE.L	A1,A0			;yes, return ptr to this FIB
	RTS

7$:	DispErr	CantSub.		;cant create subdirectory
	SETF	AbortFlg
9$:;;	BSR	ChgOrigDir
	STC
	RTS

* Scans an unselected dir, adding its files and subdirs to AbsolutePos.

ScanUnselDir:
	PUSH	A5
	MOVE.L	A2,A5			;save level
2$:	MOVE.L	df_SubLevel(A1),A2	;process this level
	MOVE.L	dl_ChildPtr(A2),A1
1$:	IFZL	A1,3$			;end of level
	MOVE.L	A1,dl_CurFib(A2)
	BTST	#7,df_Flags(A1)		;dir?
	BNE.S	2$			;yes...down another level
	MOVE.L	df_Size(A1),D0
;;	BTST	#0,D0			;odd?
;;	BEQ.S	17$
;;	INCL	D0			;yes...add 1
17$:	ADD.L	D0,AbsolutePos
	BRA.S	4$
3$:	MOVE.L	dl_ParLevel(A2),A2	;back another level
	MOVE.L	dl_CurFib(A2),A1
	IFEQL	A2,A5,9$		;thats it
4$:	MOVE.L	df_Next(A1),A1
	BRA.S	1$
9$:	POP	A5
	RTS

* Locks the directory indicated by Path., and stores the lock in the
* level entry pointed to by A2.  Lock returned in D0.

PathLock:
	MOVE.L	#Path.,D1
	MOVEQ	#ACCESS_READ,D2		;ACCESS-READ
	CALLSYS	Lock,DosBase		;lock the current path
	MOVE.L	D0,dl_DirLock(A2)	;got it?
	RTS

* Makes the current path the current dir.  Returns old dir, if you care.
* Assumes CurLevel in A2.

;;CurrentPath:
;;	BSR.S	PathLock		;get a lock on current path
;;	MOVE.L	D0,D1			;;;
;;	BEQ.S	8$			;no lock...
;;	CALLSYS	CurrentDir		;make it current;;;
;;	IFNZL	OriginalDir,9$
;;	MOVE.L	D0,OriginalDir
;;9$:	CLC
;;	RTS
;;8$:	STC
;;	RTS

* Restores original current directory, if any

;;ChgOrigDir:
;;	MOVE.L	OriginalDir,D1		;make sure we are back where we belong
;;	BEQ.S	1$			;nothing
;;	CALLSYS	CurrentDir,DosBase
;;	CLR.L	OriginalDir
;;1$:	RTS

* Update display of selected files and required disks.

UpdateNumbers:
	BSR	RecalcNumbers		;scan the catalog and recalc
	STR.	W,SelFiles,TMP2.,#32,#5	;selected files
	DispMsg	#SFil_LE,#SFil_TE,TMP2.,ORANGE,BLUE
	STR.	W,TotFiles,TMP2.,#32,#5	;total files
	DispMsg	FilTotPos,#SFil_TE,TMP2.,ORANGE,BLUE
;	STR.	L,TotBytes,TMP2.,#32,#9	;total bytes
	STR.	L,SelBytes,TMP2.,#32,#9	;selected bytes
	DispMsg	BytesPos,#SByt_TE,TMP2.,ORANGE,BLUE
	IFNZB	Mode,1$			;no selected vols on restore
;	STR.	W,TotVols,TMP2.,#32,#2	;total disks
	STR.	W,SelVols,TMP2.,#32,#3	;selected disks
	DispMsg	DisksPos,#SDsk_TE,TMP2.,ORANGE,BLUE
1$:	RTS

* Scans the catalog to recalculate selected and total files, 
* selected and total bytes, and selected and total volumes.

RecalcNumbers:
	PUSH	D0-D5/A0-A3
	ZAP	D0			;total files
	ZAP	D1			;selected files
	ZAP	D2			;selected bytes
	ZAP	D3			;catalog size
	ZAP	D4			;total bytes
	MOVE.W	D0,TotVols
	MOVE.W	D0,SelVols
	LEA	RootLevel,A2		;start at highest level
	LEA	FIB_COMM,A1		;start A1 at a safe place
1$:	MOVE.L	dl_ChildPtr(A2),A0	;start scan at this level
4$:	IFZL	A0,5$			;end of this level
	BTST	#7,df_Flags(A0)		;dir?
	BEQ.S	2$			;no, count it
	MOVE.L	A0,dl_GNFib(A2)		;save our pos at this level
	MOVE.L	A1,dl_DirLock(A2)	;save ptr to level's FIB
	MOVE.L	A0,A1			;ptr to this FIB, for set/clr sel flg
	BCLR	#6,df_Flags(A1)		;start with dir unselected
	MOVE.L	df_SubLevel(A0),A2	;and get ptr to next level
	BRA.S	1$

2$:	INCL	D0			;total files
	BTST	#6,df_Flags(A0)		;selected?
	BEQ.S	12$			;no...don't count
	INCL	D1			;selected files
	BSET	#6,df_Flags(A1)		;and show dir 'selected'
	ADD.L	df_Size(A0),D2		;and count its bytes
12$:	ADD.L	df_Size(A0),D4
3$:	ADDI.L	#CatEntSize,D3		;size plus name plus null
	LEA	df_Name(A0),A3		;check length of name
7$:	INCL	D3			;at least 1 char...a null
	TST.B	(A3)+			;what was it?
	BNE.S	7$			;wasn't null...loop
8$:	INCL	D3			;comment has at least a null
	TST.B	(A3)+
	BNE.S	8$			;wasn't null...loop
	MOVE.L	df_Next(A0),A0		;get link to next
	BRA.S	4$			;loop

5$:	MOVE.L	dl_ParLevel(A2),A2	;back up to previous level
	IFZL	A2,9$			;done
	MOVE.B	df_Flags(A1),D5		;save flags of this level
	MOVE.L	dl_DirLock(A2),A1	;restore ptr to flags
	AND.B	#$40,D5			;copy select bit only
	OR.B	df_Flags(A1),D5
	MOVE.B	D5,df_Flags(A1)		;copy the select bit to higher level
	MOVE.L	dl_GNFib(A2),A0		;restore previous position
	BRA.S	3$
9$:	MOVE.W	D0,TotFiles
	MOVE.L	D3,CatSize 
	ADD.L	D3,D3			;two catalogs
	MOVE.L	D2,SelBytes
	MOVE.L	D4,TotBytes
	MOVE.W	D1,SelFiles
	BEQ.S	6$			;no files to process, SelVols=0
	MOVE.L	D2,D0			;calc selected vols
;	IFZB	Mode,11$		;backup
;	MOVE.L	D4,D0			;for restore, selvols=totvols
;11$:
	ADD.L	D3,D0			;add size of 2 catalogs
	MOVE.L	VolumeSize,D1
	BSR.S	LongDivide
	MOVE.W	D0,SelVols
6$:	MOVE.L	D4,D0			;calc total vols
	ADD.L	D3,D0			;add size of 2 catalogs
	MOVE.L	VolumeSize,D1
	BSR.S	LongDivide
	MOVE.W	D0,TotVols
	POP	D0-D5/A0-A3
	RTS

* Divides 32-bit value in D0 by 32-bit value in D1.  Returns quotient in D0.
* This is slow and very crude, but works well for small numbers (this case).

LongDivide:
	PUSH	D2
	ZAP	D2
	IFZL	D1,9$			;zero divide is illegal
1$:	INCL	D2
	SUB.L	D1,D0			;crude but effective 32 bit divide
	BHI.S	1$			;this could be wrong branch...
9$:	MOVE.L	D2,D0
	POP	D2
	RTS

* Include/exclude gadget routines.

SAll:	BSR.S	IncAll
	BRA	SDExit


IncAll:	BSR	InitScan		;prepare to scan the list
1$:	BSR	GetNextFile		;scan the list
	RTSERR
	BSET	#6,df_Flags(A0)		;else mark as "included"
	BRA.S	1$

DAll:	BSR.S	ExcAll
	BRA.S	SDExit

ExcAll:	BSR	InitScan		;prepare to scan the list
1$:	BSR	GetNextFile		;scan the list
	RTSERR
	BCLR	#6,df_Flags(A0)		;else mark as "excluded"
	BRA.S	1$

SArc:	BSR.S	IncArc
	BRA.S	SDExit

IncArc:	BSR	InitScan		;prepare to scan the list
1$:	BSR	GetNextFile		;scan the list
	RTSERR
	BTST	#4,df_Prot(A0)		;archive bit set (file unchanged)?
	BNE.S	1$			;yes...don't include it
	BSET	#6,df_Flags(A0)		;else mark as "included"
	BRA.S	1$

DArc:	BSR.S	ExcArc
	BRA.S	SDExit

ExcArc:	BSR	InitScan		;prepare to scan the list
1$:	BSR	GetNextFile		;scan the list
	RTSERR
	BTST	#4,df_Prot(A0)		;archive bit clear (file changed)?
	BEQ.S	1$			;yes...don't exclude it
	BCLR	#6,df_Flags(A0)		;else mark as "excluded"
	BRA.S	1$

SDExit:	BSR	UpdateNumbers
	BSR	UpdateWindow		;redisplay window
	JMP	MLoop

InitScan:
	MOVE.L	CurLevel,A4		;scan this level first
	ZAPA	A0			;start with no file selected
	RTS

GetNextFile:
	IFZL	A0,3$			;need to init this level
1$:	MOVE.L	df_Next(A0),A0		;get next sorted entry
2$:	IFZL	A0,4$			;end of this list...back a level
	BTST	#7,df_Flags(A0)		;dir?
	BEQ.S	9$			;no, file...use it
	IFZB	CDASFlg,1$		;don't process subdirs
	MOVE.L	A0,dl_GNFib(A4)		;save ptr to this level
	MOVE.L	df_SubLevel(A0),A4	;else process new subdir
3$:	MOVE.L	dl_ChildPtr(A4),A0	;else start it now
	BRA.S	2$
4$:	IFZB	CDASFlg,8$		;stay at current level
	IFEQL	CurLevel,A4,8$		;that's all...end of current level
	MOVE.L	dl_ParLevel(A4),A4	;else back to previous level
	IFZL	A4,8$			;thats all folks
	MOVE.L	dl_GNFib(A4),A0		;restore last FIB at this level
	BRA.S	1$
8$:	STC
9$:	RTS

* Get name string

SNam:	LEA	INamTxt,A0
	SETF	IncFlg
	BRA.S	NamCom
DNam:	LEA	ENamTxt,A0
	CLR.B	IncFlg
NamCom:
;	LEA	NamTxt,A0
;	MOVE.L	A1,it_IText(A0)
	MOVE.L	A0,REQTXT
	LEA	ReqNameGad,A0
	MOVE.L	A0,REQGAD		;INIT GADGET LIST
	BSR	OpenRequester		;OPEN THE REQUESTER
	ERROR	MLoop			;OPEN FAILURE
	LEA	ReqNameGad,A0
;	BSR	Activate
	BSR	WaitRequester		;GET RESPONSE
	ERROR	MLoop			;cancelled
	BSR	CloseRequester
	BSR.S	NameScan
	BRA	SDExit

NameScan:
	MOVE.	NamBuf,TMP.		;make name u.c.
	UCASE.	TMP.
	BSR	InitScan		;prepare to scan the list
1$:	BSR	GetNextFile		;scan the list
	RTSERR				;done
	BSR	WildMatch		;match?
	ERROR	1$			;no...ignore it
	BCLR	#6,df_Flags(A0)		;"exclude" it
	IFZB	IncFlg,1$		;make sure that was right
	BSET	#6,df_Flags(A0)		;else mark as "included"
	BRA.S	1$

* Get date string

SDat:
	LEA	IDatTxt,A2
	SETF	IncFlg
	BRA.S	DatCom
DDat:	LEA	EDatTxt,A2
	CLR.B	IncFlg
DatCom:	MOVE.L	A2,REQTXT
	LEA	ReqDateGad,A0
	MOVE.L	A0,REQGAD		;INIT GADGET LIST
	BSR	OpenRequester		;OPEN THE REQUESTER
	ERROR	MLoop			;OPEN FAILURE
	LEA	ReqDateGad,A0
;	BSR	Activate
1$:	BSR	WaitRequester		;GET RESPONSE
	ERROR	MLoop
	BSR	ValidateDate
	ERROR	1$			;bad date
	BSR	CloseRequester
	BSR.S	DateScan
	BRA	SDExit

DateScan:
	BSR	CalcDate		;return date param in D0
	MOVE.L	D0,D6
	BSR	InitScan		;prepare to scan the list
2$:	BSR	GetNextFile		;scan the list
	RTSERR				;done
	IFZB	IncFlg,3$		;excluding
	IFLTW	df_Date(A0),D6,2$	;ignore if filedate earlier than date
	BSET	#6,df_Flags(A0)		;else mark as "included"
	BRA.S	2$
3$:	IFGEW	df_Date(A0),D6,2$	;ignore if filedate =later than date
	BCLR	#6,df_Flags(A0)		;else mark as "excluded"
	BRA.S	2$

* Validates entered date.  Returns CY=1 on error.  DD-MMM-YY.  Returns
* day in D5, month in D6, and year in D7.

ValidateDate:
	LEN.	DatBuf			;right length?
	IFEQIB	D0,#8,3$		;okay
	IFNEIB	D0,#9,8$		;no...must be 8 or 9
3$:	LEA	DatBuf,A0
	BSR	Value			;get decimal value in D0
	MOVE.L	D0,D5			;day to D5
	BEQ	8$			;no day given
	IFNEIB	(A0)+,#'-',8$		;needs a dash here
	LEA	TMP2.,A1
	MOVE.B	(A0)+,(A1)+
	MOVE.B	(A0)+,(A1)+
	MOVE.B	(A0)+,(A1)+
	CLR.B	(A1)			;copy month to TMP2.
	IFNEIB	(A0)+,#'-',8$		;need another dash
	BSR	Value			;get year
	MOVE.L	D0,D7			;year to D7
	UCASE.	TMP2.
	LEA	MONTHS,A0
	MOVEQ	#1,D1			;month counter
1$:	BSR	CompStg			;compare TMP2. with A0	
	BEQ.S	2$			;found it
	ADDQ.L	#4,A0			;advance to next entry
	INCW	D1
	IFLEIW	D1,#12,1$		;TRY NEXT MONTH
	BRA.S	8$			;month error
2$:	MOVE.L	D1,D6			;month to D6
	IFLEB	D5,DAYS-1(D1.W),9$	;DAYS OK
	IFNEIW	D1,#2,8$		;days too big...not Feb
	MOVE.L	D7,D4			;is it leap year?
	AND.W	#3,D4			;leap year?
	BNE.S	8$			;no...error
	IFLEIW	D5,#29,9$		;Feb 29 OK if leap year
8$:	BEEP				;tell op of error
	STC
9$:	RTS

DAYS	DC.B	31,28,31,30,31,30,31,31,30,31,30,31

* Compares TMP2. with string pointed to by A0.  Z=1 on match.

CompStg:
	PUSH	A0
	LEA	TMP2.,A1
	MOVEQ	#3,D0
1$:	CMP.B	(A0)+,(A1)+
	BNE.S	9$
	DBF	D0,1$
9$:	POP	A0
	RTS
	
* Calculates AmigaDOS date (days since Jan 1, 1978) in D0, given month in
* D0, day in D1, and year in D2.  From EDN, 10/17/85 p. 168.

CalcDate:
	MOVE.L	D5,D1
	MOVE.L	D6,D0
	MOVE.L	D7,D2
	CMPI.W	#2,D0
	BLE.S	2$
	SUBQ.W	#3,D0
	BRA.S	3$
2$:	ADDI.W	#9,D0
	SUBQ.W	#1,D2
3$:	MULU	#1461,D2
	LSR.L	#2,D2
	MULU	#153,D0
	ADDQ.L	#2,D0
	DIVU	#5,D0
	ADD.W	D2,D0
	ADD.W	D1,D0
	SUB.W	#28431,D0		;adjust for Jan 1, 1978.
	RTS

* Checks file defined by FIB with desired name in TMP. (with possible wildcards).
* Returns CY=1 on failure, CY=0 on match.

WildMatch:
	PUSH	A0
	MOVE.	df_Name(A0),TMP2.	;move filename to TMP2. and make u.c.
	UCASE.	TMP2.
	LEA	TMP2.,A1
	LEA	TMP.,A0			;POINTERS FOR WILD MATCH
1$:	MOVE.B	(A0)+,D0		;get next wildname char
	BNE.S	2$			;got one
	MOVE.B	(A1)+,D1		;get next filename char
	BEQ	9$
	BRA	8$			;ELSE TRY NEXT
2$:	CMP.B	#'?',D0			;WILD CARD?
	BNE.S	3$			;NO
	MOVE.B	(A1)+,D1
	BEQ	8$
	BRA.S	1$
3$:	CMP.B	#'#',D0			;UNIVERSAL WILD CARD?
	BNE	5$			;NO
	MOVE.B	(A0)+,D0
	BEQ.S	8$
	CMP.B	#'?',D0			;'#?' ?
	BNE.S	8$			;WILD CARD ERROR
;	MOVE.B	(A0)+,D0		;any more chars in WILDname?
;	BEQ	9$			;no...take what we have
;4$:	MOVE.B	(A1)+,D1		;get next filename char	
;	BEQ.S	8$			;name too short
;	CMP.B	D0,D1			;aligned?
;	BNE.S	4$			;no
;	BRA	1$

	LEN.	A0			;get length of remaining wild stg
	MOVE.W	D0,D1			;save it
	PUSH	A0			;save ptr
	MOVE.L	A1,A0			;how much is left of filename?
	LEN.	A0
	POP	A0			;restore other ptr
	SUB.W	D1,D0			;is there enough to check?
	BMI.S	8$			;no...no match
	BEQ.S	1$			;equal...check it out
	PUSH	A0
	MOVE.L	A1,A0			;else strip off some of filename
	CLIP.	D0,A0
	POP	A0
	BRA.S	1$

5$:	MOVE.B	(A1)+,D1		;try char-by-char
	BEQ.S	8$			;filename too short
	CMP.B	D0,D1			;OK?
	BEQ	1$			;YES...TRY NEXT PAIR
8$:	STC
9$:	POP	A0
	RTS

* Calculates a binary value from an ASCII string.  Stops with first non-digit.
* A0 points to the string, updated on return, binary value in D0.

Value:
	ZAP	D0
	ZAP	D1
1$:	MOVE.B	(A0)+,D1
	BEQ.S	9$		;end of input string
	SUB.B	#'0',D1		;adjust to binary range
	BMI.S	9$		;oops...out of range
	IFGTIB	D1,#9,9$	;ditto
	MULU	#10,D0		;another digit
	ADD.W	D1,D0
	BRA.S	1$
9$:	DECL	A0		;leave it pointing to last char
	RTS

UpdWinFlg	DC.B	0	;1=UPDATE FILE WINDOW
IncFlg		DC.B	0	;1=include, 0=exclude
LevelFlg	DC.B	0	;1=dont add size...this is level block
DirBlkCnt	DC.B	0	;count of blocks
DisksPos	DC.W	0	;Disks: nn
BytesPos	DC.W	0	;Bytes: nn
FilTotPos	DC.W	0	;of nn files
FilCntPos	DC.W	0	;x-coordinate for file count 
MaxEnt		DC.W	0	;max number of entries in list
CurEnt		DC.W	0	;number of current entry
TotVols		DC.W	0	;total volumes
SelVols		DC.W	0	;selected volumes
TotFiles	DC.W	0	;total files in catalog
SelFiles	DC.W	0	;number of files selected
TotBytes	DC.L	0	;total size of all files
SelBytes	DC.L	0	;size of selected files
CatSize		DC.L	0	;size of catalog
AbsolutePos	DC.L	0	;absolute position in backup disk set
BaseDirBlock	DC.L	0	;points to first allocated dir block
CurDBBase	DC.L	0	;base of current dir block
CurDBPtr	DC.L	0	;dir block ptr
CurDBCnt	DC.L	0	;count of remaining bytes in dir block
CurLevel	DC.L	0	;ptr to current level
OriginalDir	DC.L	0	;lock of original dir

RootLevel	DS.B	dl_SIZEOF	;root level entry

	CNOP	0,4

FILE_INFO			;AMIGA-DOS FILE INFO BLOCK
FIB_KEY		DS.L	1	;DISK BLOCK
FIB_TYPE	DS.L	1	;<0=FILE, >1=DIRECTORY
FIB_NAME	DS.L	27	;NULL-TERMINATED NAME STRING (108 BYTES)
FIB_PROT	DS.L	1	;PROTECTION
FIB_ETYP	DS.L	1	;?
FIB_SIZE	DS.L	1	;SIZE IN BYTES
FIB_BLKS	DS.L	1	;SIZE IN BLOCKS
FIB_DATE	DS.L	1	;DAYS SINCE JAN 1, 1978
FIB_TIME	DS.L	1	;MINUTES OF DAY
FIB_TICK	DS.L	1	;TICKS OF MINUTE
FIB_COMM	DS.L	29	;NULL-TERMINATED COMMENT STRING (116 BYTES)

Path.		DS.B	PathSize
	END

