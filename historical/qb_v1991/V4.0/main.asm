;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
********************************************************* 
*							*
*	Quarterback main backup/restore routines	*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1988 Central Coast Software	*
*	    268 Bowie Dr, Los Osos, CA 93402		*
*	     All rights reserved, worldwide		*
*********************************************************


	INCLUDE	"vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"vd0:BOXES.ASM"
	INCLUDE "I/IO.i"

DEMO	EQU	0
		;1=DEMO VERSION, 0=NORMAL/EVAL

	XDEF	BACK_REST,ABORT,COMPLETE
	XDEF	GetChar,GetWord,GetLong
	XDEF	AllocateBuffers,FreeBuffers
	XDEF	MotorOff,PacketIO,DiskIO
	XDEF	Dev0IntS,Dev1IntS
	XDEF	PktArg1
	XDEF	ProcessMsg
	XDEF	RestDateProt
	XDEF	LoadFirst
	XDEF	Dev0Change,Dev1Change
	XDEF	FreeLocalStack

	XDEF	ActiveDev,FillPtr,FillCnt,ActiveParams,ActiveCyl,ActiveDsk
	XDEF	EBufQueue,AbortFlg,FirstDskFlg
	XDEF	Dev0Params,Dev1Params
	XDEF	DATESTAMP,DATE,TIME,CurFib
	XDEF	ReadBuffer
	XDEF	FillDisk,BlocksPerVolume
	XDEF	MaxWrites,VolumeSize,TrkBufSize
	XDEF	Dev0SelFlg,Dev1SelFlg,Dev0_IOB,Dev1_IOB
	XDEF	RQTxtBuf.,RQTxtBf1.,RQTxtBf2.
	XDEF	AltAbtFlg

* Externals defined in Amiga.lib, I guess.

	XREF	_CreatePort,_DeletePort,_CreateStdIO,_DeleteStdIO

* Externals defined in QB.ASM

	XREF	MenuPk,SavePC

	XREF	MAIN
	XREF	QB_TASK	
	XREF	SysBase,DosBase,IntuitionBase,GraphicsBase
	XREF	RefreshRoutine
	XREF	Mode,WINDOW,TMP.,TMP2.
	XREF	BakDrive,ResDrive
	XREF	DriveFlg,BArcFlg,FmtFlg,ChkFlg,EmptyFlg
	XREF	BRptFlg,RRptFlg,BSubFlg,RSubFlg,RplFlg,CDASFlg,RArcFlg
	XREF	MSG_CLASS,MSG_CODE,GAD_PTR
	XREF	BatchFlg,CatLoadErrFlg
	XREF	BRDevBuf1,BRDevBuf2,BRDevPathBuf
	XREF	SlowFlg,TestFlg

* externals defined in FILES.ASM

	XREF	InitList,NextFile
	XREF	CloseDirectory
	XREF	InitScan,GetNextFile
;;	XREF	OPEN_REQ,WAIT_REQ,CLOSE_REQ
	XREF	OpenRequester,CloseRequester
	XREF	WaitRequester,ProcessReqMsg
;;	XREF	CurrentPath
;;	XREF	ChgOrigDir

	XREF	SelVols,TotVols
;;	XREF	OriginalDir
	XREF	SelFiles,CurLevel,RootLevel,Path.
	XREF	AbsolutePos,FILE_INFO

* Externals defined in GADGETS.ASM

	XREF	BakOptGList,ResOptGList,AbortGad,CompGad
	XREF	Chg0Gad,Chg1Gad
	XREF	STATUS_BOXES,FINAL_STATUS
	XREF	ReqSkip,ReqCancel,ReqYes,REQTXT,REQGAD
	XREF	NotQB1.,NotQB2.,NotQB3.
	XREF	BSEQ1.,BSEQ2.,BSEQ3.
	XREF	BSET1.,BSET2.,BSET3.
	XREF	DupTxt,DP1
	XREF	RQText,RQTxt1,RQTxt2
	XREF	UnableTxt,UN1
	XREF	WrtErrTxt,WE1,WE2

* Externals defined in REPORT.ASM

	XREF	PrintReport

* Externals defined in FUN.ASM

	XREF	ADD_GADGETS,REMOVE_GADGETS,CLRWIN,StripAppend
	XREF	DRAW_BOXES,REFRESH
	XREF	MoveInsertDevice
	XREF	ReqYN

* Externals defined in MESSAGES.ASM

	XREF	Status,StRest.,StBack.,StTest.
	XREF	Disk.,File.
	XREF	OutOf.
	XREF	DevNS,DevRdy,DevAct,DevNRdy,PlsLoad,PlsRem
	XREF	AbortMsg,GoodBak,GoodRst,GoodTst,GdBakArc
	XREF	RecovErr.
;;	XREF	ArcProt.
	XREF	ArcWP1.,ArcWP2.,ArcWP3.
	XREF	LoadL1.,LoadL2.
	XREF	LoadN1.,LoadN2.
	XREF	ReadErr.,NoMemory
	XREF	WrtPrt1.,WrtPrt2.,WrtPrt3.
	XREF	BDSK1.,BDSK2.,BDSK3.
	XREF	ADOS1.,ADOS2.,ADOS3.
	XREF	MiscEr1.,MiscEr2.,MiscEr3.
	XREF	CantFindAltCat
	XREF	CantBak.,CantRes.
	XREF	FileInUse.
	XREF	DeviceFull.
	XREF	FileDelPrt.
	XREF	FileWrtPrt.
	XREF	FileRdPrt.
	XREF	WrtErrCode.
	XREF	RdEr1.,RdEr2.,RdEr3.
	XREF	CatEr1.,CatEr2.,CatEr3.

* Externals in DEVICE.ASM

	XREF	OpenBRDevice,CloseBRDevice,CFAbort
	XREF	TwoDevFlg,FlopFlg
	XREF	Dev0StatMsg,Dev1StatMsg
	XREF	VolumeOffset
	XREF	BufMemType

NBufs		EQU	4	;number of trackbuffers
LocalStackSize	EQU	8000	;private stack...wild ass guess
;IDSize		EQU	14	;bytes in Disk id block
IntVec		EQU	$12A	;offset into trackdisk data for interrupt vec
MenuNbr		EQU	$7E0	;NOITEM menu number
RQTxtBufSize	EQU	60	;max size of requester text
MP_SIGBIT	EQU	$F	;signal bit number in msg port

ERROR_OBJECT_IN_USE	EQU	202
ERROR_DISK_FULL		EQU	221
ERROR_DELETE_PROTECTED	EQU	222
ERROR_WRITE_PROTECTED	EQU	223
ERROR_READ_PROTECTED	EQU	224

ACTION_PROT	EQU	21	;packet command codes
ACTION_INHIBIT	EQU	31
ACTION_DATE	EQU	34
ACTION_COMMENT	EQU	28

TDERR_NotSpecified	EQU	20
TDERR_NoSecHdr		EQU	21
TDERR_BadSecPreamble	EQU	22
TDERR_BadSecID		EQU	23
TDERR_BadHdrSum		EQU	24
TDERR_BadSecSum		EQU	25
TDERR_TooFewSecs	EQU	26
TDERR_BadSecHdr		EQU	27
TDERR_WriteProt		EQU	28
TDERR_DiskChanged	EQU	29
TDERR_SeekError		EQU	30
TDERR_NoMem		EQU	31
TDERR_BadUnitNum	EQU	32
TDERR_BadDriveType	EQU	33
TDERR_DriveInUse	EQU	34
TDERR_PostReset		EQU	35

BACK_REST:
	IFNZB	BatchFlg,2$
	CLR.L	RefreshRoutine
	LEA	BakOptGList,A0
	IFZB	Mode,1$
	LEA	ResOptGList,A0
1$:	BSR	REMOVE_GADGETS
2$:	MOVE.W	#MenuNbr,D0
	MOVE.L	WINDOW,A0
	CALLSYS	OffMenu,IntuitionBase
	BSR	OpenBRDevice		;allocate ports, io blocks, and open
	ERROR	BR_EXIT,L		;problems allocating BR device(s)
	BSR	BuildSigBits
	CLR.B	IOActive		;no io active yet
	CLR.L	FHandle			;no file open
	CLR.B	InpCompFlg		;input/output complete flags
	CLR.B	OutCompFlg
	CLR.B	AbortFlg
	CLR.B	Dev0Status		;show devices not ready
	CLR.B	Dev1Status
	CLR.B	Dev0ChgFlg
	CLR.B	Dev1ChgFlg
	CLR.B	AltCatFlg
	CLR.W	ActiveDev
	CLR.W	ActiveFile
	CLR.W	ActiveCyl		;start with cyl 0 (0 origin)
	CLR.B	ErrFillFlg
;;	CLR.L	OriginalDir		;make sure nothing there
	MOVEQ	#1,D0
	MOVE.W	D0,ActiveDsk		;finish init for backup
	IFNZB	Mode,StatScreen		;if restore, skip the rest

	MOVE.W	D0,ActiveDev		;force switch to Dev0
	MOVE.B	D0,FillDisk		;start with disk 1
	MOVE.L	#DATESTAMP,D1
	CALLSYS	DateStamp,DosBase	;current date/time for this backup

* initialize status display

StatScreen:
	MOVE.B	#1,Dev0Msg		;load disk 1
	CLR.B	Dev1Msg
	IFEQIW	SelVols,#1,1$		;don't need Dev1...only 1 disk
	MOVE.B	#2,Dev1Msg		;load disk 2
1$:	MOVE.L	#RefreshStatus,RefreshRoutine
	BSR	RefreshStatus
	LEA	Chg0Gad,A0		;enable status screen gadgets
	IFZB	FlopFlg,2$		;use CHANGE gadgets
	LEA	AbortGad,A0
2$:	BSR	ADD_GADGETS
	IFNZB	Mode,RestoreFromFloppies,L
	CLR.L	ActiveParams		;start backup with no active drive
	BSR	StackInit		;allocate and init the local stack
	ERROR	BR_EXIT			;cant get stack...bail out
	SETF	CatFlg			;show in catalog mode
	BSR	InitFileDir		;get ready to scan the file catalog
	BSR	UpdateDisk

BackupLoop:
1$:	BSR	ProcessMsg		;check for IntuiMsg and process it
	IFNZB	AbortFlg,AbortExit,L	;bail out
	BSR	FillBuffer		;fill a buffer, if available
	BSR	DiskChanged		;check if diskette changed
	BSR	CheckWriteComp		;check for write complete
	IFNZB	AbortFlg,AbortExit,L	;bail out
	BSR	StartWrite		;start write, if none going
	IFNZB	AbortFlg,AbortExit,L	;just in case...
	IFNZB	InpCompFlg,3$		;input done...dont fill
	IFNZL	EBufQueue,1$		;empty buffers to be filled
3$:	IFNZB	OutCompFlg,BackupExit	;output done...notify operator
	MOVE.L	SigBits,D0		;else wait till something happens
	CALLSYS	Wait,SysBase		;wait until operator acts or io done
	BRA	1$

BackupExit:
	BSR	StatusNotRdy
	BSR	DskMsgRemove
	MOVE.L	#RefreshBExit,RefreshRoutine
	BEEP
	BSR	BackupExitMsg
	BSR	Cleanup
	BRA	ExitCom

* Main Restore routine

RestoreFromFloppies:
	CLR.L	ReadCnt			;nothing in buffer to start...
	MOVE.L	#TMP.,D0
	LSR.L	#2,D0			;make into BSTR for ADos
	MOVE.L	D0,BSTR_TMP.		;save for date/prot update
	BSR	InitList		;make sure ready to work the list
	BSR	UpdateDisk		;put up first drive number

RestoreLoop:
1$:	BSR	ProcessMsg		;check for IntuiMsg and process it
	IFNZB	AbortFlg,AbortExit,L	;bail out
	BSR	DiskChanged		;check out possible new disk
	IFNZL	FHandle,2$		;already have file open
	BSR	NextFile		;returns next selected file
	ERROR	RestoreExit		;last entry...all done
	INCW	ActiveFile		;bump active file number
	BSR	UpdateFile		;update number being processed
	BSR	StatusActive		;show disk active
	BSR	UpdateDisk		;update disk number on screen 
	BSR	SkipToDisk		;position to the correct disk/track
	ERROR	1$			;problems loading/reading disk/track
	BSR	OpenRestoreFile		;try to open the new file
	ERROR	1$			;oops...couldn't open it
2$:	BSR	ReadBackupData		;read data into trk buf, close on err
	ERROR	1$
	BSR	WriteBuffer		;write data to file, close if eof
	BRA	1$

RestoreExit:
	IFNZB	AbortFlg,AbortExit	;really is an abort
	MOVE.L	#RefreshRExit,RefreshRoutine
	BEEP
	BSR	StatusNotRdy
	BSR	DskMsgRemove
	BSR	RestoreExitMsg
	BSR	Cleanup
	BRA	ExitCom

* Come here when op aborts in middle of operation.

AbortExit:
	MOVE.L	#RefreshAExit,RefreshRoutine
	IFZL	ActiveParams,2$
	BSR	StatusNotRdy		;show disk not ready
	BSR	DskMsgRemove
2$:;;	BSR	UnLockDir		;unlock active dir
	BSR	CloseFile		;just in case...
;;	BSR	ChgOrigDir
	BSR	Cleanup
	BSR	AbortExitMsg
;;	CLR.B	BRptFlg			;no report on abort
;;	CLR.B	RRptFlg
;;	CLR.B	BArcFlg			;and don't update archive bits
	SETF	AltAbtFlg

ExitCom:
	CLR.B	CompFlg			;make sure flags aren't set
	CLR.B	AbortFlg
	CLR.B	Dev1ChgFlg
	CLR.B	Dev0ChgFlg
	IFNZB	BatchFlg,GetOut		;batch stream...don't wait
1$:	BSR	ProcessMsg		;operator finally act?
	BSR	DiskChanged		;in case op pulls a disk
	IFNZB	CompFlg,GetOut		;yes
	IFNZB	AbortFlg,GetOut		;yes
	MOVE.L	SigBits,D0		;else wait till something happens
	CALLSYS	Wait,SysBase		;wait until operator acts or io done
	BRA.S	1$

GetOut:	BSR	CloseBRDevice		;shut down the devices
	LEA	CompGad,A0
	BSR	REMOVE_GADGETS
	IFNZB	AbortFlg,8$,L		;aborted...
	IFNZB	AltAbtFlg,8$,L		;aborted...
	IFNZB	Mode,8$,L		;restore...
	IFZB	BArcFlg,8$		;don't set the archive bits
	MOVE.L	#RefreshArc,RefreshRoutine
	BSR	RefreshArc
	LEA	AbortGad,A0
	BSR	ADD_GADGETS
	BSR	InitList		;work the list again
1$:	BSR	ProcessMsg		;check for possible abort
	IFNZB	AbortFlg,8$		;op wants to quit
	BSR	NextFile		;scan for another file
	ERROR	8$			;done
2$:	MOVE.L	CurFib,A0
	BTST	#5,df_Flags(A0)		;error in backing up this one?
	BNE.S	1$			;yes...don't set archive bit
	BSET	#4,df_Prot(A0)		;else set the arc bit
	BNE.S	1$			;archive bit already set...
	BSR	RestDateProt		;else set it now
	MOVE.L	PktRes2,D0		;get possible error code
	IFNEIW	D0,#214,1$		;not write protected
	LEA	ArcWP1.,A0
	MOVE.L	#ArcWP2.,D0
	MOVE.L	#ArcWP3.,D1
	BSR	BldWarningMsg		;put unit number into msg
	BSR	ReqRQTxt
	NOERROR	2$
	SETF	AbortFlg
8$:	LEA	AbortGad,A0
	BSR	REMOVE_GADGETS
	BSR	PrintReport
	BRA.S	BR_EX1

BR_EXIT:
	BSR	CFAbort			;close any open floppies
BR_EX1:	BSR	CloseDirectory
	JMP	MAIN			

Cleanup:
	IFZL	ActiveParams,1$		;may need to close last disk
	IFZB	IOActive,2$		;no io active
	MOVE.L	ActiveParams,A2
	MOVE.L	DevIOB(A2),A1
	CALLSYS	AbortIO,SysBase		;try to stop the IO
	MOVE.L	DevIOB(A2),A1
	CALLSYS	WaitIO
	CLR.B	IOActive
2$:	LEA	Dev0Params,A2
	BSR	CleanupDrive
	IFZB	Dev1SelFlg,3$		;not using Dev1
	LEA	Dev1Params,A2
	BSR	CleanupDrive
3$:	CLR.L	ActiveParams
1$:	LEA	Chg0Gad,A0
	IFZB	FlopFlg,4$
	LEA	AbortGad,A0
4$:	BSR	REMOVE_GADGETS
	GadColor CompGad,BLACK
	LEA	CompGad,A0
	BSR	ADD_GADGETS
	CLR.B	CompFlg			;wait till op wants to proceed
	BSR	FreeLocalStack		;release Local STack
	BSR	EmptyFullBuffers
	MOVE.W	#MenuNbr,D0
	MOVE.L	WINDOW,A0
	CALLSYS	OnMenu,IntuitionBase
	RTS

* turns off motor and puts up final msgs for floppy drives.  Ptr in A2.

CleanupDrive:
	MOVE.L	A2,ActiveParams
	BSR	MotorOff		;just in case...
	IFZB	DevStatus(A2),1$	;drive wasn't ready
	BSR	DskMsgRemove		;tell op to remove disk
	BRA.S	2$
1$:	BSR	DskLMsgClear
2$:	BSR	StatusNotRdy		;last drive done
	RTS

* Open file to be restored.  CurFib points to FIB for file.
* Assume that we are already in current dir for this file (set up elsewhere).
* Looks for existing file and checks some option flags depending on what it
* finds.

OpenRestoreFile:
	MOVE.L	CurFib,A0		;save ptr to this file's FIB
	MOVE.L	df_Size(A0),ByteCount	;restore this many bytes
	LEA	df_Name(A0),A5
	IFZB	TestFlg,4$		;not test node, try to open
	MOVEQ	#1,D0
	MOVE.L	D0,FHandle		;mark as "test" file
	BRA	9$
;;4$:	IFNZB	RplFlg,1$,L		;replacing old files...don't even check
4$:	IFEQIB	RplFlg,#1,1$,L		;replacing old files...don't even check
;;	MOVE.L	A5,D1
	MOVE.	Path.,TMP.
	MOVE.L	A5,A0
	APPEND.	A0,TMP.
	MOVE.L	#TMP.,D1
	MOVE.L	#ACCESS_READ,D2
	CALLSYS	Lock,DosBase		;try to find the file
	MOVE.L	D0,D1
;;	BEQ.S	2$			;didn't get it
	BEQ.S	1$			;didn't find file
	CALLSYS	UnLock			;else unlock
	IFNEIB	RplFlg,#2,3$		;don't overwrite it...and don't ask
	LEA	DP1,A0			;else ask op what to do
	MOVE.L	A5,it_IText(A0)		;pass name of file	
	LEA	DupTxt,A0
	BSR	ReqYN			;yes/no requester
	ERROR	3$			;op wants to skip this one
	BRA.S	1$			;else overwrite it

2$:;;	IFZB	ExistFlg,1$		;restore it anyway
3$:	BSR	MarkFileBad		;mark it bad in catalog
	BRA.S	5$			;and bail out

1$:
	IFNE	DEMO-1

;;	MOVE.L	A5,D1
	MOVE.	Path.,TMP.
	MOVE.L	A5,A0
	APPEND.	A0,TMP.
	MOVE.L	#TMP.,D1
	MOVE.L	#NEW,D2
	CALLSYS	Open,DosBase		;create a new one (replacing old)
	MOVE.L	D0,FHandle		;got a handle?
	BNE.S	9$			;yes...lets go..
	BSR	DispWriteError		;check error and warn op
	ENDC

	BSR	UnableToProcess		;warn op can't restore this file
5$:;;	BSR	UnLockDir		;bail out...free the dir
	BSR	CloseFile		;and close the file
	STC				;bad open
9$:	RTS

* Reads data from backup floppy into read buffer.

ReadBackupData:
	IFNZL	ReadCnt,9$		;something in read buffer...
	IFZL	ByteCount,9$		;no need to read anything
	BSR	ReadNxtTrk		;else read a track
	NOERROR	9$
	BSR	MarkFileBad		;
	BSR	UnableToProcess		;warn op can't restore this file
;;	BSR	UnLockDir		;bail out...free the dir
	BSR	CloseFile		;and close the file
	STC				;bad open
9$:	RTS

* Writes full (or partial) buffer to open file.  Returns CY=1 on write error.

WriteBuffer:
	BSR	UpdateStatusBar
	MOVE.L	ReadCnt,D3		;write this many...or less
	IFGEL	ByteCount,D3,1$		;file can hold all of track buffer
	MOVE.L	ByteCount,D3		;write smaller of ReadCnt or ByteCount
1$:
	IFNE	DEMO-1

	IFNZB	TestFlg,2$		;test only, don't try to write data
	MOVE.L	FHandle,D1
	MOVE.L	ReadPtr,D2
	PUSH	D3
	CALLSYS	Write,DosBase		;write it out to the drive
	POP	D3
	TST.L	D0
	BPL.S	2$			;good write
	BSR	DispWriteError		;else warn operator
	IFNEIW	WrtErrNbr,#ERROR_DISK_FULL,3$ 
	SETF	AbortFlg		;abort restore on disk full
;;	BSR	UnLockDir
3$:	BSR	CloseFile
	BSR	UnableToProcess		;cant finish this file
	STC
	RTS

	ENDC

2$:	ADD.L	D3,ReadPtr		;bump ptr to here, in case of short
	SUB.L	D3,ReadCnt		;adjust remaining count
	SUB.L	D3,ByteCount		;wrote this many
	BHI.S	4$			;still more in this file...go again
	BSR	CloseFile		;else close the file
	IFNZB	TestFlg,4$		;test mode, don't update date time
	BSR	RestDateProt		;restore file date/time and prot bits
4$:	CLC
	RTS

* Marks file bad in catalog for restoration report

MarkFileBad:
	MOVE.L	CurFib,A0
	BSET	#5,df_Flags(A0)		;mark as 'error' for report
	RTS

* Checks write error and puts up write error requester with proper msg.

DispWriteError:
	BSR.S	MarkFileBad
	CALLSYS	IoErr,DosBase		;get io error code in D0
	MOVE.L	D0,WrtErrNbr		;save error code
	LEA	WrtErrMsgTbl,A1		;ptr to msg table
1$:	MOVE.L	(A1)+,WE2+it_IText	;move ptr to msg into table
	MOVE.L	(A1)+,D1		;get code from table
	BEQ.S	2$			;no match in table...use default
	IFNEL	D0,D1,1$		;no match
	BRA.S	3$
2$:	PUSH	D0			;save error code
	MOVE.L	-8(A1),A0		;get msg address 
	MOVE.	A0,TMP.
	POP	D0
	STR.	W,D0,TMP2.,#32,#4
	APPEND.	TMP2.,TMP.
	MOVE.L	#TMP.,WE2+it_IText
3$:	MOVE.L	CurFib,A0
	LEA	df_Name(A0),A1		;pass name for requester
	LEA	WE1,A0
	MOVE.L	A1,it_IText(A0)
	LEA	WrtErrTxt,A0
	BSR	ReqCom			;put up requester
	RTS

* Warns that file in CurFib cannot be backed up or restored.
* Also sets flag for report.

UnableToProcess:
	MOVE.L	CurFib,A0
	BSET	#5,df_Flags(A0)		;mark error for report
	LEA	df_Name(A0),A1		;pass name for requester
	LEA	UN1,A0
	MOVE.L	A1,it_IText(A0)
	LEA	UnableTxt,A0
	LEA	CantBak.,A1
	IFZB	Mode,1$
	LEA	CantRes.,A1
1$:	MOVE.L	A1,it_IText(A0)
	BSR	ReqCom			;put up requester
	RTS				;either way, go on

;* Puts up yes/no requester.

;ReqYN:	MOVE.L	A0,REQTXT
;	LEA	ReqYes,A0		;yes/no gadgets
;	MOVE.L	A0,REQGAD		;INIT GADGET LIST
;	BSR	OpenRequester		;OPEN THE REQUESTER
;	ERROR	8$			;OPEN FAILURE
;	BSR	WaitRequester		;GET RESPONSE
;	ERROR	8$			;NO
;	BSR	CloseRequester		;YES
;	CLC
;8$:	RTS

* Positions to correct disk, track, and offset, based on AbsolutePos.

SkipToDisk:
	MOVE.L	AbsolutePos,D0		;get position of start of file
	MOVE.L	VolumeSize,D1
	SUB.L	#IDSize,D1
	ZAP	D2			;disks are 1 origin
1$:	INCL	D2
	SUB.L	D1,D0			;crude divide routine
	BPL.S	1$
	MOVE.W	D2,NewDsk		;desired diskette
	ADD.L	D1,D0			;D0 now has remainder
	ADD.L	#IDSize,D0		;adjust for track 0 offset
	MOVE.L	TrkBufSize,D1		;size of a cylinder
	MOVEQ	#-1,D2			;tracks are 0 origin
2$:	INCL	D2
	SUB.L	D1,D0			;calc track
	BPL.S	2$
	MOVE.W	D2,NewCyl		;desired track
	ADD.L	D1,D0			;desired offset in track
	MOVE.L	D0,NewOffset
	IFNEIW	DiskID+2,#'c2',11$	;not alt cat disk
	MOVE.W	#1,ActiveDev		;else force switch to device 0
	BRA.S	12$
11$:	IFEQW	ActiveDsk,NewDsk,6$	;already on the correct disk
12$:	MOVE.W	NewDsk,ActiveDsk	;this is disk we need
	BSR	LoadNextFlop		;else try next floppy
	ERROR	8$			;op bailed out
6$:	IFNZB	BadDskFlg,8$		;show a bad read...bad disk
3$:	IFEQW	ActiveCyl,NewCyl,5$	;already on proper cylinder
13$:	BSR	UpdateStatusBar
	INCW	ActiveCyl
	IFLTW	ActiveCyl,NewCyl,13$
;;	MOVE.W	NewCyl,ActiveCyl	;else this is proper cyl position
	BSR	ReadTrackErr		;and read that sucker
	ERROR	8$			;oops...problems reading disk
	BRA.S	4$
5$:	IFNZB	BadTrkFlg,8$
4$:	MOVE.L	ReadBuffer,D0		;base of buffer
	MOVE.L	NewOffset,D1		;start here in buffer
7$:	ADD.L	D1,D0
	MOVE.L	D0,ReadPtr
	MOVE.L	TrkBufSize,D0
	SUB.L	D1,D0			;number of bytes left in buffer
	MOVE.L	D0,ReadCnt
	RTS				;ready to use data...
8$:	BSR	MarkFileBad		;make sure we show it bad
	STC
	RTS

* Initialize local stack and prepare for operation

StackInit:
	MOVE.L	#LocalStackSize,D2	;size of stack we need
	MOVE.L	D2,D0
	MOVE.L	#MEM_CLEAR,D1
;	ZAP	D1
	CALLSYS	AllocMem,SysBase
	MOVE.L	D0,LocalStackBase	;else set up ptrs
	BNE.S	StackReset		;GOT IT
	DispErr	NoMemory		;can't get enuf
	STC
	RTS

* Called when backup starts or if bad disk detected during backup.

StackReset:
	MOVE.L	LocalStackBase,D0
	MOVE.L	#LocalStackSize,D2
	ADD.L	D2,D0
	MOVE.L	D0,A0
	LEA	FBGo,A1			;set initial PC
	MOVE.L	A1,-(A0)
	MOVEM.L	A0-A6/D0-D7,-(A0)	;put a set of regs on the stack
	MOVE.L	A0,LocalStackPtr	;save new SP
	CLR.W	FBCylCnt		;Current cyl on disk
	RTS

* Load track buffer with data.  Get the buffer from empty queue and put it on
* the full queue.  If we reach the end of the operation, set the DoneFlg.

FillBuffer:
	IFNZB	InpCompFlg,9$		;no more files to process
	IFGEW	FBCylCnt,MaxWrites,9$	;at end of disk...wait for last write
	LEA	EBufQueue,A1		;empty buffer queue
	IFZL	(A1),9$			;no buffer empty
	BSR	UnQueueBuf		;get buffer
	MOVE.L	D0,FillBuf		;use this buffer
	MOVE.L	D0,FillPtr		;set data ptr to start of buffer
	BEQ	9$			;just in case...
	CLR.L	FillCnt			;no bytes in buffer
	IFNZW	FBCylCnt,1$		;not first cyl of disk
	BSR	InitFirstCyl		;else put Quarterback ID into buffer
1$:	PUSH	A2-A5/D2-D7		;follow the rules
	MOVE.L	SP,SaveSP		;save current SP
	MOVE.L	LocalStackPtr,SP	;swap to new stack
	POP	A0-A6/D0-D7		;restore context
9$:	RTS				;and away we go...

* Release FillBuffer stack

FreeLocalStack:
	MOVE.L	LocalStackBase,D0
	BEQ.S	9$
	MOVE.L	D0,A1
	MOVE.L	#LocalStackSize,D0	;size of stack
	CALLSYS	FreeMem,SysBase
	CLR.L	LocalStackBase
9$:	RTS

* This is initial entry point at start of Backup.

FBGo:	IFZB	CatFlg,4$		;not doing the catalog
	BSR	WriteCatalog		;write catalog to disk
	BSR	InitList

4$:	IFNZB	ErrFillFlg,14$,L	;filling...
	IFNZL	FHandle,1$		;already have file open
5$:;	IFGEW	ActiveFile,SelFiles,7$,L ;reached the end
	BSR	NextFile		;returns next selected file
	ERROR	7$,L			;last entry...all done
	INCW	ActiveFile		;bump active file number
	MOVE.L	df_Size(A0),ByteCount	;size of this file
	LEA	df_Name(A0),A5		;save name ptr
	MOVE.L	A0,CurFib		;save ptr to this FIB
	BSR	UpdateFile		;and update the screen
	MOVE.	Path.,TMP.
	MOVE.L	A5,A0
	APPEND.	A0,TMP.
	MOVE.L	#TMP.,D1
;;	MOVE.L	A5,D1
	MOVE.L	#EXISTING,D2
	CALLSYS	Open,DosBase		;try to open that sucker
	MOVE.L	D0,FHandle		;good open?
	BEQ.S	13$			;NO...gotta fill
1$:
	IFNZB	ErrFillFlg,14$		;filling...
;;	BTST	#0,FillPtr+3		;odd boundary?
;;	BEQ.S	6$
;;	INCL	FillPtr			;round data buffer ptr up to word boundary
;;	INCL	FillCnt			;and count the "unused" byte
6$:	MOVE.L	FHandle,D1		;read from this file
	MOVE.L	FillPtr,D2		;read data into buffer here
	MOVE.L	TrkBufSize,D3
	SUB.L	FillCnt,D3		;number of bytes in buffer
	CALLSYS	Read,DosBase		;try to fill the buffer
	TST.L	D0			;what happened?
	BPL.S	2$			;good result
	BSR	CloseFile		;read error...
13$:	SETF	ErrFillFlg		;now we must fill file
	BSR	UnableToProcess		;can't backup this file
14$:	MOVE.L	ByteCount,D0
	BEQ.S	8$			;no more fill required
	MOVE.L	TrkBufSize,D1
	SUB.L	FillCnt,D1		;how much room is left?
	IFLEL	D0,D1,15$		;we have room...bump ptrs
	MOVE.L	D1,D0			;else use room we have
	BRA.S	15$
2$:	BNE.S	3$			;not end of file
	BSR	CloseFile		;else close this one
	IFNZL	ByteCount,13$		;oops...file too short
;	IFZB	BArcFlg,8$		;don't set the archive bit
;	MOVE.L	CurFib,A0
;	BSET	#4,df_Prot(A0)		;else set the arc bit
;	BSR	RestDateProt		;and call common routine
;	MOVE.L	PktRes2,D0		;get possible error code
;	IFNEIW	D0,#214,8$		;not write protected
;	DispErr	ArcProt.		;else warn about write protection
;	SETF	AbortFlg
;	BRA	FBLastExit		;stop now
8$:
	CLR.B	ErrFillFlg		;not filling any more
	BRA	4$			;...and open the next file, if any

3$:	MOVE.L	ByteCount,D1
	IFLEL	D0,D1,15$		;amount read not too many
	MOVE.L	D1,D0			;else truncate at this length
	MOVE.L	CurFib,A0
	BSET	#5,df_Flags(A0)		;and report the error
15$:	SUB.L	D0,ByteCount		;count of bytes in this file
	ADD.L	D0,FillPtr		;adjust data ptr
	ADD.L	D0,FillCnt		;count the bytes
	IFLTL	FillCnt,TrkBufSize,1$,L ;loop for more data
	BSR	QueueFullBuf
	BRA	1$

7$:;;	BSR	ChgOrigDir		;make sure we are back home again
	BSR	PadandQueue		;write out last buffer
	BSR	InitFileDir		;reset catalog for second copy
	SETF	CatFlg			;show we are in catalog again
	SETF	AltCatFlg		;show writing alt catalog
	BSR	InitFirstCyl		;set up buffer for ID again
	BSR	WriteCatalog		;write out catalog second time
	SETF	InpCompFlg		;else show input complete
;	BRA	FBLastExit		;that's all...no more

FBLastExit:
	BSR	PadandQueue		;pad remaining space in buffer and queue

* CALL this when ready to exit from FillBuffer.  Restores original context.
* Fall through on final exit.

FBExit:
	PUSH	A0-A6/D0-D7		;save FillBuffer context
	MOVE.L	SP,LocalStackPtr	;save SP
	MOVE.L	SaveSP,SP		;restore original SP
	POP	A2-A5/D2-D7
	RTS				;back to caller

* Pads remaining space in a buffer and queues buffer.

PadandQueue:
	MOVE.L	TrkBufSize,D0
	SUB.L	FillCnt,D0		;calc number of bytes to fill
	BLS.S	1$			;buffer full
	BSR	FillWithNulls
1$:	MOVE.L	FillBuf,A0		;else queue the full buffer
	LEA	FBufQueue,A1
	BSR	QueueFullBuf		;add buffer to full queue
	RTS

* Stores catalog on disk, either on first disk or last disk.

WriteCatalog:
	IFEQIB	CatFlg,#2,1$		;already put out file count
	LEA	RootLevel,A1		;count entries at root level
	BSR	CountEntries
	BSR	PutWord			;save this count
	MOVE.B	#2,CatFlg		;have put out filecount
1$:	BSR	GetFileDir		;else scan for next file in catalog
	ERROR	9$,L			;thats end of catalog
	BTST	#7,df_Flags(A0)		;dir?
	BEQ.S	5$			;no...file
	IFNZB	BSubFlg,2$		;we backing up full subdir struct...
5$:	BTST	#6,df_Flags(A0)		;else is object selected?
	BEQ.S	1$			;no...ignore it
2$:	MOVE.L	A0,A3			;gotta save the reg...
	MOVE.L	df_Size(A3),D0		;file size
	BSR	PutLong
	MOVE.W	df_Date(A3),D0		;file date
	BSR	PutWord
	MOVE.W	df_Time(A3),D0		;file time
	BSR	PutWord
	MOVE.W	df_Ticks(A3),D0		;file ticks
	BSR	PutWord
	MOVE.W	df_FilCnt(A3),D0	;dir file count
	BSR	PutWord
	MOVE.B	df_Prot(A3),D0		;protection bits
	BSR	PutChar
	MOVE.B	df_Flags(A3),D0		;file flags
	BSR	PutChar
	LEA	df_Name(A3),A3		;file name
4$:	MOVE.B	(A3),D0
	BSR	PutChar
	TST.B	(A3)+
	BNE.S	4$
6$:	MOVE.B	(A3),D0			;file comments
	BSR	PutChar
	TST.B	(A3)+
	BNE.S	6$
	BRA	1$

9$:	CLR.B	CatFlg			;done with catalog...write the files
;	BSR	InitList		;prepare to scan the catalog
	RTS


InitFileDir:
	LEA	RootLevel,A4
	MOVE.L	A4,CurLevel
	CLR.L	dl_CurFib(A4)		;force reset on entrance
	RTS

* Gets next file or dir in A0.  Used for catalog writes only.

GetFileDir:
	MOVE.L	CurLevel,A4		;check current level
	MOVE.L	dl_CurFib(A4),A0	;this was last one we processed
	IFZL	A0,1$			;first time in, init and go
	BTST	#7,df_Flags(A0)		;dir?
	BEQ.S	2$			;no...a file
	MOVE.L	A0,dl_GNFib(A4)		;else save ptr to this level
	MOVE.L	df_SubLevel(A0),A4	;new level
	MOVE.L	A4,CurLevel
1$:	MOVE.L	dl_ChildPtr(A4),A0	;first entry at new level
	BRA.S	3$
2$:	MOVE.L	df_Next(A0),A0		;link to next in this chain
3$:	IFNZL	A0,4$			;not end of chain...process this
	MOVE.L	dl_ParLevel(A4),A4	;backup to previous level
	IFZL	A4,8$			;end of root level...that's all folks!
	MOVE.L	A4,CurLevel
	MOVE.L	dl_GNFib(A4),A0		;ptr to dir entry at prev level
	BRA.S	2$
4$:	BTST	#7,df_Flags(A0)		;a dir?
	BEQ.S	7$			;no...use it
	MOVE.L	df_SubLevel(A0),A1
	BSR	CountEntries		;count entries in new level
	MOVE.W	D0,df_FilCnt(A0)	;save new level count in old level entry
7$:	MOVE.L	A0,dl_CurFib(A4)
	RTS				;A0 points to FIB to use...
8$:	STC
	RTS

* Restores a file date/time stamp, protection bits and comments.  Called 
* during backup to set the archive bit on a file, if op wants to.

RestDateProt:
	PUSH	A2-A4
	MOVE.L	CurFib,A2
;;	BSR	UnLockDir
	MOVE.	Path.,TMP.
	APPEND.	df_Name(A2),TMP.
	MOVE.L	#TMP.,D1
;	MOVE.L	#Path.,D1
	CALLSYS	DeviceProc,DosBase
	IFZL	D0,9$,L
	MOVE.L	D0,A3			;process ID
	CALLSYS	IoErr
	MOVE.L	D0,PktArg2		;lock...from DeviceProc via IoErr
;	BEQ	9$			;no lock...must be a dir
	LEN.	TMP.
	MOVE.B	D0,-(A0)		;store length just ahead of TMP.
	MOVE.L	A0,D0
	LSR.L	#2,D0			;make BSTR out of TMP.-1's address
	MOVE.L	D0,PktArg3
	ZAP	D0
	MOVE.B	df_Prot(A2),D0
	IFZB	Mode,2$			;not restore
	IFZB	RArcFlg,2$		;don't set arc bit
	BSET	#4,D0			;else set arc bit
2$:	MOVE.L	D0,PktArg4
	MOVE.L	#ACTION_PROT,D0
	MOVE.L	A3,A0			;process id
	BSR	PacketIO		;update protection on file

	IFZB	Mode,9$			;don't update date stamp if backup
	ZAP	D0
	MOVE.W	df_Date(A2),D0
	MOVE.L	D0,DATE
	MOVE.W	df_Time(A2),D0
	MOVE.L	D0,TIME
	MOVE.W	df_Ticks(A2),D0
	MOVE.L	D0,TICKS
	MOVE.L	#DATESTAMP,PktArg4
	MOVE.L	#ACTION_DATE,D0
	MOVE.L	A3,A0
	BSR	PacketIO		;update date on file
	LEA	df_Name(A2),A0		;point to name
1$:	TST.B	(A0)+			;skip past name to comments, if any
	BNE.S	1$
	IFZB	(A0),9$			;no comment anyway...ignore it
	MOVE.	A0,FILE_INFO+1		;else copy comments
	LEN.	FILE_INFO+1
	MOVE.B	D0,-(A0)		;length to make it BSTR
	MOVE.L	A0,D0
	LSR.L	#2,D0			;make BSTR out of address
	MOVE.L	D0,PktArg4
	MOVE.L	#ACTION_COMMENT,D0
	MOVE.L	A3,A0			;process id
	BSR	PacketIO		;update comment on file
9$:	POP	A2-A4
	RTS

* Unlocks current dir, if locked.

;;UnLockDir:
;;	PUSH	A4
;;	MOVE.L	CurLevel,A4
;;	IFZL	A4,1$			;no current dir
;;	MOVE.L	dl_DirLock(A4),D1
;;	BEQ.S	1$
;;	CALLSYS	UnLock,DosBase
;;	CLR.L	dl_DirLock(A4)		;unlock dir for now
;;1$:	POP	A4
;;	RTS

* Counts entries in next level defined by the dir entry in A1.  Count in D0.

CountEntries:
	PUSH	A1
	ZAP	D0			;name counter
	LEA	dl_ChildPtr(A1),A1	;count files in new level chain
5$:	MOVE.L	df_Next(A1),A1		;get next entry
	IFZL	A1,6$			;end of chain
	BTST	#7,df_Flags(A1)		;dir?
	BEQ.S	2$			;no, file
	IFNZB	BSubFlg,1$		;full sub structure...count it
2$:	BTST	#6,df_Flags(A1)		;selected?
	BEQ.S	5$			;no...dont count it
1$:	INCL	D0			;count the name
	BRA.S	5$
6$:	POP	A1
	RTS

* Inits first 14 bytes of first cyl.  Format:  
*	DC.B	'QBnn'		;keeps AmigaDOS from trying to use it
*	DC.B	N		;disk number, 1 origin
*	DC.B	0		;1=alternate catalog wrapped
*	DC.L	DATE		;date of backup, binary days since Jan 1, 1978
*	DC.L	TIME		;time of backup, in binary minutes since midnight

InitFirstCyl:
	MOVE.L	CurLevel,SavCurLevel	;save params for start of this disk
	MOVE.L	CurFib,SavCurFib
	MOVE.L	ByteCount,SavByteCount	;number of bytes left to process
	MOVE.W	ActiveFile,SavActiveFile
	MOVE.	Path.,SavPath.
	BSR	SaveLevelFib		;save CurFib at each level
	STR.	B,FillDisk,DiskNbr,#$30,#2
	MOVE.L	FillPtr,A0		;start here
	MOVE.W	#'QB',(A0)+
	MOVE.W	DiskNbr,D0		;get ASCII disk number
	IFZB	AltCatFlg,1$		;not ready to write alt cat
	MOVE.W	#'c2',D0
1$:	MOVE.W	D0,(A0)+		;store 'QBnn'
	MOVE.B	FillDisk,(A0)+		;disk number
	ZAP	D0
	IFZB	AltCatFlg,2$		;not alt cat
	IFNZW	FBCylCnt,2$		;alt cat NOT at first track
	INCW	D0
2$:	MOVE.B	D0,(A0)+		;version 2.0: 1=alt cat wrapped to nxt dsk
	MOVE.L	DATE,(A0)+		;date of backup
	MOVE.L	TIME,(A0)+		;time of backup
	MOVE.L	A0,FillPtr
	MOVE.L	#IDSize,FillCnt		;number of bytes in id block
	RTS

* Fills buffer with D0 nulls.

FillWithNulls:
	DECL	D0
	ADD.L	D0,FillCnt
	ZAP	D1
	MOVE.L	FillPtr,A0		;point to place to store
1$:	MOVE.B	D1,(A0)+
	DBF	D0,1$
	RTS

* Store longword in D0 in buffer.

PutLong:
	PUSH	D0
	SWAP	D0
	BSR.S	PutWord
	POP	D0

* Store word in D0 in buffer.

PutWord:
	PUSH	D0
	SWAB	D0
	BSR.S	PutChar
	POP	D0
;	BSR.S	PutChar
;	RTS

* Stores char in D0 into buffer, and checks for buffer full.  Flushes the
* buffer, if so.

PutChar:
	MOVE.L	FillPtr,A1
	MOVE.B	D0,(A1)+		;store byte
	MOVE.L	A1,FillPtr		;update pointer
	INCL	FillCnt			;and count it
	IFLTL	FillCnt,TrkBufSize,1$	;not full yet
	BSR	QueueFullBuf
1$:	RTS

GetLong:
	BSR.S	GetWord
	RTSERR
	SWAP	D0
	PUSH	D0
	BSR.S	GetWord
	POP	D1
	RTSERR
	MOVE.W	D0,D1
	EXG	D0,D1
	RTS

GetWord:
	BSR.S	GetChar			;get a byte
	RTSERR
	SWAB	D0
	PUSH	D0
	BSR.S	GetChar			;get another
	POP	D1
	RTSERR
	MOVE.B	D0,D1
	EXG	D0,D1
	RTS

* Returns next char from buffer in D0.  If buffer empty, reads in next
* buffer, which also implies a disk change, if necessary.

GetChar:
	IFNZL	ReadCnt,1$		;there still is some
	BSR	ReadNxtTrk		;else get more
	RTSERR				;oops...problems
1$:	MOVE.L	ReadPtr,A0
	MOVE.B	(A0)+,D0		;get next char
	MOVE.L	A0,ReadPtr
	DECL	ReadCnt			;one less char in buffer
9$:	RTS

QueueFullBuf:
	MOVE.L	FillBuf,A0		;else queue the full buffer
	LEA	FBufQueue,A1
	BSR	QueueBuf		;add buffer to full queue
	INCW	FBCylCnt		;count the cylinder
	BSR	FBExit			;and wait 'till a buffer is free
	RTS

* Queues a write to the active floppy, if no IO active and buffer ready to go.
* If floppy isn't ready, warns the operator.  Uses first buffer on full queue.
* (But doesn't unqueue it.)

StartWrite:
	IFNZB	IOActive,10$,L		;IO active...don't try to start
	LEA	FBufQueue,A1		;full buffer queue
	MOVE.L	(A1),ActiveBuffer
	BEQ	9$			;no buffer ready to write
	IFNZL	ActiveParams,1$		;already have a drive active
	BSR	UpdateDisk
	BSR	SelectNextDrive		;else get another drive
	ERROR	10$			;selected drive not yet ready
	BSR	UpdateDisk		;display new disk number
1$:	MOVE.L	ActiveParams,A1
	MOVE.B	DevStatus(A1),D0
	BEQ	10$			;drive selected, but not ready
	IFNEIB	D0,#2,2$		;no track check needed
	BSR	CheckDiskFmt		;check track 0 for format
	ERROR	10$			;oops...op bailed out
2$:	BSR	UpdateStatusBar		;show progress
	MOVEQ	#3,D0
	MOVE.B	D0,WriteRetry		;write retry count
	BSR	WriteTrack
	BSR	StatusActive		;this drive now running
	BRA.S	10$
9$:	IFZB	InpCompFlg,10$		;input not done yet
	SETF	OutCompFlg
10$:	RTS

* Issues write I/O command to driver for ActiveBuffer.

WriteTrack:
	MOVE.W	#CMD_WRITE,D0
	IFZB	FlopFlg,3$		;not floppy, use write command
	MOVE.W	#TD_FORMAT,D0		;use format cmd for floppies
3$:	MOVE.L	ActiveParams,A1		;active drive io parameters
	MOVE.L	DevIOB(A1),A1
	MOVE.W	D0,io_Command(A1)
	MOVE.L	ActiveBuffer,io_Data(A1) ;ptr to track data
	MOVE.L	TrkBufSize,D0
	MOVE.L	D0,io_Length(A1)
	MULU	ActiveCyl,D0		;where this write takes place
	ADD.L	VolumeOffset,D0		;correct for possible partition
	MOVE.L	D0,io_Offset(A1)
	CALLSYS	SendIO,SysBase	
	SETF	IOActive		;show IO in process
	RTS

CheckWriteComp:
	IFZB	IOActive,9$,L		;io not active
	MOVE.L	ActiveParams,A2		;check if IO has finished yet
	MOVE.L	DevIOB(A2),A1
	CALLSYS	CheckIO,SysBase
	IFZL	D0,9$,L			;IO not yet complete
	CLR.B	IOActive		;IO not active now
	MOVE.L	DevIOB(A2),A1
	CALLSYS	Remove
	MOVE.L	DevIOB(A2),A1		;IOB ptr
	IFZB	io_Error(A1),1$		;no errors...keep on truckin'
	BSR	WriteError		;check out the problem...
	RTS				;exit without freeing buffer...retry
1$:	IFZB	ChkFlg,2$		;don't check the write
	BSR	ReadAfterWrite		;else read the track just written
	NOERROR	2$			;all okay
	DECB	WriteRetry		;try again?
	BEQ.S	3$			;no...reached retry limit
	BSR	WriteTrack		;else retry the write
	RTS				;wait for retry to complete

3$:	BSR	ShutDevDown
	LEA	BDSK1.,A0
	MOVE.L	#BDSK2.,D0
	MOVE.L	#BDSK3.,D1
	BSR	BldWarningMsg		;put unit number into msg
	BSR	ReqRQTxt		;display requester
	RTSERR				;op elected to bail out...abortflg set
	BSR	ResetDisk		;reset to beginning of active disk
	RTS

2$:	LEA	FBufQueue,A1		;pop active buffer off full queue
	BSR	UnQueueBuf
	MOVE.L	D0,A0
	LEA	EBufQueue,A1
	BSR	QueueBuf		;queue buffer to free list
	IFNZB	AbortFlg,7$		;aborting...bail out
	INCW	ActiveCyl		;bump to next cyl
	IFLTW	ActiveCyl,MaxWrites,9$	;not end of disk yet...
	BSR	MotorOff
	BSR	StatusNotRdy
	BEEP
	INCW	ActiveDsk		;next diskette also
	INCB	FillDisk
	CLR.W	FBCylCnt
	MOVE.W	ActiveDsk,D0
	IFZB	TwoDevFlg,4$
	INCW	D0			;using both...this drive gets next+1
4$:	IFLTW	SelVols,D0,7$		;far enough...don't load next
	BSR	DskMsgLoadNext		;else ask op to load next disk
7$:	BSR	DskMsgRemove		;disk complete...remove this one
8$:	CLR.L	ActiveParams		;no drive currently active
9$:	RTS

* Reads track just written to make sure it is readable.  If not, asks op
* for another disk.

ReadAfterWrite:
	MOVE.W	ActiveCyl,D0
	BSR	ReadTrack		;read the track just written
	ERROR	9$			;oops...cant read that sucker
	BSR	CompareTracks		;compare the data just in case...
	BEQ.S	9$			;all is well...go on
	STC
9$:	RTS

* Selects next (first) floppy for transfer, based on current floppy and 
* floppy selection flags.  Sets ActiveParams, ActiveDev, and ActiveCyl.

SelectNextDrive:
	IFNZB	TwoDevFlg,1$		;using both...switch to other
	IFZB	Dev0SelFlg,3$		;not Dev0...must be Dev1
	BRA.S	4$			;else use Dev0
1$:	EORI.W	#1,ActiveDev		;switch to other drive
	BEQ.S	4$			;now use Dev0
3$:	MOVEQ	#1,D0			;Dev1
	LEA	Dev1Params,A1
	BRA.S	2$
4$:	ZAP	D0			;Dev0
	LEA	Dev0Params,A1
2$:	MOVE.W	D0,ActiveDev
	MOVE.L	A1,ActiveParams
	CLR.W	ActiveCyl
	IFNZB	DevStatus(A1),9$	;already ready...don't ask
	BEEP
	STC
9$:	RTS

* Check out write errors.

WriteError:
	MOVE.B	io_Error(A1),D0		;get the error code
	PUSH	D0
	BSR	ShutDevDown		;now shut down the drive
	POP	D0
	IFNEIB	D0,#TDERR_WriteProt,1$
	LEA	WrtPrt1.,A0
	MOVE.L	#WrtPrt2.,D0
	MOVE.L	#WrtPrt3.,D1
	BSR	BldWarningMsg
	BRA.S	ReqRQTxt

1$:	LEA	MiscEr1.,A0
	MOVE.L	#MiscEr2.,D0
	MOVE.L	#MiscEr3.,D1
	BSR	BldWarningMsg
	BSR.S	ReqRQTxt
	RTSERR				;oops op wants out
	BSR	ResetDisk		;else reset and try again
	CLC
	RTS

ReqRQTxt:
	LEA	RQText,A0
ReqCom:	MOVE.L	A0,REQTXT
	LEA	ReqCancel,A0
	MOVE.L	A0,REQGAD
Requester:
	BSR	OpenRequester
	ERROR	8$
	BSR	WaitRequester
	ERROR	8$
	PUSH	D0
	BSR	CloseRequester
	POP	D0
	CLC
	RTS
8$:	SETF	AbortFlg
	STC
	RTS

* Loads 3 text string pointers into requester structure.  String 1 ptr in
* A0, string 2 ptr in D0, string 3 ptr in D1.  Loads unit number of active 
* drive into a message pointed to by A0, D0, & D1.
* Drive unit goes into text at position marked by '@'.
* This code assumes that ITEXT tables in GADGETS.ASM are adjacent.

;BldWarningMsg:
;	PUSH	A0			;save ptr to first line text
;	LEA	RQTxt2+it_IText,A0
;	MOVE.L	D1,(A0)			;ptr to 3rd text line
;	MOVEQ	#it_SIZEOF,D1
;	SUB.L	D1,A0
;	MOVE.L	D0,(A0)			;ptr to 2nd text line
;	SUB.L	D1,A0
;	LEA	RQTxtBuf.,A1
;	MOVE.L	A1,(A0)			;ptr to 1st text line buffer
;	MOVE.	Warning.,A1		;'WARNING - Volume in drive '
;	DECL	A1			;tricky...A1 left from MOVE.
;	MOVE.L	ActiveParams,A0
;	MOVE.L	DevNamePtr(A0),A0	;point to name string
;	BSR.S	MoveDevice		;append device name to warning
;	POP	A0
;	APPEND.	A0,RQTxtBuf.		;now add rest of text
;	RTS

* Loads 3 text string pointers into requester structure.  String 1 ptr in
* A0, string 2 ptr in D0, string 3 ptr in D1.  Loads unit number of active 
* device into a message pointed to by A0, D0, & D1.
* Device unit goes into text at position marked by '@'.
* At point marked by '%' insert string in TMP2.

BldWarningMsg:
	PUSH	D1
	PUSH	D0
	PUSH	A0
	MOVE.L	ActiveParams,A0
	MOVE.L	DevNamePtr(A0),A0	;point to name string
	MOVE.	A0,TMP.
	POP	A0
	LEA	RQTxtBuf.,A1
	BSR	MoveInsertDevice
	POP	A0
	LEA	RQTxtBf1.,A1
	BSR	MoveInsertDevice
	POP	A0
	LEA	RQTxtBf2.,A1
	CLR.B	(A1)
	IFZL	A0,9$
	BSR	MoveInsertDevice
9$:	RTS

* Compares contents of two track buffers looking for read errors.

CompareTracks:
	MOVE.L	ReadBuffer,A0
	MOVE.L	ActiveBuffer,A1
	MOVE.L	TrkBufSize,D0
	DECL	D0
1$:	CMP.B	(A0)+,(A1)+
	DBNE	D0,1$
	RTS

* Reset conditions to what they were at the start of this disk when a bad 
* disk is found.

ResetDisk:
	BSR	CloseFile		;close the current file now
	BSR	StackReset		;reset things for FillBuffer
	CLR.B	InpCompFlg		;input not complete	
	CLR.B	ErrFillFlg
	CLR.W	ActiveCyl		;start over on new disk
	MOVE.W	SavActiveFile,ActiveFile
;;	BSR	UnLockDir
	IFNEW	ActiveDsk,#1,1$		;not resetting on first disk
;;	BSR	ChgOrigDir		;reset to original
	SETF	CatFlg			;else show in catalog mode
	BSR	InitFileDir		;get ready to scan the file catalog
	BRA	6$
1$:	MOVE.	SavPath.,Path.		;restore the original path
	MOVE.L	SavCurLevel,A2		;restore rest of conditions
	MOVE.L	A2,CurLevel
;;	BSR	CurrentPath		;make the saved path current and lock it
	BSR	RestLevelFib		;restore CurFib at each level
	MOVE.L	SavCurFib,A1
	MOVE.L	A1,CurFib
	MOVE.L	A1,dl_CurFib(A2)
	MOVE.L	df_Size(A1),ByteCount	;number of bytes of this file
	LEA	df_Name(A1),A5		;save name ptr
	BSR	UpdateFile		;and update the screen
;;	MOVE.L	A5,D1
	MOVE.	Path.,TMP.
	MOVE.L	A5,A0
	APPEND.	A0,TMP.
	MOVE.L	#TMP.,D1
	MOVE.L	#EXISTING,D2
	CALLSYS	Open,DosBase		;try to open that sucker
	MOVE.L	D0,FHandle		;good open?
	BEQ.S	4$			;no...cant recover...bail out
2$:	MOVE.L	ByteCount,D3		;this many we need
	SUB.L	SavByteCount,D3		;this many were left
	BLE.S	6$			;okay...now at right spot
	MOVE.L	TrkBufSize,D4		;else read more
	IFLEL	D3,D4,3$		;buffer can hold it all
	MOVE.L	D4,D3			;else read only what the buffer holds
3$:	MOVE.L	FHandle,D1		;read from this file
	MOVE.L	ReadBuffer,D2		;read data into special buffer
	CALLSYS	Read,DosBase		;fill the buffer
	TST.L	D0			;what happened?
	BGT.S	5$			;good result
4$:	BSR	CloseFile		;read or open error...
	DispErr	RecovErr.		;can't recover...AFU...bail out
	SETF	AbortFlg
	STC
	RTS
5$:	SUB.L	D0,ByteCount		;add what we read to what we have
	BRA.S	2$			;and check if that's enuf
6$:	MOVE.W	ActiveDsk,D0		;reset FillDisk
	MOVE.B	D0,FillDisk
	BSR	EmptyFullBuffers
	MOVE.W	ActiveDsk,D0
	BSR	DskMsgLoadNext		;ask op to put in another
	CLC
	RTS

* These two routines save and restore the CurFib at each active level at
* disk change and reset.

SaveLevelFib:
	LEA	RootLevel,A0
1$:	MOVE.L	dl_CurFib(A0),A1
	IFZL	A1,9$			;just in case...
	MOVE.L	A1,dl_GNFib(A0)
	BTST	#7,df_Flags(A1)		;file (bottom of tree)?
	BEQ.S	9$			;yes...stop now
	MOVE.L	df_SubLevel(A1),A0	;else down another level
	BRA.S	1$
9$:	RTS

RestLevelFib:
	LEA	RootLevel,A0
1$:	MOVE.L	dl_GNFib(A0),A1
	IFZL	A1,9$			;just in case...
	MOVE.L	A1,dl_CurFib(A0)
	BTST	#7,df_Flags(A1)		;file (bottom of tree)?
	BEQ.S	9$			;yes...stop now
	MOVE.L	df_SubLevel(A1),A0	;else down another level
	BRA.S	1$
9$:	RTS

* Checks disk just loaded for ADOS identifier.  If so, warn the op.

CheckDiskFmt:
	ZAP	D0			;force cylinder 0
	BSR	ReadTrack		;read the disk in active drive
	ERROR	9$			;cant read it...just use it
	MOVE.L	ReadBuffer,A0
	MOVE.L	(A0),D0			;get first 4 of disk
;	IFEQIL	D0,#'NDOS',9$		;aha! one of ours!?????
;	IFEQIL	D0,#'QBak',9$
	AND.L	#$FFFFFF00,D0
	MOVE.B	#32,D0
	IFNEIL	D0,<#'DOS '>,9$		;not ADOS disk...use it
	IFNZB	FmtFlg,9$		;don't care if ADOS format
	BSR	ShutDevDown		;shut this drive down
	LEA	ADOS1.,A0
	MOVE.L	#ADOS2.,D0
	MOVE.L	#ADOS3.,D1
	BSR	BldWarningMsg
	BSR	ReqRQTxt		;warn the op
	RTSERR				;bail out...aborting
	MOVE.L	ActiveParams,A1		;op hit proceed
	IFZB	DevChgFlg(A1),9$	;disk not changed...use it anyway
	CLR.B	DevChgFlg(A1)
	BRA.S	CheckDiskFmt		;else check out the new disk
9$:	MOVE.L	ActiveParams,A1
	MOVE.B	#1,DevStatus(A1)	;don't need trk 0 check now
	CLC				;else op hit 'proceed'
	RTS

* Turn off active drive when trouble is found

ShutDevDown:
	BSR	MotorOff		;turn off motor
	BSR	StatusNotRdy		;shut that sucker down
;;	MOVE.L	ActiveParams,A1
;;	CLR.B	DevStatus(A1)		;not ready now
	RTS

* Software interrupt on disk removed or inserted.  Pointer to params in A1.

DskChgInt:
	SETF	DevChgFlg(A1)		;show something has happened
	MOVE.L	SigBits,D0
	MOVE.L	QB_TASK,A1
	CALLSYS	Signal,SysBase		;wake us up...
	RTS

DiskChanged:
	PUSH	A2
	IFZB	Dev0SelFlg,1$		;drive not in use
	LEA	Dev0Params,A2		;check it out
	BSR.S	DiskX
1$:	IFZB	Dev1SelFlg,9$		;drive not in use
	LEA	Dev1Params,A2
	BSR.S	DiskX
9$:	POP	A2
	RTS

DiskX:	IFZB	DevChgFlg(A2),9$	;no change
	CLR.B	DevChgFlg(A2)		;clear flag
;	IFNZB	DevStatus(A2),9$	;already ready...don't process
	MOVE.L	ActiveParams,-(A7)	;save active params
	MOVE.L	A2,ActiveParams
	IFZB	FlopFlg,3$		;not a floppy...just proceed
	MOVE.L	#TD_CHANGESTATE,D0
	MOVE.L	DevIOB(A2),A1
	BSR.S	DiskIO			;check it out...disk loaded?
	ERROR	2$
	IFZL	D0,3$			;got a disk
	BSR	StatusNotRdy		;no disk yet
	BRA.S	2$
3$:	IFEQIB	DevStatus(A2),#3,4$	;don't change status if already active
	BSR	StatusReady		;drive now ready
	MOVE.B	#2,DevStatus(A2)	;needs trk 0 check
	BSR	DskLMsgClear
2$:	BSR	DskRMsgClear
4$:	MOVE.L	(A7)+,ActiveParams	;restore active params
9$:	RTS

* Miscellaneous synchronous IO.  IOblock in A1.  Command in D0.

DiskIO:
;	MOVE.L	A7,DebugSP		;just in case...
	PUSH	A1			;preserve ptr to IOB
	MOVE.W	D0,io_Command(A1)
	CALLSYS	DoIO,SysBase
	POP	A1
;	CLR.L	DebugSP
	IFZL	D0,1$
	MOVE.B	io_Error(A1),D0
	STC
	RTS
1$:	MOVE.L	io_Actual(A1),D0	;status, if any, returned in D0
	RTS

* Close file indentified by FHandle and zap handle.

CloseFile:
	MOVE.L	FHandle,D1
	BEQ.S	1$			;just in case...
	IFEQIL	D1,#1,1$		;in case of "test" mode
	CALLSYS	Close,DosBase		;else close this one...
1$:	CLR.L	FHandle
	RTS

* Packet type (ACTION_CODE) in D0, ProcessID in A0.

PacketIO:
	MOVE.L	D0,PktType		;save packet type
	MOVE.L	QB_TASK,A4
	LEA	pr_MsgPort(A4),A4	;point to our own port
	MOVE.L	A4,PktPort		;reload reply port
	LEA	Packet,A1
	MOVE.L	#DosPkt,ln_Name(A1)
	CALLSYS	PutMsg,SysBase		;send the packet
	MOVE.L	A4,A0
	CALLSYS	WaitPort		;wait for a reply
	LEA	Packet,A1
	CALLSYS	Remove			;dequeue the packet
	MOVE.L	PktRes1,D0		;response in D0
	RTS

* Buffer queue routines.  Enter with selected queue in A1.

* Add buffer address in A0 to end of queue A1.

QueueBuf:
1$:	IFNZL	(A1)+,1$	;find end of list
	CLR.L	(A1)		;make sure last entry is followed by 0
	MOVE.L	A0,-(A1)	;enter this item in list
	RTS

* Remove top item of queue in A1, if any, and return in D0.  Move everything
* else up in the queue.

UnQueueBuf:	
	MOVE.L	A1,A0
	MOVE.L	(A1)+,D0	;get top item, if any
	BEQ.S	9$		;empty...
1$:	MOVE.L	(A1)+,(A0)+	;move up one item
	IFNZL	(A0),1$		;loop if that wasn't the terminator
9$:	RTS

EmptyFullBuffers:
7$:	LEA	FBufQueue,A1
	BSR.S	UnQueueBuf		;empty the buffers
	IFZL	D0,8$			;thats all
	MOVE.L	D0,A0
	LEA	EBufQueue,A1
	BSR.S	QueueBuf
	BRA.S	7$
8$:	RTS

UpdateFile:
	MOVE.	File.,TMP.
	STR.	W,ActiveFile,TMP2.,#32,#5
	BSR	StripAppend
	APPEND.	OutOf.,TMP.
	STR.	W,SelFiles,TMP2.,#32,#5
	BSR	StripAppend
	ACHAR.	#SPACE,TMP.
	DispMsg	#FNbr_LE,#FNbr_TE,TMP.,WHITE,BLACK
	BSR	EraseFileName
	MOVE.	Path.,TMP.
	MOVE.L	CurFib,A0
	APPEND.	df_Name(A0),TMP.
	BRA.S	DispFileName

EraseFileName:
	STRING.	#SPACE,#62,TMP.		;erase old name
;	BSR.S	DispFileName

* Display name in TMP. on filename line

DispFileName:
	LEA	TMP.,A0
	CLR.B	60(A0)			;truncate to avoid screwing up screen
	DispMsg	#FNam_LE,#FNam_TE,TMP.,ORANGE,BLACK
	RTS

UpdateDisk:
	MOVE.	Disk.,TMP.
	STR.	W,ActiveDsk,TMP2.,#32,#3
	BSR	StripAppend
	IFNZB	Mode,1$			;no "out of" for restore
	APPEND.	OutOf.,TMP.
	STR.	W,SelVols,TMP2.,#32,#3
	BSR	StripAppend
1$:	DispMsg	#DNbr_LE,#DNbr_TE,TMP.,WHITE,BLACK
	RTS
	
* Volume changed gadget handlers

Dev0Change:
	IFZB	Dev0SelFlg,ProcMsgLoop
	SETF	Dev0ChgFlg
	BRA.S	ProcMsgLoop
Dev1Change:
	IFZB	Dev1SelFlg,ProcMsgLoop
	SETF	Dev1ChgFlg
	BRA.S	ProcMsgLoop

COMPLETE:
	SETF	CompFlg
	BRA.S	ProcMsgLoop

* Process IDCMP inputs during backup/restore

ProcessMsg:
	PUSH	A2-A5
ProcMsgLoop:
1$:	MOVE.L	WINDOW,A5
	MOVE.L	wd_UserPort(A5),A0	;get port
	CALLSYS	GetMsg,SysBase		;get any pending msg
	IFZL	D0,ProcEx,L		;no msg
	MOVE.L	D0,A1
	MOVE.L	im_Class(A1),MSG_CLASS	;SAVE MSG CLASS
	MOVE.W	im_Code(A1),MSG_CODE
	MOVE.L	im_IAddress(A1),GAD_PTR	;SAVE PTR TO POSSIBLE GADGET
	CALLSYS	ReplyMsg		;LET INTUITION KNOW WE HAVE IT
	MOVE.L	MSG_CLASS,D0
	CMP.L	#CLOSEWINDOW,D0		;TERMINATE PROGRAM?
	BEQ.S	ABORT			;EXIT PROGRAM IMMEDIATELY
	CMP.L	#REFRESHWINDOW,D0	;REFRESH REQUEST?
	BNE.S	2$			;yes
	BSR	REFRESH
	BRA.S	1$
2$:	IFEQIB	D0,#GADGETDOWN,3$
	IFNEIB	D0,#GADGETUP,4$		;not gadget...maybe menu?
3$:	MOVE.L	GAD_PTR,A0		;ptr to gadget
	MOVE.L	gg_UserData(A0),A6	;proper routine for this gadget
	JMP	(A6)			;and away we go...

4$:	IFNEIW	D0,#MENUPICK,1$,L	;not menu...ignore it
	BSR	MenuPk			;else process menu
	BRA	1$

ABORT:	SETF	AbortFlg
ProcEx:	POP	A2-A5
	RTS

RefreshArc:
	BSR	CLRWIN
	DispCent #320,#CMsg_TE,GdBakArc,ORANGE
	GadColor AbortGad,ORANGE
	RTS

RefreshBExit:
	BSR	RefreshStatus
	GadColor CompGad,BLACK
BackupExitMsg:
	BSR	EraseFileName		;get rid of last file name
	DispCent #320,#CMsg_TE,GoodBak,ORANGE
	RTS

RefreshRExit:
	BSR	RefreshStatus
	GadColor CompGad,BLACK
RestoreExitMsg:
	BSR	EraseFileName		;get rid of last file name
	LEA	GoodRst,A0
	IFZB	TestFlg,1$
	LEA	GoodTst,A0
1$:	DispCent #320,#CMsg_TE,A0,ORANGE
	RTS

RefreshAExit:
	BSR	RefreshStatus
	GadColor CompGad,BLACK
AbortExitMsg:
	BSR	EraseFileName		;get rid of last file name
	DispCent #320,#AMsg_TE,AbortMsg,ORANGE
	RTS

RefreshStatus:
	BSR	CLRWIN
	LEA	STATUS_BOXES,A0
	BSR	DRAW_BOXES		;put up the status boxes
	DispMsg	#Stat_LE,#Stat_TE,Status
	MOVE.	StBack.,TMP.
	IFZB	Mode,2$
	MOVE.	StRest.,TMP.
	IFZB	TestFlg,2$
	MOVE.	StTest.,TMP.
	BRA.S	6$
2$:	APPEND.	BRDevPathBuf,TMP.
6$:	DispMsg	#SSum_LE,#SSum_TE,TMP.,WHITE,BLACK
	IFNZB	FlopFlg,5$
	GadColor Chg0Gad,ORANGE
	GadColor Chg1Gad,ORANGE
5$:	GadColor AbortGad,ORANGE
	IFZW	ActiveFile,3$
	BSR	UpdateFile
3$:	IFZW	ActiveDsk,4$
	BSR	UpdateDisk
4$:	DispMsg	#Dev0Tl_LE,#Dev0Tl_TE,Dev0StatMsg
	IFZB	Dev1SelFlg,DisplayDevStatus
	DispMsg	#Dev1Tl_LE,#Dev1Tl_TE,Dev1StatMsg

DisplayDevStatus:
	MOVE.L	ActiveParams,-(A7)
	MOVE.L	#Dev1Params,ActiveParams
	BSR.S	DriveStatus
	MOVE.L	#Dev0Params,ActiveParams
	BSR.S	DriveStatus
	MOVE.L	(A7)+,ActiveParams
	RTS

DriveStatus:
	MOVE.L	ActiveParams,A2
	IFNZB	DevSelFlg(A2),1$
	BRA.S	StatusNotSel
1$:	MOVE.B	DevMsg(A2),D0
	IFZB	Mode,2$			;backup always displays first
	IFEQIB	D0,#1,3$		;restore load disk 1
2$:	BSR	DskMsgLoadNext
3$:	MOVE.B	DevStatus(A2),D0
	BEQ.S	StatusNotRdy
	IFEQIB	D0,#3,StatusActive
	BRA.S	StatusReady

StatusNotSel:
	DispMsg	#Dev1Tl_LE,#Dev1Tl_TE,DevNS
	RTS

StatusNotRdy:
	ZAP	D0
	LEA	DevNRdy,A0
	BRA.S	StatusCom

StatusReady:
	MOVEQ	#1,D0
	LEA	DevRdy,A0
	BRA.S	StatusCom

StatusActive:
	MOVE.L	ActiveParams,A2
	IFNEIB	DevStatus(A2),#3,1$
	RTS
1$:	CLRBOX	DevBarLE(A2),DevBarTE(A2),#DevBar_WD,#DevBar_HT,ORANGE
	MOVEQ	#3,D0
	LEA	DevAct,A0
StatusCom:
	MOVE.L	ActiveParams,A2
	MOVE.B	D0,DevStatus(A2)
	DispMsg	DevStLE(A2),DevStTE(A2),A0,ORANGE,BLACK
	RTS

DskMsgRemove:
	BSR.S	DskRMsgClear
	LEA	PlsRem,A0
;	MOVE.L	ActiveParams,A2
	DispMsg	DevRMsgLE(A2),DevRMsgTE(A2),A0,ORANGE,BLACK
	RTS

DskRMsgClear:
	CLRBOX	DevBarLE(A2),DevBarTE(A2),#DevBar_WD,#DevBar_HT,BLACK
;	STRING.	#SPACE,#23,TMP.
;	LEA	TMP.,A0
	RTS

* Number of disk to load in D0.

DskMsgLoadNext:
	MOVE.L	ActiveParams,A2
	MOVE.B	D0,DevMsg(A2)
	BEQ.S	DskLMsgClear
	STR.	W,D0,TMP2.,#32,#2
	STRIP_LB. TMP2.
	LEA	PlsLoad,A0
	LEA	TMP.,A1
	BSR	MoveInsertDevice
	LEA	TMP.,A0
	BRA.S	DskLCom

DskLMsgClear:
	MOVE.L	ActiveParams,A2
	CLR.B	DevMsg(A2)
	STRING.	#SPACE,#25,TMP.
	LEA	TMP.,A0
DskLCom:
	DispMsg	DevLMsgLE(A2),DevLMsgTE(A2),A0,ORANGE,BLACK
	RTS

* Reads next track (ActiveCyl) of current disk, or switches disks.
* Returns CY=1 on error.

ReadNxtTrk:
	INCW	ActiveCyl		;bump to next
	IFLTW	ActiveCyl,MaxWrites,1$	;still have more on this disk
	INCW	ActiveDsk		;else switch to next disk
2$:	BSR	LoadNextFlop		;ask for next disk, read first track
	RTS				;CY properly set by LoadNextFlop
1$:
;	BSR	ReadTrackErr		;read that sucker...report errors
;	RTS

ReadTrackErr:
	MOVE.W	ActiveCyl,D0
	BSR.S	ReadTrack
	RTSNOERR
	DispErr	ReadErr.		;error reading track of active
	STC
	RTS

* Reads cylinder in D0 into ReadBuffer of drive identified by ActiveParams.
* This is synchronous IO.

ReadTrack:
	PUSH	D2
	MOVE.W	D0,D2
	CLR.B	BadTrkFlg
	MOVE.B	#2,RetryCount		;retry limit
1$:	MOVE.L	ActiveParams,A1
	MOVE.L	DevIOB(A1),A1		;IOBlock
	MOVE.L	TrkBufSize,D0
	MOVE.L	D0,io_Length(A1)
	MULU	D2,D0			;calc place on disk
	ADD.L	VolumeOffset,D0		;correct for possible partition
	MOVE.L	D0,io_Offset(A1)
	MOVE.L	ReadBuffer,io_Data(A1)	;use preassigned track buffer
	MOVE.L	#CMD_READ,D0
	BSR	DiskIO			;do the read
	NOERROR	8$
	DECB	RetryCount
	BNE.S	1$
	SETF	BadTrkFlg
	STC
	BRA.S	9$
8$:	MOVE.L	TrkBufSize,ReadCnt	;this many bytes to play with
	MOVE.L	ReadBuffer,ReadPtr	;start here working the data
9$:	POP	D2
	RTS

* Asks for next floppy, then validates it.  Returns CY=1 on abort.

LoadNextFlop:
	BSR	MotorOff		;turn off this drive
	IFNZB	CatLoadErrFlg,4$	;reading alt cat...		;2/14/90
	BSR	StatusNotRdy		;mark drive not ready
	IFEQIW	ActiveDsk,#1,4$		;don't update status if first time
	BSR	RestLookAhead		;look ahead for next drive
	BSR	DskMsgRemove		;remove last disk
4$:	BSR	SelectNextDrive		;switch drives, if using 2
	MOVE.L	ActiveParams,A1		;now check if next drive ready
	MOVE.B	DevStatus(A1),D0
	BEQ.S	2$			;drive not ready
1$:	CLR.B	NoSeqCheck
	BSR	ValidateDisk		;else is this proper volume?
	NOERROR	3$			;yes...let'er rip!
	BSR	StatusNotRdy		;and mark drive not ready
	BSR	WrongFormatOrSeqErr
	RTSERR				;op wants to bail out
	IFNEIB	D0,#2,2$		;proceed
	SETF	BadDskFlg		;else skip this disk
	RTS
2$:	BSR	NextFlopReq		;ask op to load proper flop
	NOERROR	1$			;let's see what we got
	RTS				;else op bailed out
3$:	IFNZB	CatLoadErrFlg,8$	;dont update status if alt cat	;2/14/90
	BSR	DskLMsgClear
	BSR	DskRMsgClear
;;	BSR	StatusReady		;show drive ready now...
	BSR	StatusActive		;show disk active
	BSR	UpdateDisk		;update disk number on screen 
8$:	CLR.B	BadDskFlg
	CLC
9$:	RTS

* During restore puts up msg for loading next disk, if not last.
* add code here for disk look-ahead

RestLookAhead:
	MOVE.W	ActiveDsk,D0
	IFZB	TwoDevFlg,1$		;not using two devices
	INCW	D0
1$:	IFLTW	TotVols,D0,2$
	BSR	DskMsgLoadNext
2$:	RTS

* Puts up requester asking op to load the first volume to be restored.
* Then validates volume.  CY=1 if not valid volume.  AbortFlg=1 on abort.

LoadFirst:
	CLR.B	NoSeqCheck
	MOVE.L	#Dev0Params,ActiveParams
1$:	BEEP
	BSR	NextFlopReq		;ask for catalog volume
	RTSERR				;op bailed out (AbortFlg set)
	MOVE.B	CatLoadErrFlg,NoSeqCheck ;on load err, take any volume
	BSR	ValidateDisk		;check for proper id
	NOERROR	3$			;found one of our disks...
2$:	BSR	WrongFormatOrSeqErr
	RTSERR				;op wants to abort (AbortFlg set)
	IFNEIB	D0,#2,1$		;op wants to proceed (retry)
	STC				;op wants to skip (switch to alt cat)
	RTS
3$:	IFZB	CatLoadErrFlg,5$	;not trying to find alt cat
	MOVE.W	DiskID,D0
	IFEQIW	D0,#'QB',4$		;normal id, scan for cat
	MOVE.W	DiskID+2,D0
	IFNEIW	D0,#'c2',2$		;oops...should never happen
	IFZB	AltCatFlg,5$		;alt cat starts at trk 0
	DECW	ActiveDsk		;alt cat started on previous disk
	BSR	MotorOff		;turn off motor for last disk	;2/14/90
	BRA	1$			;now ask for it
4$:	INCW	ActiveCyl		;scan fwd for cat on this disk
	IFGEW	ActiveCyl,MaxWrites,6$	;end of disk, can't find alt cat
	BSR	ValidateTrk		;check for proper id on this track
	ERROR	4$			;no id on this track...
	IFNEIW	DiskID+2,#'c2',4$	;just in case...
5$:	CLC				;okay...all set to read cat
	RTS
6$:	BSR	MotorOff		;turn off motor, just in case
	DispErr	CantFindAltCat		;can't find cat on this disk
	MOVE.W	#1,ActiveDsk		;reset to first disk...
	BRA	1$			;request last disk again


NextFlopReq:
	MOVE.L	ActiveParams,A1
	CLR.B	DevChgFlg(A1)
	IFNEIW	ActiveDsk,#1,1$		;not disk 1
	LEA	LoadL1.,A0
	MOVE.L	#LoadL2.,D0
;	MOVE.L	#LoadL3.,D1
	ZAP	D1
	IFNZB	CatLoadErrFlg,2$	;load last, not first
1$:	STR.	W,ActiveDsk,TMP2.,#SPACE,#3
	STRIP_LB. TMP2.
	LEA	LoadN1.,A0
	MOVE.L	#LoadN2.,D0
;	MOVE.L	#LoadN3.,D1
	ZAP	D1
2$:	BSR	BldWarningMsg
;;	BSR	ReqRQTxt
	LEA	RQText,A0
	MOVE.L	A0,REQTXT
	LEA	ReqCancel,A0
	MOVE.L	A0,REQGAD
	BSR	OpenRequester
	ERROR	8$
3$:	BSR	BuildSigBits
4$:	MOVE.L	SigBits,D0
	CALLSYS	Wait,SysBase		;wait until operator acts
	MOVE.L	#TD_CHANGESTATE,D0
	MOVE.L	ActiveParams,A1
	IFZB	DevChgFlg(A1),5$	;no disk change yet
	CLR.B	DevChgFlg(A1)
	MOVE.L	DevIOB(A1),A1
	BSR	DiskIO			;check it out...disk loaded?
	ERROR	5$
	IFZL	D0,9$			;got a disk...check it out
5$:	MOVEA.L	WINDOW,A0
	MOVE.L	wd_UserPort(A0),A0
	CALLSYS	GetMsg,SysBase		;msg ready?
	IFZL	D0,4$			;no
	BSR	ProcessReqMsg		;yes...check it out
	ERROR	8$			;cancel
	IFZL	D0,4$			;ignore it...wait
9$:	BSR	CloseRequester		;else close requester
	CLC
	RTS
8$:	SETF	AbortFlg
	STC
	RTS

* Checks for Quarterback disk with valid sequence number.  CY=1 if not.
* Error codes: 0=read error, 1=QB id check failed, 2=sequence error,
* 3=date/time stamp error.

ValidateDisk:
	CLR.W	ActiveCyl		;force to cyl 0
ValidateTrk:
	CLR.B	ValErrCode		;error 0=read error on trk 0
	BSR	ReadTrackErr		;read it
	RTSERR				;problems reading track
	MOVE.B	#1,ValErrCode		;error code=not Quarterback disk
	BSR	GetLong			;get disk id
	MOVE.L	D0,DiskID		;save id for possible later check
	MOVE.W	DiskID,D0
	IFEQIW	D0,#'QB',1$		;old version
;	IFEQIL	D0,#'QBv2',1$		;current version
	IFEQIW	D0,#'ND',1$		;REAL OLD VERSION
;	IFNEIL	D0,#'QBc2',9$
	BRA.S	9$			;oops...not one of ours
1$:	MOVE.B	#2,ValErrCode		;error code=QB sequence error
	ZAP	D0
	BSR	GetChar			;if alt cat, skip the sequence number
	MOVE.B	D0,ValDisk		;save the number we found
	IFZB	NoSeqCheck,5$		;DO check the seq number
	MOVE.W	D0,ActiveDsk		;else this is our active disk now...
	BRA.S	4$			;don't perform the sequence check
5$:	IFNEW	ActiveDsk,D0,9$		;this is not the disk we want

4$:	BSR	GetChar			;spare byte...alt cat flag
	MOVE.B	D0,AltCatFlg		;save for LoadFirst
	IFNZB	FirstDskFlg,2$		;first disk...load date and time
	MOVE.B	#3,ValErrCode		;error code=QB date/time error
	BSR	GetLong			;backup date
	IFNEW	RDATE,D0,9$		;not from same set
	BSR	GetLong			;backup time
	IFNEW	RTIME,D0,9$		;not from same set
	BRA.S	3$
2$:	BSR	GetLong			;get date of backup
	MOVE.W	D0,RDATE
	BSR	GetLong			;get time of backup
	MOVE.W	D0,RTIME
3$:	CLR.B	ValErrCode
	RTS
9$:	STC
	RTS

* Uses result from ValidateDisk ValErrCode to warn operator
* that (0) error reading trk 0, (1) disk during restore is not QB 
* format, (2) not the desired volume sequence number, or 
* (3) not from the same backup set.
* Returns gadget code in D0, CY=1 on cancel.

WrongFormatOrSeqErr:
	BSR	MotorOff
	MOVE.B	ValErrCode,D0
	IFNZB	D0,3$			;read error on disk?
	LEA	RdEr1.,A0		;yes...unable to validate
	MOVE.L	#RdEr2.,D0
	MOVE.L	#RdEr3.,D1
	IFZB	FirstDskFlg,5$,L	;not first
	LEA	CatEr1.,A0		;yes...unable to read catalog
	MOVE.L	#CatEr2.,D0
	MOVE.L	#CatEr3.,D1
	BRA.S	5$
3$:	IFNEIB	D0,#1,1$		;QB format error?
	LEA	NotQB1.,A0		;yes...not a QB disk
	MOVE.L	#NotQB2.,D0
	MOVE.L	#NotQB3.,D1
	BRA.S	5$
1$:	IFNEIB	D0,#2,2$		;sequence number wrong?
	STR.	B,ValDisk,TMP2.,#32,#2	;yes...tell him what he loaded
	STRIP_LB. TMP2.
	LEA	BSEQ1.,A0
	MOVE.L	#BSEQ2.,D0
	MOVE.L	#BSEQ3.,D1
	BRA.S	5$
2$:	IFNEIB	D0,#3,9$		;not from the same backup set?
	LEA	BSET1.,A0		;yes
	MOVE.L	#BSET2.,D0
	MOVE.L	#BSET3.,D1
5$:	BSR	BldWarningMsg		;build message text
	MOVE.L	#RQText,REQTXT		;text goes into requester
	LEA	ReqSkip,A0
	MOVE.L	A0,REQGAD		;INIT GADGET LIST
	BSR	Requester		;put up requester..code in D0
9$:	RTS

* Updates progress status bar based on value of ActiveCyl and MaxWrites.

UpdateStatusBar:
	PUSH	D2
	MOVE.L	ActiveParams,A1		;active drive io parameters
	MOVE.W	ActiveCyl,D2
	IFNZB	FlopFlg,1$		;don't do math if floppy
	MULU	#160,D2			;else use percentage calc
	DIVU	MaxWrites,D2
1$:	DispWBar DevBarLE(A1),DevBarTE(A1),D2,WINDOW
	POP	D2
	RTS

* Builds signal bits for use by WAIT from the window user port and the two
* backup/restore device msg ports.

BuildSigBits:
	CLR.L	SigBits
	MOVE.L	WINDOW,A0
	MOVE.L	wd_UserPort(A0),A0	;ptr to IDCMP msg port
	BSR.S	GetPortSigBit
	MOVE.L	Dev0Port,A0
	BSR.S	GetPortSigBit
	MOVE.L	Dev1Port,A0
GetPortSigBit:
	IFZL	A0,9$			;NO port
	ZAP	D0
	MOVE.B	MP_SIGBIT(A0),D1	;signal bit number=number to shift
	BSET	D1,D0
	OR.L	D0,SigBits
9$:	RTS

* Turns selected drive motor off.

MotorOff:
;;	IFZB	FlopFlg,9$		;not floppy device
	MOVEQ	#CMD_UPDATE,D0
	BSR.S	8$
	MOVEQ	#CMD_CLEAR,D0
	BSR.S	8$
	MOVEQ	#TD_MOTOR,D0		;turn off the motor
	BSR.S	8$
	MOVEQ	#TD_EJECT,D0
8$:	MOVE.L	ActiveParams,A1		;active drive io parameters
	IFZL	A1,9$
	MOVE.L	DevIOB(A1),A1
	MOVE.W	D0,io_Command(A1)
	ZAP	D0
	MOVE.L	D0,io_Length(A1)
	CALLSYS	DoIO,SysBase
9$:	RTS

* Allocate as many track buffers as we can, up to NBufs limit.
* If operator selects slow mode, allocate only one.

AllocateBuffers:
	MOVE.L	ReadBuffer,A1
	IFNZL	A1,4$
	MOVEQ	#NBufs-1,D5		;try to get this many
	IFZB	SlowFlg,1$		;not slow mode
	ZAP	D5			;else force one buffer
1$:	MOVE.L	TrkBufSize,D0		;try to allocate track buffers
	MOVE.L	BufMemType,D1
	CALLSYS	AllocMem,SysBase
	IFZL	D0,2$
	IFNZL	ReadBuffer,3$		;use first for read operations
	MOVE.L	D0,ReadBuffer
	BRA.S	1$
3$:	LEA	EBufQueue,A1
	MOVE.L	D0,A0
	BSR	QueueBuf
	DBF	D5,1$
	RTS
2$:	STC
4$:	RTS

* Free the allocated track buffers on either queue.

FreeBuffers:
	MOVE.L	ReadBuffer,A1
	IFZL	A1,1$
	BSR.S	FreeBuf
	CLR.L	ReadBuffer		;read buffer free
1$:	LEA	FBufQueue,A5
	BSR.S	FreeQ
	LEA	EBufQueue,A5
FreeQ:	MOVE.L	A5,A1
	BSR	UnQueueBuf
	IFZL	D0,9$
	MOVE.L	D0,A1
	BSR.S	FreeBuf
	BRA.S	FreeQ
9$:	RTS

FreeBuf:
	MOVE.L	TrkBufSize,D0
	CALLSYS	FreeMem,SysBase
	RTS

NODE    MACRO
        DC.L    0       ;SUCC
        DC.L    0       ;PRED
        DC.B    \1      ;TYPE
        DC.B    \2      ;PRI
        DC.L    \3      ;NAME POINTER
        ENDM

WrtErrMsgTbl
	DC.L	FileInUse.
	DC.L	ERROR_OBJECT_IN_USE
	DC.L	DeviceFull.
	DC.L	ERROR_DISK_FULL
	DC.L	FileDelPrt.
	DC.L	ERROR_DELETE_PROTECTED
	DC.L	FileWrtPrt.
	DC.L	ERROR_WRITE_PROTECTED
	DC.L	FileRdPrt.
	DC.L	ERROR_READ_PROTECTED
	DC.L	WrtErrCode.		;default error
	DC.L	0			;end of table

EBufQueue	DCB.L	NBufs+1,0	;Empty buffer queue
FBufQueue	DCB.L	NBufs+1,0	;Full buffer queue

Dev0Params
Dev0Port	DC.L	0		;Dev0's msg port
Dev0_IOB	DC.L	0		;Dev0's ioblock 1
Dev0Proc	DC.L	0		;"id" of handler process
Dev0Vec		DC.L	0		;saved int vector
Dev0NamPtr	DC.L	BRDevBuf1	;ptr to device name
Dev0StLE	DC.W	Dev0St_LE	;msg location
Dev0StTE	DC.W	Dev0St_TE
Dev0RMsgLE	DC.W	Dev0RMsg_LE	;remove msg location
Dev0RMsgTE	DC.W	Dev0RMsg_TE
Dev0LMsgLE	DC.W	Dev0LMsg_LE	;load msg location
Dev0LMsgTE	DC.W	Dev0LMsg_TE
Dev0BarLE	DC.W	Dev0Bar_LE
Dev0BarTE	DC.W	Dev0Bar_TE
Dev0Flags	DC.L	0		;flags from mountlist
Dev0Unit	DC.W	0		;unit number
Dev0SelFlg	DC.B	0		;1=drive selected
Dev0Status	DC.B	0		;1=Dev rdy, 2=needs trk0 chk, 3=active
Dev0ChgFlg	DC.B	0		;1=disk has been changed
Dev0Msg		DC.B	0		;message number, if any

	CNOP	0,2

Dev1Params
Dev1Port	DC.L	0		;Dev1's msg port
Dev1_IOB	DC.L	0		;Dev1's ioblock 1
Dev1Proc	DC.L	0		;"id" of handler process
Dev1Vec		DC.L	0		;saved int vector
Dev1NamPtr	DC.L	BRDevBuf2	;ptr to device name
Dev1StLE	DC.W	Dev1St_LE	;msg location
Dev1StTE	DC.W	Dev1St_TE
Dev1RMsgLE	DC.W	Dev1RMsg_LE	;remove msg location
Dev1RMsgTE	DC.W	Dev1RMsg_TE
Dev1LMsgLE	DC.W	Dev1LMsg_LE	;load msg location
Dev1LMsgTE	DC.W	Dev1LMsg_TE
Dev1BarLE	DC.W	Dev1Bar_LE
Dev1BarTE	DC.W	Dev1Bar_TE
Dev1Flags	DC.L	0		;flags from mountlist
Dev1Unit	DC.W	1		;unit number
Dev1SelFlg	DC.B	0		;1=drive selected
Dev1Status	DC.B	0		;1=Dev rdy, 2=needs trk0 chk, 3=active
Dev1ChgFlg	DC.B	0		;1=disk has been changed
Dev1Msg		DC.B	0		;message number, if any

	CNOP	0,4

Dev0IntS	NODE	2,0,0
		DC.L	Dev0Params
		DC.L	DskChgInt

Dev1IntS	NODE	2,0,0
		DC.L	Dev1Params
		DC.L	DskChgInt

	CNOP	0,4

Packet
PktMsg	NODE	5,0,DosPkt
	DC.L	0
	DC.W	PMsgSize
DosPkt	DC.L	PktMsg
PktPort	DC.L	0
PktType	DC.L	0
PktRes1	DC.L	0
PktRes2	DC.L	0
PktArg1	DC.L	0
PktArg2	DC.L	0
PktArg3	DC.L	0
PktArg4	DC.L	0,0,0,0
PMsgSize EQU	*-PktMsg

ActiveParams	DC.L	0		;ptr to params of active drive
ReadBuffer	DC.L	0		;track buffer used during read ops
ReadPtr		DC.L	0		;ptr to read buffer
ReadCnt		DC.L	0		;read buffer byte count
ActiveBuffer	DC.L	0		;ptr to buffer being read/written
SigBits		DC.L	0		;signal bits for Wait
BSTR_TMP.	DC.L	0		;BSTR ptr to TMP.
LocalStackBase	DC.L	0		;base of local stack
LocalStackPtr	DC.L	0		;saved local stack ptr
DiskID		DC.L	0		;disk ID as found by Validate
BlocksPerVolume	DC.L	0		;max usable blocks per volume
VolumeSize	DC.L	0		;size of backup volume in bytes
TrkBufSize	DC.L	0		;size of cylinder buffer in bytes
FBCylCnt	DC.W	0		;fill buffer cylinder count
MaxWrites	DC.W	0		;max cylinder
SaveSP		DC.L	0		;saved main SP
FillBuf		DC.L	0		;ptr to buffer being filled
FillPtr		DC.L	0		;FillBuffer data ptr
FillCnt		DC.L	0		;fill buffer byte count
FHandle		DC.L	0		;ADos File handle
CurFib		DC.L	0		;ptr to current FIB block
ByteCount	DC.L	0		;count of bytes in current file
;DebugSP	DC.L	0		;saved sp for debugging
WrtErrNbr	DC.L	0		;IoErr code on write error
DiskNbr		DC.L	0		;used as string for ASCII disk nbr
DATESTAMP
DATE		DC.L	0		;days since Jan 1, 1978
TIME		DC.L	0		;minutes since midnight
TICKS		DC.L	0		;ticks of current minute
RDATE		DC.W	0		;restore's date/time from backup disks
RTIME		DC.W	0

;saved params for bad disk recovery during write
SavCurLevel	DC.L	0		;saved subdir level
SavCurFib	DC.L	0		;saved file block of file crossing
SavByteCount	DC.L	0		;saved ByteCount or 0
SavActiveFile	DC.W	0		;saved file number of backup
ActiveCyl	DC.W	0		;cylinder for current read/write
ActiveDev	DC.W	0		;0=Dev0, 1=Dev1 (LEAVE AS WORD)
ActiveDsk	DC.W	0		;number of volume being processed
ActiveFile	DC.W	0		;number of file being processed
NewDsk		DC.W	0		;skip to new disk number
NewCyl		DC.W	0		;new cylinder
NewOffset	DC.L	0		;offset in new cyl
ValDisk		DC.B	0		;disk number found during validation
FillDisk	DC.B	0		;number of disk used by FillBuffer
IOActive	DC.B	0		;1=waiting for IO to complete
InpCompFlg	DC.B	0		;1=no more data from source
OutCompFlg	DC.B	0		;1=last write now completed
AbortFlg	DC.B	0		;1=Operator wants to quit
CompFlg		DC.B	0		;1=return to Main
CatFlg		DC.B	0		;1=writing catalog
WriteRetry	DC.B	0		;read-after-write retry count
RetryCount	DC.B	0		;readtrack retry count
FirstDskFlg	DC.B	0		;1=first restore disk (set by readdir)
ErrFillFlg	DC.B	0		;1=fill file on backup read error
BadTrkFlg	DC.B	0		;1=bad track on last read
BadDskFlg	DC.B	0		;1=skip entire disk on restore
AltCatFlg	DC.B	0		;1=alt cat starts on prior disk
NoSeqCheck	DC.B	0		;1=don't perform sequence check
ValErrCode	DC.B	0		;1=format error, 2=sequence error
AltAbtFlg	DC.B	0		;1=abort flag saved for arc bits/rpt
RQTxtBuf.	DS.B	RQTxtBufSize	;buffer for requester text
RQTxtBf1.	DS.B	RQTxtBufSize	;buffer for requester text
RQTxtBf2.	DS.B	RQTxtBufSize	;buffer for requester text
SavPath.	DS.B	PathSize	;saved path

	END

