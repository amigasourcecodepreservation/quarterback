;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*****************************************************************
*								*
*			Device.asm				*
*								*
*	Quarterback backup/restore device open/close		*
*								*
* This code first checks for DF0-DF3, then checks ADOS device	*
* list for matching entry.  If the device is found, then device	*
* parameters are extracted, disk sizes calculated, and driver	*
* information preserved.					*
*								*
*****************************************************************

	INCLUDE "vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"
	INCLUDE	"I/IO.I"

	XDEF	CheckBRDevice,CheckDFn
	XDEF	OpenBRDevice,CloseBRDevice
	XDEF	CFAbort
	XDEF	BldDevGadList

	XDEF	DevErrFlg,TwoDevFlg,FlopFlg
	XDEF	Dev0StatMsg,Dev1StatMsg
	XDEF	DF0.,DF1.
	XDEF	VolumeOffset
	XDEF	VolumeCnt
	XDEF	BufMemType

	XREF	PacketIO,DiskIO
	XREF	AllocateBuffers,FreeBuffers
	XREF	_CreatePort,_DeletePort,_CreateExtIO,_DeleteExtIO
	XREF	MoveInsertDevice

	XREF	SysBase,GraphicsBase,DosBase
	XREF	BRDevBuf1,BRDevBuf2,TMP.,TMP2.
	XREF	NoDev.,SameDev.
	XREF	MixedDev.,NoDFn.,NotFSD.
	XREF	DevOpnErr.,Status.,NoMemory
	XREF	MaxWrites,VolumeSize,BlocksPerVolume,TrkBufSize
	XREF	Mode
	XREF	Dev0Params,Dev1Params
	XREF	Dev0IntS,Dev1IntS
	XREF	PktArg1
	XREF	Dev0SelFlg,Dev1SelFlg,Dev0_IOB,Dev1_IOB
	XREF	SavePC
	XREF	DevGList,DevGadTbl
	XREF	DevGad,BRDevGad1

DR_GetUnitID	EQU	-30	;not the proper way to do this...
ACTION_INHIBIT	EQU	31
OldIntVec	EQU	$12A	;offset into trackdisk data for int vec
NewIntVec	EQU	$134	;offset into trackdisk data for int vec
TDVerOffset	EQU	$1C	;offset to trackdisk version
NumBRGads	EQU	10	;max number of backup/restore gadgets
FloppySize	EQU	901120	;capacity of floppy in bytes

* Offsets used to find backup/restore device parameters from device list

dl_Root		EQU	$22
rn_Info		EQU	$18
di_DevInfo	EQU	4
dn_Next		EQU	0
dn_Type		EQU	4
dn_Startup	EQU	$1C
dn_Name		EQU	$28
fssm_Unit	EQU	0
fssm_Device	EQU	4
fssm_Environ	EQU	8
fssm_Flags	EQU	$C
de_SizeBlock	EQU	4
de_NumHeads	EQU	$C
de_BlksPerTrack	EQU	$14
de_Reserved	EQU	$18
de_LowCyl	EQU	$24
de_HighCyl	EQU	$28
de_BufMemType	EQU	$30

AFIX	MACRO	;reg - converts BPTR to APTR
	ADD.L	\1,\1
	ADD.L	\1,\1
	ENDM

* Mult 16-bit <ea> (param1) times 32-bit reg (param2), result to param2.
* Param3 is working reg (destroyed).

LONGMUL	MACRO	;16-bit <ea>, 32-bit reg, working reg.
	MOVE.L	\2,\3
	SWAP	\3
	MULU	\1,\3
	SWAP	\3
	MULU	\1,\2
	ADD.L	\3,\2
	ENDM

* Checks to make sure that there is a valid backup/restore device, calculates
* params.  Device strings assumed to be already defined.  Returns CY=1 on
* device failure.  Sets DevErrFlg to device causing problem (1 or 2).

CheckBRDevice:
	BSR	FreeBuffers		;get rid of any possible buffers
	CLR.B	DevErrFlg		;start with no error
	CLR.B	FlopFlg			;no floppies yet
;	CLR.B	NonFlopFlg		;nothing else
	CLR.B	TwoDevFlg
	STRIP_LB. BRDevBuf1		;just in case...
	CLR.B	Dev0SelFlg
	CLR.B	Dev0StatMsg
	CLR.B	GotOneDev
	LEN.	BRDevBuf1		;got any first device?
	IFZB	D0,1$,L			;need at least 1 valid device
	LEA	Dev0Params,A5		;work the first drive table
	BSR	CheckDevice		;see what we have here
	ERROR	2$,L			;invalid first device
	SETF	GotOneDev		;got one valid device
	MOVE.	TMP.,Dev0StatMsg	;set up device name in status msg
	CLR.B	Dev1SelFlg
	CLR.B	Dev1StatMsg
;	BSR	CalcVolSize
	STRIP_LB. BRDevBuf2
	LEN.	BRDevBuf2
	IFZB	D0,9$			;only 1 device
	IFEQ.	BRDevBuf1,BRDevBuf2,4$	;oops...same device for both
	LEA	Dev1Params,A5
	BSR	CheckDevice		;else see what it is
	ERROR	3$			;invalid second device
	MOVE.	TMP.,Dev1StatMsg
	SETF	TwoDevFlg		;show two devices in use
9$:	BSR	CalcVolSize		;figure max disk params
	RTS
1$:	DispErr	NoDev.			;gotta have at least one device
2$:	MOVEQ	#1,D0
	BRA.S	8$
4$:	DispErr	SameDev.
3$:	MOVEQ	#2,D0
8$:	MOVE.B	D0,DevErrFlg		;error code 1=dev0, 2=dev1
	STC
	RTS

* Checks device in TMP. for standard floppy, then searches ADOS device list 
* for a valid entry.  Returns CY=1 if no device, or if device not valid.
* A5 points to device parameter table (part of Main.asm)

CheckDevice:
	MOVE.L	DevNamePtr(A5),A0
	MOVE.	A0,TMP.
	UCASE.	TMP.
;;	IFEQ.	DF0.,TMP.,1$		;check for standard floppy
;;	IFEQ.	DF1.,TMP.,1$
;;	IFEQ.	DF2.,TMP.,1$
;;	IFNE.	DF3.,TMP.,2$
;;1$:	SETF	FlopFlg
;;2$:
	BSR	CheckDeviceList		;check for ADOS device
	RTSERR				;not a defined device...error
	SETF	DevSelFlg(A5)		;show drive selected
	LEA	TMP.,A0
3$:	MOVE.B	(A0)+,D0
	CMP.B	#':',D0
	BNE.S	3$
	CLR.B	-1(A0)			;zap the colon
	APPEND.	Status.,TMP.		;make into proper status msg
	BRA.S	9$
8$:	STC
9$:	RTS

* Checks ADOS device list for device in TMP., then, if first device, loads
* params from environment block.  If not first, compares params to make sure
* devices are same capacity.

CheckDeviceList:
	PUSH	A2			;just in case...
	MOVE.L	DosBase,A2		;start from Dos library structure
	MOVE.L	dl_Root(A2),A2		;Dos RootNode
	MOVE.L	rn_Info(A2),A2		;Info structure
	AFIX	A2
	MOVE.L	di_DevInfo(A2),D0	;BPTR to device list
1$:	BNE.S	2$
	APPEND.	NoDFn.,TMP.
	DispErr	TMP.			;oops...no drive
	BRA	8$
2$:	AFIX	D0			;make into APTR
	MOVE.L	D0,A2
	IFNZL	dn_Type(A2),3$		;not a device entry...
	MOVE.L	dn_Name(A2),A0		;BSTR ptr to device name
	LEA	TMP2.,A1
	BSR	MoveBstr		;Move device name into TMP2.
	ACHAR.	#':',TMP2.		;ADOS doesn't keep ":"
	UCASE.	TMP2.
	IFEQ.	TMP.,TMP2.,4$		;is this the one we want?
3$:	MOVE.L	dn_Next(A2),D0		;no...get link to next entry
	BRA.S	1$			;and try it
4$:;	MOVE.L	dn_Startup(A2),A2	;BPTR to FileSysStartupMsg
;	AFIX	A2
	MOVE.L	dn_Startup(A2),D0
	IFGTIL	D0,#100,7$		;must be file-structured
	APPEND.	NotFSD.,TMP.
	DispErr	TMP.
	BRA	8$
7$:	AFIX	D0
	MOVE.L	D0,A2
	MOVE.W	fssm_Unit+2(A2),DevUnit(A5) ;save unit for open
	MOVE.L	fssm_Device(A2),A0	;BSTR ptr to driver name
	LEA	DeviceName,A1		;goes here
	BSR	MoveBstr
	IFNE.	TrkDev.,DeviceName,10$
	SETF	FlopFlg
10$:	MOVE.L	fssm_Flags(A2),DevFlags(A5) ;for later OpenDevice
	MOVE.L	fssm_Environ(A2),A2	;BSTR ptr to device params
	AFIX	A2
	IFNZB	GotOneDev,5$		;already got one, verify
	MOVE.W	de_SizeBlock+2(A2),D0	;blocksize in longword
	ASL	#2,D0			;convert to bytes per block
	MOVE.W	D0,BlockSize
	MOVE.W	de_NumHeads+2(A2),Surfaces
	MOVE.W	de_BlksPerTrack+2(A2),BlocksPerTrack
	MOVE.W	de_Reserved+2(A2),Reserved
	MOVE.W	de_LowCyl+2(A2),LowCyl
	MOVE.W	de_HighCyl+2(A2),HighCyl
	MOVE.L	de_BufMemType(A2),BufMemType
	BRA.S	9$
5$:	MOVE.W	de_SizeBlock+2(A2),D0
	ASL	#2,D0
	CMP.W	BlockSize,D0
	BNE.S	6$
	MOVE.W	de_NumHeads+2(A2),D0
	CMP.W	Surfaces,D0
	BNE.S	6$
	MOVE.W	de_BlksPerTrack+2(A2),D0
	CMP.W	BlocksPerTrack,D0
	BNE.S	6$
	MOVE.W	de_Reserved+2(A2),D0
	CMP.W	Reserved,D0
	BNE.S	6$
	MOVE.W	de_LowCyl+2(A2),D0
	CMP.W	LowCyl,D0
	BNE.S	6$
	MOVE.W	de_HighCyl+2(A2),D0
	CMP.W	HighCyl,D0
	BEQ.S	9$
6$:	DispErr	MixedDev.		;can't mix floppies and others
8$:	STC
9$:	POP	A2
	RTS

* Called to build the backup/restore gadget list.  Returns CY=1 if no
* volumes found.  This routine tailors the gadget list to match the
* number of volumes found, up to the limit of 9.

BldDevGadList:
	PUSH	D2/A2-A4
	CLR.B	VolumeCnt		;no volumes found yet
	BSR	InitDeviceList		;set up to scan device list
	LEA	DevGadTbl,A3
;;	MOVE.W	D2,D0
1$:	MOVE.L	(A3)+,D0		;get entry from table
	BEQ.S	2$			;end of list
	MOVE.L	D0,A2
	CLR.L	(A2)			;zap the fwd link
	MOVE.L	gg_GadgetText(A2),A1	;init the gadget list
	MOVE.L	it_IText(A1),A1
	CLR.B	(A1)			;zap gadget text string
	BRA.S	1$
2$:	LEA	DevGad,A4
	LEA	DevGadTbl,A3
4$:	BSR	NextVolume		;get first/next valid volume
	ERROR	3$			;no more
	INCB	VolumeCnt		;count number of vols
	LEA	TMP2.,A1
	BSR	MoveBstr		;Move device name into TMP2.
	ACHAR.	#':',TMP2.		;ADOS doesn't keep ":"
	UCASE.	TMP2.
	MOVE.L	(A3)+,D0		;get entry from table
	BEQ.S	3$			;end of gadget list...no more allowed
	MOVE.L	D0,A2
	MOVE.L	A2,(A4)			;fix up fwd link from previous gadget
	MOVE.L	A2,A4
	MOVE.L	gg_GadgetText(A2),A1
	MOVE.L	it_IText(A1),A1		;get ptr to gadget text buffer
	MOVE.	TMP2.,A1		;move device name into gadget txt buf
	BRA.S	4$
3$:	MOVE.L	#BRDevGad1,D0
	MOVE.L	D0,(A4)			;final fwd link
	IFGEIB	VolumeCnt,#2,9$		;found at least 2 volumes
	MOVE.L	D0,DevGad		;else remove gadgets from list
9$:	POP	D2/A2-A4
	RTS

* Called to initialize ptr to device list.

InitDeviceList:
	MOVE.L	DosBase,A0		;start from Dos library structure
	MOVE.L	dl_Root(A0),A0		;Dos RootNode
	MOVE.L	rn_Info(A0),A0		;Info structure
	AFIX	A0
	MOVE.L	di_DevInfo(A0),D0	;BPTR to device list
	AFIX	D0			;make into APTR
	MOVE.L	D0,DevicePtr		;save ptr for use by NextMassDevice
	RTS

* Called to check out the next volume.  To qualify, a volume
* must have a capacity greater than a floppy disk.

NextVolume:
	PUSH	D2-D3/A2
	MOVE.L	DevicePtr,A2		;point to a list entry
1$:	IFNZL	A2,2$			;end of list
	STC
	BRA	8$
2$:	IFEQIL	dn_Type(A2),#2,4$	;got a volume
3$:	MOVE.L	dn_Next(A2),D0		;no...get link to next entry
	AFIX	D0
	MOVE.L	D0,A2
	BRA.S	1$			;and try it

4$:	MOVE.L	dn_Name(A2),A0		;BSTR to device name
	LEA	TMP2.,A1
	BSR	MoveBstr		;move into TMP2.
	ACHAR.	#':',TMP2.		;ADOS doesn't keep ":"
	MOVE.L	#TMP2.,D1
	MOVEQ	#ACCESS_READ,D2
	CALLSYS	Lock,DosBase
	MOVE.L	D0,D3			;lock to D1
	BEQ.S	3$			;no lock...go on to next entry
	MOVE.L	D3,D1
	MOVE.L	#InfoData,D2
	CALLSYS	Info			;get device info
	MOVE.L	D3,D1
	CALLSYS	UnLock
	MOVE.L	NumBlocks,D0
	MOVE.L	BytesPerBlock,D1
	LONGMUL	D1,D0,D2
	IFLEIL	D0,#FloppySize,3$	;too small for hard disk
	MOVE.L	dn_Name(A2),A0
	MOVE.L	0(A2),A2
	AFIX	A2
	MOVE.L	A2,DevicePtr
	CLC
8$:	POP	D2-D3/A2
	RTS
	
* Converts BSTR pointed to by A0 into normal stored at ptr in A1.

MoveBstr:
	CLR.B	(A1)			;zap dest string
	PUSH	A1			;dest string goes here
	AFIX	A0			;BPTR-->APTR
	ZAP	D0
	MOVE.B	(A0)+,D0		;get length of name
	BEQ.S	2$			;oops...no name?
	DECW	D0
1$:	MOVE.B	(A0)+,(A1)+
	DBF	D0,1$
	CLR.B	(A1)			;terminate name
2$:	POP	A0			;now clean up string
;	UCASE.	A0
	RTS

* Calculate QB device max parameters from data from mountlist

CalcVolSize:
	PUSH	D2
	MOVE.W	LowCyl,D0		;calc volume offset
	MULU	BlocksPerTrack,D0
	LONGMUL	Surfaces,D0,D2		;cylinder offset in blocks
	LONGMUL	BlockSize,D0,D2		;actual offset to proper place on disk
	MOVE.L	D0,VolumeOffset		;byte offset for partitions
	MOVE.W	HighCyl,D0		;calc number of cylinders
	SUB.W	LowCyl,D0
	INCW	D0			;account for 0 origin
	MULU	Surfaces,D0
	MOVE.W	D0,MaxWrites		;this is the max trk count
	MOVE.W	BlocksPerTrack,D1
	LONGMUL	D1,D0,D2		;gives total blocks per disk
	MOVE.L	D0,BlocksPerVolume
	LONGMUL	BlockSize,D0,D2
	MOVE.L	D0,VolumeSize		;available backup space in bytes
	MULU	BlockSize,D1
	MOVE.L	D1,TrkBufSize		;bytes per track
	POP	D2
	RTS

* Verify that floppy drive unit number (0-3) in D6 exists in this 
* hardware configuration.  CY=1 if not.

CheckDFn:
	IFNZL	DiskBase,1$
	LEA	DiskName,A1
	CALLSYS	OpenResource,SysBase	;get ptr to disk resource
	MOVE.L	D0,DiskBase
	BEQ.S	8$			;oops...can't open
1$:	MOVE.L	D6,D0			;unit number
	MOVE.L	DiskBase,A6
	JSR	DR_GetUnitID(A6)	;find out what kind it is
	IFZL	D0,9$			;okay...drive is available
8$:	STC
9$:	RTS

* Open either or both backup/restore devices, according to selection flags.
* Returns CY=1 on any failure.

OpenBRDevice:
	IFZB	Dev0SelFlg,1$		;not using Dev0
	IFNZL	Dev0_IOB,1$		;already have Dev0, don't do it again
	LEA	Dev0Params,A2
	BSR	InitDevice
	RTSERR
;	MOVE.L	Dev0_IOB,A1		;enable disk changed interrupts
	LEA	Dev0IntS,A0
	BSR	InitDiskInt

1$:	IFZB	Dev1SelFlg,9$		;not using Dev1
	IFNZL	Dev1_IOB,9$		;already have Dev1, don't do it again
	LEA	Dev1Params,A2
	BSR	InitDevice
	RTSERR
;	MOVE.L	Dev1_IOB,A1
	LEA	Dev1IntS,A0
	BSR	InitDiskInt
9$:	BSR	AllocateBuffers		;now allocate necessary buffers
	NOERROR	10$
	BSR	FreeBuffers		;free what we got, it may be needed
	DispErr	NoMemory
	STC
10$:	RTS

* Allocates a msg port and an IO block, then opens the device driver.
* Drive param table in A2.

InitDevice:
	MOVE.L	DevNamePtr(A2),D1
	CALLSYS	DeviceProc,DosBase
	MOVE.L	D0,DevProc(A2)
	BEQ.S	8$			;didn't get process id...can't open
	PEA	0
	PEA	0
	JSR	_CreatePort		;allocate msg port
	ADDQ.L	#8,SP
	MOVE.L	D0,DevPort(A2)
	BEQ.S	8$			;problems
	PEA	IOTD_SIZE
	MOVE.L	D0,-(SP)
	JSR	_CreateExtIO		;allocate and init IOB
	ADDQ.L	#8,SP
	MOVE.L	D0,DevIOB(A2)
	BEQ.S	8$			;problems
	MOVE.L	D0,A1
	ZAP	D0
	MOVE.W	DevUnit(A2),D0		;get unit ID
	LEA	DeviceName,A0
	MOVE.L	DevFlags(A2),D1		;device flags
	CALLSYS	OpenDevice,SysBase	;open it
	IFNZL	D0,8$			;bad open
	BSR	Inhibit			;make ADOS leave drive alone
	RTS
8$:	BSR	FreeDriveResources
	MOVE.L	DevNamePtr(A2),A0
	MOVE.	A0,TMP.
	LEA	DevOpnErr.,A0
	LEA	TMP2.,A1
	BSR	MoveInsertDevice
	DispErr	TMP2.
	STC
	RTS

* Closes either or both backup/restore devices, according to selection flags.

CloseBRDevice:
;	MOVE.L	Dev0_IOB,D0
;	ADD.L	Dev1_IOB,D0
;	BEQ.S	CFAbort			;no drives allocated...no msg
;	DispErr	RemAll.			;make sure all floppies removed
CFAbort:
	LEA	Dev0Params,A2		;try to close Dev0
	BSR.S	CloseDev
	LEA	Dev1Params,A2		;try to close Dev1
	BSR.S	CloseDev
	RTS

* Close and release a single floppy drive.  A2 points to param table.
* Note: drive may never have been allocated.

CloseDev:
	IFZB	DevSelFlg(A2),9$	;drive not in use
	IFZL	DevIOB(A2),1$		;drive already closed
;	MOVE.L	DevIOB(A2),A1
	BSR	RestDiskInt
	BSR	Enable			;let ADOS get at drive again
	MOVE.L	DevIOB(A2),A1
	CALLSYS	CloseDevice,SysBase
1$:	BSR.S	FreeDriveResources
9$:	RTS

FreeDriveResources:
	MOVE.L	DevIOB(A2),D0
	BEQ.S	1$
	MOVE.L	D0,-(SP)
	JSR	_DeleteExtIO
	ADDQ.L	#4,SP
	CLR.L	DevIOB(A2)
1$:	MOVE.L	DevPort(A2),D0
	BEQ.S	9$			;no port
	MOVE.L	D0,-(SP)
	JSR	_DeletePort
	ADDQ.L	#4,SP
	CLR.L	DevPort(A2)
9$:	RTS

* Keeps AmigaDOS from fooling around with the backup/restore device
* while we use it.

Enable:
	ZAP	D0
	BRA.S	IECom
Inhibit:
	MOVEQ	#1,D0
IECom:	MOVE.L	D0,PktArg1		;store inhibit/enable code
	MOVEQ	#ACTION_INHIBIT,D0	;packet type
	MOVE.L	DevProc(A2),A0		;process ID of handler
	BSR	PacketIO
	RTS

* Sets or clears disk change interrupts.
* This code was changed February, 1991, to use the formal workaround
* provided by Randall Jessup of Commodore to make QB compatible with
* all versions of Trackdisk.  The old code depends on knowledge of
* Trackdisk's private data area.

;;RestDiskInt:
;;	IFZB	FlopFlg,9$
;;	MOVE.L	DevVec(A2),A0
;;	IFZL	A0,9$
;;	BSR.S	DICom
;;9$:	RTS

;;InitDiskInt:
;;	IFZB	FlopFlg,9$
;;	PUSH	A0/A3
;;	MOVE.L	io_Unit(A1),A3
;;	MOVE.L	OldIntVec(A3),D0	;trackdisk for V1.2 or V1.3
;;	MOVE.L	ln_Name(A3),A0		;point to trackdisk.device identifier
;;	MOVE.W	TDVerOffset(A0),D1	;get version number of trackdisk
;;	IFLEIW	D1,#'34',1$		;older version
;;	MOVE.L	NewIntVec(A3),D0	;vector offset for V2.0
;;1$:	MOVE.L	D0,DevVec(A2)		;save old int vector
;;	POP	A0/A3
;;	BSR.S	DICom
;;9$:	RTS
	
;;DICom:	MOVE.L	A0,io_Data(A1)
;;	MOVE.L	#TD_REMOVE,D0
;;	BSR	DiskIO
;;	RTS

REMOVE	MACRO
	MOVE.L	A0,-(A7)
	MOVE.L	(A0),A1
	CMPA.W	#0,A1
	BEQ.S	*+12
	MOVE.L	4(A0),A0
	MOVE.L	A1,(A0)
	MOVE.L	A0,4(A1)
	MOVE.L	(A7)+,A0
	CLR.L	(A0)
	CLR.L	4(A0)
	ENDM

RestDiskInt:
	IFZB	FlopFlg,9$
	MOVE.L	DevIntIOB(A2),A0
	IFZL	A0,9$
	MOVE.L	A0,-(SP)
	CALLSYS	Forbid,SysBase
	REMOVE
	CALLSYS	Permit
	JSR	_DeleteExtIO
	ADDQ.L	#4,SP
	CLR.L	DevIntIOB(A2)
9$:	RTS

* Address of interrupt vector in A0, base of Device table in A2.
* Creates IOB and sends to Trackdisk.device.

InitDiskInt:
	IFZB	FlopFlg,9$
	PUSH	A3
	MOVE.L	A0,A3
	PEA	IOTD_SIZE
	MOVE.L	DevPort(A2),D0		;port
	MOVE.L	D0,-(SP)
	JSR	_CreateExtIO		;allocate and init IOB
	ADDQ.L	#8,SP
	MOVE.L	D0,DevIntIOB(A2)
	BEQ.S	8$			;problems
	MOVE.L	D0,A1
	MOVE.L	DevIOB(A2),A0
	MOVE.L	io_Device(A0),io_Device(A1)
	MOVE.L	io_Unit(A0),io_Unit(A1)
	MOVE.L	A3,io_Data(A1)		;point IOB to interrupt vector
	MOVEQ	#TD_ADDCHANGEINT,D0
	MOVE.W	D0,io_Command(A1)
	CALLSYS	SendIO,SysBase
8$:	POP	A3
9$:	RTS
	
TrkDev.		TEXTZ	'trackdisk.device'
DiskName	TEXTZ	'disk.resource'
DF0.		TEXTZ	'DF0:'
DF1.		TEXTZ	'DF1:'
;DF2.		TEXTZ	'DF2:'
;DF3.		TEXTZ	'DF3:'

	SECTION	MEM,BSS
	CNOP	0,4

InfoData
NumSoftErrors	DS.L	1
UnitNumber	DS.L	1
DiskState	DS.L	1
NumBlocks	DS.L	1
NumBlocksUsed	DS.L	1
BytesPerBlock	DS.L	1
DiskType	DS.L	1
VolumeNode	DS.L	1
InUse		DS.L	1

DiskBase	DS.L	1		;disk resource
VolumeOffset	DS.L	1		;offset from beginning of physical device
DevicePtr	DS.L	1		;ptr to current mass device
BufMemType	DS.L	1		;trackbuffer memory type
Surfaces	DS.W	1		;surfaces (heads)
BlockSize	DS.W	1		;size of block in longwords
BlocksPerTrack	DS.W	1		;11 for standard floppy
LowCyl		DS.W	1		;low cylinder range of device
HighCyl		DS.W	1		;high cylinder range of device
Reserved	DS.W	1		;reserved blocks
Dev0IntIOB	DS.L	1		;ptr to interrupt IOB
Dev1IntIOB	DS.L	1		;ptr to interrupt IOB
DevErrFlg	DS.B	1		;1=improper device
FlopFlg		DS.B	1		;1=first device is standard floppy
;NonFlopFlg	DS.B	1		;1=first device is non floppy device
TwoDevFlg	DS.B	1		;1=using two devices
GotOneDev	DS.B	1		;1=already found a valid device
VolumeCnt	DS.B	1		;count of volumes available to backup
DeviceName	DS.B	80
Dev0StatMsg	DS.B	15
Dev1StatMsg	DS.B	15

	END

