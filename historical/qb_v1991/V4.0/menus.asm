;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*							*
*		Quarterback menus			*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1987 Central Coast Software	*
*	    268 Bowie Dr, Los Osos, CA 93402		*
*	     All rights reserved, worldwide		*
*********************************************************


	INCLUDE	"vd0:MACROS.ASM"
	INCLUDE	"vd0:EQUATES.ASM"

	SECTION	MENUS,DATA

	XDEF	QB_MENU

	XREF	T80

* MENU MACROS

MENU	MACRO		;WID,HT,FLG,TEXT,ITM,0
	IFEQ	NARG-5
	DC.L	*+30
	ENDC
	IFEQ	NARG-6
	DC.L	0
	ENDC
	DC.W	MNUPOS,0,\1,\2,\3
	DC.L	\4,\5
	DC.W	0,0,0,0
MNUPOS	SET	MNUPOS+MNUWID
	ENDM

MNUITM	MACRO		;LE,WID,FLG,CHAR,TEXT,XOR,SUBITM,ROUTINE,0
	XREF	\8
	IFEQ	NARG-8
	DC.L	*+38
	ENDC
	IFEQ	NARG-9
	DC.L	0
	ENDC
	DC.W	\1,ITEMPOS,\2,MNUHT
	DC.W	\3
	DC.L	\6,\5,0
	DC.B	\4,0
	DC.L	\7
	DC.W	0
	DC.L	\8
ITEMPOS	SET	ITEMPOS+MNUHT
	ENDM

ITEXT	MACRO	;FPEN,BPEN,MODE,RLE,RTE,FONT,STG,NXT
	DC.B	\1,\2,\3,0	;FPEN, BPEN, MODE, FILL
	DC.W	\4,\5		;REL LE, REL TE
	DC.L	\6,\7,\8	;FONT PTR, STG PTR, NXT PTR
	ENDM

MTXT	MACRO	;STG
	ITEXT	0,1,JAM2,1,1,T80,IT\@,0
IT\@	DC.B	\1,0
	CNOP	0,2
	ENDM

PTEXTZ	MACRO
	DC.B	\1,0
	CNOP	0,2
	ENDM

* MENU DEFINITIONS

MNUWID	EQU	100
MNUHT	EQU	12
MNUPOS	SET	0
IF_NORM	EQU	$52
IF_COMS	EQU	$56
IF_CHECK EQU	$5B

QB_MENU		;LE,TE,WID,HT,FLG,TEXT,ITM,0

	MENU	MNUWID,0,1,PROJ_TXT,PROJ_ITEMS,0

* MENU ITEM DEFINITIONS

		;LE,WID,FLG,CHAR,TEXT,XOR,SUBITM,ROUTINE,0

ITEMPOS	SET	0

PROJ_ITEMS
	MNUITM	0,152,IF_NORM,0,ABOUT_TXT,0,0,AboutQB
	MNUITM	0,152,IF_NORM,0,PRINT_TXT,0,0,PrintCat
	MNUITM	0,152,IF_NORM,0,PARAM_TXT,0,0,SaveParams
	MNUITM	0,152,IF_NORM,0,CLI_TXT,0,0,OpnNewCli
	MNUITM	0,152,IF_NORM,0,QUIT_TXT,0,0,QuitQB,0

ITEMPOS	SET	0

* TEXT STRINGS USED IN MENUS

PROJ_TXT	PTEXTZ	<'PROJECT '>

ABOUT_TXT	MTXT	<'About Quarterback'>
PARAM_TXT	MTXT	<'Save Options     '>
PRINT_TXT	MTXT	<'Print Catalog    '>
CLI_TXT		MTXT	<'Open New CLI     '>
QUIT_TXT	MTXT	<'Exit Quarterback '>


	END
