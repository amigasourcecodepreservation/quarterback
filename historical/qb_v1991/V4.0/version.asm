;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
	XDEF	PGM_NAME
	XDEF	About0.,About1.,About2.,About3.,About4.,About5.

PGM_NAME
	DC.B	'Quarterback v4.3',0

About0.	DC.B	'Quarterback V4.3, February 14, 1991',0
About1.	DC.B	'Copyright ',$A9,' 1991',0
About2.	DC.B	'Central Coast Software',0
About3.	DC.B	'424 Vista Avenue',0
About4.	DC.B	'Golden, CO 80401 USA',0
About5.	DC.B	'303-526-1030',0

	END

