;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*************************************************
*						*
* This patch performs error checks for GiveUnit,*
* which is part of DiskResource.  If the task	*
* issuing the GiveUnit does not control the	*
* disk hardware, the request is ignored.	*
*						*
*************************************************

	INCLUDE	"VD0:MACROS.ASM"

	XDEF	PatchGiveUnit,UnPatchGiveUnit
	XREF	SysBase

DEBUG	EQU	0

PatchGiveUnit:
	LEA	DiskName,A1
	CALLSYS	OpenResource,SysBase
	MOVE.L	D0,DRBase
	BEQ.S	1$
	MOVE.L	D0,A0
	MOVE.L	-22(A0),SaveVec
;	MOVE.W	#$4000,$DFF09A
	MOVE.L	#GiveUnit,-22(A0)
;	MOVE.W	#-$4000,$DFF09A
1$:	RTS

UnPatchGiveUnit:
	MOVE.L	DRBase,A0
	IFZL	A0,1$
	MOVE.L	SaveVec,-22(A0)
1$:	RTS

GiveUnit:
	MOVE.L	SysBase,A0
	MOVE.L	$114(A0),A0	;get this task ptr
	BTST	#7,$26(A6)	;is unit still busy?
	BEQ.S	1$		;NO...don't release it again!
	MOVE.L	$22(A6),A1	;disk block
	IFZL	A1,2$		;nobody current...bad awful
	MOVE.L	$E(A1),A1	;MSG PORT IN DISK BLOCK
	MOVE.L	$10(A1),A1	;task from disk block
	IFEQL	A0,A1,OKAY	;fine...same guy...let it go
	INCB	ErrCode3
	MOVE.L	A0,ErrTask
	RTS
2$:	INCB	ErrCode2
	RTS
1$:	INCB	ErrCode1
ErrCom:	RTS			;back to caller

OKAY:	MOVE.L	SaveVec,A0
	JMP	(A0)

SaveVec		DC.L	0
DRBase		DC.L	0
ErrTask		DC.L	0
ErrCode1	DC.B	0
ErrCode2	DC.B	0
ErrCode3	DC.B	0
DiskName	DC.B	'disk.resource',0

	END
