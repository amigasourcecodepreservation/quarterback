;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*							*
*	Quarterback macros				*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1987 Central Coast Software	*
*	    268 Bowie Dr, Los Osos, CA 93402		*
*	     All rights reserved, worldwide		*
*********************************************************


BEEP	MACRO
	XREF	BEEP..
	BSR	BEEP..
	ENDM

TEXTZ	MACRO	;NULL TERM STRING
	DC.B	\1,0
	ENDM

ONCALL	MACRO	;REG,LIM,ADDRESS LIST PTR
	XREF	FATAL_ERROR
	CMP.B	\2,\1
	BLS.S	*+8
	BSR	FATAL_ERROR
	ADD.W	\1,\1
	ADD.W	\1,\1
	MOVE.L	\3(\1.W),A6
	JSR	(A6)
	ENDM

ONGOTO	MACRO	;REG,LIM,ADDRESS LIST PTR
	XREF	FATAL_ERROR
	CMP.B	\2,\1
	BLS.S	*+8
	BSR	FATAL_ERROR
	ADD.W	\1,\1
	ADD.W	\1,\1
	MOVE.L	*+6(\1.W),A6
	JMP	(A6)
	ENDM

SETF	MACRO
	BSET	#0,\1
	ENDM

PUSH	MACRO
	MOVEM.L \1,-(A7)
	ENDM

POP	MACRO
	MOVEM.L (A7)+,\1
	ENDM

LCASE	MACRO	;reg
	CMP.B	#65,\1
	BCS.S	98$
	CMP.B	#90,\1
	BHI.S	98$
	ADD.B	#32,\1
98$:
	ENDM

UCASE	MACRO	;reg
	CMP.B	#97,\1
	BCS.S	99$
	CMP.B	#122,\1
	BHI.S	99$
	SUB.B	#32,\1
99$:
	ENDM

INCB	MACRO
	ADDQ.B #1,\1
	ENDM

DECB	MACRO
	SUBQ.B #1,\1
	ENDM

INCW	MACRO
	ADDQ.W #1,\1
	ENDM

DECW	MACRO
	SUBQ.W #1,\1
	ENDM

INCL	MACRO
	ADDQ.L #1,\1
	ENDM

DECL	MACRO
	SUBQ.L #1,\1
	ENDM

ZAP	MACRO
	MOVEQ	#0,\1
	ENDM

ZAPA	MACRO
	SUB.L	\1,\1
	ENDM

ZAPU	MACRO	;clears upper word of reg
	SWAP	\1
	CLR.W	\1
	SWAP	\1
	ENDM

STC	MACRO
	ORI.B	#1,CCR
	ENDM

CLC	MACRO
	CMP.B	D0,D0
	ENDM

SWAB	MACRO
	ROL.W	#8,\1
	ENDM

TSTAN	MACRO	;SIZE,<EA>
	IFC	'\2','A0'
	CMPA.\1	#0,\2
	MEXIT
	ENDC
	IFC	'\2','A1'
	CMPA.\1	#0,\2
	MEXIT
	ENDC
	IFC	'\2','A2'
	CMPA.\1	#0,\2
	MEXIT
	ENDC
	IFC	'\2','A3'
	CMPA.\1	#0,\2
	MEXIT
	ENDC
	IFC	'\2','A4'
	CMPA.\1	#0,\2
	MEXIT
	ENDC
	IFC	'\2','A5'
	CMPA.\1	#0,\2
	MEXIT
	ENDC
	IFC	'\2','A6'
	CMPA.\1	#0,\2
	MEXIT
	ENDC
	IFC	'\2','A7'
	CMPA.\1	#0,\2
	MEXIT
	ENDC
	TST.\1	\2
	ENDM


IFZB	MACRO
	TSTAN	B,\1
	IFEQ	NARG-2
	BEQ.S	\2
	ENDC
	IFNE	NARG-2
	BEQ	\2
	ENDC	
	ENDM

IFZW	MACRO
	TSTAN	W,\1
	IFEQ	NARG-2
	BEQ.S	\2
	ENDC
	IFNE	NARG-2
	BEQ	\2
	ENDC	
	ENDM

IFZL	MACRO
	TSTAN	L,\1
	IFEQ	NARG-2
	BEQ.S	\2
	ENDC
	IFNE	NARG-2
	BEQ	\2
	ENDC	
	ENDM

IFNZB	MACRO
	TSTAN	B,\1
	IFEQ	NARG-2
	BNE.S	\2
	ENDC
	IFNE	NARG-2
	BNE	\2
	ENDC	
	ENDM

IFNZW	MACRO
	TSTAN	W,\1
	IFEQ	NARG-2
	BNE.S	\2
	ENDC
	IFNE	NARG-2
	BNE	\2
	ENDC	
	ENDM

IFNZL	MACRO
	TSTAN	L,\1
	IFEQ	NARG-2
	BNE.S	\2
	ENDC
	IFNE	NARG-2
	BNE	\2
	ENDC	
	ENDM

* THE FOLLOWING MACRO IMPLEMENTS CMP <EA>,<EA> BY LOADING D0 WITH PARAM2
* IF IT IS NOT ONE OF THE DATA REGS (D0-D7), OR ADDRESS REGS (A0-A7).
* WHERE PARAM2 IS An, CMPA.L IS USED (SIZE PARAM IS IGNORED).

COMP	MACRO	;SIZE,<EA>,<EA>
	IFC	'\3','D0'
	CMP.\1	\2,\3
	MEXIT
	ENDC
	IFC	'\3','D1'
	CMP.\1	\2,\3
	MEXIT
	ENDC
	IFC	'\3','D2'
	CMP.\1	\2,\3
	MEXIT
	ENDC
	IFC	'\3','D3'
	CMP.\1	\2,\3
	MEXIT
	ENDC
	IFC	'\3','D4'
	CMP.\1	\2,\3
	MEXIT
	ENDC
	IFC	'\3','D5'
	CMP.\1	\2,\3
	MEXIT
	ENDC
	IFC	'\3','D6'
	CMP.\1	\2,\3
	MEXIT
	ENDC
	IFC	'\3','D7'
	CMP.\1	\2,\3
	MEXIT
	ENDC
	IFC	'\3','A0'
	CMPA.L	\2,\3
	MEXIT
	ENDC
	IFC	'\3','A1'
	CMPA.L	\2,\3
	MEXIT
	ENDC
	IFC	'\3','A2'
	CMPA.L	\2,\3
	MEXIT
	ENDC
	IFC	'\3','A3'
	CMPA.L	\2,\3
	MEXIT
	ENDC
	IFC	'\3','A4'
	CMPA.L	\2,\3
	MEXIT
	ENDC
	IFC	'\3','A5'
	CMPA.L	\2,\3
	MEXIT
	ENDC
	IFC	'\3','A6'
	CMPA.L	\2,\3
	MEXIT
	ENDC
	IFC	'\3','A7'
	CMPA.L	\2,\3
	MEXIT
	ENDC
	MOVE.\1	\3,D0
	CMP.\1	\2,D0
	ENDM

IFEQB	MACRO
	COMP	B,\1,\2
	IFEQ	NARG-3
	BEQ.S	\3
	ENDC
	IFNE	NARG-3
	BEQ	\3
	ENDC
	ENDM

IFEQW	MACRO
	COMP	W,\1,\2
	IFEQ	NARG-3
	BEQ.S	\3
	ENDC
	IFNE	NARG-3
	BEQ	\3
	ENDC
	ENDM

IFNEB	MACRO
	COMP	B,\1,\2
	IFEQ	NARG-3
	BNE.S	\3
	ENDC
	IFNE	NARG-3
	BNE	\3
	ENDC
	ENDM

IFNEW	MACRO
	COMP	W,\1,\2
	IFEQ	NARG-3
	BNE.S	\3
	ENDC
	IFNE	NARG-3
	BNE	\3
	ENDC
	ENDM

IFEQL	MACRO
	COMP	L,\1,\2
	IFEQ	NARG-3
	BEQ.S	\3
	ENDC
	IFNE	NARG-3
	BEQ	\3
	ENDC
	ENDM

IFNEL	MACRO
	COMP	L,\1,\2
	IFEQ	NARG-3
	BNE.S	\3
	ENDC
	IFNE	NARG-3
	BNE	\3
	ENDC
	ENDM

* SENSE OF COMPARISON FOR ALL FOLLOWING MACROS IS PARAM1 TO PARAM2 (UNSIGNED).
* FOR EXAMPLE, IFLT.B D0,D1 TAKES BRANCH IF D0 < D1

IFLTB	MACRO
	COMP	B,\1,\2
	IFEQ	NARG-3
	BHI.S	\3
	ENDC
	IFNE	NARG-3
	BHI	\3
	ENDC
	ENDM

IFLEB	MACRO
	COMP	B,\1,\2
	IFEQ	NARG-3
	BCC.S	\3
	ENDC
	IFNE	NARG-3
	BCC	\3
	ENDC
	ENDM

IFGEB	MACRO
	COMP	B,\1,\2
	IFEQ	NARG-3
	BLS.S	\3
	ENDC
	IFNE	NARG-3
	BLS	\3
	ENDC
	ENDM

IFGTB	MACRO
	COMP	B,\1,\2
	IFEQ	NARG-3
	BCS.S	\3
	ENDC
	IFNE	NARG-3
	BCS	\3
	ENDC
	ENDM

IFLTW	MACRO
	COMP	W,\1,\2
	IFEQ	NARG-3
	BHI.S	\3
	ENDC
	IFNE	NARG-3
	BHI	\3
	ENDC
	ENDM

IFLEW	MACRO
	COMP	W,\1,\2
	IFEQ	NARG-3
	BCC.S	\3
	ENDC
	IFNE	NARG-3
	BCC	\3
	ENDC
	ENDM

IFGEW	MACRO
	COMP	W,\1,\2
	IFEQ	NARG-3
	BLS.S	\3
	ENDC
	IFNE	NARG-3
	BLS	\3
	ENDC
	ENDM

IFGTW	MACRO
	COMP	W,\1,\2
	IFEQ	NARG-3
	BCS.S	\3
	ENDC
	IFNE	NARG-3
	BCS	\3
	ENDC
	ENDM

IFLTL	MACRO
	COMP	L,\1,\2
	IFEQ	NARG-3
	BHI.S	\3
	ENDC
	IFNE	NARG-3
	BHI	\3
	ENDC
	ENDM

IFLEL	MACRO
	COMP	L,\1,\2
	IFEQ	NARG-3
	BCC.S	\3
	ENDC
	IFNE	NARG-3
	BCC	\3
	ENDC
	ENDM

IFGEL	MACRO
	COMP	L,\1,\2
	IFEQ	NARG-3
	BLS.S	\3
	ENDC
	IFNE	NARG-3
	BLS	\3
	ENDC
	ENDM

IFGTL	MACRO
	COMP	L,\1,\2
	IFEQ	NARG-3
	BCS.S	\3
	ENDC
	IFNE	NARG-3
	BCS	\3
	ENDC
	ENDM

* THE FOLLOWING MACROS ARE FOR COMPARISON OF <EA> WITH AN IMMEDIATE VALUE.
* IN ORDER TO MAKE THE PARAMS MORE NATURAL, THE IMMEDIATE VALUE IS LISTED
* AS PARAM2, BUT IS ACTUALLY REVERSED INSIDE THE MACRO.

IFEQIB	MACRO
	CMP.B	\2,\1
	IFEQ	NARG-3
	BEQ.S	\3
	ENDC
	IFNE	NARG-3
	BEQ	\3
	ENDC
	ENDM

IFEQIW	MACRO
	CMP.W	\2,\1
	IFEQ	NARG-3
	BEQ.S	\3
	ENDC
	IFNE	NARG-3
	BEQ	\3
	ENDC
	ENDM

IFEQIL	MACRO
	CMP.L	\2,\1
	IFEQ	NARG-3
	BEQ.S	\3
	ENDC
	IFNE	NARG-3
	BEQ	\3
	ENDC
	ENDM

IFNEIB	MACRO
	CMP.B	\2,\1
	IFEQ	NARG-3
	BNE.S	\3
	ENDC
	IFNE	NARG-3
	BNE	\3
	ENDC
	ENDM

IFNEIW	MACRO
	CMP.W	\2,\1
	IFEQ	NARG-3
	BNE.S	\3
	ENDC
	IFNE	NARG-3
	BNE	\3
	ENDC
	ENDM

IFNEIL	MACRO
	CMP.L	\2,\1
	IFEQ	NARG-3
	BNE.S	\3
	ENDC
	IFNE	NARG-3
	BNE	\3
	ENDC
	ENDM

IFLTIB	MACRO
	CMP.B	\2,\1
	IFEQ	NARG-3
	BCS.S	\3
	ENDC
	IFNE	NARG-3
	BCS	\3
	ENDC
	ENDM

IFLEIB	MACRO
	CMP.B	\2,\1
	IFEQ	NARG-3
	BLS.S	\3
	ENDC
	IFNE	NARG-3
	BLS	\3
	ENDC
	ENDM

IFGEIB	MACRO
	CMP.B	\2,\1
	IFEQ	NARG-3
	BCC.S	\3
	ENDC
	IFNE	NARG-3
	BCC	\3
	ENDC
	ENDM

IFGTIB	MACRO
	CMP.B	\2,\1
	IFEQ	NARG-3
	BHI.S	\3
	ENDC
	IFNE	NARG-3
	BHI	\3
	ENDC
	ENDM

IFLTIW	MACRO
	CMP.W	\2,\1
	IFEQ	NARG-3
	BCS.S	\3
	ENDC
	IFNE	NARG-3
	BCS	\3
	ENDC
	ENDM

IFLEIW	MACRO
	CMP.W	\2,\1
	IFEQ	NARG-3
	BLS.S	\3
	ENDC
	IFNE	NARG-3
	BLS	\3
	ENDC
	ENDM

IFGEIW	MACRO
	CMP.W	\2,\1
	IFEQ	NARG-3
	BCC.S	\3
	ENDC
	IFNE	NARG-3
	BCC	\3
	ENDC
	ENDM

IFGTIW	MACRO
	CMP.W	\2,\1
	IFEQ	NARG-3
	BHI.S	\3
	ENDC
	IFNE	NARG-3
	BHI	\3
	ENDC
	ENDM

IFLTIL	MACRO
	CMP.L	\2,\1
	IFEQ	NARG-3
	BCS.S	\3
	ENDC
	IFNE	NARG-3
	BCS	\3
	ENDC
	ENDM

IFLEIL	MACRO
	CMP.L	\2,\1
	IFEQ	NARG-3
	BLS.S	\3
	ENDC
	IFNE	NARG-3
	BLS	\3
	ENDC
	ENDM

IFGEIL	MACRO
	CMP.L	\2,\1
	IFEQ	NARG-3
	BCC.S	\3
	ENDC
	IFNE	NARG-3
	BCC	\3
	ENDC
	ENDM

IFGTIL	MACRO
	CMP.L	\2,\1
	IFEQ	NARG-3
	BHI.S	\3
	ENDC
	IFNE	NARG-3
	BHI	\3
	ENDC
	ENDM

IFEQ.	MACRO	;STG1,STG2,LABEL
	XREF	CMP..
	LEA	\1,A0
	LEA	\2,A1
	BSR	CMP..
	BEQ	\3
	ENDM

IFNE.	MACRO	;STG1,STG2,LABEL
	XREF	CMP..
	LEA	\1,A0
	LEA	\2,A1
	BSR	CMP..
	BNE	\3
	ENDM

IFLCEQ.	MACRO	;STG,CHAR,LABEL
	XREF	GTLCHAR..
	LEA	\1,A0
	MOVEQ	\2,D7
	BSR	GTLCHAR..
	BEQ	\3
	ENDM

IFLCNE.	MACRO	;STG,CHAR,LABEL
	XREF	GTLCHAR..
	LEA	\1,A0
	MOVEQ	\2,D7
	BSR	GTLCHAR..
	BNE	\3
	ENDM

LEN.	MACRO	;source
	XREF	LEN..
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	BSR	LEN..
	ENDM

SCAN.	MACRO	;SOURCE,DEST
	XREF	SCAN..
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	LEA	\2,A1
	BSR	SCAN..
	ENDM

MOVE.	MACRO	;SOURCE,DEST
	XREF	MOVE..
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	IFNC	'\2','A1'
	LEA	\2,A1
	ENDC
	BSR	MOVE..
	ENDM

MOVEZ.	MACRO	;SOURCE,DEST,COUNT
	MOVEQ	\3-1,D0
	IFNC	'\1','A0'
	MOVE.L	\1,A0
	ENDC
	IFNC	'\2','A1'
	MOVE.L	\2,A1
	ENDC
	MOVE.B	(A0)+,(A1)+
	DBEQ	D0,*-2
	ENDM

LEFT.	MACRO	;SRC,LEN,DEST
	XREF	LEFT..
	LEA	\1,A0
	MOVE.W	\2,D0
	LEA	\3,A1
	BSR	LEFT..
	ENDM

RIGHT.	MACRO	;SRC,LEN,DEST
	XREF	RIGHT..
	LEA	\1,A0
	MOVE.W	\2,D0
	LEA	\3,A1
	BSR	RIGHT..
	ENDM

STRIP_LB.	MACRO
	XREF	STRIP_LB..
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	BSR	STRIP_LB..
	ENDM

STRIP_TB.	MACRO
	XREF	STRIP_TB..
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	BSR	STRIP_TB..
	ENDM

UCASE.	MACRO
	XREF	UCASE..
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	BSR	UCASE..
	ENDM

APPEND. MACRO
	XREF	APPEND..
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	IFNC	'\2','A1'
	LEA	\2,A1
	ENDC
	BSR	APPEND..
	ENDM

STG_Z.	MACRO
	XREF	STG_Z..
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	BSR	STG_Z..
	ENDM

Z_STG.	MACRO
	XREF	Z_STG..
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	IFNC	'\2','A1'
	LEA	\2,A1
	ENDC
	BSR	Z_STG..
	ENDM

STRING. MACRO	;CHAR,COUNT,DEST
	XREF	STRING..
	MOVE.B	\1,D0
	MOVE.W	\2,D1
	LEA	\3,A0
	BSR	STRING..
	ENDM

DISP.	MACRO
	XREF	DISP..
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	BSR	DISP..
	ENDM

PROMPT. MACRO
	XREF	PROMPT..
	LEA	\1,A0
	BSR	PROMPT..
	ENDM

ACHAR.	MACRO ;CHAR,DEST
	XREF	ACHAR..
	MOVE.B	\1,D0
	LEA	\2,A0
	BSR	ACHAR..
	ENDM

STR.	MACRO	;VARIABLE TYPE (B,W,L),VARIABLE NAME,STG,FILL CHAR,LENGTH
	XREF	STR..
	IFNC	'\2','D0'
	IFNC	'\1','L'
	ZAP	D0
	ENDC
	MOVE.\1 \2,D0
	ENDC
	LEA	\3,A0
	MOVE.W	\5,D1
	SWAP	D1
	MOVE.B	\4,D1
	BSR	STR..
	ENDM

CLIP.	MACRO	;COUNT,DEST
	XREF	CLIP..
	IFNC	'\1','D0'
	MOVE.W	\1,D0
	ENDC
	IFNC	'\2','A0'
	LEA	\2,A0
	ENDC
	BSR	CLIP..
	ENDM

ERROR	MACRO
	BCS	\1
	ENDM

NOERROR MACRO
	BCC	\1
	ENDM

RTSERR	MACRO
	BCC.S *+4
	RTS
	ENDM

RTSNOERR MACRO
	BCS.S *+4
	RTS
	ENDM

CALLSYS	MACRO	;OFFSET, BASE
	IFEQ	NARG-2
	MOVE.L	\2,A6
	ENDC
	IFND	_LVO\1
	XREF	_LVO\1
	ENDC
	IFEQ	DEBUG-1
	MOVE.L	#*,SavePC
	ENDC
	JSR	_LVO\1(A6)
	ENDM

GadColor MACRO ;gadget,color
	XREF	GAD_COLOR
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	MOVEQ	#\2,D0
	BSR	GAD_COLOR
	ENDM

CLRBOX	MACRO	;BOX ID,COLOR,OPT=FULL
	XREF	CLEAR_BOX
	MOVE.W	\1,D0
	SWAP	D0
	MOVE.W	\2,D0
	MOVE.W	\3,D1
	SWAP	D1
	MOVE.W	\4,D1
	MOVEQ	#\5,D2
	BSR	CLEAR_BOX
	ENDM

;CLRRBOX	MACRO	;BOX ID,COLOR,OPT=FULL
;	XREF	CLEAR_RBOX
;	IFEQ	NARG-2
;	MOVE.W	#\1_LE+2,D0
;	SWAP	D0
;	MOVE.W	#\1_TE+1,D0
;	MOVE.W	#\1_WD-4,D1
;	SWAP	D1
;	MOVE.W	#\1_HT-2,D1
;	MOVEQ	#\2,D2
;	ENDC
;	IFNE	NARG-2
;	MOVE.W	#\1,D0
;	SWAP	D0
;	MOVE.W	#\2,D0
;	MOVE.W	#\3,D1
;	SWAP	D1
;	MOVE.W	#\4,D1
;	MOVEQ	#\5,D2
;	ENDC
;	BSR	CLEAR_RBOX
;	ENDM

DispRBar MACRO	;x,y,width,requester
	XREF	DISP_RBAR
	MOVE.W	\2,D0
	SWAP	D0
	MOVE.W	\1,D0
	MOVE.W	\3,D1
	MOVE.L	\4,A0
	BSR	DISP_RBAR
	ENDM

DispWBar MACRO	;x,y,width,window
	XREF	DISP_WBAR
	MOVE.W	\2,D0
	SWAP	D0
	MOVE.W	\1,D0
	MOVE.W	\3,D1
	MOVE.L	\4,A0
	BSR	DISP_WBAR
	ENDM

DispMsg	MACRO	;X,Y,STG,Fcolor,Bcolor
	XREF	DISP_MSG
	IFEQ	NARG-3
	MOVEQ	#JAM1,D1
	SWAP	D1
	MOVE.W	#WHITE*256,D1
	ENDC
	IFEQ	NARG-4
	MOVEQ	#JAM1,D1
	SWAP	D1
	MOVE.W	#\4*256,D1
	ENDC
	IFEQ	NARG-5
	MOVEQ	#JAM2,D1
	SWAP	D1
	MOVE.W	#\4*256+\5,D1
	ENDC
	MOVE.W	\1,D0
	SWAP	D0
	MOVE.W	\2,D0
	IFNC	'\3','A0'
	LEA	\3,A0
	ENDC
	BSR	DISP_MSG
	ENDM

DispCur	MACRO	;STG,Fcolor,Bcolor
	XREF	DISP_CURRENT
	IFEQ	NARG-1
	MOVEQ	#JAM1,D1
	SWAP	D1
	MOVE.W	#WHITE*256,D1
	ENDC
	IFEQ	NARG-2
	MOVEQ	#JAM1,D1
	SWAP	D1
	MOVE.W	#\2*256,D1
	ENDC
	IFEQ	NARG-3
	MOVEQ	#JAM2,D1
	SWAP	D1
	MOVE.W	#\2*256+\3,D1
	ENDC
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	BSR	DISP_CURRENT
	ENDM

DispCent	MACRO	;X,Y,STG,Fcolor,Bcolor
	XREF	DISP_CENT
	IFEQ	NARG-3
	MOVEQ	#JAM1,D1
	SWAP	D1
	MOVE.W	#WHITE*256,D1
	ENDC
	IFEQ	NARG-4
	MOVEQ	#JAM1,D1
	SWAP	D1
	MOVE.W	#\4*256,D1
	ENDC
	IFEQ	NARG-5
	MOVEQ	#JAM2,D1
	SWAP	D1
	MOVE.W	#\4*256+\5,D1
	ENDC
	MOVE.W	\1,D0
	SWAP	D0
	MOVE.W	\2,D0
	IFNC	'\3','A0'
	LEA	\3,A0
	ENDC
	BSR	DISP_CENT
	ENDM

DispTxt	MACRO	;X,Y,STG,WINDOW
	XREF	DISP_TEXT
	IFNC	'\1','D0'
	MOVE.W	\1,D0
	ENDC
	IFNC	'\2','D1'
	MOVE.W	\2,D1
	ENDC
	IFNC	'\3','A0'
	LEA	\3,A0
	ENDC
	IFNC	'\4','A1'
	MOVE.L	\4,A1
	ENDC
	BSR	DISP_TEXT
	ENDM

DispImg	MACRO	;X,Y,STG
	XREF	DISP_IMAGE
	MOVE.W	\1,D0
	MOVE.W	\2,D1
	IFNC	'\3','A1'
	LEA	\3,A1
	ENDC
	BSR	DISP_IMAGE
	ENDM

DispReq	MACRO	;BODY, POS, NEG
	XREF	DISP_REQ
	LEA	\1,A1
	LEA	\2,A2
	LEA	\3,A3
	BSR	DISP_REQ
	ENDM

DispErr	MACRO	;ERR PTR
	XREF	DISP_ERR
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	BSR	DISP_ERR
	ENDM

DispYN	MACRO	;msg
	XREF	DISP_YN
	IFNC	'\1','A0'
	LEA	\1,A0
	ENDC
	BSR	DISP_YN
	ENDM

DispWin	MACRO	;LF,COLOR,TEXT,LF
	XREF	DISP_WIN
	MOVEQ	#\1,D0
	MOVEQ	#\2,D1
	MOVEQ	#\4,D2
	IFNC	'\3','A0'
	LEA	\3,A0
	ENDC
	BSR	DISP_WIN
	ENDM

STRUCTURE MACRO
\1	SET	0
SOFFSET	SET	\2
	ENDM

STRUCT	MACRO	;NAME,SIZE
\1	EQU	SOFFSET
SOFFSET	SET	SOFFSET+\2
	ENDM

APTR	MACRO
\1	EQU	SOFFSET
SOFFSET	SET	SOFFSET+4
	ENDM

BYTE	MACRO
\1	EQU	SOFFSET
SOFFSET	SET	SOFFSET+1
	ENDM

WORD	MACRO
\1	EQU	SOFFSET
SOFFSET	SET	SOFFSET+2
	ENDM

LONG	MACRO
\1	EQU	SOFFSET
SOFFSET	SET	SOFFSET+4
	ENDM

LABEL	MACRO
\1	EQU	SOFFSET
	ENDM

