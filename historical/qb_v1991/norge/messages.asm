;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*							*
*	Quarterback language-dependent text messages	*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1987 Central Coast Software	*
*	    268 Bowie Dr, Los Osos, CA 93402		*
*	     All rights reserved, worldwide		*
*********************************************************

	INCLUDE	"vd0:MACROS.ASM"

	SECTION	MESSAGES,DATA

;;	XDEF	CP1.,CP2.,CP3.,CP4.,CP5.,CP6.
	XDEF	MONTHS
	XDEF	Restfm,Backto
	XDEF	ResOpt
;;	XDEF	ResExist
	XDEF	ResEmpty
	XDEF	ResOvr,ResSub,ResRpt
	XDEF	BakOpt,BakArc,BakOvr
;;	XDEF	BakSub
	XDEF	BakRpt,BakChk
	XDEF	Slow.,ResTest
	XDEF	ChgBeep,BDev1Txt.,BDev2Txt.,RDev1Txt.,RDev2Txt.
	XDEF	SelFrom
	XDEF	FSel.,OutOf.,Bytes.,Disks.
	XDEF	Restto.,Backfm.
;	XDEF	Device.
	XDEF	Status,Status.
	XDEF	StRest.
	XDEF	StBack.
	XDEF	StTest.
	XDEF	Disk.
	XDEF	File.
	XDEF	DevNS
	XDEF	DevRdy
	XDEF	DevAct
	XDEF	DevNRdy
	XDEF	PlsLoad
	XDEF	PlsRem
	XDEF	GoodBak,GoodRst,GoodTst,GdBakArc
	XDEF	AbortMsg
;	XDEF	BadVol.,NotDir.
	XDEF	NoBrDev.,BadVol.,BadDir.,NotDir.
	XDEF	NoMemory
	XDEF	SubDir.
;	XDEF	EntNam.
	XDEF	IncNam1.,ExcNam1.
	XDEF	IncNam2.,ExcNam2.
	XDEF	Wild1.,Wild2.
	XDEF	IncDat1.,IncDat2.
	XDEF	ExcDat1.,ExcDat2.
;	XDEF	Onafter.
;	XDEF	Before.
	XDEF	Format.
	XDEF	BldCat.,FilCnt.
	XDEF	CatFor.
	XDEF	Legnd.,LgInc.,LgExc.
	XDEF	DiskBad.
	XDEF	LoadN1.,LoadN2.
	XDEF	LoadL1.,LoadL2.
	XDEF	NoPrt.,NoRpt.
	XDEF	BackRpt.,RestRpt.,CatRpt.
	XDEF	BRptDev.,RRptDev.
	XDEF	Page.
	XDEF	PrtRpt.,PrtCat.
	XDEF	DevOpnErr.
	XDEF	CantSub.
	XDEF	WrtPrt1.,WrtPrt2.,WrtPrt3.
	XDEF	MiscEr1.,MiscEr2.,MiscEr3.
	XDEF	ADOS1.,ADOS2.,ADOS3.
	XDEF	NotQB1.,NotQB2.,NotQB3.
	XDEF	BSEQ1.,BSEQ2.,BSEQ3.
	XDEF	BSET1.,BSET2.,BSET3.
	XDEF	BDSK1.,BDSK2.,BDSK3.
	XDEF	RdEr1.,RdEr2.,RdEr3.
	XDEF	CatEr1.,CatEr2.,CatEr3.
	XDEF	Dup1.,Dup2.
	XDEF	RecovErr.
	XDEF	With.,Backup.,Restore.,Date.,Arc.,UCInc.,UCExc.,All.
	XDEF	NoBatch.
	XDEF	BadSyntax.
;;	XDEF	ArcProt.
	XDEF	ArcWP1.,ArcWP2.,ArcWP3.
	XDEF	ReadErr.
	XDEF	CantBak.,CantRes.
	XDEF	NoneSel.
	XDEF	RPTxt.
	XDEF	ResArc
	XDEF	NoDev.
;	XDEF	UnkDev.
;	XDEF	MListErr.
	XDEF	SameDev.
	XDEF	MixedDev.
;	XDEF	DevMLErr.
	XDEF	NoDFn.
	XDEF	Err.
;	XDEF	Warning.
	XDEF	CatError
	XDEF	CantFindAltCat
	XDEF	DirBlk.
	XDEF	WrtErrRest.,DeviceFull.
	XDEF	FileRdPrt.,FileWrtPrt.
	XDEF	FileDelPrt.,FileInUse.
	XDEF	WrtErrCode.
	XDEF	NotFSD.
	XDEF	BakVol.,ResVol.
	XDEF	VolLst.
	XDEF	AllRes.
	XDEF	Files.
	XDEF	NoCat.,For.

MONTHS	TEXTZ	'JAN'
	TEXTZ	'FEB'
	TEXTZ	'MAR'
	TEXTZ	'APR'
	TEXTZ	'MAI'
	TEXTZ	'JUN'
	TEXTZ	'JUL'
	TEXTZ	'AUG'
	TEXTZ	'SEP'
	TEXTZ	'OKT'
	TEXTZ	'NOV'
	TEXTZ	'DES'

;These text strings appear inside various gadgets:

Backto	;TEXTZ	<'BACKUP FILES'>
	TEXTZ	<'KOPIER FILER'>

Restfm	;TEXTZ	<'RESTORE FILES'>
	TEXTZ	<'HENT FILER'>

; These text strings appear on the device selection screen:

VolLst.	;TEXTZ	<'Here are the hard disk volumes (drives or partitions) on your Amiga.'>
	TEXTZ	<'Her er de tilgjengelige harddiskpartisjonene p� din Amiga.'>

BakVol.	;TEXTZ	<'Click on the name of the volume you wish to backup from.'>
	TEXTZ	<'Klikk p� navnet p� den partisjonen du vil ta sikkerhetskopi av.'>

ResVol.	;TEXTZ	<'Click on the name of the volume you wish to restore to.'>
	TEXTZ	<'Klikk p� navnet p� den enhet du vil sikkerhetskopien skal legges p�.'>

Backfm.	;TEXTZ	<'Backup files from this hard disk volume:'>
	TEXTZ	<'Sikkerhetskopier denne harddisken:'>

Restto.	;TEXTZ	<'Restore files to this hard disk volume:'>
	TEXTZ	<'Les sikkerhetskopi inn p� denne harddisken:'>

BDev1Txt.
	;TEXTZ	<'Backup files from hard disk to this primary device:'>
	TEXTZ	<'Sikkerhetskopier til denne hovedenhet:'>

BDev2Txt.
	;TEXTZ	<'Backup files from hard disk to this alternate device:'>
	TEXTZ	<'Sikkerhetskopier til denne sekund�renhet:'>

RDev1Txt.
	;TEXTZ	<'Restore files to hard disk from this primary device:'>
	TEXTZ	<'Hent sikkerhetskopi til denne hovedenhet:'>

RDev2Txt. 
	;TEXTZ	<'Restore files to hard disk from this alternate device:'>
	TEXTZ 	<'Hent sikkerhetskopi til denne sekund�renhet:'>


;These are requester strings which may appear after device selection:

NoBrDev. ;TEXTZ	<'Please select a hard disk volume to backup/restore.'>
	TEXTZ	<'Venligst velg en harddiskpartisjon.'>

BadVol. ;TEXTZ	<'Can''t find hard disk volume you entered.'>
	TEXTZ	<'Finner ikke den harddiskenheten du har valgt.'>

BadDir. ;TEXTZ	<'Can''t find the specified directory on that volume.'>
	TEXTZ	<'Finner ikke spesifisert katalog p� den enheten.'>

NotDir.	;TEXTZ	<'Enter hard disk volume name, not a file name.'>
	TEXTZ	<'Skriv harddisknavnet - ikke et filnavn.'>

NoDev.	;TEXTZ	<'You must specify a primary device.'>
	TEXTZ	<'Du m� spesifisere hovedenhet.'

MixedDev. ;TEXTZ <'Primary and alternate devices must be of the same type.'>
	TEXTZ	<'Hovedenhet og sekund�renhet m� v�re av samme type.'>

SameDev.  ;TEXTZ	<'Alternate device cannot be same as primary device.'>
	TEXTZ	<'Sekund�renheten kan ikke v�re den samme som hovedenheten.'

NoDFn.	  ;TEXTZ	<'@ is not available.'>
	TEXTZ	<'@ er ikke tilgjengelig.'>

NotFSD.	  ;TEXTZ	<' is not a file storage device.'>
	TEXTZ	<'@ er ikke et lagringsmedium.'>


NoMemory ;TEXTZ	<'Out of memory ... can''t perform your request.'>
	TEXTZ	<'For lite hukommelse ... kan ikke utf�re valget.'>


; These text strings appear while building the catalog:

BldCat.	;TEXTZ	<'Building catalog of files ... please wait!'>
	TEXTZ	<'Bygger filkatalogen ... venligst vent!'>

FilCnt.	;TEXTZ	<'Files found:     '>
	TEXTZ	<'Filer funnet:     '>

; These requester strings may appear during restore:

CatError ;TEXTZ	<'Unable to read backup catalog.'>
	TEXTZ	<'Kan ikke lese katalog fra sikkerhetskopi.'>

CantFindAltCat 
	;TEXTZ	<'Unable to find alternate catalog.'>
	TEXTZ	<'Finner ikke alternativ katalog.'>


;These text strings appear on the catalog screen:

FSel.	;TEXTZ	<'Files included:'>
	TEXTZ	<'Valgte filer:'>

OutOf.	;TEXTZ	<' of '>
	TEXTZ	<' av '>

Bytes.	;TEXTZ	<'Bytes: '>
	TEXTZ	<'Bytes: '

Disks.	;TEXTZ	<'Volumes: '>
	TEXTZ	<'Disker: '>

SubDir.	;TEXTZ	<'  (Subdirectory)       '>
	TEXTZ	<'  (katalog)            '>

SelFrom	;TEXTZ	<'Commands apply to:'>
	TEXTZ	<'Kommandoer gjelder:'>

CatFor.	;TEXTZ	<'Catalog for: '>
	TEXTZ	<'Katalog for: '>

Legnd.	;TEXTZ	<'Legend: '>
	TEXTZ	<'Betydning: '>

LgInc.	;TEXTZ	<' included '>
	TEXTZ	<' valgt '>

LgExc.	;TEXTZ	<' excluded '>
	TEXTZ	<' utelatt '>


;These are requester text strings which may appear during selection of files on
;the catalog screen:

IncNam1. ;TEXTZ	<'Enter name of file to be included.'>
	TEXTZ	<'Skriv navnet p� filer som skal velges.'>
IncNam2. TEXTZ	<' '>

ExcNam1. ;TEXTZ	<'Enter name of file to be excluded.'>
	TEXTZ	<'Skriv navnet p� filer som skal utelates.'>
ExcNam2. TEXTZ	<' '>

Wild1.	;TEXTZ	<'Use wildcards ''?'' or ''#?'' for groups.'>
	TEXTZ	<'Bruk '?' eller '#?' for gruppevalg.'>
Wild2.	TEXTZ	<' '>

IncDat1. ;TEXTZ	<'Include files which changed on or after'>
	TEXTZ	<'Velg filer som er forandret p� eller etter'
IncDat2. TEXTZ	<' '>

ExcDat1. ;TEXTZ	<'Exclude files which haven''t changed since'>
	TEXTZ	<'Utelat filer som ikke er forandret siden'
ExcDat2. TEXTZ	<' '>

Format.	;TEXTZ	<'Date format: 31-DEC-87'>
	TEXTZ	<'Dato format: 31-DES-90'>


; These text strings appear on the backup/restore options screen:

ResOpt	;TEXTZ	<'SELECT RESTORE OPTIONS'>
	TEXTZ	<'VELG OPSJONER'>

BakOpt	;TEXTZ	<'SELECT BACKUP OPTIONS'>
	TEXTZ	<'VELG OPSJONER'>

ResOvr	;TEXTZ	<'Overwrite existing files:'>
	TEXTZ	<'Erstatt eksisterende filer:'>

ResEmpty TEXTZ	<'Restore empty subdirectories:'>
	TEXTZ	<'Ta med tomme kataloger:'>

ResSub	TEXTZ	<'Restore original subdirectory path:'>
	TEXTZ	<'Hent original katalogstruktur:'>

BakArc	;TEXTZ	<'Set archive flag on backed-up files:'>
	TEXTZ	<'Sett arkivflagg p� kopierte filer:'>

ResArc	;TEXTZ	<'Set archive bit for restored files:'>
	TEXTZ	<'Sett arkivflagg p� leste filer:'>

BakOvr	;TEXTZ	<'Overwrite AmigaDOS format on floppies:'>
	TEXTZ	<'Skriv over AmigaDOS format p� diskettene:'>

BakChk	;TEXTZ	<'Read data after write:'>
	TEXTZ	<'Kontroller data etter skriving:'>

Slow.	;TEXTZ	<'Slow speed, reduced memory: '>
	TEXTZ	<'Lav hastighet - redusert bruk av minne:'>

ResTest	;TEXTZ	<'Read backup volumes, don''t restore files:'>
	TEXTZ	<'Sjekk diskene i kopien, ikke hent filer:'>

ChgBeep	;TEXTZ	<'Beep for QB volume change: '>
	TEXTZ	<'Lydsignal ved diskettbytte: '>

BakRpt	;TEXTZ	<'Send archive report to:'>
	TEXTZ	<'Send arkivrapport til:'>

ResRpt	;TEXTZ	<'Send restoration report to:'>
	TEXTZ	<'Send arkivrapport til:'>


; This requester appears if you select DISK for the report:

RPTxt.	;TEXTZ	<'Enter device:path/name for report:'>
	TEXTZ	<'Skriv enhet:katalog/navn for rapport:'>


;This requester text appears if the disk report path is bad:

NoPrt.	;TEXTZ	<'Can''t open printer for report...retry?'>
	TEXTZ	<'Kan ikke �pne skriver for rapport...pr�v igjen?'>

NoRpt.	;TEXTZ	<'Can''t open file for report...retry?'>
	TEXTZ	<'Kan ikke �pne fil for rapport...pr�v igjen?'>

; These text strings appear on the backup/restore status screen:


Status	;TEXTZ	<'S T A T U S'>
	TEXTZ	<'S T A T U S'>

StRest.	;TEXTZ	<'Restoring to '>
	TEXTZ	<'Henter sikkerhetskopi til '>

StBack.	;TEXTZ	<'Backing up '>
	TEXTZ	<'Sikkerhetskopierer '>

StTest.	;TEXTZ	<'Testing backup volumes'>
	TEXTZ	<'Sjekker sikkerhetskopi'>

Disk.	;TEXTZ	<'Volume '>
	TEXTZ	<'Disk '>

File.	;TEXTZ	<'File '>
	TEXTZ	<'Fil '>

DevNS	;TEXTZ	<'No second drive selected'>
	TEXTZ	<'Ingen ekstra enhet valgt'>

Status.	;TEXTZ	<' status:'>
	TEXTZ	<' Status:'>

DevRdy	;TEXTZ	<'ready    '>
	TEXTZ	<'klar     '>

DevAct	;TEXTZ	<'active   '>
	TEXTZ 	<'aktiv    '>

DevNRdy	;TEXTZ	<'not ready'>
	TEXTZ 	<'ikke klar'>

PlsLoad	;TEXTZ	<'Please load volume '>
	TEXTZ	<'Vennligst sett inn disk %'>

PlsRem	;TEXTZ	<'Please remove volume '>
	TEXTZ	<'Vennligst fjern disk '>

GoodBak	;TEXTZ	<'File backup completed.'>
	TEXTZ	<'Sikkerhetskopiering ferdig.'>

GoodRst	;TEXTZ	<'File restore completed.'>
	TEXTZ	<'Henting av sikkerhetskopi ferdig.'>

GoodTst	;TEXTZ	<'Volume testing completed.'>
	TEXTZ	<'Sjekk av disketter ferdig'>

GdBakArc ;TEXTZ	<'Backup successful -- setting archive bits.'>
	TEXTZ	<'Sikkerhetskopiering ferdig ... setter arkivflagg.'>

AbortMsg ;TEXTZ	<'WARNING ---- Backup/restore aborted ---- WARNING'>
	TEXTZ	<'ADVARSEL -- Kopiering/Henting avbrutt -- ADVARSEL'>


;These requester messages may appear during the backup/restore process:

WrtPrt1. ;TEXTZ	<'WARNING - Volume in drive@ is write'>
	TEXTZ	<'ADVARSEL - diskett i@ er skrivebeskyttet'>
WrtPrt2. ;TEXTZ	<'protected.  Enable writing or replace'>
	TEXTZ	<'Sl� av beskyttelsen eller bytt diskett'
WrtPrt3. ;TEXTZ	<'volume.  PROCEED to retry.'>
	TEXTZ	<'FORTSETT for nytt fors�k.'>

MiscEr1. ;TEXTZ	<'WARNING - Error writing to drive@.'>
	TEXTZ	<'ADVARSEL - Feil under skriving til@.'
MiscEr2. ;TEXTZ	<'Replace volume and PROCEED.  New'>
	TEXTZ	<'Bytt diskett og FORTSETT. Ny diskett'
MiscEr3. ;TEXTZ	<'volume replaces old volume in sequence.'>
	TEXTZ	<'erstatter gammel disk i sekvensen.'>

ADOS1.	;TEXTZ	<'WARNING - Volume in drive@ contains'>
	TEXTZ	<'ADVARSEL - Disketten i@ inneholder'>
ADOS2.	;TEXTZ	<'AmigaDOS files.  Replace volume and '>
	TEXTZ	<'AmigaDOS filer. Bytt diskett og'>
ADOS3.	;TEXTZ	<'PROCEED to avoid losing these files.'>
	TEXTZ	<'FORTSETT for � ikke miste disse filene.'>

BSET1.	;TEXTZ	<'Backup volume in drive@ has a different'>
	TEXTZ	<'Disken i@ har feil dato/tids merke.'>
BSET2.	;TEXTZ	<'backup date/time stamp.  It does not belong'>
	TEXTZ	<'Den tilh�rer ikke denne sikkerhetskopien.'>
BSET3.	;TEXTZ	<'to the backup set you are restoring.'>
	TEXTZ	<' '>

BSEQ1.	;TEXTZ	<'Volume % in drive@ is not the volume'>  
	TEXTZ	<'Disketten % i@ er ikke neste diskett.'>
BSEQ2.	;TEXTZ	<'needed next.  Please remove it and'>
	TEXTZ	<'Venligst fjern den og velg FORTSETT eller'>
BSEQ3.	;TEXTZ	<'PROCEED, or SKIP to bypass, or CANCEL.'>
	TEXTZ	<'HOPP OVER for � g� videre. AVBRYT avslutter.'>

NotQB1.	;TEXTZ	<'Volume in drive@ is not a Quarterback'>
	TEXTZ	<'Disketten i@ er ikke en Quarterback diskett.'>
NotQB2.	;TEXTZ	<'backup volume.  Load correct volume and'>
	TEXTZ	<'Sett inn rett disk og velg FORTSETT eller'>
NotQB3.	;TEXTZ	<'PROCEED, or SKIP to bypass, or CANCEL.'>
	TEXTZ	<'HOPP OVER' for � g� videre. AVBRYT avslutter.>

ArcWP1.	;TEXTZ	<'Can''t update archive bits; drive is'>
	TEXTZ	<'Kan ikke oppdatere arkivflagg - harddisken'>
ArcWP2.	;TEXTZ	<'write protected.  Enable writing and'>
	TEXTZ	<'er skrive beskyttet. Sl� av skrivebeskyttelsen'>
ArcWP3.	;TEXTZ	<'select PROCEED to retry.'>
	TEXTZ	<'og velg FORTSETT for � pr�ve igjen.'>

RdEr1.	;TEXTZ	<'Read error on track 0 of volume in'>
	TEXTZ	<'Lesefeil p� spor 0 p� disketten i@.'>
RdEr2.	;TEXTZ	<'drive@.  Can''t restore from bad volume.'>
	TEXTZ	<'Kan ikke lese fra disketten. Velg'>
RdEr3.	;TEXTZ	<'Select SKIP to proceed with next volume.'>
	TEXTZ	<'HOPP OVER for � fortsette med neste disk.'>

CatEr1.	;TEXTZ	<'Error reading catalog from first volume.'>
	TEXTZ	<'Feil under lesing av katalog fra f�rste disk.'>
CatEr2.	;TEXTZ	<'Select SKIP to switch to alternate'>
	TEXTZ	<'Velg HOPP OVER for � lese katalogen fra'>
CatEr3.	;TEXTZ	<'catalog on last backup volume.'>
	TEXTZ	<'den siste disken i sikkerhetskopien.'>

BDSK1.	;TEXTZ	<'WARNING - Volume in drive@ is unusable.'>
	TEXTZ	<'Sikkerhetskopi i@ er ubrukelig.'>
BDSK2.	;TEXTZ	<'Replace bad volume and PROCEED.  New'>
	TEXTZ	<'Bytt diskett og FORTSETT. Ny diskett'>
BDSK3.	;TEXTZ	<'volume replaces old volume in sequence.'>
	TEXTZ	<'erstatter gammel disk i sekvensen.'>

Dup1.	;TEXTZ	<'This file already exists:'>
	TEXTZ	<'Denne filen finnes allerede:'>
Dup2.	;TEXTZ	<'Do you want to replace it?'>
	TEXTZ	<'Vil du erstatte den?'>

RecovErr.
	;TEXTZ	<'Bad volume recovery error...aborting'>
	TEXTZ	<'Feil under oppretting av disk...avslutter'>

ReadErr. ;TEXTZ	<'Read error on active volume'>
	TEXTZ	<'Lesefeil p� aktiv disk'>

CantRes. ;TEXTZ	<'Unable to restore this file:'>
	TEXTZ	<'Kan ikke lese denne filen:'>

CantBak. ;TEXTZ	<'Unable to backup this file:'>
	TEXTZ	<'Kan ikke kopiere denne filen:'>

NoneSel. ;TEXTZ	<'No files selected to process.'>
	TEXTZ	<'Du har ikke valgt noen filer.'>

DiskBad. ;TEXTZ	<'Hard disk directory corrupted...continuing'>
	TEXTZ	<'Harddisk katalogen er �delagt...fortsetter'>

WrtErrRest.
	;TEXTZ	<'Write error while restoring file:'>
	TEXTZ	<'Feil ved skriving av innhentet fil:'>

FileWrtPrt.
	;TEXTZ	<'File is write protected.'>
	TEXTZ	<'Eksisterende fil er skrivebeskyttet.'>

FileRdPrt.
	;TEXTZ	<'File is read protected.'>
	TEXTZ	<'Filen er lesebeskyttet.'>

FileInUse.
	;TEXTZ	<'File is in use.'>
	TEXTZ	<'Eksisterende fil er i bruk.'

FileDelPrt.
	;TEXTZ	<'File is delete protected.'>
	TEXTZ	<'Eksisterende fil kan ikke slettes.'>

DeviceFull.
	;TEXTZ	<'Device is full.'>
	TEXTZ	<'Enheten er full.'>

WrtErrCode.
	;TEXTZ	<'Write error code: '>
	TEXTZ	<'Feilkode:'>


;These requester strings may appear at various times during a restore:

LoadN1.	;TEXTZ	<'Load volume % from the backup set'>
	TEXTZ	<'Sett inn disk % fra sikkerhetskopien'>
LoadN2.	;TEXTZ	<'you wish to restore into drive@'>
	TEXTZ	<'i@.'>

LoadL1.	;TEXTZ	<'Load the LAST volume from the backup set'>
	TEXTZ	<'Sett inn SISTE disk fra sikkerhetskopien'>
LoadL2.	;TEXTZ	<'you wish to restore into drive@'>
	TEXTZ	<'i@.'>

DevOpnErr. ;TEXTZ <'Unable to open device@'>
	TEXTZ	<'Kan ikke �pne@'>

CantSub. ;TEXTZ	<'Cannot create subdirectory'>
	TEXTZ	<'Kan ikke lage katalog'>


;This message appears while reports are being produced:

NoCat.	;TEXTZ	<'There is no catalog to print.'>
	TEXTZ	<'Det er ingen katalog � skrive ut.'>

PrtRpt.	;TEXTZ	<'Producing backup/restore report...please wait.'>
	TEXTZ	<'Produserer statusrapport...venligst vent.'>

PrtCat.	;TEXTZ	<'Printing Quarterback catalog...please wait.'>
	TEXTZ	<'Skriver ut Quarterback katalog...venligst vent.'>

;These text strings appear on backup/restore reports:

BackRpt. ;TEXTZ	<'Archive Report'>
	TEXTZ	<'Arkiveringsrapport'>

RestRpt. ;TEXTZ	<'Restoration Report'>
	TEXTZ	<'Statusrapport'>

CatRpt.	;TEXTZ	<'Quarterback Catalog'>
	TEXTZ	<'Quarterback katalog'>

BRptDev. ;TEXTZ	<'% files backed up from@'>
	TEXTZ	<'% filer kopiert fra@'>

RRptDev. ;TEXTZ	<'% files restored to@'>
	TEXTZ	<'% filer hentet til@'>

Page.	;TEXTZ	<'Page '>
	TEXTZ	<'Side '>

Err.	;TEXTZ	<' ** Error **'>
	TEXTZ	<' ** Feil **'>


;These are general requester messages:

DirBlk.	;TEXTZ	<'Directory blocks not released.'>
	TEXTZ	<'Katalogblokker ikke frigjort.'>

NoBatch. ;TEXTZ	<'I can''t find the command file.'>
	TEXTZ	<'Kan ikke finne kommandofilen.'>

BadSyntax.
	;TEXTZ	<'Command file error...See manual.'>
	TEXTZ	<'Feil i kommandofil...Se i manualen.'>


;These messages are used to decode CLI command line options or command files:


With.	;TEXTZ	'WITH'
	TEXTZ	'WITH'

Backup.	;TEXTZ	'BACKUP'
	TEXTZ	'BACKUP'

Restore. ;TEXTZ	'RESTORE'
	TEXTZ	'RESTORE'

All.	;TEXTZ	'ALL'
	TEXTZ	'ALL'

Date.	;TEXTZ	'DATE'
	TEXTZ	'DATE'

UCInc.	;TEXTZ	'INCLUDE'
	TEXTZ	'INCLUDE'

UCExc.	;TEXTZ	'EXCLUDE'
	TEXTZ	'EXCLUDE'

Arc.	;TEXTZ	'ARCHIVE'
	TEXTZ	'ARCHIVE'

For.	;TEXTZ	<' for '>
	TEXTZ	<' for '>

Files.	;TEXTZ	<'Files: '>
	TEXTZ	<'Filer: '>

AllRes.	TEXTZ	<'ALLE RETTIGHETER RESERVERT'>

	END
