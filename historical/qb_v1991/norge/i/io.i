;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
* IO macros and structures

 STRUCTURE	IO,MN_SIZE
	APTR	io_Device
	APTR	io_Unit
	WORD	io_Command
	BYTE	io_Flags
	BYTE	io_Error
	LABEL	IO_SIZE
	LONG	io_Actual
	LONG	io_Length
	APTR	io_Data
	LONG	io_Offset
	LONG	io_RESERVED1
	LONG	io_RESERVED2
	LABEL	IOSTD_SIZE
	LONG	IOTD_COUNT
	LONG	IOTD_SECLABEL
	LABEL	IOTD_SIZE

DEVINIT	MACRO	*
	IFC	'\1',''
CMD_COUNT	SET	CMD_NONSTD
	ENDC
	IFNC	'\1',''
CMD_COUNT	SET	\1
	ENDC
	ENDM

DEVCMD	MACRO	*
\1	EQU	CMD_COUNT
CMD_COUNT	SET	CMD_COUNT+1
	ENDM

	DEVINIT	0
	DEVCMD	CMD_INVALID
	DEVCMD	CMD_RESET
	DEVCMD	CMD_READ
	DEVCMD	CMD_WRITE
	DEVCMD	CMD_UPDATE
	DEVCMD	CMD_CLEAR
	DEVCMD	CMD_STOP
	DEVCMD	CMD_START
	DEVCMD	CMD_FLUSH
	DEVCMD	CMD_NONSTD
	DEVINIT
	DEVCMD	TD_MOTOR
	DEVCMD	TD_SEEK
	DEVCMD	TD_FORMAT
	DEVCMD	TD_REMOVE
	DEVCMD	TD_CHANGENUM
	DEVCMD	TD_CHANGESTATE
	DEVCMD	TD_PROTSTATUS
	DEVCMD	TD_RAWREAD
	DEVCMD	TD_RAWWRITE
	DEVCMD	TD_GETDRIVETYPE
	DEVCMD	TD_GETNUMTRACKS
	DEVCMD	TD_ADDCHANGEINT
	DEVCMD	TD_REMCHANGEINT
	DEVCMD	TD_GETGEOMETRY
	DEVCMD	TD_EJECT

ADHARD_CHANNELS         EQU     4
ADALLOC_MINPREC         EQU     -128
ADALLOC_MAXPREC         EQU     127
ADCMD_FREE              EQU     CMD_NONSTD+0
ADCMD_SETPREC           EQU     CMD_NONSTD+1
ADCMD_FINISH            EQU     CMD_NONSTD+2
ADCMD_PERVOL            EQU     CMD_NONSTD+3
ADCMD_LOCK              EQU     CMD_NONSTD+4
ADCMD_WAITCYCLE         EQU     CMD_NONSTD+5
ADCMDB_NOUNIT           EQU     5
ADCMDF_NOUNIT           EQU     1<<5
ADCMD_ALLOCATE          EQU     ADCMDF_NOUNIT+0
ADIOB_PERVOL            EQU     4
ADIOF_PERVOL            EQU     1<<4
ADIOB_SYNCCYCLE         EQU     5
ADIOF_SYNCCYCLE         EQU     1<<5
ADIOB_NOWAIT            EQU     6
ADIOF_NOWAIT            EQU     1<<6
ADIOB_WRITEMESSAGE      EQU     7
ADIOF_WRITEMESSAGE      EQU     1<<7
ADIOERR_NOALLOCATION    EQU     -10
ADIOERR_ALLOCFAILED     EQU     -11
ADIOERR_CHANNELSTOLEN   EQU     -12

 STRUCTURE	IOAudio,IO_SIZE
	WORD	ioa_AllocKey
	APTR	ioa_Data
	LONG	ioa_Length
	WORD	ioa_Period
	WORD	ioa_Volume
	WORD	ioa_Cycles
	STRUCT	ioa_WriteMsg,MN_SIZE
	LABEL	ioa_SIZEOF

