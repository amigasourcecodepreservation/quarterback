;
; This file is part of Quarterback and Quarterback Tools.
; Copyright (C) 1996-2018 Canux Corporation
;
; Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
;
*********************************************************
*							*
*	Quarterback language-dependent text messages	*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1987 Central Coast Software	*
*	    268 Bowie Dr, Los Osos, CA 93402		*
*	     All rights reserved, worldwide		*
*********************************************************

	INCLUDE	"vd0:MACROS.ASM"

	SECTION	MESSAGES,DATA

;;	XDEF	CP1.,CP2.,CP3.,CP4.,CP5.,CP6.
	XDEF	MONTHS
	XDEF	Restfm,Backto
	XDEF	ResOpt
;;	XDEF	ResExist
	XDEF	ResEmpty
	XDEF	ResOvr,ResSub,ResRpt
	XDEF	BakOpt,BakArc,BakOvr
;;	XDEF	BakSub
	XDEF	BakRpt,BakChk
	XDEF	Slow.,ResTest
	XDEF	ChgBeep,BDev1Txt.,BDev2Txt.,RDev1Txt.,RDev2Txt.
	XDEF	SelFrom
	XDEF	FSel.,OutOf.,Bytes.,Disks.
	XDEF	Restto.,Backfm.
;	XDEF	Device.
	XDEF	Status,Status.
	XDEF	StRest.
	XDEF	StBack.
	XDEF	StTest.
	XDEF	Disk.
	XDEF	File.
	XDEF	DevNS
	XDEF	DevRdy
	XDEF	DevAct
	XDEF	DevNRdy
	XDEF	PlsLoad
	XDEF	PlsRem
	XDEF	GoodBak,GoodRst,GoodTst,GdBakArc
	XDEF	AbortMsg
;	XDEF	BadVol.,NotDir.
	XDEF	NoBrDev.,BadVol.,BadDir.,NotDir.
	XDEF	NoMemory
	XDEF	SubDir.
;	XDEF	EntNam.
	XDEF	IncNam.,ExcNam.
	XDEF	Wild.
	XDEF	IncDat.
	XDEF	ExcDat.
;	XDEF	Onafter.
;	XDEF	Before.
	XDEF	Format.
	XDEF	BldCat.,FilCnt.
	XDEF	CatFor.
	XDEF	Legnd.,LgInc.,LgExc.
	XDEF	DiskBad.
	XDEF	LoadN1.,LoadN2.
	XDEF	LoadL1.,LoadL2.
	XDEF	NoPrt.,NoRpt.
	XDEF	BackRpt.,RestRpt.,CatRpt.
	XDEF	BRptDev.,RRptDev.
	XDEF	Page.
	XDEF	PrtRpt.,PrtCat.
	XDEF	DevOpnErr.
	XDEF	CantSub.
	XDEF	WrtPrt1.,WrtPrt2.,WrtPrt3.
	XDEF	MiscEr1.,MiscEr2.,MiscEr3.
	XDEF	ADOS1.,ADOS2.,ADOS3.
	XDEF	NotQB1.,NotQB2.,NotQB3.
	XDEF	BSEQ1.,BSEQ2.,BSEQ3.
	XDEF	BSET1.,BSET2.,BSET3.
	XDEF	BDSK1.,BDSK2.,BDSK3.
	XDEF	RdEr1.,RdEr2.,RdEr3.
	XDEF	CatEr1.,CatEr2.,CatEr3.
	XDEF	Dup1.,Dup2.
	XDEF	RecovErr.
	XDEF	With.,Backup.,Restore.,Date.,Arc.,UCInc.,UCExc.,All.
	XDEF	NoBatch.
	XDEF	BadSyntax.
;;	XDEF	ArcProt.
	XDEF	ArcWP1.,ArcWP2.,ArcWP3.
	XDEF	ReadErr.
	XDEF	CantBak.,CantRes.
	XDEF	NoneSel.
	XDEF	RPTxt.
	XDEF	ResArc
	XDEF	NoDev.
;	XDEF	UnkDev.
;	XDEF	MListErr.
	XDEF	SameDev.
	XDEF	MixedDev.
;	XDEF	DevMLErr.
	XDEF	NoDFn.
	XDEF	Err.
;	XDEF	Warning.
	XDEF	CatError
	XDEF	CantFindAltCat
	XDEF	DirBlk.
	XDEF	WrtErrRest.,DeviceFull.
	XDEF	FileRdPrt.,FileWrtPrt.
	XDEF	FileDelPrt.,FileInUse.
	XDEF	WrtErrCode.
	XDEF	NotFSD.
	XDEF	BakVol.,ResVol.
	XDEF	VolLst.
	XDEF	AllRes.
	XDEF	Files.
	XDEF	NoCat.,For.

;;CP1.	TEXTZ	<'Copyright ',$A9,' 1989'>
;;CP2.	TEXTZ	<'Central Coast Software'>
;;CP3.	TEXTZ	<'All Rights Reserved'>
;;CP4.	TEXTZ	<'424 Vista Avenue'>
;;CP5.	TEXTZ	<'Golden, Colorado 80401'>
;;CP6.	TEXTZ	<'(303) 526-1030'>

MONTHS	TEXTZ	'JAN'
	TEXTZ	'FEB'
	TEXTZ	'MAR'
	TEXTZ	'APR'
	TEXTZ	'MAY'
	TEXTZ	'JUN'
	TEXTZ	'JUL'
	TEXTZ	'AUG'
	TEXTZ	'SEP'
	TEXTZ	'OCT'
	TEXTZ	'NOV'
	TEXTZ	'DEC'

Backto	TEXTZ	<'BACKUP FILES'>
Restfm	TEXTZ	<'RESTORE FILES'>

ResOpt	TEXTZ	<'SELECT RESTORE OPTIONS'>
BakOpt	TEXTZ	<'SELECT BACKUP OPTIONS'>
BakArc	TEXTZ	<'Set archive bit on backed-up files:'>
ResOvr	TEXTZ	<'Overwrite existing files:'>
BakOvr	TEXTZ	<'Overwrite AmigaDOS format on floppies:'>
BakChk	TEXTZ	<'Read data after write:'>
Slow.	TEXTZ	<'Slow speed, reduced memory: '>
ResTest	TEXTZ	<'Read backup disks, don''t restore files:'>
ChgBeep	TEXTZ	<'Beep for QB disk change: '>
ResRpt	TEXTZ	<'Send restoration report to:'>
BakRpt	TEXTZ	<'Send archive report to:'>
;;BakSub	TEXTZ	<'Backup full subdirectory structure:'>
;;ResExist 	TEXTZ	<'Restore only files which already exist:'>
;;ResSub	TEXTZ	<'Restore full subdirectory structure:'>
ResEmpty TEXTZ	<'Restore empty subdirectories:'>
ResSub	TEXTZ	<'Restore original subdirectory path:'>
ResArc	TEXTZ	<'Set archive bit for restored files:'>
SelFrom	TEXTZ	<'Commands apply to:'>
VolLst.	TEXTZ	<'Here are the hard disk volumes (drives or partitions) on your Amiga.'>
BakVol.	TEXTZ	<'Click on the name of the volume you wish to backup from.'>
ResVol.	TEXTZ	<'Click on the name of the volume you wish to restore to.'>
;;BakVol.	TEXTZ	<'Select the name of the volume you wish to backup files from:'>
;;ResVol.	TEXTZ	<'Select the name of the volume you wish to restore files to:'>
Backfm.	TEXTZ	<'Backup files from this hard disk volume:'>
Restto.	TEXTZ	<'Restore files to this hard disk volume:'>
;;Restto.	DC.B	'Hard disk volume name and optional subdirectory path to '
;;	TEXTZ	<'restore to:'>
;;Backfm.	DC.B	'Hard disk volume name and optional subdirectory path to '
;;	TEXTZ	<'backup from:'>
FSel.	TEXTZ	<'Files included:'>
OutOf.	TEXTZ	<' of '>
Bytes.	TEXTZ	<'Bytes: '>
Disks.	TEXTZ	<'disks: '>
Status	TEXTZ	<'S T A T U S'>
Status.	TEXTZ	<' status:'>
StRest.	TEXTZ	<'Restoring to '>
StBack.	TEXTZ	<'Backing up '>
StTest.	TEXTZ	<'Testing backup disks'>
Disk.	TEXTZ	<'disk '>
File.	TEXTZ	<'File '>
DevNS	TEXTZ	<'No second drive selected'>
DevRdy	TEXTZ	<'ready    '>
DevAct	TEXTZ	<'active   '>
DevNRdy	TEXTZ	<'not ready'>
PlsLoad	TEXTZ	<'Please load disk %'>
PlsRem	TEXTZ	<'Please remove disk '>
GoodBak	TEXTZ	<'File backup completed.'>
GoodRst	TEXTZ	<'File restore completed.'>
GoodTst	TEXTZ	<'Disk testing completed.'>
GdBakArc TEXTZ	<'Backup successfull -- setting archive bits.'>
AbortMsg TEXTZ	<'WARNING ---- Backup/restore aborted ---- WARNING'>
NoBrDev. TEXTZ	<'Please select a hard disk volume to backup/restore.'>
BadVol. TEXTZ	<'Can''t find hard disk volume you entered.'>
BadDir. TEXTZ	<'Can''t find the specified directory on that volume.'>
NotDir.	TEXTZ	<'Enter hard disk volume name, not a file name.'>
DiskBad. TEXTZ	<'Hard disk directory corrupted...continuing'>
NoMemory TEXTZ	<'Out of memory ... can''t perform your request.'>
SubDir.	TEXTZ	<'  (Subdirectory)       '>
IncNam.	TEXTZ	<'Enter name of file to be included.'>
ExcNam.	TEXTZ	<'Enter name of file to be excluded.'>
Wild.	TEXTZ	<'Use wildcards ''?'' or ''#?'' for groups.'>
IncDat.	TEXTZ	<'Include files which changed on or after'>
ExcDat.	TEXTZ	<'Exclude files which haven''t changed since'>
RPTxt.	TEXTZ	<'Enter device:path/name for report:'>
;Onafter. TEXTZ	<'after'>
;Before.	TEXTZ	<'since'>
Format.	TEXTZ	<'Date format: 31-DEC-87'>
BldCat.	TEXTZ	<'Building catalog of files ... please wait!'>
FilCnt.	TEXTZ	<'Files found:     '>
CatFor.	TEXTZ	<'Catalog for: '>
Legnd.	TEXTZ	<'Legend: '>
LgInc.	TEXTZ	<' included '>
LgExc.	TEXTZ	<' excluded '>
LoadN1.	TEXTZ	<'Load disk % from the backup set'>
LoadL2.
LoadN2.	TEXTZ	<'you wish to restore into drive@.'>
LoadL1. TEXTZ	<'Load the LAST disk from the backup set'>
NoPrt.	TEXTZ	<'Can''t open printer for report...retry?'>
NoRpt.	TEXTZ	<'Can''t open file for report...retry?'>
BackRpt. TEXTZ	<'Archive Report'>
RestRpt. TEXTZ	<'Restoration Report'>
CatRpt.	TEXTZ	<'Quarterback Catalog'>
BRptDev. TEXTZ	<'% files backed up from@'>
RRptDev. TEXTZ	<'% files restored to@'>
Files.	TEXTZ	<'Files: '>
Page.	TEXTZ	<'Page '>
PrtRpt.	TEXTZ	<'Producing backup/restore report...please wait.'>
PrtCat.	TEXTZ	<'Printing Quarterback catalog...please wait.'>
DevOpnErr. TEXTZ <'Unable to open device@'>
CantSub. TEXTZ	<'Cannot create subdirectory'>
;;ArcProt. TEXTZ	<'Can''t update archive bits...write protected.'>
ArcWP1.	TEXTZ	<'Can''t update archive bits; drive is'>
ArcWP2.	TEXTZ	<'write protected.  Enable writing and'>
ArcWP3.	TEXTZ	<'select PROCEED to retry.'>
;Warning. TEXTZ	<'WARNING - Disk in drive '>
WrtPrt1. TEXTZ	<'WARNING - Disk in drive@ is write'>
WrtPrt2. TEXTZ	<'protected.  Enable writing or replace'>
WrtPrt3. TEXTZ	<'disk.  PROCEED to retry.'>
MiscEr1. TEXTZ	<'WARNING - Error writing to drive@.'>
MiscEr2. TEXTZ	<'Replace disk and PROCEED.  New'>
MiscEr3. TEXTZ	<'disk replaces old disk in sequence.'>
ADOS1.	TEXTZ	<'WARNING - Disk in drive@ contains'>
ADOS2.	TEXTZ	<'AmigaDOS files.  Replace disk and '>
ADOS3.	TEXTZ	<'PROCEED to avoid losing these files.'>
BSET1.	TEXTZ	<'Backup disk in drive@ has a different'>
BSET2.	TEXTZ	<'backup date/time stamp.  It does not belong'>
BSET3.	TEXTZ	<'to the backup set you are restoring from.'>
BSEQ1.	TEXTZ	<'Disk % in drive@ is not the disk'>  
BSEQ2.	TEXTZ	<'needed next.  Please remove it and'>
NotQB3.
BSEQ3.	TEXTZ	<'PROCEED, or SKIP to bypass, or CANCEL.'>
NotQB1.	TEXTZ	<'Disk in drive@ is not a Quarterback'>
NotQB2.	TEXTZ	<'backup disk.  Load correct disk and'>
;NotQB3.	TEXTZ	<'PROCEED, or SKIP to bypass, or CANCEL.'>
BDSK1.	TEXTZ	<'Backup disk in drive@ is unusable.'>
BDSK2.	TEXTZ	<'Replace bad disk and PROCEED.  New'>
BDSK3.	TEXTZ	<'disk replaces old disk in sequence.'>
RdEr1.	TEXTZ	<'Read error on track 0 of disk in'>
RdEr2.	TEXTZ	<'drive@.  Can''t restore from bad disk.'>
RdEr3.	TEXTZ	<'Select SKIP to proceed with next disk.'>
CatEr1.	TEXTZ	<'Error reading catalog from first disk.'>
CatEr2.	TEXTZ	<'Select SKIP to switch to alternate'>
CatEr3.	TEXTZ	<'catalog on last backup disk.'>
Dup1.	TEXTZ	<'This file already exists:'>
Dup2.	TEXTZ	<'Do you want to replace it?'>
RecovErr. TEXTZ	<'Bad disk recovery error...aborting'>
With.	TEXTZ	'WITH'
Backup.	TEXTZ	'BACKUP'
Restore. TEXTZ	'RESTORE'
All.	TEXTZ	'ALL'
Date.	TEXTZ	'DATE'
UCInc.	TEXTZ	'INCLUDE'
UCExc.	TEXTZ	'EXCLUDE'
Arc.	TEXTZ	'ARCHIVE'
NoBatch. TEXTZ	<'I can''t find the command file.'>
BadSyntax.
	TEXTZ	<'Command file error...See manual.'>
ReadErr. TEXTZ	<'Read error on active disk'>
CantRes. TEXTZ	<'Unable to restore this file:'>
CantBak. TEXTZ	<'Unable to backup this file:'>
NoneSel. TEXTZ	<'No files selected to process.'>
NoDev.	TEXTZ	<'You must specify a primary device.'>
MixedDev. TEXTZ <'Primary and alternate devices must be of the same type.'>
SameDev.  TEXTZ	<'Alternate device cannot be same as primary device.'>
NoDFn.	  TEXTZ	<' is not available.'>
NotFSD.	  TEXTZ	<' is not a file storage device.'>
BDev1Txt. TEXTZ <'Backup files to this primary device:'>
BDev2Txt. TEXTZ <'Backup files to this alternate device:'>
RDev1Txt. TEXTZ	<'Restore files from this primary device:'>
RDev2Txt. TEXTZ	<'Restore files from this alternate device:'>

Err.	TEXTZ	<' ** Error **'>
CatError TEXTZ	<'Unable to read backup catalog.'>
CantFindAltCat TEXTZ <'Unable to find alternate catalog.'>
DirBlk.	TEXTZ	<'Directory blocks not released.'>
WrtErrRest. TEXTZ <'Write error while restoring file:'>
FileWrtPrt. TEXTZ <'Existing file is write protected.'>
FileRdPrt.  TEXTZ <'File is read protected.'>
FileInUse.  TEXTZ <'Existing file is in use.'>
FileDelPrt. TEXTZ <'Existing file protected from deletion.'>
DeviceFull. TEXTZ <'Device is full.'>
WrtErrCode. TEXTZ <'Write error code: '>
AllRes.	TEXTZ	<'All Rights Reserved'>
NoCat.		TEXTZ	<'There is no catalog to print.'>
For.		TEXTZ	<' for '>
	END
