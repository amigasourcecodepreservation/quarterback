/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software
 *
 *	Global variables
 */

#include <exec/types.h>
#include <libraries/dos.h>
#include <intuition/intuition.h>
#include <devices/console.h>

#include <Toolbox/DateTime.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/List.h>
#include <Toolbox/Language.h>

#include "QB.h"

/*
 *  Library base pointers
 */

struct IntuitionBase		*IntuitionBase;
struct GfxBase			*GfxBase;
struct Library			*LayersBase, *IconBase, *WorkbenchBase, *DiskfontBase;
struct Library			*DiskBase;
struct Device			*ConsoleDevice;
struct RexxLib			*RexxSysBase;

/*
 * Pathname for stuffing a full directory name
 */
TextPtr		strPath, strSavePath, destFileName, currFileName;

/*
 * General use buffer (i.e. concatenating a filename to a path)
 */
TextChar		strBuff[PATHSIZE+MAX_FILENAME_LEN];

/*
 *  Program variables
 */

TextChar		progPathName[100];
TextChar		printerName[FILENAME_SIZE + 3];

WORD			intuiVersion;

ScreenPtr	screen;
WindowPtr	backWindow;
MsgPortPtr	mainMsgPort;
MsgPortPtr	appIconMsgPort;
MsgPort		rexxMsgPort;
MenuPtr		menuStrip;

BOOL			fromCLI;

WORD			numWindows;				/* Inited to 0 */

RequestPtr	requester;				/* Current requester */

WindowPtr	cmdWindow;

WindowPtr	closeWindow;
BOOL			closeFlag;				
BOOL			quitFlag;

/*
 *	Function key menu command and AREXX command tables (without/with shift key)
 */

TextChar macroNames1[10][MACRONAME_LEN];
TextChar macroNames2[10][MACRONAME_LEN] = {
	"Macro_1", "Macro_2", "Macro_3", "Macro_4", "Macro_5",
	"Macro_6", "Macro_7", "Macro_8", "Macro_9", "Macro_10"
};

FKey	fKeyTable1[] = {
	{ FKEY_MENU, (Ptr) MENUITEM(UTILITIES_MENU, TAPE_UTILITIES_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(UTILITIES_MENU, SCSI_INTERROGATOR_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU, SAVEAS_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU, PAGESETUP_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU, PRINT_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(OPTIONS_MENU, BACKUP_OPTS_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(OPTIONS_MENU, RESTORE_OPTS_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(OPTIONS_MENU, CATALOG_OPTS_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(OPTIONS_MENU, LOGFILE_OPTS_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(OPTIONS_MENU, BUFF_OPTS_ITEM, NOSUB) },
};

#if (AMERICAN | BRITISH | FRENCH)
#define strMacroNames2 macroNames2
#else
extern TextChar strMacroNames2[10][32];
#endif

FKey	fKeyTable2[] = {
	{ FKEY_MACRO, &strMacroNames2[0][0] },
	{ FKEY_MACRO, &strMacroNames2[1][0] },
	{ FKEY_MACRO, &strMacroNames2[2][0] },
	{ FKEY_MACRO, &strMacroNames2[3][0] },
	{ FKEY_MACRO, &strMacroNames2[4][0] },
	{ FKEY_MACRO, &strMacroNames2[5][0] },
	{ FKEY_MACRO, &strMacroNames2[6][0] },
	{ FKEY_MACRO, &strMacroNames2[7][0] },
	{ FKEY_MACRO, &strMacroNames2[8][0] },
	{ FKEY_MACRO, &strMacroNames2[9][0] },
};

/*
 *	Parameters determined from printer driver
 */
 
BOOL	graphicPrinter, colorPrinter, pagePrinter;

TextAttr screenAttr = {
	"Topaz.font", 11, FS_NORMAL, FPF_ROMFONT | FPF_DISKFONT | FPF_DESIGNED
};

TextAttr smallAttr = {
	"Topaz.font", 8, FS_NORMAL, FPF_ROMFONT | FPF_DISKFONT | FPF_DESIGNED
};

StatStruct stats;

WORD	numColors;

/*
 * RGB values for display color registers
 */

/*
RGBColor stdColors[NUM_STDCOLORS];
RGBColor	screenColors[256];
*/

/*
 *	Gray patterns
 */

UWORD	blackPat[] = { 0xFFFF, 0xFFFF };
UWORD	grayPat[] = { 0xAAAA, 0x5555 };
UWORD	ltGrayPat[] = { 0x8888, 0x2222 };
UWORD	dkGrayPat[] = { 0xDDDD, 0x7777 };

/*
 * One instance of prefs directly worked with
 */
 
ProgPrefs prefs;

APTR SaveVec;	/* Save vector stolen from disk.resource */
struct Library *DRBase;

/*
 * Information pertaining to scrolling list in window
 */
 
WORD	firstLine, totalLines, topLine;
WORD	leftOffset, pageWidth, pages, pagePixWidth;

struct DateStamp sessionDateStamp, endDateStamp, firstSessionDateStamp;

UBYTE	*bufferQueue[MAX_NUM_BUFS];
UBYTE *allocBufferQ[MAX_NUM_BUFS];

WORD waitCount;			/* BeginWait() called, inited to 0 */
BOOL inMacro;				/* Executing macro, inited to FALSE */

WORD charWidth;			/* Stuffed at Init time */
WORD charHeight;			/* Stuffed at Init time */
WORD charBaseline;			/* Stuffed at Init time */

DevParamsPtr firstDevice;	/* The first device to backup/restore from in the list */
DevParamsPtr activeDevice;	/* The current device to backup or restore from */

ProcessPtr process;			/* Quarterback process, for convenience */

ULONG sigBits;				/* Signal bits for the main window */
UBYTE mode;					/* Are we doing a backup, restore, or neither? */
UBYTE operation;			/* At what point along is the backup or restore? */
UBYTE startMode;			/* Always same as mode unless comparing after backup */
UBYTE	encryptVal;			/* Private variable for encryption */
ULONG	encryptSize;			/* Number of bytes left to encrypt of catalog */

ScrollList scrollList;		/* Scroll list, always there. */

ULONG trkBufSize;			/* Buffer size to read/write in reality */
ULONG	currTrkBufSize;		/* Current buffer size for restoration */
ULONG	userTrkBufSize;		/* Buffer size to read/write that the user preferred */
ULONG tapeTrkBufSize;		/* For Dir-Acc tapes, the trkBufSize for this tape */
ULONG bufMemType;			/* Buffer memory type to use */
ULONG	writtenCount;			/* Number of bytes restored in file so far */
BOOL	skipToNext;			/* Skip to next restoration device? */

DirLevel rootLevel;			/* Root level instance */
DirLevelPtr curLevel;		/* Current level ptr */
DirLevelPtr	saveCurLevel;	/* Save the level when disk/tape started. */

BOOL	abortFlag;			/* Abort the operation in progress? */
BOOL	levelFlag;			/* TRUE=don't add size...this is level block */

DirFibPtr curFib;			/* Current dir block */
DirFibPtr baseDirFib;		/* Points to first allocated dir block */
DirFibPtr curDBBase;			/* Base of current dir block */
DirFibPtr curDBPtr;			/* Dir block ptr */
DirFibPtr saveCurFib;		/* Saved for disk change or reset */
ULONG curDBCount;			/* Count of remaining bytes in dir block */	

UWORD totalVols;			/* Total volumes */
UWORD selVols;				/* Selected volumes */
ULONG totalBytes;			/* Total size of all files */
ULONG selBytes;			/* Total size of selected files only */
ULONG totalFiles;			/* Total files in catalog */
ULONG selFiles;			/* Number of files selected */

WORD	indentLevel;			/* Directory level */
WORD	saveIndentLevel;		/* Saved directory level */
	
ULONG absolutePos;			/* Absolute position in backup disk set */
ULONG catSize;				/* Size of catalog */
ULONG	catCorruptAddition;	/* Additional size for corrupted file comments (rare) */
File 	fHandle;				/* Current file handle on backup/restore */

UBYTE *readPtr;			/* Pointer to read buffer */
UBYTE *startReadPtr;			/* Is ReadBuffer, unless alt-Cat w/ SyQuest or floptical */
UBYTE *readBuffer;			/* Track buffer used during read ops */
ULONG	readCount;			/* Read buffer byte count left in buffer */
Ptr	allocReadBuffer;		/* Where the buffer is actually allocated */
Ptr	compressBuffer;		/* Compression buffer used during compression only */

LONG	lowCylinder;			/* Low cylinder */
LONG	activeCylinder;		/* Current cylinder (same as session if rewind off) */
LONG	highCylinder;			/* High cylinder */
LONG	maxCylinders;			/* Maximum cylinder, or -1 if end not known */
LONG	sessionCurrCylinder;	/* Current cylinder for this session */

UWORD blocksPerTrack;		/* Blocks per track */
UWORD surfaces;			/* Number of heads/surfaces */
UWORD blockSize;			/* Block size */
UWORD reservedBlocks;		/* Reserved blocks */
ULONG volumeSize;			/* Volume size */
ULONG volumeOffset;			/* Offset from start of volume */

ULONG	fillCount;			/* Fill buffer byte count. KEEP DIRECTLY ABOVE fillPtr! */
Ptr	fillPtr;				/* Fill buffer data pointer. KEEP ONE BELOW fillCount! */
Ptr	fillBuffer;			/* Pointer to buffer being filled */
UWORD	activeDisk;			/* Number of volume in the backup set */
UWORD sessionCurrDisk;		/* Current disk for this session */
UWORD	firstTape;			/* First tape in backup/verify session */
UWORD numDevices;			/* I tossed this in, the number of Devices selected */

BOOL	inputCompleted;		/* No more data from source? */
BOOL	outputCompleted;		/* Last write now completed? */
BOOL	abortFlag;			/* User aborted operation? */
WORD	catFlag;				/* Writing catalog? - See QB.h for codes */
BOOL 	altCatFlag;			/* Alternate catalog starts on prior disk? */
BOOL	errFillFlag;			/* Error filling buffer */
BOOL	catErrFlag;			/* Catalog error flag */
BOOL	noSeqCheck;			/* Catalog sequence check */

BOOL	skipUpdate;			/* Prevent scroll list redraw on simple refresh */
TextPtr dialogVarStr;		/* Pointer to text to print in dialog */
LONG	dialogVarNum;			/* Number to print in dialog */
LONG	dialogVarNum2;			/* Secondary number to print */

UWORD tabbing[] = { 256,80,72, 80, 80 };		/* Pixel widths of rach field */
UWORD tabChars[] = { 32,10, 9, 10, 10 };		/* For printing (monospaced chars) */
UWORD tabTable[] = { 0, 0, 0, 0, 0 };			/* Equals "tabbing[x]" if to be shown */
UWORD matchDateFormat[] = {						/* Match enum to DateString(type) */
	DATE_MILITARY, DATE_SHORT, DATE_ABBR, DATE_ABBRDAY };
UWORD widthDateFormat[]	= {						/* Width in chars of date formats */
#if (AMERICAN | FRENCH | SWEDISH)
	 10, 9, 13, 18 };
#elif (BRITISH | GERMAN)
	 10,11, 13, 18 };
#endif
TextChar attrChars[] = { 'd', 'e', 'w', 'r', 'a', 'p', 's', '-' };

TextChar		currBackupName[MAX_BACKUPNAME_LEN];	/* Current name of session info */
TextChar		currVolName[MAX_BACKUPNAME_LEN];		/* Current name of volume */
TextChar		currComment[MAX_BACKUPCOMM_LEN];		/* Current comment of session info */

WORD			extraLen;
TextChar		sessionPassword[MAX_PASSWORD_LEN+1];
File			destFile;
UBYTE			readErr;
BYTE			lastReadBufferResult;
UBYTE			backupCompress;
UBYTE			restoreCompress;
ULONG			compressBufSize;

UBYTE			floppyDrives[] = { 0, 0, 0, 0 };
UBYTE			floppyEnable[] = { 0, 0, 0, 0 };
UBYTE			floppyBusy[]	= { 0, 0, 0, 0 };
UBYTE			floppy525[]		= { 0, 0, 0, 0 };
ImagePtr		iconList[NUM_MINI_ICONS];

BOOL			enterState;
MsgPortPtr	keyPort;						/* Need this for V1.3 alt key testing */
struct IOStdReq *keyRequest;			/* Need this for V1.3 alt key testing */
BOOL			tagsDone;
GadgetPtr	mainSelGadgets;			/* Pointer to enter, parent and tag buttons */
GadgetPtr	tagGadgets;					/* Pointer to tag buttons */
GadgetPtr	runtimeGadgets;			/* Pointer to replacement buttons during backup */
WORD			cpi, lpi;					/* Characters and lines per inch for printouts */
ListHeadPtr devList;						/* Pointer to list of possible backup/restore devices */
LONG			diskID;						/* The four-char identifer of the backup media */
WORD			numBufs = NUM_BUFS;		/* Number of buffers */
ULONG			tapeBufLen = 512;			/* Tape buffer length */
UBYTE			densityCode;				/* Density of tape- not currently used */
UBYTE			fixedBit;					/* If a sequential device, this bit is to be set */
BOOL			needRewind;					/* Is tape drive in need of rewinding? */
BOOL			initRewound;				/* Have we done an initial rewind? */
ULONG			numBlks;						/* Number of blocks for TapeIO() to use */
ULONG			tranLen;						/* Length for TapeIO() to use */
ULONG			tapeBlks;					/* If a direct-access tape, number of blocks */
ULONG			tapeSize;					/* If a direct-access tape, current tape size */
BOOL			sequentialDevice;			/* Is the backup/restore device a seq. tape drive? */
BOOL			tapeDevice;					/* Is the backup/restore device a tape drive? */
BOOL			useAmigaDOS;				/* Is backup/restore going to an AmigaDOS file? */
Ptr			fib;						/* Pointer to an allocated FileInfoBlock */
BOOL			firstProtErr;				/* If a previous protection bit error, save */
WORD			checkVolCounter;			/* How many intuiticks until next update */
ListHeadPtr	backupFilterList;			/* Pointer to backup filter list */
ListHeadPtr restoreFilterList;		/* Pointer to restore filter list */
UBYTE			iconType;					/* Saved icon byte field of list entry */
ULONG			bugMask;						/* A2090 needs bit 24 set on writes to tape */
WORD			qbCode;						/* On restore, first two bytes of first disk */
ULONG			linkCount;					/* Number of links encountered on restore */
WORD			currListItem;				/* Current scrollable list item # */
WORD			saveCurrListItem;			/* Delete everything after here when disk reset */
WORD			lastPercent;				/* Last percentage in bytes completed */
ULONG			lastTick;					/* Last tick that bytes completed was updated */
WORD			useCache;					/* For compression, since too much data was read */
Ptr			usePtr;						/* Read "useCache" bytes from here, not "readPtr" */ 
ULONG			pauseTicks;					/* Ticks to subtract to determine rate */
ULONG			bufferSize;					/* Buffer size of tape for first backup on tape */
UWORD			numVols;						/* Number of volumes in this backup (currently 1) */
UWORD			sessionNumber;				/* Zero, unless more than one session on a tape */
UWORD			firstTapeSession;			/* The first session number on this tape */
UWORD			firstSessionOnFirstTape;/* First session on first tape for verify */
BOOL			processAll;					/* On restore, scan entire backup even if selective? */
BOOL			needSpace;					/* Do we need to space before the next operation? */
UBYTE			prevMode;					/* For spacing, what was last mode? */
UBYTE			verifyMode;					/* Should restore actually compare or test? */
BOOL			disableDisplay;			/* Temporary copy of the session log variable */
TextPtr		currDevPtr;					/* Current SCSI device name */
WORD			currUnit;					/* Current SCSI unit ID */
WORD			errFillLevelCount;		/* If non-zero, pad files with zeros */
BOOL			resetDisk;					/* During backup, are we resetting a disk? */
Dir			saveCurrDir;				/* Current directory at time of ResetDisk() */
Dir			oldCurrDir;					/* Keep the actual QB current directory here */
BOOL			force;						/* If TRUE, don't bring up confirm dialog */
ListHeadPtr volumeList;					/* Volume list selected initially */
BOOL			noSCSI;						/* Bogus SCSI card in system. NO SCSI ALLOWED! */
ListHeadPtr	devProcVolList;			/* See Device.c as to why these are needed */
MsgPortPtr	*devProcIDArray;			/* Need to save because DeviceProc() fails */
WORD			useSortFormat;				/* 0 = backup made alphabetically, dirs first */
BOOL			rewritingCat;				/* If TRUE, rewriting first catalog on floppy */
BOOL			badDiskFlag;				/* Has the disk been marked as skipped on restore? */
ULONG			restoreVolumeSize;		/* For SyQuests, volume size during backup */
ULONG			numSCSICyls;				/* Number of blocks in SyQuest device */
WORD			verifyIOB;					/* Which IOB is the one to ReadAfterWrite()? */
BOOL			supportsSetMark;			/* Written out with setmarks? */
UBYTE			scriptError;				/* 1=trivial err, 2=moderate err, 3=serious */
UBYTE			backupType;					/* 0=Complete, 1=Selective, 2=Image */
BOOL			attemptCompressionPage;	/* Attempted to MODE_SENSE w/ this yet? */
WORD			statusIOBIndex;			/* Which IOB in the array is the status IOB? */
BOOL			noAutoSense;				/* Auto-Sense not supported */
BOOL			notSCSI;						/* Current device not SCSI */
BOOL			useRecalcCatSize;			/* Use catalog's account of how big catSize is */
BOOL			restartCatalog;			/* Restart writing/reading at end of tape */
