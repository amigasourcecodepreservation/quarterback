/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Quarterback
 * Copyright (c) 1992-93 New Horizons Software, Inc.
 *
 * About box support routines
 */

#include <exec/types.h>

#include <Toolbox/Globals.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Utility.h>

#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include <typedefs.h>
#include <string.h>

#include "QB.h"
#include "DlogDefs.h"
#include "Proto.h"

/*
 * External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;
extern TextPtr		aboutStrings[];
extern WindowPtr	cmdWindow;

extern DlgTemplPtr	dlgList[];

/*
 * Local variables and definitions
 */
 
#define NUM_LINES			(12)
#define BASE_SLOPE			(-540)
#define NUM_ABOUT_STRINGS	(2)

static WORD		direction, index, xInc, textStart, xStage;
static UBYTE	penGreen;

/*
 *	Local prototypes
 */

static RastPtr	BeginAboutBox(DialogPtr, RectPtr);
static void		EndAboutBox(RastPtr);
static void		DrawAboutBox(DialogPtr, RastPtr, RectPtr);

/*
 * Prepares about box rendering - returns gadget rectangle and created rastport.
 */
 
static RastPtr BeginAboutBox(DialogPtr dlg, register RectPtr rect)
{
	RastPtr		rPort;
	Rectangle	drawRect;

	direction = -1;							// Initialize about box globals
	xStage = textStart = index = 0;
	GetGadgetRect(GadgetItem(dlg->FirstGadget, ABOUT_BORDER), dlg, NULL, rect);
	InsetRect(rect, 1, 1);
	xInc = ((rect->MaxX - rect->MinX)/NUM_LINES) + 1;
	drawRect = *rect;
	OffsetRect(&drawRect, -rect->MinX, -rect->MinY);	
	rPort = CreateRastPort(drawRect.MaxX + 1, drawRect.MaxY + 1, screen->RastPort.BitMap->Depth);
	if (rPort) {
		SetAPen(rPort, _tbPenLight);
		FillRect(rPort, &drawRect);
		SetBPen(rPort, _tbPenLight);
		SetDrMd(rPort, JAM2);
		SetFont(rPort, dlg->RPort->Font);
	}
	return (rPort);
}
	
/*
 * Deallocates structures of BeginAboutBox().
 */
 
static void EndAboutBox(RastPtr rPort)
{
	DisposeRastPort(rPort);
}

/*
 * Draw a single animation frame for the about box.
 */

static void DrawAboutBox(DialogPtr dlg, RastPtr rPort, RectPtr rect)
{
	WORD	i, topY, slope, xPos, slopeInc, absWidth, absHeight, width, numLines;
	TextPtr	text;

	absWidth = (rect->MaxX - rect->MinX) + 1;
	absHeight = (rect->MaxY - rect->MinY) + 1;
	topY = absHeight/3;
//	topY = 22;

	width = absWidth;
	numLines = NUM_LINES;
	xPos = xStage;
	slopeInc = (BASE_SLOPE + BASE_SLOPE) / NUM_LINES;
	slope = BASE_SLOPE - ((xPos * slopeInc) / xInc);
/*
	Draw text strings
*/
	text = aboutStrings[index];
	if (absWidth + textStart + TextLength(rPort, text, strlen(text)) < 0) {
		index++;
		if (index >= NUM_ABOUT_STRINGS)
			index = 0;
		textStart = 0;
	} 
	SetAPen(rPort, _tbPenBlack);
	Move(rPort, absWidth + textStart, (topY - rPort->TxHeight)/2 + rPort->TxBaseline);
	Text(rPort, text, strlen(text));
	textStart--;
/*
	Draw football field
*/
	SetAPen(rPort, penGreen);
	RectFill(rPort, 0, topY, absWidth, absHeight);
	SetAPen(rPort, _tbPenWhite);
	Move(rPort, 0, topY);
	Draw(rPort, absWidth, topY);
	for (i = 0; i <= numLines; i++) {
		Move(rPort, xPos, topY);
		Draw(rPort, xPos + ((slope*(absHeight - topY)) >> 8), absHeight + topY);
		slope -= slopeInc;
		xPos += xInc;
	}
/*
	Copy image to screen and update variables for next pass
*/
	WaitBOVP(&screen->ViewPort);
	BltBitMapRastPort(rPort->BitMap, 0, 0, dlg->RPort, rect->MinX, rect->MinY, absWidth, absHeight, 0xC0);
	xStage += direction;
	if (ABS(xStage) == xInc)
		xStage = 0;
}

/*
 * Open the about box, wait for "OK" to be hit, close it, return TRUE if opened OK.
 */
 
BOOL DoAboutBox()
{
	WORD		item;
	ULONG		secs, micros, currSecs, currMicros;
	BOOL		firstTime;
	DialogPtr	dlg;
	RastPtr		rPort;
	Rectangle	rect;

	BeginWait();
	dlgList[DLG_ABOUT]->Gadgets[LICENSE_STATTEXT].Info = NULL;
	dlgList[DLG_ABOUT]->Gadgets[LICENSE_STATTEXT + 1].Info = NULL;
	if ((dlg = GetDialog(dlgList[DLG_ABOUT], screen, mainMsgPort)) != NULL) {
		OutlineOKButton(dlg);
		penGreen = PenNumber(0x3A3, screen->ViewPort.ColorMap, 1 << screen->RastPort.BitMap->Depth);
		rPort = BeginAboutBox(dlg, &rect);		// NOTE: A NULL return IS handled
		CurrentTime(&secs, &micros);
		firstTime = TRUE;
		do {
			item = CheckDialog(mainMsgPort, dlg, DialogFilter);
			if (rPort == NULL)
				continue;
			CurrentTime(&currSecs, &currMicros);
			if ((DiffTimeMilli(secs, micros, currSecs, currMicros) > 1000) || firstTime) {
				firstTime = FALSE;
				DrawAboutBox(dlg, rPort, &rect);
			}
		} while (item != OK_BUTTON);
		EndAboutBox(rPort);
		DisposeDialog(dlg);
	}
	EndWait();
	return (dlg != NULL);
}
