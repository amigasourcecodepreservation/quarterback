/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/intuition.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Window.h>

#include <string.h>

#include "QB.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr	backWindow;
extern struct RexxLib *RexxSysBase;
extern UBYTE		mode, operation;
extern ProgPrefs	prefs;
extern TextChar	strPrCat[], strPrLog[];
extern TextChar	strSaveCatAs[], strSaveLogAs[];
extern BOOL			noSCSI, tapeDevice, sequentialDevice;
extern UWORD		sessionNumber;

/*
 *	Local prototypes
 */

void	OnOffMenu(WindowPtr, LONG, BOOL);

/*
 *	Turn on or off the specified menu or menu item
 */

void OnOffMenu(WindowPtr window, LONG menuNum, BOOL on)
{
	if (on)
		OnMenu(window, menuNum);
	else
		OffMenu(window, menuNum);
}

/*
 *	Set up project menu
 */

void SetProjectMenu()
{
	register BOOL isCat = (operation == OPER_SELECTION) || (operation == OPER_LIMBO);
	
	SetMenuItem(backWindow, MENUITEM(PROJECT_MENU, SAVEAS_ITEM, NOSUB), isCat ? strSaveCatAs: strSaveLogAs);
	SetMenuItem(backWindow, MENUITEM(PROJECT_MENU, PRINT_ITEM, NOSUB), isCat ? strPrCat : strPrLog);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, SAVEAS_ITEM, NOSUB), mode != MODE_LIMBO );
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, PRINT_ITEM, NOSUB), mode != MODE_LIMBO);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, SETTINGS_ITEM, LOADSETTINGS_SUBITEM), isCat || (operation == OPER_COMPLETE) );
}

/*
 * Adjust the Options menu
 */
 
void SetOptionsMenu()
{
	OnOffMenu(backWindow, MENUITEM(OPTIONS_MENU, BUFF_OPTS_ITEM, NOSUB), !(tapeDevice && !sequentialDevice && sessionNumber) &&
		((operation == OPER_LIMBO) || (operation == OPER_COMPLETE) || (operation == OPER_SELECTION && mode == MODE_BACKUP)) );
	OnOffMenu(backWindow, MENUITEM(OPTIONS_MENU, BACKUP_OPTS_ITEM, NOSUB), (mode != MODE_RESTORE) );
	OnOffMenu(backWindow, MENUITEM(OPTIONS_MENU, RESTORE_OPTS_ITEM, NOSUB), (mode != MODE_BACKUP) );
}

/*
 * Adjust the Utilities menu
 */
 
void SetUtilitiesMenu()
{
	/* OnOffMenu(backWindow, MENUITEM(UTILITIES_MENU, NOITEM, NOSUB), operation != OPER_LIMBO); */
	OnOffTapeMenuItem();
	OnOffMenu(backWindow, MENUITEM(UTILITIES_MENU, GET_SESSINFO_ITEM, NOSUB),
		(mode == MODE_RESTORE) || (operation == OPER_IN_PROG) || (operation == OPER_PAUSE) || (operation == OPER_COMPLETE));
	OnOffMenu(backWindow, MENUITEM(UTILITIES_MENU, SET_SESSINFO_ITEM, NOSUB),
		(mode == MODE_BACKUP) && ((operation == OPER_SELECTION) || (operation == OPER_PREP)) );
	OnOffMenu(backWindow, MENUITEM(UTILITIES_MENU, SCSI_INTERROGATOR_ITEM, NOSUB), !noSCSI);
}

/*
 *	Adjust Macro menu
 *	Only needs to be called once at program start-up
 */

void SetMacroMenu()
{
	OnOffMenu(backWindow, MENUITEM(MACRO_MENU, NOITEM, NOSUB), (BOOL) RexxSysBase);
}

/*
 * Adjust the "Tape Options" menu item.
 */
 
void OnOffTapeMenuItem()
{
	OnOffMenu(backWindow, MENUITEM(UTILITIES_MENU, TAPE_UTILITIES_ITEM, NOSUB),
		!noSCSI && (operation != OPER_IN_PROG) && (operation != OPER_PAUSE));
}

/*
 * Set up all menus
 */
	 
void SetAllMenus()
{
	SetProjectMenu();
	OnOffMenu(backWindow, MENUITEM(TAG_MENU, NOITEM, NOSUB), operation == OPER_SELECTION );
	SetOptionsMenu();
	SetUtilitiesMenu();
	SetMacroMenu();
}
