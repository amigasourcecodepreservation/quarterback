/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Buffer/queue routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Utility.h>

#include "QB.h"
#include "Proto.h"

/*
 *	External variables
 */

extern DevParamsPtr firstDevice;
extern ProgPrefs prefs;
extern LONG		trkBufSize, bufMemType;

extern UBYTE	*bufferQueue[], *allocBufferQ[];
extern UBYTE	*readPtr, *readBuffer, *startReadPtr, *allocReadBuffer;

extern UBYTE	mode;
extern WORD		numBufs, intuiVersion;
extern ULONG	bugMask;

/*
 * Allocate I/O buffers, up to MAX_NUM_BUFS.
 * Return FALSE if failure.
 */
 
BOOL AllocateBuffers(ULONG readBufferSize)
{
	register WORD i;
	register UBYTE *buf;
	register WORD num = numBufs;
	BOOL success = TRUE;
	ULONG memType = firstDevice->Floppy ? bufMemType : prefs.BuffOpts.MemType;
	BOOL changed = FALSE;
	register ULONG size = trkBufSize + 16;		/* Quad longword pad */
/*
	Uncomment below if we'd rather get the unused memory in exchange
	for the convenience of being able to change the "slow backup"
	variable after file selection mode.
*/
	if( (mode == MODE_RESTORE) /*|| (prefs.OldPrefs.SlowBackup)*/ ) {
		num = 1;
	}
	BlockClear( &bufferQueue[0], sizeof(Ptr) * MAX_NUM_BUFS);
	for( i = 0 ; i < num ; i++ ) {
/*
	If not OS 2.0, turn off 24BITDMA bit.
*/
		if( (memType & MEMF_24BITDMA) && (intuiVersion < OSVERSION_2_0) ) {
			memType &= ~MEMF_24BITDMA;
			changed = TRUE;
		}
/*
	If bugMask turned on, don't use memory addresses with non-zero top byte!
*/
		if( bugMask ) {
			memType = MEMF_CHIP;
		}
		buf = AllocMem(size, memType);
/*
	If non-chip selected and buffer allocated falls into non-DMAable space,
	use chip memory instead.
*/
		if( changed && (((ULONG)buf) & 0xFFFFFF) ) {
			FreeMem(buf, size);
			buf = AllocMem(size, MEMF_CHIP);
		}
		if( buf != NULL ) {
			allocBufferQ[i] = buf;
/*	
	Force to Quad-longword for 68040 cache compatibility.
*/
			if( ((ULONG)buf) & 0xF ) {
				buf = (UBYTE *) (((ULONG)buf) | 0xF);
				buf++;
			}
			bufferQueue[i] = buf;
		} else {
			success = FALSE;
		}
	}
	if( readBufferSize ) {
		if( success = (buf = AllocMem(size, memType)) != NULL ) {
			allocReadBuffer = buf;
			if( ((ULONG) buf) & 0xF ) {
				buf = (UBYTE *) (((ULONG)buf) | 0xF);
				buf++;
			}
			readBuffer = buf;
		}
	}
	if( !success ) {			/* If a failure, free up what was allocated so far. */
		FreeBuffers();
	}
	startReadPtr = readPtr = readBuffer;

	return(success);
}

/*
 * Free all buffers everywhere, provided we are not in need of rewinding.
 */
 
void FreeBuffers()
{
	register WORD i;
	register ULONG size = trkBufSize + 16;
	
	if( readBuffer != NULL ) {
		FreeMem(allocReadBuffer, size);
		readBuffer = NULL;
	}
	for( i = 0 ; i < MAX_NUM_BUFS ; i++ ) {
		if( bufferQueue[i] != NULL ) {
			FreeMem(allocBufferQ[i], size);
		}
		bufferQueue[i] = NULL;
	}
}
