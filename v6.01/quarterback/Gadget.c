/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Gadget routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/ScrollList.h>

#include "QB.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WORD			intuiVersion;

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern TextPtr		strPath, strSavePath, dialogVarStr;
extern TextChar	strBuff[], strColon[], strDrawer[];
extern TextChar	strDescLimbo[], strDescLimboDir[];

extern WORD			topLine, totalLines, leftOffset, pageWidth, pagePixWidth;
extern WORD			charHeight, charWidth;

extern WindowPtr	cmdWindow, backWindow;
extern ScrollListPtr	scrollList;
extern RequestPtr	requester;

extern UBYTE		mode, operation;
extern BOOL			abortFlag, disableDisplay;
extern WORD			currListItem;

extern DirLevelPtr	curLevel;

/*
 *	Adjust vertical and horizontal scroll bars to current values
 */

void AdjustScrollBars(WindowPtr window)
{
	register LONG totAmount, visAmount, currPos, newPot, newBody;
	register GadgetPtr gadget;
	register PropInfoPtr propInfo;
	Rectangle rect;
/*
	Adjust horizontal scroll bar
*/
	if ((gadget = GadgetItem(window->FirstGadget, HORIZ_SCROLL)) != NULL &&
		!(gadget->Flags & SELECTED)) {
		GetGadgetRect(scrollList->ListBox, window, NULL, &rect);
		propInfo = (PropInfoPtr) gadget->SpecialInfo;
		totAmount = pagePixWidth;
		visAmount = (rect.MaxX - rect.MinX) /* charWidth;*/;
		currPos = leftOffset * charWidth;
		
		if( totAmount < visAmount + currPos )
			totAmount = visAmount + currPos;
		newPot = (totAmount == visAmount ) ? 0xFFFF : (0xFFFFL*currPos)/(totAmount - visAmount);
		newBody = (0xFFFFL*visAmount)/totAmount;
		if (newPot != propInfo->HorizPot || newBody != propInfo->HorizBody) {
			/* WaitBOVP(&screen->ViewPort); */
			NewModifyProp(gadget, window, NULL, AUTOKNOB | FREEHORIZ,
						  newPot, 0, newBody, 0xFFFF, 1);
		}
	}
}

/*
 *	Adjust horizontal position to value in scroll box
 */

void AdjustToHorizScroll(WindowPtr window)
{
	register LONG visAmount, totAmount, currPos, newPos;
	register GadgetPtr gadget;
	register PropInfoPtr propInfo;
	Rectangle rect;
	WORD cols;
	
	if ((gadget = GadgetItem(window->FirstGadget, HORIZ_SCROLL)) != NULL) {
		propInfo = (PropInfoPtr ) gadget->SpecialInfo;
/*
	Get the top line
*/
		GetGadgetRect(scrollList->ListBox, window, NULL, &rect);
		cols = (rect.MaxX - rect.MinX) / charWidth;
		visAmount = cols;
		totAmount = pageWidth;
		currPos = leftOffset;
		if (totAmount - currPos < visAmount)
			totAmount = visAmount + currPos;
		newPos = propInfo->HorizPot*(totAmount - visAmount);
		newPos = (newPos + 0x7FFFL)/0xFFFFL;
		if( newPos - currPos ) {
			SLHorizScroll(scrollList, charWidth * (newPos - currPos));
			leftOffset = newPos;
		}
	}
}

/*
 *	Scroll left one character
 */

void ScrollLeft(WindowPtr window)
{
	if( leftOffset ) {
		leftOffset--;
		SLHorizScroll(scrollList, -charWidth);
		AdjustScrollBars(window);
	}
}

/*
 *	Scroll right one character
 */

void ScrollRight(WindowPtr window)
{
	Rectangle rect;
	WORD cols;
	
	GetGadgetRect(scrollList->ListBox, window, NULL, &rect);
	cols = (rect.MaxX - rect.MinX) / charWidth;
	if( leftOffset + cols < pageWidth ) {
		leftOffset++;
		SLHorizScroll(scrollList, charWidth);
		AdjustScrollBars(window);
	}
}

/*
 * Process cursor key
 */
 
void DoCursorKey(UWORD code, UWORD modifier)
{
	register ScrollListPtr scrollListReg = scrollList;
	register WORD items;
	register WORD listItem;
	register WORD offItem;
	register WORD direction;
	BOOL select;
	BOOL bumpSel;
	
	if( code == CURSORUP || code == CURSORDOWN ) {
		if( scrollListReg != NULL ) {
			listItem = SLNextSelect(scrollListReg, -1);
			items = SLNumItems(scrollListReg);
			bumpSel = SLNextSelect(scrollListReg, listItem) == -1;
			if( items && (listItem != -1) ) {
				if( code == CURSORUP ) {
					direction = -1;
					select = listItem != 0;
				} else {
					direction = 1;
					/*select = listItem < (items - 1);*/
					/*offItem = listItem;*/
					listItem--;
					do {
						offItem = SLNextSelect(scrollListReg, ++listItem);
					} while( (offItem - listItem) == 1 );
					select = (listItem+1) != items;
				}
				if( modifier & SHIFTKEYS ) {
					if( !select ) {
						ErrBeep();
					}
					select = TRUE;
					bumpSel = TRUE;
				} else if( select || !bumpSel ) {
					while( (offItem = SLNextSelect(scrollListReg, -1)) != -1 ) {
						SLSelectItem(scrollListReg, offItem, FALSE);
					}
					if( !select ) {
						select = TRUE;
						direction = 0;
					}
				}
				switch(operation) {
				case OPER_LIMBO:
				case OPER_SELECTION:
					if( select ) {
						if( bumpSel ) {
							listItem += direction;
						}
						SLSelectItem(scrollListReg, listItem, TRUE);
						SLAutoScroll(scrollListReg, listItem);
						UpdateEnterGadget();
					} else {
						ErrBeep();
					}
				default:
					break;
				}
			}
		}
	} else if( code == CURSORRIGHT || code == CURSORLEFT ) {
		if( (scrollList != NULL) && (operation == OPER_SELECTION) || (operation == OPER_LIMBO) ) {
			if( code == CURSORRIGHT ) {
				DepressGadget( GadgetItem(cmdWindow->FirstGadget, BUTTON_ENTER), cmdWindow, NULL);
				DoEnterDirectory();
			} else {
				DepressGadget( GadgetItem(cmdWindow->FirstGadget, BUTTON_PARENT), cmdWindow, NULL);
				DoParentDirectory(modifier & ALTKEYS);
			}
		}
	}
}

/*
 * Enter the directory via right arrow or "Enter" gadget hit.
 */
 
void DoEnterDirectory()
{
	register WORD prevItem;
	register WORD listItem;
	register DirFibPtr dirFib;
	
	if( mode != MODE_LIMBO ) {
		dirFib = curLevel->dl_ChildPtr;
	}
	prevItem = listItem = -1;
	while( (listItem = SLNextSelect(scrollList, listItem)) != -1 ) {
		if( mode != MODE_LIMBO ) {
			while( ++prevItem < listItem ) {
				dirFib = dirFib->df_Next;
			}
			if( dirFib->df_Flags & FLAG_DIR_MASK 
				/*&& (((DirLevelPtr) dirFib->df_SubLevel)->dl_ChildPtr != NULL)*/ ) {
				EnterDirectory(dirFib);
				break;
			}
		} else {
			SetGadgetItemText(cmdWindow->FirstGadget, DESCR_TEXT, cmdWindow, NULL, strDescLimboDir);

			SLGetItem(scrollList, listItem, strBuff);
			if( strPath[0] == 0 ) {
				OnGadget(GadgetItem(cmdWindow->FirstGadget, BUTTON_PARENT), cmdWindow, NULL);
				strcat(&strBuff[2], strColon);
			}
			AppendDirPath(strPath, &strBuff[2]);
			BuildVolumeList(strPath);
		}
		prevItem--;
	}
}

/*
 * Go to parent of current directory, via left arrow or "Parent" gadget hit.
 * If "root" TRUE, go all the way to root.
 */
  
void DoParentDirectory(BOOL root)
{
	DirFibPtr parentDirFib;
	TextChar origVolName[MAX_FILENAME_LEN+1];
	TextPtr volPtr;
	WORD len;
	
	if( mode == MODE_LIMBO ) {
		if( strPath[0] ) {
			if( root || (strPath[strlen(strPath)-1] == ':') ) {
				len = strchr(strPath, ':') - &strPath[0];
				stccpy(origVolName, strPath, len+1);	/* Zap colon */
				volPtr = origVolName;
				strPath[0] = 0;
			} else {
				strcpy(strSavePath, strPath);
				TruncatePath();
				volPtr = &strSavePath[strlen(strPath)+1];
				if( strPath[strlen(strPath)-1] == ':' ) {
					volPtr--;
				}
			}
			if( strPath[0] == 0 ) {
				SetGadgetItemText(cmdWindow->FirstGadget, DESCR_TEXT, cmdWindow, NULL, strDescLimbo);
				OffGadget(GadgetItem(cmdWindow->FirstGadget, BUTTON_PARENT), cmdWindow, NULL);
			}
			BuildVolumeList(volPtr);
		} else {
			ErrBeep();
		}
	} else if( curLevel->dl_ParLevel != NULL ) {
		parentDirFib = curLevel->dl_ParFib;
		UpdateDirStatus(root);
		BuildFileList(root ? NULL : parentDirFib);
	}
}

/*
 *	Handle gadget down event
 */

void DoGadgetDown(WindowPtr window, IntuiMsgPtr intuiMsg)
{
	BOOL reply = TRUE;
	GadgetPtr gadget = (GadgetPtr) intuiMsg->IAddress;
	WORD item = GadgetNumber(gadget);
	static BOOL enableDoubleClick;	/* Jim doesn't like the third clicks toggling */
/*
	Is main window gadget?
*/	
	if( window == cmdWindow ) {
		switch( item ) {
		case LEFT_ARROW:
			TrackGadget(mainMsgPort, window, gadget, (void (*) (WindowPtr, WORD)) ScrollLeft);
			break;
		case RIGHT_ARROW:
			TrackGadget(mainMsgPort, window, gadget, (void (*) (WindowPtr, WORD)) ScrollRight);
			break;
		case HORIZ_SCROLL:
			TrackGadget(mainMsgPort, window, gadget, (void (*) (WindowPtr, WORD)) AdjustToHorizScroll);
			AdjustScrollBars(window);
			break;
		case SCROLL_LIST:
		case UP_ARROW:
		case DOWN_ARROW:
		case VERT_SCROLL:
			if( operation == OPER_SELECTION ) {
				if( (SLNextSelect(scrollList, -1) == -1) && SLNumItems(scrollList) ) {
					OnGList(GadgetItem(window->FirstGadget, BUTTON_TAG), window, NULL, 2);
					OnMenu(backWindow, MENUITEM(TAG_MENU, TAG_ITEM, NOSUB));
					OnMenu(backWindow, MENUITEM(TAG_MENU, UNTAG_ITEM, NOSUB));
				}
			}
/*
	Don't allow hilighting of anything in the list during backup/restore operations.
 */
			if( (operation == OPER_SELECTION) || (operation == OPER_LIMBO) || (item != SCROLL_LIST) ) {
				if( disableDisplay && (operation != OPER_PAUSE) ) {
					EnableScrollList();
					SLGadgetMessage(scrollList, mainMsgPort, intuiMsg);
					DisableScrollList();
				} else {
					SLGadgetMessage(scrollList, mainMsgPort, intuiMsg);
				}
				reply = FALSE;
				if( item == SCROLL_LIST ) {
					if( SLIsDoubleClick(scrollList) && ((intuiMsg->Qualifier & SHIFTKEYS) == 0) ) {
						if( enableDoubleClick ) {
							DoDoubleClick(SLDoubleClickItem(scrollList));
						}
						enableDoubleClick = !enableDoubleClick;
					} else {
						enableDoubleClick = TRUE;
					}
					UpdateEnterGadget();
				}
			}
			break;
		}
	}
	if( reply ) {
		ReplyMsg( (MsgPtr) intuiMsg);
	}
}

/*
 *	Handle gadget up event
 */

void DoGadgetUp(WindowPtr window, IntuiMsgPtr intuiMsg)
{
	DoGadgetItem(window, intuiMsg, GadgetNumber((GadgetPtr)intuiMsg->IAddress) );
}

/* 
 * Process the specified gadget item
 */
 
void DoGadgetItem(WindowPtr window, IntuiMsgPtr intuiMsg, WORD item)
{
	BOOL reply = TRUE;
	BOOL altOn;
	
	if( cmdWindow == window ) {
		if( intuiMsg != NULL ) {
			altOn = (intuiMsg->Qualifier & ALTKEYS) != 0;
		}
		switch (item) {
		case BUTTON_1:
		case BUTTON_2:
			HandleOperationButton(item == BUTTON_1, altOn);
			break;
		case HORIZ_SCROLL:
			AdjustToHorizScroll(window);
			break;
		case UP_ARROW:
		case DOWN_ARROW:
		case VERT_SCROLL:
			if( intuiMsg != NULL ) {
				SLGadgetMessage(scrollList, mainMsgPort, intuiMsg);
				reply = FALSE;
			}
			break;
		case BUTTON_ENTER:
			DoEnterDirectory();
			break;
		case BUTTON_PARENT:
			DoParentDirectory(altOn);
			break;
		case BUTTON_TAG:
		case BUTTON_UNTAG:
			DoTag(item == BUTTON_TAG, altOn);
		default:
			break;
		}
	}
	if( reply && intuiMsg != NULL ) {
		ReplyMsg( (MsgPtr) intuiMsg);
	}
	return;
}

