/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Display routines
 */

#include <exec/types.h>
#include <dos/dos.h>
#include <intuition/intuition.h>
#include <graphics/gfxmacros.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/icon.h>
#include <proto/utility.h>

#include <Toolbox/Border.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Request.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/DateTime.h>
#include <Toolbox/Utility.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Image.h>

#include "QB.h"
#include "Proto.h"
#include "DlogDefs.h"

extern ProgPrefs	prefs;

extern TextChar	strBuff[], strScanVol[], strScanDir[], strScanDrive[];
extern TextChar	strTag[], strUntag[], strTagAll[], strUntagAll[], strBack[], strRoot[];
extern TextPtr		dialogVarStr;
extern TextPtr		strPath;

extern LONG			dialogVarNum;
extern WORD			leftOffset, pageWidth, pagePixWidth;
extern WindowPtr	cmdWindow, backWindow;

extern UWORD		tabbing[], tabChars[], widthDateFormat[], matchDateFormat[], tabTable[];
extern WORD			charWidth, charHeight, charBaseline;
extern TextChar	attrChars[];

extern DevParamsPtr	firstDevice;
extern TextPtr		driveStrings[];

extern UBYTE		startMode, mode, operation;

extern RequestPtr	requester;
extern ScrollListPtr	scrollList;
extern DirLevelPtr	curLevel;

extern UBYTE		_tbPenWhite, _tbPenDark, _tbPenBlack, _tbPenLight;
extern WORD			_tbXSize;
extern UWORD		grayPat[];
extern ScreenPtr	screen;

extern ImagePtr	iconList[];
extern BOOL			enterState, inputCompleted, tapeDevice, useAmigaDOS;
extern WORD			indentLevel;

extern UBYTE		floppyEnable[], floppyDrives[], floppy525[], floppyBusy[];

extern MsgPortPtr	mainMsgPort;
extern TextChar	strWrongPass[], strRenameFile[];
extern TextChar	strTotal[], strCompleted[], strSel[], strFiles[], strBytes[];
extern TextChar	strHardLink[], strSoftLink[], strSubdir[], strQuote[];
extern TextChar	strDevNReady[], strDevReady[], strDevCheck[], strDevCheckF[];
extern TextChar	strDevActiveB[], strDevActiveR[], strDevNotSel[], strDevNAvail[];
extern TextChar	strDevActiveBF[], strDevActiveRF[], strDevDone[], strDevRemove[];
extern TextChar	strSessionNum[];
extern ULONG		totalFiles, totalBytes, selFiles, selBytes;

extern TextChar	strDevPaused[], strSpace[], strRAM[], currVolName[], strSkippedErr[];

extern WORD			checkVolCounter;

extern UBYTE		iconType, verifyMode;
extern Ptr			fib;
extern WORD			currListItem;
extern WORD			lastPercent;
extern ULONG		lastTick;
extern UWORD		numDevices, sessionNumber;
extern BOOL			disableDisplay, skipUpdate;
extern BOOL			outputCompleted, catErrFlag, badDiskFlag, abortFlag, notSCSI;
extern DirFibPtr	curFib;
extern WORD			saveCurrListItem;
extern StatStruct	stats;

/*
 *	Icon data
 */

static WORD iconNumPts[] = {
	16, 5, 8, 9, 5, 5,
	 6, 4, 5, 8, 4, 4, 5
};

static WORD iconX[] = {
	12, 12, 4, 4, 2, 2, 5, 5, 11, 11, 13, 14, 14, 2, 2, 13,
	1, 15, 15, 1, 1,
	9, 9, 12, 12, 4, 4, 9, 12,
	0, 1, 14, 15, 15, 14, 1, 0, 0,
	4, 8, 12, 8, 4,
	2, 14, 14, 2, 2,
	
	2, 13, 14, 14, 2, 2,
	1, 15, 15, 1,
	9, 12, 12, 4, 4,
	0, 1, 14, 15, 15, 14, 1, 0,
	4, 8, 12, 8,
	2, 14, 14, 2,
	0, 1, 2, 6, 2		/* This is the checkmark mask AND image (one plane) */
};

static WORD iconY[] = {
	9, 5, 5, 9, 9, 1, 1, 3, 3, 1, 1, 2, 9, 9, 1, 1,
	4, 4, 8, 8, 4,
	0, 3, 3, 9, 9, 0, 0, 3,
	4, 4, 4, 4, 9, 9, 9, 9, 4,
	5, 1, 5, 9, 5,
	3, 3, 9, 9, 3,
	
	1, 1, 2, 9, 9, 1,
	4, 4, 8, 8,
	0, 3, 9, 9, 0,
	4, 4, 4, 4, 9, 9, 9, 9,
	5, 1, 5, 9,
	3, 3, 9, 9,
	5, 6, 9, 0, 9
};

static RastPtr prevScanReqRP;


/*
 * Local Prototypes
 */

static LONG		AddQBListItem(ListHeadPtr, TextPtr, WORD);
static void		DoStuffScanText(TextPtr, WORD);
static void		StuffScanText(TextPtr, WORD);
static ImagePtr ConstructIconImage(WORD);
static ImagePtr GetIconAndMask(BOOL, BOOL, APTR *);
static void		DrawCText(TextPtr, WORD);
static void		ConstructIconField(TextPtr, UBYTE);
static void 	TweakCorners(Point *, BOOL);
static void		TweakDiamond(Point *, BOOL);
static void		DisplayDriveStatus(DevParamsPtr, WORD);

/*
 * Local defines
 */

#define PASS_EDIT_TEXT		2
#define PASS_STAT_TEXT		3

#define START_X			70
#define END_X				(BUTTONS_WIDTH - 20)

#define SCALE(x, srcWidth, dstWidth)	\
	(((LONG) (x)*(dstWidth - 1) + (srcWidth - 1)/2)/(srcWidth - 1))

#define INDENT_PIXELS		16

/*
 * Set the button text of those buttons which can change when "ALT" is depressed.
 */

void SetAltButtons(BOOL altOn)
{
	BOOL sel;
	static BOOL lastAltOn;
	
	if( (operation == OPER_SELECTION) || (operation == OPER_LIMBO) ) {
		if( lastAltOn != altOn ) {
			if( altOn ) {
				if( operation == OPER_SELECTION ) {
#if !GERMAN
					SetButtonItem(cmdWindow->FirstGadget, BUTTON_TAG, cmdWindow, NULL, strTagAll, 0);
					SetButtonItem(cmdWindow->FirstGadget, BUTTON_UNTAG, cmdWindow, NULL, strUntagAll, 0);
#endif				
					EnableGadget(cmdWindow, BUTTON_TAG, curLevel->dl_ChildPtr != NULL);
					EnableGadget(cmdWindow, BUTTON_UNTAG, curLevel->dl_ChildPtr != NULL);
				}
#if !GERMAN
				SetButtonItem(cmdWindow->FirstGadget, BUTTON_PARENT, cmdWindow, NULL, strRoot, 0);
#endif
			} else {
				if( operation == OPER_SELECTION ) {
					SetButtonItem(cmdWindow->FirstGadget, BUTTON_TAG, cmdWindow, NULL, strTag, KEY_TAG);
					SetButtonItem(cmdWindow->FirstGadget, BUTTON_UNTAG, cmdWindow, NULL, strUntag, KEY_UNTAG2);
					sel = SLNextSelect(scrollList, -1) != -1;
					EnableGadget(cmdWindow, BUTTON_TAG, sel);
				}
				SetButtonItem(cmdWindow->FirstGadget, BUTTON_PARENT, cmdWindow, NULL, strBack, KEY_BACK2);
			}
			lastAltOn = altOn;
		}
	}
}

/*
 * Disable/enable the enter gadget in the main window
 */
 
void UpdateEnterGadget()
{
	register WORD item;
	register WORD prevItem;
	register DirFibPtr nextFib;
	BOOL enterOn;
	
	if( operation == OPER_SELECTION ) {
		enterOn = FALSE;
		prevItem = item = -1;
		nextFib = curLevel->dl_ChildPtr;
		while( ((item = SLNextSelect(scrollList, item)) != -1) && !enterOn ) {
			while( ++prevItem < item ) {
				nextFib = nextFib->df_Next;
			}
			if( nextFib->df_Flags & FLAG_DIR_MASK
				 /* && (((DirLevelPtr) nextFib->df_SubLevel)->dl_ChildPtr != NULL) ) {*/
				&& ((nextFib->df_Flags & (FLAG_SLINK_MASK | FLAG_HLINK_MASK)) == 0) ) { 
				enterOn = TRUE;
			}
			prevItem--;
		}
		if( enterState != enterOn ) {
			EnableGadget(cmdWindow, BUTTON_ENTER, enterOn);
			enterState = enterOn;
		}
	}
}

/*
 * Initialize dialog which scans the volume
 */
 
void InitScanRequester(RequestPtr requester)
{
	prevScanReqRP = NULL;
	if( requester != NULL ) {
		StuffScanDirText(NULL);					/* Just display volume title */
	}
}

/*
 * Just like the Toolbox AddListItem, except it always sorts, beginning with
 * the second character. Note "list" assumed to be non-NULL, "len" non-zero.
 */
 
static LONG AddQBListItem(list, text, len)
ListHeadPtr list;
TextPtr text;
register WORD len;
{
	register LONG num;
	register ListItemPtr item, prevItem;
	ListItemPtr newItem;
	
	text[len] = 0;
	if( (newItem = CreateListItem(text, len)) == NULL)
		return (-1);
	prevItem = NULL;
	num = 0;
	item = list->First;
	while (item && CmpString(text+1, (item->Text)+1, len, (WORD) strlen((item->Text)+1), FALSE) > 0) {
		prevItem = item;
		item = item->Next;
		num++;
	}
	AddToList(list, prevItem, newItem);
	len = TextLength(cmdWindow->RPort, text, len);
	if( len > pagePixWidth ) {
		pagePixWidth = len;
	}
	return(num);
}

/*
 * Update volume list
 */
 
void UpdateVolumeList()
{
	TextChar buff[MAX_FILENAME_LEN+3];
	WORD item;
	WORD prevItem;
	register WORD i;
	register BOOL diff;
	register BOOL abort;
	register struct DevInfo *volume;
	register TextPtr volName;
	register WORD num = 0;
	register WORD numItems;
		
	if( operation == OPER_LIMBO && (strPath[0] == 0) ) {
		prevItem = SLNextSelect(scrollList, -1);
		buff[0] = 0;
		if( prevItem != -1 ) {
			strcpy(buff, GetListItem(scrollList->List, prevItem));
		}
		numItems = SLNumItems(scrollList);
		diff = FALSE;
			
		for( volume = NHLockDosList(DLT_VOLUME) ; (volume != NULL) && !diff ; volume = NHNextDosEntry(volume, DLT_VOLUME) ) {
			if( volume->dvi_Task != NULL ) {		/* Make sure mounted */
				volName = (TextPtr) BADDR(volume->dvi_Name);
				for( i = 0, abort = FALSE ; (i < numItems) && !abort ; i++ ) {
					SLGetItem(scrollList, i, strBuff);
					diff = CmpString(&strBuff[2], &volName[1], strlen(&strBuff[2]), *volName, TRUE);
					if( diff == 0 ) {
						abort = TRUE;
					}
				}
				diff = !abort;
				num++;
			}
		}
		NHUnlockDosList(DLT_VOLUME);
		if( (!diff) && prefs.IncludeAssignDirs ) {
			for( volume = NHLockDosList(DLT_DIRECTORY) ; (volume != NULL) && !diff ; volume = NHNextDosEntry(volume, DLT_DIRECTORY) ) {
				volName = (TextPtr) BADDR(volume->dvi_Name);
				for( i = 0, abort = FALSE ; (i < numItems) && !abort ; i++ ) {
					SLGetItem(scrollList, i, strBuff);
					diff = CmpString(&strBuff[2], &volName[1], strlen(&strBuff[2]), *volName, TRUE);
					if( !diff ) {
						abort = TRUE;
					}
				}
				diff = !abort;
				num++;
			}
			NHUnlockDosList(DLT_DIRECTORY);
		}
		if( diff || (num != numItems) ) {
			BuildVolumeList(&buff[3]);
			if( (item = FindListItem(scrollList->List, buff)) != -1 ) {
				if( prevItem != item ) {
					SLSelectItem(scrollList, prevItem, FALSE);
				}
			}
		}
	}
	checkVolCounter = 20;
}

/*
 * Build scrolling list of volumes, selecting the volume name passed.
 */

BOOL BuildVolumeList(TextPtr volName)
{
	register BOOL success = FALSE;
	TextPtr fileName;
	GadgetPtr gadgList;
	register ListHeadPtr list;
	struct DevInfo *volume;
	register WORD num = 0;
	register WORD len;
	register WORD i;
	struct Task *sigTask;
	ULONG taskName;
	register WindowPtr window = cmdWindow;
	struct FileInfoBlock *fibPtr;
	BPTR lock;
	LONG entryType;
	
	pagePixWidth = 0;
	if( scrollList != NULL ) {
		if( (list = CreateList()) != NULL ) {
			SetStdPointer(window, POINTER_WAIT);
			success = TRUE;
			SLRemoveAll(scrollList);
			SLDrawBorder(scrollList);
			SLDoDraw(scrollList, FALSE);
			SLHorizScroll(scrollList, -leftOffset * charWidth );
			tabbing[0] = 256;						/* Always tab over at least this many pixels */
			leftOffset = 0;
			SLSetDrawProc(scrollList, QBDrawProc);
			SLSetSelectMode(scrollList, SL_SINGLESELECT);

			i = TRUE;
			if( strPath[0] ) {
				fibPtr = (struct FileInfoBlock *) fib;
				lock = Lock(strPath, ACCESS_READ);
				if( success = lock != NULL ) {
					if( success = Examine(lock, fibPtr) != NULL ) {
						while( ExNext(lock, fibPtr) ) {
							entryType = fibPtr->fib_DirEntryType;
							if( (entryType > 0) && (entryType != ST_SOFTLINK) && (entryType != ST_LINKDIR) ) {
								strBuff[0] = DRAWER_ICON+1;
								strcpy(strBuff+1, fibPtr->fib_FileName);
								if( AddQBListItem(list, strBuff, strlen(strBuff)) != -1 ) {
									num++;
								} else {
									success = FALSE;
								}
							}
						}
					}
					UnLock(lock);
				}
/*
	If a failure, retry with root.
*/
				i = !success;
				if( i ) {
					ErrBeep();
					DisposeListItems(list);
					strPath[0] = 0;
					num = 0;
				} else {
					EnableGadget(window, BUTTON_PARENT, strPath[0]);
				}
			}
			if( i ) {
				success = TRUE;
				for (volume = NHLockDosList(DLT_VOLUME); (volume != NULL) && success; volume = NHNextDosEntry(volume, DLT_VOLUME) ) {
					if( volume->dvi_Task != NULL ) {
						fileName = (TextPtr) BADDR(volume->dvi_Name);
						len = (UWORD) *fileName;
						sigTask = (struct Task *) ((MsgPortPtr)volume->dvi_Task)->mp_SigTask;
						strBuff[0] = HD_ICON+1;
/*
	If volume handler task is DFx or RAM, then give volume a floppy icon.
*/
						taskName = *((ULONG *) sigTask->tc_Node.ln_Name);
						if( ((taskName & 0xFFFFC8FF) == 0x44460000 ) ||
							(strcmp(sigTask->tc_Node.ln_Name, strRAM) == 0) ) {
							strBuff[0] = VOL_ICON+1;
						}
						BlockMove( fileName+1, &strBuff[1], ++len);
						if( AddQBListItem(list, strBuff, len) != -1 ) {
							num++;
						} else {
							success = FALSE;
						}
					}
				}
				NHUnlockDosList(DLT_VOLUME);
				if( prefs.IncludeAssignDirs ) {
					for( volume = NHLockDosList(DLT_DIRECTORY); (volume != NULL) && success; volume = NHNextDosEntry(volume, DLT_DIRECTORY) ) {
						fileName = (TextPtr) BADDR(volume->dvi_Name);
						len = (UWORD) *fileName;
						/*sigTask = (struct Task *) ((MsgPortPtr)volume->dvi_Task)->mp_SigTask;*/
						strBuff[0] = LFLAG_ITALIC_MASK | (DRAWER_ICON+1);
						BlockMove( fileName+1, &strBuff[1], ++len);
						if( AddQBListItem(list, strBuff, len) != -1 ) {
							num++;
						} else {
							success = FALSE;
						}
					}
					NHUnlockDosList(DLT_DIRECTORY);
				}
			}
			len = 0;
			for( i = 0 ; i < num; i++ ) {
				strBuff[0] = 1;
				strcpy(&strBuff[1], GetListItem(list, i));
				SLAddItem(scrollList, strBuff, (WORD) strlen(strBuff), i);
				if( volName != NULL ) {
					if( strcmp(&strBuff[2], volName) == 0 ) {
						len = i;
					}
				}
			}
			SLDoDraw(scrollList, TRUE);
			gadgList = GadgetItem(window->FirstGadget, OK_BUTTON);
			if( success ) {
				EnableGadget(window, BUTTON_ENTER, num);
				EnableGadget(window, BUTTON_1, num);
				EnableGadget(window, BUTTON_2, num);
			} else {
/*
	Failure! Turn off backup/restore controls.
*/
				OffGList(gadgList, window, NULL, 4);
			}
			SLSelectItem(scrollList, len, TRUE);
			DisposeList(list);
			pagePixWidth += iconList[VOL_ICON]->Width + (iconList[CHECK_ICON_MASK]->Width >> 2) + 2;
			pageWidth = pagePixWidth / charWidth;
			AdjustScrollBars(window);
			SetArrowPointer();
		}
	}
	return(success);
}

/*
 * Build scrolling list of files - if dirFib is non-NULL, have it pre-selected.
 */
 
BOOL BuildFileList(DirFibPtr parentDirFib)
{
	register WORD i;
	TextChar text[150];
	register DirFibPtr dirFib;
	register WindowPtr window;
	
	if( (mode == MODE_BACKUP) && (prefs.OldPrefs.BackupType != 1) ) {
		return(TRUE);
	}
	window = cmdWindow;
	i = 0;
/*
	We used to not automatically hilight the first entry in the list if NULL.
*/
	SortDir();
	enterState = parentDirFib != NULL;
	if( parentDirFib == NULL ) {
		parentDirFib = curLevel->dl_ChildPtr;
		enterState = (parentDirFib != NULL) && ((parentDirFib->df_Flags & FLAG_DIR_MASK) != 0 );
	}
	EnableGadget(window, BUTTON_ENTER, enterState);
	EnableGadget(window, BUTTON_PARENT, curLevel->dl_ParLevel != NULL);
	EnableGadget(window, BUTTON_TAG, parentDirFib != NULL );
	EnableGadget(window, BUTTON_UNTAG, parentDirFib != NULL );
	OnOffMenu(backWindow, MENUITEM(TAG_MENU, NOITEM, NOSUB), curLevel->dl_ChildPtr != NULL);
	OnOffMenu(backWindow, MENUITEM(TAG_MENU, TAG_ITEM, NOSUB), parentDirFib != NULL);
	OnOffMenu(backWindow, MENUITEM(TAG_MENU, UNTAG_ITEM, NOSUB), parentDirFib != NULL);
	SLRemoveAll(scrollList);			/* First remove previous entries */
	SLDoDraw(scrollList, FALSE);		/* Turn drawing off while we add */
	SLSetSelectMode(scrollList, SL_MULTISELECT);
	SLSetDrawProc(scrollList, QBDrawProc);
	SLHorizScroll(scrollList, -leftOffset * charWidth );	/* Return List to origin */
	tabbing[0] = 256;						/* Always tab over at least this many pixels */
	leftOffset = 0;
	AdjustWidth();						/* Adjusts horizontal scroll bar accordingly */
	dirFib = curLevel->dl_ChildPtr;
	while( ++i < MAX_LIST_ITEMS && dirFib != NULL ) {
		BuildListEntry(dirFib, &text[2]);
		ConstructIconField(&text[0], dirFib->df_Flags);
		SLAddItem(scrollList, text, (WORD) strlen(text), i);
		if( dirFib == parentDirFib ) {
			SLSelectItem(scrollList, i-1, TRUE);
		}
		dirFib = dirFib->df_Next;
	}
	if( parentDirFib == NULL ) {
		SLSelectItem(scrollList, 0, TRUE);
	}
	SLDoDraw(scrollList, TRUE);		/* Turn back on so we can see! */
	SLAutoScroll(scrollList, 0);		/* Shove to beginning of list. */
	AdjustScrollBars(window);			/* Update horizontal scroll bar */
	return(TRUE);
}

/*
 * Construct first two bytes of icon list field
 */
 
static void ConstructIconField(register TextPtr text, register UBYTE flags)
{
	*text = 1;
	text[1] = DOC_ICON + 1;
	if( flags & FLAG_DIR_MASK ) {
		text[1] = DRAWER_ICON + 1;
		/*
		if( ((DirLevelPtr)dirFib->df_SubLevel)->dl_ChildPtr == NULL ) {
			*text |= LFLAG_EMPTY_MASK;
		}
		*/
	} else if( flags & FLAG_SLINK_MASK ) {
		text[1] = SLINK_ICON + 1;
	}
	if( flags & FLAG_SEL_MASK ) {
		*text |= LFLAG_SEL_MASK;
		if( flags & FLAG_PART_MASK ) {
			*text |= LFLAG_PART_MASK;
		}
	}
	if( flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK) ) {
		if( flags & FLAG_HLINK_MASK ) {
			*text |= LFLAG_EMPTY_MASK;
		}
		text[1] |= LFLAG_ITALIC_MASK;
	}
}

/*
 * Assemble a line of text for adding to a list item
 */

void BuildListEntry(DirFibPtr dirFib, register TextPtr destPtr)
{
	register WORD len;
	TextChar buff[20];
	register TextPtr bufPtr = &buff[0];
	register WORD hour;
	register WORD i;
	BOOL status;
	
	len = stccpy(destPtr, dirFib->df_Name, tabChars[0]+1);
	destPtr[len-1] = TAB;
	if( prefs.CatOpts.IncludeSize ) {
		if( dirFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK | FLAG_DIR_MASK) ) {
			if( dirFib->df_Flags & FLAG_HLINK_MASK ) {
				bufPtr = strHardLink;
			} else if( dirFib->df_Flags & FLAG_SLINK_MASK ) {
				bufPtr = strSoftLink;
			} else {
				bufPtr = strSubdir;
			}
			len += stccpy(destPtr + len, bufPtr, tabChars[1]+1);
			bufPtr = &buff[0];
		} else {
			NumToString(dirFib->df_Size, bufPtr);
			len += stccpy(destPtr + len, bufPtr, tabChars[1]+1);
			destPtr[len-1] = ' ';		/* Append space to end, for neatness */
			++len;
		}
	} else {
		++len;
	}
	destPtr[len-1] = BS;
	if( prefs.CatOpts.IncludeProt ) {
		for( i = 7 ; i >= 0 ; i-- ) {
			status = dirFib->df_Prot & (1 << i);
			if( i < 4 ) {
				status = !status;
			}
			destPtr[len++] = status ? attrChars[i] : '-';
		}
	}
	++len;
	destPtr[len-1] = TAB;
	if( prefs.CatOpts.IncludeDate ) {
		XDateString( &dirFib->df_DateStamp, prefs.CatOpts.DateFormat, bufPtr);
		len += stccpy(destPtr + len, bufPtr, tabChars[3]+1);
	} else {
		len++;
	}
	destPtr[len-1] = TAB;
	if( prefs.CatOpts.IncludeTime ) {
		TimeString( &dirFib->df_DateStamp, prefs.CatOpts.ShowSecs, prefs.CatOpts.ShowAMPM, &buff[1]);
		hour = dirFib->df_DateStamp.ds_Minute / 60;
/*
	If a single-digit hour, then pad  with a space or a leading zero
*/
		if(( hour == 0 && !prefs.CatOpts.ShowAMPM ) || 
			( hour && ( ( hour < 10 ) || ( prefs.CatOpts.ShowAMPM && (hour > 12) && (hour < 22) ) ) ) ) {
			buff[0] = prefs.CatOpts.ShowAMPM ? ' ' : '0';
		} else {
			bufPtr++;
		}
		len += stccpy(destPtr + len, bufPtr, tabChars[4]+1);
	} else {
		len++;
	}
	destPtr[len-1] = TAB;
	destPtr[len] = 0;
}

/*
 * Attempt to move to file-selection mode from a different mode.
 */
 
BOOL AllocMiniIcons()
{
	BOOL success = TRUE;
	register WORD i;

	for( i = 0 ; i < NUM_MINI_ICONS ; i++ ) {
		iconList[i] = ConstructIconImage(i);
		if( iconList[i] == NULL ) {
			success = FALSE;
			FreeMiniIcons();
			break;
		}
	}
	return(success);
}

/*
 * Exit selection mode. Frees allocated stuff above.
 */
 
void FreeMiniIcons()
{
	register WORD i;
	
	for( i = 0 ; i < NUM_MINI_ICONS ; i++ ) {
		if( iconList[i] != NULL ) {
			FreeImage(iconList[i]);
			iconList[i] = NULL;
		}
	}
}

/*
 * Tweak the corners of the rectangle of the hard disk mini-icon.
 */
 
static void TweakCorners(Point *ptList, BOOL full)
{
	ptList[0].y++; 
	ptList[3].y++;	
	ptList[4].y--; 
	ptList[7].y--;
	if( full ) {
		ptList[8].y++;
	}
}

/*
 * Ensure uniform diamond.
 */
 
static void TweakDiamond(Point *ptList, BOOL full)
{
	ptList[0].x = ptList[1].x - (ptList[2].x - ptList[1].x);
	ptList[1].y = ptList[2].y - (ptList[3].y - ptList[2].y);
	if( full ) {
		ptList[4].x = ptList[0].x;
	}
}

/*
 *	Create mini-icon image.
 */

static ImagePtr ConstructIconImage(WORD origID)
{
	register WORD i, size, numPts, width, height;
	ImagePtr image;
	RastPort rPort;
	BitMap bitMap;
	PLANEPTR imageData;
	Point ptList[16];
	Point maskPtList[16];	
	BYTE old_cp_y;
	WORD depth, offset;
	WORD iconOffsets[NUM_MINI_ICONS];
	WORD id;
	BOOL gray = FALSE;
	BOOL black = FALSE;
/*
	Calcualate the icon offsets on the fly, even though they are constants...
	...for maintainability's sake.
*/
	id = origID;
	if( id == 0 ) {
		numPts = 0;
		for( i = 0 ; i < NUM_MINI_ICONS ; i++ ) {
			iconOffsets[i] = numPts;
			numPts += iconNumPts[i];
		}
	}
	switch(id) {
	case DRAWER_ICON_GRAY:
	case DOC_ICON_GRAY:
	case VOL_ICON_GRAY:
	case TAPE_ICON_GRAY:
		gray = TRUE;
		break;
	case DOC_ICON_BLACK:
	case VOL_ICON_BLACK:
	case TAPE_ICON_BLACK:
		black = TRUE;
		break;
	}
	
	if( id == DRAWER_ICON_GRAY ) {
		id = DRAWER_ICON;
	} else if( id == DOC_ICON_GRAY || id == DOC_ICON_BLACK ) {
		id = DOC_ICON;
	} else if( id == VOL_ICON_GRAY || id == VOL_ICON_BLACK ) {
		id = VOL_ICON;
	} else if( id == TAPE_ICON_GRAY || id == TAPE_ICON_BLACK ) {
		id = TAPE_ICON;
	}
	
	height = charHeight;
	width = (((height * 3) >> 1));		/* Make width 1.5 times height */
	if( (height == 8) /*&& (id != TAPE_ICON_MASK) && (id != TAPE_ICON)*/ ) {
		width += 3;
	}
/*
	Allocate image data
*/
	depth = screen->RastPort.BitMap->Depth;
	if( id >= VOL_ICON_MASK ) {
		depth = 1;							/* Hey, this is just a mask, dude. */
	}
	if( (screen->ViewPort.Modes & LACE) == 0 ) {
		width += (width >> 1);				/* Increase width if non-interlaced */
	}
	size = RASSIZE(width, height);
	if ((image = NewImage(width, height, depth)) == NULL)
		return (NULL);
/*
	Initialize bitMap and rPort for drawing
*/
	InitBitMap(&bitMap, depth, width, height);
	InitRastPort(&rPort);
	rPort.BitMap = &bitMap;
	imageData = (PLANEPTR) image->ImageData;
	for (i = 0; i < depth; i++)
		bitMap.Planes[i] = imageData + i*size;
/*
	Draw into rPort
*/
	SetRast(&rPort, 0);						/* Needed for masks only */

	numPts = iconNumPts[id];
	offset = iconOffsets[id];
	
	for( i = 0; i < numPts; i++ ) {
		ptList[i].x = SCALE(iconX[i+offset], 16, width);
		ptList[i].y = SCALE(iconY[i+offset], 10, height);
	}
/*
	Try to make a round-rect hard-disk as best we can here.
*/
	if( (id == HD_ICON) || (id == HD_ICON_MASK) ) {
		TweakCorners(ptList, id == HD_ICON);
	} else if( (id == SLINK_ICON) || (id == SLINK_ICON_MASK) ) {
		TweakDiamond(ptList, id == SLINK_ICON);
	}
	if( id < VOL_ICON_MASK ) {
		offset = iconOffsets[id+VOL_ICON_MASK];
		for( i = 0 ; i < iconNumPts[id+VOL_ICON_MASK] ; i++ ) {
			maskPtList[i].x = SCALE(iconX[i+offset], 16, width);
			maskPtList[i].y = SCALE(iconY[i+offset], 10, height);
		}
		if( id == HD_ICON ) {
			TweakCorners(maskPtList, FALSE);
		} else if( id == SLINK_ICON ) {
			TweakDiamond(maskPtList, FALSE);
		}
/*
	Set fill color to white, unless it's the "empty folder" icon and not monochrome.
*/
		SetAPen(&rPort, black ? _tbPenBlack : _tbPenWhite);
		if( (depth > 1) && gray ) {
			SetAPen(&rPort, _tbPenLight);
		}
		FillPoly(&rPort, iconNumPts[id+VOL_ICON_MASK], maskPtList);
	}
	SetAPen(&rPort, black ? _tbPenWhite : _tbPenBlack);
	if( id == HD_ICON ) {
		Move(&rPort, SCALE(3, 16, width), SCALE(7, 10, height));
		Draw(&rPort, SCALE(4, 16, width), rPort.cp_y);
	} else {
		if( id == DRAWER_ICON ) {		/* Drawer requires an additional set of lines */
			Move(&rPort, SCALE(5, 16, width), SCALE(5, 10, height));
			old_cp_y = rPort.cp_y;
			Draw(&rPort, rPort.cp_x, SCALE(6, 10, height));
			Draw(&rPort, SCALE(11, 16, width), rPort.cp_y);
			Draw(&rPort, rPort.cp_x, old_cp_y);
		} else if( id == TAPE_ICON ) {
			i = SCALE(5, 10, height);
			size = i >> 1;
			if( (screen->ViewPort.Modes & LACE) == 0 ) {
				size >>= 1;
			}
			DrawEllipse(&rPort, SCALE(5, 16, width), i, i >> 1, size);
			DrawEllipse(&rPort, SCALE(11,16, width), i, i >> 1, size);
		}
		if( gray && (depth == 1) ) {
			SetDrMd(&rPort, JAM1);
			SetAfPt(&rPort, &grayPat[0], 1);
			RectFill(&rPort, 0, 0, width - 1, height - 1);
			SetAfPt(&rPort, NULL, 0);
		}
	}
	if( id < VOL_ICON_MASK ) {
		FramePoly(&rPort, numPts, ptList);
		if( black ) {
			if( origID == VOL_ICON_BLACK ) {
				SetAPen(&rPort, _tbPenBlack);
			}
			FramePoly(&rPort, iconNumPts[id+VOL_ICON_MASK], maskPtList);
		}
	} else {
		SetAPen(&rPort, 1);
		FillPoly(&rPort, numPts, ptList);
	}
	return (image);
}

/*
 * Drawing procedure
 */

void QBDrawProc(register RastPtr rPort, TextPtr text, RectPtr rect)
{
	register TextPtr str;
	register WORD i;
	register WORD len;
	register WORD width;
	register WORD oldX;
	BYTE flags;
	WORD oldY, textLen, height;
	register ImagePtr icon;
	APTR mask;
	BitMap bitMap;
	WORD depth;
	BYTE minterm = 0xE0;										/* Cookie-cut copy */
	WORD begin = 0;
	WORD indent;
	BOOL newStyle = FALSE;
	
	if( operation == OPER_SELECTION ) {
		begin = (iconList[CHECK_ICON_MASK]->Width >> 2) + 2;
	}
	oldX = rPort->cp_x;
	oldY = rPort->cp_y;
	flags = *text++;
	indent = ((flags & LFLAG_INDENT_MASK)-1) * INDENT_PIXELS;
	
	if( (i = *text & LFLAG_ID_MASK) ) {
		width = iconList[VOL_ICON]->Width;				/* Nothing wider */
		if( i != LFLAG_ID_MASK ) {
			switch( i ) {
			case HD_ICON+1:
				icon = iconList[HD_ICON];					/* Hard disk icon */
				mask = iconList[HD_ICON_MASK]->ImageData;	/* Hard disk mask */
				break;
			case VOL_ICON+1:
				icon = iconList[VOL_ICON];					/* Disk icon */
				mask = iconList[VOL_ICON_MASK]->ImageData;	/* Disk mask */
				break;
			case SLINK_ICON+1:
				icon = iconList[SLINK_ICON];				/* Softlink icon */
				mask = iconList[SLINK_ICON_MASK]->ImageData;/* Softlink mask */
				begin += indent;
				break;
			case DRAWER_ICON+1:
				if( flags & LFLAG_EMPTY_MASK ) {
					icon = iconList[DRAWER_ICON_GRAY];	/* Gray drawer icon */
				} else {
					icon = iconList[DRAWER_ICON];			/* Drawer icon */
				}
				begin += indent;
				mask = iconList[DRAWER_ICON_MASK]->ImageData;/* Drawer mask */
				break;
			case DOC_ICON+1:
				if( flags & LFLAG_EMPTY_MASK ) {
					icon = iconList[DOC_ICON_GRAY];		/* Gray doc icon */
				} else {
					icon = iconList[DOC_ICON];				/* Doc icon */
				}
				begin += indent;
				mask = iconList[DOC_ICON_MASK]->ImageData;	/* Doc mask */
				break;
			case TAPE_ICON+1:
				icon = iconList[TAPE_ICON];				/* Tape icon */
				mask = iconList[TAPE_ICON_MASK]->ImageData;	/* Tape icon mask */
				break;
			}
			/* width = icon->Width; - Decided to hard-code this instead, so I wouldn't
				have to waste space allocating a icon just for no icon.
			*/
			height = icon->Height;
			depth = screen->RastPort.BitMap->Depth;
			InitBitMap(&bitMap, depth, width, height);
			for (i = 0; i < depth; i++) {
				bitMap.Planes[i] = (PLANEPTR) icon->ImageData + i*RASSIZE(width, height);
			}
			if( flags & LFLAG_SEL_MASK ) {
				if( flags & LFLAG_PART_MASK ) {
					Move(rPort, (oldX + 2) - scrollList->XOffset, rect->MinY + (height >> 1));
					Draw(rPort, rPort->cp_x + (begin - 2), rPort->cp_y);
					Move(rPort, rPort->cp_x, rPort->cp_y + 1);
					Draw(rPort, (oldX + 2) - scrollList->XOffset, rPort->cp_y);
				} else {
					BltTemplate((PLANEPTR) iconList[CHECK_ICON_MASK]->ImageData, 0, (((width-1) >> 4)+1) << 1, rPort, (rect->MinX + 2) - scrollList->XOffset, rect->MinY, width, height);
				}
			}
			BltMaskBitMapRastPort(&bitMap, 0, 0, rPort, rect->MinX + begin + 3 - scrollList->XOffset,
				rect->MinY + 0, width, height, minterm, mask);
		}
		begin += (width + 5);
	}
/*
	Now draw the text portion of the line
*/							
	Move(rPort, oldX + begin, oldY);
	if( flags & LFLAG_ERROR_MASK ) {
		SetSoftStyle(rPort, FSF_BOLD, 0xFF);
		newStyle = TRUE;
	} else if( *text & LFLAG_ITALIC_MASK ) {
		SetSoftStyle(rPort, FSF_ITALIC, 0xFF);
		newStyle = TRUE;
	}
	text++;
	
	if( operation != OPER_SELECTION ) {
		len = strlen(text);
		Text(rPort, text, len);
	} else {
		for( i = 0 ; *text ; i++ ) {					/* Tabbed text drawing here */
			for( str = text; (*str != TAB) && (*str != BS); str++ );
			len = str - text;
			oldX = rPort->cp_x;
			if( i ) {
				textLen = TextLength(rPort, text, len);
				if( *str == BS ) {
					Move(rPort, oldX + (tabTable[i] - textLen), rPort->cp_y);
				}
				Text(rPort, text, len);
			} else {
				textLen = tabbing[0];
				TextInWidth(rPort, text, len, textLen, FALSE);
				if( newStyle ) {
					SetSoftStyle(rPort, FS_NORMAL, 0xFF);
					newStyle = FALSE;
				}
			}
			Move(rPort, oldX + tabTable[i], rPort->cp_y);
			text = ++str;
		}
	}
	if( newStyle ) {
		SetSoftStyle(rPort, FS_NORMAL, 0xFF);
	}
}

/*
 * Initialize runtime display variable(s) and send out first string.
 */
 
void InitRuntimeDisplay(TextPtr initStr)
{
	SLRemoveAll(scrollList);
	currListItem = 0;
	DisplayStr(initStr);
}

/*
 * Send the file in hierarchical (tabbed) form to the scroll list.
 * All selection markings are to be suppressed.
 */
 
void DisplayNextFile(DirFibPtr dirFib)
{
	TextChar buff[PATHSIZE+MAX_FILENAME_LEN+1];
	
	if( operation == OPER_IN_PROG ) {
		if( !inputCompleted ) {
			if( dirFib != NULL ) {
				ConstructRuntimeItem(dirFib->df_Name, dirFib->df_Flags, buff);
				if( badDiskFlag ) {
					DisplayErrStr(buff, strSkippedErr);
				} else {
					AddRuntimeItem(buff, (WORD) strlen(buff) );
				}
			}
		}
	}
	return;
}

/*
 * Generate a runtime item line of text w/ indentation & proper icon
 */
 
void ConstructRuntimeItem(TextPtr name, UBYTE flags, TextPtr buff)
{
	WORD indent;
	
	if( prefs.LogOpts.LogLevel == LOG_RESULTS ) {
		indent = 1;
		strcpy(&buff[2], strPath);
		AppendDirPath(&buff[2], name);
	} else {
		indent = MIN(indentLevel+1, 15);
		strcpy(&buff[2], name);
	}
	ConstructIconField(buff, flags);
	*buff = (*buff & ~(LFLAG_SEL_MASK | LFLAG_INDENT_MASK)) | indent;
}
	
/*
 * Send the volume name passed to the scroll list.
 * Assumes "iconType" global variable has already been set.
 */
 
void DisplayNextVol(TextPtr volName)
{
	TextChar buff[PATHSIZE];
	register TextPtr str;
	WORD len;
	
	if( !inputCompleted && (prefs.LogOpts.LogLevel != LOG_RESULTS) ) {
		buff[0] = 1;
		if( verifyMode == RMODE_TEST || verifyMode == RMODE_SCAN ) {
			if( useAmigaDOS ) {
				buff[1] = DOC_ICON+1;
			} else if( tapeDevice ) {
				buff[1] = TAPE_ICON+1;
			} else {
				buff[1] = VOL_ICON+1;
			}
			len = strlen(currVolName);
			strcpy(&buff[2], currVolName);
			str = &buff[2+len];
		} else {
			buff[1] = DRAWER_ICON+1;
			strcpy(&buff[2], volName);
			len = strlen(volName);
			str = &buff[2+len];
			if( len && (*(str-1) == ':') ) {
				for( str = &buff[2] ; *str && (*str != ':') ; str++ );
				buff[1] = iconType;
			}
		}
		AddRuntimeItem(buff, str - buff);
	}
}

/*
 * Send an arbitrary string "str". If NULL, do nothing.
 */

void DisplayStr(TextPtr str)
{
	TextChar buff[GADG_MAX_STRING];		/* Ought to be enough, but be careful! */
	
	if( str == NULL ) {
		buff[2] = '\0';
	} else {
		strcpy(&buff[2], str);
	}
	buff[0] = 1;
	buff[1] = LFLAG_DUMMY_MASK;
	AddRuntimeItem(buff, strlen(buff));
}

/*
 * Add the list item to the runtime display
 */
 
void AddRuntimeItem(TextPtr buff, WORD len)
{
	WORD oldListItem = currListItem;
	register TextPtr lastFileName = NULL;
	register BOOL eraseLast = FALSE;

	if( currListItem ) {
		lastFileName = GetListItem(scrollList->List, currListItem-1);
	}
/*
	Do not erase plain text or error listings (in bold).
*/
	if( prefs.LogOpts.LogLevel != LOG_FULL ) {
		if( lastFileName != NULL ) {
			if( (((lastFileName[1] & LFLAG_INDENT_MASK) != 1) || (prefs.LogOpts.LogLevel == LOG_RESULTS))
				|| (verifyMode != RMODE_TEST) ) {
				if( eraseLast = ((lastFileName[2] & LFLAG_DUMMY_MASK) == 0) 
					&& ((lastFileName[1] & LFLAG_ERROR_MASK) == 0) ) {
					if( prefs.LogOpts.LogLevel == LOG_DRAWERS ) {
						eraseLast = FALSE;
						switch(lastFileName[2] & LFLAG_ID_MASK) {
						case DRAWER_ICON+1:
							if( (lastFileName[1] & LFLAG_EMPTY_MASK) == 0 ) {
								break;
							}
						case SLINK_ICON+1:
						case DOC_ICON+1:
							eraseLast = TRUE;
						default:
							break;
						}
					}
				}
			}
		}
	}
	if( eraseLast ) {
		SLChangeItem(scrollList, --currListItem, buff, len);
	} else {
		SLAddItem(scrollList, buff, len, currListItem);
	}
	if( currListItem == MAX_LIST_ITEMS ) {
		SLRemoveItem(scrollList, 0);
	} else {
		currListItem++;
	}
	if( !disableDisplay ) {
		SLAutoScroll(scrollList, oldListItem);
	}
	return;
}

/*
 * Create error string, catenating "str" to filename passed. Text drawn in bold.
 */
 
void DisplayErrStr(TextPtr fileStr, TextPtr str)
{
	TextChar tempBuff[GADG_MAX_STRING];
	
	strcpy(tempBuff, fileStr);
	strcat(tempBuff, str);
	tempBuff[0] |= LFLAG_ERROR_MASK;
	SLAddItem(scrollList, tempBuff, strlen(tempBuff), currListItem++);
	if( disableDisplay ) {
		SLAutoScroll(scrollList, currListItem);
		EnableScrollList();
		DisableScrollList();
	}
}
	
/*
 * Append to the current list item, used ONLY FOR ERRORS (makes text bold)
 * Arbitrarily, you may only append one message to a given line. This prevents
 * multiple error messages for a given file, so that if a file has a different
 * length and different contents, it only prints one of the messages.
 */
 
void AppendToRuntimeItem(TextPtr str)
{
	TextChar tempBuff[GADG_MAX_STRING];
	
	SLGetItem(scrollList, --currListItem, tempBuff);
	if( (tempBuff[0] & LFLAG_ERROR_MASK) == 0 ) {
		strcat(tempBuff, str);
		tempBuff[0] |= LFLAG_ERROR_MASK;
		SLChangeItem(scrollList, currListItem, tempBuff, strlen(tempBuff));
		if( disableDisplay ) {
			SLAutoScroll(scrollList, currListItem);
			EnableScrollList();
			DisableScrollList();
		}
	}
	currListItem++;
}

/*
 * Remove a runtime item.
 */
 
void RemoveRuntimeItem(WORD item)
{
	SLGetItem(scrollList, item, strBuff);
	if( strBuff[0] & LFLAG_ERROR_MASK ) {
		--stats.numErrors;
	}
	currListItem--;
	SLRemoveItem(scrollList, item);
}

/*
 * Resets the scroll list to what it was before the disk error.
 */

void ResetScrollListAfterDiskError()
{
	if( currListItem != saveCurrListItem ) {
		DisableScrollList();
		while( currListItem > saveCurrListItem ) {
			RemoveRuntimeItem(currListItem-1);
		}
		EnableScrollList();
	}
	if( prefs.LogOpts.LogLevel != LOG_RESULTS && stats.completedFiles ) {
		RemoveRuntimeItem(currListItem-1);
	}
	if( (curFib != NULL) && (curFib->df_Flags & FLAG_DIR_MASK) == 0 ) {
		DisplayNextFile(curFib);
	}
	RefreshScrollList();
}

/* 
 * Call StuffScanText() below, but first check for valid rastport in requester.
 */
 
static void DoStuffScanText(TextPtr buff, WORD item)
{
	TextChar buff2[DEVICE_PATH_BUFFER_SIZE + DEV_BUF+1];
	register UWORD len;
	register TextPtr cpyStr;
	TextPtr catStr;
	register BOOL quote;
		
	if( requester->ReqLayer != NULL ) {
		LockLayer(0, requester->ReqLayer);
		if( prevScanReqRP == NULL ) {
			SetFont(requester->ReqLayer->rp, screen->RastPort.Font);
			if( mode == MODE_BACKUP ) {
				if( strPath[0] && strPath[strlen(strPath)-1] != ':' ) {
					cpyStr = strScanDir;
				} else {
					cpyStr = strScanVol;
				}
				quote = TRUE;
				catStr = prefs.OldPrefs.DevPathBuf;
			} else {
				strcpy(buff2, strScanDrive);
				cpyStr = strScanDrive;
				catStr = firstDevice->DevName;
				quote = useAmigaDOS;
			}
			strcpy(buff2, cpyStr);
			if( quote ) {
				strcat(buff2, strQuote);
			}
			strcat(buff2, catStr);
			len = strlen(buff2);
			if( len && ((mode == MODE_BACKUP) || useAmigaDOS) ) {
				if( buff2[--len] != ':' ) {
					len++;
				}
			}
			if( quote ) {
				buff2[len++] = '"';
			}
			buff2[len] = 0;
			StuffScanText(buff2, SCANVOL_TEXT);
		} else {
			StuffScanText(buff, item);
		}
		prevScanReqRP = requester->ReqLayer->rp;
		UnlockLayer(requester->ReqLayer);
	} else {
		prevScanReqRP = NULL;
	}
}

/*
 * Makes a copy of the text passed, and puts in an ellipsis if it won't fit
 * and stores it in the gadget passed.
 */
 
static void StuffScanText(TextPtr buff, WORD item)
{
	Rectangle rect, rect2;
	GadgetPtr gadgList = requester->ReqGadget;
	register WindowPtr window = requester->RWindow;
	register RastPtr rp = requester->ReqLayer->rp;
	
	GetGadgetRect(GadgetItem(gadgList, item), window, requester, &rect);
	rect.MaxX = requester->Width - 4;
	if( item == SCANFILE_TEXT ) {
		GetGadgetRect(GadgetItem(gadgList, CANCEL_BUTTON), window, requester, &rect2);
		rect.MaxX = rect2.MinX-1;
	}
	rect.MaxY = rect.MinY + charHeight;
	SetAPen(rp, _tbPenLight);
	FillRect(rp, &rect);
	if( buff != NULL ) {
		SetAPen(rp, _tbPenBlack);
		Move(rp, rect.MinX, rect.MinY + charBaseline);
		TextInWidth(rp, buff, strlen(buff), rect.MaxX - rect.MinX, FALSE);
	}
}

/*
 * Call StuffScanText() for the directory gadget text w/ full path - volume name
 */
 
void StuffScanDirText(register TextPtr dirPtr)
{
	if( dirPtr != NULL ) {
		if( mode == MODE_BACKUP ) {
			dirPtr += strlen(prefs.OldPrefs.DevPathBuf);
			if( *dirPtr == '/' ) {
				dirPtr++;
			}
		}
	}
	DoStuffScanText(dirPtr, SCANDIR_TEXT);
}

/*
 * Call StuffScanText() for file gadget
 */

void StuffScanFileText(TextPtr filePtr)
{
	DoStuffScanText(filePtr, SCANFILE_TEXT);
}

/*
 * Adjust the width of the horizontal dimensions in pixels
 */
 
void AdjustWidth()
{
	register WORD i;
	struct DateStamp dateStamp;

	tabbing[1] = tabChars[1] * charWidth;
	tabbing[2] = tabChars[2] * charWidth;
	
	tabChars[3] = widthDateFormat[prefs.CatOpts.DateFormat];
	dateStamp.ds_Days = 10;
	dateStamp.ds_Minute = 1200;
	dateStamp.ds_Tick = 0;
	XDateString( &dateStamp, prefs.CatOpts.DateFormat, strBuff);
	tabbing[3] = TextLength(cmdWindow->RPort, strBuff, strlen(strBuff)) + charWidth;
	tabChars[4] = 6;
	if( prefs.CatOpts.ShowSecs )
		tabChars[4] += 3;
	if( prefs.CatOpts.ShowAMPM )
		tabChars[4] += 3;
	TimeString( &dateStamp, prefs.CatOpts.ShowSecs, prefs.CatOpts.ShowAMPM, strBuff);
	tabbing[4] = TextLength(cmdWindow->RPort, strBuff, strlen(strBuff)) + charWidth;
	
	pagePixWidth = tabbing[0] + iconList[DRAWER_ICON]->Width + (iconList[CHECK_ICON_MASK]->Width >> 2);
	
	for( i = 0 ; i <= 4 ; i++ ) {
		tabTable[i] = tabbing[i];
	}
	if( prefs.CatOpts.IncludeSize )
		pagePixWidth += tabbing[1];
	else
		tabTable[1] = 0;
	if( prefs.CatOpts.IncludeProt )
		pagePixWidth += tabbing[2];
	else
		tabTable[2] = 0;
	if( prefs.CatOpts.IncludeDate )
		pagePixWidth += tabbing[3];
	else
		tabTable[3] = 0;
	if( prefs.CatOpts.IncludeTime )
		pagePixWidth += tabbing[4];
	else
		tabTable[4] = 0;
	AdjustScrollBars(cmdWindow);
	pageWidth = (pagePixWidth / charWidth) + 1;
}

/*
 * Request the user to type in a password for this backup if he/she so desires.
 */
 
void DoPasswordRequest(TextPtr passwordPtr)
{
	register RequestPtr req;
	register DialogPtr window = cmdWindow;
	register BOOL done;
	
	BeginWait();
	if( (req = DoGetRequest(REQ_PROMPTPASS)) != NULL ) {
		requester = req;
		OutlineOKButton(window);
		done = FALSE;
		do {
			switch( ModalRequest(mainMsgPort, window, DialogFilter) ) {
			case OK_BUTTON:
				GetEditItemText(req->ReqGadget, PASS_EDIT_TEXT, strBuff);
				strncpy(passwordPtr, strBuff, MAX_PASSWORD_LEN);
			case CANCEL_BUTTON:
				done = TRUE;
			default:
				break;
			}
		
		} while(!done);
		DestroyRequest(req);
	}
	EndWait();
	return;
}

/*
 * Request the password that was specified for this backup.
 */
 
BOOL DoAskPasswordRequest(TextPtr passwordPtr)
{
	register RequestPtr req;
	register DialogPtr window = cmdWindow;
	register BOOL done;
	WORD item;
	BOOL success = FALSE;
	register GadgetPtr gadgList;
	register WORD retries = 0;
	
	BeginWait();
	SetArrowPointer();
	if( (req = DoGetRequest(REQ_ASKPASS)) != NULL ) {
		gadgList = req->ReqGadget;
		requester = req;
		OutlineOKButton(window);
		done = FALSE;
		do {
			item = ModalRequest(mainMsgPort, window, DialogFilter);
			switch(item) {
			case OK_BUTTON:
				GetEditItemText(gadgList, PASS_EDIT_TEXT, strBuff);
				if( strncmp(passwordPtr, strBuff, MAX_PASSWORD_LEN) == 0 ) {			
					success = TRUE;
				} else {
					ErrBeep();
					SetGadgetText(GadgetItem(gadgList, PASS_STAT_TEXT), window, req, strWrongPass);
					if( ++retries < 3 ) {
						SetEditItemText(gadgList, PASS_EDIT_TEXT, window, req, NULL);
						ActivateGadget(GadgetItem(gadgList, PASS_EDIT_TEXT), window, req);
						Delay(5);
						break;
					}
				}
			case CANCEL_BUTTON:
				done = TRUE;
			default:
				break;
			}
		
		} while(!done);
		DestroyRequest(req);
	}
	EndWait();
	if( catErrFlag ) {
		SetStdPointer(cmdWindow, POINTER_WAIT);
	}
	return(success);
}

/*
 * From restore, prompt the user to rename this file
 * Returns filename to use, or NULL if this file is not to be restored.
 */
 
TextPtr RenameFileRequest(TextPtr origFileName)
{
	register RequestPtr req;
	DialogPtr window = cmdWindow;
	register BOOL done;
	register TextPtr str = NULL;
	register GadgetPtr gadgList;
	TextChar parseBuff[MAX_FILENAME_LEN + 50/*strlen(strRenameFile)*/];
	TextChar editBuff[GADG_MAX_STRING];
	File lock;
	WORD item;
		
	BeginWait();
	AutoActivateEnable(FALSE);
	req = DoGetRequest(REQ_RENAMEFILE);
	AutoActivateEnable(TRUE);
	if( req != NULL ) {
		gadgList = req->ReqGadget;
		requester = req;
		OutlineOKButton(window);
		dialogVarStr = str = origFileName;
		ErrBeep();
		do {
			ParseDialogString(strRenameFile, parseBuff);
			SetGadgetText(GadgetItem(gadgList, RENAME_STAT_TEXT), window, req, parseBuff);
			SetEditItemText(gadgList, RENAME_EDIT_TEXT, window, req, str);

			item = ModalRequest(mainMsgPort, window, DialogFilter);
			done = TRUE;
			switch(item) {
			case RENAME_BUTTON:
			case REPLACE_BUTTON:
				GetEditItemText(gadgList, RENAME_EDIT_TEXT, editBuff);
				dialogVarStr = editBuff;
				str = strBuff;
				strcpy(strBuff, strPath);
				AppendDirPath(strBuff, editBuff);
				if( (item == RENAME_BUTTON) && (lock = Lock(str, ACCESS_READ)) ) {
					UnLock(lock);
					BumpRevision(strBuff, editBuff);
					done = FALSE;
					break;
				}
				break;
			case CANCEL_BUTTON:
				abortFlag = TRUE;
			case SKIP_BUTTON:
				str = NULL;
			default:
				break;
			}
		
		} while(!done);
		DestroyRequest(req);
	}
	EndWait();
	return(str);
}

/*
 * Display the number of files and bytes total and selected, in the main window.
 */
 
void DisplayStats(BOOL full)
{
	TextChar buff[20];
	Rectangle rect;
	register WindowPtr window;
	GadgetPtr gadgList;
	RastPtr rPort;
		
	if( (operation != OPER_LIMBO) && (operation != OPER_SCAN) ) {
		if( full ) {
/*
	The following text drawing is necessary only because the clipping is
	not respected when the window is made smaller.
*/
			window = cmdWindow;
			gadgList = window->FirstGadget;
			rPort = window->RPort;
			GetGadgetRect(GadgetItem(gadgList, TOTAL_BOX), window, NULL, &rect);
			/*
			rect.MinX++;
			rect.MinY++;
			*/
			DrawShadowBox(rPort, &rect, 0, FALSE);
			GetGadgetRect(GadgetItem(gadgList, SEL_BOX), window, NULL, &rect);
			/*
			rect.MinX++;
			rect.MinY++;
			*/
			DrawShadowBox(rPort, &rect, 0, FALSE);
			
			DrawCText(strSel, SEL_TEXT);
			DrawCText(strFiles, FILES1_TEXT);
			DrawCText(strFiles, FILES2_TEXT);
			DrawCText(strBytes, BYTES1_TEXT);
			DrawCText(strBytes, BYTES2_TEXT);
/*
	If in file selection, the top files/bytes info box contains the total
	number of bytes on the volume.
*/
			if( operation == OPER_SELECTION ) {
				DrawCText(strTotal, TOTAL_TEXT);
				NumToCommaString(totalFiles, buff);
				DrawCText(buff, TOT_FILES_TEXT);
				NumToCommaString(totalBytes, buff);
				DrawCText(buff, TOT_BYTES_TEXT);
			} else {
				DisplayAllDevicesStatus();
			}
		}
		if( operation != OPER_SELECTION ) {
			DrawCText(strCompleted, TOTAL_TEXT);
			lastPercent = 0;							/* Ensure it is drawn */
			lastTick = 0;
			UpdateFilesStat();
			UpdateBytesStat();
		}
		if( full || (operation == OPER_SELECTION ) ) {
			NumToCommaString(selFiles, buff);
			DrawCText(buff, SEL_FILES_TEXT);
			NumToCommaString(selBytes, buff);
			DrawCText(buff, SEL_BYTES_TEXT);
		}
	}
}

/*
 * Update files line in backup/restore.
 */
 
void UpdateFilesStat()
{
	TextChar buff[20];
	
	NumToCommaString(stats.completedFiles, buff);
	DrawCText(buff, TOT_FILES_TEXT);
}

/*
 * Update bytes line in backup/restore.
 */
 
void UpdateBytesStat()
{
	TextChar buff[20];
	register WORD percent = 0;
	register ULONG bytes;
	register WORD tBytes;
	register WORD shift;
	register TextPtr bufPtr;
	ULONG cBytes = stats.completedBytes;
	struct DateStamp currDateStamp;
	ULONG ticks;
	
	DateStamp(&currDateStamp);
	ticks = (currDateStamp.ds_Minute * 3000) + currDateStamp.ds_Tick;
	if( (ticks - lastTick) > 20 ) {
		lastTick = ticks;
		NumToCommaString(cBytes, buff);
		DrawCText(buff, TOT_BYTES_TEXT);
		*((USHORT *)&buff[0]) = 0x2020;
		for( shift = 0, bytes = selBytes; bytes > 32767; shift++, bytes >>= 1 );
		
		if( tBytes = bytes ) {
			bytes = MIN(selBytes, cBytes) >> shift;
			percent = (bytes * 100) / tBytes;
		}
		if( (percent == 0) || (lastPercent != percent) ) {
			bufPtr = &buff[0];
			if( percent < 100 ) {
				bufPtr++;
			}
			if( percent < 10 ) {
				bufPtr++;
			}
			NumToString(percent, bufPtr);
			buff[3] = '%';
			buff[4] = 0;
			DrawCText(buff, COMPL_PCT_TEXT);
			lastPercent = percent;
		}
	}
}

/*
 * Update both the files and bytes lines.
 */
 
void UpdateStats()
{
	static ULONG lastCompletedFiles;
	
	if( (lastTick == 0L) || (lastCompletedFiles == 0) || (stats.completedFiles != lastCompletedFiles) ) {
		lastCompletedFiles = stats.completedFiles;
		UpdateFilesStat();
	}	
	UpdateBytesStat();
}

/*
 * Draw a C string into the gadget item passed, right justified.
 */
 
static void DrawCText(TextPtr text, WORD item)
{
	register RastPtr rp = cmdWindow->RPort;
	Rectangle rect;
	WORD len;
	
	GetGadgetRect(GadgetItem(cmdWindow->FirstGadget, item), cmdWindow, NULL, &rect);
	len = strlen(text);
	rect.MaxY = rect.MinY + charHeight;
	if( (item > SEL_BYTES_TEXT) && (item != COMPL_PCT_TEXT) ) {
		rect.MaxX = START_X;
		Move(rp, rect.MinX, rect.MinY + screen->RastPort.Font->tf_Baseline);
	} else {
		rect.MaxX = (END_X * _tbXSize) >> 3;
		rect.MaxY -= 1;
		SetAPen(rp, _tbPenLight);
		FillRect(rp, &rect);
		Move(rp, rect.MaxX - TextLength(rp, text, len), rect.MinY + screen->RastPort.Font->tf_Baseline);
	}
	SetAPen(rp, _tbPenBlack);
	Text(rp, text, len);
}

/*
 * Displays device status for all drives from firstDevice on down.
 */
 
void DisplayAllDevicesStatus()
{
	register DevParamsPtr devParam;
	register BOOL found;
	register WORD unit;
	WORD maxUnits;
	
	if( startMode == MODE_BACKUP ) {
		maxUnits = (prefs.Destination == DEST_FLOPPY) ? 4 : 1;
	} else {
		maxUnits = (prefs.Source == DEST_FLOPPY) ? 4 : 1;
	}
	for( unit = 0 ; unit < maxUnits ; unit++ ) {
		found = FALSE;
		for( devParam = firstDevice; devParam != NULL ; devParam = devParam->Next ) {
			if( !devParam->Floppy || devParam->Unit == unit ) {
				found = TRUE;
				break;
			}
		}
		DisplayDriveStatus(found ? devParam : NULL, unit);
	}
}
		
/*
 * Display the drive status of the specified drive
 */

void DriveStatus(register DevParamsPtr devParam)
{
	DisplayDriveStatus(devParam, devParam->Unit);
}

/*
 * The workhorse of displaying the drive status of the given drive.
 */
 
static void DisplayDriveStatus(register DevParamsPtr devParam, WORD line)
{
	register TextPtr text;
	Rectangle rect, rect2;
	register RastPtr rp = cmdWindow->RPort;
	TextChar buff[60];
	register WORD incr;
	register WORD i;
	register BOOL disable = devParam == NULL;
	register BOOL isFloppy;
	TextChar parseStr[60];
	ImagePtr icon;
	BitMap bitMap;
	register UBYTE status;
	APTR mask;
	BOOL isActive = FALSE;
	BOOL isReady = FALSE;
	WORD width, height;
	WORD depth = screen->RastPort.BitMap->Depth;
	LONG saveVarNum = dialogVarNum;
	
	if( numDevices && (devParam != NULL) ) {
		status = devParam->Status;
		if( startMode == MODE_BACKUP ) {
			isFloppy = prefs.Destination == DEST_FLOPPY;
		} else {
			isFloppy = prefs.Source == DEST_FLOPPY;
		}
		if( !isFloppy ) {
			line = 0;
		}
	}
/*	dialogVarStr = tapeDevice ? strTape : strDisk;*/	
	
	if( (operation != OPER_LIMBO) && (operation != OPER_SCAN) && (operation != OPER_SELECTION) ) {
		if( startMode == MODE_BACKUP ) {
			isFloppy = prefs.Destination == DEST_FLOPPY;
		} else {
			isFloppy = prefs.Source == DEST_FLOPPY;
		}
		if( disable ) {
			if( floppyDrives[line] && !floppyBusy[line] ) {
				if( floppyEnable[line] ) {
					text = outputCompleted ? strDevDone : strDevNReady;
					disable = FALSE;
				} else {
					text = strDevNotSel;
				}
			} else {
				text = strDevNAvail;
			}
		} else {
			if( !useAmigaDOS ) {
				if( ( (devParam->Floppy && (devParam->Unit < 4) && floppy525[devParam->Unit])
					 || (notSCSI && !isFloppy) ) &&
					 ( (status == STATUS_READY) || ((status == STATUS_CHECK) && (operation != OPER_IN_PROG)) ) ) {
					status = STATUS_NOT_READY;
					devParam->Msg = devParam->NextMsg;
				}
				if( (!(dialogVarNum = (status == STATUS_NOT_READY) ? devParam->NextMsg : devParam->Msg)) ) {
					status = STATUS_DONE;
				}
			}
			switch(status) {
			case STATUS_NOT_READY:
			default:
				text = strDevNReady;
				break;
			case STATUS_CHECK:
				text = useAmigaDOS ? strDevCheckF : strDevCheck;
				if( operation == OPER_IN_PROG ) {
					isActive = TRUE;
					break;
				}
			case STATUS_READY:
				text = strDevReady;
				isReady = TRUE;
				break;
			case STATUS_ACTIVE:
				isActive = TRUE;
				if( useAmigaDOS ) {
					text = (mode == MODE_BACKUP) ? strDevActiveBF : strDevActiveRF;
				} else {
					text = (mode == MODE_BACKUP) ? strDevActiveB : strDevActiveR;
				}
				break;
			case STATUS_REMOVE:
				text = strDevRemove;
				break;
			case STATUS_DONE:
				text = strDevDone;
				break;
			case STATUS_PAUSED:
				text = strDevPaused;
				isActive = TRUE;
				break;
			}
		}
		parseStr[0] = 0;
		if( isFloppy ) {
			strcpy(parseStr, (devParam == NULL) ? driveStrings[line] : devParam->DevName);
			strcat(parseStr, strSpace);
		}
		strcat(parseStr, text);

		(void) ParseDialogString(parseStr, buff);
		text = &buff[0];
		dialogVarNum = saveVarNum;
		
		GetGadgetRect(GadgetItem(cmdWindow->FirstGadget, DRIVE_ICON_BORDER), cmdWindow, NULL, &rect);			
		GetGadgetRect(GadgetItem(cmdWindow->FirstGadget, DRIVE_TEXTBOX), cmdWindow, NULL, &rect2);
		incr = (rect.MaxY - rect.MinY) >> 2;
		if( !disable ) {
			icon = GetIconAndMask(isActive, isReady, &mask);

			width = icon->Width;
			height = icon->Height;			
			OffsetRect(&rect, ((rect.MaxX - rect.MinX) - width) >> 1,
				(screen->RastPort.Font->tf_Baseline - charHeight) >> 1);
		
			InitBitMap(&bitMap, depth, width, height);
			for (i = 0; i < depth; i++) {
				bitMap.Planes[i] = (PLANEPTR) icon->ImageData + i*RASSIZE(width, height);
			}
			rect.MinY += (line * incr);
			rect.MaxY = rect.MinY + (incr-1);
			
			BltMaskBitMapRastPort(&bitMap, 0, 0, rp, rect.MinX, rect.MinY, 
				width, height, 0xE0, mask);
		}
		i = strlen(text);
		rect2.MinY += (line * incr);
		rect2.MaxY = rect2.MinY + (incr-1);
		SetAPen(rp, _tbPenLight);
		FillRect(rp, &rect2);
		SetAPen(rp, _tbPenBlack);
		Move(rp, rect2.MinX, rect2.MinY + screen->RastPort.Font->tf_Baseline);
		Text(rp, text, i);
		if( disable ) {
			SetAfPt(rp, &grayPat[0], 1);
			SetAPen(rp, _tbPenLight);
			FillRect(rp, &rect2);
			SetAfPt(rp, NULL, 0);
		} else if( tapeDevice && !prefs.TapeOpts.AutoRewind && isActive) {
			/*
			OffsetRect(&rect2, 0, incr + incr);
			SetAPen(rp, _tbPenLight);
			FillRect(rp, &rect2);
			SetAPen(rp, _tbPenBlack);
			*/
			Move(rp, rect2.MinX, rp->cp_y + incr + incr);
			text = &parseStr[0];
			strcpy(text, strSessionNum);
			NumToString(sessionNumber+1, buff);
			strcat(text, buff);
			Text(rp, text, strlen(text));
		}
	}
}

/*
 * Return the proper icon and mask for this type of icon.
 */
 
static ImagePtr GetIconAndMask(BOOL isActive, BOOL isReady, APTR *maskPtr)
{
	register WORD id, maskID, grayID, blackID;
	ImagePtr icon;
		
	if( useAmigaDOS ) {
		id = DOC_ICON;
		maskID = DOC_ICON_MASK;
		grayID = DOC_ICON_GRAY;
		blackID = DOC_ICON_BLACK;
	} else if( !tapeDevice ) {
		id = VOL_ICON;
		maskID = VOL_ICON_MASK;
		grayID = VOL_ICON_GRAY;
		blackID = VOL_ICON_BLACK;
	} else {
		id = TAPE_ICON;
		maskID = TAPE_ICON_MASK;
		grayID = TAPE_ICON_GRAY;
		blackID = TAPE_ICON_BLACK;			
	}
	if( isActive ) {
		icon = iconList[blackID];
	} else {
		icon = isReady ? iconList[id] : iconList[grayID];
	}
	*maskPtr = iconList[maskID]->ImageData;
	return(icon);
}

/* 
 * Change device status to not ready and display.
 */
 
void StatusNotReady(DevParamsPtr devParam)
{
	RefreshDeviceStatus(devParam, STATUS_NOT_READY);
}
	
/*
 * Change device status to status ready and display.
 */

void StatusReady(DevParamsPtr devParam)
{
	devParam->Msg = devParam->NextMsg;
	RefreshDeviceStatus(devParam, STATUS_READY);
}
 
/*
 * Display and update status to STATUS_ACTIVE only if not active beforehand.
 */

void StatusActive(DevParamsPtr devParam)
{
	if( devParam->Status == STATUS_PAUSED ) {
		RefreshDeviceStatus(devParam, STATUS_CHECK);
	} else {
		devParam->Msg = devParam->NextMsg;
		RefreshDeviceStatus(devParam, STATUS_ACTIVE);
	}
}

/*
 * Display and update status to STATUS_REMOVE
 */

void StatusRemove(DevParamsPtr devParam)
{
	RefreshDeviceStatus(devParam, STATUS_REMOVE);
}

/*
 * Change device status to completed.
 */
 
void StatusCompleted(DevParamsPtr devParam)
{
	RefreshDeviceStatus(devParam, STATUS_DONE);
}

/*
 * Change device status to paused.
 */

void StatusPaused(DevParamsPtr devParam)
{
	RefreshDeviceStatus(devParam, STATUS_PAUSED);
}

/*
 * Change device status to check track zero.
 */

void StatusCheck(DevParamsPtr devParam)
{
	devParam->Msg = devParam->NextMsg;
	RefreshDeviceStatus(devParam, STATUS_CHECK);
}

/*
 * Update the appropriate device status and display it.
 */
 
void RefreshDeviceStatus(DevParamsPtr devParam, WORD status)
{
	if( devParam->Status != status ) {
		devParam->Status = status;
		DriveStatus(devParam);
	}
}

/*
 * Disable further updating of scroll list and disable the scroll list gadgets
 * Call when "No display..." checkbox on
 */
 
void DisableScrollList()
{
	SLDoDraw(scrollList, FALSE);
/*	OffGList(GadgetItem(cmdWindow->FirstGadget, LEFT_ARROW), cmdWindow, NULL, 3);
	OffGList(GadgetItem(cmdWindow->FirstGadget, UP_ARROW), cmdWindow, NULL, 3);
*/
}

/*
 * Re-enable the scroll list gadgets and update the scroll list.
 * Call when "No display..." checkbox on
 */

void EnableScrollList()
{
/*
	OnGList(GadgetItem(cmdWindow->FirstGadget, LEFT_ARROW), cmdWindow, NULL, 3);
	OnGList(GadgetItem(cmdWindow->FirstGadget, UP_ARROW), cmdWindow, NULL, 3);
*/
	SLDoDraw(scrollList, !skipUpdate);
}

 
/*
 * Refresh the scroll list's status
 */
 
void RefreshScrollList()
{
	if( disableDisplay ) {
		DisableScrollList();
	} else {
		EnableScrollList();
	}
}
