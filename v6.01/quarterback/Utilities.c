/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software
 *
 *	Utilities menu functions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/DateTime.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Request.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/List.h>
#include <Toolbox/Graphics.h>

#include <string.h>

#include "QB.h"
#include "Proto.h"
#include "Tape.h"
#include "DlogDefs.h"

/*
 *	External variables
 */

extern UBYTE		_tbPenBlack;

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern ProgPrefs	prefs;

extern RequestPtr	requester;
extern TextChar	strBuff[], strBackup[], strRestore[];

extern WindowPtr	cmdWindow;
extern UBYTE		startMode, mode, operation;

extern TextPtr		strDriveType[], strsErrors[];
extern TextChar	strTpDrDATape[], strUnknown[], strM[], strDevNotFnd[];
extern TextChar	strComma[], strAt[], strTpDrVar[], strTpDrFixed[];
extern TextChar	strTrackdisk[];
extern TextChar	strBackupOf[], strBackupCr[], strBackupOld[];
extern TextChar	currVolName[], currBackupName[], currComment[];
extern TextChar	strSession[], strSessions[];

extern BOOL			needRewind, sequentialDevice, tapeDevice;
extern LONG			dialogVarNum;
extern TextPtr		dialogVarStr, currDevPtr;
extern WORD			currUnit, qbCode;

extern struct DateStamp sessionDateStamp;
extern ListHeadPtr	devList;
extern BOOL			noSCSI;
extern UBYTE		_tbPenLight;
extern WORD			charBaseline;

/*
 *	Local variables and definitions
 */

static BOOL	CheckForTapeDrive(UBYTE *);
static BOOL	DoSessionInfo(BOOL);

/*
 *	Adjust tape options
 */

BOOL DoTapeUtilities()
{
	register WORD item;
	register GadgetPtr gadgList;
	register DialogPtr window = cmdWindow;
	register RequestPtr req;
	UBYTE inqData[INQ_LEN];
	BOOL done = FALSE;
	BOOL success = FALSE;
	register WORD numSpaces = 1;
	register BOOL recalc = TRUE;
	BOOL quick = FALSE;

/*
	If not trackdisk.device, go ahead and get requester
 	if (strcmp(dev, strTrackdisk) && CheckSCSIDrive(dev, unit, inqData, FALSE)) { 
*/
	if (!noSCSI) {
		BeginWait();
		SetCurrentDev();
		CheckSCSIDrive(currDevPtr, currUnit, inqData, FALSE);
		req = DoGetRequest(REQ_TAPECONTROL);
		if (req == NULL)
			Error(ERR_NO_MEM);
		else {
			requester = req;
			gadgList = req->ReqGadget;
/*
	If not a tape drive, disable the works. If not sequential, disable spacing only.
*/
			if (!tapeDevice)
				OffGList(GadgetItem(gadgList, ERASE_BUTTON), window, req, (QUICK_BOX - ERASE_BUTTON)+1);
			EnableGadgetItem(gadgList, QUICK_BOX, window, req, tapeDevice && sequentialDevice);
			OutlineOKButton(window);
/*
	Handle requester
*/
			do {
				if (recalc) {
					recalc = sequentialDevice && !prefs.TapeOpts.AutoRewind;
					EnableGadgetItem(gadgList, SPACE_BUTTON, window, req, recalc);
					NumToString(numSpaces, strBuff);
					strcat(strBuff, (numSpaces == 1) ? strSession : strSessions);
					SetGadgetItemText(gadgList, NUMSPACE_TEXT, window, req, strBuff);
					EnableGadgetItem(gadgList, NUMSPACE_UPARROW, window, req, recalc && numSpaces < 99);
					EnableGadgetItem(gadgList, NUMSPACE_DNARROW, window, req, recalc && numSpaces > 1);
					recalc = FALSE;
				}					
				item = ModalRequest(mainMsgPort, window, DialogFilter);
				switch (item) {
				case OK_BUTTON:
					success = TRUE;
				case CANCEL_BUTTON:
					done = TRUE;
					break;
				case ERASE_BUTTON:
					if (CheckForTapeDrive(inqData)) {
						if (!WarningDialog(ERR_ERASE_WARN, TRUE)) {
/*
 * The 150 megabyte Caliper and Wangteks (and maybe more) seem to not be
 * able to handle immediate erases. Idiots.
 */
							if (!EraseTape(NULL, FALSE, quick))
								Error(ERR_ERASE_FAILED);
						}
					}
					break;
				case REWIND_BUTTON:
					if (CheckForTapeDrive(inqData)) {
						needRewind = TRUE;		/* I don't care what reality is, just do it! */
						RewindTape(NULL, FALSE);/* Now "needRewind" will be FALSE. */
						InitPosition();
					}
					break;
				case RETENSION_BUTTON:
					if (CheckForTapeDrive(inqData)) {
						if (!WarningDialog(ERR_RETENSION_WARN, TRUE)) {
							RetensionTape(NULL, FALSE);
						}
					}
					break;
				case SPACE_BUTTON:
					if (CheckForTapeDrive(inqData) && sequentialDevice)
						DoSpaceTape(NULL, numSpaces);
					break;
				case NUMSPACE_UPARROW:
					if (numSpaces < 99) {
						recalc = TRUE;
						numSpaces++;
					}
					break;
				case NUMSPACE_DNARROW:
					if (numSpaces > 1) {
						recalc = TRUE;
						numSpaces--;
					}
				case QUICK_BOX:
					ToggleCheckbox(&quick, item, window);
					break;
				default:
					break;
				}	
			} while (!done);
			DestroyRequest(req);
		}
		EndWait();
		SetOptionsMenu();
	}
	return (success);
}

/*
 * Make sure a tape drive is hooked up - if not display the problem.
 */
 
static BOOL CheckForTapeDrive(UBYTE *inqData)
{
	BOOL success = FALSE;
	
	if (tapeDevice) {
		if (IS_REMOVABLE(inqData)) {
			success = TRUE;
		} else {
			Error(ERR_NOT_REMOVABLE);
		}
	} else {
		Error(ERR_NO_SCSI_DRIVE);
	}
	return (success);
}

/*
 * Get session info
 */
 
BOOL DoGetSessionInfo()
{
	return (DoSessionInfo(FALSE));
}

/*
 * Set session info
 */
 
BOOL DoSetSessionInfo()
{
	return (DoSessionInfo(TRUE));
}

/*
 * Display SessionInfo requester, allow changes if "set" TRUE.
 */
 
static BOOL DoSessionInfo(BOOL set)
{
	WORD item;
	register RequestPtr req;
	BOOL success = FALSE;
	WORD dlgID = set ? REQ_SETSESSINFO : REQ_GETSESSINFO;
	TextChar buff[20];
	register GadgetPtr gadgList;
	Rectangle rect;
	RastPtr rp;
	register WindowPtr window = cmdWindow;
	
	if (mode != MODE_LIMBO) {
		BeginWait();
		if ((req = DoGetRequest(dlgID)) == NULL) {
			Error(ERR_NO_MEM);
		} else {
			requester = req;
			gadgList = req->ReqGadget;
			OutlineOKButton(cmdWindow);
			if (!set) {
				if ((startMode == MODE_RESTORE) && (qbCode != QB_ID)) {
					strcpy(strBuff, strBackupOld);
				} else {
					strcpy(strBuff, strBackupOf);
					strcat(strBuff, currVolName);
				}
				strcat(strBuff, strBackupCr);
				XDateString(&sessionDateStamp, prefs.CatOpts.DateFormat, buff);
				strcat(strBuff, buff);
				strcat(strBuff, strAt);
				TimeString(&sessionDateStamp, FALSE, TRUE, buff);
				strcat(strBuff, buff);
				SetGadgetItemText(gadgList, DATE_LABEL, window, req, strBuff);
				Forbid();
				if (req->ReqLayer != NULL) {
					rp = req->ReqLayer->rp;
					SetAPen(rp, _tbPenBlack);
					SetDrMd(rp, JAM1);
					SetFont(rp, screen->RastPort.Font);
					GetGadgetRect(GadgetItem(gadgList, NAME_TEXT), window, req, &rect);
					rect.MinX += 3;
					Move(rp, rect.MinX, rect.MinY + (charBaseline + 3));
					TextInWidth(rp, currBackupName, strlen(currBackupName), rect.MaxX - rect.MinX, FALSE);
					GetGadgetRect(GadgetItem(gadgList, COMMENT_TEXT), window, req, &rect);
					rect.MinX += 3;
					Move(rp, rect.MinX, rect.MinY + (charBaseline + 3));
					TextInWidth(rp, currComment, strlen(currComment), rect.MaxX - rect.MinX, FALSE);
				}
				Permit();
				/*
				SetGadgetItemText(gadgList, NAME_TEXT, window, req, currBackupName);
				SetGadgetItemText(gadgList, COMMENT_TEXT, window, req, currComment);
				*/
			} else {
				SetEditItemText(gadgList, NAME_TEXT, window, req, currBackupName);
				SetEditItemText(gadgList, COMMENT_TEXT, window, req, currComment);
			}
			do {
				item = ModalRequest(mainMsgPort, window, DialogFilter);
			} while (item != OK_BUTTON && item != CANCEL_BUTTON);
			if (success = item == OK_BUTTON) {
				if (set) {
					GetEditItemText(gadgList, NAME_TEXT, currBackupName);
					GetEditItemText(gadgList, COMMENT_TEXT, currComment);
				}
			}
			DestroyRequest(req);
		}
		EndWait();
	}
	return (success);
}

/*
 * Update interrogator strings
 */
 
static void UpdateInterrogator(UBYTE *inqData)
{
	register RequestPtr req = requester;
	register WindowPtr window = cmdWindow;
	GadgetPtr gadgList = requester->ReqGadget;
	register TextPtr str;
	UBYTE devType;
	TextPtr str2 = strUnknown;
	ULONG currBlkSize;
	ULONG high;
	UWORD low;
	register ULONG num;
	ULONG numBlks = 0L;
	UBYTE revBuff[5];
	
	strncpy(revBuff, &inqData[32], 4);
	revBuff[4] = 0;
	strncpy(strBuff, &inqData[8], 8);
	strBuff[8] = 0;
	SetGadgetItemText(gadgList, VENDOR_TEXT, window, req, strBuff);
	strncpy(strBuff, &inqData[16], 16);
	strBuff[16] = 0;
	SetGadgetItemText(gadgList, MODEL_TEXT, window, req, strBuff);
	SetGadgetItemText(gadgList, REV_TEXT, window, req, revBuff);
	devType = inqData[0] & 0x1F;
	if (devType < 10) {
		str = strDriveType[devType];
	} else if (devType == 16) {
		str = strTpDrDATape;
	} else if (devType == 31) {
		str = strDevNotFnd;
	} else {
		str = strUnknown;
	}
	SetGadgetItemText(gadgList, DRIVETYPE_TEXT, window, req, str);
	if (devType != 31) {
		if (currBlkSize = GetBlockSizes(devType == 1, &high, &low, &numBlks)) {
			NumToString(currBlkSize, strBuff);
			strcat(strBuff, strComma);
			if (((ULONG)low) != high) {
				str = strTpDrVar;
			} else {
				str = strTpDrFixed;
			}
			strcat(strBuff, str);
			str = strBuff;
			
		} else {
			str = strUnknown;
		}
	} else {
		str = str2 = strBuff;
	}
	SetGadgetItemText(gadgList, BLKSIZE_TEXT, window, req, str);
	num = numBlks;
	if (num) {
		str2 = strBuff;
		num *= currBlkSize;
		num >>= 20;
		NumToString(num, str2);
		strcat(str2, strM);
	}
	SetGadgetItemText(gadgList, MEDIASIZE_TEXT, window, req, str2);
}

/*
 * Display requester containing tape drive statistics
 */
 
BOOL DoSCSIInterrogator()
{
	register WindowPtr window = cmdWindow;
	register RequestPtr req = NULL;
	UBYTE inqData[INQ_LEN];
	GadgetPtr gadgList;
	register WORD temp;	
	register WORD devListItem;
	register WORD count;
	register WORD item;
	TextChar dev[GADG_MAX_STRING];	/* DoCheckSCSIDrive() demands it */
	TextChar oldDev[MAX_FILENAME_LEN+1];
	WORD oldUnit, saveCurrUnit;
	TextPtr saveCurrDevPtr;
	WORD numListItems;
	LONG foundTDItem;
	
	if (!noSCSI) {
		BeginWait();
		AutoActivateEnable(FALSE);
		req = DoGetRequest(REQ_TAPEINFO);
		AutoActivateEnable(TRUE);
		if (req != NULL) {
			saveCurrDevPtr = currDevPtr;
			saveCurrUnit = currUnit;

			SetCurrentDev();
	
			strcpy(dev, currDevPtr);	
			currDevPtr = dev;
			
			requester = req;	
			OutlineOKButton(cmdWindow);
			gadgList = req->ReqGadget;
			
			UpdateDeviceList();
			foundTDItem = FindListItem(devList, strTrackdisk);
			if (foundTDItem != -1) {
				RemoveListItem(devList, foundTDItem);
			}
			numListItems = NumListItems(devList);
			if (numListItems < 2) {
				OffGList(GadgetItem(gadgList, SCSIDEV_UPARROW), window, req, 2);
			}
			devListItem = FindListItem(devList, dev);
			if (devListItem == -1) {
				if (numListItems) {
					devListItem = 0;
					currDevPtr = GetListItem(devList, devListItem);
					strcpy(dev, currDevPtr);
				} else {
					dev[0] = 0;
				}
				currUnit = 0;
			}
			count = 0;
			oldDev[0] = 0;
			
			SetEditItemText(gadgList, SCSIDEV_TEXT, window, req, dev);
			NumToString(currUnit, strBuff);
			SetEditItemText(gadgList, SCSIUNIT_TEXT, window, req, strBuff);
			item = -1;		
			do {
				switch (item) {
				case -1:
				if (DifferentDev(gadgList, oldDev, &oldUnit) && count) {
					count = 6;
				} else if (count >= 0) {
					if (count == 0) {
						memset(inqData, '-', INQ_LEN);
						inqData[0] = 0x1F;
						SetStdPointer(window, POINTER_WAIT);
						(void) DoCheckSCSIDrive(gadgList, &currUnit, currDevPtr, inqData, FALSE);
						UpdateInterrogator(inqData);
						SetArrowPointer();
					}
					count--;
				}
				break;
				case SCSIUNIT_UPARROW:
				case SCSIUNIT_DNARROW:
					GetEditItemText(gadgList, SCSIUNIT_TEXT, strBuff);
					temp = StringToNum(strBuff);
					if (item == SCSIUNIT_DNARROW) {
						if (temp) {
							temp--;
						} else {
							temp = 999;
						}
					} else {
						if (temp < 999) {
							temp++;
						} else {
							temp = 0;
						}
					}
					NumToString(temp, strBuff);
					SetEditItemText(gadgList, SCSIUNIT_TEXT, window, req, strBuff);
					count = 0;
					break;
				case SCSIDEV_UPARROW:
				case SCSIDEV_DNARROW:
					if (item == SCSIDEV_UPARROW) {
						devListItem++;
						if (devListItem == numListItems) {
							devListItem = 0;
						}
					} else {
						if (devListItem == 0) {
							devListItem = numListItems;
						}
						devListItem--;
					}
					SetEditItemText(gadgList, SCSIDEV_TEXT, window, req, GetListItem(devList, devListItem));
					count = 0;
					break;
				}
				WaitPort(mainMsgPort);
				item = CheckRequest(mainMsgPort, window, DialogFilter);
			} while (item != OK_BUTTON);
			DestroyRequest(req);
			if (foundTDItem != -1) {
				AddListItem(devList, strTrackdisk, strlen(strTrackdisk), foundTDItem);
			}
			currUnit = saveCurrUnit;
			currDevPtr = saveCurrDevPtr;
		}
		EndWait();
	}
	return (req != NULL);
}

/*
 *	Process Edit menu selection
 */

BOOL DoUtilitiesMenu(WindowPtr window, UWORD item, UWORD sub)
{
	BOOL success = FALSE;
	
	switch (item) {
	case TAPE_UTILITIES_ITEM:
		success = DoTapeUtilities();
		break;
	case GET_SESSINFO_ITEM:
		success = DoGetSessionInfo();
		break;
	case SET_SESSINFO_ITEM:
		success = DoSetSessionInfo();
		break;
	case SCSI_INTERROGATOR_ITEM:
		success = DoSCSIInterrogator();
	default:
		break;
	}
	return (success);
}
