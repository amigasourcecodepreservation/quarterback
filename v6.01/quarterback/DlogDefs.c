/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Quarterback
 * Copyright (c) 1992-93 New Horizons Software, Inc.
 *
 * Dialog/Gadget templates
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/PopUpList.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Language.h>

#include "QB.h"

extern TextChar strRename[], strReplace[], strCancel[];
extern TextChar strProgName[];
extern TextChar macroNames2[10][MACRONAME_LEN];

extern TextChar strDone[];

extern TextChar strBackup[], strRestore[], strCompare[], strTest[];
extern TextChar strOK[], strEmptyStr[], strSkip[], strSave[];
extern TextChar strEnter[], strBack[], strDisks[];
extern TextChar strDF0[], strDF1[], strDF2[], strDF3[];
extern TextChar strArchive1[], strUntag[];
extern TextChar strTpDrVendor[], strTpDrModel[], strTpDrRev[], strTpDrType[];
extern TextChar strTpDrBlkSz[], strTpDrMedSz[];
extern TextChar strPrint[], strQuestionMark[];

#define MY_ARROW_WIDTH	(ARROW_WIDTH-6)
#define MY_ARROW_HEIGHT	(ARROW_HEIGHT-2)

#define GT_UPARROW(left, top, key)	\
	{ GADG_ACTIVE_STDIMAGE,										\
		left, 1 + (top) + (MY_ARROW_HEIGHT / 2) - MY_ARROW_HEIGHT, 0, 0, MY_ARROW_WIDTH, MY_ARROW_HEIGHT, 0, 0,\
		key, 0, (Ptr) IMAGE_ARROW_UP }

#define GT_DNARROW(left, top, key)	\
	{ GADG_ACTIVE_STDIMAGE,										\
		left, 2 + (top) + (MY_ARROW_HEIGHT / 2), 0, 0, MY_ARROW_WIDTH, MY_ARROW_HEIGHT, 0, 0,	\
		key, 0, (Ptr) IMAGE_ARROW_DOWN }

#define GT_TEXT(left, top)	\
	{ GADG_STAT_TEXT, (left)+10+ARROW_WIDTH, top, 0, 0, 0, 0, 0, 0, 0, 0, NULL }

/*
#define GT_ADJUST_UPARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,													\
		left, (top)+5-ARROW_HEIGHT, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		'Y', 0, (Ptr) IMAGE_ARROW_UP }

#define GT_ADJUST_DOWNARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,										\
		left, (top)+5, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		'Z', 0, (Ptr) IMAGE_ARROW_DOWN }

#define GT_ADJUST_TEXT GT_TEXT
*/

/*
	 Repeated and general purpose text strings
*/

static TextChar strK[]			= "K";
static TextChar strDateSep[]	= "-";

#if (AMERICAN | BRITISH)

static TextChar strYes[]		= "Yes";
static TextChar strNo[]			= "No";
static TextChar strPrompt[]		= "Prompt";
static TextChar strCustom[]		= "Custom";
static TextChar strUse[]		= "Use";
static TextChar strDevice[]		= "Dev";
static TextChar strUnit[]		= "Unit";
static TextChar strSettings[]	= "Settings:";
static TextChar strFloppies[]	= "Floppy drives:";
static TextChar strTapes[]		= "Tape/Removables:";
static TextChar strADOSFile[]	= "AmigaDOS file:";
static TextChar strBackupName[]	= "Backup name:";
static TextChar strComments[]	= "Comments:";
static TextChar strOptions[]	= "More..";
static TextChar strBeep[]		= "Beep Sound";
static TextChar strFlash[]		= "Flash Screen";
static TextChar strPassword[]	= "Password:";

#elif GERMAN

static TextChar strYes[]		= "Ja";
static TextChar strNo[]			= "Nein";
static TextChar strPrompt[]		= "Eingabe";
static TextChar strCustom[]		= "Benutzereigene";
static TextChar strUse[]		= "Benutzen";
static TextChar strDevice[]		= "Treiber;		// was "Ger�t"
static TextChar strUnit[]		= "Einheit";
static TextChar strSettings[]	= "Einstellungen:";
static TextChar strFloppies[]	= "Diskettenlaufwerke:";
static TextChar strTapes[]		= "Band/Wechselplatten:";
static TextChar strADOSFile[]	= "AmigaDOS-Datei:";
static TextChar strBackupName[]	= "Name der Datensicherung:";
static TextChar strComments[]	= "Bemerkungen:";

#define strOptions strSettings

static TextChar strBeep[]		= "Ton ausgeben";
static TextChar strFlash[]		= "Anzeige blitzen";
static TextChar strPassword[]	= "Kennwort:"

#elif FRENCH

static TextChar strYes[]		= "Oui";
static TextChar strNo[]			= "Non";
static TextChar strPrompt[]		= "Demande";
static TextChar strCustom[]		= "Sp�cif.";
static TextChar strUse[]		= "OK";
static TextChar strDevice[]		= "Dev";
static TextChar strUnit[]		= "Unit�";
static TextChar strSettings[]	= "Options:";
static TextChar strFloppies[]	= "Lecteurs disq.:";
static TextChar strTapes[]		= "Bande/Amovibles:";
static TextChar strADOSFile[]	= "Fichier Amiga:";
static TextChar strBackupName[]	= "Nom sauvegarde:";
static TextChar strComments[]	= "Commentaires:";
static TextChar strOptions[]	= "Options";
static TextChar strBeep[]		= "Signal sonore";
static TextChar strFlash[]		= "Flash �cran";
static TextChar strPassword[]	= "Mot cl�:"

#elif SWEDISH

#endif

/*
	 Page Setup requester
*/

static TextPtr		paperList[] =
#if (AMERICAN | BRITISH)
	{ "US Letter", "US Legal", "A4 Letter",	"Wide Carriage", strCustom, NULL };
#elif GERMAN
	{ "US Letter", "US Legal", "DIN A4", "Breitformat", strCustom, NULL };
#elif FRENCH
	{ "Lettre US", "US L�gal", "Lettre A4", "Tracteur large" strCustom, NULL };
#endif

#if (AMERICAN | BRITISH)
static GadgetTemplate pageGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 40, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel },

	{ GADG_POPUP,		116, 40, 0, 0,130, 11, 0, 0, 0, 0, &paperList },
		
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 171, 70, 0, 0, 48, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 292, 70, 0, 0, 48, 11, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON,	 94,104, 0, 0,  0,  0, 0, 0,'P',0, "Pica" },
	{ GADG_RADIO_BUTTON,	168,104, 0, 0,  0,  0, 0, 0,'E',0, "Elite" },
	{ GADG_RADIO_BUTTON,	242,104, 0, 0,  0,  0, 0, 0,'d',0, "Condensed" },
		
	{ GADG_RADIO_BUTTON,	 94,124, 0, 0,  0,  0, 0, 0,'6',0, "6 LPI" },
	{ GADG_RADIO_BUTTON,	168,124, 0, 0,  0,  0, 0, 0,'8',0, "8 LPI" },
		
	{ GADG_CHECK_BOX,		 94,164, 0, 0,  0,  0, 0, 0,'N',0, "No Gaps Between Pages" },
	
	{ GADG_STAT_TEXT,		134, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	
	{ GADG_RADIO_BUTTON,	 94,144, 0, 0,  0,  0, 0, 0,'I',0, "Inches" },
	{ GADG_RADIO_BUTTON, 168,144, 0, 0,  0,  0, 0, 0,'m',0, "Centimeters" },
		
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Page Setup" },
	{ GADG_STAT_TEXT,		 20, 40, 0, 0,  0,  0, 0, 0, 0, 0, "Paper size:" },
	{ GADG_STAT_TEXT,		116, 70, 0, 0,  0,  0, 0, 0, 0, 0, "width:" },
	{ GADG_STAT_TEXT,		232, 70, 0, 0,  0,  0, 0, 0, 0, 0, "height:" },
	{ GADG_STAT_TEXT,		 20,104, 0, 0,  0,  0, 0, 0, 0, 0, "Pitch:" },
	{ GADG_STAT_TEXT,		 20,124, 0, 0,  0,  0, 0, 0, 0, 0, "Spacing:" },
	{ GADG_STAT_TEXT,		 20,164, 0, 0,  0,  0, 0, 0, 0, 0, "Options:" },
	{ GADG_STAT_TEXT,		 20,144, 0, 0,  0,  0, 0, 0, 0, 0, "Units:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,290,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate pageRequestT = {
	-1, -1, 410, 184, pageGadgets
};

#elif GERMAN

static GadgetTemplate pageGadgets[] = {
	{ GADG_PUSH_BUTTON,	-100, 10, 0, 0, 80, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-100, 40, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel },

	{ GADG_POPUP,		116, 40, 0, 0,130, 11, 0, 0, 0, 0, &paperList },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 291, 74, 0, 0, 48, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 399, 74, 0, 0, 48, 11, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON,	150,104, 0, 0,	 0,  0, 0, 0,'P',0, "Pica (10 Zeichen/Inch)" },
	{ GADG_RADIO_BUTTON,	150,119, 0, 0,	 0,  0, 0, 0,'E',0, "Elite (12 Zeichen/Inch)" },
	{ GADG_RADIO_BUTTON,	150,134, 0, 0,	 0,  0, 0, 0,'K',0, "Komprimiert (17 Zeichen/Inch)" },

	{ GADG_RADIO_BUTTON,	150,154, 0, 0,	 0,  0, 0, 0,'6',0, "6 Zeilen/Inch" },
	{ GADG_RADIO_BUTTON,	284,154, 0, 0,	 0,  0, 0, 0,'8',0, "8 Zeilen/Inch" },

	{ GADG_CHECK_BOX,		150,209, 0, 0,	 0,  0, 0, 0,'L',0, "Keine L�cken zwischen den Seiten" },

	{ GADG_STAT_TEXT,		230, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON,	150,174, 0, 0,	 0,  0, 0, 0,'I',0, "Inch" },
	{ GADG_RADIO_BUTTON, 284,174, 0, 0,	 0,  0, 0, 0,'Z',0, "Zentimeter" },

	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Druckseiteneinstellungen" },
	{ GADG_STAT_TEXT,		 20, 38, 0, 0,  0,  0, 0, 0, 0, 0, "Papier format" },
	{ GADG_STAT_TEXT,		230, 74, 0, 0,  0,  0, 0, 0, 0, 0, "Breite:" },
	{ GADG_STAT_TEXT,		354, 74, 0, 0,  0,  0, 0, 0, 0, 0, "H�he:" },
	{ GADG_STAT_TEXT,		 20,104, 0, 0,  0,  0, 0, 0, 0, 0, "Zeichenabstand:" },
	{ GADG_STAT_TEXT,		 20,154, 0, 0,  0,  0, 0, 0, 0, 0, "Zeilenabstand:" },
	{ GADG_STAT_TEXT,		 20,194, 0, 0,  0,  0, 0, 0, 0, 0, "Zus�tzliche Einstellungen:" },
	{ GADG_STAT_TEXT,		 20,174, 0, 0,  0,  0, 0, 0, 0, 0, "Ma�einheiten:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,290,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate pageRequestT = {
	-1, -1, 470, 224+8, pageGadgets
};

#elif FRENCH

static GadgetTemplate pageGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 40, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel },
	
	{ GADG_POPUP,		116, 40, 0, 0,130, 11, 0, 0, 0, 0, &paperList },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 216, 74, 0, 0, 48, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 338, 74, 0, 0, 48, 11, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON,	 90,104, 0, 0,  0,  0, 0, 0,'P',0, "Pica" },
	{ GADG_RADIO_BUTTON,	164,104, 0, 0,  0,  0, 0, 0,'i',0, "Elite" },
	{ GADG_RADIO_BUTTON,	238,104, 0, 0,  0,  0, 0, 0,'C',0, "Condens�e" },

	{ GADG_RADIO_BUTTON,	 90,124, 0, 0,  0,  0, 0, 0,'6',0, "6 LPI" },
	{ GADG_RADIO_BUTTON,	164,124, 0, 0,  0,  0, 0, 0,'8',0, "8 LPI" },

	{ GADG_CHECK_BOX,		 90,164, 0, 0,  0,  0, 0, 0,'d',0, "Pas d'espace entre les pages" },

	{ GADG_STAT_TEXT,		150, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON,	 90,144, 0, 0,  0,  0, 0, 0,'e',0, "Pouces" },
	{ GADG_RADIO_BUTTON, 164,144, 0, 0,  0,  0, 0, 0,'n',0, "Centim�tres" },

	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Format de page" },
	{ GADG_STAT_TEXT,		 20, 38, 0, 0,  0,  0, 0, 0, 0, 0, "Taille" },
	{ GADG_STAT_TEXT,		 28, 56, 0, 0,  0,  0, 0, 0, 0, 0, "page:" },
	{ GADG_STAT_TEXT,		164, 74, 0, 0,  0,  0, 0, 0, 0, 0, "larg.:" },
	{ GADG_STAT_TEXT,		278, 74, 0, 0,  0,  0, 0, 0, 0, 0, "haut.:" },
	{ GADG_STAT_TEXT,		 20,104, 0, 0,  0,  0, 0, 0, 0, 0, "Police:" },
	{ GADG_STAT_TEXT,		 20,124, 0, 0,  0,  0, 0, 0, 0, 0, "Espacmt:" },
	{ GADG_STAT_TEXT,		 20,164, 0, 0,  0,  0, 0, 0, 0, 0, "Options:" },
	{ GADG_STAT_TEXT,		 20,144, 0, 0,  0,  0, 0, 0, 0, 0, "Unit�s:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,290,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate pageRequestT = {
	-1, -1, 410, 184, pageGadgets
};

#elif SWEDISH
#endif

/*
	 Print requester
*/

#if (AMERICAN | BRITISH)

static GadgetTemplate printGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0, KEY_PRINT,0, strPrint },
	{ GADG_PUSH_BUTTON,	-80, 40, 0, 0, 60, 20, 0, 0, KEY_CANCEL,0, strCancel },

	{ GADG_RADIO_BUTTON,	122, 40, 0, 0,  0,  0, 0, 0,'N',0, "NLQ" },
	{ GADG_RADIO_BUTTON,	222, 40, 0, 0,  0,  0, 0, 0,'D',0, "Draft" },		

	{ GADG_RADIO_BUTTON,	122, 80, 0, 0,  0,  0, 0, 0,'A',0, "Automatic" },
	{ GADG_RADIO_BUTTON,	222, 80, 0, 0,  0,  0, 0, 0,'H',0, "Hand Feed" },

	{ GADG_EDIT_TEXT,		122, 60, 0, 0, 40, 11, 0, 0, 0, 0, NULL },

	GT_UPARROW(202,122, 0x1C),
	GT_DNARROW(202,122, 0x1D),
	GT_TEXT(202-8,122),

	{ GADG_RADIO_BUTTON,	122,100, 0, 0,  0,  0, 0, 0,'r',0, "Printer" },
	{ GADG_RADIO_BUTTON,	222,100, 0, 0,  0,  0, 0, 0,'F',0, "File" },
		
	{ GADG_STAT_TEXT,		120, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Print" },
	{ GADG_STAT_TEXT,		 20, 40, 0, 0,  0,  0, 0, 0, 0, 0, "Quality:" },
	{ GADG_STAT_TEXT,		 20, 60, 0, 0,  0,  0, 0, 0, 0, 0, "Copies:" },
	{ GADG_STAT_TEXT,		 20, 80, 0, 0,  0,  0, 0, 0, 0, 0, "Paper feed:" },
	{ GADG_STAT_TEXT,		 20,122, 0, 0,  0,  0, 0, 0, 0, 0, "Printer font number:" },
	{ GADG_STAT_TEXT,		 20,100, 0, 0,  0,  0, 0, 0, 0, 0, "Destination:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,290,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate printRequestT = {
	-1, -1, 410, 144, printGadgets
};

#elif GERMAN

static GadgetTemplate printGadgets[] = {
	{ GADG_PUSH_BUTTON, -100, 10, 0, 0, 80, 20, 0, 0,KEY_PRINT,0, strPrint },
	{ GADG_PUSH_BUTTON, -100, 40, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel },

	{ GADG_RADIO_BUTTON,	142, 40, 0, 0,  0,  0, 0, 0,'N',0, "NLQ" },
	{ GADG_RADIO_BUTTON, 196, 40, 0, 0,  0,  0, 0, 0,'S',0, "Schnellschrift" },

	{ GADG_RADIO_BUTTON, 157, 80, 0, 0,  0,  0, 0, 0,'u',0, "Automatisch" },
	{ GADG_RADIO_BUTTON, 282, 80, 0, 0,  0,  0, 0, 0,'H',0, "von Hand" },

	{ GADG_EDIT_TEXT,		172, 60, 0, 0, 40, 11, 0, 0, 0, 0, NULL },

	GT_UPARROW(237,122),
	GT_DOWNARROW(237,122),
	GT_TEXT(229,122),

	{ GADG_RADIO_BUTTON, 157,100, 0, 0,  0,  0, 0, 0,'r',0, "Drucker" },
	{ GADG_RADIO_BUTTON, 282,100, 0, 0,  0,  0, 0, 0,'t',0, "Datei" },

	{ GADG_STAT_TEXT,		120, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Drucker:" },
	{ GADG_STAT_TEXT,		 20, 40, 0, 0,  0,  0, 0, 0, 0, 0, "Druckqualit�t:" },
	{ GADG_STAT_TEXT,		 20, 60, 0, 0,  0,  0, 0, 0, 0, 0, "Anzahl der Kopien:" },
	{ GADG_STAT_TEXT,		 20, 80, 0, 0,  0,  0, 0, 0, 0, 0, "Papierzuf�hrung:" },
	{ GADG_STAT_TEXT,		 20,122, 0, 0,  0,  0, 0, 0, 0, 0, "Druckerzeichensatznummer:" },
	{ GADG_STAT_TEXT,		 20,100, 0, 0,  0,  0, 0, 0, 0, 0, "Ausgabe auf:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,290,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate printRequestT = {
	-1, -1, 430, 144, printGadgets
};

#elif FRENCH

static GadgetTemplate printGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_PRINT,0, strPrint },
	{ GADG_PUSH_BUTTON,	-80, 40, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel },

	{ GADG_RADIO_BUTTON,	122, 40, 0, 0,  0,  0, 0, 0,'N',0, "NLQ" },
	{ GADG_RADIO_BUTTON,	222, 40, 0, 0,  0,  0, 0, 0,'B',0, "Brouillon" },

	{ GADG_RADIO_BUTTON,	132, 80, 0, 0,  0,  0, 0, 0,'u',0, "Automatique" },
	{ GADG_RADIO_BUTTON,	242, 80, 0, 0,  0,  0, 0, 0,'M',0, "Manuelle" },

	{ GADG_EDIT_TEXT,		122, 60, 0, 0, 40, 11, 0, 0, 0, 0, NULL },

	GT_UPARROW(182,122),
	GT_DOWNARROW(182,122),
	GT_TEXT(174,122),

	{ GADG_RADIO_BUTTON,	132,100, 0, 0,  0,  0, 0, 0,'p',0, "Imprimante" },
	{ GADG_RADIO_BUTTON,	242,100, 0, 0,  0,  0, 0, 0,'F',0, "Fichier" },

	{ GADG_STAT_TEXT,		120, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Imprimante:" },
	{ GADG_STAT_TEXT,		 20, 40, 0, 0,  0,  0, 0, 0, 0, 0, "Qualit�:" },
	{ GADG_STAT_TEXT,		 20, 60, 0, 0,  0,  0, 0, 0, 0, 0, "Exemplaires:" },
	{ GADG_STAT_TEXT,		 20, 80, 0, 0,  0,  0, 0, 0, 0, 0, "Alimentation:" },
	{ GADG_STAT_TEXT,		 20,122, 0, 0,  0,  0, 0, 0, 0, 0, "Police imprimante:" },
	{ GADG_STAT_TEXT,		 20,100, 0, 0,  0,  0, 0, 0, 0, 0, "Destination:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0, 290, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate printRequestT = {
	-1, -1, 410, 144, printGadgets
};

#elif SWEDISH
#endif

/*
	Backup options
*/

#if (AMERICAN | BRITISH)

static GadgetTemplate backOptsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-90, 10, 0, 0, 70, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON,	-90, 40, 0, 0, 70, 20, 0, 0, KEY_CANCEL, 0, strCancel},
	{ GADG_PUSH_BUTTON,	-90, 70, 0, 0, 70, 20, 0, 0, 'M', 0, strOptions},

	{ GADG_RADIO_BUTTON,	 30, 80, 0, 0,  0,  0, 0, 0,'F',0, strFloppies },
	{ GADG_RADIO_BUTTON,	 30,100, 0, 0,  0,  0, 0, 0,'T',0, strTapes },
	{ GADG_RADIO_BUTTON,	 30,120, 0, 0,  0,  0, 0, 0,'A',0, strADOSFile },

	{ GADG_CHECK_BOX,		165, 80, 0, 0,  0,  0, 0, 0,'0',0, strDF0 },
	{ GADG_CHECK_BOX,		220, 80, 0, 0,  0,  0, 0, 0,'1',0, strDF1 },
	{ GADG_CHECK_BOX,		275, 80, 0, 0,  0,  0, 0, 0,'2',0, strDF2 },
	{ GADG_CHECK_BOX,		330, 80, 0, 0,  0,  0, 0, 0,'3',0, strDF3 },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,	215,100, 0, 0,128, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 413,100, 0, 0, 24, 12, 0, 0, 0, 0, NULL },

	GT_UPARROW(348, 100, 0),
	GT_DNARROW(348, 100, 0),

	GT_UPARROW(442, 100, 0x1C),
	GT_DNARROW(442, 100, 0x1D),

	{ GADG_EDIT_TEXT,		187,120, 0, 0,265, 12, 0, 0, 0, 0, NULL },

	{ GADG_PUSH_BUTTON,		165,117, 0, 0, 16, 18, 0, 0, '?', 0, strQuestionMark },

	{ GADG_STAT_TEXT,		184,100, 0, 0, 24, 12, 0, 0, 0, 0, strDevice },
	{ GADG_STAT_TEXT,		374,100, 0, 0, 32, 12, 0, 0, 0, 0, strUnit },	

	{ GADG_STAT_TEXT,		125, 60, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_CHECK_BOX,		 20,150, 0, 0,  0,  0, 0, 0,'D',0, "Display this requester before volume scanned" },

	{ GADG_RADIO_BUTTON,	125, 35, 0, 0,  0,  0, 0, 0, 'p', 0, "Complete" },
	{ GADG_RADIO_BUTTON,	225, 35, 0, 0,  0,  0, 0, 0, 'S', 0, "Selective" },

	{ GADG_STAT_STDBORDER,	 20, 25, 0, 0, 475-120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Backup Options" },
	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Backup type:" },
	{ GADG_STAT_TEXT,		 20, 60, 0, 0,  0,  0, 0, 0, 0, 0, "Destination:" },

	{ GADG_ITEM_NONE }
};

static RequestTemplate backOptsRequestT = {
	-1, -1, 480 - 4, 170, backOptsGadgets
};

#elif GERMAN

static GadgetTemplate backOptsGadgets[] = {
	{ GADG_PUSH_BUTTON, -100,  9, 0, 0, 80, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON, -100, 37, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel},
	{ GADG_PUSH_BUTTON, -100, 65, 0, 0, 80, 20, 0, 0,'E',0, strOptions},
		
	{ GADG_RADIO_BUTTON,	 30, 80, 0, 0,  0,  0, 0, 0,'D',0, strFloppies },
	{ GADG_RADIO_BUTTON,	 30,100, 0, 0,  0,  0, 0, 0,'B',0, strTapes },
	{ GADG_RADIO_BUTTON,	 30,120, 0, 0,  0,  0, 0, 0,'m',0, strADOSFile },
	{ GADG_CHECK_BOX,		200, 80, 0, 0,  0,  0, 0, 0,'0',0, strDF0 },
	{ GADG_CHECK_BOX,		252, 80, 0, 0,  0,  0, 0, 0,'1',0, strDF1 },
	{ GADG_CHECK_BOX,		304, 80, 0, 0,  0,  0, 0, 0,'2',0, strDF2 },
	{ GADG_CHECK_BOX,		356, 80, 0, 0,  0,  0, 0, 0,'3',0, strDF3 },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,	260,100, 0, 0, 128, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 474,100, 0, 0, 24, 12, 0, 0, 0, 0, NULL },

	GT_UPARROW(393,100, 0),
	GT_DNARROW(393,100, 0),

	GT_UPARROW(503,100, 0x1C),
	GT_DNARROW(503,100, 0x1D),
	
	{ GADG_EDIT_TEXT,		192,120, 0, 0,265, 12, 0, 0, 0, 0, NULL },

	{ GADG_PUSH_BUTTON,	170,117, 0, 0, 16, 18, 0, 0,'?', 0, strQuestionMark },

	{ GADG_STAT_TEXT,		215,100, 0, 0, 24, 12, 0, 0, 0, 0, strDevice },
	{ GADG_STAT_TEXT,		413,100, 0, 0, 32, 12, 0, 0, 0, 0, strUnit },

	{ GADG_STAT_TEXT,		 66, 61, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_CHECK_BOX,		 22,150, 0, 0,  0, 12, 0, 0,'R',0, "Diesen Requester vor Starten der Datensicherung anzeigen" },

	{ GADG_RADIO_BUTTON, 126, 35, 0, 0,  0,  0, 0, 0,'p',0, "Complete" },
	{ GADG_RADIO_BUTTON,	224, 35, 0, 0,  0,  0, 0, 0,'S',0, "Selective" },
		
	{ GADG_STAT_TEXT,		 20,  9, 0, 0,  0,  0, 0, 0, 0, 0, "Einstellungen: Daten sichern" },
	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Backup type:" },
	{ GADG_STAT_TEXT,		 20, 61, 0, 0,  0,  0, 0, 0, 0, 0, "Ziel:" },

	{ GADG_STAT_STDBORDER,20, 24, 0, 0,356,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate backOptsRequestT = {
	-1, -1, 530, 174, backOptsGadgets
};

#elif FRENCH

static GadgetTemplate backOptsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-84,  9, 0, 0, 64, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-84, 37, 0, 0, 64, 20, 0, 0,KEY_CANCEL,0, strCancel},
	{ GADG_PUSH_BUTTON,	-84, 65, 0, 0, 64, 20, 0, 0,'p',0, strOptions},
		
	{ GADG_RADIO_BUTTON,	 30, 80, 0, 0,  0,  0, 0, 0,'L',0, strFloppies },
	{ GADG_RADIO_BUTTON,	 30,100, 0, 0,  0,  0, 0, 0,'B',0, strTapes },
	{ GADG_RADIO_BUTTON,	 30,120, 0, 0,  0,  0, 0, 0,'F',0, strADOSFile },
	{ GADG_CHECK_BOX,		184, 80, 0, 0,  0,  0, 0, 0,'0',0, strDF0 },
	{ GADG_CHECK_BOX,		236, 80, 0, 0,  0,  0, 0, 0,'1',0, strDF1 },
	{ GADG_CHECK_BOX,		288, 80, 0, 0,  0,  0, 0, 0,'2',0, strDF2 },
	{ GADG_CHECK_BOX,		340, 80, 0, 0,  0,  0, 0, 0,'3',0, strDF3 },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,	215,100, 0, 0,128, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 413,100, 0, 0, 24, 12, 0, 0, 0, 0, NULL },
	
	GT_UPARROW(348,100, 0),
	GT_DNARROW(348,100, 0),
	
	GT_UPARROW(442,100,0x1C),
	GT_DNARROW(442,100,0x1D),

	{ GADG_EDIT_TEXT,		187,120, 0, 0,265, 12, 0, 0, 0, 0, NULL },
	
	{ GADG_PUSH_BUTTON,	165,117, 0, 0, 16, 18, 0, 0,'?', 0, strQuestionMark },
		
	{ GADG_STAT_TEXT,		184,100, 0, 0, 24, 12, 0, 0, 0, 0, strDevice },
	{ GADG_STAT_TEXT,		374,100, 0, 0, 32, 12, 0, 0, 0, 0, strUnit },

	{ GADG_STAT_TEXT,		128, 61, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_CHECK_BOX,		 22,150, 0, 0,  0,  0, 0, 0,'i',0, "Afficher cette fen�tre avant la sauvegarde" },

	{ GADG_RADIO_BUTTON, 126, 35, 0, 0,  0,  0, 0, 0,'p',0, "Complete" },
	{ GADG_RADIO_BUTTON,	224, 35, 0, 0,  0,  0, 0, 0,'S',0, "Selective" },
		
	{ GADG_STAT_TEXT,		 20,  9, 0, 0,  0,  0, 0, 0, 0, 0, "Options de sauvegarde" },
	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Backup type:" },
	{ GADG_STAT_TEXT,		 20, 61, 0, 0,  0,  0, 0, 0, 0, 0, "Destination:" },

	{ GADG_STAT_STDBORDER,20, 24, 0, 0,352,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate backOptsRequestT = {
	-1, -1, 476, 174, backOptsGadgets
};

#elif SWEDISH
#endif
	
/*
	Backup sub-options
*/

#if (AMERICAN | BRITISH)

static GadgetTemplate backOpts2Gadgets[] = {
	{ GADG_PUSH_BUTTON,  -80,  9, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,  -80, 37, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},
		
	{ GADG_RADIO_BUTTON,  30, 58, 0, 0,  0,  0, 0, 0,'N',0, "None" },
	{ GADG_RADIO_BUTTON,	 30, 76, 0, 0,  0,  0, 0, 0,'U',0, "Use:" },
	{ GADG_RADIO_BUTTON,  30, 94, 0, 0,  0,  0, 0, 0,'t',0, "Use device compression" },
	
	GT_UPARROW(84, 76,0x1C),
	GT_DNARROW(84, 76,0x1D),
	{ GADG_STAT_TEXT,		100, 76, 0, 0, 48, 12, 0, 0, 0, 0, NULL },
		
	{ GADG_CHECK_BOX,		 30,136, 0, 0,  0,  0, 0, 0,'P',0, "Password protection" },
	{ GADG_CHECK_BOX,		 30,154, 0, 0,  0,  0, 0, 0,'S',0, "Set archive flags" },
	{ GADG_CHECK_BOX,		 30,172, 0, 0,  0,  0, 0, 0,'V',0, "Verify data after write" },
	{ GADG_CHECK_BOX,		 30,190, 0, 0,  0,  0, 0, 0,'W',0, "Warn if destination is an AmigaDOS volume" },
	{ GADG_CHECK_BOX,		 30,208, 0, 0,  0,  0, 0, 0,'B',0, "Backup entire directory structure of volume" },

/*	{ GADG_EDIT_TEXT,		310, 76, 0, 0,144, 12, 0, 0, 0, 0, NULL },*/

	{ GADG_STAT_TEXT,		 20, 40, 0, 0,  0,  0, 0, 0, 0, 0, "Compression:" },
	{ GADG_STAT_TEXT,		 20,118, 0, 0,  0,  0, 0, 0, 0, 0, strSettings },
	
/*	{ GADG_STAT_TEXT,		166, 76, 0, 0,  0,  0, 0, 0, 0, 0, "Exclusion filter:" },*/
	{ GADG_STAT_TEXT,		 20, 	9, 0, 0,  0,  0, 0, 0, 0, 0, "More Backup Options" },
	{ GADG_STAT_STDBORDER,20, 24, 0, 0,352,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
};

static RequestTemplate backOpts2RequestT = {
	-1, -1, 476, 232, backOpts2Gadgets
};

#elif GERMAN

static GadgetTemplate backOpts2Gadgets[] = {
	{ GADG_PUSH_BUTTON, -100,  9, 0, 0, 80, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON, -100, 37, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel},
		
	{ GADG_RADIO_BUTTON,  30, 58, 0, 0,  0,  0, 0, 0,'N',0, "None" },
	{ GADG_RADIO_BUTTON,	 30, 76, 0, 0,  0,  0, 0, 0,'k',0, "Ausgabe komrimieren mit:" },
	{ GADG_RADIO_BUTTON,  30, 94, 0, 0,  0,  0, 0, 0,'U',0, "Use tape drive compression" },
	
	GT_UPARROW(200, 76,0x1C),
	GT_DNARROW(200, 76,0x1D),
	{ GADG_STAT_TEXT,		216, 76, 0, 0, 48, 12, 0, 0, 0, 0, NULL },
		
	{ GADG_CHECK_BOX,		 30,136, 0, 0,  0,  0, 0, 0,'S',0, "Kennwort-Schutz" },
	{ GADG_CHECK_BOX,		 30,154, 0, 0,  0,  0, 0, 0,'v',0, "Archivierungs-Bit setzen" },
	{ GADG_CHECK_BOX,		 30,172, 0, 0,  0,  0, 0, 0,'c',0, "Daten nach Schreiben �berpr�fen" },
	{ GADG_CHECK_BOX,		 30,190, 0, 0,  0,  0, 0, 0,'W',0, "Warnen, wenn das Sicherungsmedium ein AmigaDOS-Datentr�ger ist" },

	{ GADG_CHECK_BOX,		 30,208, 0, 0,  0,  0, 0, 0,'B',0, "Backup entire directory structure of volume" },
	{ GADG_STAT_TEXT,		 20, 40, 0, 0,  0,  0, 0, 0, 0, 0, "Compression:" },
	{ GADG_STAT_TEXT,		 20,118, 0, 0,  0,  0, 0, 0, 0, 0, strSettings },
	
	{ GADG_STAT_TEXT,		 20, 	9, 0, 0,  0,  0, 0, 0, 0, 0, strOptions },
	{ GADG_STAT_STDBORDER,20, 24, 0, 0,352,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
};

static RequestTemplate backOpts2RequestT = {
	-1, -1, 476, 232, backOpts2Gadgets
};

#elif FRENCH

static GadgetTemplate backOpts2Gadgets[] = {
	{ GADG_PUSH_BUTTON,	-80,  9, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 37, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},
		
	{ GADG_RADIO_BUTTON,  30, 58, 0, 0,  0,  0, 0, 0,'N',0, "None" },
	{ GADG_RADIO_BUTTON,	 30, 76, 0, 0,  0,  0, 0, 0,'C',0, "Compression en: },
	{ GADG_RADIO_BUTTON,  30, 94, 0, 0,  0,  0, 0, 0,'U',0, "Use tape drive compression" },
	
	GT_UPARROW(200, 76,0x1C),
	GT_DNARROW(200, 76,0x1D),
	{ GADG_STAT_TEXT,		216, 76, 0, 0, 48, 12, 0, 0, 0, 0, NULL },
		
	{ GADG_CHECK_BOX,		 30,136, 0, 0,  0,  0, 0, 0,'P',0, "Cl� de protection" },
	{ GADG_CHECK_BOX,		 30,154, 0, 0,  0,  0, 0, 0,'t',0, "Activer bit archive" },
	{ GADG_CHECK_BOX,		 30,172, 0, 0,  0,  0, 0, 0,'V',0, "V�rifier apr�s �criture" },
	{ GADG_CHECK_BOX,		 30,190, 0, 0,  0,  0, 0, 0,'e',0, "Avertir si la destination est un volume AmigaDOS" },

	{ GADG_CHECK_BOX,		 30,208, 0, 0,  0,  0, 0, 0,'B',0, "Backup entire directory structure of volume" },
	{ GADG_STAT_TEXT,		 20, 40, 0, 0,  0,  0, 0, 0, 0, 0, "Compression:" },
	{ GADG_STAT_TEXT,		 20,118, 0, 0,  0,  0, 0, 0, 0, 0, strSettings },
	
	{ GADG_STAT_TEXT,		 20, 	9, 0, 0,  0,  0, 0, 0, 0, 0, strOptions },
	{ GADG_STAT_STDBORDER,20, 24, 0, 0,352,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
};

static RequestTemplate backOpts2RequestT = {
	-1, -1, 476, 232, backOpts2Gadgets
};

#elif SWEDISH
#endif

/*
	Restore options
*/

#if (AMERICAN | BRITISH)

static GadgetTemplate restOptsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-90, 10, 0, 0, 70, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON,	-90, 40, 0, 0, 70, 20, 0, 0, KEY_CANCEL, 0, strCancel},
	{ GADG_PUSH_BUTTON,	-90, 70, 0, 0, 70, 20, 0, 0, 'M', 0, strOptions},

	{ GADG_RADIO_BUTTON,	 30, 80, 0, 0,  0,  0, 0, 0, 'F',0, strFloppies },
	{ GADG_RADIO_BUTTON,	 30,100, 0, 0,  0,  0, 0, 0, 'T',0, strTapes },
	{ GADG_RADIO_BUTTON,	 30,120, 0, 0,  0,  0, 0, 0, 'A',0, strADOSFile },

	{ GADG_CHECK_BOX,		165, 80, 0, 0,  0,  0, 0, 0, '0',0, strDF0 },
	{ GADG_CHECK_BOX,		220, 80, 0, 0,  0,  0, 0, 0, '1',0, strDF1 },
	{ GADG_CHECK_BOX,		275, 80, 0, 0,  0,  0, 0, 0, '2',0, strDF2 },
	{ GADG_CHECK_BOX,		330, 80, 0, 0,  0,  0, 0, 0, '3',0, strDF3 },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,	215,100, 0, 0,128, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 413,100, 0, 0, 24, 12, 0, 0, 0, 0, NULL },

	GT_UPARROW(348,100, 0),
	GT_DNARROW(348,100, 0),

	GT_UPARROW(442,100, 0x1C),
	GT_DNARROW(442,100, 0x1D),

	{ GADG_EDIT_TEXT,		187,120, 0, 0,265, 12, 0, 0, 0, 0, NULL },

	{ GADG_PUSH_BUTTON,		165,117, 0, 0, 16, 18, 0, 0,'?', 0, strQuestionMark },

	{ GADG_STAT_TEXT,		184,100, 0, 0, 24, 12, 0, 0, 0, 0, strDevice },
	{ GADG_STAT_TEXT,		374,100, 0, 0, 32, 12, 0, 0, 0, 0, strUnit },

	{ GADG_STAT_TEXT,		 85, 60, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_CHECK_BOX,		 20,150, 0, 0,  0,  0, 0, 0,'D',0, "Display this requester before catalog is read" },

	{ GADG_RADIO_BUTTON,	135, 35, 0, 0,  0,  0, 0, 0,'R',0, strRestore },
	{ GADG_RADIO_BUTTON,	305, 35, 0, 0,  0,  0, 0, 0,'s',0, strTest },
	{ GADG_RADIO_BUTTON,	220, 35, 0, 0,  0,  0, 0, 0,'p',0, strCompare },

	{ GADG_STAT_STDBORDER,	 20, 25, 0, 0,356,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Restore Options" },
	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Restore type:" },
	{ GADG_STAT_TEXT,		 20, 60, 0, 0,  0,  0, 0, 0, 0, 0, "Source:" },

	{ GADG_ITEM_NONE }
};

static RequestTemplate restOptsRequestT = {
	-1, -1, 480 - 4, 170, restOptsGadgets
};

#elif GERMAN

static GadgetTemplate restOptsGadgets[] = {
	{ GADG_PUSH_BUTTON, -100,  9, 0, 0, 80, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON, -100, 37, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel},
	{ GADG_PUSH_BUTTON, -100, 65, 0, 0, 80, 20, 0, 0,'E',0, strOptions},

	{ GADG_RADIO_BUTTON,	 30, 80, 0, 0,  0,  0, 0, 0,'D',0, strFloppies },
	{ GADG_RADIO_BUTTON,	 30,100, 0, 0,  0,  0, 0, 0,'B',0, strTapes },
	{ GADG_RADIO_BUTTON,	 30,120, 0, 0,  0,  0, 0, 0,'m',0, strADOSFile },
	{ GADG_CHECK_BOX,		200, 80, 0, 0,  0,  0, 0, 0,'0',0, strDF0 },
	{ GADG_CHECK_BOX,		252, 80, 0, 0,  0,  0, 0, 0,'1',0, strDF1 },
	{ GADG_CHECK_BOX,		304, 80, 0, 0,  0,  0, 0, 0,'2',0, strDF2 },
	{ GADG_CHECK_BOX,		356, 80, 0, 0,  0,  0, 0, 0,'3',0, strDF3 },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,	260,100, 0, 0, 128, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 474,100, 0, 0, 24, 12, 0, 0, 0, 0, NULL },

	GT_UPARROW(393,100, 0),
	GT_DNARROW(393,100, 0),

	GT_UPARROW(503,100, 0x1C),
	GT_DNARROW(503,100, 0x1D),
	
	{ GADG_EDIT_TEXT,		192,120, 0, 0,265, 12, 0, 0, 0, 0, NULL },

	{ GADG_PUSH_BUTTON,	170,117, 0, 0, 16, 18, 0, 0,'?', 0, strQuestionMark },

	{ GADG_STAT_TEXT,		215,100, 0, 0, 24, 12, 0, 0, 0, 0, strDevice },
	{ GADG_STAT_TEXT,		413,100, 0, 0, 32, 12, 0, 0, 0, 0, strUnit },

	{ GADG_STAT_TEXT,		 66, 61, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_CHECK_BOX,		 22,150, 0, 0,  0,  0, 0, 0,'R',0, "Diesen Requester vor Einlesen des Inhaltsverzeichnisses anzeigen" },
	
	{ GADG_RADIO_BUTTON, 132, 35, 0, 0,  0,  0, 0, 0,'W',0, strRestore },
	{ GADG_RADIO_BUTTON,	308, 35, 0, 0,  0,  0, 0, 0,'T',0, strTest },
	{ GADG_RADIO_BUTTON, 220, 35, 0, 0,  0,  0, 0, 0,'V',0, strCompare },
		
	{ GADG_STAT_TEXT,		 20,  9, 0, 0,  0,  0, 0, 0, 0, 0, "Einstellungen: Daten wiederherstellen" },
	{ GADG_STAT_TEXT,		 20, 61, 0, 0,  0,  0, 0, 0, 0, 0, "Quelle:" },

	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Restore type:" },
	{ GADG_STAT_STDBORDER,20, 24, 0, 0,356,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_ITEM_NONE }
};

static RequestTemplate restOptsRequestT = {
	-1, -1, 476, 174, restOptsGadgets
};

#elif FRENCH

static GadgetTemplate restOptsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-84,  9, 0, 0, 64, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-84, 37, 0, 0, 64, 20, 0, 0,KEY_CANCEL,0, strCancel},
	{ GADG_PUSH_BUTTON,	-84, 65, 0, 0, 64, 20, 0, 0,'M',0, strOptions},
		
	{ GADG_RADIO_BUTTON,	 30, 80, 0, 0,  0,  0, 0, 0,'L',0, strFloppies },
	{ GADG_RADIO_BUTTON,	 30,100, 0, 0,  0,  0, 0, 0,'B',0, strTapes },
	{ GADG_RADIO_BUTTON,	 30,120, 0, 0,  0,  0, 0, 0,'F',0, strADOSFile },
	{ GADG_CHECK_BOX,		184, 80, 0, 0,  0,  0, 0, 0,'0',0, strDF0 },
	{ GADG_CHECK_BOX,		236, 80, 0, 0,  0,  0, 0, 0,'1',0, strDF1 },
	{ GADG_CHECK_BOX,		288, 80, 0, 0,  0,  0, 0, 0,'2',0, strDF2 },
	{ GADG_CHECK_BOX,		340, 80, 0, 0,  0,  0, 0, 0,'3',0, strDF3 },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,	215,100, 0, 0,128, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 413,100, 0, 0, 24, 12, 0, 0, 0, 0, NULL },
	
	GT_UPARROW(348,100, 0),
	GT_DNARROW(348,100, 0),
	
	GT_UPARROW(442,100,0x1C),
	GT_DNARROW(442,100,0x1D),

	{ GADG_EDIT_TEXT,		187,120, 0, 0,265, 12, 0, 0, 0, 0, NULL },
	
	{ GADG_PUSH_BUTTON,	165,117, 0, 0, 16, 18, 0, 0,'?', 0, strQuestionMark },
		
	{ GADG_STAT_TEXT,		184,100, 0, 0, 24, 12, 0, 0, 0, 0, strDevice },
	{ GADG_STAT_TEXT,		374,100, 0, 0, 32, 12, 0, 0, 0, 0, strUnit },

	{ GADG_STAT_TEXT,		 90, 61, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_CHECK_BOX,		 22,150, 0, 0,  0,  0, 0, 0,'f',0, "Afficher cette fen�tre avant de lire le catalogue" },
	
	{ GADG_RADIO_BUTTON, 132, 35, 0, 0,  0,  0, 0, 0,'R',0, strRestore },
	{ GADG_RADIO_BUTTON,	308, 35, 0, 0,  0,  0, 0, 0,'V',0, strTest },
	{ GADG_RADIO_BUTTON, 220, 35, 0, 0,  0,  0, 0, 0,'C',0, strCompare },
		
	{ GADG_STAT_TEXT,		 20,  9, 0, 0,  0,  0, 0, 0, 0, 0, "Options de restauration" },
	{ GADG_STAT_TEXT,		 20, 61, 0, 0,  0,  0, 0, 0, 0, 0, "Source:" },

	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Restore type:" },
	{ GADG_STAT_STDBORDER,20, 24, 0, 0,356,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_ITEM_NONE }
};

static RequestTemplate restOptsRequestT = {
	-1, -1, 476, 174, restOptsGadgets
};

#elif SWEDISH
#endif

/*
 * Restore sub-options
 */
 
static TextPtr	dupActList[] = { strYes, strNo, strPrompt, NULL };

#if (AMERICAN | BRITISH)

static GadgetTemplate restOpts2Gadgets[] = {
	{ GADG_PUSH_BUTTON,	-80,  9, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 37, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},
	
	{ GADG_POPUP,		240, 53, 0, 0, 76, 11, 0, 0, 0, 0, &dupActList },
	{ GADG_POPUP,		240, 72, 0, 0, 76, 11, 0, 0, 0, 0, &dupActList },

	{ GADG_RADIO_BUTTON,	182,105, 0, 0,  0,  0, 0, 0,'i',0, "Original" },
	{ GADG_RADIO_BUTTON, 376,105, 0, 0,  0,  0, 0, 0,'B',0, "Backup" },
	{ GADG_RADIO_BUTTON, 280,105, 0, 0,  0,  0, 0, 0,'u',0, "Current" },
		
	{ GADG_CHECK_BOX,		 30,123, 0, 0,  0,  0, 0, 0,'R',0, "Restore empty drawers" },
	{ GADG_CHECK_BOX,		 30,141, 0, 0,  0,  0, 0, 0,'s',0, "Restore archive flags" },
	{ GADG_CHECK_BOX,		 30,159, 0, 0,  0,  0, 0, 0,'K',0, "Keep directory structure of restored data"  },
	
	{ GADG_STAT_TEXT,		 20, 34, 0, 0,  0,  0, 0, 0, 0, 0, "Restore existing files:" },
	{ GADG_STAT_TEXT,		 30, 53, 0, 0,  0,  0, 0, 0, 0, 0, "Replace earlier versions:" },
	{ GADG_STAT_TEXT,		 30, 72, 0, 0,  0,  0, 0, 0, 0, 0, "Replace later versions:" },
	{ GADG_STAT_TEXT,		 30,105, 0, 0,  0,  0, 0, 0, 0, 0, "Set file dates to:" },
	
	{ GADG_STAT_TEXT,		 20, 	9, 0, 0,  0,  0, 0, 0, 0, 0, "More Restore Options" },
	{ GADG_STAT_STDBORDER,20, 24, 0, 0,352,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
};

static RequestTemplate restOpts2RequestT = {
	-1, -1, 476, 179, restOpts2Gadgets
};

#elif GERMAN

static GadgetTemplate restOpts2Gadgets[] = {
	{ GADG_PUSH_BUTTON, -100,  9, 0, 0, 80, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON, -100, 37, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel},
	
	{ GADG_POPUP,		240, 53, 0, 0, 76, 11, 0, 0, 0, 0, &dupActList },
	{ GADG_POPUP,		240, 72, 0, 0, 76, 11, 0, 0, 0, 0, &dupActList },

	{ GADG_RADIO_BUTTON,	182,105, 0, 0,  0,  0, 0, 0,'i',0, "Original" },
	{ GADG_RADIO_BUTTON, 376,105, 0, 0,  0,  0, 0, 0,'s',0, "Datensicherung" },
	{ GADG_RADIO_BUTTON, 280,105, 0, 0,  0,  0, 0, 0,'z',0, "Jetzt" },
		
	{ GADG_CHECK_BOX,		 30,123, 0, 0,  0,  0, 0, 0,'w',0, "leere Verzeichnisse wiederh." },
	{ GADG_CHECK_BOX,		 30,141, 0, 0,  0,  0, 0, 0,'c',0, "Archivierungs-Bit wiederh." },
	{ GADG_CHECK_BOX,		 30,159, 0, 0,  0,  0, 0, 0,'h',0, "Verzeichnisstrucktur der wiederhergestellten Daten beibehalten" },
	
	{ GADG_STAT_TEXT,		 20, 34, 0, 0,  0,  0, 0, 0, 0, 0, "Restore existing files:" },
	{ GADG_STAT_TEXT,		 30, 53, 0, 0,  0,  0, 0, 0, 0, 0, "Replace earlier versions:" },
	{ GADG_STAT_TEXT,		 30, 72, 0, 0,  0,  0, 0, 0, 0, 0, "Replace later versions:" },
	{ GADG_STAT_TEXT,		 30,105, 0, 0,  0,  0, 0, 0, 0, 0, "Datum der Datei setzen auf:" },
	
	{ GADG_STAT_TEXT,		 20, 	9, 0, 0,  0,  0, 0, 0, 0, 0, strOptions },
	{ GADG_STAT_STDBORDER,20, 24, 0, 0,352,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
};

static RequestTemplate restOpts2RequestT = {
	-1, -1, 476, 179, restOpts2Gadgets
};

#elif FRENCH

static GadgetTemplate restOpts2Gadgets[] = {
	{ GADG_PUSH_BUTTON,	-80,  9, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 37, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},
	
	{ GADG_POPUP,		240, 53, 0, 0, 76, 11, 0, 0, 0, 0, &dupActList },
	{ GADG_POPUP,		240, 72, 0, 0, 76, 11, 0, 0, 0, 0, &dupActList },

	{ GADG_RADIO_BUTTON,	182,105, 0, 0,  0,  0, 0, 0,'i',0, "Origine" },
	{ GADG_RADIO_BUTTON, 376,105, 0, 0,  0,  0, 0, 0,'S',0, "Sauv�e" },
	{ GADG_RADIO_BUTTON, 280,105, 0, 0,  0,  0, 0, 0,'t',0, "Courante" },
		
	{ GADG_CHECK_BOX,		 30,123, 0, 0,  0,  0, 0, 0,'e',0, "Rest. tiroirs vides" },
	{ GADG_CHECK_BOX,		 30,141, 0, 0,  0,  0, 0, 0,'h',0, "Restaurer bit archive" },
	{ GADG_CHECK_BOX,		 30,159, 0, 0,  0,  0, 0, 0,'g',0, "Conserver l'arborescence originale des donn�es" },
	
	{ GADG_STAT_TEXT,		 20, 34, 0, 0,  0,  0, 0, 0, 0, 0, "Restore existing files:" },
	{ GADG_STAT_TEXT,		 30, 53, 0, 0,  0,  0, 0, 0, 0, 0, "Replace earlier versions:" },
	{ GADG_STAT_TEXT,		 30, 72, 0, 0,  0,  0, 0, 0, 0, 0, "Replace later versions:" },
	{ GADG_STAT_TEXT,		 30,105, 0, 0,  0,  0, 0, 0, 0, 0, "Utiliser date: },
	
	{ GADG_STAT_TEXT,		 20, 	9, 0, 0,  0,  0, 0, 0, 0, 0, strOptions },
	{ GADG_STAT_STDBORDER,20, 24, 0, 0,352,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
};

static RequestTemplate restOpts2RequestT = {
	-1, -1, 476, 179, restOpts2Gadgets
};

#elif SWEDISH
#endif

/*
	Catalog options
*/

#if (AMERICAN | BRITISH)

static GadgetTemplate catOptsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 38, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_CHECK_BOX,		 30, 50, 0, 0,  0,  0, 0, 0,'F',0, "File size" },
	{ GADG_CHECK_BOX,		 30, 65, 0, 0,  0,  0, 0, 0,'P',0, "Protection flags" },
	{ GADG_CHECK_BOX,		 30, 80, 0, 0,  0,  0, 0, 0,'D',0, "Date modified" },
	{ GADG_CHECK_BOX,		 30, 95, 0, 0,  0,  0, 0, 0,'T',0, "Time modified" },

	{ GADG_CHECK_BOX,		 30,132, 0, 0,  0,  0, 0, 0,'S',0, "Show seconds" },
	{ GADG_CHECK_BOX,		 30,147, 0, 0,  0,  0, 0, 0,'h',0, "Show AM/PM" },
	{ GADG_RADIO_BUTTON,	220,132, 0, 0,160, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON,	220,147, 0, 0,160, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON,	220,162, 0, 0,160, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON,	220,177, 0, 0,160, 12, 0, 0, 0, 0, NULL },
	
	{ GADG_RADIO_BUTTON, 220, 50, 0, 0,  0,  0, 0, 0,'N',0, "Name" },
	{ GADG_RADIO_BUTTON, 220, 65, 0, 0,  0,  0, 0, 0,'i',0, "Size" },
	{ GADG_RADIO_BUTTON, 220, 80, 0, 0,  0,  0, 0, 0,'a',0, "Date" },
	{ GADG_CHECK_BOX,		220, 95, 0, 0,  0,  0, 0, 0,'G',0, "Group drawers first" },
		
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Catalog Options" },
	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Show file information:" },
	{ GADG_STAT_TEXT,		 20,117, 0, 0,  0,  0, 0, 0, 0, 0, "Time format:" },
	{ GADG_STAT_TEXT,		210,117, 0, 0,  0,  0, 0, 0, 0, 0, "Date format:" },
	{ GADG_STAT_TEXT,		210, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Sort by:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,290,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate catOptsRequestT = {
	-1, -1, 410, 200, catOptsGadgets
};

#elif GERMAN

static GadgetTemplate catOptsGadgets[] = {
	{ GADG_PUSH_BUTTON, -100, 10, 0, 0, 80, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON, -100, 38, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_CHECK_BOX,		 30, 65, 0, 0,  0, 12, 0, 0,'D',0, "Dateigr��e" },
	{ GADG_CHECK_BOX,		 30, 80, 0, 0,  0, 12, 0, 0,'S',0, "Schutzbits" },
	{ GADG_CHECK_BOX,		 30, 95, 0, 0,  0, 12, 0, 0,'g',0, "Datum ge�ndert" },
	{ GADG_CHECK_BOX,		 30,110, 0, 0,  0, 12, 0, 0,'Z',0, "Zeit ge�ndert" },

	{ GADG_CHECK_BOX,		 30,147, 0, 0,  0, 12, 0, 0,'k',0, "Sekunden anzeigen" },
	{ GADG_STAT_TEXT,		 30,162, 0, 0,  0, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON,	220,147, 0, 0, 88, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON,	320,147, 0, 0,160, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON,	220,162, 0, 0,160, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON,	220,177, 0, 0,160, 12, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON, 220, 50, 0, 0,  0, 12, 0, 0,'N',0, "Namen" },
	{ GADG_RADIO_BUTTON, 220, 65, 0, 0,  0, 12, 0, 0,'r',0, "Gr��e" },
	{ GADG_RADIO_BUTTON, 220, 80, 0, 0,  0, 12, 0, 0,'t',0, "Datum" },
	{ GADG_CHECK_BOX,		220, 95, 0, 0,  0, 12, 0, 0,'V',0, "Verzeichnisse zuerst\nanzeigen" },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Einstellungen: Inhaltsverzeichnis" },
	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Anzeige von\nDateiinformationen:" },
	{ GADG_STAT_TEXT,		 20,132, 0, 0,  0,  0, 0, 0, 0, 0, "Zeitformat:" },
	{ GADG_STAT_TEXT,		210,132, 0, 0,  0,  0, 0, 0, 0, 0, "Datumsformat:" },
	{ GADG_STAT_TEXT,		210, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Sortieren nach:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,290,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate catOptsRequestT = {
	-1, -1, 435, 200, catOptsGadgets
};

#elif FRENCH

static GadgetTemplate catOptsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 38, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_CHECK_BOX,		 30, 50, 0, 0,  0, 12, 0, 0,'T',0, "Taille fichier" },
	{ GADG_CHECK_BOX,		 30, 65, 0, 0,  0, 12, 0, 0,'B',0, "Bits de protection" },
	{ GADG_CHECK_BOX,		 30, 80, 0, 0,  0, 12, 0, 0,'D',0, "Date modification" },
	{ GADG_CHECK_BOX,		 30, 95, 0, 0,  0, 12, 0, 0,'m',0, "Heure modification" },

	{ GADG_CHECK_BOX,		 30,132, 0, 0,  0, 12, 0, 0,'s',0, "Afficher secondes" },
	{ GADG_CHECK_BOX,		 30,147, 0, 0,  0, 12, 0, 0,'h',0, "Heures AM/PM" },
	{ GADG_RADIO_BUTTON,	220,132, 0, 0,160, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON,	220,147, 0, 0,160, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON,	220,162, 0, 0,160, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON,	220,177, 0, 0,160, 12, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON, 220, 50, 0, 0,  0, 12, 0, 0,'N',0, "Noms" },
	{ GADG_RADIO_BUTTON, 220, 65, 0, 0,  0, 12, 0, 0,'i',0, "Tailles" },
	{ GADG_RADIO_BUTTON, 220, 80, 0, 0,  0, 12, 0, 0,'e',0, "Dates" },
	{ GADG_CHECK_BOX,		220, 95, 0, 0,  0, 12, 0, 0,'p',0, "Tiroirs en premier" },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Options du catalogue" },
	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Informations fichier:" },
	{ GADG_STAT_TEXT,		 20,117, 0, 0,  0,  0, 0, 0, 0, 0, "Format heure:" },
	{ GADG_STAT_TEXT,		210,117, 0, 0,  0,  0, 0, 0, 0, 0, "Format date:" },
	{ GADG_STAT_TEXT,		210, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Classer par:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,290,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate catOptsRequestT = {
	-1, -1, 410, 200, catOptsGadgets/*, NULL*/
};

#elif SWEDISH
#endif


/*
	Logfile options
*/

#if (AMERICAN | BRITISH)

static GadgetTemplate logOptsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 38, 0, 0, 60, 20, 0, 0,KEY_CANCEL, 0, strCancel },
	
	{ GADG_RADIO_BUTTON,  30, 50, 0, 0,  0,  0, 0, 0,'E', 0, "Errors only" },
	{ GADG_RADIO_BUTTON,  30, 68, 0, 0,  0,  0, 0, 0,'D', 0, "Errors, drawer names only" },
	{ GADG_RADIO_BUTTON,  30, 86, 0, 0,  0,  0, 0, 0,'F', 0, "Errors, drawer names and file names" },
	
	{ GADG_CHECK_BOX,		 30,108, 0, 0,  0,  0, 0, 0,'R', 0, "Rate in MB/min" },
	{ GADG_CHECK_BOX,		176,108, 0, 0,  0,  0, 0, 0,'m', 0, "Compression efficiency" },
	{ GADG_RADIO_BUTTON,  30,166, 0, 0,  0,  0, 0, 0,'N', 0, "No session log files" },
	{ GADG_RADIO_BUTTON,  30,184, 0, 0,  0,  0, 0, 0,'S', 0, "Save as default time-stamped file name" },
	{ GADG_RADIO_BUTTON,  30,202, 0, 0,  0,  0, 0, 0,'a', 0, "Save as:" },
	{ GADG_PUSH_BUTTON,	112,199, 0, 0, 16, 18, 0, 0,'?', 0, strQuestionMark },
	{ GADG_EDIT_TEXT,		134,202, 0, 0,232, 12, 0, 0, 0,  0, NULL },
	{ GADG_CHECK_BOX,		 30,125, 0, 0,  0,  0, 0, 0,'w', 0, "No display while operation in progress" },
		
	{ GADG_STAT_TEXT,		 20,151, 0, 0,  0,  0, 0, 0, 0, 0, "Session file maintenance:" },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Session Log Options" },
	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Information to output:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,270,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate logOptsRequestT = {
	-1, -1, 390, 228, logOptsGadgets
};

#elif GERMAN

static GadgetTemplate logOptsGadgets[] = {
	{ GADG_PUSH_BUTTON, -100, 10, 0, 0, 80, 20, 0, 0,KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -100, 38, 0, 0, 80, 20, 0, 0,KEY_CANCEL, 0, strCancel },

	{ GADG_RADIO_BUTTON,  30, 50, 0, 0,  0, 12, 0, 0,'N', 0, "Nur Fehler" },
	{ GADG_RADIO_BUTTON,  30, 68, 0, 0,  0, 12, 0, 0,'F', 0, "Fehler, nur Verzeichnisnamen" },
	{ GADG_RADIO_BUTTON,  30, 86, 0, 0,  0, 12, 0, 0,'V', 0, "Fehler, Verzeichnis- und Dateinnamen" },

	{ GADG_CHECK_BOX,		 30,108, 0, 0,  0, 12, 0, 0,'M', 0, "�bertragungsrate in MB/min" },
	{ GADG_CHECK_BOX,		270,108, 0, 0,  0, 12, 0, 0,'K', 0, "Komprimierungsrate" },
	{ GADG_RADIO_BUTTON,  30,166, 0, 0,  0, 12, 0, 0,'S', 0, "Kein Abspeichern des Sitzungsprotokolls" },
	{ GADG_RADIO_BUTTON,  30,184, 0, 0,  0, 12, 0, 0,'b', 0, "Abspeichern unter voreingestellten Namen mit Zeitstempel" },
	{ GADG_RADIO_BUTTON,  30,202, 0, 0,  0, 12, 0, 0,'p', 0, "Speichern als:" },
	{ GADG_PUSH_BUTTON,	162,199, 0, 0, 16, 18, 0, 0,'?', 0, strQuestionMark },
	{ GADG_EDIT_TEXT,		184,202, 0, 0,232, 12, 0, 0, 0,  0, NULL },
	{ GADG_CHECK_BOX,		 30,125, 0, 0,  0, 12, 0, 0,'w', 0, "Keine Anzeige w�hrend des Vorgangs" },

	{ GADG_STAT_TEXT,		 20,151, 0, 0,  0,  0, 0, 0, 0, 0, "Abspeichern des Sitzungsprotokolls:" },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Einstellungen: Sitzungsprotokoll" },
	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Ausgegebene Information:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,270,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate logOptsRequestT = {
	-1, -1, 500, 228, logOptsGadgets
};

#elif FRENCH

static GadgetTemplate logOptsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 38, 0, 0, 60, 20, 0, 0,KEY_CANCEL, 0, strCancel },

	{ GADG_RADIO_BUTTON,  30, 50, 0, 0,  0, 12, 0, 0,'e', 0, "Seulement erreurs" },
	{ GADG_RADIO_BUTTON,  30, 68, 0, 0,  0, 12, 0, 0,'t', 0, "Seulement erreurs et noms des tiroirs" },
	{ GADG_RADIO_BUTTON,  30, 86, 0, 0,  0, 12, 0, 0,'f', 0, "Erreurs, noms des tiroirs et des fichiers" },

	{ GADG_CHECK_BOX,		 30,108, 0, 0,  0, 12, 0, 0,'V', 0, "Vitesse en Mo/mn" },
	{ GADG_CHECK_BOX,		196,108, 0, 0,  0, 12, 0, 0,'c', 0, "Taux de compression" },
	{ GADG_RADIO_BUTTON,  30,166, 0, 0,  0, 12, 0, 0,'P', 0, "Pas de fichier rapport" },
	{ GADG_RADIO_BUTTON,  30,184, 0, 0,  0, 12, 0, 0,'S', 0, "Sauver en utilisant l'heure comme nom" },
	{ GADG_RADIO_BUTTON,  30,202, 0, 0,  0, 12, 0, 0,'u', 0, "Sauver:" },
	{ GADG_PUSH_BUTTON,	104,199, 0, 0, 16, 18, 0, 0,'?', 0, strQuestionMark },
	{ GADG_EDIT_TEXT,		126,202, 0, 0,232, 12, 0, 0, 0,  0, NULL },
	{ GADG_CHECK_BOX,		 30,125, 0, 0,  0, 12, 0, 0,'d', 0, "Pas d'affichage pendant l'op�ration" },

	{ GADG_STAT_TEXT,		 20,151, 0, 0,  0,  0, 0, 0, 0, 0, "Gestion fichier rapport:" },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Options du rapport" },
	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Informations � afficher:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,270,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate logOptsRequestT = {
	-1, -1, 390, 228, logOptsGadgets
};

#elif SWEDISH
#endif

/*
	Preferences
*/

#if (AMERICAN | BRITISH)

static GadgetTemplate prefsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 38, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_CHECK_BOX,     30, 50, 0, 0,  0,  0, 0, 0,'F',0, strFlash },
	{ GADG_CHECK_BOX,		 30, 67, 0, 0,  0,  0, 0, 0,'B',0, strBeep },

	{ GADG_CHECK_BOX,		220, 50, 0, 0,  0,  0, 0, 0,'l',0, strFlash },
	{ GADG_CHECK_BOX,		220, 67, 0, 0,  0,  0, 0, 0,'e',0, strBeep },
		
	{ GADG_CHECK_BOX,		 30,102, 0, 0,  0,  0, 0, 0,'S',0, "Save icons with Quarterback files" },
	{ GADG_CHECK_BOX,		 30,119, 0, 0,  0,  0, 0, 0,'W',0, "Warn on invalid filenames" },
	{ GADG_CHECK_BOX,		 30,136, 0, 0,  0,  0, 0, 0,'I',0, "Include assigned directories in list of volumes" },
	 
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Preferences" },
	{ GADG_STAT_TEXT,     20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Error notification:" },
	{ GADG_STAT_TEXT,		210, 35, 0, 0,  0,  0, 0, 0, 0, 0, "On disk completion:" },
	{ GADG_STAT_TEXT,		 20, 87, 0, 0,  0,  0, 0, 0, 0, 0, "General:" },

	{ GADG_STAT_STDBORDER,20, 25, 0, 0,330,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate prefsRequestT = {
	-1, -1, 450, 156, prefsGadgets
};

#elif GERMAN

static GadgetTemplate prefsGadgets[] = {
	{ GADG_PUSH_BUTTON, -100, 10, 0, 0, 80, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON, -100, 38, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel},
							
							
	{ GADG_CHECK_BOX,		 30, 50, 0, 0,  0,  0, 0, 0,'n',0, strFlash },
	{ GADG_CHECK_BOX,		 30, 67, 0, 0,  0,  0, 0, 0,'T',0, strBeep },

	{ GADG_CHECK_BOX,		220, 50, 0, 0,  0,  0, 0, 0,'e',0, strFlash },
	{ GADG_CHECK_BOX,		220, 67, 0, 0,  0,  0, 0, 0,'u',0, strBeep },

	{ GADG_CHECK_BOX,		 30,102, 0, 0,  0,  0, 0, 0,'Q',0, "Quarterback-Dateien mit Piktogramm abspeichern" },
	{ GADG_CHECK_BOX,		 30,119, 0, 0,  0,  0, 0, 0,'B',0, "Bei ung�ltigen Dateinamen eine Warnung ausgeben" },
	{ GADG_CHECK_BOX,		 30,136, 0, 0,  0,  0, 0, 0,'Z',0, "Zugewiesene Verzeichnisse in der Liste der\nDatentr�ger anzeigen" },
	 
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Allgemeine Einstellungen" },
	{ GADG_STAT_TEXT,     20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Notification Method:" },
	{ GADG_STAT_TEXT,		210, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Disk Change:" },
	{ GADG_STAT_TEXT,		 20, 87, 0, 0,  0,  0, 0, 0, 0, 0, "Verschiedenes:" },

	{ GADG_STAT_STDBORDER,20, 25, 0, 0,330,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate prefsRequestT = {
	-1, -1, 470, 168, prefsGadgets
};

#elif FRENCH

static GadgetTemplate prefsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 38, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_CHECK_BOX,     30, 50, 0, 0,  0,  0, 0, 0,'F',0, strFlash },
	{ GADG_CHECK_BOX,		 30, 67, 0, 0,  0,  0, 0, 0,'S',0, strBeep },

	{ GADG_CHECK_BOX,		220, 50, 0, 0,  0,  0, 0, 0,'l',0, strFlash },
	{ GADG_CHECK_BOX,		220, 67, 0, 0,  0,  0, 0, 0,'g',0, strBeep },
		
	{ GADG_CHECK_BOX,		 30,102, 0, 0,  0,  0, 0, 0,'i',0, "Save icons with Quarterback files" },
	{ GADG_CHECK_BOX,		 30,119, 0, 0,  0,  0, 0, 0,'v',0, "Warn on invalid filenames" },
	{ GADG_CHECK_BOX,		 30,136, 0, 0,  0,  0, 0, 0,'n',0, "Include assigned directories in list of volumes" },
	 
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Pr�f�rences" },
	{ GADG_STAT_TEXT,     20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Type d'avertissement:" },
	{ GADG_STAT_TEXT,		210, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Disk Change:" },
	{ GADG_STAT_TEXT,		 20, 87, 0, 0,  0,  0, 0, 0, 0, 0, "G�n�rales:" },

	{ GADG_STAT_STDBORDER,20, 25, 0, 0,330,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate prefsRequestT = {
	-1, -1, 450, 156, prefsGadgets
};

#elif SWEDISH
#endif

/*
	Tape options requester
*/

#if (AMERICAN | BRITISH | FRENCH)

static GadgetTemplate tapeOptsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 38, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},

#if (AMERICAN | BRITISH)
	{ GADG_CHECK_BOX,		 20, 40, 0, 0,  0,  0, 0, 0,'A',0, "Auto-retension inserted tapes" },
	{ GADG_CHECK_BOX,		 20, 60, 0, 0,  0,  0, 0, 0,'R',0, "Rewind after backup or restore" },
#elif FRENCH
	{ GADG_CHECK_BOX,		 20, 40, 0, 0,  0,  0, 0, 0,'i',0, "Retendre les bandes ins�r�es" },
	{ GADG_CHECK_BOX,		 20, 60, 0, 0,  0,  0, 0, 0,'R',0, "Rembobiner apr�s utilisation" },
#endif
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Tape Options" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,248,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate tapeOptsRequestT = {
	-1, -1, 368, 84, tapeOptsGadgets
};

#elif GERMAN

static GadgetTemplate tapeOptsGadgets[] = {
	{ GADG_PUSH_BUTTON, -100, 10, 0, 0, 80, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON, -100, 38, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_CHECK_BOX,		 20, 40, 0, 0,  0,  0, 0, 0,'B',0, "Eingelegte B�nder automatisch straffen" },
	{ GADG_CHECK_BOX,		 20, 60, 0, 0,  0,  0, 0, 0,'N',0, "Nach sicherung oder wiederherstellung\nautomatisch zur�ckspulen" },

	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Tape Options" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,248,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate tapeOptsRequestT = {
	-1, -1, 455, 84, tapeOptsGadgets
};

#elif SWEDISH
#endif

/*
	SCSI info requester
*/

#if (AMERICAN | BRITISH | FRENCH)
#define SCSIBORDER_WIDTH	135
#elif GERMAN
#define SCSIBORDER_WIDTH	183
#elif SWEDISH
#endif

static GadgetTemplate scsiInfoGadgets[] = {
#if (AMERICAN | BRITISH)
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "SCSI Interrogator" },
	{ GADG_STAT_TEXT,		 20, 36, 0, 0, 24, 12, 0, 0, 0, 0, strDevice },
	{ GADG_STAT_TEXT,		210, 36, 0, 0, 32, 12, 0, 0, 0, 0, strUnit },
#elif GERMAN
	{ GADG_PUSH_BUTTON, -100, 10, 0, 0, 80, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "SCSI-Bus abfragen" },
	{ GADG_STAT_TEXT,		 20, 36, 0, 0, 24, 12, 0, 0, 0, 0, strDevice },
	{ GADG_STAT_TEXT,		225, 36, 0, 0, 24, 12, 0, 0, 0, 0 ,strUnit },
#elif FRENCH
	{ GADG_PUSH_BUTTON, 	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Infos sur unit�s SCSI" },
	{ GADG_STAT_TEXT,		 20, 36, 0, 0, 24, 12, 0, 0, 0, 0, strDevice },
	{ GADG_STAT_TEXT,		210, 36, 0, 0, 32, 12, 0, 0, 0, 0, strUnit },
#elif SWEDISH
#endif
	{ GADG_STAT_TEXT,	123, 80, 0, 3,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	123, 99, 0, 3,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 	123,118, 0, 3,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	123, 61, 0, 3,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	123,137, 0, 3,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	123,156, 0, 3,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,	51, 36, 0, 0,128, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 249, 36, 0, 0, 24, 12, 0, 0, 0, 0, NULL },
	
	GT_UPARROW(184,36, 0),
	GT_DNARROW(184,36, 0),
	
	GT_UPARROW(278,36, 0x1C),
	GT_DNARROW(278,36, 0x1D),
	
	{ GADG_STAT_TEXT,		 20, 61, 0, 3,  0,  0, 0, 0, 0, 0, strTpDrType },	
	{ GADG_STAT_TEXT,		 20, 80, 0, 3,  0,  0, 0, 0, 0, 0, strTpDrVendor },
	{ GADG_STAT_TEXT,		 20, 99, 0, 3,  0,  0, 0, 0, 0, 0, strTpDrModel },
	{ GADG_STAT_TEXT,		 20,118, 0, 3,  0,  0, 0, 0, 0, 0, strTpDrRev },
	{ GADG_STAT_TEXT,		 20,137, 0, 3,  0,  0, 0, 0, 0, 0, strTpDrBlkSz },
	{ GADG_STAT_TEXT,		 20,156, 0, 3,  0,  0, 0, 0, 0, 0, strTpDrMedSz },

	{ GADG_STAT_STDBORDER,120, 61, 0, 0,SCSIBORDER_WIDTH, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,120, 80, 0, 0,SCSIBORDER_WIDTH, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,120, 99, 0, 0,SCSIBORDER_WIDTH, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,120,118, 0, 0,SCSIBORDER_WIDTH, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,120,137, 0, 0,SCSIBORDER_WIDTH, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,120,156, 0, 0,SCSIBORDER_WIDTH, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
		
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,270,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate scsiInfoRequestT = {
	-1, -1, 390, 184, scsiInfoGadgets
};

/*
	Tape control requester
*/

#if (AMERICAN | BRITISH)

static GadgetTemplate tapeControlGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 64, 20, 0, 0,'D',0, strDone },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,130,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	
	{ GADG_PUSH_BUTTON,	 20,100, 0, 0, 64, 20, 0, 0,'E',0, "Erase" },
	{ GADG_PUSH_BUTTON,	 20, 70, 0, 0, 64, 20, 0, 0,'T',0, "Tension" },
	{ GADG_PUSH_BUTTON,	 20, 40, 0, 0, 64, 20, 0, 0,'R',0, "Rewind" },
	{ GADG_PUSH_BUTTON,	 20,130 ,0, 0, 64, 20, 0, 0,'A',0, "Advance" },
	
	GT_UPARROW(100,135, 0x1C),
	GT_DNARROW(100,135, 0x1D),
	
	{ GADG_CHECK_BOX,		100,106, 0, 0,  0,  0, 0, 0,'Q',0, "Quick Erase" },
		
	{ GADG_STAT_TEXT, 	118,136, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Tape Control" },
	{ GADG_ITEM_NONE }
};

static RequestTemplate tapeControlRequestT = {
	-1, -1, 250, 164, tapeControlGadgets
};

#elif GERMAN

static GadgetTemplate tapeControlGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 64, 20, 0, 0,'D',0, strDone },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,170,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	
	{ GADG_PUSH_BUTTON,	 20,100, 0, 0,100, 20, 0, 0,'L',0, "L�schen" }, 
	{ GADG_PUSH_BUTTON,	 20, 70, 0, 0,100, 20, 0, 0,'S',0, "Straffen" },
	{ GADG_PUSH_BUTTON,	 20, 40, 0, 0,100, 20, 0, 0,'Z',0, "Zur�ckspulen" },
	{ GADG_PUSH_BUTTON,	 20,130 ,0, 0,100, 20, 0, 0,'e',0, "Leerstelle" },
	
	GT_UPARROW(100,135, 0x1C),
	GT_DNARROW(100,135, 0x1D),
	
	{ GADG_CHECK_BOX,		140,106, 0, 0,  0,  0, 0, 0,'Q',0, "Quick Erase" },
		
	{ GADG_STAT_TEXT, 	118,136, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Band-Steuerung" },
	{ GADG_ITEM_NONE }
};

static RequestTemplate tapeControlRequestT = {
	-1, -1, 290, 164, tapeControlGadgets
};

#elif FRENCH

static GadgetTemplate tapeControlGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 64, 20, 0, 0,'D',0, strDone },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,146,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	
	{ GADG_PUSH_BUTTON,	 20,100, 0, 0, 80, 20, 0, 0,'E',0, "Efface" },
	{ GADG_PUSH_BUTTON,	 20, 70, 0, 0, 80, 20, 0, 0,'R',0, "Retend" },
	{ GADG_PUSH_BUTTON,	 20, 40, 0, 0, 80, 20, 0, 0,'b',0, "Rembobine" },
	{ GADG_PUSH_BUTTON,	 20,130 ,0, 0, 80, 20, 0, 0,'v',0, "Avance" },
	
	GT_UPARROW(100,135, 0x1C),
	GT_DNARROW(100,135, 0x1D),
	
	{ GADG_CHECK_BOX,		116,106, 0, 0,  0,  0, 0, 0,'Q',0, "Quick Erase" },
		
	{ GADG_STAT_TEXT, 	118,136, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Contr�le des bandes" },
	{ GADG_ITEM_NONE }
};

static RequestTemplate tapeControlRequestT = {
	-1, -1, 266, 164, tapeControlGadgets
};

#elif SWEDISH
#endif

/*
	Tape wait requester
*/

static GadgetTemplate tapeWaitGadgets[] = {
	{ GADG_STAT_STDIMAGE, 12,  10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_NOTE },
	{ GADG_PUSH_BUTTON,  -75, -32, 0, 0, 60, 20, 0, 0, KEY_CANCEL, 0, strCancel },

#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT, 57, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Tape drive is not ready.\nPlease stand by." },
#elif GERMAN
	{ GADG_STAT_TEXT, 57, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Das Bandlaufwerk ist\nnoch nicht bereit.\nBitte warten Sie." },
#elif FRENCH
	{ GADG_STAT_TEXT, 57, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Le lecteur de bandes\nn'est pas pr�t.\nAttendez, s'il vous pla�t." },
#elif SWEDISH
#endif

	{ GADG_ITEM_NONE }
};

static RequestTemplate tapeWaitRequestT = {
	-1, -1, 300, 68, tapeWaitGadgets
};

/*
 *	Buffer options requester
 */

#if (AMERICAN | BRITISH)

static GadgetTemplate buffOptsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 38, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_STAT_TEXT,		 20, 38, 0, 0,  0,  0, 0, 0, 0, 0, "Backup buffer size:" },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 190, 38, 0, 0, 32, 12, 0, 0, 0, 0, NULL },
	GT_UPARROW(190+37,38, 0x1C),
	GT_DNARROW(190+37,38, 0x1D),

	{ GADG_STAT_TEXT,		 20, 58, 0, 0,  0,  0, 0, 0, 0, 0, "Restore buffer size:" },	
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 190, 58, 0, 0, 32, 12, 0, 0, 0, 0, NULL },
	GT_UPARROW(190+37,58, 0x1E),
	GT_DNARROW(190+37,58, 0x1F),
	
	{ GADG_RADIO_BUTTON,	293, 84, 0, 0,  0,  0, 0, 0,'n',0, "Any" },
	{ GADG_RADIO_BUTTON,	212, 84, 0, 0,  0,  0, 0, 0,'2',0, "24-bit" },
	{ GADG_RADIO_BUTTON,	122, 84, 0, 0,  0,  0, 0, 0,'G',0, "Graphics" },

	{ GADG_CHECK_BOX,		 20,104, 0, 0,  0,  0, 0, 0,'A',0, "Asynchronous I/O" },
			
	{ GADG_STAT_TEXT,		244, 38, 0, 0,  0,  0, 0, 0, 0, 0, strK },
	{ GADG_STAT_TEXT,		244, 58, 0, 0,  0,  0, 0, 0, 0, 0, strK }, 
	{ GADG_STAT_TEXT,		 20, 84, 0, 0,  0,  0, 0, 0, 0, 0, "Memory type:" },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Buffer Options" },

	{ GADG_STAT_STDBORDER,20, 25, 0, 0,232,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate buffOptsRequestT = {
	-1, -1, 352, 126, buffOptsGadgets
};

#elif GERMAN

static GadgetTemplate buffOptsGadgets[] = {
	{ GADG_PUSH_BUTTON, -100, 10, 0, 0, 80, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON, -100, 38, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_STAT_TEXT,		 20, 38, 0, 0,  0,  0, 0, 0, 0, 0, "Gr��e des Puffers f�r Datensicherung:" },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 190, 38, 0, 0, 32, 12, 0, 0, 0, 0, NULL },
	GT_UPARROW(190+37,38, 0x1C),
	GT_DNARROW(190+37,38, 0x1D),

	{ GADG_STAT_TEXT,		 20, 58, 0, 0,  0,  0, 0, 0, 0, 0, "Gr��e des Puffers f�r Datenwiederherstellung:" },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 190, 58, 0, 0, 32, 12, 0, 0, 0, 0, NULL },
	GT_UPARROW(190+37,58, 0x1E),
	GT_DNARROW(190+37,58, 0x1F),
	
	{ GADG_RADIO_BUTTON,	293, 84, 0, 0,  0,  0, 0, 0,'B',0, "Beliebig" },
	{ GADG_RADIO_BUTTON,	212, 84, 0, 0,  0,  0, 0, 0,'2',0, "24-bit" },
	{ GADG_RADIO_BUTTON,	122, 84, 0, 0,  0,  0, 0, 0,'C',0, "Chip-Mem" },

	{ GADG_CHECK_BOX,		 20,104, 0, 0,  0,  0, 0, 0,'A',0, "Asynchronous I/O" },
			
	{ GADG_STAT_TEXT,		244, 38, 0, 0,  0,  0, 0, 0, 0, 0, strK },
	{ GADG_STAT_TEXT,		244, 58, 0, 0,  0,  0, 0, 0, 0, 0, strK }, 
	{ GADG_STAT_TEXT,		 20, 84, 0, 0,  0,  0, 0, 0, 0, 0, "Art des Speichers:" },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Einstellungen Puffer" },

	{ GADG_STAT_STDBORDER,20, 25, 0, 0,260,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate buffOptsRequestT = {
	-1, -1, 400, 126, buffOptsGadgets
};

#elif FRENCH

static GadgetTemplate buffOptsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 38, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_STAT_TEXT,		 20, 38, 0, 0,  0,  0, 0, 0, 0, 0, "Backup buffer size:" },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 190, 38, 0, 0, 32, 12, 0, 0, 0, 0, NULL },
	GT_UPARROW(190+37,38, 0x1C),
	GT_DNARROW(190+37,38, 0x1D),

	{ GADG_STAT_TEXT,		 20, 58, 0, 0,  0,  0, 0, 0, 0, 0, "Restore buffer size:" },	
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 190, 58, 0, 0, 32, 12, 0, 0, 0, 0, NULL },
	GT_UPARROW(190+37,58, 0x1E),
	GT_DNARROW(190+37,58, 0x1F),
	
	{ GADG_RADIO_BUTTON,	293, 84, 0, 0,  0,  0, 0, 0,'Q',0, "Quelconque" },
	{ GADG_RADIO_BUTTON,	212, 84, 0, 0,  0,  0, 0, 0,'2',0, "24-bit" },
	{ GADG_RADIO_BUTTON,	122, 84, 0, 0,  0,  0, 0, 0,'G',0, "Graphique" },

	{ GADG_CHECK_BOX,		 20,104, 0, 0,  0,  0, 0, 0,'E',0, "E/S asynchrones" },
			
	{ GADG_STAT_TEXT,		244, 38, 0, 0,  0,  0, 0, 0, 0, 0, strK },
	{ GADG_STAT_TEXT,		244, 58, 0, 0,  0,  0, 0, 0, 0, 0, strK }, 
	{ GADG_STAT_TEXT,		 20, 84, 0, 0,  0,  0, 0, 0, 0, 0, "Type m�moire:" },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Options m�moire" },

	{ GADG_STAT_STDBORDER,20, 25, 0, 0,235,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate buffOptsRequestT = {
	-1, -1, 355, 126, buffOptsGadgets
};

#elif SWEDISH
#endif

/*
	Tag filter requester
*/

#if (AMERICAN | BRITISH)

#define TAG_LIST_WIDTH	230
#define TAG_LIST_NUM		4
#define TAG_LIST_X		174
#define TAG_LIST_Y		118

static GadgetTemplate tagGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_UNTAG,0, strUntag },
	{ GADG_PUSH_BUTTON,	-80, 38, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_CHECK_BOX,		 20,200, 0, 0,  0,  0, 0, 0,'p',0, "Apply only to files in current drawer" },
	
	{ GADG_RADIO_BUTTON,	130, 36, 0, 0,  0,  0, 0, 0,'I',0, "Include:" },
	{ GADG_RADIO_BUTTON,	 20, 36, 0, 0,  0,  0, 0, 0,'E',0, "Exclude:" },

	{ GADG_CHECK_BOX,		 30, 78, 0, 0,  0,  0, 0, 0,'b',0, "Files modified on or before:" },
	{ GADG_CHECK_BOX,		 30, 98, 0, 0,  0,  0, 0, 0,'a',0, "Files modified on or after:" },
	{ GADG_CHECK_BOX,		 30,118, 0, 0,  0,  0, 0, 0,'m',0, "Files matching:" },

	{ GADG_PUSH_BUTTON,	 30, 167, 0, 0, 60, 20, 0, 0,'d',0, "Add" },
	{ GADG_PUSH_BUTTON,  101, 167, 0, 0, 60, 20, 0, 0,'R',0, "Remove" },
		
	SL_GADG_BOX(	TAG_LIST_X, TAG_LIST_Y, TAG_LIST_WIDTH, TAG_LIST_NUM),
	SL_GADG_UPARROW(	TAG_LIST_X, TAG_LIST_Y, TAG_LIST_WIDTH, TAG_LIST_NUM),
	SL_GADG_DOWNARROW(TAG_LIST_X, TAG_LIST_Y, TAG_LIST_WIDTH, TAG_LIST_NUM),
	SL_GADG_SLIDER(	TAG_LIST_X, TAG_LIST_Y, TAG_LIST_WIDTH, TAG_LIST_NUM),

	GT_UPARROW(392, 78, 0),
	GT_DNARROW(392, 78, 0),
	
	GT_UPARROW(392, 98, 0),
	GT_DNARROW(392, 98, 0),

	{ GADG_EDIT_TEXT,	TAG_LIST_X + 2, TAG_LIST_Y+51+((20-12)/2), 0, 0,TAG_LIST_WIDTH - 4, 12, 0, 0, 0, 0, NULL },


	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 274, 78, 0, 0, 24, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT,	314, 78, 0, 0, 32, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 362, 78, 0, 0, 24, 12, 0, 0, 0, 0, NULL },	
		
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 274, 98, 0, 0, 24, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT,	314, 98, 0, 0, 32, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 362, 98, 0, 0, 24, 12, 0, 0, 0, 0, NULL },	

	{ GADG_CHECK_BOX,		 30, 58, 0, 0,  0,  0, 0, 0,KEY_TAG_ARC,0, strArchive1 },
	
	{ GADG_PUSH_BUTTON,	 30,140, 0, 0, 60, 20, 0, 0,'L',0, "Load" },
	{ GADG_PUSH_BUTTON,  101,140, 0, 0, 60, 20, 0, 0,KEY_SAVE2,0, strSave },
		
	{ GADG_STAT_TEXT,		302, 78, 0, 0,  0,  0, 0, 0, 0, 0, strDateSep },
	{ GADG_STAT_TEXT,		350, 78, 0, 0,  0,  0, 0, 0, 0, 0, strDateSep },
		
	{ GADG_STAT_TEXT,		302, 98, 0, 0,  0,  0, 0, 0, 0, 0, strDateSep },
	{ GADG_STAT_TEXT,		350, 98, 0, 0,  0,  0, 0, 0, 0, 0, strDateSep },

	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Tag Filter" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,306,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate tagRequestT = {
	-1, -1, 426, 222, tagGadgets
};

#elif GERMAN

#define TAG_LIST_WIDTH	230
#define TAG_LIST_NUM	4
#define TAG_LIST_X		210
#define TAG_LIST_Y		137

static GadgetTemplate tagGadgets[] = {
	{ GADG_PUSH_BUTTON, -100, 10, 0, 0, 80, 20, 0, 0,KEY_UNTAG,0, strUntag },
	{ GADG_PUSH_BUTTON, -100, 38, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_CHECK_BOX,		 20,215, 0, 0,  0,  0, 0, 0,'N',0, "Nur auf Dateien im aktuellen Verzeichnis anwenden" },

	{ GADG_RADIO_BUTTON,	150, 36, 0, 0,  0,  0, 0, 0,'i',0, "Einschlie�en:" },
	{ GADG_RADIO_BUTTON,	 20, 36, 0, 0,  0,  0, 0, 0,'u',0, "Ausschlie�en:" },

	{ GADG_CHECK_BOX,		 30, 75, 0, 0,  0,  0, 0, 0,'D',0, "Dateien die am oder davor ver�ndert wurden:" },
	{ GADG_CHECK_BOX,		 30,105, 0, 0,  0,  0, 0, 0,'t',0, "Dateien die am oder danach ver�ndert wurden:" },
	{ GADG_CHECK_BOX,		 30,135, 0, 0,  0,  0, 0, 0,'s',0, "Dateien-Auswahl:" },

	{ GADG_PUSH_BUTTON,	 30,182, 0, 0, 80, 20, 0, 0,'H',0, "Anf�gen" },
	{ GADG_PUSH_BUTTON,  120,182, 0, 0, 80, 20, 0, 0,'f',0, "Entfernen" },

	SL_GADG_BOX(	TAG_LIST_X, TAG_LIST_Y, TAG_LIST_WIDTH, TAG_LIST_NUM),
	SL_GADG_UPARROW(	TAG_LIST_X, TAG_LIST_Y, TAG_LIST_WIDTH, TAG_LIST_NUM),
	SL_GADG_DOWNARROW(TAG_LIST_X, TAG_LIST_Y, TAG_LIST_WIDTH, TAG_LIST_NUM),
	SL_GADG_SLIDER(	TAG_LIST_X, TAG_LIST_Y, TAG_LIST_WIDTH, TAG_LIST_NUM),

	GT_UPARROW(192, 90),
	GT_DNARROW(192, 90),

	GT_UPARROW(192, 120),
	GT_DNARROW(192, 120),

	{ GADG_EDIT_TEXT,	TAG_LIST_X + 2, TAG_LIST_Y+51+((20-12)/2), 0, 0,TAG_LIST_WIDTH - 4, 12, 0, 0, 0, 0, NULL },


	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 74, 90, 0, 0, 24, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT,	114, 90, 0, 0, 32, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 162, 90, 0, 0, 24, 12, 0, 0, 0, 0, NULL },

	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 74, 120, 0, 0, 24, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT,	114, 120, 0, 0, 32, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 162, 120, 0, 0, 24, 12, 0, 0, 0, 0, NULL },

	{ GADG_CHECK_BOX,		 30, 60, 0, 0,  0,  0, 0, 0,KEY_TAG_ARC,0, strArchive1 },

	{ GADG_PUSH_BUTTON,	 30,155, 0, 0, 80, 20, 0, 0,'L',0, "Laden" },
	{ GADG_PUSH_BUTTON,  120,155, 0, 0, 80, 20, 0, 0,KEY_SAVE2,0, strSave },

	{ GADG_STAT_TEXT,		102, 90, 0, 0,  0,  0, 0, 0, 0, 0, strDateSep },
	{ GADG_STAT_TEXT,		150, 90, 0, 0,  0,  0, 0, 0, 0, 0, strDateSep },

	{ GADG_STAT_TEXT,		102,120, 0, 0,  0,  0, 0, 0, 0, 0, strDateSep },
	{ GADG_STAT_TEXT,		150,120, 0, 0,  0,  0, 0, 0, 0, 0, strDateSep },

	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Auswahlkriterium" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,306,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate tagRequestT = {
	-1, -1, 455, 240, tagGadgets
};

#elif FRENCH

#define TAG_LIST_WIDTH	230
#define TAG_LIST_NUM	4
#define TAG_LIST_X		174
#define TAG_LIST_Y		118

static GadgetTemplate tagGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_UNTAG,0, strUntag },
	{ GADG_PUSH_BUTTON,	-80, 38, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_CHECK_BOX,		 20,200, 0, 0,  0,  0, 0, 0,'N',0, "N'appliquer qu'aux fichiers du tiroir courant" },

	{ GADG_RADIO_BUTTON,	130, 36, 0, 0,  0,  0, 0, 0,'I',0, "Inclure:" },
	{ GADG_RADIO_BUTTON,	 20, 36, 0, 0,  0,  0, 0, 0,'x',0, "Exclure:" },

	{ GADG_CHECK_BOX,		 30, 78, 0, 0,  0,  0, 0, 0,'F',0, "Fich. chang�s le/avant le:" },
	{ GADG_CHECK_BOX,		 30, 98, 0, 0,  0,  0, 0, 0,'c',0, "Fich. chang�s le/depuis le:" },
	{ GADG_CHECK_BOX,		 30,118, 0, 0,  0,  0, 0, 0,'l',0, "Filtrage noms:" },

	{ GADG_PUSH_BUTTON,	 30,167, 0, 0, 60, 20, 0, 0,'j',0, "Ajoute" },
	{ GADG_PUSH_BUTTON,  101,167, 0, 0, 60, 20, 0, 0,'v',0, "Enl�ve" },

	SL_GADG_BOX(	TAG_LIST_X, TAG_LIST_Y, TAG_LIST_WIDTH, TAG_LIST_NUM),
	SL_GADG_UPARROW(	TAG_LIST_X, TAG_LIST_Y, TAG_LIST_WIDTH, TAG_LIST_NUM),
	SL_GADG_DOWNARROW(TAG_LIST_X, TAG_LIST_Y, TAG_LIST_WIDTH, TAG_LIST_NUM),
	SL_GADG_SLIDER(	TAG_LIST_X, TAG_LIST_Y, TAG_LIST_WIDTH, TAG_LIST_NUM),

	GT_UPARROW(392, 78),
	GT_DNARROW(392, 78),

	GT_UPARROW(392, 98),
	GT_DNARROW(392, 98),

	{ GADG_EDIT_TEXT,	TAG_LIST_X + 2, TAG_LIST_Y+51+((20-12)/2), 0, 0,TAG_LIST_WIDTH - 4, 12, 0, 0, 0, 0, NULL },


	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 274, 78, 0, 0, 24, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT,	314, 78, 0, 0, 32, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 362, 78, 0, 0, 24, 12, 0, 0, 0, 0, NULL },

	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 274, 98, 0, 0, 24, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT,	314, 98, 0, 0, 32, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 362, 98, 0, 0, 24, 12, 0, 0, 0, 0, NULL },

	{ GADG_CHECK_BOX,		 30, 58, 0, 0, 0, 12, 0, 0,KEY_TAG_ARC,0, strArchive1 },

	{ GADG_PUSH_BUTTON,	 30,140, 0, 0, 60, 20, 0, 0,'h',0, "Charge" },
	{ GADG_PUSH_BUTTON,  101,140, 0, 0, 60, 20, 0, 0,KEY_SAVE2,0, strSave },

	{ GADG_STAT_TEXT,		302, 78, 0, 0,  0,  0, 0, 0, 0, 0, strDateSep },
	{ GADG_STAT_TEXT,		350, 78, 0, 0,  0,  0, 0, 0, 0, 0, strDateSep },

	{ GADG_STAT_TEXT,		302, 98, 0, 0,  0,  0, 0, 0, 0, 0, strDateSep },
	{ GADG_STAT_TEXT,		350, 98, 0, 0,  0,  0, 0, 0, 0, 0, strDateSep },

	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Filtre de marquage" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,306,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate tagRequestT = {
	-1, -1, 426, 222, tagGadgets
};

#elif SWEDISH
#endif

/*
	Prompt for password requester
*/

#if (AMERICAN | BRITISH | FRENCH)

static GadgetTemplate passProGadgets[] = {
	{ GADG_PUSH_BUTTON,  -75, -32, 0, 0, 60, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -150, -32, 0, 0, 60, 20, 0, 0, KEY_CANCEL, 0, strCancel },
	
	{ GADG_EDIT_TEXT,	 130,  92, 0, 0, 80, 12, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDIMAGE,	12, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },	
	{ GADG_STAT_TEXT,		54, 92, 0, 0, 0, 0, 0, 0, 0, 0, strPassword },

#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT,	54, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Type in a password at the prompt, or" },
	{ GADG_STAT_TEXT, 	54, 24, 0, 0, 0, 0, 0, 0, 0, 0, "hit \"Cancel\" to nullify password protection." },
		
	{ GADG_STAT_TEXT,	54, 50, 0, 0, 0, 0, 0, 0, 0, 0, "Do not forget the password, or you" },
	{ GADG_STAT_TEXT,	54, 64, 0, 0, 0, 0, 0, 0, 0, 0, "will not be able to recover this backup!" },
#elif FRENCH
	{ GADG_STAT_TEXT,	54, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Entrez le mot cl�, ou \"Annule\"" },
	{ GADG_STAT_TEXT, 	54, 24, 0, 0, 0, 0, 0, 0, 0, 0, "pour abandonner la protection par cl�." },

	{ GADG_STAT_TEXT,	54, 50, 0, 0, 0, 0, 0, 0, 0, 0, "N'oubliez pas ce mot cl� sinon cette" },
	{ GADG_STAT_TEXT,	54, 64, 0, 0, 0, 0, 0, 0, 0, 0, "sauvegarde sera inutilisable !" },
#endif

	{ GADG_ITEM_NONE }
};

static RequestTemplate passProRequestT = {
	-1, -1, 416, 122, passProGadgets
};

#elif GERMAN

static GadgetTemplate passProGadgets[] = {
	{ GADG_PUSH_BUTTON,  -95,-32, 0, 0, 80, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON, -185,-32, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel },

	{ GADG_EDIT_TEXT,		130,107, 0, 0, 80, 12, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDIMAGE, 12, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_STAT_TEXT,		 54,107, 0, 0,  0,  0, 0, 0, 0, 0, strPassword },

	{ GADG_STAT_TEXT,		 54, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Geben Sie bei der Eingabeaufforderung ein\nKennwort ein oder dr�cken sie \"Abbrechen\", um den" },
	{ GADG_STAT_TEXT, 	 54, 24, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,		 54, 50, 0, 0,  0,  0, 0, 0, 0, 0, "Kennwortschutz au�er Kraft zu setzen. Vergessen\nSie auf keinen Fall das Kennwort, da Sie sonst\nIhre Datensicherung nicht wiederherstellen\nk�nnen." },
	{ GADG_STAT_TEXT,		 54, 64, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_ITEM_NONE }
};

static RequestTemplate passProRequestT = {
	-1, -1, 460, 137, passProGadgets
};

#elif SWEDISH
#endif

/*
 *	Ask for the password requester
 */

static GadgetTemplate passAskGadgets[] = {
	{ GADG_PUSH_BUTTON, 	-75, -32, 0, 0, 60, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON,	   -150, -32, 0, 0, 60, 20, 0, 0, KEY_CANCEL, 0, strCancel },
	
	{ GADG_EDIT_TEXT,		130,-30, 0, 0, 80, 12, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 54, 40, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
		
	{ GADG_STAT_STDIMAGE,	 12, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_STAT_TEXT,		 54,-30, 0, 0,  0,  0, 0, 0, 0, 0, strPassword },
		
#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT,		 54, 10, 0, 0,  0,  0, 0, 0, 0, 0, "This backup was password-protected." },
	{ GADG_STAT_TEXT,		 54, 26, 0, 0,  0,  0, 0, 0, 0, 0, "Please enter its password at the prompt." },
#elif GERMAN
	{ GADG_STAT_TEXT,		 54, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Diese Datensicherung wurde durch ein Kennwort\ngesch�tzt. Bitte geben Sie dieses Kennwort\nbei der Eingabeaufforderung ein." },
	{ GADG_STAT_TEXT,		 54, 26, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
#elif FRENCH
	{ GADG_STAT_TEXT,		 54, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Cette sauvegarde est prot�g�e par une cl�." },
	{ GADG_STAT_TEXT,		 54, 26, 0, 0,  0,  0, 0, 0, 0, 0, "Entrez ce mot cl�." },
#elif SWEDISH
#endif

	{ GADG_ITEM_NONE }	
};

static RequestTemplate passAskRequestT = {
	-1, -1, 406, 100, passAskGadgets
};

/*
 *	Prompt to rename the file
 */

#if (AMERICAN | BRITISH)

static GadgetTemplate renameGadgets[] = {
	{ GADG_PUSH_BUTTON,  -80,-32, 0, 0, 66, 20, 0, 0,'R',0, strRename },
	{ GADG_PUSH_BUTTON, -320,-32, 0, 0, 66, 20, 0, 0,KEY_CANCEL,0, strCancel },
	{ GADG_PUSH_BUTTON, -240,-32, 0, 0, 66, 20, 0, 0,'S',0, strSkip },
	{ GADG_PUSH_BUTTON, -160,-32, 0, 0, 66, 20, 0, 0,'p',0, strReplace },
	
	{ GADG_EDIT_TEXT,		198, 28, 0, 0,224, 12, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 54, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
		
	{ GADG_STAT_STDIMAGE, 12, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_STAT_TEXT,		 54, 28, 0, 0,  0,  0, 0, 0, 0, 0, "Enter a new name:" },
	{ GADG_ITEM_NONE }	
};

static RequestTemplate renameRequestT = {
	-1, -1, 440, 84, renameGadgets
};

#elif GERMAN

static GadgetTemplate renameGadgets[] = {
	{ GADG_PUSH_BUTTON, -100,-32, 0, 0, 90, 20, 0, 0,'U',0, strRename },
	{ GADG_PUSH_BUTTON, -355,-32, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel },
	{ GADG_PUSH_BUTTON, -270,-32, 0, 0, 80, 20, 0, 0,'b',0, strSkip },
	{ GADG_PUSH_BUTTON, -185,-32, 0, 0, 80, 20, 0, 0,'E',0, strReplace },

	{ GADG_EDIT_TEXT,		 85, 40, 0, 0,224, 12, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 54, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDIMAGE, 12, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_STAT_TEXT,		 54, 25, 0, 0,  0,  0, 0, 0, 0, 0, "Neuen Dateinamen eingeben:" },
	{ GADG_ITEM_NONE }
};

static RequestTemplate renameRequestT = {
	-1, -1, 440, 90, renameGadgets
};

#elif FRENCH

static GadgetTemplate renameGadgets[] = {
	{ GADG_PUSH_BUTTON,  -90,-32, 0, 0, 80, 20, 0, 0,'R',0, strRename },
	{ GADG_PUSH_BUTTON, -360,-32, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel },
	{ GADG_PUSH_BUTTON, -270,-32, 0, 0, 80, 20, 0, 0,'S',0, strSkip },
	{ GADG_PUSH_BUTTON, -180,-32, 0, 0, 80, 20, 0, 0,'p',0, strReplace },

	{ GADG_EDIT_TEXT,		238, 28, 0, 0,184, 12, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 54, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDIMAGE, 12, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_STAT_TEXT,		 54, 28, 0, 0,  0,  0, 0, 0, 0, 0, "Entrez un nouveau nom:" },
	{ GADG_ITEM_NONE }
};

static RequestTemplate renameRequestT = {
	-1, -1, 440, 84, renameGadgets
};

#elif SWEDISH
#endif

/*
	Rewinding tape requester
*/

static GadgetTemplate rewindGadgets[] = {
	{ GADG_STAT_TEXT,		 56, 22, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
		
	{ GADG_STAT_STDIMAGE, 12, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_NOTE },
	{ GADG_ITEM_NONE }	
};

static RequestTemplate rewindRequestT = {
#if FRENCH
	-1, -1, 281, 58, rewindGadgets
#else
	-1, -1, 249, 58, rewindGadgets
#endif
};


/*
	Seek alt-cat/archive flag tape requester
*/

static GadgetTemplate seekGadgets[] = {
	{ GADG_STAT_TEXT,		 54, 14, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
		
	{ GADG_STAT_STDIMAGE, 12, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_NOTE },
#if (AMERICAN | BRITISH)
	{ GADG_STAT_BORDER, 	 56, 32, 0, 0,(292-56)-16, 12, 0, 0, 0, 0, NULL },
#elif FRENCH
	{ GADG_STAT_BORDER,	 56, 32, 0, 0,(356-56)-16, 12, 0, 0, 0, 0, NULL },
#elif GERMAN
	{ GADG_STAT_BORDER,	 56, 32, 0, 0,(414-56)-16, 12, 0, 0, 0, 0, NULL },
#endif
	{ GADG_ITEM_NONE }	
};
	
/*
	Seeking to alternate catalog requester
*/

static RequestTemplate seekRequestT = {
#if (AMERICAN | BRITISH)
	-1, -1, 292, 58, seekGadgets
#elif FRENCH
	-1, -1, 356, 58, seekGadgets
#elif GERMAN
	-1, -1, 414, 58, seekGadgets
#endif
};

/*
	Scan all files on a volume dialog
*/

#if (AMERICAN | BRITISH | FRENCH)

static GadgetTemplate scanGadgets[] = {
	{ GADG_STAT_TEXT,	 14, 8, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	
	{ GADG_PUSH_BUTTON,	-70, -28, 0, 0, 60, 19, 0, 0, KEY_CANCEL, 0, strCancel},

#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT,	 78, 30, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	 78, 48, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	 14, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Drawer: " },
	{ GADG_STAT_TEXT,	 14, 48, 0, 0, 0, 0, 0, 0, 0, 0, "File: " },
#elif FRENCH
	{ GADG_STAT_TEXT,	 86, 30, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	 86, 48, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	 14, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Tiroir: " },
	{ GADG_STAT_TEXT,	 14, 48, 0, 0, 0, 0, 0, 0, 0, 0, "Fichier: " },
#endif
	{ GADG_ITEM_NONE } 
};

static RequestTemplate scanRequestT = {
	-1, -1, 442, 72, scanGadgets
};

#elif GERMAN

static GadgetTemplate scanGadgets[] = {
	{ GADG_STAT_TEXT,		 14,  8, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_PUSH_BUTTON,	-90,-28, 0, 0,80,19, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_STAT_TEXT,		118, 30, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		118, 48, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 14, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Verzeichnis: " },
	{ GADG_STAT_TEXT,		 14, 48, 0, 0, 0, 0, 0, 0, 0, 0, "Datei: " },
	{ GADG_ITEM_NONE }
};

static RequestTemplate scanRequestT = {
	-1, -1, 442, 72, scanGadgets
};

#elif SWEDISH
#endif

/*
	 Error requester
*/

static GadgetTemplate errorGadgets[] = {
	{ GADG_PUSH_BUTTON, -75,-32, 0, 0, 60, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_STAT_TEXT,	  0,  0, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	  0,  0, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	 52, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	 52, 25, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE,10, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

static RequestTemplate errorRequestT = {
#if (AMERICAN | BRITISH)
	-1, -1, 388, 80, errorGadgets
#elif GERMAN
	-1, -1, 404, 95, errorGadgets
#elif FRENCH
	-1, -1, 440, 95, errorGadgets
#elif SWEDISH
#endif
};

/*
	Warning requester, has two buttons instead of one.
*/

static GadgetTemplate warnGadgets[] = {
#if (AMERICAN | BRITISH | FRENCH)
	{ GADG_PUSH_BUTTON,  -75,-32, 0, 0, 60, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -150,-32, 0, 0, 60, 20, 0, 0, KEY_CANCEL, 0, strCancel },
#elif GERMAN
	{ GADG_PUSH_BUTTON,  -95,-32, 0, 0, 80, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -195,-32, 0, 0, 80, 20, 0, 0, KEY_CANCEL, 0, strCancel },
#elif SWEDISH
#endif
	{ GADG_STAT_TEXT, 	   0,  0, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	  52, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	  52, 25, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

static RequestTemplate warnRequestT = {
#if (AMERICAN | BRITISH)
	-1, -1, 388, 80, warnGadgets
#elif GERMAN
	-1, -1, 404, 95, warnGadgets
#elif FRENCH
	-1, -1, 440, 80, warnGadgets
#elif SWEDISH
#endif
};

/*
	Three-choice requester w/ skip button, has three buttons.
*/

static GadgetTemplate skipGadgets[] = {
#if (AMERICAN | BRITISH | FRENCH)
	{ GADG_PUSH_BUTTON,  -75,-32, 0, 0, 60, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -150,-32, 0, 0, 60, 20, 0, 0, KEY_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON, -225,-32, 0, 0, 60, 20, 0, 0, 'S', 0, strSkip },
#elif GERMAN
	{ GADG_PUSH_BUTTON,  -95,-32, 0, 0, 80, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -195,-32, 0, 0, 80, 20, 0, 0, KEY_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON, -295,-32, 0, 0, 80, 20, 0, 0, 'S', 0, strSkip },
#elif SWEDISH
#endif
	{ GADG_STAT_TEXT,	  52, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,	  52, 25, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

static RequestTemplate skipRequestT = {
#if (AMERICAN | BRITISH)
	-1, -1, 388, 80, skipGadgets
#elif GERMAN
	-1, -1, 404, 95, skipGadgets
#elif FRENCH
	-1, -1, 440, 80, skipGadgets
#elif SWEDISH
#endif
};

/*
	Session info requester for backup mode.
*/

#if (AMERICAN | BRITISH)

static GadgetTemplate setSessInfoGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 38, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Please enter a name for this backup,\nalong with any comment you wish to make." },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,	122, 72, 0, 0,296, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,	122, 94, 0, 0,296, 12, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 	 20, 72, 0, 0,  0,  0, 0, 0, 0, 0, strBackupName },
	{ GADG_STAT_TEXT,		 20, 94, 0, 0,  0,  0, 0, 0, 0, 0, strComments },	
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Set Session Information" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,320,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }

};

static RequestTemplate setSessInfoReqT = {
	-1, -1, 440, 120, setSessInfoGadgets
};

#elif GERMAN

static GadgetTemplate setSessInfoGadgets[] = {
	{ GADG_PUSH_BUTTON,	-100, 10, 0, 0, 80, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-100, 38, 0, 0, 80, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Geben Sie bitte einen Namen f�r die\nDatensicherung ein, zusammen mit allen\nBemerkungen die Sie evtl. machen wollen." },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,	42, 95, 0, 0,296, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,	42,130, 0, 0,296, 12, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 	 20, 80, 0, 0,  0,  0, 0, 0, 0, 0, strBackupName },
	{ GADG_STAT_TEXT,		 20,115, 0, 0,  0,  0, 0, 0, 0, 0, strComments },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Sitzungsinformation eingeben" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,320,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }

};

static RequestTemplate setSessInfoReqT = {
	-1, -1, 460, 155, setSessInfoGadgets
};

#elif FRENCH

static GadgetTemplate setSessInfoGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	-80, 38, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel},

	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, "Indiquez un nom pour la sauvegarde\nainsi qu'un commentaire facultatif." },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,	146, 72, 0, 0,272, 12, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,	146, 94, 0, 0,272, 12, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 	 20, 72, 0, 0,  0,  0, 0, 0, 0, 0, strBackupName },
	{ GADG_STAT_TEXT,		 20, 94, 0, 0,  0,  0, 0, 0, 0, 0, strComments },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Entrer infos sauvegarde" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,340,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }

};

static RequestTemplate setSessInfoReqT = {
	-1, -1, 440, 120, setSessInfoGadgets
};

#elif SWEDISH
#endif

/*
	Session info requester for restore mode.
*/

static GadgetTemplate getSessInfoGadgets[] = {

#if (AMERICAN | BRITISH)
	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,320,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
		
	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	/*
	{ GADG_STAT_TEXT,		120, 69, 0, 3,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		120, 91, 0, 3,  0,  0, 0, 0, 0, 0, NULL },
	*/
	{ GADG_STAT_STDBORDER,118,69, 0, 0,302, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,118,91, 0, 0,302, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	
	{ GADG_STAT_TEXT,		 20, 69, 0, 3,  0,  0, 0, 0, 0, 0, strBackupName },	
	{ GADG_STAT_TEXT,		 20, 91, 0, 3,  0,  0, 0, 0, 0, 0, strComments },

	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Get Session Information" },	
#elif GERMAN

	{ GADG_PUSH_BUTTON, -100, 10, 0, 0, 80, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,320,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	
	/*
	{ GADG_STAT_TEXT,		 42, 69, 0, 3,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 42, 91, 0, 3,  0,  0, 0, 0, 0, 0, NULL },
	*/
	{ GADG_STAT_STDBORDER,118,69, 0, 0,302, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,118,91, 0, 0,302, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	
	{ GADG_STAT_TEXT,		 20, 69, 0, 3,  0,  0, 0, 0, 0, 0, strBackupName },
	{ GADG_STAT_TEXT,		 20, 91, 0, 3,  0,  0, 0, 0, 0, 0, strComments },

	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Sitzungsinformation anzeigen" },

#elif FRENCH

	{ GADG_PUSH_BUTTON,	-80, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,320,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT,		 20, 35, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	/*
	{ GADG_STAT_TEXT,		144, 69, 0, 3,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		144, 91, 0, 3,  0,  0, 0, 0, 0, 0, NULL },
	*/
	{ GADG_STAT_STDBORDER,118,69, 0, 0,302, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,118,91, 0, 0,302, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	
	{ GADG_STAT_TEXT,		 20, 69, 0, 3,  0,  0, 0, 0, 0, 0, strBackupName },
	{ GADG_STAT_TEXT,		 20, 91, 0, 3,  0,  0, 0, 0, 0, 0, strComments },

	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Lire infos sauvegarde" },
#elif SWEDISH
#endif
	
	/*
	{ GADG_STAT_STDBORDER,118,69, 0, 0,302, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,118,91, 0, 0,302, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	*/
	
	{ GADG_ITEM_NONE }
};

static RequestTemplate getSessInfoReqT = {
	-1, -1, 440, 120, getSessInfoGadgets
};

/********************************************************************
	DIALOGS FOLLOW BELOW...
********************************************************************/

/*
 * About box
 */

static GadgetTemplate aboxGadgets[] = {
	{ GADG_PUSH_BUTTON, -75, -30, 0, 0, 60, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_STAT_TEXT, 35,  85, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 35, 100, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,135, 115, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 15,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Quarterback 6.0" },
#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT, 15,  23, 0, 0, 0, 0, 0, 0, 0, 0, "Central Coast Software" },
	{ GADG_STAT_TEXT, 15,  36, 0, 0, 0, 0, 0, 0, 0, 0, "A div. of New Horizons Software, Inc." },
	{ GADG_STAT_TEXT, 15, 152, 0, 0, 0, 0, 0, 0, 0, 0, "Copyright \251 1992-93" },
	{ GADG_STAT_TEXT, 15, 165, 0, 0, 0, 0, 0, 0, 0, 0, "All Rights Reserved" },
#elif GERMAN
	{ GADG_STAT_TEXT, 15,  23, 0, 0, 0, 0, 0, 0, 0, 0, "Central Coast Software, eine Abteilung" },
	{ GADG_STAT_TEXT, 15,  36, 0, 0, 0, 0, 0, 0, 0, 0, "von New Horizons Software, Inc." },
	{ GADG_STAT_TEXT, 15, 152, 0, 0, 0, 0, 0, 0, 0, 0, "Copyright \251 1992-93" },
	{ GADG_STAT_TEXT, 15, 165, 0, 0, 0, 0, 0, 0, 0, 0, "Alle Rechte vorbehalten" },
#elif FRENCH
	{ GADG_STAT_TEXT, 15,  23, 0, 0, 0, 0, 0, 0, 0, 0, "Central Coast Software, division" },
	{ GADG_STAT_TEXT, 15,  36, 0, 0, 0, 0, 0, 0, 0, 0, "de New Horizons Software, Inc." },
	{ GADG_STAT_TEXT, 15, 152, 0, 0, 0, 0, 0, 0, 0, 0, "Copyright \251 1992-93" },
	{ GADG_STAT_TEXT, 15, 165, 0, 0, 0, 0, 0, 0, 0, 0, "Tous droits r�serv�s" },
#elif SWEDISH
#endif
	{ GADG_STAT_STDBORDER, 15,  56, 0, 0, 290, 85, 0, 0, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_TEXT, 20,  60, 0, 0, 0, 0, 0, 0, 0, 0, "This program is licensed to:" },
	{ GADG_STAT_TEXT, 35, 115, 0, 0, 0, 0, 0, 0, 0, 0, "Serial no.:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboxDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 330, 185, aboxGadgets, NULL
};

/*
	Destination file stdfile replacement
*/

#define LIST_WIDTH	(24*8)
#define SAVE_NUM	7
#define PUT_PROMPT	12

#if (AMERICAN | BRITISH)

GadgetTemplate destFileGadgets[] = {
	{ GADG_PUSH_BUTTON,	-80,-60, 0, 0, 60, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON,	-80,-30, 0, 0, 60, 20, 0, 0, KEY_CANCEL, 0, strCancel },

	{ GADG_PUSH_BUTTON,	-80, 30, 0, 0, 60, 20, 0, 0, KEY_ENTER, 0, strEnter },
	{ GADG_PUSH_BUTTON,	-80, 60, 0, 0, 60, 20, 0, 0, KEY_BACK, 0, strBack },
	{ GADG_PUSH_BUTTON,	-80, 90, 0, 0, 60, 20, 0, 0, 'D', 0, strDisks },

	SL_GADG_BOX(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_UPARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_DOWNARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_SLIDER(20, 30, LIST_WIDTH, SAVE_NUM),

	{ GADG_EDIT_TEXT,		 20, -40, 0, 0, LIST_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,		 65, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE,	 20, 15, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,		 20,-58, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE,	    20,-20, 0, 0, 76, 12, 0, 0, 'N', 0, "Normal" },
	{ GADG_RADIO_BUTTON, 104,-20, 0, 0,116, 12, 0, 0, 'H', 0, "Hierarchical" },
	{ GADG_ITEM_NONE },
};

DialogTemplate destFileDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 120 + LIST_WIDTH, 180, destFileGadgets, NULL
};

#elif GERMAN

GadgetTemplate destFileGadgets[] = {
	{ GADG_PUSH_BUTTON, -100, -60, 0, 0, 80, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -100, -30, 0, 0, 80, 20, 0, 0, KEY_CANCEL, 0, strCancel },

	{ GADG_PUSH_BUTTON, -100,  30, 0, 0, 80, 20, 0, 0, KEY_ENTER, 0, strEnter },
	{ GADG_PUSH_BUTTON, -100,  60, 0, 0, 80, 20, 0, 0, KEY_BACK, 0, strBack },
	{ GADG_PUSH_BUTTON, -100,  90, 0, 0, 80, 20, 0, 0, 'D', 0, strDisks },

	SL_GADG_BOX(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_UPARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_DOWNARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_SLIDER(20, 30, LIST_WIDTH, SAVE_NUM),

	{ GADG_EDIT_TEXT,		 20, -26, 0, 0, LIST_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,		 65,  10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE,	 20,  15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,		 20, -58, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE,	    20, -20, 0, 0, 76, 12, 0, 0, 'N', 0, "Normal" },
	{ GADG_RADIO_BUTTON, 104, -20, 0, 0,  0, 12, 0, 0, 'H', 0, "Hierarchie" },
	{ GADG_ITEM_NONE },
};

DialogTemplate destFileDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 140 + LIST_WIDTH, 180, destFileGadgets, NULL
};

#elif FRENCH

GadgetTemplate destFileGadgets[] = {
	{ GADG_PUSH_BUTTON,	-85, -60, 0, 0, 70, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON,	-85, -30, 0, 0, 70, 20, 0, 0, KEY_CANCEL, 0, strCancel },

	{ GADG_PUSH_BUTTON,	-85,  30, 0, 0, 70, 20, 0, 0, KEY_ENTER, 0, strEnter },
	{ GADG_PUSH_BUTTON,	-85,  60, 0, 0, 70, 20, 0, 0, KEY_BACK, 0, strBack },
	{ GADG_PUSH_BUTTON,	-85,  90, 0, 0, 70, 20, 0, 0, 'V', 0, strDisks },

	SL_GADG_BOX(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_UPARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_DOWNARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_SLIDER(20, 30, LIST_WIDTH, SAVE_NUM),

	{ GADG_EDIT_TEXT,		 20, -40, 0, 0, LIST_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,		 65,  10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE,	 20,  15, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,		 20, -58, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE,		 20, -20, 0, 0, 76, 12, 0, 0, 'N', 0, "Normal" },
	{ GADG_RADIO_BUTTON, 104, -20, 0, 0,  0, 12, 0, 0, 'H', 0, "Hi�rarchique" },
	{ GADG_ITEM_NONE },
};

DialogTemplate destFileDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 120 + LIST_WIDTH, 180, destFileGadgets, NULL
};

#elif SWEDISH
#endif

/*
	Customize macro menu dialog
*/

#if (AMERICAN | BRITISH)

static GadgetTemplate customMacroGadgets[] = {
	{ GADG_PUSH_BUTTON, 105, -30, 0, 0,  60, 20, 0, 0, 'U', 0, strUse },
	{ GADG_PUSH_BUTTON, 185, -30, 0, 0,  60, 20, 0, 0, KEY_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON,  25, -30, 0, 0,  60, 20, 0, 0, KEY_SAVE, 0, strSave },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  30, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  50, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[1] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  70, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[2] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  90, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[3] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 110, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[4] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 130, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[5] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 150, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[6] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 170, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[7] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 190, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[8] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 210, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[9] },

	{ GADG_STAT_TEXT, 20,  30, 0, 0, 0, 0, 0, 0, 0, 0, "F1" },
	{ GADG_STAT_TEXT, 20,  50, 0, 0, 0, 0, 0, 0, 0, 0, "F2" },
	{ GADG_STAT_TEXT, 20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "F3" },
	{ GADG_STAT_TEXT, 20,  90, 0, 0, 0, 0, 0, 0, 0, 0, "F4" },
	{ GADG_STAT_TEXT, 20, 110, 0, 0, 0, 0, 0, 0, 0, 0, "F5" },
	{ GADG_STAT_TEXT, 20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "F6" },
	{ GADG_STAT_TEXT, 20, 150, 0, 0, 0, 0, 0, 0, 0, 0, "F7" },
	{ GADG_STAT_TEXT, 20, 170, 0, 0, 0, 0, 0, 0, 0, 0, "F8" },
	{ GADG_STAT_TEXT, 20, 190, 0, 0, 0, 0, 0, 0, 0, 0, "F9" },
	{ GADG_STAT_TEXT, 20, 210, 0, 0, 0, 0, 0, 0, 0, 0, "F10" },
	{ GADG_STAT_TEXT, 20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Macro names:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate customMacroDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 270, 260, customMacroGadgets, NULL
};

#elif GERMAN

static GadgetTemplate customMacroGadgets[] = {
	{ GADG_PUSH_BUTTON, 105, -30, 0, 0,  80, 20, 0, 0, 'B', 0, strUse },
	{ GADG_PUSH_BUTTON, 195, -30, 0, 0,  80, 20, 0, 0, KEY_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON,  15, -30, 0, 0,  80, 20, 0, 0, KEY_SAVE, 0, strSave },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  30, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  50, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[1] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  70, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[2] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  90, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[3] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 110, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[4] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 130, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[5] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 150, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[6] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 170, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[7] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 190, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[8] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 210, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[9] },

	{ GADG_STAT_TEXT, 20,  30, 0, 0, 0, 0, 0, 0, 0, 0, "F1" },
	{ GADG_STAT_TEXT, 20,  50, 0, 0, 0, 0, 0, 0, 0, 0, "F2" },
	{ GADG_STAT_TEXT, 20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "F3" },
	{ GADG_STAT_TEXT, 20,  90, 0, 0, 0, 0, 0, 0, 0, 0, "F4" },
	{ GADG_STAT_TEXT, 20, 110, 0, 0, 0, 0, 0, 0, 0, 0, "F5" },
	{ GADG_STAT_TEXT, 20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "F6" },
	{ GADG_STAT_TEXT, 20, 150, 0, 0, 0, 0, 0, 0, 0, 0, "F7" },
	{ GADG_STAT_TEXT, 20, 170, 0, 0, 0, 0, 0, 0, 0, 0, "F8" },
	{ GADG_STAT_TEXT, 20, 190, 0, 0, 0, 0, 0, 0, 0, 0, "F9" },
	{ GADG_STAT_TEXT, 20, 210, 0, 0, 0, 0, 0, 0, 0, 0, "F10" },
	{ GADG_STAT_TEXT, 20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Makro Namen:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate customMacroDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 290, 260, customMacroGadgets, NULL
};

#elif FRENCH

static GadgetTemplate customMacroGadgets[] = {
	{ GADG_PUSH_BUTTON, 105, -30, 0, 0,  60, 20, 0, 0, 'O', 0, strUse },
	{ GADG_PUSH_BUTTON, 185, -30, 0, 0,  60, 20, 0, 0, KEY_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON,  25, -30, 0, 0,  60, 20, 0, 0, KEY_SAVE, 0, strSave },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  30, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  50, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[1] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  70, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[2] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  90, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[3] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 110, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[4] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 130, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[5] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 150, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[6] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 170, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[7] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 190, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[8] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 210, 0, 0, 200, 11, 0, 0, 0, 0, macroNames2[9] },

	{ GADG_STAT_TEXT, 20,  30, 0, 0, 0, 0, 0, 0, 0, 0, "F1" },
	{ GADG_STAT_TEXT, 20,  50, 0, 0, 0, 0, 0, 0, 0, 0, "F2" },
	{ GADG_STAT_TEXT, 20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "F3" },
	{ GADG_STAT_TEXT, 20,  90, 0, 0, 0, 0, 0, 0, 0, 0, "F4" },
	{ GADG_STAT_TEXT, 20, 110, 0, 0, 0, 0, 0, 0, 0, 0, "F5" },
	{ GADG_STAT_TEXT, 20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "F6" },
	{ GADG_STAT_TEXT, 20, 150, 0, 0, 0, 0, 0, 0, 0, 0, "F7" },
	{ GADG_STAT_TEXT, 20, 170, 0, 0, 0, 0, 0, 0, 0, 0, "F8" },
	{ GADG_STAT_TEXT, 20, 190, 0, 0, 0, 0, 0, 0, 0, 0, "F9" },
	{ GADG_STAT_TEXT, 20, 210, 0, 0, 0, 0, 0, 0, 0, 0, "F10" },
	{ GADG_STAT_TEXT, 20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Nom de macro:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate customMacroDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 270, 260, customMacroGadgets, NULL
};

#elif SWEDISH
#endif

/*
	User request dialogs
*/

static GadgetTemplate userReq1Gadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, KEY_OK, 0, strOK },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReq1DialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 85, userReq1Gadgets, NULL
};

static GadgetTemplate userReq2Gadgets[] = {
	{ GADG_PUSH_BUTTON,  40, -30, 0, 0, 60, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, 140, -30, 0, 0, 60, 20, 0, 0, KEY_CANCEL, 0, strCancel },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReq2DialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 85, userReq2Gadgets, NULL
};

static GadgetTemplate userReq3Gadgets[] = {
	{ GADG_PUSH_BUTTON,  30, -30, 0, 0, 60, 20, 0, 0, KEY_YES, 0, strYes },
	{ GADG_PUSH_BUTTON, 210, -30, 0, 0, 60, 20, 0, 0, KEY_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON, 120, -30, 0, 0, 60, 20, 0, 0, KEY_NO, 0, strNo },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReq3DialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 90, userReq3Gadgets, NULL
};

static GadgetTemplate userReqTextGadgets[] = {
	{ GADG_PUSH_BUTTON,  55, -30, 0, 0,  70, 20, 0, 0, KEY_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, 175, -30, 0, 0,  70, 20, 0, 0, KEY_CANCEL, 0, strCancel },

	{ GADG_EDIT_TEXT, 22, 55, 0, 0, 256, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReqTextDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 110, userReqTextGadgets, NULL
};

/*
 *	 Cancel print dialog
 */

static GadgetTemplate cancelPrGadgets[] = {
#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT, 60, 15, 0, 0,  0,  0, 0, 0, 0, 0, "Printing in progress." },
#elif GERMAN
	{ GADG_STAT_TEXT, 60, 15, 0, 0,  0,  0, 0, 0, 0, 0, "Druckvorgang l�uft." },
#elif FRENCH
	{ GADG_STAT_TEXT, 60, 15, 0, 0,  0,  0, 0, 0, 0, 0, "Impression en cours." },
#elif SWEDISH
#endif
	{ GADG_PUSH_BUTTON,	 -80, 50, 0, 0, 60, 20, 0, 0, KEY_CANCEL, 0, strCancel },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_NOTE },
	{ GADG_ITEM_NONE }
};

DialogTemplate cancelPrDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 80, cancelPrGadgets, NULL
};

/*
 *	 Next Page dialog
 */

static GadgetTemplate nextPageGadgets[] = {
	{ GADG_PUSH_BUTTON,	290, 10, 0, 0, 60, 20, 0, 0,KEY_OK,0, strOK },
	{ GADG_PUSH_BUTTON,	290, 40, 0, 0, 60, 20, 0, 0,KEY_CANCEL,0, strCancel },
#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT,    60, 15, 0, 0,  0,  0, 0, 0, 0, 0, "Please insert the next\nsheet of paper" },
#elif GERMAN
	{ GADG_STAT_TEXT,    60, 15, 0, 0,  0,  0, 0, 0, 0, 0, "Bitte das n�chste Blatt\nPapier einlegen" },
#elif FRENCH
	{ GADG_STAT_TEXT,    60, 15, 0, 0,  0,  0, 0, 0, 0, 0, "Ins�rez une autre feuille\nde papier, SVP." },
#elif SWEDISH
#endif
	{ GADG_STAT_STDIMAGE, 10, 15, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

DialogTemplate nextPageDialogT = {
	DLG_TYPE_ALERT, 0, -1, 30, 370, 70, nextPageGadgets, NULL
};

/*
 * Dialog list
 */

DlgTemplPtr dlgList[] = {
	&aboxDialogT,	&destFileDialogT,		&customMacroDialogT,
	&userReq1DialogT,	&userReq2DialogT,		&userReq3DialogT,	&userReqTextDialogT,
	&cancelPrDialogT,	&nextPageDialogT
};

/*
 * Requester list
 */
 
ReqTemplPtr reqList[] = {
	&scanRequestT,	&errorRequestT,		&warnRequestT,	&skipRequestT,
	&backOptsRequestT,&backOpts2RequestT,	&restOptsRequestT,	&restOpts2RequestT,
	&catOptsRequestT,	&logOptsRequestT,
	&prefsRequestT,	&tapeControlRequestT,&pageRequestT,	&printRequestT,
	&passProRequestT, &passAskRequestT,		&tagRequestT,		&tapeOptsRequestT,
	&scsiInfoRequestT,&renameRequestT,		&rewindRequestT,	&tapeWaitRequestT,
	&buffOptsRequestT,&getSessInfoReqT,		&setSessInfoReqT,	&seekRequestT
};
