/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Gadget routines
 */

#include <exec/types.h>
#include <dos/dos.h>

#include <Toolbox/DateTime.h>
#include <Toolbox/List.h>
#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include "QB.h"
#include "Proto.h"
#include "DlogDefs.h"

/*
 *	External variables
 */

extern DirLevelPtr	curLevel;
extern BOOL			tagsDone;
extern ScrollListPtr	scrollList;
extern UBYTE		mode, operation, filterListMode;

extern RequestPtr	requester;
extern WindowPtr	cmdWindow;
extern MsgPortPtr	mainMsgPort;
extern ReqTemplPtr	reqList[];
extern DlgTemplPtr	dlgList[];
extern TextChar	strArchive0[], strArchive1[], strTag[], strUntag[];
extern TextChar	strSaveFilter[], strOpenFilter[];
extern TextChar	strBuff[];
extern DirLevel	rootLevel;
extern WORD			intuiVersion;
extern ULONG		selFiles, selBytes;
extern ScreenPtr	screen;
extern Ptr			fib;
extern ListHeadPtr 	backupFilterList, restoreFilterList;
extern TextChar	strBackupFilter[], strRestoreFilter[];
extern ProgPrefs	prefs;

/*
 * Local prototypes
 */
 
static void	OnOffFileItem(DirFibPtr, WORD, BOOL);
static WORD	NextFileListItem(BOOL, WORD);
static BOOL	TagFilterRequestFilter(IntuiMsgPtr, WORD *);
static WORD	MonthNum(TextPtr);
static BOOL	GetDayMonthYear(WORD, WORD *, WORD *, WORD *);
static void	RefreshDateArrows(WORD, WORD);
static void	ConvertToDateStamp(struct DateStamp *, UWORD, UWORD, UWORD);
static BOOL	ProcessTagFilter(BOOL, BOOL, BOOL, BOOL, BOOL, BOOL, struct DateStamp *, struct DateStamp *);
static BOOL	SaveFilter(ListHeadPtr, TextPtr);
static void	UpdateFilterList(ListHeadPtr);

static UBYTE daysPerMonth[] = {
	31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

#define GADDOWN_FLAG			64
#define GADUP_FLAG			128
#define DAYS_IN_YEAR(yr)	((yr % 4) ? 365 : ((yr % 400) ? 366 : 365))

/*
 * Call this whenever a selection needs to be tagged. Inputs are TRUE/FALSE for
 * whether it should be tagged or untagged, and whether all should be that way.
 * It assumes the first item to do is the first selected item in "scrollList".
 */
 
void DoTag(BOOL tagOffOn, BOOL altOn)
{
	register WORD prevItem, listItem;
	register DirFibPtr dirFib;
	
	prevItem = listItem = -1;
	dirFib = curLevel->dl_ChildPtr;
	while( (listItem = NextFileListItem(altOn, listItem)) != -1 ) {
		while( ++prevItem < listItem ) {
			dirFib = dirFib->df_Next;
		}
		OnOffFileItem(dirFib, listItem, tagOffOn);

		if( (dirFib->df_Flags & FLAG_DIR_MASK) && (dirFib->df_SubLevel != NULL) ) {
			SelectDir(((DirLevelPtr)dirFib->df_SubLevel)->dl_ChildPtr, tagOffOn);
		}
		prevItem--;
	}
	DisplayStats(FALSE);
}

/*
 * Tag filter filter (spam, spam, spam, spam...)
 */
 
static BOOL TagFilterRequestFilter(intuiMsg, item)
register IntuiMsgPtr intuiMsg;
WORD *item;
{
	register WORD itemHit;
	register UWORD code;
	register ULONG class;
	WORD modifier;
	register RequestPtr req = requester;
	WORD saveItem;
	WORD monthItem;
	struct DateStamp currDateStamp;
	
	class = intuiMsg->Class;
	code = intuiMsg->Code;
	modifier = intuiMsg->Qualifier;
	if( intuiMsg->IDCMPWindow == cmdWindow ) {
		switch(class) {
		case RAWKEY:
			if (code == CURSORUP || code == CURSORDOWN) {
				ReplyMsg((MsgPtr) intuiMsg);
				SLCursorKey(scrollList, code);
				*item = PATTERN_LIST;
				return (TRUE);
			}
			break;
		case GADGETUP:
		case GADGETDOWN:
			itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
			if (itemHit == PATTERN_LIST || itemHit == PATTERN_PROP ||
				itemHit == PATTERN_UPARROW || itemHit == PATTERN_DNARROW) {
				SLGadgetMessage(scrollList, mainMsgPort, intuiMsg);
				*item = itemHit;
				return (TRUE);
			}
			if( class == GADGETUP ) {
				switch(itemHit) {
				case PATTERN_TEXT:
					if( code == 0 && ((modifier & IEQUALIFIER_NUMERICPAD) == 0 ) ) {
						DepressGadget(GadgetItem(req->ReqGadget, TAG_ADD_BUTTON), cmdWindow, req);
						ReplyMsg((MsgPtr) intuiMsg);
						*item = TAG_ADD_BUTTON;
						return(TRUE);
					}
					break;
				case BEFORE_TEXT1:
				case BEFORE_TEXT2:
				case BEFORE_TEXT3:
				case AFTER_TEXT1:
				case AFTER_TEXT2:
				case AFTER_TEXT3:
					monthItem = itemHit >= AFTER_TEXT1 ? AFTER_TEXT2 : BEFORE_TEXT2;
					GetEditItemText(req->ReqGadget, monthItem, strBuff);
					if( MonthNum(strBuff) == 0 ) {
/*
	If an invalid month, beep the user and replace the month text with
	the text of the current month.
*/
						ErrBeep();
						DateStamp(&currDateStamp);
						XDateString(&currDateStamp, 0, strBuff);
						strBuff[6] = 0;
						SetEditItemText(req->ReqGadget, monthItem, cmdWindow, req, &strBuff[3]);
					}
					if( intuiMsg->Code == 0 && ((modifier & IEQUALIFIER_NUMERICPAD) == 0 ) ) {
						saveItem = itemHit;
						if( modifier & SHIFTKEYS ) {
							if( itemHit == BEFORE_TEXT1 || itemHit == AFTER_TEXT1 ) {
							itemHit += 3;
						}	
						itemHit--;
						} else {
							if( itemHit == BEFORE_TEXT3 || itemHit == AFTER_TEXT3 ) {
								itemHit -= 3;
							}
							itemHit++;
						}
						ActivateGadget(GadgetItem(req->ReqGadget, itemHit), cmdWindow, req);
						Delay(5);
						ReplyMsg((MsgPtr) intuiMsg);
						*item = saveItem;
						return(TRUE);
					}
					break;
				case BEFORE_UPARROW:
				case BEFORE_DNARROW:
				case AFTER_UPARROW:
				case AFTER_DNARROW:
					ReplyMsg((MsgPtr) intuiMsg);
					*item = itemHit | GADUP_FLAG;
					return(TRUE);
				}
			} else {
				switch(itemHit) {	
				case PATTERN_LIST:
				case PATTERN_UPARROW:
				case PATTERN_DNARROW:
				case PATTERN_PROP:
					SLGadgetMessage(scrollList, mainMsgPort, intuiMsg);
					*item = itemHit;
					return(TRUE);			
				case BEFORE_TEXT1:
				case BEFORE_TEXT2:
				case BEFORE_TEXT3:
				case AFTER_TEXT1:
				case AFTER_TEXT2:
				case AFTER_TEXT3:
					ReplyMsg((MsgPtr) intuiMsg);
					*item = itemHit | GADDOWN_FLAG;
					return(TRUE);
				case BEFORE_UPARROW:
				case BEFORE_DNARROW:
				case AFTER_UPARROW:
				case AFTER_DNARROW:
					ReplyMsg((MsgPtr) intuiMsg);
					*item = itemHit;
					return(TRUE);
				}
			}
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 * Return month #, or zero if not a month name.
 */
 
static WORD MonthNum(TextPtr monthName)
{
	TextChar buff[40];
	register WORD i;
		
	for( i = 1 ; i <= 12 ; i++ ) {
		MonthName(i, TRUE, buff);
		if( CmpString( monthName, buff, 3, 3, FALSE ) == 0 ) {
			return(i);
		}
	}
	return(0);
}

/*
 * Converts day, month, and year into a DateStamp structure
 */
 
static void ConvertToDateStamp(struct DateStamp *dateStamp, register UWORD days,
			UWORD month, register UWORD year)
{
	register WORD i;
	
	if( year < 77 ) {
		year += 100;
	}
	year += 1900;
	for( i = 1978 ; i < year ; i++ ) {
		days += DAYS_IN_YEAR(i);
	}
	for( i = 0 ; i < (month-1) ; i++ ) {
		days += daysPerMonth[i];
	}
/*
 * Unless we're into a leap year past Feb, we're going to have subtract one
 * to compensate for the origin of "days" being one rather than zero.
 */
	if( !( (month > 2) && (((year % 4) == 0) && (year % 400)) ) ) {
		--days;
	}
	dateStamp->ds_Days = days;
	dateStamp->ds_Minute = 0;
	dateStamp->ds_Tick = 0;
}
	
/*
 * Return day, month, and year given the trio of string gadgets in "itemSet",
 * where they are ordered such.
 * Returns a boolean flag indicating whether the strings made sense.
 */
  
static BOOL GetDayMonthYear(register WORD itemSet, register WORD *day, WORD *monthVar, WORD *year)
{
	TextChar dateBuff[GADG_MAX_STRING];
	BOOL success = FALSE;
	GadgetPtr gadgList = requester->ReqGadget;
	register WORD month;
	
	GetEditItemText(gadgList, itemSet++, dateBuff);
	*day = StringToNum(dateBuff);
	if( *day == 0 ) {
		*day++;
	}
	GetEditItemText(gadgList, itemSet++, dateBuff);
	month = StringToNum(dateBuff);
	if( month == 0 ) {
		month = MonthNum(dateBuff);
		if( month == 0 ) {
			month++;
		}
	}
	*monthVar = month;
	GetEditItemText(gadgList, itemSet, dateBuff);
	*year = StringToNum(dateBuff);
	
	if( *day && month && (*day <= daysPerMonth[month-1]) && (month < 13) && *year < 100 ) {
		success = TRUE;
	}
	return(success);
}

/*
 * Update filter list
 */

static void UpdateFilterList(ListHeadPtr list)
{
	register WORD i;
	register WORD num;
	register TextChar buff[GADG_MAX_STRING];
	 
	DisposeListItems(list);
	num = SLNumItems(scrollList);
	for( i = 0 ; i < num ; ) {
		SLGetItem(scrollList, i, buff);
		InsertListItem(list, buff, strlen(buff), ++i);
	}
}

/*
 * Handle tag filtering - by archive bit, date, wildcard, etc.
 */
 
BOOL DoTagFilterRequest()
{
	register WORD item;
	register WORD temp;
	register WORD num;
	register WORD i;
	register WORD day;
 	register GadgetPtr gadgList;
	register DialogPtr window = cmdWindow;
	register RequestPtr req;
	WORD include;
	BOOL done = FALSE;
	BOOL success = FALSE;
	ScrollListPtr saveList = scrollList;
	TextChar tempBuff[GADG_MAX_STRING];
	TextChar buff[4];
	struct DateStamp beforeDateStamp, afterDateStamp;
	WORD currField = 0;
	WORD strItem;
	WORD incr;
	WORD origDay, month, year, origYear;
	BOOL holdDelay = FALSE;
	WORD arrowHit = 0;
	WORD delay = 0;
	WORD resetDay;
	BOOL rebuild;
	BOOL recalcDate = TRUE;
	TextPtr str;
	SFReply sfReply;
	/* BOOL changed = FALSE;*/
	TextPtr filterName;
	TagParams tagOpts = prefs.TagOpts;
	ListHeadPtr list = (mode == MODE_BACKUP) ? backupFilterList : restoreFilterList;
/*
	Get requester
*/
	BeginWait();
/*
	reqList[REQ_TAGFILTER].Gadgets[OK_BUTTON].Info = strTag;
*/
	req = GetRequest(reqList[REQ_TAGFILTER], window, FALSE);
	if( req == NULL ) {
		Error(ERR_NO_MEM);
	} else {
		requester = req;
		gadgList = req->ReqGadget;
		scrollList = NewScrollList(SL_SINGLESELECT);
		if( scrollList != NULL ) {
			InitScrollList(scrollList, GadgetItem(gadgList, PATTERN_LIST), window, req);
			AutoActivateEnable(FALSE);
			ShowRequest(req, window);
			AutoActivateEnable(TRUE);
			SLDrawBorder(scrollList);
			OutlineOKButton(window);
			OffGList(GadgetItem(gadgList, TAG_ADD_BUTTON), window, req, 2);
/*
	Set current settings
*/
			filterName = mode == MODE_BACKUP ? strBackupFilter : strRestoreFilter;

			num = NumListItems(list);
			for( i = 0 ; i < num ; i++ ) {
				str = GetListItem(list, i);
				SLAddItem(scrollList, str, strlen(str), i);
			}
/*
	Handle requester
*/
			item = include = tagOpts.IncludeMode ? INCLUDE_RADBTN : EXCLUDE_RADBTN;
			if( tagOpts.DoArchive )
				GadgetOn(window, TAG_ARCHIVE_BOX);
			if( tagOpts.DoBefore )
				GadgetOn(window, TAG_BEFORE_BOX);
			if( tagOpts.DoAfter )
				GadgetOn(window, TAG_AFTER_BOX);
			if( tagOpts.DoPattern )
				GadgetOn(window, PATTERN_BOX);
			if( tagOpts.DoLocal )
				GadgetOn(window, TAG_LOCAL_BOX);
			do {
				/*
				EnableGadgetItem(gadgList, TAG_SAVE_BUTTON, window, req, changed);
				*/
				if( recalcDate ) {
					recalcDate = FALSE;
					DateStamp(&beforeDateStamp);
					XDateString(&beforeDateStamp, 0, tempBuff);
					*((LONG *) &buff[0]) = 0L;
					*((UWORD *) &buff[0]) = *((UWORD *) &tempBuff[0]);
					/*day = StringToNum(buff);*/
					SetEditItemText(gadgList, BEFORE_TEXT1, window, req, buff);
					SetEditItemText(gadgList, AFTER_TEXT1, window, req, buff);
					buff[0] = tempBuff[7];
					buff[1] = tempBuff[8];
					month = StringToNum(buff);
					SetEditItemText(gadgList, BEFORE_TEXT3, window, req, buff);
					SetEditItemText(gadgList, AFTER_TEXT3, window, req, buff);
					BlockMove( &tempBuff[3], buff, 3);
					origYear = year = StringToNum(buff);
					SetEditItemText(gadgList, BEFORE_TEXT2, window, req, buff);
					SetEditItemText(gadgList, AFTER_TEXT2, window, req, buff);
				}
				strItem = -1;
				incr = 0;
				GetEditItemText(gadgList, PATTERN_TEXT, strBuff);

				if( item == -1 ) {				/* INTUITICKS message? */
					EnableGadgetItem(gadgList, TAG_ADD_BUTTON, window, req, (strlen(strBuff) != 0) && (num < MAX_LIST_ITEMS) );
					if( arrowHit ) {				/* Is arrow being held down? */
						if( holdDelay = GadgetSelected(gadgList, arrowHit) ) {
							if( delay > 0 ) {
								delay--;
							} else {				/* Emulate an arrow gadget down event */
								item = arrowHit;
							}
						} else {
							arrowHit = 0;		/* "Forget" the arrow once it's released */
						}
					} else {
						resetDay = 0;			/* "Forget" this value on NULL events */
					}
				}
				
				switch(item) {
				case OK_BUTTON:
					success = TRUE;
					if( tagOpts.DoBefore ) {
						if( GetDayMonthYear(BEFORE_TEXT1, &origDay, &month, &origYear) ) {
							ConvertToDateStamp(&beforeDateStamp, origDay, month, origYear);
						} else {
							success = FALSE;
						}
					}
					if( tagOpts.DoAfter ) {
						if( GetDayMonthYear(AFTER_TEXT1, &origDay, &month, &origYear) ) {
							ConvertToDateStamp(&afterDateStamp, origDay, month, origYear);
						} else {
							success = FALSE;
						}
					}
					if( !success ) {
						recalcDate = TRUE;	/* Validity check failed, don't leave yet */
						Error(ERR_INVALID_DATE);
						break;
					}
				case CANCEL_BUTTON:
					done = TRUE;
					break;
				case INCLUDE_RADBTN:
				case EXCLUDE_RADBTN:
					GadgetOff(window, include);
					include = item;
					tagOpts.IncludeMode = include == INCLUDE_RADBTN;
					SetButtonItem(gadgList, TAG_ARCHIVE_BOX, window, req,
						tagOpts.IncludeMode ? strArchive0 : strArchive1, KEY_TAG_ARC);
					str = tagOpts.IncludeMode ? strTag : strUntag;
					SetButtonItem(gadgList, OK_BUTTON, window, req, str, KEY_OK);
					GadgetOn(window, item);
					break;
				case TAG_ADD_BUTTON:
					if( strBuff[0] ) {
						temp = SLNextSelect(scrollList, -1);
						if( temp != -1 ) {
							SLSelectItem(scrollList, temp, FALSE);
						}
						for( i = 0 ; i < num ; i++ ) {
							SLGetItem(scrollList, i, tempBuff);
							if( strcmp(tempBuff, strBuff) == 0 ) {
								SLSelectItem(scrollList, i, TRUE);
								SLAutoScroll(scrollList, i);
								break;
							}
						}
						if( i == num ) {
							if( num < MAX_LIST_ITEMS ) {
								SLAddItem(scrollList, strBuff, (WORD) strlen(strBuff), num);
								num = SLNumItems(scrollList);
								SLAutoScroll(scrollList, num-1);
								strItem = PATTERN_TEXT;	
								/* changed = TRUE; */
							}
							SetEditItemText(gadgList, PATTERN_TEXT, window, req, NULL);
						}
					}
					tagOpts.DoPattern = TRUE;
					SetGadgetItemValue(gadgList, PATTERN_BOX, window, req, TRUE);
					break;
				case TAG_DEL_BUTTON:
					temp = SLNextSelect(scrollList, -1);
					num = SLNumItems(scrollList);
					if( temp != -1 ) {
						SLDoDraw(scrollList, FALSE);
						SLRemoveItem(scrollList, temp);
						if( temp >= --num ) {
							temp--;
						}
						/* changed = TRUE; */
						SLSelectItem(scrollList, temp, TRUE);
						SLAutoScroll(scrollList, temp);
						SLDoDraw(scrollList, TRUE);
					}
				case PATTERN_LIST:
					temp = SLNextSelect(scrollList, -1);
					EnableGadgetItem(gadgList, TAG_DEL_BUTTON, window, req, temp != -1);
					strBuff[0] = 0;
					if( temp != -1 ) {
						SLGetItem(scrollList, temp, strBuff);
					}
					SetEditItemText(gadgList, PATTERN_TEXT, window, req, strBuff);
					if( item == PATTERN_LIST ) {
						strItem = PATTERN_TEXT;
					}
					break;
				case TAG_BEFORE_BOX:
					ToggleCheckbox(&tagOpts.DoBefore, TAG_BEFORE_BOX, window);
					if( tagOpts.DoBefore ) {
						strItem = BEFORE_TEXT1;
					}
					break;
				case TAG_AFTER_BOX:
					ToggleCheckbox(&tagOpts.DoAfter, TAG_AFTER_BOX, window);
					if( tagOpts.DoAfter ) {
						strItem = AFTER_TEXT1;
					}
					break;
				case PATTERN_BOX:
					ToggleCheckbox(&tagOpts.DoPattern, item, window);
					if( tagOpts.DoPattern ) {
						strItem = PATTERN_TEXT;
					}
					break;
				case TAG_ARCHIVE_BOX:
					ToggleCheckbox(&tagOpts.DoArchive, item, window);
					break;
				case TAG_LOCAL_BOX:
					ToggleCheckbox(&tagOpts.DoLocal, item, window);
					break;
				case AFTER_TEXT1 | GADDOWN_FLAG:
				case AFTER_TEXT2 | GADDOWN_FLAG:
				case AFTER_TEXT3 | GADDOWN_FLAG:
					tagOpts.DoAfter = TRUE;
					SetGadgetItemValue(gadgList, TAG_AFTER_BOX, window, req, TRUE);
					if( currField != item - (AFTER_TEXT1 | GADDOWN_FLAG) ) {
						currField = item - (AFTER_TEXT1 | GADDOWN_FLAG);
						resetDay = 0;
					}
					RefreshDateArrows(AFTER_UPARROW, currField);
					break;
				case BEFORE_TEXT1 | GADDOWN_FLAG:
				case BEFORE_TEXT2 | GADDOWN_FLAG:
				case BEFORE_TEXT3 | GADDOWN_FLAG:
					tagOpts.DoBefore = TRUE;				
					SetGadgetItemValue(gadgList, TAG_BEFORE_BOX, window, req, TRUE);
					if( currField != item - (BEFORE_TEXT1 | GADDOWN_FLAG) ) {
						currField = item - (BEFORE_TEXT1 | GADDOWN_FLAG);
						resetDay = 0;
					}
					RefreshDateArrows(BEFORE_UPARROW, currField); 
					break;
				case BEFORE_UPARROW | GADUP_FLAG:
				case BEFORE_DNARROW | GADUP_FLAG:
				case AFTER_UPARROW | GADUP_FLAG:
				case AFTER_DNARROW | GADUP_FLAG:
					if( arrowHit ) {
						strItem = currField + (item >= (AFTER_UPARROW | GADUP_FLAG) ? AFTER_TEXT1 : BEFORE_TEXT1);
					}
					break;
				case AFTER_TEXT1:
				case AFTER_TEXT2:
				case AFTER_TEXT3:
				case BEFORE_TEXT1:
				case BEFORE_TEXT2:
				case BEFORE_TEXT3:
					item = (item > BEFORE_TEXT3) ? (AFTER_UPARROW | GADUP_FLAG) : (BEFORE_UPARROW | GADUP_FLAG);
				case BEFORE_UPARROW:
				case BEFORE_DNARROW:
				case AFTER_UPARROW:
 				case AFTER_DNARROW:
					if( item & GADUP_FLAG ) {
						arrowHit = 0;
						item &= ~GADUP_FLAG;
					} else {
						incr = (item == BEFORE_DNARROW || item == AFTER_DNARROW) ? -1 : 1;
						arrowHit = item;
					}
					if( !holdDelay ) {
						delay = 5;	
					}
					temp = item >= AFTER_UPARROW ? AFTER_TEXT1 : BEFORE_TEXT1;
					(void) GetDayMonthYear(temp, &origDay, &month, &origYear);
					day = origDay;
					year = origYear;
					switch(currField) {
					case 0:
/*
	If this is a verify, then bypass day adjustment
*/
						if( incr ) {
							day += incr;
							if( ( day <= daysPerMonth[month-1] ) &&
								 ( day > 0 ) ) {
								NumToString(day, tempBuff);
								SetEditItemText(gadgList, temp, window, req, tempBuff);
								break;
							}
							if( day ) {
								day = 1;
							} else if( (month == 1) && (year == 78) ) {
								break;
							}
						}
					case 1:
						i = 1;
						month += incr;
						if( incr < 0 ) {
							if( month < 1 ) {
								month = 12;
								i = 0;
							}
						} else {
							if( month > 12 ) {
								month = 1;
								i = 0;
							}
						}
/*
	Because some months have less days than others, if we are cycling through
	the months, then if the user started out with a number that is greater than
	some of the other months, make sure it is preserved, so for example, if it
	starts January 31 and cycles through February and ends up in March, it is
	still 31 and not constrained to 28.
*/						
						if( currField ) {
							if( resetDay == 0 ) {
								resetDay = day;
							}
							day = MIN(resetDay, daysPerMonth[month-1]);
						} else if( (day < 1) || (day > daysPerMonth[month-1]) ) {
							day = daysPerMonth[month-1];
						}
						if( day != origDay ) {
							NumToString(day, tempBuff);
							SetEditItemText(gadgList, temp, window, req, tempBuff);
						}
						if( incr ) {
							MonthName(month, TRUE, tempBuff);
							SetEditItemText(gadgList, temp+1, window, req, tempBuff);
						}
						if( i ) {
							break;
						}
					case 2:
						if( incr < 0 ) {
							if( year != 78 ) {
								if( year == 0 ) {
								year = 100;
								}
								year--;
							}
						} else {
							if( year != 77 ) {
								year += incr;		/* If incr=0, don't adjust year */
								if( year > 99 ) {
									year = 0;
								}
							}
						}
						NumToString(year, tempBuff);
						if( year < 10 ) {			/* Ensure two digits in year */
							tempBuff[2] = 0;
							tempBuff[1] = tempBuff[0];
							tempBuff[0] = '0';
						}
						SetEditItemText(gadgList, temp+2, window, req, tempBuff);
						break;
					}
					RefreshDateArrows( item >= AFTER_UPARROW ? AFTER_UPARROW : BEFORE_UPARROW, currField);
					break;
				case TAG_SAVE_BUTTON:
					SFPPutFile(screen, mainMsgPort, strSaveFilter, filterName,
						DialogFilter, NULL, dlgList[DLG_DESTFILE], &sfReply);
					if( sfReply.Result == SFP_OK ) {
						UpdateFilterList(list);
						SaveFilter(list, sfReply.Name);
						/* changed = FALSE; */
					}
					break;
				case TAG_LOAD_BUTTON:
					if( DoStdGetFile(&sfReply, strOpenFilter) ) {
						/* changed = FALSE; */
						if( LoadFilter(list, sfReply.Name) ) {
							SLRemoveAll(scrollList);
							num = NumListItems(list);
							for( i = 0 ; i < num ; i++ ) {
								str = GetListItem(list, i);
								SLAddItem(scrollList, str, strlen(str), i);
							}
						}
					}
					break;
				default:
					break;
				}
				WaitPort(mainMsgPort);
				item = CheckRequest(mainMsgPort, window, TagFilterRequestFilter);
				
				if( strItem != -1 ) {
					ActivateGadget(GadgetItem(gadgList, strItem), window, req);
					Delay(5);
				}
			} while( !done );
			DestroyRequest(req);
			if( success ) {
				prefs.TagOpts = tagOpts;
				UpdateFilterList(list);
				rebuild = ProcessTagFilter(tagOpts.IncludeMode, tagOpts.DoLocal, tagOpts.DoArchive,
						 tagOpts.DoBefore, tagOpts.DoAfter, tagOpts.DoPattern, 
						 &beforeDateStamp, &afterDateStamp);
				DisplayStats(FALSE);
			}
			DisposeScrollList(scrollList);
			scrollList = saveList;
			if( rebuild ) {
				BuildFileList(NULL);
			}
		}
	}
	EndWait();
	return(success);
}

/*
 * Refresh the enable status of the up/down arrows of the date boxes, since
 * you are not permitted to go back before 1978 or after 2077.
 */
 
static void RefreshDateArrows(WORD item, WORD currField)
{
	WORD day, month, year;
	register BOOL flag = TRUE;
	register RequestPtr req = requester;
	
	(void) GetDayMonthYear( item == BEFORE_UPARROW ? BEFORE_TEXT1 : AFTER_TEXT1, &day, &month, &year);	
	if( year == 77 ) {
		switch(currField) {
		case 0:
			if( day < daysPerMonth[month-1] ) {
				break;
			}
		case 1:
			if( month < 12 ) {
				break;
			}
		default:
			flag = FALSE;
			break;
		}
	}
	EnableGadgetItem(req->ReqGadget, item++, cmdWindow, req, flag);
	flag = TRUE;
	if( year == 78 ) {
		switch(currField) {
		case 0:
			if( day > 1 )
				break;
			case 1:
			if( month > 1 )
				break;
			default:
				flag = FALSE;
				break;
		}
	}
	EnableGadgetItem(req->ReqGadget, item, cmdWindow, req, flag);
	return;
}

/*
 * Process filter
 */
 
BOOL TagFilter(BOOL include, BOOL local)
{
	return( ProcessTagFilter(include, local, FALSE, FALSE, FALSE, TRUE, NULL, NULL) );
}

/*
 * Tag all of the appropriate items specified
 */
 
static BOOL ProcessTagFilter(register BOOL include, register BOOL local, BOOL archive, BOOL before, 
		BOOL after, BOOL pattern, struct DateStamp *beforeDateStamp, struct DateStamp *afterDateStamp)
{
	register DirFibPtr dirFib;
	register DirLevelPtr level;
	register WORD i;
	register BOOL change;
	BOOL rebuild = FALSE;
	register WORD num;
	DirLevelPtr saveCurLevel;
	TextPtr text;
	ListHeadPtr list = (mode == MODE_BACKUP) ? backupFilterList : restoreFilterList;

	SetStdPointer(cmdWindow, POINTER_WAIT);	
	saveCurLevel = curLevel;	
	level = local ? curLevel : &rootLevel;
	dirFib = level->dl_ChildPtr;
	while( level != NULL ) {
		if( dirFib == NULL ) {
			if( local ) {
				level = NULL;
			} else {
				curLevel = level;
				(void) UpdateDirStatus(FALSE);
				level = level->dl_ParLevel;
				if( level != NULL ) {
					dirFib = level->dl_CurFib->df_Next;
				}
			}
		} else {
			if( (dirFib->df_Flags & FLAG_DIR_MASK) && ((dirFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK)) == 0) ) {
				if( local ) {
					dirFib = dirFib->df_Next;
				} else {
					level->dl_CurFib = dirFib;
					level = (DirLevelPtr) dirFib->df_SubLevel;
					level->dl_ParFib = dirFib;
					dirFib = level->dl_ChildPtr;
				}
			} else {
				change = FALSE;
				if( archive ) {
					change = SetResetTag(include, dirFib, ((dirFib->df_Prot & FIBF_ARCHIVE) == 0) == include );
				}
				if( before && !change ) {
					change = SetResetTag(include, dirFib, beforeDateStamp->ds_Days >= dirFib->df_DateStamp.ds_Days);
				}
				if( after && !change ) {
					change = SetResetTag(include, dirFib, afterDateStamp->ds_Days <= dirFib->df_DateStamp.ds_Days);
				}
				if( pattern && !change ) {
					num = NumListItems(list);
					for( i = 0 ; (!change) && (i < num) ; i++ ) {
						text = GetListItem(list, i);
						strcpy(strBuff, text);
						change = SetResetTag(include, dirFib, MatchFilename(strBuff, dirFib->df_Name));
					}
				}
				if( change ) {
					rebuild = TRUE;
				}
				dirFib = dirFib->df_Next;
			}
		}
	}
	curLevel = saveCurLevel;
	SetArrowPointer();
	return(rebuild);
}

#ifdef NOTOOLBOX_TAG_SUPPORT
static BOOL	MatchFilenameString(TextPtr, TextPtr);
static BOOL	WildMatch(TextPtr, TextPtr);

/*
 * Determine whether the pattern name passed "matches" the filename.
 */
 		
static BOOL MatchFilenameString(TextPtr patternName, TextPtr fileName)
{
	TextChar tempBuff[GADG_MAX_STRING*2];	/* ParsePattern() requires buffer 2x size */

	if( intuiVersion >= OSVERSION_2_0 ) {
		ParsePattern(patternName, tempBuff, GADG_MAX_STRING*2);
		return( MatchPatternNoCase(tempBuff, fileName) );
	} else {
		strcpy( tempBuff, fileName );
		return( WildMatch(patternName, tempBuff) );
	}
}

/*
 * Old-style Quarterback pattern matcher. Used before AmigaDOS V2.0 came
 * around and lifted us from this burden. Should act like pre-V5.0 Quarterback
 * versions... understands '?' and '#?' in a limited way, and that's all.
 */

static BOOL WildMatch(TextPtr patternName, TextPtr fileName)
{
	register TextPtr ptr1, ptr2;
	register TextChar ch;
	register BOOL abort = FALSE;
	BOOL match = FALSE;
	register WORD diff;
	TextPtr savePtr1, savePtr2;
		
	ptr1 = patternName;
	ptr2 = fileName;
	while( !abort ) {
		ch = *ptr1++;
		if( ch == 0 ) {							/* End of wildcard string reached? */
			match = *ptr2 == 0;					/* A match only if end of filename string */
			abort = TRUE;							/* We're finished in any case. */
		} else if( ch == '?' ) {				/* Is this wild card? */
			if( *ptr2++ == 0 ) {
				abort = TRUE;
			}
		} else if( ch == '#' ) {				/* Is this Universal wild card? */
			if( (*ptr1 == 0) || (*ptr1++ != '?') ) {
				abort = TRUE;
			} else {
				diff = strlen(ptr2) - strlen(ptr1);
				if( diff < 0 ) {
					abort = TRUE;					/* Not enough to check, so not a match. */
				} else if( diff != 0 ) {		/* If not equal lengths, clip string. */
					if( diff > strlen(ptr2) ) {
						*ptr2 = 0;
					} else {
						savePtr1 = ptr1;		/* Clip string */
						savePtr2 = ptr2;
						ptr1 = ptr2;
						ptr1 += diff;
						while( *ptr2++ = *ptr1++ );
						ptr1 = savePtr1;
						ptr2 = savePtr2;
					}
				}
			}
		} else {
			if( toUpper[ch] != toUpper[*ptr2++] ) {
				abort = TRUE;						/* If null byte, or different */
			}
		}
	}
	return(match);
}
#endif

/*
 *	Save Filter file
 */

static BOOL SaveFilter(ListHeadPtr list, TextPtr fileName)
{
	BOOL success;
	register File file;
	register WORD numItems;
	register WORD i;
	register WORD len;
	register TextPtr text;
	TextChar buff[GADG_MAX_STRING];
	
	success = FALSE;
	strcpy( mode == MODE_BACKUP ? strBackupFilter : strRestoreFilter, fileName );
	if ((file = Open(fileName, MODE_NEWFILE)) != NULL) {
		
		success = TRUE;
		numItems = NumListItems(list);
		for( i = 0 ; i < numItems ; i++ ) {

			text = GetListItem(list, i);
			stccpy(buff, text, GADG_MAX_STRING-1);
			len = strlen(buff);
			buff[len++] = LF;
			
			if( Write(file, buff, len) != len ) {
				success = FALSE;
				break;
			}
		}
		Close(file);
		if (success) {
			SaveIcon(fileName, ICON_TEXT);
		}
	}
	return(success);
}

/*
 *	Load Filter file
 */

BOOL LoadFilter(ListHeadPtr list, TextPtr fileName)
{
	register struct FileInfoBlock *fibPtr = (struct FileInfoBlock *)fib;
	register ULONG i, size;
	register WORD len;
	register BOOL success;
	File file;
	BPTR lock;
	UBYTE *buff;
	register TextPtr str;
	register WORD numItems = 0;
	
	success = FALSE;
	if( (file = Open(fileName, MODE_OLDFILE)) != NULL) {
		if( (lock = Lock(fileName, ACCESS_READ)) != NULL ) {
			if( Examine(lock, fibPtr) ) {
				buff = NewPtr(fibPtr->fib_Size);
				if( buff == NULL ) {
					if( fibPtr->fib_Size ) {
						Error(ERR_NO_MEM);
					}
				} else {
					size = Read( file, buff, fibPtr->fib_Size );
					if( size == fibPtr->fib_Size ) {
						DisposeListItems(list);
						success = TRUE;
						str = buff;
						i = 0;
						len = 0;
/*
 * For each chunk that ends with a Line Feed, add a line to the scroll list.
 * Truncate each line to GADG_MAX_STRING-1 characters.
 */
						while( success && (i++ < size) ) {
							if( (str[len] == LF) || (str[len] == 0) ) {
								if( len ) {
									success = InsertListItem(list, str, MIN(GADG_MAX_STRING-1, len), numItems);
									numItems++;
								}
								str += len + 1;
								len = -1;
							}
							len++;
						}
						if( len ) {
							success = InsertListItem(list, str, MIN(GADG_MAX_STRING-1, len), numItems);
						}
					}
					DisposePtr(buff);
				}
			}
			UnLock(lock);
		}
		Close(file);
	}
	return(success);
}

/*
 * Process double-click on file or directory
 */
 
void DoDoubleClick(WORD item)
{
	register WORD i;
	register DirFibPtr dirFib;

	if( operation == OPER_SELECTION ) {
		dirFib = curLevel->dl_ChildPtr;
		for( i = 0 ; i < item && dirFib != NULL ; i++ ) {
			dirFib = dirFib->df_Next;
		}
		if( (dirFib->df_Flags & FLAG_DIR_MASK) && ((dirFib->df_Flags & (FLAG_SLINK_MASK | FLAG_HLINK_MASK)) == 0) ) {
			/* if( ((DirLevelPtr) dirFib->df_SubLevel)->dl_ChildPtr != NULL ) {*/
				EnterDirectory(dirFib);
			/*}*/
		} else {
			OnOffFileItem(dirFib, item, (dirFib->df_Flags & FLAG_SEL_MASK) == 0);
			DisplayStats(FALSE);
		}
	} else {
		DoEnterDirectory();
	}
}

/*
 * Turn off or on one dir item
 */
 
static void OnOffFileItem(register DirFibPtr dirFib, WORD listItem, BOOL onOff)
{
	register UBYTE *text;
	
	tagsDone = TRUE;
	text = GetListItem(scrollList->List, listItem) + 1;
	dirFib->df_Flags &= ~FLAG_PART_MASK;
	*text &= ~LFLAG_PART_MASK;
	
	SetResetTag(onOff, dirFib, ((dirFib->df_Flags & FLAG_DIR_MASK) == 0) 
		&& (onOff != ((dirFib->df_Flags & FLAG_SEL_MASK) != 0) ) );

	if( onOff ) {
		*text |= LFLAG_SEL_MASK;
		dirFib->df_Flags |= FLAG_SEL_MASK;
	} else {
		*text &= ~LFLAG_SEL_MASK;
		dirFib->df_Flags &= ~FLAG_SEL_MASK;
	}
	SLDrawItem(scrollList, listItem);
}

/*
 * Return next item selected, or if Alt down, just the next item in the list.
 * If no more items, return -1.
 */
 
static WORD NextFileListItem(BOOL altOn, register WORD listItem)
{
	if( altOn ) {
		listItem = (++listItem == SLNumItems(scrollList)) ? -1 : listItem;
	} else {
		listItem = SLNextSelect(scrollList, listItem);
	}
	return(listItem);
}


/*
 *	Handle tag menu
 */

BOOL DoTagMenu(WindowPtr window, register UWORD item, UWORD sub)
{
	register BOOL success = mode != MODE_LIMBO;

	if( success ) {
		if( item == TAG_FILTER_ITEM ) {
			success = DoTagFilterRequest();
		} else {
			DoTag( item == TAG_ITEM || item == TAG_ALL_ITEM, 
				 item == TAG_ALL_ITEM || item == UNTAG_ALL_ITEM );
			success = TRUE;
		}
	}
	return (success);
}

