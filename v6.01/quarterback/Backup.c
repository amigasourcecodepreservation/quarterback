/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Backup loop and support routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <dos/dos.h>
#include <devices/trackdisk.h>
#include <devices/scsidisk.h>
#include <rexx/errors.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/intuition.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Utility.h>			/* For pointer definitions */

#include <string.h>

#include "QB.h"
#include "Proto.h"
#include "Tape.h"

/*
 *	External variables
 */

extern TextChar	strBuff[], strOpBegin[], strSpace[];
extern TextChar	strReadErr[], strPartReadErr[], strIncomplErr[], strWriteErr[];
extern TextPtr		strPath, strSavePath;

extern WindowPtr	cmdWindow;
extern MsgPortPtr	mainMsgPort;
extern ULONG		sigBits;

extern ProgPrefs	prefs;
extern ULONG		tapeBufLen, trkBufSize, tapeTrkBufSize, bufferSize;
extern UWORD		blockSize/*, numBlks*/;
extern Ptr			readPtr;
extern UBYTE 		*fillBuffer, *fillPtr; 
extern UBYTE		*readBuffer, *compressBuffer;
extern ULONG		fillCount, compressBufSize;
extern UWORD		activeDisk, sessionCurrDisk, firstTape;
extern DirLevel	rootLevel;
extern struct DateStamp sessionDateStamp, firstSessionDateStamp;
extern UBYTE		*bufferQueue[];
extern DirLevelPtr	curLevel, saveCurLevel;
extern DirFibPtr	curFib, saveCurFib;
extern BOOL			catFlag, altCatFlag;
extern DevParamsPtr	firstDevice, activeDevice;
extern ULONG		volumeOffset, catSize;
extern LONG			activeCylinder, maxCylinders, sessionCurrCylinder;
extern BOOL			inputCompleted, outputCompleted, abortFlag;
extern UBYTE		mode, operation, prevMode;
extern UWORD		numDevices;
extern TextPtr		dialogVarStr, tapeErrorStrs[];
extern LONG			dialogVarNum, dialogVarNum2;
extern File			fHandle, destFile;

extern TextChar	sessionPassword[], currBackupName[], currVolName[], currComment[];
extern WORD			numBufs;
extern BOOL			firstProtErr, useAmigaDOS, tapeDevice, needRewind, needSpace;
extern ULONG		tranLen, lastTick;
extern WORD			useCache;
extern UBYTE		verifyMode, readErr;
extern UWORD		sessionNumber, firstTapeSession, firstSessionOnFirstTape;
extern WORD			errFillLevelCount;
extern BOOL			resetDisk, sequentialDevice, processAll;
extern WORD			saveCurrListItem, currListItem;
extern ProcessPtr	process;
extern Dir			oldCurrDir, saveCurrDir;
extern WORD			saveIndentLevel, indentLevel;
extern BOOL			async, useAsync, attemptCompressionPage, catErrFlag;
extern WORD			qbCode;
extern ULONG		encryptSize;
extern ULONG		absolutePos;
extern UBYTE		backupCompress, restoreCompress, backupType;
extern UBYTE		floppy525[];
extern WORD			useSortFormat, verifyIOB;
extern BOOL			rewritingCat, badDiskFlag, earlyWarning, supportsSetMark;
extern BOOL			restartCatalog;
extern WORD			extraLen, statusIOBIndex;
extern StatStruct	stats;

/*
 * Prototypes
 */
 
static void	WaitForEvent(void);
static BOOL	PadAndQueue(void);
static void	WriteQBIdentifier(void);
static void	PadToLongword(void);
static void	OutputDelimiter(ULONG, BOOL);
static void	OutputEndMark(ULONG);
static BOOL	DoBackupLoop(void);
static void	BackupLoop(void);
static BOOL	VerifyTapeAfterBackup(void);
static WORD	HandleErrors(DevParamsPtr, WORD);
static BYTE	SuspectAmigaDOSFormat(DevParamsPtr);
static void	PromptForNextDisk(void);
static BOOL	WriteFirstCatalog(void);
static BYTE	DoDiskSenseDialog(DevParamsPtr, WORD);
static void	AbortBackup(void);
static BOOL	WriteError(DevParamsPtr, BYTE, BOOL);
static BOOL	DoNextDiskOrTape(DevParamsPtr);
static void	TransferUnwrittenDataToNextTape(DevParamsPtr);
static void	RewriteInitialCatalog(void);
static BOOL	ResetDisk(DevParamsPtr, BOOL);

WORD resetPad;
UBYTE resetData[DELIMITER_SIZE];

/*
 * Initialize for backup/restore
 */
 
BOOL InitLoops()
{
	register BOOL success = TRUE;
	UBYTE outputType;

	if (mode == MODE_BACKUP) {
		outputType = prefs.Destination;
		extraLen = sizeof(QBNewFirstCylHdr) - sizeof(QBFirstCylHdr);
	} else {
		if (prefs.DisplayRestoreOpts) {
			success = DoRestoreOptions();
		}
		outputType = prefs.Source;
	}
	useAmigaDOS = outputType == DEST_FILE;
	badDiskFlag = earlyWarning = abortFlag = firstProtErr = restartCatalog = FALSE;
	stats.sourceBytesReset = stats.ignoreCount = useCache = 0;
	saveCurrDir = NULL;
	if (success) {
		if (success = AllocDeviceList()) {
			if (success = AllocateBuffers(trkBufSize)) {
				if (success = InitOp()) {
					BuildSigBits();
					sessionCurrCylinder = 0;
					sessionCurrDisk = 1;
					errFillLevelCount = 0;
					if (tapeDevice && (prefs.TapeOpts.AutoRewind || !needRewind)) {
						bufferSize = tapeBufLen;
					}
					DoInitDriveStates(FALSE);
					altCatFlag = CAT_DONE;
					if (mode != MODE_BACKUP) {
						if (prefs.TapeOpts.AutoRewind) {
							readPtr = readBuffer;
						}
					}
 				} else {
					if (mode == MODE_RESTORE) {
						Error(ERR_READING_CATALOG);
					}
				}
			} else {
				Error(ERR_NO_MEM_BUFFER);
			}
		}
		if (!success) {
			FreeDeviceList();
			FreeBuffers();
		}
	}
	return (success);
}

/*
 * Initialize drive state for all selected drives
 */
 
void InitDriveStates(BOOL recalc)
{
	register DevParamsPtr devParam;
	UBYTE saveOp;
	register BOOL isComp;
	BOOL scanMode;
	DirFibPtr firstFib, firstSelFib;
	
	if (recalc) {
/*
	Re-sort, since the backup may have had its files sorted a different way, and
	the user may have different a catalog sorting method at the moment.
*/
		if (mode == MODE_RESTORE) {
			scanMode = verifyMode == RMODE_SCAN;
			isComp = restoreCompress & COMP_ON_FLAG;
		} else {
			scanMode = FALSE;
			isComp = prefs.Compress & COMP_ON_FLAG;
		}
		InitList();
		processAll = scanMode || (isComp && (tapeDevice || (useSortFormat < 2)));
		catErrFlag = FALSE;
		saveOp = operation;
		operation = OPER_PREP;
		SortAllDirs();
		operation = saveOp;
		firstSelFib = NextFile(FALSE, FALSE);
		InitList();
		firstFib = NextFile(TRUE, FALSE);
		InitList();
		for (devParam = firstDevice ; devParam != NULL ; devParam = devParam->Next) {
			devParam->Msg = (devParam->Number - numDevices) + (firstTape - 1);
		}
	}
	for (devParam = firstDevice ; devParam != NULL ; devParam = devParam->Next) {
		devParam->Changed = TRUE;
		if (curLevel != NULL) {
			curLevel->dl_CurFib = NULL;
		}
		if (useAmigaDOS || (mode == MODE_RESTORE)) {
			devParam->Status = STATUS_READY;
		}
		if ((mode == MODE_BACKUP) || recalc) {
			RecalcDisk(devParam, firstFib != firstSelFib);
		}
		ClearSense(devParam);
		/*
		for (i = 0 ; i < numBufs ; i++) {
			devParam->BufWait[i] = FALSE;
			devParam->BytesWritten[i] = 0L;
		}
		*/
	}
	InitStats();
	InitList();
}

/*
 * Call InitDriveStates() and do display.
 */

void DoInitDriveStates(BOOL recalc)
{
	InitDriveStates(recalc);
	DiskChanged();
	DisplayAllDevicesStatus();
}

/*
 * Perform rewind operation at end of operation, if necessary.
 */
 
void DoRewind(BOOL waitRewind, BOOL autoRewind)
{
	UWORD saveFirstTape;
	
	if (autoRewind || (!tapeDevice)) {
		saveFirstTape = firstTape;
		InitPosition();						// Reset values if not multi-session tape
		firstTape = saveFirstTape;
	}
	if (tapeDevice) {
		if (autoRewind) {
			RewindTape(activeDevice, waitRewind);
		}
	} else if (useAmigaDOS) {
		Seek(destFile, 0, OFFSET_BEGINNING);
	}
	if (abortFlag && (mode == MODE_RESTORE)) {
		activeDevice = firstDevice;
		useCache = 0;							/* Flush from decompression */
		sessionCurrCylinder = activeCylinder = -1;
		
/**** DISABLED FOR NOW...WARNING: LEAVES ABORTED INFORMATION INTACT!
		if (abortFlag && (mode == MODE_BACKUP)) {
			SetStdPointer(cmdWindow, POINTER_WAIT);
			BlockClear(bufferQueue[0], trkBufSize);
			TapeIO(activeDevice, MYCMD_WRITE_HEADER, (ULONG) bufferQueue[0]);
			TapeIO(activeDevice, CMD_REWIND, NULL);
			SetArrowPointer();
		}
*/
	}
}

/*
 * Perform cleanup after a backup/restore operation is aborted/done.
 */
 
void DoEndLoops()
{
	register DevParamsPtr devParam;
	
	for (devParam = firstDevice ; devParam != NULL ; devParam = devParam->Next) {
		if ((devParam->Status == STATUS_NOT_READY) || useAmigaDOS  || tapeDevice) {
			StatusCompleted(devParam);
		} else {
			StatusRemove(devParam);
		}
	}
	if (tapeDevice && (!prefs.TapeOpts.AutoRewind)) {
		if (outputCompleted) {
			sessionNumber++;
		}
		firstTape = activeDisk;
	}
	EndOp();
	FreeDeviceList();
	CompressDeInit();
	FreeBuffers();
}

/*
 * Perform clean up, and then put back into a different mode.
 */
 
void EndLoops(BOOL success)
{
	if (success) {
		if (operation != OPER_SCAN && operation != OPER_SELECTION)
			DoOutputReport();
		DoEndLoops();
		FreeDirBlocks();
/*
	If this is really just aborting during file selection on restore, skip below...
*/
		if (mode == MODE_BACKUP || (operation != OPER_SCAN && operation != OPER_SELECTION))
			SetModeOp(mode, OPER_COMPLETE);
		else {
			SetModeOp(MODE_LIMBO, OPER_LIMBO);
			BuildVolumeList(NULL);
		}
	}
	else {
		if (mode == MODE_BACKUP) {
			DoEndLoops();
		} else {
			InitDriveStates(FALSE);
			MotorOff(FALSE);						/* Aborted restore */
		}
		SetModeOp(mode, OPER_SELECTION);
		if (operation == OPER_SELECTION) {
			InitDirectory();
			BuildFileList(NULL);
		}
	}
	return;
}

/* 
 * GRAND ENTRY POINT: DoBackup() starts the file by file backup
 */

BOOL DoBackup()
{
	register BOOL success = TRUE;

	backupCompress = prefs.Compress;
	if (backupCompress & COMP_ON_FLAG) {
		if (!(success = CompressInit( (WORD)((UWORD)backupCompress)))) {
			success = !WarningDialog(ERR_COMP_NOMEM, TRUE);
			backupCompress &= ~COMP_ON_FLAG;
		}
	}
	sessionPassword[0] = 0;
	if (prefs.Password) {
		DoPasswordRequest(sessionPassword);
	}
	if (success) {
		success = DoBackupLoop();
	}
	EndLoops(success);
	
	return (success);
}

/*
 * Prepare to do the Backup loop
 */

static BOOL DoBackupLoop()
{
	BPTR dir;
	register DevParamsPtr devParam = activeDevice;
	
	attemptCompressionPage = FALSE;
	if (tapeDevice) {
		abortFlag = !InitTapeParameters(devParam, TRUE);
		devParam->Msg = devParam->NextMsg = activeDisk = firstTape;
	}
	verifyMode = RMODE_RESTORE;
	FirstRuntimeStrings();
/*
	In case of multiple tapes and verify is on, we need to know where to start
	our compare on the beginning tape when backup is done, so store this here.
	This is the number of times to space on the orginial tape if we straddle
	tapes with the upcoming backup.
*/
	firstSessionOnFirstTape = sessionNumber - firstTapeSession;
	
	if (devParam->Status == STATUS_NOT_READY) {
		if (devParam->Floppy && (devParam->Unit < 4) && floppy525[devParam->Unit]) {
			StatusCheck(devParam);
		} else {
			abortFlag |= !InsertDisk(devParam, 0);		/* Ignore if tape drive */
		}
	}
	if (!abortFlag) {
		if (activeCylinder == -1) {
			activeCylinder++;
			sessionCurrCylinder = 0;
		}
		if (tapeDevice && needSpace && (prevMode == MODE_RESTORE)) {
			TapeIO(devParam, CMD_SPACE, 0x00000100);
		}
		prevMode = MODE_BACKUP;
/*
	Disable INTUITICKS during backup, for a tiny bit more speed. Be warned that
	you will not get these messages until they are re-enabled!
*/
		ModifyIDCMP(cmdWindow, cmdWindow->IDCMPFlags & ~IDCMP_INTUITICKS);
/*
	Write first cylinder and catalog to first disk/tape.
	Make synchronous I/O, because we don't know when to wait here,
	and the catalog could be more than one buffer long.
*/
		dir = Lock(strPath, ACCESS_READ);
		rootLevel.dl_DirLock = dir;
		oldCurrDir = CurrentDir(dir);		/* Save previous dir */
		if (useAmigaDOS) {
			StatusActive(devParam);			/* Per James' request */
		}
		
		/* firstTape = sessionCurrDisk; */
		
		useAsync = !(useAmigaDOS || prefs.OldPrefs.SlowBackup);
		if (WriteFirstCatalog()) {
/*
	Because we change dir each time NextFile() finds a file in a different
	directory, we create an initial lock to change from, at the root level.
*/
			if ((backupCompress & COMP_ON_FLAG) == 0) {
				fillPtr = &readBuffer[fillCount];
				BlockMove(fillBuffer, readBuffer, fillCount);
			}
			BackupLoop();						/* Go for it! */
		} else {
			DoFinalRuntimeStrings();		/* Print aborted message */
		}
		(void) CurrentDir(oldCurrDir);	/* Restore previous dir */
		UnLock(dir);
		oldCurrDir = NULL;
		ModifyIDCMP(cmdWindow, cmdWindow->IDCMPFlags | IDCMP_INTUITICKS);
	}
	return (!abortFlag);
}

/*
 * Write the first catalog on the backup
 */
 
static BOOL WriteFirstCatalog()
{
	BOOL success;
	
	do {
		firstDevice->CurrIOB = 0;
		readErr = resetDisk = FALSE;
		InitList();	
		DiskChanged();
		stats.byteCount = 0;
		InitFirstCylinder();	
		
		altCatFlag = FALSE;
		if (success = WriteCatalog()) {
			stats.padBytes = 0;
			PadToLongword();
		}
	} while ((!abortFlag) && (!success));
	InitList();
	return ( success && !abortFlag);
}

/*
 * Write QB identifier on first cylinder of a disk
 */
 
void InitFirstCylinder()
{
	fillPtr = fillBuffer = bufferQueue[0];
	fillCount = 0;
	WriteQBIdentifier();
}

/*
 * Pad data to nearest longword.
 */
 
static void PadToLongword()
{
	while ((fillCount & 3) && (readErr == 0)) {
		PutChar(0);						/* Pad to longword */
		if (readErr == 0) {
			stats.padBytes++;
		}
	}
}

/*
 * Output a delimiter between file data to ensure data integrity when verifying.
 */
 
static void OutputDelimiter(ULONG realSize, BOOL compressed)
{
	register WORD i;
	UWORD oldCyl = activeCylinder;
	UBYTE buff[40];
	register ULONG temp;
	
	if (compressed) {
		temp = ((backupType == BACKUP_IMAGE) ? CIMG_ID : CFIL_ID) | backupCompress;
	} else {
		temp = (backupType == BACKUP_IMAGE) ? IMAGE_ID : FILE_ID;
	}
	*((ULONG *)&buff[0]) = temp;
	BlockMove(curFib->df_Name, &buff[4], MAX_FILENAME_LEN+1);
	*((ULONG *)&buff[4+MAX_FILENAME_LEN+1]) = realSize ? curFib->df_Size : NULL;
	
	for (i = 0 ; i < 40; i++) {
		PutChar(buff[i]);
		if (resetDisk) {
			return;
		} else if (readErr == 0) {
			stats.padBytes++;
		}
	}
	if ((oldCyl != activeCylinder) && (activeCylinder == 0)) {
		resetPad = (fillCount | 3) + 1;
		BlockMove(fillPtr - resetPad, resetData, resetPad);
	}
}

/*
 *	Output length actually written (for corrupt files)
 * Currently BOOLean TRUE or FALSE converted to longword, TRUE (non-zero) if
 * the entire file is intact and was not padded due to problems reading it.
 */

static void OutputEndMark(ULONG flags)
{
	register WORD i;
	UWORD oldCyl = activeCylinder;
	
	for (i = 0 ; i < 4 ; i++) {
		PutChar( ((UBYTE *)&flags)[i]);
		if (resetDisk) {
			return;
		} else if (readErr == 0) {
			stats.padBytes++;
		}
	}
	if ((oldCyl != activeCylinder) && (activeCylinder == 0)) {
		resetPad = 4;
		if (fillCount & 3) {				/* Pad to longword */
			resetPad += (4 - (fillCount & 3));
		}
		*((ULONG *)resetData) = *((ULONG *)fillPtr - resetPad);
	}
}

/*
 * The backup loop, responsible for reading each file one by one from the source
 * and outputting them to the destination. Does not return until backup is complete
 * or aborted. If aborted, abortFlag will be TRUE.
 */
 
static void BackupLoop()
{
	register DirFibPtr dirFib;
	register BOOL fileDone;
	register LONG numRead = 0L;
	register LONG numToRead;
	register BOOL done;
	register UBYTE *bufPtr = readBuffer;
	LONG totalNumRead;
	BOOL errFillFlag;
	UBYTE *whereToRead;
	register LONG offset = fillCount;		/* Read() works best when this is aligned! */
	BOOL newFile;
	BOOL reverify;
	BOOL compressOn = (backupCompress & COMP_ON_FLAG) != 0;
	BOOL fileCompressed;
	ULONG amountAdded;
	
BLoop:
	resetPad = 0;
	done = errFillFlag = FALSE;
	while ((!(abortFlag |= CheckAbortButton())) && !done) {
		readErr = FALSE;
		if (fHandle == NULL) {
			amountAdded = 0;
			if (resetDisk) {
				resetDisk &= (activeDisk != 1);
			}
			if (!resetDisk || (stats.byteCount == 0)) {
				curFib = NextFile(FALSE, FALSE);
			}
			fileDone = TRUE;
			if ((dirFib = curFib) != NULL) {
				fileCompressed = compressOn && (dirFib->df_Flags & FLAG_BITS_MASK);
				DisplayNextFile(dirFib);
				if ((dirFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK)) == 0) {
					dirFib->df_ActualSize = dirFib->df_Size;
					if (!resetDisk) {
						stats.completedFiles++;
					}
					if (errFillLevelCount == 0) {
			
						if (dirFib->df_Prot & FIBF_READ) {
							EnableFileAccess(dirFib->df_Name);
						}
						fHandle = Open(dirFib->df_Name, MODE_OLDFILE);
					}
					if (!resetDisk || (stats.byteCount == 0)) {
						totalNumRead = 0;
						resetDisk = FALSE;
						OutputDelimiter(fHandle, fileCompressed);
						if (!resetDisk) {
							offset = ((ULONG)fillPtr) - ((ULONG)readBuffer);
							stats.byteCount = dirFib->df_Size;
						}
					} else {
						resetDisk = FALSE;
						totalNumRead = dirFib->df_Size - stats.byteCount;
						if (fHandle != NULL) {
							Seek(fHandle, totalNumRead, OFFSET_BEGINNING);
						}
					}
					fileDone = FALSE;
					if ((errFillFlag = fHandle == NULL) || errFillLevelCount) {
						if (!resetDisk) {
							UnableToProcess(dirFib, GetIOErrStr(IoErr()), RC_WARN);
						}
						if (fileCompressed && !stats.ignoreCount) {
							fileDone = TRUE;
							stats.completedBytes += stats.byteCount;
							dirFib->df_ActualSize = 0L;
							OutputEndMark(0L);
						}
					}
					newFile = TRUE;
				}
			} else {
				done = TRUE;
			}
		}
		while (!(fileDone || resetDisk || abortFlag)) {
			if (compressOn) {
				numToRead = compressBufSize;
				whereToRead = compressBuffer;
			} else {
				numToRead = trkBufSize - offset;
				whereToRead = fillPtr;
			}
			if (numToRead > stats.byteCount) {
				numToRead = stats.byteCount;
				amountAdded = ((numToRead | 3) + 1) - numToRead;
			}
			if (!errFillFlag) {
				if (numToRead) {
					numRead = Read(fHandle, whereToRead, numToRead + amountAdded);
					if ((numRead == -1) || (numRead == 0)) {
						dirFib->df_Flags |= FLAG_ERR_MASK;
						errFillFlag = TRUE;
						UnableToProcess(dirFib, numRead ? strReadErr : strPartReadErr, RC_WARN);
						numRead = stats.byteCount;
					} else {
						if (amountAdded && (numRead > numToRead)) {
							numRead -= amountAdded;
							UnableToProcess(dirFib, strIncomplErr, RC_WARN);
						}
						totalNumRead += numRead;
					}
				} else {
					numRead = 0;
				}
			}
			if (errFillFlag) {
				numRead = numToRead;
				BlockClear(whereToRead, numToRead);
			}
			stats.byteCount -= numRead;
			if (stats.byteCount == 0) {
				CloseFile();
				UpdateProtectionBits(curFib->df_Name);
				fileDone = TRUE;
				errFillFlag = FALSE;
			}
			if (compressOn) {
				if (fileCompressed) {
					Compress(compressBuffer, numRead, newFile);
					newFile = FALSE;
				} else {
					bufPtr = compressBuffer;
					while (numRead--) {
						stats.compressBytesInput++;
						PutChar(*bufPtr++);
					}
				}
				WaitForEvent();
				if (fileDone && (readErr == 0)) {
					if (fileCompressed) {
						if (dirFib->df_Size) {
							CompressEOF();
							dirFib->df_ActualSize = stats.bytesCompressedInFile;
						} else {
							dirFib->df_ActualSize = 0L;
						}
					}
					dirFib->df_NumRead = totalNumRead;
					OutputEndMark(totalNumRead);
					PadToLongword();
				}
			} else {
				/*offset += numRead;*/
				fillCount += numRead;
				fillPtr += numRead;
				
				if (fileDone) {
					dirFib->df_NumRead = totalNumRead;
					OutputEndMark(totalNumRead);
					PadToLongword();
				}
				offset = ((ULONG)fillPtr) - ((ULONG)readBuffer);
				if (offset >= trkBufSize) {
					WaitForEvent();
					if (!(abortFlag | resetDisk)) {	/* WaitIOReq() can abort backup! */
						offset = 0;
						if (fillCount >= trkBufSize) {
							async = (!earlyWarning) && useAsync;
							(void) WriteFullBuffer(trkBufSize);
							offset = ((ULONG)fillPtr) - ((ULONG)readBuffer);
							if ((!resetDisk) && (activeCylinder == 0)) {
								resetPad = 0;
							}
						}
						fillPtr = &bufPtr[offset];
					}
				}
			}
		}
	}
	if (abortFlag) {
		AbortBackup();
		UnLockLocks(&rootLevel);
	} else {
		WaitIOReq(activeDevice->CurrIOB);
		if (!compressOn) {
			offset = fillPtr - bufPtr;
			if (bufPtr == readBuffer) {
				BlockMove(readBuffer, fillBuffer, offset);
			}
		}
		PadToLongword();
		PutLong(ENDDATA_ID);
		stats.padBytes += 4;						/* Update now in case PutLong() reads in next. */
		PutLong(ENDBACKUP_ID);
		stats.padBytes += 4;
		if (!tapeDevice) {
			if (!PadAndQueue())
				goto BLoop;
			if (!useAmigaDOS) {
				fillPtr = fillBuffer;
				InitFileDir();
				altCatFlag = catFlag = CAT_WRITING;
				sessionCurrCylinder = 0;	/* Force encryption */
				WriteQBIdentifier();
				WriteCatalog();
				altCatFlag = CAT_DONE;
/*
	New for V5.0.2, go back and rewrite first catalog with correct sizes.
*/
				PadAndQueue();
			}
			if (compressOn) {
				inputCompleted = TRUE;		/* Just in case */
				RewriteInitialCatalog();
			}
		} else {
			PadAndQueue();						/* Pad remaining space in buffer and queue */
		}
	}
/*	UnLock(saveCurrDir);*/					/* Unlock the ResetDisk() lock */
	WaitAllIOReq();						/* In case aborted */
	if ((outputCompleted = !abortFlag) && tapeDevice) {
		WriteFilemark(activeDevice);
	}
	DoFinalRuntimeStrings();

	if (compressOn) {
		CompressDeInit();
	}
	if (!abortFlag) {
		reverify = (tapeDevice || useAmigaDOS) && prefs.OldPrefs.Verify && (sequentialDevice || (sessionNumber == 0));
		if (reverify) {
			VerifyTapeAfterBackup();
		} else {
			DoRewind(FALSE, prefs.TapeOpts.AutoRewind);
			ProcessAllArchiveBits();
		}
	}
	DoRewind(FALSE, prefs.TapeOpts.AutoRewind);
}

/*
 * Rewrite the initial catalog on compression.
 */
 
static void RewriteInitialCatalog()
{
	DevParamsPtr saveActiveDevice;
	WORD saveActiveDisk;
	BOOL done = FALSE;
	register BOOL success;
	
	rewritingCat = TRUE;
	saveActiveDevice = activeDevice;
	saveActiveDisk = activeDisk;
	activeDevice = firstDevice;
	activeDevice->NextMsg = 1;
	DriveStatus(activeDevice);
	DiskChanged();
	
	success = (activeDisk == 1) || GetCurrDisk();
	
	if (activeDevice != saveActiveDevice) {
		ShutDevDown(saveActiveDevice);
	}
/*
	Loop until disk in first drive is definitely #1, or the user aborted.
*/
	while (!done && !abortFlag) {
		while ((!success || (activeDisk != 1)) && !abortFlag) {
			InitPosition();
			ShutDevDown(firstDevice);
			PromptForNextDisk();
			success = GetCurrDisk();
		}
		if (!abortFlag) {
			success = FALSE;
			DoRewind(FALSE, FALSE);			/* If AmigaDOS, seek to beginning */
			firstDevice->Status = STATUS_CHECK;
			WriteFirstCatalog();
			catFlag = CAT_WRITING;
			done = DoWriteBuffer(firstDevice, fillBuffer, fillCount) == 0;
			if (!done) {
				HandleErrors(firstDevice, firstDevice->CurrIOB);
			}
			catFlag = CAT_DONE;
		}
	}
	rewritingCat = FALSE;
	activeDisk = saveActiveDisk;
}

/*
 * Call this when backup is reset or aborted
 */
 
static void AbortBackup()
{
	register DirFibPtr dirFib = curFib;
	/* register WORD i; */
	
	errFillLevelCount = 0;
	if (dirFib != NULL && (dirFib->df_Prot & FIBF_READ)) {
		UpdateProtectionBits(dirFib->df_Name);
	}
	/* We may want to do this later... Shouldn't hurt I would think...
	for (i = 0 ; i < numBufs ; i++) {
		if (activeDevice->BufWait[i]) {
			AbortIO((struct IOStdReq *) activeDevice->IOB[i]);
		}
	}
	*/
	CloseFile();
}

/*
 * Verify tape written OK after backup completed
 */
 
static BOOL VerifyTapeAfterBackup()
{
	register BOOL success = FALSE;
	register UWORD num = sessionNumber - firstTapeSession;
	LONG saveCylinder = activeCylinder;
	register DevParamsPtr devParam = firstDevice;
	WORD tempSessionNum;
	register UWORD saveFirstTape = firstTape;
	
	if (tapeDevice && (sessionCurrDisk != 1)) {
		sessionCurrDisk = 1;
		StatusRemove(devParam);
		devParam->Msg = devParam->NextMsg = activeDisk = saveFirstTape;
		sessionPassword[0] = '\0';
		success = LoadNextDisk();
		num = firstSessionOnFirstTape;
	}
	if (!abortFlag) {
		tempSessionNum = sessionNumber - num;
		DoRewind(TRUE, TRUE);
		activeDisk = saveFirstTape;
		sessionNumber = tempSessionNum;			/* DoSpaceTape() will add "num" back. */
		if (tapeDevice) {
			DoSpaceTape(devParam, num);
		}
		activeCylinder = 0;
		DisplayStr(NULL);								/* Display a blank line */
		FreeDirBlocks();
		verifyMode = RMODE_COMPARE;				/* Compare mode */
		SetModeOp(MODE_RESTORE, operation);
		DriveStatus(devParam);
		if (DoLoadFirst(FALSE)) {
			InitStats();
			RecalcNumbers(TRUE);
			if (success = DoRestore(TRUE)) {
				activeCylinder = saveCylinder;
				ProcessAllArchiveBits();
			}
		}
	}
	return (success);
}

/*
 * Wait until an input event (i.e. INTUITICKS) occurs or a SendIO() is complete.
 */
 
static void WaitForEvent()
{
	register DevParamsPtr devParam = activeDevice;
	register WORD index = devParam->CurrIOB;
	register ULONG signal;
	register ULONG portMask;
	
	while (((portMask = devParam->BufWait[index] ? 1 << devParam->MsgPort[index]->mp_SigBit : 0) ||
		((IntuiMsgPtr)mainMsgPort->mp_MsgList.lh_Head)->ExecMessage.mn_Node.ln_Succ) && (!abortFlag)) {
		signal = Wait(sigBits | portMask);
		if (signal & (~portMask)) {
			abortFlag |= CheckAbortButton();
		}
		if (signal & portMask) {
			WaitIORequest(index);	/* SendIO() must have finished. */
		}
	}
}

/*
 * Wait for all I/O Requests to finish.
 */

BYTE WaitAllIOReq()
{
	register WORD index;
	register WORD originalIndex;
	register BYTE error = 0;
		
	index = originalIndex = activeDevice->CurrIOB;
	
	do {
		error |= WaitIOReq(index);
		index++;
		if (index == numBufs) {
			index = 0;
		}
	} while (originalIndex != index);
	MotorOff(operation != OPER_PAUSE);
	lastTick = 0L;
	UpdateStats();
	return (error);
}

/*
 * Pads remaining space in a buffer and sends buffer out to be written.
 */

static BOOL PadAndQueue()
{
	register ULONG size = fillCount;
	BYTE err;
	
	if (size) {
		if (tapeDevice) {
			size = ((fillCount / bufferSize) * bufferSize) + bufferSize;
			stats.padBytes += size - fillCount;
		} else if (!useAmigaDOS) {
			BlockClear(fillPtr, trkBufSize - size);
			stats.padBytes += trkBufSize - size;
			size = trkBufSize;
		}
		WaitIOReq(activeDevice->CurrIOB);
		err =	WriteFullBuffer(size);
		fillCount = trkBufSize;
		WaitIOReq(activeDevice->CurrIOB);
	}
	return ( inputCompleted = err == 0);
}

/*
 * Wait for the specified IORequest to finish, and update disk changed
 */
 
BYTE WaitIOReq(WORD index)
{
	BYTE error;
	
	error = WaitIORequest(index);
	DiskChanged();
	return (error);
}

/*
 * Wait for IORequest to finish
 */
 
BYTE WaitIORequest(register WORD index)
{
	register DevParamsPtr devParam = activeDevice;
	BYTE error = 0;
	
	if (devParam->BufWait[index]) {
		WaitIO((struct IOStdReq *) devParam->IOB[index]);
		devParam->BufWait[index] = FALSE;
		error = HandleErrors(devParam, index);
	}
	return (error);
}

/*
 * Read any errors which have resulted from the writing of the last buffer.
 */
 
static WORD HandleErrors(register DevParamsPtr devParam, WORD index)
{
	register WORD err = 0;
	register WORD oldIndex;
	register struct IOStdReq *iobPtr;
	TextChar buff[60];
	
	if (!abortFlag) {
		oldIndex = devParam->CurrIOB;
		
		devParam->CurrIOB = index;
		iobPtr = (struct IOStdReq *)devParam->IOB[index];
		if (!useAmigaDOS) {
			if (tapeDevice) {
				err = ProcessSenseData(devParam, index);
				if ((err > 1) && (err < 8)) {
					dialogVarNum = err;
					dialogVarStr = NULL;
					if ((err > 2) && (err < 8)) {
						strcat(strcpy(buff, tapeErrorStrs[err-3]), strSpace);
						dialogVarStr = buff;
					}
					abortFlag = err = TRUE;
					WarningDialog(ERR_TAPE_BAD, FALSE);
				}
			} else {
				err = iobPtr->io_Error;
				if (err) {
					abortFlag |= !WriteError(devParam, err, TRUE);
				}
			}
		} else {
			abortFlag |= err = IoErr();
		}
		if (!resetDisk) {
			devParam->CurrIOB = oldIndex;
		}
	}
	readErr |= abortFlag;
	return (err);
}

/*
 * Write the current full buffer to the destination, in the amount of "size" bytes.
 * Swaps the current buffer in DoWriteBuffer(), for double-buffering.
 * Leaves "fillPtr" as pointing to the next buffer which will be ready.
 * Leaves "fillBuffer" as pointing to its final buffer destination.
 * Zeroes "fillCount" to signify empty buffer.
 *
 * This is called by the BackupLoop() and whenever PutChar/Word/Long overflows
 * its buffer (as in WriteCatalog() or writing delimiters).
 */

BYTE WriteFullBuffer(ULONG size)
{
	register DevParamsPtr devParam = activeDevice;
	register WORD oldIndex = devParam->CurrIOB;
	register WORD index;
	register BYTE err;
	BOOL compressOn = (backupCompress & COMP_ON_FLAG) != 0;
	BOOL saveAsync;
	
/*
	If actually filling into readBuffer, copy over info fillBuffer first.
*/
	if (tapeDevice) {
		saveAsync = async;
		async = FALSE;
		WaitIOReq(oldIndex);
		if (earlyWarning) {
			WaitAllIOReq();
		}
		async = saveAsync;
	} else {
		WaitIOReq(oldIndex);
	}
	
	if (!(err = readErr)) {
		if (((ULONG) fillPtr) - ((ULONG)fillCount) == ((ULONG)readBuffer)) {
			CopyMemQuick(readBuffer, fillBuffer, size);
		}
/*
	Test previous buffer written now, if verify on and not checked at the time.
*/
		if (activeCylinder) {
			if (!readErr && (verifyIOB >= 0) && prefs.OldPrefs.Verify && !tapeDevice) {
				devParam->CurrIOB = verifyIOB;
				if (!ReadAfterWrite(devParam, bufferQueue[verifyIOB], trkBufSize)) {
					err = readErr;
				}
				if (!resetDisk) {
					devParam->CurrIOB = oldIndex;
				}
			} 
		}
		if (!err) {
			dialogVarStr = devParam->DevName;
			if (catFlag != CAT_WRITING || (devParam->Status == STATUS_NOT_READY)) {
				PromptForNextDisk();
			}
			if (!abortFlag) {
				if ((!earlyWarning) || (activeCylinder < maxCylinders)) {
					devParam->BufWait[oldIndex] = async;
					err = DoWriteBuffer(devParam, fillBuffer, size);	/* Swaps CurrIOB */
				
					if (err) {
						if (useAmigaDOS) {
							UnableToProcess(curFib, strWriteErr, RC_WARN + 2);
							abortFlag = TRUE;
						} else {
							if (rewritingCat) {
								resetDisk = TRUE;
							}
							if (HandleErrors(devParam, oldIndex) == VOLUME_OVERFLOW) {
								WaitIO((struct IOStdReq *) devParam->IOB[oldIndex]);
								err = 0;
								stats.rawCompletedBytes += stats.padBytes;
							}
						}
					} else {
						if (!async) {
							HandleErrors(devParam, oldIndex);
						} else if (compressOn) {
/*
	If compression on and verify off, we must make sure we don't fudge with
	the data in the other buffer before it has been written out.
*/
							err = WaitIORequest(devParam->CurrIOB);
						}
					}
				}
				if (err == 0) {
					index = devParam->CurrIOB;
			
					fillBuffer = bufferQueue[index] ;
					fillPtr = (catFlag | compressOn) ? fillBuffer : readBuffer;
					fillCount = 0;
					
					AddToCompletedBytes(devParam, oldIndex);
 					if (( !useAmigaDOS) && (activeCylinder >= maxCylinders) && 
						(!inputCompleted || (catFlag == CAT_WRITING))) {
						if (DoNextDiskOrTape(devParam)) {
							if (sequentialDevice) {
								needRewind = FALSE;
								if (catFlag != CAT_WRITING) {
									TransferUnwrittenDataToNextTape(devParam);
								} else {
									readErr = resetDisk = TRUE;
								}
							} else {
								if (tapeDevice) {
									trkBufSize = tapeTrkBufSize;
									(void) InitTapeParameters(devParam, FALSE);
								}
								WriteQBIdentifier();
							}
						}
					}
				}
			} else {
				fillCount = 0;			/* If in middle of WriteCatalog(), cut it off. */
			}
		}
	}
	async = FALSE;
	return (err);
}

/*
 * Take the data in buffers which did not make it to the previous tape and set it
 * up for writing to the next tape.
 * Do not do this with the catalog - if in the middle of a catalog, simply
 * rewrite the whole thing on the next tape and do not bother with this.
 */
 
static void TransferUnwrittenDataToNextTape(register DevParamsPtr devParam)
{
	register DirFibPtr dirFib;
	register LONG offset;
	register LONG rawBytesDone;
	register LONG temp;
	register LONG bytes;
	LONG files = 0;
	WORD delimiter = 0;
	BOOL compressOn = backupCompress & COMP_ON_FLAG;

	stats.saveComplBytes = 0;
	bytes = stats.finishedTapeBytes + sizeof(QBFirstCylHdr);
/*
	We now know where the last byte written is. In order to backtrack, we'll
	have to traverse the entire backup written up to this point from the 
	beginning until the file which contains this last byte exists.
*/
	UnLockLocks(&rootLevel);
	CurrentDir(saveCurrDir);
	InitList();
	operation = OPER_SELECTION;
	SetBusyPointer();
	temp = 4;
	
	dirFib = NextFile(FALSE, TRUE);

/*
	First make sure that the catalog was fully written on the previous tape. If it
	wasn't, then rewrite it on the next tape. Otherwise, we need to find exactly
	where we left off, as found in the code below.
*/
	if (stats.rawCompletedBytes > bytes) {
		rawBytesDone = stats.saveRawComplBytes = (stats.rawCompletedBytes -= (devParam->EOMBufs * tapeBufLen));
		
		while (dirFib != NULL) {
			if (IS_DIR(dirFib->df_Flags)) {
				if (prefs.LogOpts.LogLevel != LOG_RESULTS) {
					temp++;
				}
			} else {
				if (prefs.LogOpts.LogLevel != LOG_DRAWERS) {
					temp++;
				}
				files++;
				bytes += DELIMITER_SIZE;
				if ((offset = bytes - rawBytesDone) >= 0) {
					stats.ignoreCount = DELIMITER_SIZE - offset;
					delimiter++;
					offset = dirFib->df_ActualSize;
					break;
				}
				bytes += dirFib->df_ActualSize;
				if (compressOn && (dirFib->df_Flags & FLAG_BITS_MASK)) {
					if ((offset = bytes - rawBytesDone) >= 0) {
						break;
					}
					stats.saveComplBytes += dirFib->df_Size;
				} else {
					stats.saveComplBytes += dirFib->df_ActualSize;
					if ((offset = bytes - rawBytesDone) >= 0) {
						stats.saveComplBytes -= offset;
						break;
					}
					while (bytes & 3) {
						bytes++;
					}
				}
				bytes += sizeof(ULONG);
				if ((offset = bytes - rawBytesDone) >= 0) {
					offset = 0;
					delimiter += 2;
					break;
				}
				while (bytes & 3) {
					bytes++;
				} 
				NextBusyPointer();
			}
			dirFib = NextFile(FALSE, TRUE);
		}
	} else {
/*
	Simply rewrite the catalog and backup all the files over again.
*/
		stats.saveRawComplBytes = 0;
		firstTape = sessionCurrDisk;		/* Tell verify to use this tape now... */
		firstSessionOnFirstTape = 0;		/* ...and it's the first session now */
		sessionCurrCylinder = 0;
		WriteCatalog();
		offset = curFib->df_ActualSize;	/* Do entire first file in backup loop */
		delimiter = 1;							/* Force writing of OutputDelimiter() below */
	}
	stats.padBytes = 0;
	saveCurrListItem = temp;
	stats.saveComplFiles = files;
	curFib = dirFib;
	strcpy(strSavePath, strPath);
	SetArrowPointer();
	operation = OPER_IN_PROG;
	
	if (temp = (compressOn && (dirFib->df_Flags & FLAG_BITS_MASK))) {
		stats.saveBytesCompressed = dirFib->df_ActualSize - offset;
	} else {
		stats.saveByteCount = offset;
	}
	ResetDisk(devParam, TRUE);
	saveCurLevel = curLevel;
	resetDisk = FALSE;						/* Enable writing, please */
	if (delimiter) {							/* End of tape in middle of delimiter? */
		if (--delimiter) {
			OutputEndMark(curFib->df_NumRead);
		} else {
			OutputDelimiter(TRUE, temp);	/* Initial file marker */
		}
	}
	resetDisk = TRUE;							/* Back what to ResetDisk() had set it to */
/*
	Make sure all entries in tree have their respective locks, since we
	do not keep pass full pathnames around to AmigaDOS (in the interest
	of speedier backups).
*/
	while (curLevel != &rootLevel) {
		curLevel->dl_DirLock = Lock(strPath, ACCESS_READ);
		curLevel = curLevel->dl_ParLevel;
		TruncatePath();
	}
	curLevel = saveCurLevel;
	strcpy(strPath, strSavePath);
	CurrentDir(curLevel->dl_DirLock);
	maxCylinders = (65535 * 32768);		/* Make unreachable again */
	devParam->CurrIOB = 0;					/* Since we're using buf[0] */
	firstTapeSession = 0;
	earlyWarning = FALSE;
}

/*
 * Prompt for next disk or tape.
 */
 
static BOOL DoNextDiskOrTape(DevParamsPtr devParam)
{
	WaitAllIOReq();
	DiskChanged();
	devParam->Changed = FALSE;
	StatusRemove(devParam);
	activeDevice = SelectNextDrive(devParam);
	sessionCurrDisk++;
	activeDisk++;
	stats.saveBytesCompressed = stats.bytesCompressedInFile;
	RecalcDisk(devParam, TRUE);			/* Bump "Insert #" message number */
	PromptForNextDisk();
	return (!abortFlag);
}

/*
 * Prompt user to either insert a disk or replace the current disk.
 */
 
static void PromptForNextDisk()
{
	register DevParamsPtr devParam = activeDevice;
	
	readErr = 0;
	while (!abortFlag && (devParam->Status == STATUS_NOT_READY || devParam->Status == STATUS_REMOVE)) {
		readErr = abortFlag |= (!InsertDisk(devParam, devParam->Status == STATUS_REMOVE ? activeDisk : 0));
		if (devParam->Floppy && (devParam->Unit < 4) && floppy525[devParam->Unit]) {
			StatusCheck(devParam);
		}
	}
}

/*
 * Writes first 14 bytes of first cylinder. Format:
 * DC.B	'Qbnn'		;keeps AmigaDOS from trying to use it
 * DC.B	N				;disk number, 1=origin
 * DC.B	0				;1=alternate catalog wrapped
 * DC.L	DATE			;date of backup, binary days since Jan 1, 1978
 * DC.L	TIME			;time of backup, in binary minutes since midnight
 *
 * New for V5.0 by GM
 * ------------------
 * DS.W	1				;length difference of new structure versus old.
 * DS.L	1				;length of catalog which follows
 * DS.L	1				;length of buffer size to pad between backups (for tape)
 * DS.W	1				;to retain longword align, set to zero
 * DS.W	1				;number of volumes on this backup (currently one)
 * DS.B	1				;compress?
 * DS.B	11				;password (first byte of 0 indicates no password)
 * DS.B	100			;backup comments
 * DS.B	32				;backup name
 *
 * NOTE: The variable "fillPtr" MUST be word aligned, and should be
 * longword aligned just for consistency.
 *
 * Currently, because the size of this structure is pretty large, the old-
 * size block is written for all disks/tapes past the first.
 */
 
static void WriteQBIdentifier()
{
	TextChar buff[4];
	register QBNewFirstCylHdrPtr cylHdr;
	TextPtr bufPtr= &buff[0];
	struct DateStamp *dateStampPtr;
	
	saveCurrDir = process->pr_CurrentDir;
	stats.saveByteCount = stats.byteCount;
	stats.saveComplFiles = stats.completedFiles;
	stats.saveComplBytes = stats.completedBytes;
	saveCurrListItem = currListItem;
	SaveLevelFib();
	
	cylHdr = (QBNewFirstCylHdrPtr) fillPtr;
	if (altCatFlag) {
		*((ULONG *)&cylHdr->id) = QB_CAT_ID;
	} else {
		cylHdr->id = QB_ID;
		if (activeDisk < 10) {
			*bufPtr++ = '0';		/* Put leading zero in if necessary */
		}
		NumToString(activeDisk % 100, bufPtr);
		cylHdr->id2 = *((UWORD *)&buff[0]);
	}
	cylHdr->diskNum = activeDisk;
	cylHdr->altCatWrappedFlag = tapeDevice ? sessionNumber : altCatFlag && (activeCylinder == 0);

	dateStampPtr = (tapeDevice && (activeCylinder == 0)) ? &firstSessionDateStamp : &sessionDateStamp;
	cylHdr->date = dateStampPtr->ds_Days;
	cylHdr->time = dateStampPtr->ds_Minute;
	
	if (inputCompleted || (sessionCurrDisk == 1)) {
		cylHdr->catSize = catSize;
		cylHdr->bufferSize = tapeDevice ? tapeBufLen : trkBufSize;
#ifdef QB_NEW_CATALOG
		cylHdr->version = (inputCompleted ? 2 : 1) | (supportsSetMark ? 0x80 : 0);
#else
		cylHdr->version = 0;
#endif
		cylHdr->unused = 0;
		cylHdr->numVols = 1;
		cylHdr->compress = (backupCompress & COMP_ON_FLAG) ? backupCompress : 0;

		BlockClear(cylHdr->password, MAX_PASSWORD_LEN+1);
		strncpy(cylHdr->password, sessionPassword, MAX_PASSWORD_LEN);
		stccpy(cylHdr->comment, currComment, MAX_BACKUPCOMM_LEN);	
		stccpy(cylHdr->name, currBackupName, MAX_BACKUPNAME_LEN);
		stccpy(cylHdr->volName, currVolName, MAX_BACKUPNAME_LEN);
		fillCount = sizeof(QBNewFirstCylHdr);
	} else {
		fillCount = sizeof(QBFirstCylHdr);
		stats.padBytes += fillCount;
	}
	cylHdr->extraLen = fillCount - sizeof(QBFirstCylHdr);
	fillPtr = ((UBYTE *) fillPtr) + fillCount;
}
	
/*
 * Saves the CurFib at each active level at disk change and reset.
 */

void SaveLevelFib()
{
	register DirLevelPtr level = &rootLevel;
	register DirFibPtr fib = level->dl_CurFib;

	saveCurLevel = curLevel;
	saveCurFib = curFib;
	strcpy(strSavePath, strPath);
	saveIndentLevel = indentLevel;
	while (fib != NULL) {
		level->dl_GNFib = fib;
		if (fib->df_Flags & FLAG_DIR_MASK) {		/* Bottom of tree reached? */
			level = (DirLevelPtr)fib->df_SubLevel;	/* No, so down another level */
			fib = level->dl_CurFib;
		} else {
			fib = NULL;										/* Abort loop */
		}
	}
	return;
}

/*
 * Restores the CurFib at each level at disk change and reset.
 */

void RestoreLevelFib()
{
	register DirLevelPtr level = &rootLevel;
	register DirFibPtr fib = level->dl_GNFib;
	
	strcpy(strPath, strSavePath);
	curLevel = saveCurLevel;
	curFib = saveCurFib;
	indentLevel = saveIndentLevel;
	while (fib != NULL) {
		level->dl_CurFib = fib;
		if (fib->df_Flags & FLAG_DIR_MASK) {
			level = (DirLevelPtr) fib->df_SubLevel;
			fib = level->dl_GNFib;
		} else {
			fib = NULL;
		}
	}
	curLevel->dl_CurFib = curFib;
	return;
}

/*
 * Prompt user for a disk because one wasn't in the drive.
 */
 
BOOL InsertDisk(DevParamsPtr devParam, WORD num)
{
	BOOL success = FALSE;
	register WORD dlg1;

	if (mode == MODE_RESTORE) {
		dlg1 = tapeDevice ? ERR_INSERT_TAPE : ERR_LOAD_DISK_N;
	} else {
		dlg1 = tapeDevice ? ERR_INSERT_TAPE : ERR_INSERT_DISK;
	}
	if (!useAmigaDOS) {
		if (num > 0) {
			dialogVarNum = num;
		} else {
			dialogVarNum = activeDisk;
		}
		dialogVarStr = devParam->DevName;
		success = !DiskSenseDialog(dlg1, TRUE);
	}
	return (success);
}

/*
 * Check out write errors for non-tape devices.
 */
 
static BOOL WriteError(DevParamsPtr devParam, BYTE error, BOOL reset)
{
	register BOOL retry = FALSE;
	BOOL wp = FALSE;
	struct IOStdReq *ioReq = (struct IOStdReq *) devParam->IOB[statusIOBIndex];
	
	if (!abortFlag) {
		ShutDevDown(devParam);
		dialogVarStr = devParam->DevName;
		ioReq->io_Command = TD_PROTSTATUS;
		if (DoIO(ioReq) == 0) {
			wp = ioReq->io_Actual != 0;
		}
		if (wp) {
			StatusRemove(devParam);
			retry = !DiskSenseDialog(ERR_WRITE_PROTECT, TRUE);
		} else {
			if (error == TDERR_BadSecSum) {
				StatusRemove(devParam);
			}
			dialogVarNum = error;
			retry = !DiskSenseDialog(ERR_WRITE_ERR, TRUE);
		}
		if ((catFlag == CAT_WRITING) || inputCompleted) {
			resetDisk = readErr = TRUE;	/* Cause WriteCatalog() to restart */
		} else if (retry && reset) {
			retry = ResetDisk(devParam, FALSE);
		}
		if (!retry) {
			readErr = abortFlag = TRUE;
		}
	}
	return (retry);
}

/*
 * Reset conditions to what they were at the start of this disk when a bad
 * disk is found.
 */
 
static BOOL ResetDisk(DevParamsPtr devParam, BOOL newTape)
{
	BOOL success = TRUE;
			
	AbortBackup();
	if (catFlag != CAT_WRITING && !newTape) {
		if (UnLockLocks(saveCurLevel)) {
			if (saveCurLevel->dl_DirLock = Lock(strSavePath, ACCESS_READ)) {
				CurrentDir(saveCurLevel->dl_DirLock);
			} else {
				success = FALSE;
			}
		} else {
			CurrentDir(saveCurrDir);
		}
	}
	if (success) {
		verifyIOB = devParam->CurrIOB = 0;
		inputCompleted = FALSE;
		activeCylinder = 0;
		if (activeDisk == 1) {
			sessionCurrCylinder = 0;
			encryptSize = 0;
		}
		stats.byteCount = stats.saveByteCount;
		stats.completedFiles = stats.saveComplFiles;
		stats.completedBytes = stats.saveComplBytes;
		stats.rawCompletedBytes = stats.saveRawComplBytes;

		if (!newTape) {
			RestoreLevelFib();
		}
		ResetScrollListAfterDiskError();
		
		ClearSense(devParam);
		if ((catFlag != CAT_WRITING) && activeDisk == 1) {
			success = WriteFirstCatalog();
		} else {
			stats.padBytes = 0;
			InitFirstCylinder();
		}
		if (success) {
			BlockMove(resetData, fillPtr, resetPad);	/* Any delimiter/endmark? */
			fillCount += resetPad;
			if (backupCompress & COMP_ON_FLAG) {
				stats.bytesCompressedInFile = stats.compressBytesInFile = stats.compressBytesInput = 0;
				if (activeDisk > 1) {
					stats.byteCount = curFib->df_Size;
					stats.ignoreCount = stats.saveBytesCompressed;
				}
			} else {
				BlockMove(fillBuffer, readBuffer, fillCount);
				fillPtr = &readBuffer[fillCount];
			}
			UpdateStats();
		}
		readErr = resetDisk = TRUE;		/* If compression, warn it to reset. */
		devParam->Status = STATUS_READY;
	}
	return (success);
}
					
/*
 * Read track just written to make sure it is readable. If not, ask user
 * for another disk.
 */
 
BOOL ReadAfterWrite(register DevParamsPtr devParam, Ptr buffer, ULONG size)
{
	BOOL success = TRUE;
	UBYTE choice;
	register WORD cylinder;

	if (!tapeDevice && !useAmigaDOS) {	
		if ((WaitIOReq(devParam->CurrIOB) == 0)) {
/*
	On verifying, don't bother retrying - any problems should be immediately
 	flagged I feel, so call ReadBuffer() rather than DoReadBuffer(). -GM
*/	
			success = FALSE;
			if (cylinder = activeCylinder) {
				cylinder--;
			}
			if (ReadBuffer(devParam, cylinder) == 0) {
				success = CompareTracks(buffer, readPtr, size) == 0;
				if (!success) {
					dialogVarStr = devParam->DevName;
					choice = WarningDialog(ERR_VERIFY, 1);
					if (choice == 0) {
						if (!ResetDisk(devParam, FALSE)) {
							readErr = abortFlag = TRUE;
						}
					} else {
						readErr = abortFlag = choice == 1;
					} 
				}
			} else {
				HandleErrors(devParam, devParam->CurrIOB);
			}
		} else {
			success = FALSE;
			StatusReady(devParam);
		}
	}
	return (success);
}

/*
 * AmigaDOS suspected on media. Warn user if preferences flag set
 */
 
static BYTE SuspectAmigaDOSFormat(DevParamsPtr devParam)
{
	BYTE choice = 0;
	
	if (!prefs.OldPrefs.Reformat || rewritingCat) {
		if (rewritingCat) {
			dialogVarNum = 1;
			choice = DoDiskSenseDialog(devParam, ERR_INSERT_DISK);
		} else {
			choice = DoDiskSenseDialog(devParam, devParam->Floppy ? ERR_AMIGA_DOS_DISK : ERR_AMIGA_DOS_TAPE);
		}
	}
	return (choice);
}

/*
 * Checks disk just loaded for ADOS identifier. If so, warn user.
 * Returns TRUE if disk OK and user gave permission to overwrite AmigaDOS block,
 * or if not an AmigaDOS disk.
 */
 
BOOL CheckDiskFormat(register DevParamsPtr devParam)
{
	register ULONG firstLong = 0L;
	register BOOL readMore;
	ULONG saveTrkBufSize;
	UBYTE *saveRdBuf;
	WORD saveIOB;
	register WORD i;
	register BYTE result = 0;
	QBFirstCylHdr qbHdr;

	if (!useAmigaDOS) {
		DriveStatus(devParam);
		do {
			saveRdBuf = readBuffer;
/*
	Prevent corruption of IOB by ReadBuffer().
*/
			saveIOB = devParam->CurrIOB;
			if (devParam->BufWait[saveIOB]) {
				devParam->CurrIOB = !devParam->CurrIOB;
			}
			
			if (devParam->Floppy) {
/*
	Try to read the first buffer, being mindful that if it's an unformatted disk,
	it will fail, which means it is OK to write over it.
*/
				if (ReadBuffer(devParam, 0) == 0) {
					firstLong = *((ULONG *) readBuffer);
					firstLong &= 0xFFFFFF00;
					/*firstLong |= ' ';*/
					result = (firstLong != DOS_ID) ? 0 : SuspectAmigaDOSFormat(devParam);
					if (result == -1) {
						result = 0;
					}
				}
			} else {
				if (!tapeDevice) {
					saveTrkBufSize = trkBufSize;
					trkBufSize = blockSize;
					for (readMore = TRUE, i = 0 ; (i < 16) && readMore ; i++) {
						if (readMore = (ReadBuffer(devParam, i) == 0)) {
							if (*((ULONG *)readBuffer) == RDSK_ID) {
								result = SuspectAmigaDOSFormat(devParam);
								readMore = FALSE;
							}
						}
					}
					if ((result <= 0) && (ReadBuffer(devParam, 0) == 0)) {
						result = 0;
						firstLong = *((ULONG *)readBuffer);
					}
					trkBufSize = saveTrkBufSize;
				} else if ((sessionCurrDisk > 1) && (activeCylinder == 0)) {
					readBuffer = (UBYTE *)&qbHdr; /*bufferQueue[!devParam->CurrIOB];*/
					result = 0;
					if (ReadTapeHeader(devParam, readBuffer, sizeof(QBFirstCylHdr))) {
						firstLong = *((ULONG *) readBuffer);
					}
					TapeIO(devParam, CMD_REWIND, TRUE);
					sessionCurrCylinder = activeCylinder = 0;
				}
/*
	If backing up to tape, it would be too inconvenient to check the format,
	especially on the vast majority of drives which are sequential. Therefore,
	this routine does nothing with tape drives.
*/
			}
			devParam->CurrIOB = saveIOB;
			if ((result == 0) && ((firstLong >> 16) == QB_ID)) {
				if (rewritingCat || activeCylinder) {
					if (( ((QBNewFirstCylHdrPtr)readBuffer)->date != sessionDateStamp.ds_Days) ||
						 ( ((QBNewFirstCylHdrPtr)readBuffer)->time != sessionDateStamp.ds_Minute)) {
						result = DoDiskSenseDialog(devParam, ERR_RD_BAD_SET);
					} else if (((QBNewFirstCylHdrPtr)readBuffer)->diskNum != sessionCurrDisk) {
						dialogVarNum = ((QBNewFirstCylHdrPtr)readBuffer)->diskNum;
						dialogVarNum2 = activeDisk;
						result = DoDiskSenseDialog(devParam, ERR_RD_BAD_SEQ);
					}
				} else if (sessionCurrDisk != 1) {
					if (( ((QBNewFirstCylHdrPtr)readBuffer)->date == firstSessionDateStamp.ds_Days) &&
						 ( ((QBNewFirstCylHdrPtr)readBuffer)->time == firstSessionDateStamp.ds_Minute)) {
/*
	If the number is equal to (or greater just in case), then this won't destroy any
	data of this backup, so let it slide.
*/
						dialogVarNum = activeDisk;
						if (((QBNewFirstCylHdrPtr)readBuffer)->diskNum < sessionCurrDisk) {
							result = DoDiskSenseDialog(devParam, tapeDevice ? ERR_QB_TAPE : ERR_QB_DISK);
						}
						if (result == 1) {
							abortFlag = readErr = TRUE;
						}
					}
				}
			} else if (result == 1) {
				result = WriteError(devParam, 0, FALSE) ? -1 : 1;
			}
			readBuffer = saveRdBuf;
		} while (result < 0);
	}
	return (result == 0);
}

/*
 * Call DiskSenseDialog() after shutting down device, and tweak result
 */
 
static BYTE DoDiskSenseDialog(DevParamsPtr devParam, WORD id)
{
	register BYTE result;

	dialogVarStr = devParam->DevName;
	MotorOff(FALSE);
	
	DiskChanged();
	StatusRemove(devParam);
	
	result = DiskSenseDialog(id, (BYTE) -1);
	if (result == 0) {
		result--;
	}
	return (result);
}

