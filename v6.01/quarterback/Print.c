/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Quarterback
 * Copyright (c) 1991 New Horizons Software
 *
 * Print routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <graphics/gfxbase.h>
#include <devices/printer.h>
#include <devices/prtbase.h>
#include <dos/dos.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <clib/alib_protos.h>

#include <string.h>

#include <Toolbox/DateTime.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/ScrollList.h>

#include "QB.h"
#include "Proto.h"

/*
 * External variables
 */

extern MsgPortPtr	mainMsgPort;
extern BOOL 		pagePrinter;
extern UBYTE		printerName[];
extern DlgTemplPtr 	dlgList[];
extern ScreenPtr 	screen;
extern TextChar 	strBuff[], strQBCatalog[], strPage[], strAt[];
extern TextChar	strDirOf[], strLF[], strQBLogfile[];
extern TextPtr		strPath, strSavePath;
extern ProgPrefs 	prefs;
extern UWORD 		tabChars[];
extern WORD			cpi, lpi;
extern DirFibPtr	curFib;
extern DirLevelPtr	curLevel, saveCurLevel;
extern DirLevel	rootLevel;
extern WORD			currListItem, indentLevel, saveIndentLevel;
extern UBYTE		operation;
extern ScrollListPtr	scrollList;

/*
 * Local prototypes
 */
 
static PrintIOPtr	OpenPrinter(void);
static void			ClosePrinter(PrintIOPtr);
static void			SetCancelButton(BOOL);
static WORD			PrintPage(File, WORD, BOOL *);
static BOOL			NextPage(void);
static BOOL			InitCharPrint(File);
static BOOL			AdvanceLines(File, WORD);
static BOOL			PrintSpaces(File, WORD);
static void			BuildListEntryWithSpaces(DirFibPtr, TextPtr, BOOL);

/*
 * Local variables
 */

static struct Preferences  origPrefs;  /* For OpenPrinter() call */
static struct Preferences  lastPrefs;	/* For GetSysPrefs() call */
  
static BOOL havePrinterType;	  		/* To avoid excessive calls to GetPrinterType() */

static DialogPtr cancelDialog = NULL;

/*
#define PITCH_PICA		100
#define PITCH_PICAWIDE	50
#define PITCH_ELITE		120
#define PITCH_ELITEWIDE	60
#define PITCH_COND		150
#define PITCH_CONDWIDE	75
#define PITCH_ULTRA		172
#define PITCH_ULTRAWIDE	86
*/

#define PITCH_TO_DECIPOINTS(pitch)	((7200 + (pitch - 1))/(pitch))	/* Must round up */

/* TextChar styleCommUnderline[]	= "\033[4m"; */
TextChar styleCommItalic[]		= "\033[3m";
TextChar styleCommBold[]		= "\033[1m";
TextChar styleCommNormal[]		= "\033[0m";

/*
 *	Get new preferences values
 *	Return TRUE if preferences changed since last call
 */

BOOL GetSysPrefs()
{
	register WORD i, len;
	struct Preferences newPrefs;

/*
	Check for changed preferences
*/
	GetPrefs(&newPrefs, sizeof(struct Preferences));
	if (memcmp((BYTE *) &newPrefs, (BYTE *) &lastPrefs, sizeof(struct Preferences)) == 0)
		return (FALSE);
	BlockMove(&newPrefs, &lastPrefs, sizeof(struct Preferences));
/*
	Get printer name here
	All other printer defaults are caught by PrintDefault()
*/
	*printerName = '\"';
	len = strlen(lastPrefs.PrinterFilename);
	for (i = len; i; i--) {
		if (lastPrefs.PrinterFilename[i - 1] == ':' ||
			lastPrefs.PrinterFilename[i - 1] == '/')
			break;
	}
	len -= i;
	memcpy(printerName + 1, &lastPrefs.PrinterFilename[i], len);
	printerName[len + 1] = '\"';
	printerName[len + 2] = '\0';
	for (i = 1; i < len + 1; i++) {
		if (printerName[i] == '_')
			printerName[i] = ' ';
	}
	if (printerName[1] >= 'a' && printerName[1] <= 'z')
		printerName[1] -= 'a' - 'A';
	havePrinterType = FALSE;
	return (TRUE);
}

/*
 * Assemble a line of text for adding to a list item
 */

static void BuildListEntryWithSpaces(DirFibPtr dirFib, register TextPtr destPtr, BOOL printing)
{
	TextChar tempBuff[160];
	register TextPtr srcPtr = &tempBuff[0];
	register WORD tabNum = 0;
	register TextChar ch;
	register WORD fieldLen = 0;
	register WORD i;
	BOOL change = FALSE;
	
	BuildListEntry(dirFib, tempBuff);
	
	if( printing && dirFib->df_Flags & (FLAG_SLINK_MASK | FLAG_HLINK_MASK) ) {
		strcpy(destPtr, styleCommItalic);
		destPtr += 4;
		change = TRUE;
	}
	while( ch = *srcPtr++ ) {
		if( (ch == TAB) || (ch == BS) ) {
/*
	If filename has just been printed (always first field), return to normal.
*/
			if( fieldLen ) {
				if( change && (tabNum == 0) ) {
					for( i = 0 ; i < 4 ; i++ ) {
						*destPtr++ = styleCommNormal[i];
					}
					change = FALSE;
				}
				if( ch == TAB ) {
/*
	Pad rest of field with spaces, and then add a few more (left justify).
*/
					for( i = 0 ; i < (tabChars[tabNum] + 2) - fieldLen; i++ ) {
						*destPtr++ = ' ';
					}
				} else {
/*
	Right justify
*/
					destPtr -= fieldLen;
					i = tabChars[tabNum];
					BlockMove(destPtr, destPtr + (i - fieldLen), fieldLen);
					memset(destPtr, ' ', i - fieldLen);
					destPtr += i;
					*destPtr++ = ' ';
					*destPtr++ = ' ';
				}
				fieldLen = 0;
			}
			tabNum++;
		} else {
			fieldLen++;
			*destPtr++ = ch;
		}
	}
	*destPtr++ = LF;
	*destPtr = 0;
}

/*
 * Request the next sheet of paper
 * Return success status
 */

static BOOL NextPage()
{
	WORD item;
	
	SetCancelButton(FALSE);	
	item = StdDialog(DLG_NEXTPAGE);
	SetCancelButton(TRUE);
	return( (BOOL) (item == OK_BUTTON) ) ;
}

/*
 * Advance the specified number of lines
 * Return success status
 */

static BOOL AdvanceLines(File printer, WORD num)
{
	if (num > 0) {
		while (num--) {
			if (Write(printer, strLF, 1) != 1)
				return (FALSE);
		}
	}
	return (TRUE);
}

/*
 * Print the specified number of spaces
 * Return success status
 */

static BOOL PrintSpaces(File printer, WORD num)
{
	if (num > 0) {
		while (num--) {
			if (Write(printer, " ", 1) != 1)
				return (FALSE);
		}
	}
	return (TRUE);
}

/*
 * Print the heading of a directory.
 */
 
WORD WriteDirHeading(File printer, TextPtr buff, BOOL printing)
{
	WORD status = PRINT_ERROR;
	WORD len = prefs.PrintInfo.PageWidth - 1;
	
	buff[0] = 0;
	if( !printing ) {
		buff[0] = LF;
		buff[1] = 0;
	}
/*
	Don't print "Directory of " when saving in hierarchical format...
*/
	if( !prefs.SaveCatFormat ) {
		strcat(buff, strDirOf);
		buff[len] = 0;
	}
	len -= strlen(strDirOf);
	if( len > 0 ) {
		strncat(buff, strPath, len - strlen(strDirOf));
	}
	strcat(buff, strLF);
	len = strlen(buff);
	if( Write(printer, buff, len) == len ) {
		/*prefs.PrintInfo.PrintRec.Flags & PRT_NOGAPS;
		if( AdvanceLines(printer, 1) ) {
			status = PRINT_OK;
		}
		*/
		status = PRINT_OK;
	}
	return(status);
}

/*
 * Write the header found on the beginning of file or every page.
 */
 
BOOL WriteHeader(File printer, BOOL printing)
{
	WORD len;
	struct DateStamp dateStamp;
	TextChar tempBuff[40];
	BOOL success;
	
	strcpy(strBuff, operation == OPER_SELECTION ? strQBCatalog : strQBLogfile);
	DateStamp(&dateStamp);
	DateString(&dateStamp, (WORD) DATE_ABBRDAY, tempBuff);
	strcat(strBuff, tempBuff);
	strcat(strBuff, strAt);
	TimeString(&dateStamp, FALSE, TRUE, tempBuff);
	strcat(strBuff, tempBuff);
	len = strlen(strBuff);
	if( !printing ) {
		strBuff[len++] = LF;
		strBuff[len] = 0;
	}
	success = Write(printer, strBuff, len) == len;
	if( printing && success ) {
		success = AdvanceLines(printer, 2);
	}
	return(success);
}

/*
 * Write a catalog or session log entry to a file or printer
 */
 
WORD WriteEntry(File printer, WORD *line, BOOL *printing)
{
	TextChar buff[PATHSIZE];
	register TextPtr text = &buff[0];
	register WORD len;
	register WORD status = PRINT_OK;
	register WORD temp;
	BOOL heading = FALSE;
	register BOOL isDir;
	
	if( operation == OPER_SELECTION ) {
		if( prefs.SaveCatFormat ) {
			if( (curFib = NextFile(TRUE, TRUE)) == NULL ) {
				status = PRINT_DONE;
			} else {
				if( ((curFib->df_Flags & FLAG_DIR_MASK) != 0) &&
					 ((curFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK)) == 0) ) {
					SortDir();
				}
			}
		}
		if( curFib != NULL ) {
			isDir = ((curFib->df_Flags & FLAG_DIR_MASK) != 0) &&
			 ((curFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK)) == 0);
			BuildListEntryWithSpaces(curFib, text, *printing);
			if( prefs.SaveCatFormat ) {
				len = strlen(curFib->df_Name);
				text[len++] = LF;
				text[len] = 0;
				temp = indentLevel;
				if( !isDir ) {
					temp++;
				}
				if( !*printing ) {
					BlockMove(text, &text[temp], len+1);
					memset(text, TAB, temp);
				} else {
					temp <<= 2;
/* 
	On a printer, just emulate tab w/ four spaces
 */
					BlockMove(text, &text[temp], len+1);
					memset(text, ' ', temp);
				}
			}
			len = strlen(text);
			if( Write(printer, text, len) != len ) {
				status = PRINT_ERROR;
			} else {
				if( isDir ) {
					if( curLevel->dl_CurFib == NULL ) {
						curLevel->dl_CurFib = curFib;
					}
				}
				curFib = curFib->df_Next;
			}
		}
		if( !prefs.SaveCatFormat && status != PRINT_ERROR ) {
			while( (curFib == NULL) && (status == PRINT_OK) ) {
				if( curLevel != NULL ) {
					if( curLevel->dl_CurFib != NULL ) {
						indentLevel++;
						AppendPath(curLevel->dl_CurFib->df_Name);
						curLevel = (DirLevelPtr) curLevel->dl_CurFib->df_SubLevel;
						curLevel->dl_CurFib = NULL;	
						SortDir();
/*
	If not enough room to fit a header line set (1 line + blank), 
	then skip to next page now, else write it now.
*/
						if( *printing ) {
							temp = (prefs.PrintInfo.PageHeight - 4) - *line;
							if( (prefs.PrintInfo.PrintRec.Flags & PRT_NOGAPS) || (temp > 2) ) {
								if( !AdvanceLines(printer, 1) ) {
									status = PRINT_ERROR;
								} else {
									*line += 2;
									status = WriteDirHeading(printer, buff, TRUE);
								}
							} else {
								*line += temp;
								if( !AdvanceLines(printer, temp) ) {
									status = PRINT_ERROR;
								}
								heading = TRUE;
							}
						} else {
							status = WriteDirHeading(printer, buff, FALSE);
						}
						if( curLevel != NULL ) {
							curFib = curLevel->dl_ChildPtr;
						}
					} else {
						curLevel = curLevel->dl_ParLevel;
						TruncatePath();
						indentLevel--;
						if( curLevel != NULL ) {
							do {
								curLevel->dl_CurFib = curLevel->dl_CurFib->df_Next;
							} while( (curLevel->dl_CurFib != NULL) &&
								( ((curLevel->dl_CurFib->df_Flags & FLAG_DIR_MASK) == 0) ||
								((curLevel->dl_CurFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK)) != 0) ) );
						}
					}
				} else {
					status = PRINT_DONE;
				}
			}
		}
		if( printing != NULL ) {
			*printing = heading;
		}
	} else {
		SLGetItem(scrollList, currListItem++, buff);
		len = strlen(&text[2]);
		if( printing ) {
			if( !PrintSpaces(printer, ((text[0] & LFLAG_INDENT_MASK)-1) * 3) ) {
				status = PRINT_ERROR;
			}
		} else {
			text[len++] = LF;
			text[len] = 0;
		}
		if( status != PRINT_ERROR ) {			
			if( Write(printer, &text[2], len) != len ) {
				status = PRINT_ERROR;
			} else if( printing && !AdvanceLines(printer, 1) ) {
				status = PRINT_ERROR;
			} else if( currListItem == SLNumItems(scrollList) ) {
				status = PRINT_DONE;
			}
		}
	}
	return(status);
}

/*
 * Print one page of text, unless NOGAPS on, in which case doesn't
 * return until entire print job is finished.
 * Assume we start one line down from the top
 * Return -1(PRINT_ERROR) if error, 1(PRINT_ABORT) if cancelled, 0 if OK
 */

static WORD PrintPage(File printer, WORD page, BOOL *heading)
{
	register WORD len, numLines;
	TextChar buff[256];
	TextChar tempBuff[20];
	WORD status = PRINT_ERROR;
	BOOL noGaps;
	WORD line;
	
/*
	Print header
*/
	numLines = prefs.PrintInfo.PageHeight - 4;
	noGaps = prefs.PrintInfo.PrintRec.Flags & PRT_NOGAPS;
	line = 0;
	if( WriteHeader(printer, TRUE) ) {
/*
	Print body, beginning with possibly a directory header line.
*/
		if( *heading ) {
			*heading = FALSE;
			if( (status = WriteDirHeading(printer, buff, TRUE)) == PRINT_ERROR ) {
				return(status);
			}
			line++;
		}
		status = PRINT_OK;
/*
	Main print loop
*/
		while( (noGaps || (line < numLines)) && (status == PRINT_OK) && 
					((operation != OPER_SELECTION) || curLevel != NULL) ) {

			if (CheckDialog(mainMsgPort, cancelDialog, DialogFilter) == CANCEL_BUTTON)
				return(PRINT_ABORT);
				*heading = TRUE;			/* WriteEntry() will set its proper value */
			status = WriteEntry(printer, &line, heading);
			line++;
		}
/*
 	Print footer
*/
		if( !noGaps ) {
			if( AdvanceLines(printer, (numLines+1) - line) ) {
				strcpy(buff, strPage);
				NumToString(page, tempBuff);
				strcat(buff, tempBuff);
				len = strlen(buff);
				if( (!PrintSpaces(printer, prefs.PrintInfo.PageWidth - len)) ||
					(Write(printer, buff, len) != len) ) {
					status = PRINT_ERROR;
				}
			} else {
				status = PRINT_ERROR;
			}
		} else {
			if( !AdvanceLines(printer, 1) ) {
				status = PRINT_ERROR;
			}
		}
	}
	return(status);
}

/*
 * Set up global variables to do a print of the catalog
 */
 
void InitCatalogPrint(TextPtr savePath)
{
	saveCurLevel = curLevel;
	curLevel = &rootLevel;
	curFib = rootLevel.dl_ChildPtr;
	rootLevel.dl_CurFib = NULL;
	strcpy(savePath, strPath);
	strcpy(strPath, prefs.OldPrefs.DevPathBuf);
	saveIndentLevel = indentLevel;
	SortDir();
	currListItem = 0;
	indentLevel = 0;
}

/*
 * Match with InitCatalogPrint()
 */
 
void EndCatalogPrint(TextPtr savePath)
{
	strcpy(strPath, savePath);
	curLevel = saveCurLevel;
	indentLevel = saveIndentLevel;
}

/*
 * Print catalog
 */

BOOL PrintList()
{
	register WORD page;
	register BOOL success;
	register File printer;
	register PrintRecPtr printRec = &prefs.PrintInfo.PrintRec;
	register WORD status = PRINT_OK;
	register WORD copies = printRec->Copies;
	DirFibPtr saveFib = curFib;
	BOOL heading;
	TextChar buff[PATHSIZE];

	success = FALSE;	
	BeginWait();
/*
	Open the printer device for output
	Set for no margins and proper setup
*/
/*
	Put up cancel requester
*/
	if( (cancelDialog = GetDialog(dlgList[DLG_CANCELPRINT], screen, mainMsgPort)) != NULL ) {

		SetCancelButton(FALSE);
		if( (printer = Open(printRec->FileName, MODE_NEWFILE)) != NULL ) {
			if( InitCharPrint(printer) ) {
/*
	Print the pages
*/
				success = TRUE;
				SetCancelButton(TRUE);
				while( copies ) {
					page = 1;
					copies--;
					heading = (operation == OPER_SELECTION);
					InitCatalogPrint(buff);
					do {
						status = PrintPage(printer, page, &heading);
						if( status == PRINT_OK ) {
							if( (operation != OPER_SELECTION) || (curLevel != NULL) ) {
								if( (!(printRec->Flags & PRT_TOFILE)) 
									&& printRec->PaperFeed == PRT_CUTSHEET && !NextPage()) {
									status = PRINT_ERROR;
								}
							}
						}
						page++;
					} while( status == PRINT_OK );
					if( status == PRINT_ERROR || Write(printer, "\f", 1) != 1) {
						success = FALSE;
					}
				}
				EndCatalogPrint(buff);
			}
			Close(printer);
		}
		if( cancelDialog != NULL ) {
			DisposeDialog(cancelDialog);
			cancelDialog = NULL;
		}
		if( status == PRINT_ERROR ) {
			Error(ERR_PRINT);
		}
	}
	EndWait();
	
	if( operation == OPER_SELECTION ) {
		curFib = saveFib;
	}
	return (success);
}

/*
 * Initialize printer for character-based operation
 */
 
static BOOL InitCharPrint(register File printer )
{
	register BOOL success = FALSE;
	PrintRecPtr printRec = &prefs.PrintInfo.PrintRec;
	register ULONG height;
	TextChar command[10];
	register WORD len;
			
	if (Write(printer, "\033#3", 3) == 3) { 
		if( !((printRec->Quality == PRT_NLQ &&
			Write(printer, "\033[2\"z", 5) != 5) ||
							(printRec->Quality == PRT_DRAFT &&
							Write(printer, "\033[1\"z", 5) != 5) ) ) {
			if(!((cpi == 10 && Write(printer, "\033[0w", 4) != 4) ||
					 (cpi == 12 && Write(printer, "\033[2w", 4) != 4) ||
					 (cpi == 15 && Write(printer, "\033[4w", 4) != 4))){
				if(!((lpi == 6 && Write(printer, "\033[1z", 4) != 4) ||
					  (lpi == 8 && Write(printer, "\033[0z", 4) != 4))){

/*
	Set form height and turn off perf skip
*/
					success = TRUE;
					if( !pagePrinter ) {
						height = ((LONG) printRec->PaperHeight*6L)/720L;
						if( height > 255 )
							height = 255;
						strcpy(command, "\033[");
						NumToString(height, strBuff);
						strcat(command, strBuff);
						strcat(command, "t");
						len = strlen(command);
/*
	Issue "no perf skip" and "normal character set" commands.
*/
						if(   ( Write(printer, command, len) != len ) ||
								( Write( printer, "\033[0q", 4 ) != 4 ) || 
								( Write( printer, styleCommNormal, 4 ) != 4 ) ) {
							success = FALSE;
						}
					}
				}
			}
		}
	}
	return(success);
}
								 
/*
 *	Enable/disable cancel button and set pointer
 */

static void SetCancelButton(BOOL on)
{
	EnableGadgetItem(cancelDialog->FirstGadget, CANCEL_BUTTON, cancelDialog, NULL, on);
	SetStdPointer(cancelDialog, (WORD) (on ? POINTER_ARROW : POINTER_WAIT));
}

/*
 *	Get the printer type
 */

void GetPrinterType()
{
	register WORD i;
	register PrintIOPtr printIO;
	register struct PrinterData *pData;
	register struct PrinterExtendedData *ped;

	if (!havePrinterType) {
/*
	Set default values
*/
		pagePrinter = FALSE;
/*
	Get printer values
*/
		if ((printIO = OpenPrinter()) == NULL) {
			BeginWait();
			for (i = 0; i < 10; i++) {				/* Retry counter */
				if ((printIO = OpenPrinter()) != NULL)
					break;
				Delay(10L);
			}
			EndWait();
		}
		if (printIO != NULL) {
			pData = (struct PrinterData *) printIO->Std.io_Device;
			if (pData->pd_SegmentData) {
				ped = &pData->pd_SegmentData->ps_PED;
				pagePrinter = (ped->ped_MaxYDots != 0);
			}
			ClosePrinter(printIO);
			havePrinterType = TRUE;
		}
	}
}

/*
 *	Open the printer for direct output
 */

static PrintIOPtr OpenPrinter()
{
	register PrintIOPtr printIO = NULL;
	MsgPortPtr replyPort;
	register struct PrinterData *pData;

	if ((replyPort = CreatePort(NULL, 0)) != NULL) {
		if ((printIO = (PrintIOPtr) CreateExtIO(replyPort, sizeof(PrintIO))) == NULL) {
			DeletePort(replyPort);
		} else {
			if (OpenDevice("printer.device", 0, (IOReqPtr) printIO, 0)) {
				DeleteExtIO((IOReqPtr) printIO);
				DeletePort(replyPort);
				return (NULL);
			}
			pData = (struct PrinterData *) printIO->Std.io_Device;	

			BlockMove(&pData->pd_Preferences, &origPrefs, sizeof(struct Preferences));

			pData->pd_Preferences.PrintImage = IMAGE_POSITIVE;
			if (pData->pd_Preferences.PrintShade == SHADE_BW)
				pData->pd_Preferences.PrintShade = SHADE_GREYSCALE;
			pData->pd_Preferences.PrintLeftMargin = 1;
			pData->pd_Preferences.PrintRightMargin = 255;
			pData->pd_Preferences.PaperLength = 999;
			pData->pd_Preferences.PrintXOffset = 0;
			pData->pd_Preferences.PrintFlags &= ~(CORRECT_RGB_MASK | CENTER_IMAGE |
											  INTEGER_SCALING | DIMENSIONS_MASK |
											  DITHERING_MASK);
			pData->pd_Preferences.PrintFlags |= IGNORE_DIMENSIONS | ORDERED_DITHERING;
		}
	}
	return (printIO);
}

/*
 *	Close the printer device previously openned by OpenPrinter
 */

static void ClosePrinter(PrintIOPtr printIO)
{
	MsgPortPtr replyPort;
	register struct PrinterData *pData;

	pData = (struct PrinterData *) printIO->Std.io_Device;

	BlockMove(&origPrefs, &pData->pd_Preferences, sizeof(struct Preferences));

	replyPort = ((IOReqPtr) printIO)->io_Message.mn_ReplyPort;
	CloseDevice((IOReqPtr) printIO);
	DeleteExtIO((IOReqPtr) printIO);
	DeletePort(replyPort);
}
