/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1992-93 New Horizons Software, Inc.
 *
 *	Dialog definitions
 */

/*
 *	Page setup requester
 */

enum {
	PAPERSIZE_POPUP = 2,
	WIDTH_TEXT,
	HEIGHT_TEXT,
	PICA_RADBTN,
	ELITE_RADBTN,
	CONDENSED_RADBTN,
	SIXLPI_RADBTN,
	EIGHTLPI_RADBTN,
	NOGAPS_BOX,
	PRINTERNAME1_TEXT,
	INCH_RADBTN,
	CM_RADBTN
};

/*
 * Print requester
 */
 
enum {
	NLQ_RADBTN = 2,
	DRAFT_RADBTN,
	AUTOMATIC_RADBTN,
	HANDFEED_RADBTN,
	COPIES_TEXT,
	FONTUP_ARROW,
	FONTDOWN_ARROW,
	FONTNUM_TEXT,
	TOPRINTER_RADBTN,
	TOFILE_RADBTN,
	PRINTERNAME2_TEXT
};

/*
 * Replace file on restore requester
 */
 
enum {
	RENAME_BUTTON,
	REPLACE_BUTTON = 3,
	RENAME_EDIT_TEXT,
	RENAME_STAT_TEXT
};

/*
 *	Volume/catalog scan
 */
 
enum {
	SCANVOL_TEXT = 0,
	SCANDIR_TEXT = 2,
	SCANFILE_TEXT = 3
};

/*
	Tag filter requester items, enumerated here because BumpArrowHeights()
	needed to know which items are the arrows.
*/

enum {
	TAG_LOCAL_BOX = 2,
	INCLUDE_RADBTN, EXCLUDE_RADBTN,
	TAG_BEFORE_BOX, TAG_AFTER_BOX, 
	PATTERN_BOX, TAG_ADD_BUTTON, TAG_DEL_BUTTON,
	PATTERN_LIST, PATTERN_UPARROW, PATTERN_DNARROW, PATTERN_PROP,
	BEFORE_UPARROW, BEFORE_DNARROW, AFTER_UPARROW, AFTER_DNARROW, PATTERN_TEXT,
	BEFORE_TEXT1, BEFORE_TEXT2, BEFORE_TEXT3,
	AFTER_TEXT1, AFTER_TEXT2, AFTER_TEXT3,
	TAG_ARCHIVE_BOX, TAG_LOAD_BUTTON, TAG_SAVE_BUTTON
};

/*
 * Backup/restore source/destination requester
 */
 
enum {
	OPTIONS_BUTTON = 2,
	FLOPPY_RADBTN,
	TAPE_RADBTN,
	FILE_RADBTN,
	DF0_BOX,
	DF1_BOX,
	DF2_BOX,
	DF3_BOX,
	DEVICE_TEXT,
	UNIT_TEXT,
	DEV_UPARROW,
	DEV_DNARROW,
	UNIT_UPARROW,
	UNIT_DNARROW,
	FILE_TEXT,
	CHOOSE_BTN,
	DEV_LABEL,
	UNIT_LABEL,
	DRIVE_TEXT,
	DISPLAY_BOX
};

/*
 * Backup/restore options extensions
 */
 
enum {
	COMPLETE_RADBTN = DISPLAY_BOX+1,
	SELECTIVE_RADBTN
};

enum {
	R_RESTORE_RADBTN = DISPLAY_BOX+1,
	R_TEST_RADBTN,
	R_COMPARE_RADBTN
};

/*
 * Backup options subrequester
 */
 
enum {
	COMP_OFF = 2,				// Beginning of backup-specific gadgets
	COMP_LEVEL,
	COMP_DEV,
	COMPBIT_UPARROW,
	COMPBIT_DNARROW,
	COMPBIT_TEXT,
	PASSWORD_BOX,
	ARCHIVE_BOX,
	VERIFY_BOX,
	REFORMAT_BOX,
	ALLDIRS_BOX
};

/*
 * Restore options
 */
 
enum {
	R_EARLIER_POPUP = 2,		// Restore-specific gadgets
	R_LATER_POPUP,
	R_DATES_ORIG_RADBTN,
	R_DATES_BACK_RADBTN,
	R_DATES_SYS_RADBTN,
	R_EMPTY_BOX,
	R_ARCHIVE_BOX,
	R_MAINTAIN_BOX
};

/*
 * Catalog option requester controls
 */
 
enum {
	FILESIZE_BOX = 2,
	PROTECT_BOX,
	DATEMOD_BOX,
	TIMEMOD_BOX,
	SHOWSECS_BOX,
	SHOWAMPM_BOX,
	DATE1_RADBTN,
	DATE2_RADBTN,
	DATE3_RADBTN,
	DATE4_RADBTN,
	SORT_NAME_RADBTN,
	SORT_SIZE_RADBTN,
	SORT_DATE_RADBTN,
	SEPARATE_DIRS_BOX,
	REVERSE_SORT_BOX
};

/*
 * Session log option requester controls
 */
 
enum {
	LOG_LEVEL1_RADBTN = 2,
	LOG_LEVEL2_RADBTN,
	LOG_LEVEL3_RADBTN,
	LOG_RATE_BOX,
	LOG_COMP_EFF_BOX,
	LOG_NONE_RADBTN,
	LOG_DEFAULTNAME_RADBTN,
	LOG_USERNAME_RADBTN,
	LOG_CHOOSE_BTN,
	LOG_NAME_TEXT,
	LOG_DISPLAY_ON_BOX
};

/*
 * Preferences requester controls
 */
 
enum {
	FLASH1_BOX = 2,
	SOUND1_BOX,
	FLASH2_BOX,
	SOUND2_BOX,
	SAVEICONS_BOX,
	WARN_BOX,
	INCLUDE_DIRS_BOX
};

/*
 * Buffer option requester controls
 */
 
enum {
	BACKBUFFSIZE_LABEL = 2,
	BACKBUFFSIZE_TEXT,
	BACKBUFFSIZE_UPARROW,
	BACKBUFFSIZE_DNARROW,
	RESTBUFFSIZE_LABEL,
	RESTBUFFSIZE_TEXT,
	RESTBUFFSIZE_UPARROW,
	RESTBUFFSIZE_DNARROW,
	FAST_RADBTN,
	ANY24BIT_RADBTN,
	CHIP_RADBTN,
	ASYNC_BOX
};

/*
 * Tape options
 */
 
enum {
	AUTO_RET_BOX = 2,
	REWIND_BOX
};

/*
 * Tape control
 */
 
enum {
	ERASE_BUTTON = 2,
	RETENSION_BUTTON,
	REWIND_BUTTON,
	SPACE_BUTTON,
	NUMSPACE_UPARROW,
	NUMSPACE_DNARROW,
	QUICK_BOX,
	NUMSPACE_TEXT
};

/*
 * Tape session info
 */
 
enum {
	DATE_LABEL = 2,
	NAME_TEXT,
	COMMENT_TEXT
};

/*
 * SCSI Interrogator
 */
 
enum {
	VENDOR_TEXT = 4,
	MODEL_TEXT,
	REV_TEXT,
	DRIVETYPE_TEXT,
	BLKSIZE_TEXT,
	MEDIASIZE_TEXT,
	SCSIDEV_TEXT,		// Must be same numbered item as backup/restore options
	SCSIUNIT_TEXT,
	SCSIDEV_UPARROW,
	SCSIDEV_DNARROW,
	SCSIUNIT_UPARROW,
	SCSIUNIT_DNARROW
};

/*
 *	About box
 */

enum {
	USERNAME_STATTEXT = 1,
	COMPANYNAME_STATTEXT,
	SERIALNUM_STATTEXT,
	ABOUT_BORDER = 9,
	LICENSE_STATTEXT
};
