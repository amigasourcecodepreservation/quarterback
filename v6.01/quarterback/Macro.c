/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	REXX interface routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <clib/alib_protos.h>

#include <stdio.h>
#include <string.h>

#include <rexx/storage.h>
#include <rexx/rxslib.h>
#include <rexx/errors.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/AREXX.h>

// #include <IFF/WORD.h>

#include "QB.h"
#include "Proto.h"
#include "Version.h"

/*
 *	AREXX definitions and prototypes
 */

typedef struct RexxMsg	RexxMsg, *RexxMsgPtr;

ULONG			InitPort(MsgPortPtr, TextPtr);
void			FreePort(MsgPortPtr);
BOOL			IsRexxMsg(RexxMsgPtr);
RexxMsgPtr	CreateRexxMsg(MsgPortPtr, TextPtr, TextPtr);
BOOL			FillRexxMsg(RexxMsgPtr, ULONG, ULONG);
TextPtr		CreateArgstring(TextPtr, ULONG);
void			ClearRexxMsg(RexxMsgPtr, ULONG);
void			DeleteRexxMsg(RexxMsgPtr);

/*
 *	External variables
 */

extern struct RexxLib *RexxSysBase;

extern MsgPort		rexxMsgPort;
extern MsgPortPtr	mainMsgPort;

extern ScreenPtr	screen;
extern WindowPtr	backWindow;

extern WindowPtr	cmdWindow;

extern BOOL 		inMacro, fromCLI;

extern TextChar	strProgName[];
extern TextChar	strFKeysName[];
extern TextChar	strAutoExecName[];
extern TextChar	macroNames1[10][MACRONAME_LEN], macroNames2[10][MACRONAME_LEN];

extern FKey			fKeyTable1[], fKeyTable2[];

extern TextChar	strBuff[], macroDrawerName[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

#define MACRONAME_TEXT	2
#define MACROARG_TEXT	3

#define SAVE_BUTTON	2
#define NAME1_TEXT	3

#define MAX_ARGS	15				// Max number of args to macro

static TextChar	progPortName[15];

static UWORD		rexxModifiers;	// Inited to 0

static RexxMsgPtr	macroMsg;		// For macro commands (Inited to NULL)

/*
 *	Local prototypes
 */

static void	CreateMacroPathName(TextPtr, TextPtr);
static BOOL	DoOtherMacro(void);
static void	SaveFKeys(void);
static BOOL	DoCustomizeMacro(WindowPtr);

ULONG InitPort(MsgPortPtr port, TextPtr name)
{
	ULONG success = FALSE;
	int signal_bit;

	if(port != NULL)
	{
		memset(port,0,sizeof(*port));

		port->mp_Node.ln_Name	= name;
		port->mp_Node.ln_Type	= NT_MSGPORT;
		port->mp_Flags		= PA_IGNORE;

		NewList(&port->mp_MsgList);

		signal_bit = AllocSignal(-1);
		if(signal_bit > 0)
		{
			port->mp_SigBit		= signal_bit;
			port->mp_SigTask	= FindTask(NULL);

			success = TRUE;
		}
	}

	return(success);
}

void FreePort(MsgPortPtr port)
{
	if(port != NULL)
	{
		if(port->mp_SigBit > 0)
			FreeSignal(port->mp_SigBit);
	}
}

/*
 *	Open REXX library and init REXX port
 *	If REXX port already exists, then do not init REXX interface
 */

void InitRexx()
{
	WORD portNum;

/*
	Open REXX
*/
	RexxSysBase = (struct RexxLib *) OpenLibrary("rexxsyslib.library", 0);
	if (RexxSysBase == NULL)
		return;
/*
	Find unused port name and set up port
*/
	portNum = -1;
	do {
		strcpy(progPortName, strProgName);
		portNum++;				// Compiler bug, this must go after strcpy()
		if (portNum > 0) {
			NumToString((LONG) portNum, strBuff);
			strcat(progPortName, ".");
			strcat(progPortName, strBuff);
		}  
	} while (FindPort(progPortName));
	InitPort(&rexxMsgPort, progPortName);
	AddPort(&rexxMsgPort);
}

/*
 *	Shut down REXX port and close library
 */

void ShutDownRexx()
{
	if (RexxSysBase) {
		RemPort(&rexxMsgPort);
		FreePort(&rexxMsgPort);
		CloseLibrary((struct Library *)RexxSysBase);
	}
}

/*
 *	Handle REXX messages
 */

void DoRexxMsg(RexxMsgPtr rexxMsg)
{
	if (rexxMsg == macroMsg) {			// Message reply?
		if (macroMsg->rm_Result1 != RC_OK)
			Error(ERR_MACRO_FAIL);
		ClearRexxMsg(macroMsg, (macroMsg->rm_Action & 0xFF) + 1);
		DeleteRexxMsg(macroMsg);
		macroMsg = NULL;
		rexxModifiers = 0;
		inMacro = FALSE;
		SetAllMenus();
	} else {
		if( IsRexxMsg(rexxMsg) ) {
			rexxMsg->rm_Result1 = ProcessCommandText(rexxMsg->rm_Args[0], rexxModifiers, (rexxMsg->rm_Action & RXFF_RESULT) != 0, &rexxMsg->rm_Result2);
			if( rexxMsg->rm_Result2 ) {
				rexxMsg->rm_Result2 = (LONG) CreateArgstring((TextPtr)rexxMsg->rm_Result2, (ULONG)strlen((TextPtr)rexxMsg->rm_Result2) );
			}
		}
		ReplyMsg((MsgPtr) rexxMsg);
	}
}

/*
 *	Create full path name to macro
 */

static void CreateMacroPathName(TextPtr macroName, TextPtr buff)
{
	WORD	i, len;
	BOOL	hasPath;

	hasPath = FALSE;
	len = strlen(macroName);
	for (i = 0; i < len; i++) {
		if (macroName[i] == ':') {
			hasPath = TRUE;
			break;
		}
	}
	if (hasPath)
		strcpy(strBuff, macroName);
	else {
		SetPathName(strBuff, macroDrawerName, FALSE);
		AppendDirPath(strBuff, macroName);
	}
}

/*
 *	Return TRUE if filename is an existing macro file
 */

BOOL IsMacroFile(TextPtr fileName)
{
	BOOL		success;
	File		file;
	TextChar	commentText[2];

	CreateMacroPathName(fileName, strBuff);
	if ((file = Open(strBuff, MODE_OLDFILE)) == NULL)
		return (FALSE);
	success = (Read(file, commentText, 2) == 2 &&
				commentText[0] == '/' && commentText[1] == '*');
	Close(file);
	return (success);
}

/*
 *	Initiate AREXX macro
 *	If macroMsg is not NULL then we are already executing a macro, so ignore
 *	This routine will use a copy of macroName
 */

void DoMacro(TextPtr macroName, WORD numArgs, TextPtr args[])
{
	WORD		i;
	BOOL		success;
	MsgPortPtr	rexxPort;

	if (RexxSysBase == NULL || macroMsg)
		return;
/*
	Add path to program if no path specified
*/
	CreateMacroPathName(macroName, strBuff);
/*
	Find REXX port and start macro
*/
	if (numArgs > MAX_ARGS)
		numArgs = MAX_ARGS;
	numArgs++;					// Include command entry
	success = FALSE;
	macroMsg = (RexxMsgPtr) CreateRexxMsg(&rexxMsgPort, NULL, progPortName);
	if (macroMsg == NULL)
		goto Error;
	macroMsg->rm_Action = RXFUNC + (numArgs - 1);
	if (!fromCLI)
		macroMsg->rm_Action |= (1 << RXFB_NOIO);
	macroMsg->rm_Args[0] = (STRPTR) strBuff;
	for (i = 1; i < numArgs; i++)
		macroMsg->rm_Args[i] = (STRPTR) args[i - 1];
	if (!FillRexxMsg(macroMsg, numArgs, 0))
		goto Error;
	Forbid();
	rexxPort = FindPort("REXX");
	if (rexxPort)
		PutMsg(rexxPort, (MsgPtr) macroMsg);
	Permit();
	if (rexxPort)
		inMacro = success = TRUE;
	else
		ClearRexxMsg(macroMsg, numArgs);
Error:
	if (!success) {
		if (macroMsg) {
			DeleteRexxMsg(macroMsg);
			macroMsg = NULL;
		}
		Error ((rexxPort) ? ERR_NO_MEM : ERR_NO_REXX);
	}
}

/*
 *	Do startup macro
 *	If macro given on command line, execute it
 *	Otherwise run auto exec macro
 *	If auto exec macro does not exist, don't report an error
 */

void DoAutoExec(int argc, char *argv[])
{
	WORD	i, numArgs;
	LONG	lock;
	TextPtr	macroName, args[MAX_ARGS];

	if (RexxSysBase == NULL)
		return;
/*
	First look for command-line macro
*/
	macroName = NULL;
	if (fromCLI) {
		numArgs = 0;
		for (i = 1; i < argc && numArgs < MAX_ARGS; i++) {
			if (argv[i][0] == '-')
				continue;
			if (macroName == NULL) {
				if (IsMacroFile(argv[i]))
					macroName = argv[i];
			}
			else
				args[numArgs++] = argv[i];
		}
		if (macroName)
			DoMacro(macroName, numArgs, args);
	}
/*
	If not found, do auto-exec macro (if it exists)
*/
	if (macroName == NULL) {
		CreateMacroPathName(strAutoExecName, strBuff);
		if ((lock = Lock(strBuff, ACCESS_READ)) != NULL) {
			UnLock(lock);
			DoMacro(strAutoExecName, 0, NULL);
		}
	}
}

/*
 *	Get macro name from user and execute
 */

static BOOL DoOtherMacro()
{
	WORD		numArgs;
	BOOL		success;
	TextChar	macroName[GADG_MAX_STRING], arg[GADG_MAX_STRING];

/*
	Get macro name from user
*/
	BeginWait();
	success = GetMacroName(screen, mainMsgPort, DialogFilter, macroName, arg);
	numArgs = (arg[0] != '\0') ? 1 : 0;
	EndWait();
	if (success)
		DoMacro(macroName, numArgs, (TextPtr *) &arg);
	return(success);
}

/*
 *	Save FKeys file
 */

static void SaveFKeys()
{
	WORD i, version;
	ULONG fileID;
	BOOL success = FALSE;
	File file;
	TextChar pathName[200];

	SetPathName(pathName, strFKeysName, TRUE);
	if ((file = Open(pathName, MODE_NEWFILE)) == NULL)
		goto Exit;
/*
	Write fileID and version
*/
	fileID = ID_FKEYSFILE;
	version = PROGRAM_VERSION;
	if (Write(file, &fileID, sizeof(ULONG)) != sizeof(ULONG) ||
		Write(file, &version, sizeof(WORD)) != sizeof(WORD))
		goto Exit;
/*
	Write FKey tables and data
*/
	for (i = 0; i < 10; i++) {
		if (Write(file, &fKeyTable1[i], sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKeyTable1[i].Type == FKEY_MACRO &&
			Write(file, fKeyTable1[i].Data, MACRONAME_LEN) != MACRONAME_LEN)
			goto Exit;
	}
	for (i = 0; i < 10; i++) {
		if (Write(file, &fKeyTable2[i], sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKeyTable2[i].Type == FKEY_MACRO &&
			Write(file, fKeyTable2[i].Data, MACRONAME_LEN) != MACRONAME_LEN)
			goto Exit;
	}
	success = TRUE;
/*
	All done
*/
Exit:
	if (file)
		Close(file);
	if (success)
		SaveIcon(pathName, ICON_FKEYS);
}

/*
 *	Load FKeys file
 */

void LoadFKeys()
{
	WORD i, version;
	ULONG fileID;
	File file;
	FKey fKey;
	TextChar macroName[32], pathName[200];

	SetPathName(pathName, strFKeysName, TRUE);
	if ((file = Open(pathName, MODE_OLDFILE)) == NULL)
		goto Exit;
/*
	Read fileID and version
*/
	if (Read(file, &fileID, sizeof(ULONG)) != sizeof(ULONG) ||
		Read(file, &version, sizeof(WORD)) != sizeof(WORD) ||
		fileID != ID_FKEYSFILE || version != PROGRAM_VERSION)
		goto Exit;
/*
	Write FKey tables and data
*/
	for (i = 0; i < 10; i++) {
		if (Read(file, &fKey, sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKey.Type == FKEY_MACRO &&
			Read(file, macroName, MACRONAME_LEN) != MACRONAME_LEN)
			goto Exit;
		fKeyTable1[i] = fKey;
		if (fKey.Type == FKEY_MACRO) {
			strncpy(macroNames1[i], macroName, MACRONAME_LEN);
			fKeyTable1[i].Data = macroNames1[i];
		}
	}
	for (i = 0; i < 10; i++) {
		if (Read(file, &fKey, sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKey.Type == FKEY_MACRO &&
			Read(file, macroName, MACRONAME_LEN) != MACRONAME_LEN)
			goto Exit;
		fKeyTable2[i] = fKey;
		if (fKey.Type == FKEY_MACRO) {
			strncpy(macroNames2[i], macroName, MACRONAME_LEN);
			fKeyTable2[i].Data = macroNames2[i];
		}
	}
/*
	Set macro menu
*/
	for (i = 0; i < 10; i++)
		SetMenuItem(backWindow, MENUITEM(MACRO_MENU, i, NOSUB), macroNames2[i]);
/*
	All done
*/
Exit:
	if (file)
		Close(file);
}

/*
 *	Get custom macro names and insert in menu
 */

static BOOL DoCustomizeMacro(WindowPtr window)
{
	WORD i, item;
	DialogPtr dlg;
	TextChar newNames[10][MACRONAME_LEN];

/*
	Get macro name from user
*/
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_CUSTOMMACRO], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(dlg);
	do {
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
	} while (item != OK_BUTTON && item != CANCEL_BUTTON && item != SAVE_BUTTON);
	for (i = 0; i < 10; i++) {
		GetEditItemText(dlg->FirstGadget, NAME1_TEXT + i, strBuff);
		strncpy(newNames[i], strBuff, MACRONAME_LEN);
		newNames[i][MACRONAME_LEN-1] = '\0';
	}
	DisposeDialog(dlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Insert macro names into menu
*/
	for (i = 0; i < 10; i++) {
		strncpy(macroNames2[i], newNames[i], MACRONAME_LEN);
		SetMenuItem(backWindow, MENUITEM(MACRO_MENU, i, NOSUB), macroNames2[i]);
	}
	if (item == SAVE_BUTTON) {
		SetStdPointer(cmdWindow, POINTER_WAIT);
		SaveFKeys();
	}
	return (TRUE);
}

/*
 *	Handle macro menu
 */

BOOL DoMacroMenu(WindowPtr window, UWORD item, UWORD sub)
{
	BOOL success = FALSE;

	if (RexxSysBase != NULL && !inMacro) {
		if (item >= 0 && item <= 9) {
			DoMacro(macroNames2[item], 0, NULL);
			success = TRUE;
		}
		else if (item == OTHERMACRO_ITEM)
			success = DoOtherMacro();
		else if (item == CUSTOMMACRO_ITEM)
			success = DoCustomizeMacro(window);
		/* else failure */
	}
	return (success);
}
