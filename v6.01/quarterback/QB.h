/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Quarterback
 * Copyright (c) 1993 New Horizons Software, Inc.
 *
 * Quarterback include definitions
 */

#ifndef DEVICES_PRINTER_H
#include <devices/printer.h>
#endif

#ifndef EXEC_INTERRUPTS_H
#include <exec/interrupts.h>
#endif

#ifndef TYPEDEFS_H
#include <TypeDefs.h>
#endif

/*
#ifdef TOOLBOX_COLOR_H
typedef RGBColor	ColorTable[];
typedef ColorTable	*ColorTablePtr;
#endif
*/

#include <Toolbox/Language.h>

#include "Print.h"
#include "Version.h"

#define QB_NEW_CATALOG		1

#define MAX_FILENAME_LEN	31		/* Maximum length of our filenames, including NULL */
#define MAX_COMMENT_LEN		81		/* Maximum comment length, including NULL */
#define FILEBUFF_SIZE		512 	/* Load/Save buffer size */
#define MACRONAME_LEN		32		/* Customizable macro name length */
#define MAX_LOG_STRLEN		100	/* Maximum the gadget can hold anyway */
#define MAX_BACKUPNAME_LEN	40		/* Maximum length of name, volName fields */
#define MAX_BACKUPCOMM_LEN 100	/* Maximum the gadget can hold anyway */

#define PATHSIZE			512	/* Size of pathname buffers */
#define TMPSIZE			550	/* Must be at least 30 more than PATHSIZE */
#define DEVICE_PATH_BUFFER_SIZE 120	/* Device:path buffer */

#define MAX_PASSWORD_LEN	10		/* Maximum string length of password */
#define MAX_LIST_ITEMS		32000	/* Maximum any scroll list can handle */

#define COMPRESS_BUFSIZE		32768
#define MIN_COMPRESS_BUFSIZE	4096

#define MODE_LIMBO		0
#define MODE_BACKUP		1
#define MODE_RESTORE	2
#define MODE_CATREAD	3
#define MODE_QUIT		4

#define OPER_LIMBO		0
#define OPER_SCAN		1
#define OPER_SELECTION	2
#define OPER_IN_PROG	3
#define OPER_PREP		4
#define OPER_PAUSE		5
#define OPER_COMPLETE	6

#define MAX_NUM_BUFS	2
#define NUM_BUFS		2

/*
 *	Flags for dirFib
 */

#define FLAG_DIR_MASK	128
#define FLAG_SEL_MASK	64
#define FLAG_ERR_MASK	32
#define FLAG_ODD_MASK	16		// Obsolete, reserved
#define FLAG_PART_MASK	8
#define FLAG_HLINK_MASK	4
#define FLAG_SLINK_MASK	2
#define FLAG_BITS_MASK	1		// Backup-Compress or not, Restore-read-protected?

#define IS_DIR(x)	( (x & FLAG_DIR_MASK) && ((x & (FLAG_SLINK_MASK | FLAG_HLINK_MASK)) == 0) )

/*
 *	Flags for list manager
 */

#define LFLAG_SEL_MASK		128 
#define LFLAG_PART_MASK		64
#define LFLAG_EMPTY_MASK	32
#define LFLAG_ERROR_MASK	16
#define LFLAG_INDENT_MASK	(8|4|2|1)

#define LFLAG_ITALIC_MASK	128
#define LFLAG_DUMMY_MASK	64
#define LFLAG_ID_MASK		31

/* Compression flags */
#define MIN_COMPBIT			12
#define MAX_COMPBIT			16
#define COMP_BIT_MASK		0x3F
#define COMP_ON_FLAG		0x80
#define COMP_DEV_FLAG		0x40

#define PRINT_OK		0
#define PRINT_ABORT		1
#define PRINT_DONE		2
#define PRINT_ERROR		-1

enum {
	ICON_TEXT,	ICON_PREFS,	ICON_FKEYS,	ICON_BACKUP
};

enum {
	CAT_DONE, CAT_WRITING,	CAT_READING,	CAT_REREAD
};

enum {
	LOG_RESULTS,	LOG_DRAWERS,	LOG_FULL
};

enum {
	LOGNAME_NONE,	LOGNAME_DEF,	LOGNAME_USER
};

enum {
	DEST_FLOPPY,	DEST_TAPE,	DEST_FILE
};

enum {
	RMODE_RESTORE,	RMODE_TEST,		RMODE_COMPARE,	RMODE_SCAN,
};

#define RMODE_MAX	RMODE_COMPARE		/* May want to change this when desired */

#define RDSK_ID	0x5244534B
#define DOS_ID		0x444F5300
#define QB_OLD_ID	0x5142
#define QB_ID		0x5162
#define QB_CAT_ID	0x51626332			/* Qbc2 */
#define QB_O_CAT_ID	0x516200C2			/* early V5.0 goof */
#define QB_OLDE_ID	0x51627632			/* antique ID - back to V2.0 */
#define CONT_ID	0x434F4E54			/* CONT */
#define FILE_ID	0x464D524B			/* FMRK */
#define CFIL_ID	0x43464D00			/* CFM and compression level */
#define IMAGE_ID	0x494D524B			/* IMRK */
#define CIMG_ID	0x43494D00			/* CIM and compression level */
#define ENDDATA_ID	0x454E4444			/* ENDD */
#define ENDBACKUP_ID	0x454E4442			/* ENDB */

#define BUSY_ID	0x42555359			/* BUSY */
#define ID_PREFSFILE	0x06660137L
#define ID_FKEYSFILE	0x0666013AL


#define DELIMITER_SIZE			40

/*
 *	Window gadget definitions
 */

enum {
	BUTTON_1,		BUTTON_2,		LEFT_ARROW,		RIGHT_ARROW,
	HORIZ_SCROLL,	SCROLL_LIST,
	UP_ARROW,		DOWN_ARROW,		VERT_SCROLL,	DESCR_TEXT,
	DESCR_BORDER,	BUTTON_ENTER,	BUTTON_PARENT,
	BUTTON_TAG,		BUTTON_UNTAG,
	TOT_FILES_TEXT,	TOT_BYTES_TEXT,SEL_FILES_TEXT,	SEL_BYTES_TEXT,
	TOTAL_TEXT,		TOTAL_BOX,		FILES1_TEXT,	BYTES1_TEXT,
	SEL_TEXT,		SEL_BOX,			FILES2_TEXT,	BYTES2_TEXT,
	RUNTIME_ITEMS,	COMPL_PCT_TEXT,DRIVE_TEXTBOX,	DRIVE_ICON_BORDER,
	LAST_WINDOW_GADGET
};

#define FIRST_EXT_GAD	BUTTON_ENTER
#define SKIP_BUTTON		2

/*
	Mini-icon definitions
*/

#define NUM_MINI_ICONS		20		/* Mini-icons for disk, file, drawer, checkmark */

enum {
	VOL_ICON,		DRAWER_ICON,		DOC_ICON,	HD_ICON,	SLINK_ICON,
	TAPE_ICON,
	VOL_ICON_MASK,	DRAWER_ICON_MASK,	DOC_ICON_MASK,	HD_ICON_MASK,	SLINK_ICON_MASK,
	TAPE_ICON_MASK,
	CHECK_ICON_MASK,	DRAWER_ICON_GRAY, DOC_ICON_GRAY,	DOC_ICON_BLACK,	VOL_ICON_GRAY,
	VOL_ICON_BLACK,	TAPE_ICON_GRAY,	TAPE_ICON_BLACK
};

/*
 * Dialog ID's
 */

enum {
	DLG_ABOUT,		DLG_DESTFILE,	DLG_CUSTOMMACRO,
	DLG_USERREQ1,	DLG_USERREQ2,	DLG_USERREQ3,	DLG_USERREQTEXT,
	DLG_CANCELPRINT,	DLG_NEXTPAGE
};

/*
 * Requester ID's
 */

enum {
	REQ_SCAN,		REQ_ERROR,		REQ_WARN,		REQ_SKIP,
	REQ_BACKOPTS,	REQ_BACKOPTS2,	REQ_RESTOPTS,	REQ_RESTOPTS2,
	REQ_CATOPTS,	REQ_LOGOPTS,
	REQ_PREFS,		REQ_TAPECONTROL,REQ_PAGESETUP,	REQ_PRINT,
	REQ_PROMPTPASS,	REQ_ASKPASS,	REQ_TAGFILTER,	REQ_TAPEOPTS,
	REQ_TAPEINFO,	REQ_RENAMEFILE,REQ_REWIND,		REQ_TAPEWAIT,
	REQ_BUFFOPTS,	REQ_GETSESSINFO,REQ_SETSESSINFO,	REQ_SEEKALTCAT
};

/*
 *	Color register numbers for possible colors
 *	Requirements are for:
 *		Blue is color 0
 *		White is color 1
 *		Black is inverse of white (ie color 6)
 *		Red is inverse of blue (ie color 7)
 */

enum {
	COLOR_BLUE,		COLOR_WHITE,		COLOR_GREEN,	COLOR_CYAN,
/*	COLOR_DKRED,	COLOR_DKYELLOW,	COLOR_DKGREEN,	COLOR_DKCYAN,
	COLOR_DKBLUE,	COLOR_DKMAGENTA,	COLOR_LTGRAY,	COLOR_DKGRAY,
*/
	COLOR_YELLOW,	COLOR_MAGENTA,		COLOR_BLACK,	COLOR_RED
};


/*
 *	Error numbers
 */

enum {
	INIT_BADSYS = 0,	INIT_NOSCREEN = 3,	INIT_NOMEM,	INIT_ALREADY_UP, INIT_NOWIN
};

enum {
	ERR_UNKNOWN,
	ERR_NO_MEM,			/* Not Enough Memory */
	ERR_OPEN,			/* Unable to load file */
	ERR_SAVE,			/* Unable to save file */
	ERR_PRINT,  		/* Unable to print file */
	ERR_BAD_COPIES,		/* Invalid number of copies to print */
	ERR_PAGE_SIZE,		/* Bad page size */
	ERR_BACKUP_FILE,		/* Can't backup file */
	ERR_RESTORE_FILE,		/* Can't restore file */
	ERR_OPEN_DIR,		/* Couldn't open drawer */
	ERR_CREATEDIR,		/* Couldn't create directory */
	ERR_INVALID_FILENAME,/* Filename contained slash! */
	ERR_INVALID_DIRNAME,	/* Dirname contained slash! */
	ERR_DOS_ERROR,		/* Miscellaneous DOS error */
	ERR_WRITE_PROTECT,	/* Unable to write to disk- write protected, dummy! */
	ERR_WRITE_ERR,		/* Any other problem writing to disk */
	ERR_BAD_DISK,		/* Bad disk */
	ERR_HOSED_DISK,		/* Bad disk, we're gone! */
	ERR_COMP_NOMEM,		/* No memory for compression table. Continue? */
	ERR_NO_FILES,		/* File exists already for restore */
	ERR_AMIGA_DOS_DISK,	/* Warn user about to zap AmigaDOS disk */
	ERR_TRY_ALT_CAT,		/* Error reading QB catalog- try alternate? */
	ERR_RD_TRACK_ZERO,	/* Error reading catalog - track zero bad */
	ERR_RD_NOT_QB,		/* Error reading disk - not QB disk */
	ERR_RD_BAD_SEQ,		/* Error reading disk - not correct num */
	ERR_RD_BAD_SET,		/* Error reading disk - wrong backup set */
	ERR_RD_PASSWORD,		/* Error reading disk - password incorrect */
	ERR_LOAD_DISK_N,		/* Load disk #n into drive x. */
	ERR_LOAD_DISK_LAST,	/* Load last disk into drive x. */
	ERR_DISK_FULL,		/* Disk is full */
	ERR_CORRUPT_COMPRESS,/* Unable to restore - compression file corrupt */
	ERR_CANCEL_SELMODE,	/* Warning that cancel will lose tags */
	ERR_NO_DRIVES,		/* Dummy! No drives selected for backup/restore! */
	ERR_DEATH_WISH,		/* User thought he could format his hard disk! */
	ERR_RESET_OPTS,		/* Warn that all options will be reset */
	ERR_VERIFY,			/* Read after write failed. Reset disk? */
	ERR_INVALID_DATE,		/* Bad date parameter in tag filter requester */
	ERR_TAPE_BUSY,		/* Tape drive not ready */
	ERR_BAD_DEVICE,		/* Bad device description for tape (bad ID or exec.device) */
	ERR_TWRITE_PROTECT,	/* Tape is write-protected */
	ERR_ERASE_WARN,		/* Warn user is about to erase tape! */
	ERR_RETENSION_WARN,	/* Warn user this will take a while */
	ERR_NOT_REMOVABLE,	/* Can't erase/retension a non-removable drive, idiot!! */
	ERR_NO_SCSI_DRIVE,	/* Tape drive not found */
	ERR_TP_TRACK_ZERO,	/* Error reading catalog - block zero bad */
	ERR_TP_NOT_QB,		/* Error reading tape - not QB tape */
	ERR_TP_BAD_SEQ,		/* Error reading tape - not correct num */
	ERR_TP_BAD_SET,		/* Error reading tape - wrong backup set */
	ERR_DRIVES_MIXED,		/* Disk drives have different capacites, a no-no. */
	ERR_TAPE_BAD,		/* Unable to backup to tape for some reason. */
	ERR_PROT_ERR,		/* Unable to update protection bits. */
	ERR_FILE_EXISTS,		/* Backup destination file already exists. Overwrite? */
	ERR_ERASE_FAILED,		/* Erase Tape failed */
	ERR_AMIGA_DOS_TAPE,	/* Removable has AmigaDOS partition(s)! Proceed? */
	ERR_INSERT_TAPE,		/* Insert a tape */
	ERR_INSERT_DISK,		/* Insert a disk into dfx: */
	ERR_QB_TAPE,		/* This is a backup tape. Put a different one in */
	ERR_QB_DISK,		/* This is a backup disk. Put a different one in */
	ERR_ABORT_CHECK,		/* Sure you want to abort? */
	ERR_STR_TOO_BIG,		/* String too large to fit into variable */
	ERR_SAVELOG_FAIL,		/* Unable to save session log file */
	ERR_STRANGE_DRIVE,	/* A tape drive that's apparently not removable! */
	ERR_READ_FAIL,		/* Failed to read backup block */
	ERR_TP_RD_FAIL,		/* Failed to read backup block on tape */
	ERR_CORRUPT_CAT,		/* SCSI Interrogator failed */
	ERR_SAVE_PREFS,		/* Warn user save prefs chosen */
	ERR_NO_MEM_BUFFER,	/* No memory for that size a buffer */
	ERR_IS_DRAWER,		/* AmigaDOS filename supplied is an existing drawer */
	ERR_PATH_TOO_LONG,	/* Operation aborted due to pathname being too long */
	ERR_CORRUPT_CONT,		/* Corrupt delimiter- proceed or die? */
	ERR_CORRUPT_OR_NOMEM,/* Corrupt data attempting to decompress, but maybe lowmem */
	ERR_DIFFNAME,		/* Different name exists */
	ERR_READING_CATALOG,	/* Error reading catalog, no alternate exists. */
	ERR_NO_FLOPPY_DRIVE,	/* Floppy drive not found - no DFx: */
	ERR_SPACE_FAIL,		/* Spacing command failed */
	ERR_LOAD_TAPE_N,		/* Load next tape */
	ERR_INCOMPL_CAT,		/* Use what I can? */
	ERR_NO_DEV_COMP,		/* No device compression apparently */
	ERR_DRIVE_BUSY,		/* Drive busy */
	ERR_SCSI_TIMEOUT,		/* SCSI Timeout */
	ERR_INVALID_DEV,		/* Not a valid device to use */
	ERR_READ_TAPE,		/* Read failure of track zero but not catalog */
	ERR_INSERT_A_TAPE,	/* Insert a tape of any kind (to erase or whatever). */
	ERR_NO_REXX,		/* AREXX not present */
	ERR_MACRO_FAIL		/* !! IF YOU ADD TO THE BOTTOM OF THIS TABLE, UPDATE BELOW!! */
};

#define ERR_MAX_ERROR	ERR_MACRO_FAIL

/*
 * Menu and menu item numbers
 */
 
enum {
	PROJECT_MENU,
	OPTIONS_MENU,
	TAG_MENU,
	UTILITIES_MENU,
	MACRO_MENU
};

enum {
	ABOUT_ITEM,
	SAVEAS_ITEM = 2,
	PAGESETUP_ITEM = 4,
	PRINT_ITEM,
	SETTINGS_ITEM = 7,
	QUIT_ITEM = 9
};

enum {
	BACKUP_OPTS_ITEM,
	RESTORE_OPTS_ITEM,
	CATALOG_OPTS_ITEM,
	LOGFILE_OPTS_ITEM,
	BUFF_OPTS_ITEM = 5,
	TAPE_OPTS_ITEM,
	PREFERENCES_ITEM = 8,
	RESET_ITEM = 10
};

enum {
	TAG_ITEM,
	TAG_ALL_ITEM,
	UNTAG_ITEM = 3,
	UNTAG_ALL_ITEM,
	TAG_FILTER_ITEM = 6
};

enum {
	TAPE_UTILITIES_ITEM,
	GET_SESSINFO_ITEM = 2,
	SET_SESSINFO_ITEM,
	SCSI_INTERROGATOR_ITEM = 5
};

enum {
	OTHERMACRO_ITEM = 11,
	CUSTOMMACRO_ITEM = 13
};

enum {
	SAVESETTINGS_SUBITEM,
	SAVEASSETTINGS_SUBITEM,
	LOADSETTINGS_SUBITEM = 3
};

#define BUTTON_WIDTH		80

#if (AMERICAN | BRITISH)

#define BUTTONS_WIDTH	(BUTTON_WIDTH+BUTTON_WIDTH+37)
#define KEY_BACK	'B'
#define KEY_BACK2	'a'
#define KEY_ENTER	'E'
#define KEY_ENTER2	KEY_ENTER
#define KEY_SAVE	'S'
#define KEY_SAVE2	KEY_SAVE
#define KEY_TAG		'T'
#define KEY_UNTAG	'U'
#define KEY_UNTAG2	KEY_UNTAG
#define KEY_TAG_ARC	'v'
#define KEY_OK		'O'
#define KEY_CANCEL	'C'
#define KEY_YES		'Y'
#define KEY_NO		'N'
#define KEY_PRINT	'P'

#elif GERMAN

#define BUTTONS_WIDTH	(BUTTON_WIDTH+BUTTON_WIDTH+37+50)
#define KEY_BACK	'Z'
#define KEY_BACK2	KEY_BACK
#define KEY_ENTER	'U'
#define KEY_ENTER2	KEY_ENTER
#define KEY_SAVE	'S'
#define KEY_SAVE2	'p'
#define KEY_TAG		'M'
#define KEY_UNTAG	'e'
#define KEY_UNTAG2	KEY_UNTAG
#define KEY_TAG_ARC	'v'
#define KEY_OK		'O'
#define KEY_CANCEL	'A'
#define KEY_YES		'J'
#define KEY_NO		'N'
#define KEY_PRINT	'D'

#elif FRENCH

#define BUTTONS_WIDTH	(BUTTON_WIDTH+BUTTON_WIDTH+37+50)
#define KEY_BACK	'R'
#define KEY_BACK2	't'
#define KEY_ENTER	'E'
#define KEY_ENTER2'	'n'
#define KEY_SAVE	'S'
#define KEY_SAVE2	KEY_SAVE
#define KEY_TAG		'M'
#define KEY_UNTAG	'E'
#define KEY_UNTAG2	'n'
#define KEY_TAG_ARC	'b'
#define KEY_OK		'O'
#define KEY_CANCEL	'A'
#define KEY_YES		'O'
#define KEY_NO		'N'
#define KEY_PRINT	'I'

#elif SWEDISH

#define BUTTONS_WIDTH	(BUTTON_WIDTH+BUTTON_WIDTH+37)

#endif

#define BUTTONS_HEIGHT	62

/*
 *	Misc definitions
 */

#define QUOTE			'\''
#define QUOTE_OPEN		QUOTE
#define QUOTE_CLOSE		QUOTE
#define DQUOTE			'\"'
#define DQUOTE_OPEN		DQUOTE
#define DQUOTE_CLOSE	DQUOTE

#define BS		0x08
#define TAB		'\t'
#define LF		'\n'
#define CR		0x0D
#define DEL		0x7F
#define NBSP	0xA0		// Non-breaking space
#define SHYPH	0xAD		// Soft hyphen

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)
#define ALTKEYS		(IEQUALIFIER_LALT | IEQUALIFIER_RALT)
#define CMDKEY		AMIGARIGHT

/*
 *	Useful macros
 */

#define MAX(a,b)	((a)>(b)?(a):(b))
#define MIN(a,b)	((a)<(b)?(a):(b))
#define ABS(x)		((x)<0?(-(x)):(x))

#define MENUITEM(menu, item, sub)	(SHIFTMENU(menu) | SHIFTITEM(item) | SHIFTSUB(sub))

#define IS_REMOVABLE(x)	((x[1] & 128))

/*
 *	Function key definition
 */

#define FKEY_MENU	0
#define FKEY_MACRO	1

typedef struct {
	WORD  	Type;		// Menu equiv. or macro
	Ptr		Data;		// Menu item number or pointer to macro name
} FKey;

#define NUM_STDCOLORS	8

enum {
	REPLACE_YES,	REPLACE_NO,		REPLACE_ASK
};

enum {
	LOG_REPLACE,	LOG_RENAME
};

enum {
	DATE_ORIG,	DATE_BACKUP,	DATE_SYSTEM
};

enum {
	BACKUP_SEL,	BACKUP_COMPL,	BACKUP_IMAGE
};

typedef struct {
	struct IOStdReq ioStdReq;
	LONG			io_Reserved1;
	LONG			io_Reserved2;
	LONG			tdCount;
	LONG			tdSecLabel;
} IOTD, *IOTDPtr;

struct DevParams {
	struct DevParams *Next;					// Pointer to next instance in list
	TextChar	DevName[MAX_FILENAME_LEN+1];
	UBYTE		DevType;					// If not a floppy, this contains SCSI inq[0]
	MsgPortPtr	MsgPort[MAX_NUM_BUFS+1];	// Message port for this device
	IOTDPtr		IOB[MAX_NUM_BUFS+1];		// Two for R/W, one for synchronous ops
	BOOL		BufWait[MAX_NUM_BUFS];		// Whether to wait on this IOB (see below)
	UWORD		EOMBufs;					// At end of tape, how many left to write?
	WORD		CurrIOB;					// Which of the R/W is active
	IOTDPtr 	IntIOB;						// Interrupt IOB
	LONG		Flags;						// Flags from mountlist
	WORD		Unit;						// Unit number
	BOOL		Changed;					// Has disk been changed?
	BYTE		Opened;						// Has OpenDevice() been done yet?
	BYTE		Number;						// Number of drive in QB's internal list
	WORD		Msg;						// Disk # to show, except during "Insert #"
	WORD		NextMsg;					// Disk # to show as "Insert #"
	struct Interrupt IntServer;				// Interrupt node
	UBYTE		Status;						// 1=Dev rdy, 2=needs trk0 chk, 3=active
	UBYTE 		Floppy;						// 0=Non-trackdisk device
	ULONG		BytesWritten[MAX_NUM_BUFS];	// # of bytes pertaining to files only
	TextChar	DeviceName[MAX_FILENAME_LEN+1];
};

typedef struct DevParams DevParams;
typedef struct DevParams *DevParamsPtr;

#ifdef TOOLBOX_LIST_H
#define LIST_LEN sizeof(DevParams)-sizeof(ListItem)
#endif

enum {
	STATUS_NOT_READY = 0, STATUS_READY, STATUS_CHECK, STATUS_ACTIVE, 
	STATUS_DONE, STATUS_REMOVE, STATUS_PAUSED
};

#ifndef DOS_DOS_H
#include <dos/dos.h>
#endif

typedef struct DirFib {
	struct DirFib *   df_Next;			/* link to next entry or 0 */
	ULONG				df_Size;			/* file size or ptr to its sublevel */
	struct DateStamp	df_DateStamp;	/* standard datestamp */
	WORD				df_FilCnt;		/* dir count of items */
	BYTE				df_Prot;			/* archive and protection bits */
	BYTE				df_Flags;		/* Flags described below */
	TextPtr			df_Name;			/* Ptr to filename (and null terminated byte) */
	TextPtr			df_Comment;		/* Ptr to allocated comment field or NULL */
	TextPtr			df_LinkName;	/* Ptr to allocated link name or NULL */
	ULONG				df_ActualSize;	/* Compressed size if on, otherwise amount read */
	ULONG				df_NumRead;		/* For multiple tapes, how much was read */
} DirFib, *DirFibPtr;

#define df_SubLevel df_Size

typedef struct DirLevel {
	LONG				dl_DirLock;		/* lock for this directory level */
	struct DirLevel * dl_ParLevel;	/* ptr to parent DirLevel block */
	DirFibPtr		dl_GNFib;		/* current FIB for GetNextFile */
	DirFibPtr		dl_ChildPtr;	/* ptr to first (sorted) FIB entry */
	DirFibPtr		dl_CurFib;		/* ptr to current FIB at this level */
	DirFibPtr		dl_ParFib;		/* Parent FIB of this level */
} DirLevel, *DirLevelPtr;
#define dl_FilCnt dl_DirLock

typedef Ptr Queue[];

#define DEV_BUF 21						/* Twenty char max */
						
typedef struct {
	TextChar		DeviceBuf1[DEV_BUF];	/* Obsolete- The primary and alt devices */
	TextChar		DeviceBuf2[DEV_BUF];	/* Too small to be of use really */
	TextChar		BReportBuf[32];		/* Truncated from 41 to 32 */
	UBYTE			WindowPos[8];			/* Calling it "Rectangle" screws up alignment */
	UBYTE			BackupType;
	TextChar		RReportBuf[32];
	UBYTE			ReplaceEarlier;		/* Yes=0, No=1, Prompt=2 */
	UBYTE			Unused2[8];
	TextChar		DevPathBuf[DEVICE_PATH_BUFFER_SIZE+1];
	UBYTE			BReport;
	UBYTE			RReport;
	UBYTE			BeepSound;
	UBYTE			BeepFlash;
	UBYTE			BSetArchBit;
	UBYTE			RSetArchBit;
	UBYTE			BAllDirs;
	UBYTE			RSubdirs;
	UBYTE			Reformat;
	UBYTE			Verify;
	UBYTE			ReplaceLater;			/* Yes=0, No=1, Prompt=2 */
	UBYTE			REmptySubdirs;
	UBYTE			SlowBackup;				/* Used to cause QB to use only one buffer */
	UBYTE			TestMode;				/* Restore=0, Test=1, Compare=2 */
} Params, *ParamsPtr;

enum {
	SORT_FORMAT_NAME, SORT_FORMAT_SIZE, SORT_FORMAT_DATE
};

typedef struct {
	BOOL			IncludeSize;
	BOOL			IncludeProt;
	BOOL			IncludeDate;
	BOOL			IncludeTime;
	BOOL			ShowSecs;
	BOOL			ShowAMPM;
	WORD			DateFormat;
	WORD			SortFormat;
	WORD			SeparateDirs;
} CatParams, *CatParamsPtr;

typedef struct {
	WORD			LogLevel;
	WORD			LogNameMode;
	BOOL			LogRate;
	BOOL			LogCompEff;
	BOOL			LogDisplayOff;
	BOOL			Reserved;
	TextChar		LogFileName[MAX_LOG_STRLEN];
} LogParams, *LogParamsPtr;

typedef struct {
	BOOL			AutoRetension;		/* Retension automatically upon insert? */
	BOOL			AutoRewind;			/* Rewind automatically after successful op? */
} TapeParams, *TapeParamsPtr;

typedef struct {
	WORD			BackupBufferSize;
	WORD			RestoreBufferSize;
	LONG			MemType;
} BuffParams, *BuffParamsPtr;

typedef struct {
	WORD			PageWidth;
	WORD			PageHeight;
	UBYTE			MeasureUnit;
	UBYTE			Pad;
	PrintRecord PrintRec;
} PrintInfoRec, *PrintInfoPtr;

typedef struct {
	BOOL			IncludeMode;
	BOOL			DoArchive;
	BOOL			DoBefore;
	BOOL			DoAfter;
	BOOL			DoPattern;
	BOOL			DoLocal;
} TagParams, *TagParamsPtr;

typedef struct {
	ULONG			FileID;				/* Prefs file ID */
	UWORD			Version;				/* Version number of QB that created this file */
	Params		OldPrefs;			/* Old prefs structure */
	UBYTE			WarnInvalidFilenames, SaveIcons, IncludeAssignDirs;
	UBYTE			Destination, Password, Compress, DisplayBackupOpts;
	UBYTE			Source, RestoreDates, DisplayRestoreOpts;
	UBYTE			SaveCatFormat, FloppyEnableMask;
	CatParams	CatOpts;
	LogParams	LogOpts;
	TapeParams	TapeOpts;
	WORD			DriverFlags;
	WORD			Reserved;
	BuffParams	BuffOpts;
	TagParams	TagOpts;
	PrintInfoRec PrintInfo;
	TextChar		CurrBackupDev[MAX_FILENAME_LEN+1];
	TextChar		CurrRestoreDev[MAX_FILENAME_LEN+1];
	WORD			CurrBackupUnit;
	WORD			CurrRestoreUnit;
	TextChar		CompFilterName[100];
} ProgPrefs, *ProgPrefsPtr;

typedef struct {
	WORD	id;
	WORD 	id2;
	UBYTE	diskNum;
	UBYTE	altCatWrappedFlag;			/* On tapes, session number start. */
	LONG	date;
	LONG	time;
} QBOldFirstCylHdr, *QBOldFirstCylHdrPtr;

typedef struct {
	QBOldFirstCylHdr oldHdr;
	WORD	extraLen;						/* Set to zero, for longword padding too */
} QBFirstCylHdr, *QBFirstCylHdrPtr;

typedef struct {						/* This structure length must be a multiple of 4! */
	WORD	id;
	WORD	id2;
	UBYTE	diskNum;
	UBYTE	altCatWrappedFlag;			/* For tapes, the session number of this set */
	LONG	date;
	LONG	time;
	WORD	extraLen;
	ULONG	bufferSize;						/* Padding between backups on this tape */
	ULONG	catSize;						/* Everything AFTER this should be encrypted! */
	UBYTE	version;						/* version - 0 = 5.0 or 5.0.1, 1 or 2 = 5.0.2 */
	UBYTE	unused;						/* Pad to longword (set to zero) */
	UWORD	numVols;						/* Number of volumes on this backup */
	UBYTE	compress;						/* Compression level or zero if off */
	UBYTE	password[MAX_PASSWORD_LEN+1];
	TextChar comment[MAX_BACKUPCOMM_LEN];
	TextChar name[MAX_BACKUPNAME_LEN];
	TextChar volName[MAX_BACKUPNAME_LEN];
} QBNewFirstCylHdr, *QBNewFirstCylHdrPtr;

/*
 * For speed, the assembly code relies on the first six fields in the structure
 * staying right where they are, so do not rearrange the structure!
 */
 
typedef struct {
	ULONG bytes_out;						/* Length of compr/decompr output in Compress() */
	ULONG bytesCompressedInFile;		/* Number of bytes compressed in file so far */
	ULONG ignoreCount;					/* Number of bytes to skip outputting on compression */
	ULONG	sourceBytesReset;				/* How many bytes left to read? */
	ULONG compressBytesInFile;			/* Number of bytes input this file */
	ULONG compressBytesInput;			/* Number of bytes input this buffer */
	ULONG	completedFiles;				/* Number of files completed in operation */
	ULONG saveComplFiles;				/* Saved file number of backup at start of disk */
	ULONG	completedBytes;				/* Number of bytes completed in operation */
	ULONG	saveComplBytes;				/* Saved number of bytes completed */
	ULONG rawCompletedBytes;			/* Number of bytes output to media so far */
	ULONG saveRawComplBytes;			/* Saved number of raw bytes output */
	ULONG	byteCount;						/* Count of bytes remaining in current file */
	ULONG	saveByteCount;					/* Saved byteCount */
	ULONG padBytes;						/* Number of bytes of padding in this buffer */
	ULONG	totalCompressBytes;			/* Number of bytes output after compression */
	ULONG	saveBytesCompressed;			/* Save above variable for ResetDisk() */
	ULONG	finishedTapeBytes;			/* Number of bytes of backup written to prev tape */
	ULONG numErrors;						/* Number of errors in operation so far */
	ULONG	paddedBytes[MAX_NUM_BUFS];
} StatStruct, *StatStructPtr;
