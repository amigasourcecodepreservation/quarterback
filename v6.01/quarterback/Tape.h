/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Quarterback
 * Copyright (c) 1991 New Horizons Software, Inc.
 *
 * Quarterback tape definitions
 */

/* SCSI commands used by handler */
#define CMD_TEST_UNIT_READY   0x00
#define CMD_REWIND            0x01
#define CMD_REQUEST_SENSE     0x03
#define CMD_FORMAT				0x04		/* 3M drives' way of erasing a tape */
#define CMD_READ_BLOCK_LIMITS	0x05
#define CMD_SCSI_READ         0x08
#define CMD_SCSI_WRITE        0x0a
#define CMD_FILEMARK          0x10
#define CMD_SPACE             0x11
#define CMD_INQUIRY           0x12
#define CMD_MODE_SELECT       0x15
#define CMD_RESERVE_UNIT		0x16
#define CMD_RELEASE_UNIT		0x17
#define CMD_ERASE				0x19
#define CMD_MODE_SENSE        0x1a
#define CMD_LOAD_UNLOAD			0x1b
#define CMD_DIAGNOSTIC			0x1d
#define CMD_READ_CAPACITY     0x25

#define CMD_SETMARK				0x50

/* SCSI command data length returned */
#define INQ_LEN				36
#define REQ_LEN				32
#define SNS_LEN				12
#define SEQ_SNS_LEN				12+16	/* Device configuration page tacked on */
#define RBL_LEN				6

/* sense keys */
#define NOS						0x00	/* no sense */
#define RCV						0x01	/* recovered error */
#define ILLEGAL_REQUEST			0x05	/* bad command parameters */
#define UAT						0x06	/* unit attention */
#define BLANK_CHECK				0x08	/* blank check (restore fail) */
#define VOLUME_OVERFLOW			0x0D	/* volume overflow */

/* pseudo sense keys returned by GetSenseKey() */
#define FMK						0x10	/* filemark */
#define ILI						0x12	/* incorrect length */
