/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Misc support functions
 */

#include <exec/types.h>
#include <dos/dos.h> 
#include <dos/dosextens.h>
#include <intuition/intuition.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/DateTime.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Border.h>
#include <Toolbox/LocalData.h>
#include <Toolbox/BarGraph.h>
#include <Toolbox/DOS.h>

#include "QB.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;
extern WindowPtr	cmdWindow, backWindow;

extern WORD			waitCount;

extern BOOL			inMacro;

extern ProgPrefs	prefs;

extern DlgTemplPtr	dlgList[];
extern ReqTemplPtr	reqList[];

extern UWORD		matchDateFormat[];
extern ScrollListPtr scrollList;
extern RequestPtr	requester;

extern UBYTE		operation;
extern BOOL			enterState, disableDisplay;
extern DirLevelPtr	curLevel;
extern WORD			intuiVersion;
extern TextPtr		strPath;
extern ProcessPtr	process;
extern Dir			oldCurrDir;

/*
 *	Local variables and definitions
 */

#define DOCWIND_MSGS		(GADGETDOWN|GADGETUP|NEWSIZE|REFRESHWINDOW|ACTIVEWINDOW|INACTIVEWINDOW|RAWKEY|INTUITICKS)
#define DLGWIND_MSGS		(NEWSIZE|REFRESHWINDOW|ACTIVEWINDOW|INACTIVEWINDOW)
#define BACKWIND_MSGS	(REFRESHWINDOW|NEWPREFS)

static ULONG oldOperIDCMPFlags, oldBackIDCMPFlags;

/*
 * Local prototype
 */
 
static void SetBusyData(void);

/*
 *	Busy pointer images
 */

static UWORD __chip busyPointerData[] = {
	0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000
};

static Pointer busyPointer = {
	14, 14, -7, -7, &busyPointerData[0]
};

static UWORD busyData1[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1E60, 0x1FE0, 0x3E10, 0x3FF0, 0x7E08,
	0x3FF0, 0x7E08, 0x7FF8, 0xFE04, 0x7FF8, 0xFE04, 0x7FF8, 0x81FC,
	0x7FF8, 0x81FC, 0x3FF0, 0x41F8, 0x3FF0, 0x41F8, 0x1FE0, 0x21F0,
	0x0780, 0x19E0, 0x0000, 0x0780,
	0x0000, 0x0000
};

static UWORD busyData2[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1FE0, 0x1FE0, 0x3FF0, 0x3FF0, 0x5FC8,
	0x3FF0, 0x4F88, 0x7FF8, 0x8704, 0x7FF8, 0x8204, 0x7FF8, 0x8104,
	0x7FF8, 0x8384, 0x3FF0, 0x47C8, 0x3FF0, 0x4FE8, 0x1FE0, 0x3FF0,
	0x0780, 0x1FE0, 0x0000, 0x0780,
	0x0000, 0x0000
};

static UWORD busyData3[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x19E0, 0x1FE0, 0x21F0, 0x3FF0, 0x41F8,
	0x3FF0, 0x41F8, 0x7FF8, 0x81FC, 0x7FF8, 0x81FC, 0x7FF8, 0xFE04,
	0x7FF8, 0xFE04, 0x3FF0, 0x7E08, 0x3FF0, 0x7E08, 0x1FE0, 0x3E10,
	0x0780, 0x1E60, 0x0000, 0x0780,
	0x0000, 0x0000
};

static UWORD busyData4[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1860, 0x1FE0, 0x2010, 0x3FF0, 0x6038,
	0x3FF0, 0x7078, 0x7FF8, 0xF8FC, 0x7FF8, 0xFDFC, 0x7FF8, 0xFEFC,
	0x7FF8, 0xFC7C, 0x3FF0, 0x7838, 0x3FF0, 0x7018, 0x1FE0, 0x2010,
	0x0780, 0x1860, 0x0000, 0x0780,
	0x0000, 0x0000
};

static WORD busyNum;
static WindowPtr busyWindow;
static UWORD *busyData[] = {
	&busyData1[0], &busyData2[0], &busyData3[0], &busyData4[0]
};

#ifdef OLDE
/*
 *	Draw border around dialog's up/down arrows, given gadget number of up arrow
 */

void DrawArrowBorder(DialogPtr dlg, WORD gadgNum)
{
	RastPtr rPort = dlg->RPort;
	GadgetPtr gadget;
	BorderPtr border;

	gadget = GadgetItem(dlg->FirstGadget, gadgNum);
	if ((border = BoxBorder(gadget->Width, (WORD) (2*gadget->Height), COLOR_BLUE, -1))
		!= NULL) {
		DrawBorder(rPort, border, gadget->LeftEdge, gadget->TopEdge);
		FreeBorder(border);
	}
}
#endif

/*
 *	Filter function for ModalDialog()
 *	Handles window re-sizes and updates
 */

BOOL DialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	register ULONG class;
	BOOL success = FALSE;
	ULONG msgs = DLGWIND_MSGS;
	
	class = intuiMsg->Class;
	*item = -1;
	
	if (class == REFRESHWINDOW || class == NEWSIZE || class == NEWPREFS ||
		(requester == NULL && (class & msgs))) {
		DoIntuiMessage(intuiMsg);
		success = TRUE;
	}
	return(success);
}

/*
 *	Set up system for lengthy operation
 *	If window is not NULL then window should be set up for requester
 */

void BeginWait()
{
	if (waitCount++)
		return;
/*
	Set window pointers (Monitor will cancel menus when waitCount > 0)
	Also modify IDCMP to send only important messages, and remove verify msgs
*/
	if (cmdWindow != NULL) {
		EnableScrollList();
		oldOperIDCMPFlags = cmdWindow->IDCMPFlags;
		ModifyIDCMP(cmdWindow, DOCWIND_MSGS);
		/*SetStdPointer(cmdWindow, POINTER_WAIT);*/

		OffGList(GadgetItem(cmdWindow->FirstGadget, OK_BUTTON), cmdWindow, NULL, 2);
		if ((operation == OPER_SELECTION) || (operation == OPER_SCAN) || (operation == OPER_LIMBO)) {
			OffGList(GadgetItem(cmdWindow->FirstGadget, FIRST_EXT_GAD), cmdWindow, NULL, (operation == OPER_SELECTION) ? 4 : 2);
		}

	}
	oldBackIDCMPFlags = backWindow->IDCMPFlags;
	ModifyIDCMP(backWindow, BACKWIND_MSGS);
	
	FlushMsgPort(mainMsgPort);
/*
	Permit();
*/
}

/*
 *	Reset system to normal state
 */

void EndWait()
{
	register WindowPtr window;
	register GadgetPtr gadgList;

	Delay(5L);

/*
	Handle BeginWait/EndWait nesting
*/
	if (--waitCount )
		return;
/*
	First enable messages, so we don't miss any menu messages
*/
	if (cmdWindow != NULL) {
		window = cmdWindow;
		gadgList = window->FirstGadget;
		ModifyIDCMP(window, oldOperIDCMPFlags);
		/*SetStdPointer(window, POINTER_ARROW);*/
		EnableGadgetItem(gadgList, BUTTON_1, window, NULL, TRUE);
		EnableGadgetItem(gadgList, BUTTON_2, window, NULL, operation != OPER_COMPLETE);
		if ((operation == OPER_LIMBO) || (operation == OPER_SELECTION)) {
			if (operation == OPER_SELECTION) {
				EnableGadgetItem(gadgList, BUTTON_ENTER, window, NULL, enterState);
				EnableGadgetItem(gadgList, BUTTON_PARENT, window, NULL, curLevel->dl_ParLevel != NULL);
				if (SLNextSelect(scrollList, -1) != -1) {
					OnGList(GadgetItem(gadgList, BUTTON_TAG), window, NULL, 2);
				}
			} else {
				OnGList(GadgetItem(gadgList, BUTTON_ENTER), window, NULL, strPath[0] ? 2 : 1);
			}
		}
		if (disableDisplay && (operation == OPER_IN_PROG)) {
			DisableScrollList();
		}
	}
	ModifyIDCMP(backWindow, oldBackIDCMPFlags);
}

/*
 * Flush the message port without regard as to what it contains.
 */
 
void FlushMsgPort(MsgPortPtr msgPort)
{
	register MsgPtr msg;

	if (msgPort != NULL) {	
		while( msg = GetMsg(msgPort)) {
			ReplyMsg(msg);
		}
	}
}

/*
 *	Set pointer for window to busy pointer
 */

void SetBusyPointer()
{
	busyNum = 0;
	SetBusyData();
	busyWindow = cmdWindow;
	busyNum -= 2;
}

/*
 * Blasts the pointer data for its cursor
 */
 
static void SetBusyData()
{
	register WORD i;
	
	for (i = 2; i < 2*busyPointer.Height + 2; i++)
		busyPointerData[i] = busyData[busyNum][i];
}

/*
 *	Set busy pointer to next in sequence
 */

void NextBusyPointer()
{
	ULONG seconds, micros ;
	static ULONG lastMicro = 0;
	
	CurrentTime( &seconds, &micros );	
	
	if (( micros - lastMicro ) > 100000) {
		lastMicro = micros;
		switch(++busyNum){
		case -2:
		case -1:
			break;
		case 0:
			SetPointer(busyWindow, busyPointer.PointerData, busyPointer.Height,
				busyPointer.Width, busyPointer.XOffset, busyPointer.YOffset);
			break;
		case 4:
			busyNum = 0;
		default:
			SetBusyData();
			break;
		}
	}
}

/*
 * For convenience
 */
 
void SetArrowPointer()
{
	SetStdPointer(cmdWindow, POINTER_ARROW);
}

/*
 *	Flash the screen and/or beep the speaker
 */

void ErrBeep()
{
	if (prefs.OldPrefs.BeepFlash & 1 )
		DisplayBeep(screen);
	if (prefs.OldPrefs.BeepSound & 1 )
		StdBeep();
}

/*
 * Disk change beep
 */
 
void DiskChangeBeep()
{
	if (prefs.OldPrefs.BeepFlash & 0x80 )
		DisplayBeep(screen);
	if (prefs.OldPrefs.BeepSound & 0x80 )
		StdBeep();
}

/*
 * Sound a single-beep on speaker
 */
 
void StdBeep()
{
	SysBeep(5);
}

/*
 *	Check string for positive numeric value
 */

BOOL CheckNumber(TextPtr s)
{
	if (strlen(s) == 0)
		return (FALSE);
	while (*s) {
		if (*s < '0' || *s > '9')
			return (FALSE);
		s++;
	}
	return (TRUE);
}

/*
 * Standard no-frills alert-type dialog call
 */
 
WORD StdDialog(WORD dlgNum)
{
	WORD item;
	DialogPtr dlg;

	BeginWait();
	if ((dlg = GetDialog(dlgList[dlgNum], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (CANCEL_BUTTON);
	}
	OutlineOKButton(dlg);
	StdBeep();
	do {
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
	} while (item != OK_BUTTON && item != CANCEL_BUTTON);
	DisposeDialog(dlg);
	EndWait();
	return (item);
}

/*
 *	Outline first button in requester
 */

void OutlineOKButton(DialogPtr dlg)
{
	GadgetPtr	gadg;

	gadg = (requester) ? requester->ReqGadget : GadgetItem(dlg->FirstGadget, 0);
	OutlineButton(gadg, dlg, requester, TRUE);
} 

/*
 * Toggle checkbox boolean value and change its appearance
 */
 
void ToggleCheckbox(BOOL *value, WORD item, DialogPtr dlg)
{
	GadgetPtr	gadgList;

	gadgList = (requester) ? requester->ReqGadget : dlg->FirstGadget;
	*value = !*value;
	SetGadgetItemValue(gadgList, item, dlg, requester, *value);
}

/*
 * Turn dialog/requester gadget on or off
 */
 
void GadgetOnOff(DialogPtr dlg, WORD item, BOOL onOff)
{
	GadgetPtr	gadgList;

	gadgList = (requester) ? requester->ReqGadget : dlg->FirstGadget;
	SetGadgetItemValue(gadgList, item, dlg, requester, onOff);
}

void GadgetOn(DialogPtr dlg, WORD item)
{
	GadgetOnOff(dlg, item, 1);
}

/*
 * Turn dialog/requester gadget off
 */
 
void GadgetOff(DialogPtr dlg, WORD item)
{
	GadgetOnOff(dlg, item, 0);
}

/*
 * Disable/enable gadget
 */
 
void EnableGadget(register WindowPtr window, WORD item, BOOL onOff)
{
	GadgetPtr	gadgList;
	
	gadgList = (requester) ? requester->ReqGadget : window->FirstGadget;
	EnableGadgetItem(gadgList, item, window, requester, onOff);
}

/*
 * Construct a call to DateString() and put the dashes if appropriate
 */
 
void XDateString(struct DateStamp *dateStamp, WORD dateForm, TextPtr date)
{
	WORD format = matchDateFormat[dateForm];
	
	if (dateStamp->ds_Days <= 32767) {
		if (dateForm == 0) {
			strcpy(date, "\x09-\x06-\x08");
			format = DATE_CUSTOM;
		}
		DateString(dateStamp, format, date);
	} else {
		date[0] = '\0';
	}
}  

/*
 * Builds a path from a filename and lock.
 */
 
BOOL BuildPath(TextPtr fileName, TextPtr destPtr, Dir dirLock, WORD maxLen)
{
	struct FileInfoBlock fib;
	BOOL success = TRUE;
	register TextPtr newName;
	register WORD nameLen, newLen;
	register Dir dir, parentDir;
	
	strcpy(destPtr, fileName);
	newName = fib.fib_FileName;
	dir = DupLock(dirLock);
	while( dir) {
		if (!Examine(dir, &fib))
			break;
		nameLen = strlen(destPtr);
		newLen = strlen(newName);
		if (nameLen + newLen > maxLen )
			success = FALSE;
		BlockMove( destPtr, destPtr + newLen + 1, nameLen + 1);
		BlockMove( newName, destPtr, newLen);
		parentDir = ParentDir(dir);
		destPtr[newLen] = (parentDir) ? '/' : ':';
		UnLock(dir);
		dir = parentDir;
	}
	if (newLen = strlen(destPtr)) {
		if (destPtr[--newLen] == '/') {
			destPtr[newLen] = 0;
		}
	}
	if (dir) {
		UnLock(dir);
	}
	return(success);
}

/*
 * Insert thousand separators into a numeric string, output into buff.
 */
 
void NumToCommaString(LONG num, TextPtr str)
{
	TextChar buff[12];
	register TextPtr destPtr;
	register TextPtr srcPtr;
	register WORD i;

	NumToString(num, buff);
	
	i = strlen(buff);
	srcPtr = &buff[i];
	destPtr = str + i + ((i-1) / 3);
	
	i = 0;
	while( srcPtr >= &buff[0]) {
		if ((i++ == 4) && (destPtr != str)) {
			*destPtr-- = THOUSAND_SEP;
			i = 2;
		}
		*destPtr-- = *srcPtr--;
	}
}

/*
 * Call SFPGetFile() with the usual QB-specific parameters, return TRUE if ok.
 */

BOOL DoStdGetFile(SFReply *sfReply, TextPtr str)
{
	dlgList[DLG_DESTFILE]->Gadgets[12].Info = str;
	dlgList[DLG_DESTFILE]->Gadgets[9].Info = NULL;
	SFPGetFile(screen, mainMsgPort, NULL, DialogFilter, 
		NULL, dlgList[DLG_DESTFILE], 0, NULL, NULL, sfReply);
	return(sfReply->Result == SFP_OK);
}

/*
 * Call SFPPutFile() with the usual QB-specific parameters, return TRUE if ok.
 * Also, if operation is in progress and during backup, make sure the directory
 * Quarterback makes current upon completion is the directory chosen (since
 * the current directory is constantly updated during backup).
 */

BOOL DoStdPutFile(register SFReply *sfReply, TextPtr str, TextPtr nameBuff)
{
	Dir dir, saveDir;
	
	if (oldCurrDir != NULL )
		saveDir = CurrentDir(oldCurrDir);
			
	if ((dir = ConvertFileName(nameBuff)) != NULL) {
		SetCurrentDir(dir);
		UnLock(dir);
	}
	
	SFPPutFile(screen, mainMsgPort, str, nameBuff, DialogFilter, NULL,
		dlgList[DLG_DESTFILE], sfReply);
		
	if (sfReply->Result == SFP_OK) {
		BuildPath(sfReply->Name, nameBuff, sfReply->DirLock, PATHSIZE - 2);
		UnLock(sfReply->DirLock);
	}
	
	if (oldCurrDir != NULL )
		oldCurrDir = CurrentDir(saveDir);
	
	return(sfReply->Result == SFP_OK);
}

/*
 * Tests the string length to see if it is too long, and displays error
 * and returns FALSE if that is the case.
 */
 
BOOL TestStrLen(TextPtr str)
{
	register BOOL result;
	
	result = strlen(str) < 100;
	if (!result) {
		Error(ERR_STR_TOO_BIG);
	}
	return(result);
}

/*
 * Return the difference of ticks between the date stamps passed, the latter
 * being the earlier.
 */
 
ULONG GetTickDiff(struct DateStamp *dateStamp, struct DateStamp *sessionDateStamp)
{
	ULONG	ticks;
	
	ticks = ((dateStamp->ds_Tick - sessionDateStamp->ds_Tick)
		+ ((dateStamp->ds_Minute - sessionDateStamp->ds_Minute)*(60*TICKS_PER_SECOND))
		+ ((dateStamp->ds_Days - sessionDateStamp->ds_Days)*(24*60*60*TICKS_PER_SECOND)));
	return (ticks);
}

/*
 * Returns correct LDF_--- value for DLT_--- value.
 */
 
LONG DosFlagType(LONG listType)
{
	LONG flags;

	switch (listType) {
	case DLT_DEVICE:
		flags = LDF_DEVICES | LDF_READ;
		break;
	case DLT_VOLUME:
		flags = LDF_VOLUMES | LDF_READ;
		break;
	case DLT_DIRECTORY:
		flags = LDF_ASSIGNS | LDF_READ;
		break;
	}
	return(flags);
}

/*
 * Locks dos list
 */
 
struct DevInfo *NHLockDosList(LONG listType)
{
	register struct DosList *dList;
	struct DosInfo *dosInfo;
	
	dList = LockDosList1(DosFlagType(listType));
	if (intuiVersion < OSVERSION_2_0_4) {
		dosInfo = (struct DosInfo *) BADDR( ((struct RootNode *)DOSBase->dl_Root)->rn_Info );
		dList = (struct DosList *) BADDR(dosInfo->di_DevInfo);
		if (dList->dol_Type == listType) {
			return( (struct DevInfo *)dList );
		}
	}	
	return( NHNextDosEntry((struct DevInfo *)dList, listType) );
}

/*
 * Returns next entry of type "listType".
 */
 
struct DevInfo *NHNextDosEntry(struct DevInfo *dList, LONG listType)
{
	struct DevInfo *dev;
	
	if (intuiVersion >= OSVERSION_2_0_4) {
		dev = (struct DevInfo *) NextDosEntry((struct DosList *)dList, DosFlagType(listType));
	} else {
		dev = dList;
		do {
			dev = BADDR(dev->dvi_Next);
		} while( dev != NULL && (dev->dvi_Type != listType));
	}
	return(dev);
}

/*
 * Unlocks DOS list
 */

void NHUnlockDosList(LONG listType)
{
	UnLockDosList1(DosFlagType(listType));
}

/*
 * Calls GetRequest() after refreshing window
 */

RequestPtr DoGetRequest(WORD id)
{
	SLDrawBorder(scrollList);
	return(GetRequest(reqList[id], cmdWindow, TRUE));
}

/*
 * End, Dispose, and reset global requester variable
 */
 
void DestroyRequest(RequestPtr req)
{
	register WindowPtr	window;
	
	if (req) {
		if ((window = req->RWindow) != NULL) {
			EndRequest(req, req->RWindow);
			DisposeRequest(req);
			requester = window->FirstRequest;
		}
		else
			DisposeRequest(req);
	}
}

/*
 * Updates a bar graph whose rastport might have been obliterated
 */
 
void DoDrawBarGraph(RequestPtr req, BarGraphPtr barGraph)
{
	if (barGraph && req) {
		Forbid();
		if (req->ReqLayer) {
			LockLayer(0, req->ReqLayer);
			Permit();
			DrawBarGraph(req->ReqLayer->rp, barGraph);
			UnlockLayer(req->ReqLayer);
		}
		else
			Permit();
	}
}

/*
 * Sets a bar graph whose rastport might have been obliterated
 */

void DoSetBarGraph(RequestPtr req, BarGraphPtr barGraph, ULONG num)
{
	if (barGraph && req) {
		Forbid();
		if (req->ReqLayer) {
			LockLayer(0, req->ReqLayer);
			Permit();
			SetBarGraph(req->ReqLayer->rp, barGraph, num);
			UnlockLayer(req->ReqLayer);
		}
		else
			Permit();
	}
}

/*
 *	Returns the difference between two time values, in milliseconds
 */

LONG DiffTimeMilli(LONG sec1, LONG micro1, LONG sec2, LONG micro2)
{
	return ((sec2 - sec1)*1000L + (micro2 - micro1)/1000L);
}
