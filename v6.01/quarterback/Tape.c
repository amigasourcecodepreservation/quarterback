/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1992-93 New Horizons Software, Inc.
 *
 *	Tape commands
 */
 
#include <exec/io.h>
#include <devices/scsidisk.h>
#include <devices/trackdisk.h>
#include <intuition/intuition.h>		// For RequestPtr and WindowPtr typedefs
#include <dos/dos.h>

#include <proto/dos.h>
#include <proto/exec.h>

#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>

#include <string.h>
#include <typedefs.h>

#include "QB.h"
#include "Proto.h"
#include "Tape.h"

extern ProgPrefs	prefs;
extern ULONG		trkBufSize, userTrkBufSize, tapeTrkBufSize;
extern Ptr			readBuffer;
extern WindowPtr	cmdWindow;
extern ReqTemplPtr	reqList[];
extern UBYTE		densityCode;
extern UBYTE		fixedBit;
extern BOOL			needRewind, initRewound, needSpace;
extern WORD			numBufs;
extern BOOL			sequentialDevice, tapeDevice;
extern ULONG		tranLen;
extern ULONG		tapeBufLen, numBlks, tapeBlks, tapeSize;
extern ULONG		bugMask;
extern TextChar		strRewinding[], strFastSpacing[], strSpacing[], strErase[], strRetension[];
extern MsgPortPtr	mainMsgPort;
extern TextChar		strTrackdisk[], strTapeDrive[];
extern UBYTE		startMode, mode, readErr;
extern LONG			maxCylinders, activeCylinder, dialogVarNum;
extern UWORD		sessionNumber, firstTape, firstTapeSession, activeDisk;
extern ULONG		bufferSize;
extern UBYTE		prevMode, backupCompress;
extern WORD			currUnit, statusIOBIndex;
extern TextPtr		currDevPtr, dialogVarStr;
extern BOOL			async, noSCSI, notSCSI, noAutoSense, quitFlag;
extern BOOL			earlyWarning, supportsSetMark, attemptCompressionPage;
extern struct DateStamp	firstSessionDateStamp;
extern DevParamsPtr	activeDevice, firstDevice;
extern StatStruct	stats;
extern BOOL			restartCatalog;

#define CMD_BLK_SIZE	10

/*
 * Local prototypes
 */
 
static BOOL	TapeOperRequester(DevParamsPtr, UBYTE, UWORD);
static BYTE GetSenseKey(struct SCSICmd *);
static BOOL	InitDummyDevice(DevParamsPtr);
static BYTE	TryTapeIO(DevParamsPtr, UBYTE, BOOL);
static BYTE	DoTapeIO(DevParamsPtr, UBYTE, ULONG, WORD, BOOL);
static BOOL	CheckTapeReady(DevParamsPtr, TextPtr, BOOL);
static BOOL IsSetMarkSupported(DevParamsPtr);

/*
 * Static Structures used by this module
 */

struct SCSICmd	scsiCmd[MAX_NUM_BUFS+1];
UBYTE			cmdBlk[CMD_BLK_SIZE * (MAX_NUM_BUFS+1)];
UBYTE			senseData[REQ_LEN * (MAX_NUM_BUFS+1)];
BOOL			quickErase;
UBYTE			modePage;
UBYTE			currTapeCmd[MAX_NUM_BUFS+1];
BOOL			triedRewrite;

/*
 * Determine whether the name and unit of this tape drive specifier are valid.
 * Leaves string space in "ptr" with results of INQUIRY, string starts in byte #8.
 * Currently, I've munged it to zap trailing spaces and rev off if "noRev" TRUE.
 * If ptr NULL, no inquiry data is returned, just the BOOL flag.
 */

BOOL CheckSCSIDrive(TextPtr devName, WORD unit, register TextPtr ptr, BOOL noRev)
{
	register BOOL		found;
	register TextPtr	strPtr;
	BYTE				err;
	WORD				saveCurrUnit, saveStatusIndex;
	BOOL				isTD;
	TextPtr				saveCurrDev;
	DevParams			dummyDevice;
	UBYTE				buff[INQ_LEN];

	if (ptr == NULL)
		ptr = &buff[0];
	saveCurrUnit = currUnit = unit;
	saveCurrDev = currDevPtr;
	currDevPtr = devName;
	if (noRev)
		ptr[8] = 0;			// Inquiry returns ASCII text beginning here
	notSCSI = TRUE;
	saveStatusIndex = statusIOBIndex;
	found = InitDummyDevice(&dummyDevice);
	if (found) {
		isTD = (strcmp(devName, strTrackdisk) == 0);
		if (!isTD && !noSCSI) {
			err = TapeIO(&dummyDevice, CMD_INQUIRY, (ULONG) ptr);
			if (!err) {
				notSCSI = FALSE;
				if (noRev) {
					strPtr = ptr + 31;
					while (*strPtr == ' ' && (strPtr != ptr)) {
						--strPtr;
					}
					strPtr[1] = 0;
				}
			}
 			FreeDriveResources(&dummyDevice);
		}
	}
	currDevPtr = saveCurrDev;
	currUnit = saveCurrUnit;
	statusIOBIndex = saveStatusIndex;
	return (found);
}

/*
 * Sets the current SCSI device variables
 */
 
void SetCurrentDev()
{
	if (startMode == MODE_RESTORE) {
		currDevPtr = prefs.CurrRestoreDev;
		currUnit = prefs.CurrRestoreUnit;
	}
	else {
		currDevPtr = prefs.CurrBackupDev;
		currUnit = prefs.CurrBackupUnit;
	}
}

/*
 * Initialize certain globals pertaining to tape drives, based on first byte of
 * SCSI inquiry data passed. If -1 passed, it's NOT a valid SCSI device.
 */
 
void InitDriveType(BYTE driveType)
{
	driveType &= 0x1F;
	sequentialDevice = (driveType == 1);
	tapeDevice = (sequentialDevice | (driveType == 16));
}

/*
 * Call InitDevice() with a dummy device parameter block.
 */

static BOOL InitDummyDevice(register DevParamsPtr devParam)
{
	WORD saveNumBufs = numBufs;
	BOOL success;

	numBufs = 0;
	BlockClear(devParam, sizeof(DevParams));
	strcpy(devParam->DevName, currDevPtr);
	strcpy(devParam->DeviceName, currDevPtr);
	devParam->Unit = currUnit;
	success = InitDevice(devParam);
	numBufs = saveNumBufs;
	return (success);
}

/*
 * Return low, current, high block size that tape drive can allow.
 */

ULONG GetBlockSizes(BOOL readHiLow, ULONG *high, UWORD *low, ULONG *numBlocks)
{
	UBYTE buff[SNS_LEN];
	ULONG current = 0;
	DevParams dummyDevice;
	WORD saveStatusIndex;

	*high = 0L;
	*low = 0;
	*numBlocks = 0L;
	saveStatusIndex = statusIOBIndex;
	if (InitDummyDevice(&dummyDevice)) {
		modePage = 0;
		if (TapeIO(&dummyDevice, CMD_MODE_SENSE, (ULONG) buff) == 0) {
			*numBlocks = (*((ULONG *)&buff[4])) & 0x00FFFFFF;
			current = (*((ULONG *) &buff[8])) & 0x00FFFFFF;
		}
		if (readHiLow) {
			if (TapeIO(&dummyDevice, CMD_READ_BLOCK_LIMITS, (ULONG)buff) == 0) {
				*high = (*((ULONG *) &buff[0])) & 0x00FFFFFF;
				*low = *((UWORD *) &buff[4]);
			}
		}
		FreeDriveResources(&dummyDevice);
	}
	statusIOBIndex = saveStatusIndex;
	return (current);
}

/*
 * Clear sense structure and end-of-media buffer count.
 */
 
void ClearSense(DevParamsPtr devParam)
{
	register WORD index;
	
	for( index = 0 ; index < numBufs ; index++) {
		devParam->EOMBufs = 0;						/* Clear end-of-media buffer count */
		if (devParam->BufWait[index]) {
			devParam->BufWait[index] = FALSE;
			WaitIO((struct IOStdReq *)devParam->IOB[index]);
		}
		devParam->BytesWritten[index] = 0L;
		if (devParam->IOB[index]) {
			devParam->IOB[index]->ioStdReq.io_Error = 0;
		}
		senseData[(index * REQ_LEN)+2] = 0;		/* Clear snskey */
	}
	senseData[(statusIOBIndex*REQ_LEN)+2] = 0;
	triedRewrite = FALSE;
}

/*
 *	Read the initial chunk of data on the tape
 *	NOTE: For WangTek compatibility, we must pad the length to read out
 *	the nearest block size
 */

BOOL ReadTapeHeader(DevParamsPtr devParam, UBYTE *buffer, ULONG len)
{
	ULONG	newLen, saveTrkBufSize, saveLen;
	BOOL	success = FALSE;
	UBYTE	*block;

	restartCatalog = FALSE;
	if (tapeBufLen) {
		if (trkBufSize == 0)
			return (FALSE);
		newLen = (len + trkBufSize)/trkBufSize;
		newLen *= trkBufSize;
		if ((block = NewPtr(newLen)) != NULL) {
			saveTrkBufSize = trkBufSize;
			trkBufSize = newLen;
			saveLen = tranLen;
			tranLen = newLen/trkBufSize;
			success = (TapeIO(devParam, CMD_SCSI_READ, (ULONG) block) == 0);
			if (success)
				BlockMove(block, buffer, len);
			tranLen = saveLen;
			trkBufSize = saveTrkBufSize;
			DisposePtr(block);
		}
	}
	return (success);
}

/*
 * Do Tape I/O, and do request sense manually if necessary
 */

BYTE TapeIO(DevParamsPtr devParam, UBYTE tapeCmd, ULONG data)
{	
	BYTE	err;
	WORD	index;

	switch (tapeCmd) {
	case CMD_SCSI_WRITE:
	case CMD_SCSI_READ:
		index = devParam->CurrIOB;
		break;
	default:
		index = statusIOBIndex;
		break;
	}
	currTapeCmd[index] = tapeCmd;
	if (noAutoSense) {
		err = DoTapeIO(devParam, tapeCmd, data, index, FALSE);
		if (!async && tapeCmd != CMD_REQUEST_SENSE)
			DoTapeIO(devParam, CMD_REQUEST_SENSE, (ULONG) &senseData[index * REQ_LEN], index, FALSE);
		if (!async) {
			(void) ProcessSenseData(devParam, index);
			err = ((struct IOStdReq *) devParam->IOB[index])->io_Error;
		}
	}
	else
		err = DoTapeIO(devParam, tapeCmd, data, index, TRUE);
	return (err);
}

/*
 * Perform a SCSI command to the tape drive!
 * Since I have made writes asynchronous, the SCSI command structure
 * must be double-buffered, and declared globally. I have also
 * double-buffered the SCSI command block itself, although this
 * is probably not necessary, and it may thus not be even necessary 
 * to make the block global.
 */

static BYTE DoTapeIO(DevParamsPtr devParam, UBYTE tapeCmd, ULONG data,
					 register WORD index, BOOL autoSense)
{
	register struct IOStdReq *iobPtr;
	register UBYTE *cmdBlkPtr;					/* Maybe not necessary */
	register struct SCSICmd *scsiCmdPtr;
	register ULONG xferLen = tranLen;
	ULONG *data_ptr;
	ULONG rwLen = trkBufSize;
	static ULONG blkNum = 0;					/* Just in case direct-access works */
	register WORD retry = 15;					/* Timeout after five seconds */
	WORD newRetry;
	register UBYTE temp;
	BOOL doAsync = FALSE;

	iobPtr = (struct IOStdReq *) devParam->IOB[index];
	iobPtr->io_Command = HD_SCSICMD;
	iobPtr->io_Data = &scsiCmd[index];
	iobPtr->io_Length = sizeof(struct SCSICmd);
	iobPtr->io_Error = 0;
	
	cmdBlkPtr = &cmdBlk[index * CMD_BLK_SIZE];
	BlockClear(cmdBlkPtr, CMD_BLK_SIZE);
	cmdBlkPtr[0] = tapeCmd;
	cmdBlkPtr[1] = (devParam->Unit & 0x70) << 1;
	
	scsiCmdPtr = &scsiCmd[index];
	BlockClear(scsiCmdPtr, sizeof(struct SCSICmd));
	scsiCmdPtr->scsi_Command = cmdBlkPtr;
	scsiCmdPtr->scsi_CmdLength = 6;
	scsiCmdPtr->scsi_SenseData = &senseData[index * REQ_LEN];
	scsiCmdPtr->scsi_SenseLength = REQ_LEN;
	scsiCmdPtr->scsi_Flags = SCSIF_READ;

	switch (tapeCmd) {
	case CMD_SCSI_WRITE:
		doAsync = async;
/*
	If at end of backup, don't write a full buffer, write only to nearest
	block, to facilitate multiple backups to one tape.
*/
		if ((devParam->BytesWritten[index] != (trkBufSize - stats.padBytes)) && 
			(!prefs.TapeOpts.AutoRewind) && (trkBufSize >= stats.padBytes)) {
			rwLen = devParam->BytesWritten[index] + stats.padBytes;
			xferLen = rwLen ? ((rwLen-1) / tapeBufLen) + 1 : 0;
		}
	case CMD_SCSI_READ:
		if (!sequentialDevice) {
/*
	This has potential for overflow, but it's unlikely. Shift by 7 because this type
	of drive is rumored to support block size of 8320, which is divisible by 128.
	Shifting here helps reduce the possibility of overflow.
*/
			blkNum = (activeCylinder * (tapeTrkBufSize >> 7)) / (tapeBufLen >> 7);
			*((UWORD *)&cmdBlkPtr[2]) = (UWORD) blkNum;
		} else {
			cmdBlkPtr[1] |= 1;		/* Don't mess with variable lengths */
			*((UWORD *)&cmdBlkPtr[2]) = (UWORD) xferLen >> 8;
		}
		cmdBlkPtr[4] = (UBYTE) xferLen & 0xff;
		scsiCmdPtr->scsi_Length = rwLen;
		scsiCmdPtr->scsi_Data = (Ptr) data;
		if (tapeCmd == CMD_SCSI_WRITE) {
			scsiCmdPtr->scsi_Length |= bugMask;
			scsiCmdPtr->scsi_Flags = SCSIF_WRITE;
		}
		break;
	case CMD_REQUEST_SENSE:
		cmdBlkPtr[4] = REQ_LEN;
		scsiCmdPtr->scsi_Length = REQ_LEN;
		if (data == NULL) {
			data = (ULONG) &senseData[index * REQ_LEN];
		}
		BlockClear((Ptr) data, REQ_LEN);
		scsiCmdPtr->scsi_Data = (UWORD *) data;
/*
	Change the SenseData pointer for our benefit, since we usually autosense
	and use that to set the "devParam->Changed" field if unit attention occurs
*/
		scsiCmdPtr->scsi_SenseData = (UBYTE *) scsiCmdPtr->scsi_Data; 
		scsiCmdPtr->scsi_Flags = SCSIF_READ;
		break;
	case CMD_INQUIRY:
		cmdBlkPtr[4] = INQ_LEN;
		scsiCmdPtr->scsi_Length = INQ_LEN;
		scsiCmdPtr->scsi_Data = (UWORD *) data;
		break;
	case CMD_SETMARK:
		cmdBlkPtr[0] = CMD_FILEMARK;
		cmdBlkPtr[1] |= 2;
	case CMD_FILEMARK:
		*((ULONG *)&cmdBlkPtr[2]) = data;
		break;
	case CMD_READ_BLOCK_LIMITS:
		scsiCmdPtr->scsi_Length = RBL_LEN;
		scsiCmdPtr->scsi_Data = (UWORD *) data;
		break;
	case CMD_LOAD_UNLOAD:
	case CMD_FORMAT:
		if (tapeCmd == CMD_LOAD_UNLOAD) {
			cmdBlkPtr[4] = 0x03;				/* Set retension and load bits */
		} else {
			cmdBlkPtr[2] = 0x20;				/* Precondition */
			cmdBlkPtr[5] = 0x80;				/* Retry on errors */
		}
	case CMD_REWIND:
	case CMD_ERASE:
		blkNum = 0;
		temp = 0x01;
		if (tapeCmd == CMD_ERASE) {
			if (!quickErase) {
				cmdBlkPtr[1] |= 0x01;
			}
			temp = 0x02;
		}
		if (data == 0) {
			cmdBlkPtr[1] |= temp;			/* Doesn't work for CALIPER/WANGTEKs! */
		}
	case CMD_TEST_UNIT_READY:
	case CMD_RESERVE_UNIT:
	case CMD_RELEASE_UNIT:
		if (bugMask && (tapeCmd == CMD_TEST_UNIT_READY)) {
			return (0);
		}
		scsiCmdPtr->scsi_Length = 0;
		break;
	case CMD_SPACE:
		cmdBlkPtr[1] |= supportsSetMark ? 4 : 1;
		cmdBlkPtr[3] = data >> 8;			/* Number to seek ahead */
		cmdBlkPtr[4] = data & 255;
		scsiCmdPtr->scsi_Length = 0;
		retry <<= 1;							
		break;
/*
	For MODE_SENSE and MODE_SELECT, the following info is returned in "data".
	Byte 0: Mode Data Length
	Byte 1: Medium Type
	Byte 2: Device-specific: Bit 7=Write-prot, Bits 6-4 Buffer mode, Bits 3-0 = speed
	Byte 3: Block descriptor length
	Byte 4: Density code
	Byte 5: Number of blocks (3 bytes)
	Byte 8: Reserved
	Byte 9: Block length (3 bytes)
*/
	case CMD_MODE_SENSE:
		temp = SNS_LEN;
		if (sequentialDevice && modePage) {
			cmdBlkPtr[2] = modePage;
			temp = SEQ_SNS_LEN;
		}
		cmdBlkPtr[4] = temp;
		BlockClear((Ptr) data, temp);
		scsiCmdPtr->scsi_Length = temp;
		scsiCmdPtr->scsi_Data = (UWORD *) data;
		break;
	case CMD_MODE_SELECT:
		temp = SNS_LEN;
		if (sequentialDevice && attemptCompressionPage) {
			cmdBlkPtr[1] |= 0x10;
			temp = SEQ_SNS_LEN;
			if (backupCompress & COMP_DEV_FLAG)
				((UBYTE *)data)[SNS_LEN+2] |= 0x80;
			else
				((UBYTE *)data)[SNS_LEN+2] &= 0x7F;
		}
		cmdBlkPtr[4] = temp;
		scsiCmdPtr->scsi_Length = temp;
		scsiCmdPtr->scsi_Data = (UWORD *) data;
		scsiCmdPtr->scsi_Flags = SCSIF_WRITE;
		data_ptr = (ULONG *) data;
		*data_ptr++ = 0x00001008;
/*
	You MUST set the density code, because the CALIPER tape drives
	have a firmware bug. If you do not heed this advice, then you
	will get end-of-data 78M into the tape when restoring. You should
	not send the number of tapeBlks because the Exabyte demands zero.
*/
		*data_ptr++ = /* tapeBlks |*/ (densityCode << 24);
	 	*data_ptr = tapeBufLen & 0x00FFFFFF;	/* Block length */
		break;
	case CMD_READ_CAPACITY:
		scsiCmdPtr->scsi_Length = 8;
		scsiCmdPtr->scsi_CmdLength = 10;
		scsiCmdPtr->scsi_Data = (UWORD *) data;
	default:
		break;
	}
	do {
		if (autoSense) {
			scsiCmdPtr->scsi_Flags |= SCSIF_AUTOSENSE;
		}
		if (doAsync) {
			SendIO(iobPtr);
		} else {
			DoIO(iobPtr);
			if (autoSense) {
				(void) ProcessSenseData(devParam, index);	// Ignore -1 result, handled below
			}
		}
		if (iobPtr->io_Error == HFERR_SelTimeout) {
			retry--;
		} else {
			newRetry = 0;
/*
	On the Caliper, it can return OK when no bytes were actually read. This
	may happen when the tape has reached the end for its track, every several
	megabytes or so. Therefore, I retry a few times, giving ample opportunity
	for the tape to switch direction, thereby allowing it to complete.
*/
			switch (tapeCmd) {
			case CMD_SCSI_READ:
			case CMD_SPACE:
				needRewind = TRUE;
			case CMD_SCSI_WRITE:
				if ((scsiCmdPtr->scsi_Status == 8) ||
					((scsiCmdPtr->scsi_Actual == 0) && (tapeCmd != CMD_SPACE) && (scsiCmdPtr->scsi_Status == 0) && !async)) {
					Delay(80);
					if ((newRetry = retry - 5) && doAsync) {
						doAsync = async = FALSE;
						devParam->BufWait[index] = FALSE;
					}
				}
				if ((iobPtr->io_Error == HFERR_BadStatus) && (scsiCmdPtr->scsi_Actual)) {
					iobPtr->io_Error = 0;
				}
			default:
				break;
			}
			retry = newRetry;
		}
	} while (retry > 0);
	return (iobPtr->io_Error);
}

/*
 * First try an immediate-version of a non-data command, then retry with immediate
 * setting off if error.
 */
 
static BYTE TryTapeIO(DevParamsPtr devParam, UBYTE tapeCmd, BOOL wait)
{
	BYTE result = TRUE;
	register WORD pass;
	
	for( pass = 0 ; result && (pass < 2) ; pass++) {	
		if (wait || (result = TapeIO(devParam, tapeCmd, FALSE))) {
			if (result = TapeIO(devParam, tapeCmd, TRUE)) {
/*
	On the TEAC, if it is underrunning, even a forced non-immediate REWIND
	here will not be enough to kick it into action. Therefore, we do two passes,
	and on the second time around, it should work.
*/
				Delay(50);
			}
		}
	}
	return (result);
}

/*
 * Set certain flags if sense data indicates certain conditions exist,
 * namely, set "DevParam->Changed" if medium changed, and change the
 * error return to the trackdisk version if write-protected.
 */
 
BYTE ProcessSenseData(register DevParamsPtr devParam, register WORD index)
{
	UBYTE					asc;
	register BYTE			key = 0;
	register UBYTE			cmd;
	register ULONG			len;
	struct IOStdReq			*iobPtr;
	UBYTE					*cmdBlkPtr;
	register struct SCSICmd	*scsiCmdPtr;
	
	iobPtr = (struct IOStdReq *) devParam->IOB[index];
	scsiCmdPtr = &scsiCmd[index];

	if (scsiCmdPtr->scsi_SenseData) {
		if ((iobPtr->io_Error == 0) || (iobPtr->io_Error == HFERR_BadStatus)) {
			key = GetSenseKey(scsiCmdPtr);
/*
	If a SyQuest, the unit attention has been eaten already
*/
			if (!tapeDevice) {
				if (key) {
					if ((devParam->Status != STATUS_REMOVE) && 
						scsiCmdPtr->scsi_Command[1] == CMD_TEST_UNIT_READY) {
						devParam->Changed = TRUE;
					} else {
						StatusNotReady(devParam);
					}
				}
				if ((key == 0) && (devParam->Status == STATUS_NOT_READY)) {
					devParam->Changed = ((struct IOStdReq *) devParam->IOB[index])->io_Error == 0;
				}
			} else { 
				cmd = currTapeCmd[index];
				switch(key) {
				case UAT:
					asc = scsiCmdPtr->scsi_SenseData[12];
					devParam->Changed = TRUE;
					
					if (asc == 0x27) {
						iobPtr->io_Error = TDERR_WriteProt;
					}
					else {
						if (cmd == CMD_SCSI_READ)
							iobPtr->io_Error = HFERR_SelTimeout;	// Force retry
						else
							iobPtr->io_Error = 0;
					}
					break;
				case FMK:
					scsiCmdPtr->scsi_Status = 0L;	// It's OK!
					break;
				case VOLUME_OVERFLOW:
					if (devParam->EOMBufs == 0) {
						devParam->EOMBufs = (scsiCmdPtr->scsi_SenseData[5] << 8) +
							scsiCmdPtr->scsi_SenseData[6];
					}
					restartCatalog = TRUE;
					maxCylinders = activeCylinder;
					break;
				case BLANK_CHECK:
					if ((devParam->EOMBufs == 0) && (mode == MODE_RESTORE) &&
						(cmd == CMD_SCSI_READ)) {
						if (devParam->EOMBufs = (scsiCmdPtr->scsi_Length - scsiCmdPtr->scsi_Actual) / tapeBufLen)
							restartCatalog = readErr = TRUE;
					}
					iobPtr->io_Error = 0;
					break;
				case 0:
/*
	On the HP DDS, EOM is returned at the beginning of the tape, so set the
	"needRewind" only when first write is completed OK.
*/
					if (cmd == CMD_SCSI_WRITE) {
						needRewind = TRUE;
					}
/*
	TEAC kludge- it doesn't return ILLEGAL_REQUEST whenever an invalid page (like
	DDS compression for instance) is selected for MODE_SENSE, so force it.
*/
					if (!attemptCompressionPage || (scsiCmdPtr->scsi_Actual > 16)) {
						break;
					}
				case ILLEGAL_REQUEST:
					if (devParam->EOMBufs) {
						key = 0;
					}
					if (mode == MODE_BACKUP) {
						if (cmd == CMD_MODE_SENSE) {
							if (attemptCompressionPage && (prefs.Compress & COMP_DEV_FLAG))
								Error(ERR_NO_DEV_COMP);
							attemptCompressionPage = FALSE;
						}
						else if (cmd == CMD_SCSI_WRITE) {
							if (!devParam->EOMBufs) {
								devParam->EOMBufs = (scsiCmdPtr->scsi_Length - scsiCmdPtr->scsi_Actual) / tapeBufLen;
								restartCatalog = TRUE;
								iobPtr->io_Error = key = 0;
							}
						}
					}
					else {
						if (devParam->EOMBufs = (scsiCmdPtr->scsi_Length - scsiCmdPtr->scsi_Actual) / tapeBufLen) {
							restartCatalog = TRUE;
							iobPtr->io_Error = 0;
						}
					}
					break;
				case -1:
					len = scsiCmdPtr->scsi_Length -= scsiCmdPtr->scsi_Actual;
					scsiCmdPtr->scsi_Data = (Ptr)(((UBYTE *)scsiCmdPtr->scsi_Data) + scsiCmdPtr->scsi_Actual);
					cmdBlkPtr = &cmdBlk[index * CMD_BLK_SIZE];
					len = ((len - 1) / tapeBufLen) + 1;
					*((UWORD *)&cmdBlkPtr[2]) = (UWORD) len >> 8;
					cmdBlkPtr[4] = (UBYTE) len & 0xff;
					scsiCmdPtr->scsi_Actual = 0;
					DoIO(iobPtr);
					break;
				case -2:
					scsiCmdPtr->scsi_Status = 2;		/* Force to error */
				default:
					break;
				}
			}
		}	
	}
	return (key);
}

/*
 * Convert sense data to a single byte description.
 */
 
static BYTE GetSenseKey(register struct SCSICmd *scsiCmdPtr)
{
	register BYTE snskey = scsiCmdPtr->scsi_SenseData[0];
	register BYTE numkey;
	
	if ((snskey & 0x70) == 0x70) {
		snskey = scsiCmdPtr->scsi_SenseData[2];
		numkey = snskey & 0x0F;
		if (snskey & 0x80 )
			numkey = FMK;
		else if(snskey & 0x20)
	 		numkey = ILI;
		else if (snskey & 0x40) {
/*
	For some unknown reason, the Caliper may drop a buffer at EOM during backup,
	necessitating its rewrite.
	The TEAC may be writing a partial buffer.
*/
			if (mode == MODE_BACKUP) {
				if ((scsiCmdPtr->scsi_Actual != scsiCmdPtr->scsi_Length) && (numkey != VOLUME_OVERFLOW) && (scsiCmdPtr->scsi_Length == trkBufSize)) {
					numkey = -1;
					if (!triedRewrite) {
						triedRewrite = TRUE;
					} else {
						numkey = -2;
					}
				}
				earlyWarning = needRewind;
			}
		}
	} else {
		numkey = snskey & 0x0F;
	}
	return (numkey);
}

/*
 * Does drive support setmarks?
 */
 
static BOOL IsSetMarkSupported(DevParamsPtr devParam)
{
	UBYTE modeSnsData[SEQ_SNS_LEN];
	
	modePage = 0x10;
	supportsSetMark = (TapeIO(devParam, CMD_MODE_SENSE, (ULONG) &modeSnsData[0]) == 0) && (modeSnsData[SNS_LEN+8] & 0x20);
	modePage = 0x00;
	return (supportsSetMark);
}

/*
 * Writes filemarks on sequential drives, setmarks if supported (for Fast Search).
 */
 
void WriteFilemark(DevParamsPtr devParam)
{
	if (sequentialDevice) {
		if (IsSetMarkSupported(devParam)) {
			if (TapeIO(devParam, CMD_SETMARK, 0x0100L) == 0) {
				needSpace = FALSE;
			}
		} else {
			if (TapeIO(devParam, CMD_FILEMARK, 0x0100L) == 0) {
				needSpace = FALSE;
			}
		}
	}
}

/*
 * Rewind the tape
 */
 
BOOL RewindTape(DevParamsPtr devParam, BOOL wait)
{
	BOOL	success = TRUE;

	if (!initRewound || needRewind)
		success = TapeOperRequester(devParam, CMD_REWIND, wait);
	return (success);
}

/* 
 * Erase the tape
 */

BOOL EraseTape(DevParamsPtr devParam, BOOL wait, BOOL quick)
{
	BOOL	success = FALSE;

	quickErase = quick;
	if (RewindTape(devParam, TRUE))
		success = TapeOperRequester(devParam, sequentialDevice ? CMD_ERASE : CMD_FORMAT, wait);
	return (success);
}

/*
 * Retension the tape
 */

BOOL RetensionTape(DevParamsPtr devParam, BOOL wait)
{
	BOOL	success = FALSE;

	if (RewindTape(devParam, TRUE))
		success = TapeOperRequester(devParam, CMD_LOAD_UNLOAD, wait);
	return (success);
}

/* 
 * Space the tape forward to next filemark (next backup set actually)
 */

BOOL SpaceTape(DevParamsPtr devParam, UWORD times)
{
	BOOL	success = TRUE;

	if (times) {
		if (!needRewind)
			InitTapeParameters(devParam, FALSE);
		if (success = TapeOperRequester(devParam, CMD_SPACE, times))
			sessionNumber += times;
	}
	return (success);
}

/*
 *	Call SpaceTape() interactively, bringing up info requester if not successful
 */

BOOL DoSpaceTape(DevParamsPtr devParam, UWORD times)
{
	BOOL	success;

	success = SpaceTape(devParam, times);
	if (!success)
		Error(ERR_SPACE_FAIL);
	return (success);
}

static BOOL DoInitDummyDevice(DevParamsPtr deviceBlock)
{
	UBYTE data[INQ_LEN];
	BOOL success = FALSE;
	
	SetCurrentDev();
	if (InitDummyDevice(deviceBlock)) {
		TapeIO(deviceBlock, CMD_INQUIRY, (ULONG)&data[0]);
		if (((struct IOStdReq *)deviceBlock->IOB[0])->io_Error == 0) {
			InitDriveType(data[0]);
			if (tapeDevice) {
				success = TapeIO(deviceBlock, CMD_REQUEST_SENSE, NULL) == 0;
			}
		}
	}
	return (success);
}

/*
 * Perform a long synchronous tape operation.
 * Put up a dialog to inform the user of the extended nature of the operation.
 * NOTE: This is not a great way of doing this- really should be asynchronous,
 * but that would add complexity to the rest of the program.
 */

static BOOL TapeOperRequester(DevParamsPtr currentDevice, UBYTE tapeOper, UWORD wait)
{
	RequestPtr req;
	DevParams dummyDevice;
	UBYTE data[MAX(SEQ_SNS_LEN,INQ_LEN)];
	TextPtr str;
	register BOOL success;
	register DevParamsPtr devParam = currentDevice;
	WORD saveStatusIndex = statusIOBIndex;
	DevParamsPtr saveActiveDevice, saveFirstDevice;
	ULONG saveTrkBufSize;
	
	BeginWait();
	if (devParam == NULL) {
		if (DoInitDummyDevice(&dummyDevice)) {
			saveActiveDevice = activeDevice;
			saveFirstDevice = firstDevice;
			firstDevice = activeDevice = devParam = &dummyDevice;
		}
	}
	else
		devParam = currentDevice;
	success = tapeDevice;
	if (devParam != NULL && tapeDevice) {
		if (cmdWindow) {
			switch(tapeOper) {
			case CMD_LOAD_UNLOAD:
				success = CheckTapeReady(devParam, data, FALSE);
				str = strRetension;
				break;
			case CMD_ERASE:
			case CMD_FORMAT:
				success = CheckTapeReady(devParam, data, TRUE);
				str = strErase;
				break;
			case CMD_SPACE:
				success = CheckTapeReady(devParam, data, FALSE);
				if (!success)
					break;
				IsSetMarkSupported(devParam);		// Setup "supportsSetMark"
				if (sessionNumber == 0) {
					saveTrkBufSize = trkBufSize;
					trkBufSize = userTrkBufSize = tapeBufLen;// Force to read one block only
					if (tranLen == 0)
						InitTapeParameters(devParam, FALSE);
					needRewind = TRUE;
					if (ReadTapeHeader(devParam, data, INQ_LEN)) {
						sessionNumber = firstTapeSession =
							((QBNewFirstCylHdrPtr) data)->altCatWrappedFlag;
						if (sessionNumber != 1)
							bufferSize = tapeBufLen;
						else
							bufferSize = ((QBNewFirstCylHdrPtr) data)->bufferSize;
						activeDisk = firstTape = ((QBNewFirstCylHdrPtr) data)->diskNum;
						firstSessionDateStamp.ds_Days = ((QBNewFirstCylHdrPtr) data)->date;
						firstSessionDateStamp.ds_Minute = ((QBNewFirstCylHdrPtr) data)->time;
					}
					trkBufSize = saveTrkBufSize;
				}
				success &= (bufferSize != 0);
				str = supportsSetMark ? strFastSpacing : strSpacing;
				break;
			case CMD_REWIND:
				success = CheckTapeReady(devParam, data, FALSE);
				str = strRewinding;
				break;
			}
			if (success) {
				reqList[REQ_REWIND]->Gadgets[0].Info = (Ptr) str;
				req = DoGetRequest(REQ_REWIND);
				SetStdPointer(cmdWindow, POINTER_WAIT);
				if (tapeOper == CMD_SPACE)
					success = (TapeIO(devParam, tapeOper, wait) == 0);
				else
					success = (TryTapeIO(devParam, tapeOper, wait) == 0);
/*
	The following will cause backup to space past the next one, but this is better
	than getting an Illegal Request, which is what will happen if you do not
	cause a space to occur before the backup. Hmmmmm.
*/
				if (success) {
					if (needRewind = needSpace = (tapeOper == CMD_SPACE))
						prevMode = MODE_RESTORE;
					else {
						activeCylinder = sessionNumber = 0;
						initRewound = TRUE;
					}
				}
				SetArrowPointer();
				DestroyRequest(req);				// Can handle NULL
			}
		}
	}
	if (currentDevice == NULL && devParam != NULL) {
		FreeDriveResources(devParam);
		firstDevice = saveFirstDevice;
		activeDevice = saveActiveDevice;
	}
	EndWait();
	statusIOBIndex = saveStatusIndex;
	return (success);
}

/*
 * Check to make sure tape drive is ready: if tape not present,
 * prompt for a tape. If busy processing another request, display
 * requester to that effect. Return FALSE if aborted.
 */

static BOOL CheckTapeReady(DevParamsPtr devParam, TextPtr data, BOOL checkWP)
{
	BOOL	done, success;

	done = FALSE;	
	do {
		if (attemptCompressionPage)
			modePage = 0x0F;
		success = (TapeIO(devParam, CMD_MODE_SENSE, (ULONG) data) == 0);
		modePage = 0x00;
		if (success) {
			if (TapeIO(devParam, CMD_TEST_UNIT_READY, NULL) != 0) {
				dialogVarStr = strTapeDrive;
//				dialogVarNum = activeDisk;
				if (!quitFlag && DiskSenseDialog(ERR_INSERT_A_TAPE, TRUE) == OK_BUTTON)
					success = TapeWaitRequester(devParam, data, CMD_TEST_UNIT_READY);
				else {
					success = FALSE;
					done = TRUE;
				}
			}
			else {
				done = TRUE;
				if (data[2] & 0x80) {
					if (checkWP) {
						success = (DiskSenseDialog(ERR_TWRITE_PROTECT, TRUE) == OK_BUTTON);
						done = !success;
					}
				}
			}
		}
		else {
			success = TapeWaitRequester(devParam, data, CMD_MODE_SENSE);
			done = !success;
		}
	} while (!done);
	return (success);
}

/*
 *	If device is not ready yet, display a requester to that effect,
 *	and allow the user to cancel if he/she is the impatient type.
 *	Used only for Mode Sense and Mode Select commands currently.
 */

BOOL TapeWaitRequester(DevParamsPtr devParam, UBYTE *data, UBYTE tapeOper)
{
	BOOL		done, success;
	WORD		i;
	RequestPtr	req;
			
	success = (TapeIO(devParam, tapeOper, (ULONG) data) == 0);
/*
 	Retry immediately if failure, to prevent the requester from coming
	up at all if the tape drive really is in fact ready.
*/
	if (!success)
		success = (TapeIO(devParam, tapeOper, (ULONG) data) == 0);
	if (!success) {
		BeginWait();
		req = DoGetRequest(REQ_TAPEWAIT);
		if (req) {
			done = FALSE;
			while (!done) {
				for (i = 0; i < 9 && !done; i++) { 
					Delay(5);
					if (CheckRequest(mainMsgPort, cmdWindow, DialogFilter) == CANCEL_BUTTON)
						done = TRUE;
				}
				if (!done)
					done = success = (TapeIO(devParam, tapeOper, (ULONG) data) == 0);
			}
			DestroyRequest(req);
		}
		EndWait();
	}
	return (success);
}	
		
/*
 *	Initialize tape globals for I/O
 *	If TRUE passed, warn user not to write-protect
 */

BOOL InitTapeParameters(DevParamsPtr currentDevice, BOOL checkWP)
{
	BOOL			success;
	DevParamsPtr	devParam = currentDevice;
	DevParams		dummyDevice;
	UBYTE			data[MAX(SEQ_SNS_LEN,INQ_LEN)];

	if (success = (devParam == NULL)) {
		if (DoInitDummyDevice(&dummyDevice))
			devParam = &dummyDevice;
	}
	if (devParam) {
		if (sequentialDevice) {
			TapeIO(devParam, CMD_READ_BLOCK_LIMITS, (ULONG) &data[0]);
			fixedBit = (*((ULONG *) &data[0]) & 0x00FFFFFF) == (ULONG) (*((USHORT *) &data[4]));
		}
		else
			fixedBit = 0;
		success = CheckTapeReady(devParam, data, checkWP);
		if (success) {
			IsSetMarkSupported(devParam);
			densityCode = data[4];
			tapeBlks = *((ULONG *) &data[4]) & 0x00FFFFFF;
			tapeBufLen = MAX(512, *((ULONG *) &data[8]) & 0x00FFFFFF);
			tapeSize = tapeBlks * tapeBufLen;
/*
	Make sure trkBufSize is an even multiple for Direct Access devices.
*/
			if (sequentialDevice)
				tapeTrkBufSize = trkBufSize = userTrkBufSize;
			else {
				tapeTrkBufSize = (((userTrkBufSize-1) / tapeBufLen)+1) * tapeBufLen;
				if (tapeTrkBufSize != trkBufSize) {
					FreeBuffers();
					success = AllocateBuffers(trkBufSize = tapeTrkBufSize);
				}
				if (success)
					maxCylinders = ((tapeSize-1) / trkBufSize)+1;
			}
			if (success) {
				numBlks = trkBufSize / tapeBufLen;
/*
	Transfer length is the number of blocks, unless it's variable-sized, which
	we don't support right now anyway.
*/
				tranLen = /*(!sequentialDevice || fixedBit) ? numBlks : trkBufSize;*/
							numBlks;
/*
	Direct-access tape can't be sped up through setting the Buffered I/O bit anyway,
	(returns ILLEGAL REQUEST), so don't bother trying. Similarly, this cannot be
	called when the tape is not at the beginning, so check "needRewind".
*/
				if (sequentialDevice && (!needRewind || attemptCompressionPage))
					success = TapeWaitRequester(devParam, data, CMD_MODE_SELECT);
				ClearSense(devParam);
			}
		}
		if (currentDevice == NULL)
			FreeDriveResources(devParam);
	}
	return (success);
}
