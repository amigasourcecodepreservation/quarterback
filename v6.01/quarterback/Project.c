/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Quarterback
 * Copyright (c) 1991-93 New Horizons Software, Inc.
 *
 * Project menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Image.h>

#include "QB.h"
#include "Proto.h"

/*
 * External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;
extern WindowPtr	backWindow;
extern WindowPtr	cmdWindow;
extern BOOL			quitFlag, abortFlag, inMacro, initRewound;
		 
extern RequestPtr	requester;
extern DlgTemplPtr	dlgList[];
extern TextChar	strBuff[], strSaveCat[], strSaveLog[], strDefCatName[];

extern TextChar	strSaveAsSettings[], strLoadSettings[], strPrefsName[];
extern UBYTE		mode, operation;

extern ProgPrefs	prefs;

extern ProcessPtr	process;
extern Dir			oldCurrDir;


/*
 * Local definitions and prototypes
 */
 
enum {
	SAVECAT_FORMAT_TEXT = 12,
	SAVECAT_DEFAULT_RADBTN,
	SAVECAT_HIERARCHICAL_RADBTN
};

static Dir	BeginSFP(void);
static void	EndSFP(Dir);

/*
 * Dialog hook to set the radio button values when saving catalog.
 */
 
static WORD SaveDlgHook(WORD item, DialogPtr dlg)
{
	GadgetPtr gadgList = dlg->FirstGadget;

	if (item == SFPMSG_INIT || item == SAVECAT_DEFAULT_RADBTN || item == SAVECAT_HIERARCHICAL_RADBTN) {
		if (item == SAVECAT_DEFAULT_RADBTN) {
			prefs.SaveCatFormat = 0;
		} else if (item == SAVECAT_HIERARCHICAL_RADBTN) {
			prefs.SaveCatFormat = 1;
		}
		SetGadgetItemValue(gadgList, SAVECAT_DEFAULT_RADBTN, dlg, NULL,
						   (BOOL) prefs.SaveCatFormat == 0);
		SetGadgetItemValue(gadgList, SAVECAT_HIERARCHICAL_RADBTN, dlg, NULL,
						   (BOOL) prefs.SaveCatFormat == 1);
	}
	return (item);
}

/* 
 * Save either catalog or session log, whichever is appropriate
 */
 
BOOL DoSaveAs()
{
	BOOL success;
	
	if (operation != OPER_LIMBO) {
		if ((operation == OPER_PREP) || (operation == OPER_SCAN) || (operation == OPER_SELECTION)) {
			success = DoSaveCatalog();
		} else {
			success = DoSaveSessionLog();
		}
	}
	return (success);
}

/*
 * Open the standard file requester for saving either catalog or session log
 */

BOOL OpenSaveAsDialog(SFReplyPtr sfReply, BOOL isCat )
{
	if (isCat) {
		dlgList[DLG_DESTFILE]->Gadgets[SAVECAT_DEFAULT_RADBTN].Type = GADG_RADIO_BUTTON;
		SFPPutFile(screen, mainMsgPort, strSaveCat, strDefCatName,
			DialogFilter, SaveDlgHook, dlgList[DLG_DESTFILE], sfReply);
		dlgList[DLG_DESTFILE]->Gadgets[SAVECAT_DEFAULT_RADBTN].Type = GADG_ITEM_NONE;
	} else {
		SFPPutFile(screen, mainMsgPort, strSaveLog, strBuff,
			DialogFilter, NULL, dlgList[DLG_DESTFILE], sfReply);
	}
	return (sfReply->Result == SFP_OK);
}

/* 
 * Given a pathname already setup and a filename input, write out either
 * the session log or the catalog, depending on context. Called from
 * scripts as well.
 */
 
BOOL SaveFileAs(TextPtr fileName)
{
	File fHandle;
	WORD status;
	BOOL success;
	TextChar buff[PATHSIZE];			/* It's alot of stack space, be careful. */
	
	if ((operation == OPER_LIMBO) || (operation == OPER_SCAN) || (operation == OPER_SELECTION)) {
		fHandle = Open(fileName, MODE_NEWFILE);
		if (success = fHandle != NULL) {
			SetBusyPointer();
			InitCatalogPrint(buff);
			if (WriteHeader(fHandle, FALSE)) {
				if (WriteDirHeading(fHandle, strBuff, FALSE) == PRINT_OK) {
					do {
						status = WriteEntry(fHandle, NULL, &success);
						NextBusyPointer();
					} while ((status != PRINT_DONE) && (status != PRINT_ERROR) );
				}
			}
			Close(fHandle);
			if (success = status == PRINT_DONE) {
				SaveIcon(fileName, ICON_TEXT);
			}
			EndCatalogPrint(buff);
			SetArrowPointer();
		}
	} else {
		success = OutputReport(fileName);
	}
	return (success);
}

/*
 * Save the catalog of the current volume
 */

BOOL DoSaveCatalog()
{
	BOOL success;
	SFReply sfReply;
	UBYTE oldFormat = prefs.SaveCatFormat;
	Dir saveDir;
	
	saveDir = BeginSFP();

	if (success = OpenSaveAsDialog(&sfReply, TRUE)) {
		UnLock(sfReply.DirLock);
		success = SaveFileAs(sfReply.Name);
	} else {
		prefs.SaveCatFormat = oldFormat;
	}
	EndSFP(saveDir);
	return (success);
}

/*
 * Save the session log
 */

BOOL DoSaveSessionLog()
{
	BOOL success = FALSE;
	SFReply sfReply;
	Dir dir, saveDir;
	
	saveDir = BeginSFP();
	strcpy(strBuff, prefs.LogOpts.LogFileName);
	if (dir = ConvertFileName(strBuff)) {
		UnLock(dir);
	}
	if (OpenSaveAsDialog(&sfReply, FALSE)) {
		UnLock(sfReply.DirLock);
		success = SaveFileAs(sfReply.Name);
	}
	EndSFP(saveDir);
	return (success);
}

/*
 * Page setup
 */

BOOL DoPageSetup()
{
	register BOOL success;

	success = PageSetupRequest();
	return (success);
}

/*
 * Print project
 */

BOOL DoPrint(WindowPtr window, UWORD modifier, register BOOL printOne)
{
	register PrintRecPtr printRec = &prefs.PrintInfo.PrintRec;
	register BOOL success = FALSE;
	
	if (operation != OPER_LIMBO) {
		if (modifier & ALTKEYS )
			printOne = TRUE;
		printRec->Copies = 1;
		if (printOne) {
			(void) PrValidate(printRec);
		}
		if (printOne || PrintRequest()) {
			success = PrintList();						/* Print catalog or logfile */
		}
	}
	return (success);
}

/*
 * Save program preferences
 */

BOOL DoSaveSettings()
{
	BOOL success = WarningDialog(ERR_SAVE_PREFS, TRUE) == OK_BUTTON;
	
	if (success) {
		success = SaveProgPrefs(NULL);
		if (!success) {
			DOSError(ERR_SAVE, IoErr());
		}
	}
	return (success);
}

/*
 *	Save program settings with specified file name
 */

BOOL DoSaveAsSettings()
{
	BOOL success = FALSE;
	SFReply sfReply;
	Dir saveDir;
	
	saveDir = BeginSFP();
/*
	Get file name to save
*/
	SFPPutFile(screen, mainMsgPort, strSaveAsSettings, strPrefsName,
			   DialogFilter, NULL, NULL, &sfReply);
	if (sfReply.Result != SFP_CANCEL) {
		UnLock(sfReply.DirLock);
/*
	Save settings
*/
		success = SaveProgPrefs(sfReply.Name);
		if (!success) {
			DOSError(ERR_SAVE, IoErr());
		}
	}
	EndSFP(saveDir);
	return (success);
}

/*
 *	Load settings
 */

BOOL DoLoadSettings()
{
	SFReply sfReply;
	Dir saveDir;
	BOOL success = FALSE;

/*
	Get file name of settings file
*/
	saveDir = BeginSFP();
	SFPGetFile(screen, mainMsgPort, strLoadSettings, DialogFilter, NULL,
			   NULL, 0, NULL, IsPrefsFile, &sfReply);
	if(sfReply.Result != SFP_OK) {
		if (sfReply.Result == SFP_NOMEM) {
			Error(ERR_NO_MEM);
		}
	} else {
		success = GetProgPrefs(sfReply.Name);
		UnLock(sfReply.DirLock);
	}
	EndSFP(saveDir);
	return (success);
}

/*
 * Prepare to open a standard file dialog. The main function is to preserve
 * the current directory, so that during a backup, even though the current
 * directory is often changing, the "oldCurrDir" will always be the one to 
 * change to at the end.
 */

static Dir BeginSFP()
{
	Dir saveDir = NULL;
	
	if (oldCurrDir != NULL) {
		saveDir = CurrentDir(oldCurrDir);
	}
	BeginWait();
	return (saveDir);
}

/*
 * Balances call to BeginSFP().
 */
 
static void EndSFP(Dir saveDir)
{
	if (oldCurrDir != NULL) {
		oldCurrDir = CurrentDir(saveDir);
	}
	EndWait();
}

		
/*
 * Handle settings submenu
 */
 
BOOL DoSettingsMenu(UWORD item)
{
	BOOL success = FALSE;
	
	switch(item) {
	case SAVESETTINGS_SUBITEM:
		success = DoSaveSettings();
		break;
	case SAVEASSETTINGS_SUBITEM:
		success = DoSaveAsSettings();
		break;
	case LOADSETTINGS_SUBITEM:
		success = DoLoadSettings();
		break;
	}
	return (success);
}

/*
 * Test to see if user really wants to quit the program.
 * Returns TRUE if quitting should proceed
 */
 
BOOL DoShutDown()
{
	BOOL quit = TRUE;
	
	if (operation == OPER_PAUSE || operation == OPER_IN_PROG) {
		/* if (!inMacro) { */
			CheckAbort();
			quit = abortFlag;
		/*}*/
	} else {
		/*
		if (operation == OPER_COMPLETE) {
			DoOutputReport();
		} else {
			abortFlag = TRUE;
		}*/
		if (operation != OPER_COMPLETE) {
			abortFlag = TRUE;
		}
	}
	if (quit) {
		initRewound = TRUE;
	}
	return (quit);
}

/*
 * Process project menu selection
 */

BOOL DoProjectMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifier)
{
	register BOOL success = FALSE;

	switch (item) {
	case ABOUT_ITEM:
		success = DoAboutBox();
		break;
	case SAVEAS_ITEM:
		success = DoSaveAs();
		break;
	case PAGESETUP_ITEM:
		success = DoPageSetup();
		break;
	case PRINT_ITEM:
		success = DoPrint(window, modifier, FALSE);
		break;
	case SETTINGS_ITEM:
		success = DoSettingsMenu(sub);
		break;
	case QUIT_ITEM:
		success = quitFlag = DoShutDown();
		break;
	}
	return (success);
}
