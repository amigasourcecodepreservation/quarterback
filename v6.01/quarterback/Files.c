/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1992-93 New Horizons Software, Inc.
 *
 *	Prepare for backup/restore operation
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <dos/dos.h>
#include <dos/dosextens.h>
#include <exec/interrupts.h>
#include <rexx/errors.h>

#include <string.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/DOS.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include "QB.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr	backWindow, cmdWindow;
extern MsgPortPtr	mainMsgPort;

extern TextChar	strBuff[];
extern TextPtr		strPath, strSavePath, destFileName;

extern ProgPrefs	prefs;

extern DirLevel	rootLevel;
extern DirLevelPtr 	curLevel;

extern UBYTE		startMode, mode, operation;
extern DevParamsPtr	activeDevice;
extern DirFibPtr	curFib;

extern DirFibPtr	curDBBase, curDBPtr, baseDirFib;
extern ULONG		fillCount, readCount;

extern ULONG		totalFiles, totalBytes, selFiles, selBytes;
extern ULONG		catSize, catCorruptAddition, volumeSize;
extern ULONG		lastTick;

extern UWORD		totalVols, selVols;
extern BOOL			abortFlag, levelFlag, catErrFlag;
extern ULONG		absolutePos;
extern UWORD		catFlag;

extern ScreenPtr	screen;

extern BOOL			skipUpdate;
extern UWORD		tabbing[];
extern TextPtr		dialogVarStr;
extern LONG			dialogVarNum;
extern File			fHandle, destFile;
extern RequestPtr	requester;

extern TextChar	strDescLimbo[], strDescLimboDir[];
extern TextChar	strDescOpScan[], strDescOpSel[], strDescOpPause[];
extern TextChar	strDescOpPrep[], strDescOpProg[], strDescOpDone[];

extern TextChar	strBackup[], strBackupL[], strRestore[];
extern TextChar	strStart[], strDone[], strResume[], strProceed[], strCancel[];
extern TextChar	strPause[], strAbort[], strDrawer[], strVolume[], strCatalog[];

extern TextChar	strInUseErr[], strExistsErr[], strDelProtErr[];
extern TextChar	strWriteProtErr[], strReadProtErr[], strNotFoundErr[];
extern TextChar	strDiskFullErr[], strGenIOErr[];
extern TextChar	strBackProtErr[], strRestProtErr[];
extern TextChar	strPreparing[], strEstNum[], strNA[], strColon[];

extern UBYTE		readErr, backupCompress, restoreCompress, _tbPenLight;
extern GadgetPtr	mainSelGadgets, tagGadgets, runtimeGadgets;
extern BOOL			enterState, tagsDone;
extern WORD			indentLevel;
extern WORD			intuiVersion;

extern Ptr			fib;
extern UBYTE		*readBuffer, *readPtr, *fillPtr, *fillBuffer;
extern BOOL			firstProtErr;
extern ScrollListPtr	scrollList;
extern UBYTE		iconType, verifyMode;
extern WORD			qbCode, useCache;
extern TextChar	currBackupName[], currVolName[], currComment[];
extern LONG			sessionCurrCylinder, activeCylinder, maxCylinders, trkBufSize;
extern UWORD		sessionCurrDisk, activeDisk, firstTape, firstTapeSession;
extern UWORD		sessionNumber;
extern BOOL			inputCompleted, outputCompleted, tapeDevice, useAmigaDOS;
extern TextPtr		currDevPtr;
extern WORD			currUnit;
extern WORD			errFillLevelCount;
extern BOOL			disableDisplay, _tbSmartWindows, processAll, force;
extern ULONG		linkCount;
extern WORD			useSortFormat;
extern UBYTE		scriptError, backupType;
extern WORD			extraLen;
extern StatStruct	stats;
extern BOOL			useRecalcCatSize, restartCatalog, earlyWarning, resetDisk;

#define CAT_ENT_SIZE	14
#define PRESORT	1
#define NUM_EXT_GADS	16

/*
 * Local Prototypes
 */

static void	ScanUnselDir(DirFibPtr);
static void	SetActionButtonsText(UBYTE); 
static BOOL	CompareDir(DirFibPtr, DirFibPtr);
static BOOL	ValidFilename(TextPtr);
static void	DoTruncatePath(void);
static DirFibPtr ReadEntry(void);
static void	GoToLimboMode(UBYTE);
static void	GoToSelMode(UBYTE);
static void	GetLinkName(TextPtr, TextPtr *);
static BOOL DoBackupRestore(UBYTE, TextPtr);

/*
 * Local defines
 */
 
#define DIR_BLOCK_SIZE	(sizeof(DirFib)+4)

BOOL skippedAdd;

/*
 * Set the text for the primary button on the button strip on the main window.
 */
 
static void SetActionButtonsText(UBYTE oper)
{
	register TextPtr text1;
	register TextPtr text2 = strAbort;

	switch (oper) {
	case OPER_LIMBO:
		text1 = strBackup;
		text2 = strRestore;
		break;
	case OPER_SELECTION:
		text1 = strProceed;
		text2 = strCancel;
		break;
	case OPER_PREP:
		text1 = strStart;
		text2 = strCancel;
		break;
	case OPER_IN_PROG:	
		text1 = strPause;
		break;
	case OPER_PAUSE:
		text1 = strResume;
		break;
	case OPER_COMPLETE:
		text1 = strDone;
			EnableGadgetItem(cmdWindow->FirstGadget, BUTTON_2, cmdWindow, NULL, FALSE);
			break;
	default:
		text1 = NULL;
	}
	if (text1 != NULL) {
		SetButtonItem(cmdWindow->FirstGadget, BUTTON_1, cmdWindow, NULL, text1, text1[0]);
		SetButtonItem(cmdWindow->FirstGadget, BUTTON_2, cmdWindow, NULL, text2, text2[0]);
	}
}

/* 
 * Handle the "Proceed" or "Cancel/Abort" button action
 */
 
BOOL HandleOperationButton(BOOL proceed, BOOL altOn)
{
	BOOL success = TRUE;

	if (proceed) {
		switch (operation) {
		case OPER_LIMBO:
			success = EnterScanMode(MODE_BACKUP, altOn);
			break;
		case OPER_SELECTION:
			if (!selFiles) {
				Error(ERR_NO_FILES);
				break;
			} else {
				UpdateDirStatus(TRUE);
				if (mode == MODE_RESTORE || InitLoops()) {
					scriptError = readErr = 0;
					if (skippedAdd) {
						skippedAdd = FALSE;
						AddGList(cmdWindow, tagGadgets, -1, NUM_EXT_GADS - 2, NULL);
					} else {
						RefreshButtonStrip(TRUE);	/* Aesthetic for V1.3 users w/out -sw */
					}
					RecalcNumbers(mode == MODE_BACKUP);
					DoInitDriveStates(TRUE);
					SetModeOp(mode, OPER_PREP);
					skipUpdate = FALSE;			/* Enable scrolllist display now */
					EnableScrollList();
					/*
					if (backupType || (mode == MODE_RESTORE)) {
						break;
					}
					*/
					break;
				} else {
					EndLoops(FALSE);
					abortFlag = success = FALSE;
					break;
				}
			}
		case OPER_PREP:
			success = BeginOp();
			break;
		case OPER_PAUSE:
			SetModeOp(mode, OPER_IN_PROG);
			RefreshScrollList();
			break;
		case OPER_IN_PROG:
			SetModeOp(mode, OPER_PAUSE);
			WaitAllIOReq();
			StatusPaused(activeDevice);
			EnableScrollList();
			break;
		case OPER_COMPLETE:
			InitStats();
			disableDisplay = FALSE;
			SetModeOp(MODE_LIMBO, OPER_LIMBO);
			BuildVolumeList(NULL);
			break;
		case OPER_SCAN:
		default:
			break;
		}
	}
	else {
		switch (operation) {
		case OPER_LIMBO:
			success = EnterScanMode(MODE_RESTORE, altOn);
			break;
		case OPER_PREP:
			BeginWait();
			EndLoops(FALSE);
			EndWait();
			break;
		case OPER_SCAN:
			abortFlag = TRUE;
			break;
		case OPER_PAUSE:
		case OPER_IN_PROG:
			CheckAbort();
			if (operation == OPER_PAUSE && abortFlag)
				operation = OPER_IN_PROG;		// Abort, but from the loops
			break;
		case OPER_SELECTION:
			if (!tagsDone || force || (success = (WarningDialog(ERR_CANCEL_SELMODE, TRUE) == OK_BUTTON))) {
				if (mode == MODE_RESTORE)
					EndLoops(TRUE);
				else {
					SetModeOp(MODE_LIMBO, OPER_LIMBO);
					FreeDirBlocks();
					BuildVolumeList(prefs.OldPrefs.DevPathBuf);
				}
			}
		default:
			break;
		}
	}
	return (success);
}

/*
 * Start the operation from OPER_PREP, or if executing a macro, bypass OPER_PREP
 * entirely by calling this immediately.
 */

BOOL BeginOp()
{
	BOOL success;

	SetModeOp(mode, OPER_IN_PROG);
	if (mode == MODE_BACKUP)
		DoBackup();
	else
		DoRestore(FALSE);
	success = !abortFlag;
	abortFlag = FALSE;
	return (success);
}

/*
 * Sets mode value, updates menus/buttons/strings of main window accordingly
 */

void SetModeOp(UBYTE newMode, UBYTE newOper)
{
	register TextPtr text;
	Rectangle rect, tempRect;
	register UBYTE oldOper = operation;
	register WindowPtr window = cmdWindow;
	register GadgetPtr gadgList;
	register WORD num;
	GadgetPtr gadget1;
	GadgetPtr gadget2;
	TextChar buff[20];
	BOOL scanning;
	
	text = NULL;
	if (newMode == MODE_LIMBO)
		newOper = OPER_LIMBO;
	if (window && (newMode != mode || newOper != oldOper)) {
		gadgList = window->FirstGadget;
		if (oldOper != -1 && newOper != oldOper) {
			if (newMode != MODE_BACKUP || newOper != OPER_SELECTION || backupType == 1)
				SetActionButtonsText(newOper);
			if (oldOper == OPER_SELECTION) {
				if (skippedAdd) {
					skippedAdd = FALSE;
					AddGList(window, tagGadgets, -1, NUM_EXT_GADS - 2, NULL);
				}
				if (newOper == OPER_PREP) {
					gadget1 = mainSelGadgets;
					gadget2 = GadgetItem(gadgList, TOTAL_BOX);
					num = NUM_EXT_GADS;
				} else {
					gadget1 = tagGadgets;
					gadget2 = GadgetItem(gadgList, BUTTON_UNTAG);
					num = NUM_EXT_GADS - 2;
				}
				GetGadgetRect(gadget1, window, NULL, &rect);
				GetGadgetRect(gadget2, window, NULL, &tempRect);
				rect.MinX -= 2;
				rect.MaxX = tempRect.MaxX + 1;
				rect.MaxY = window->Height - window->BorderBottom - 1;
				SetAPen(window->RPort, _tbPenLight);
				FillRect(window->RPort, &rect);
				RemoveGList(window, gadget1, num);
			}
		}
		switch (newMode) {
		case MODE_LIMBO:
			text = strDescLimbo;
			selFiles = 0;
			selBytes = 0;
			strPath[0] = 0;
			break;
		case MODE_BACKUP:
		case MODE_RESTORE:
			if (newOper == OPER_SCAN) {
				dialogVarStr = (strPath[0] && strPath[strlen(strPath)-1] != ':') ?
					 strDrawer : strVolume;
				if (newMode == MODE_RESTORE)
					dialogVarStr = strCatalog;
				text = strDescOpScan;
			}
			else {
				dialogVarStr = buff;
				num = TRUE;
				switch (newOper) {
				case OPER_SELECTION:
					text = strDescOpSel;
					num = FALSE;
					break;
				case OPER_PREP:
					text = strDescOpPrep;
					num = FALSE;
					break;
				case OPER_IN_PROG:
					text = strDescOpProg;
					break;
				case OPER_PAUSE:
					text = strDescOpPause;
					break;
				case OPER_COMPLETE:
					text = strDescOpDone;
					break;
				}
				strcpy(buff, GetModeName(newMode, num));
				break;
			}
			break;
		case MODE_QUIT:
			if (operation == OPER_PREP || mode == MODE_RESTORE)
				DoEndLoops();
			FreeDirBlocks();
			strBuff[0] = 0;
			break;
		}
		if (mode != MODE_BACKUP || newOper != OPER_SELECTION || backupType == 1) {
			ParseDialogString(text, strBuff);
			SetGadgetItemText(window->FirstGadget, DESCR_TEXT, window, NULL, strBuff);
		}
		mode = newMode;
		operation = newOper;
		SetAllMenus();
		switch (newOper) {
		case OPER_LIMBO:
			startMode = mode;
			GoToLimboMode(oldOper);
			break;
		case OPER_PREP:
			dialogVarStr = GetModeName(newMode, FALSE);
			(void) ParseDialogString(strPreparing, strBuff);
			text = strPath;
			if (mode == MODE_RESTORE) {
				/*
				if (verifyMode == RMODE_TEST) {
					strcpy(text, strSavePath);
				} else {
					strcpy(text, prefs.OldPrefs.DevPathBuf);
				}
				*/
				scanning = (prefs.OldPrefs.TestMode == RMODE_SCAN);
				if (scanning || prefs.OldPrefs.TestMode == RMODE_TEST) {
					text = strBackupL;
				}
			}
			else
				scanning = FALSE;
			strcat(strBuff, text);
/*			DisplayStr(strBuff);*/
			InitRuntimeDisplay(strBuff);
			if (!(tapeDevice || useAmigaDOS || scanning)) {
				strcpy(strBuff, strEstNum);
				NumToString(selVols, buff);
				strcat(strBuff, buff);
				if ((mode == MODE_BACKUP && (prefs.Compress & COMP_ON_FLAG)) ||
					(useSortFormat < 2 && (restoreCompress & COMP_ON_FLAG))) {
					strcat(strBuff, strNA);
				}
				DisplayStr(strBuff);
			}
			if (oldOper != -1) {
	 			UpdateDirStatus(TRUE);
				InitFileDir();
				AddGList(window, runtimeGadgets, -1, NUM_EXT_GADS, NULL);
				if (intuiVersion >= OSVERSION_2_0) {
					RefreshButtonStrip(TRUE);
					RefreshGList(runtimeGadgets, window, NULL, NUM_EXT_GADS);
				} else {
					RefreshButtonStrip(_tbSmartWindows);
				}
			}
			break;
		case OPER_SELECTION:
			GoToSelMode(oldOper);
		case OPER_IN_PROG:
			/*
			if (oldOper == OPER_PREP) {
				InitRuntimeDisplay(NULL);
			}
			*/
		default:
			break;
		}
	}
}

/*
 * Call when SetModeOp() is changing operation to OPER_LIMBO.
 * Basically just adds the "Enter" and "Back" buttons back
 * if we were back to having just the two buttons before.
 */
 
static void GoToLimboMode(UBYTE oldOper)
{
	register WindowPtr window = cmdWindow;
	Rectangle rect, tempRect;

	if (skippedAdd) {
		skippedAdd = FALSE;
		AddGList(window, tagGadgets, -1, NUM_EXT_GADS - 2, NULL);
	}
	switch (oldOper) {
	case OPER_PREP:
	case OPER_IN_PROG:
	case OPER_COMPLETE:
	case OPER_PAUSE:
		GetGadgetRect(runtimeGadgets, window, NULL, &rect);
		GetGadgetRect(GadgetItem(window->FirstGadget, SEL_BOX), window, NULL, &tempRect);
		SetAPen(window->RPort, _tbPenLight);
		rect.MinX -= 2;
		rect.MaxY = tempRect.MaxY + 1;
		FillRect(window->RPort, &rect);
		RemoveGList(window, runtimeGadgets, NUM_EXT_GADS);
		AddGList(window, mainSelGadgets, -1, 2, NULL);
		RefreshGList(mainSelGadgets, window, NULL, 2);		// Fall through
	default:
		EnableGadgetItem(window->FirstGadget, BUTTON_PARENT, window, NULL, FALSE);
		break;
	}
	SetCurrentDev();
}

/*
 * Call when SetModeOp() is changing operation to OPER_SELECTION
 * Adds all the buttons back, which is either two or four,
 * depending on whether the "Enter" and "Back" buttons were
 * already present. Also resets selection variables and displays
 * initial stats in the window.
 */
 
static void GoToSelMode(UBYTE oldOper)
{
	register WORD num;
	register GadgetPtr gadgPtr;
	register WindowPtr window = cmdWindow;
	Rectangle rect, tempRect;
	register BOOL completeBackup = (mode == MODE_BACKUP && backupType == 0);

	if (oldOper != -1) {	
		switch (oldOper) {
		case OPER_PREP:
		case OPER_IN_PROG:
		case OPER_COMPLETE:
		case OPER_PAUSE:
			EnableGadgetItem(window->FirstGadget, BUTTON_1, window, NULL, TRUE);
			EnableGadgetItem(window->FirstGadget, BUTTON_2, window, NULL, TRUE);
			GetGadgetRect(runtimeGadgets, window, NULL, &rect);
			GetGadgetRect(GadgetItem(window->FirstGadget, TOTAL_BOX), window, NULL, &tempRect);
			SetAPen(window->RPort, _tbPenLight);
			rect.MaxY = tempRect.MaxY + 1;
			FillRect(window->RPort, &rect);
			RemoveGList(window, runtimeGadgets, NUM_EXT_GADS);
			gadgPtr = mainSelGadgets;
			num = NUM_EXT_GADS;
/*
	If coming from an operation, and can't go back to selection mode,
	jump to limbo mode again.
*/
			if (completeBackup) {
				AddGList(window, gadgPtr, -1, num, NULL);
				RefreshGList(gadgPtr, window, NULL, num);
				HandleOperationButton(FALSE, FALSE);
				return;
			}
			break;
		default:
			gadgPtr = tagGadgets;
			num = NUM_EXT_GADS - 2;
			break;
		}
		if (!completeBackup) {
			AddGList(window, gadgPtr, -1, num, NULL);
			RefreshGList(mainSelGadgets, window, NULL, NUM_EXT_GADS);
			EnableGadgetItem(window->FirstGadget, BUTTON_PARENT, window, NULL, FALSE);
			enterState = FALSE;
			tagsDone = FALSE;
			DisplayStats(TRUE);
		}
		else {
/*
	If not coming from an operation, and can't go to selection mode,
	jump directory to OPER_PREP, and if cancelled, go back to limbo.
*/
			skipUpdate = skippedAdd = TRUE;
			completeBackup = HandleOperationButton(oldOper == OPER_SCAN, FALSE);
			skipUpdate = FALSE;
			if (!completeBackup)
				HandleOperationButton(FALSE, FALSE);
		}
	}
}

/*
 * Do script backup if possible.
 */
 
BOOL DoScriptBackup(TextPtr optName)
{
	return ( DoBackupRestore(MODE_BACKUP, optName));
}

/*
 * Do script restore if possible.
 */
 
BOOL DoScriptRestore(TextPtr optName)
{
	return ( DoBackupRestore(MODE_RESTORE, optName));
}

/*
 * Handle "Backup" or "Restore" from script.
 */
 
static BOOL DoBackupRestore(UBYTE newMode, register TextPtr argText)
{
	BOOL success;
	register UBYTE *ptr;
	UWORD len;
	TextChar buff[PATHSIZE];				/* It's alot of stack space. Be careful. */
	
	GetNextWord(argText, buff, &len);
	if (operation == OPER_SELECTION) {
		HandleOperationButton(FALSE, FALSE);
	} else if (operation == OPER_COMPLETE) {
		HandleOperationButton(TRUE, FALSE);
	}
	if (success = (operation != OPER_SELECTION) && (operation != OPER_PREP) && (operation != OPER_PAUSE) && (operation != OPER_IN_PROG)) {
		if (mode == MODE_LIMBO) {
			strcpy(strPath, buff);
/*
	Now determine the correct icon type to display in the window
	for the volume to be backed up
*/
			stccpy(buff, strPath, MAX_FILENAME_LEN+1);
			ptr = buff;
/*
	If in test mode, no volume specifier is needed
*/
			if (newMode != MODE_RESTORE || (prefs.OldPrefs.TestMode != RMODE_TEST)) {
				while (*ptr && *ptr != ':') { 
					ptr++;
				}
			}
			*ptr = 0;
			success = DoLoadDirectory(newMode, buff);
		}
	}
	return (success);
}

/*
 * Change from limbo mode to scanning of catalog, as a result of
 * either the "Backup" or "Restore" button being hit.
 */
 
BOOL EnterScanMode(UBYTE newMode, BOOL altOn)
{
	register WORD listItem;
	register BOOL success = FALSE;
	
	listItem = SLNextSelect(scrollList, -1);
	if (listItem != -1) {
		SLGetItem(scrollList, listItem, strBuff);
		iconType = strBuff[1];
		if (strPath[0] == 0) {
			strcat(&strBuff[2], strColon);
		}
		AppendDirPath(strPath, &strBuff[2]);
		backupType = prefs.OldPrefs.BackupType;
		success = LoadDirectory(newMode, altOn);
	}
	return (success);
}

/* 
 * Calculate the FLAG_SEL status for the current level.
 * Go all the way to the root if "toRoot" is TRUE.
 */

BOOL UpdateDirStatus(BOOL toRoot)
{
	register BOOL fullySel;
	register BOOL anySel;
	register DirFibPtr nextFib;
	register UBYTE flags;
	BOOL cont = TRUE;
	BOOL success = FALSE;
	
	if (curLevel != NULL) {
		while (curLevel->dl_ParLevel != NULL && cont) {
			nextFib = curLevel->dl_ChildPtr;
			fullySel = TRUE;
			flags = curLevel->dl_ParFib->df_Flags;
			anySel = (curLevel->dl_ChildPtr == NULL) && (flags & FLAG_SEL_MASK);
			while (nextFib != NULL) {
				flags = nextFib->df_Flags;
				if (flags & FLAG_SEL_MASK) {
					anySel = TRUE;
					if (flags & FLAG_PART_MASK) {
						fullySel = FALSE;
					}
				} else {
					fullySel = FALSE;
				}
				nextFib = nextFib->df_Next;
			}
			nextFib = curLevel->dl_ParFib;
			curLevel = curLevel->dl_ParLevel;
			if (nextFib != NULL) {
				if (anySel) {
					nextFib->df_Flags |= FLAG_SEL_MASK;
					if (!fullySel) {
						nextFib->df_Flags |= FLAG_PART_MASK;
					} else {
						nextFib->df_Flags &= ~FLAG_PART_MASK;
					}
				} else {
					nextFib->df_Flags &= ~FLAG_SEL_MASK;
				}
			}
			success = TRUE;
			cont = toRoot;
		}
	}
	return (success);
}

/*
 * Return a plain-English string for the error code passed.
 */
 
TextPtr GetIOErrStr(WORD err)
{
	register TextPtr str;
		
	switch (err) {

	case ERROR_OBJECT_IN_USE:
		str = strInUseErr;
		break;
	case ERROR_OBJECT_EXISTS:
		str = strExistsErr;
		break;
	case ERROR_DISK_NOT_VALIDATED:
	case ERROR_DEVICE_NOT_MOUNTED:
	case ERROR_NOT_A_DOS_DISK:
	case ERROR_NO_DISK:
	case ERROR_DIR_NOT_FOUND:
	case ERROR_OBJECT_NOT_FOUND:
		str = strNotFoundErr;
		break;
	case ERROR_DISK_FULL:
		str = strDiskFullErr;
		break;
	case ERROR_DELETE_PROTECTED:
		str = strDelProtErr;
		break;
	case ERROR_DISK_WRITE_PROTECTED:
	case ERROR_WRITE_PROTECTED:
		str = strWriteProtErr;
		break;
	case ERROR_READ_PROTECTED:
		str = strReadProtErr;
		break;
	default:
		dialogVarNum = err;
		(void) ParseDialogString(strGenIOErr, strBuff);
		str = strBuff;
		break;
	}
	return (str);
}

/*
 * Initialize active cylinder and disk
 */
 
void InitPosition()
{
	sessionNumber = 0;
	sessionCurrCylinder = activeCylinder = 0;
	sessionCurrDisk = activeDisk = firstTape = 1;
}

/*
 * Initialize completed files and bytes
 */
 
void InitStats()
{
	inputCompleted = outputCompleted = FALSE;
	lastTick = 0L;
	BlockClear(&stats, sizeof(StatStruct));
}

/*
 * Warns that file in curFib cannot be backed up or restored.
 * Also sets flag for report.
 */
 
void UnableToProcess(DirFibPtr dirFib, TextPtr str, WORD errLevel)
{
	/* TextChar buff[GADG_MAX_STRING]; */
	
	if ((dirFib->df_Flags & FLAG_ERR_MASK) == 0) {
		stats.numErrors++;
		dirFib->df_Flags |= FLAG_ERR_MASK;
	}
	AppendToRuntimeItem(str);
	scriptError = MAX(scriptError, errLevel);
	
	/*
	if (prefs.LogOpts.LogLevel != LOG_RESULTS) {
		ConstructRuntimeItem(dirFib, buff);
		DisplayErrStr(buff, str);
	} else {
		AppendToRuntimeItem(str);
	}
	*/
}

/* 
 * Init Directory structure
 */

void InitDirectory()
{
	curLevel = &rootLevel;
	rootLevel.dl_CurFib = rootLevel.dl_ChildPtr; 
	InitPath();
}

/* 
 * Resets dir list
 */
 
void InitList()
{
	levelFlag = TRUE;
	InitDirectory();
	curLevel->dl_CurFib = NULL;	/* Was curLevel->dl_ChildPtr? */
	absolutePos = catSize + catCorruptAddition;
	if (extraLen) {
		BumpPositionMarker();
	}
	stats.finishedTapeBytes = absolutePos + extraLen;
	absolutePos = 0;
}
	
/*
 * Initialize path var - if it doesn't end with colon, ensure slash at end.
 */
 
void InitPath()
{
	register int len;
	register TextChar ch;
	register TextPtr ptr;
	
	if (mode == MODE_RESTORE && verifyMode == RMODE_TEST)
		ptr = strSavePath;
	else
		ptr = prefs.OldPrefs.DevPathBuf;
	len = stccpy(strPath, ptr, DEVICE_PATH_BUFFER_SIZE);
	ch = strPath[len-1];
	if (ch != ':' && ch != '/') {
		strPath[len] = '/';
		strPath[len+1] = 0;
	}
	indentLevel = 1;
}			

/* 
 * Scans the catalog to recalculate selected and total files, selected and
 * total bytes, and selected and total volumes.
 */
 
void RecalcNumbers(BOOL recalcCat)
{
	register DirFibPtr nextFib, dirFib;
	register DirLevelPtr dirLevel;
	register ULONG sBytes, tBytes, tCat;
	register ULONG size;
	register ULONG vSize = volumeSize;
	BYTE flags;
	WORD len;
	DirFib tempFib;
	BPTR saveRootLock = rootLevel.dl_DirLock;
	BOOL isLink;
	
	tBytes = sBytes = totalBytes = 
		totalFiles = selFiles = selBytes = totalVols = selVols = 0;
	tCat = sizeof(UWORD);									/* Number of entries */
	dirLevel = &rootLevel;
	dirFib = &tempFib;
	nextFib = dirLevel->dl_ChildPtr;
	while (dirLevel != NULL) {

		while (nextFib != NULL && (nextFib->df_Flags & FLAG_DIR_MASK) && ((nextFib->df_Flags & FLAG_HLINK_MASK) == 0) && (nextFib->df_SubLevel != NULL)) {
			dirLevel->dl_GNFib = nextFib;
			dirLevel->dl_DirLock = (ULONG) dirFib;		/* Save ptr to level's FIB */
			/* nextFib->df_Flags &= ~FLAG_SEL_MASK;*/	/* Dir unselected */
			dirLevel = (DirLevelPtr) nextFib->df_SubLevel;
			dirFib = nextFib;
			nextFib = dirLevel->dl_ChildPtr;
		}
		if (nextFib != NULL) {
			if ((isLink = nextFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK)) == 0) {
				size = nextFib->df_Size;
				totalBytes += size;
				if (mode != MODE_RESTORE || qbCode == QB_ID) {
					size += DELIMITER_SIZE + sizeof(ULONG) + (((size - 1) & 3) ^ 3);
					if (mode == MODE_BACKUP || useSortFormat > 1)
						size += nextFib->df_ActualSize - nextFib->df_Size;
				}
				if (nextFib->df_Flags & FLAG_SEL_MASK) {
					++selFiles;
					selBytes += nextFib->df_Size;
					sBytes += size;
				}
				++totalFiles;
				tBytes += size;
			}
nextC:
			if ((nextFib->df_Flags & FLAG_SEL_MASK) || (IS_DIR(nextFib->df_Flags) && prefs.OldPrefs.BAllDirs)) {
				len = strlen(nextFib->df_Name);
				if (nextFib->df_Comment != NULL) {
					len += strlen(nextFib->df_Comment);
				}
				if (nextFib->df_LinkName != NULL) {
					len += strlen(nextFib->df_LinkName) + 1;
				}
				tCat += len + 2 + CAT_ENT_SIZE;	/* Add two NULL bytes plus entry size */
				if (useSortFormat) {
					tCat += sizeof(ULONG);
				}
			}
			nextFib = nextFib->df_Next;
		} else {
			dirLevel = dirLevel->dl_ParLevel;
			if (dirLevel != NULL) {
				flags = dirFib->df_Flags;
				dirFib = (DirFibPtr) dirLevel->dl_DirLock;	/* Restore ptr to FIB */
				if (mode == MODE_BACKUP) {
					dirFib->df_Flags |= (flags & FLAG_SEL_MASK);
				}
				nextFib = dirLevel->dl_GNFib;
				goto nextC;
			}
		}
	}
	if (recalcCat) {
		catSize = tCat;
	}
	if (vSize) {
		tBytes += tCat;
		if (mode != MODE_RESTORE || qbCode == QB_ID) {
/*
	On new Quarterbacks, add the padding after catalog to longword, the two end
	delimiters, and the newer first volume header. Also subtract the subsequent
	volume header from the size of a volume.
*/
			size = (((tCat - 1) & 3) ^ 3);
			tBytes += size + (2*sizeof(ULONG)) + sizeof(QBNewFirstCylHdr);
			sBytes += size + (2*sizeof(ULONG)) + sizeof(QBNewFirstCylHdr);
			vSize -= sizeof(QBFirstCylHdr);
		}
		if (size = tBytes % trkBufSize) {
			tBytes += trkBufSize - size;
		}
		totalVols = (tBytes + tCat + (vSize - 1)) / vSize;
		if (selFiles) {
			if (mode == MODE_RESTORE) {
				selVols = totalVols;
			} else {
				tBytes = sBytes + tCat;
				if (size = tBytes % trkBufSize) {
					tBytes += trkBufSize - size;
				}
				selVols = (tBytes + tCat + (vSize - 1)) / vSize;
			}
		}
	}
	rootLevel.dl_DirLock = saveRootLock;
}

/*
 * Appends name passed to path variable. Limits path to PATHSIZE length.
 * On restore, if full path NOT being restored, does nothing. Algorithm
 * appends a "/" if another "/" is detected before the volume specifier 
 * (":") is reached.
 */

BOOL AppendPath(register TextPtr str)
{
	BOOL success = FALSE;
	
	if (mode != MODE_RESTORE || prefs.OldPrefs.RSubdirs) {
		success = AppendDirPath(strPath, str);
	}
	return (success);
}

/*
 * Unconditionally appends file/dir name "str" to vol/dir spec "strPathname"
 */
 
BOOL AppendDirPath(register TextPtr strPathname, register TextPtr str)
{
	register UWORD len;
	register TextChar ch;
	BOOL success = FALSE;
	
	len = strlen(strPathname);
	if (len + strlen(str) < PATHSIZE - 2) {
		success = TRUE;
		if (len) {
			ch = strPathname[len-1];
			if (ch != ':' && ch != '/') {
				strPathname[len] = '/';
				strPathname[len+1] = 0;
			}
		}
		strcat(strPathname, str);
	}
	return (success);
}

/*
 * Call TruncatePath() if in backup or restoring subdirectories
 */
 
static void DoTruncatePath()
{
	if (mode != MODE_RESTORE || prefs.OldPrefs.RSubdirs) {
		TruncatePath();
	}
}

/*
 * Deletes last subdir name from path variable. On restore, if full path
 * NOT being restored, does nothing. Algorithm deletes the trailing "/"
 * if there is one, but not a trailing ":". If neither present, you get
 * an empty string as the result.
 */
 
void TruncatePath()
{
	register UWORD len;
	register TextPtr str;
	
	if ((len = strlen(strPath)) != 0) {
		str = &strPath[len];
		while ((str > strPath) && (*(str-1) != ':') && (*str != '/'))
			str--;
		*str = 0;					/* Null-terminate truncated string */
	}
}

/*
 * Returns ptr to FIB of the next selected file to backup or restore in A0,
 * or NULL if no more entries to process.
 * If "ignoreSel" TRUE, then it returns the next selected OR unselected file,
 * used to process special tag filtering and currently restores of compressed
 * data where we cannot directly seek to the correct spot on the backup.
 */

DirFibPtr NextFile(BOOL ignoreSel, BOOL stopOnDirs)
{
	register DirFibPtr nextFib;
	TextPtr namePtr;
	register BOOL done = FALSE;
	BOOL hasArchive;
	Dir dir;
	register BOOL isRestoreMode = mode != MODE_BACKUP;
	register BOOL changeDirs;
	register UBYTE temp;
	
	changeDirs = ((!isRestoreMode) || (outputCompleted && verifyMode)) && (operation != OPER_SELECTION);
	
	if ((nextFib = curLevel->dl_CurFib) == NULL) {
		nextFib = curLevel->dl_ChildPtr;
		done = TRUE;
	}
	do {
		if (!levelFlag && ((nextFib->df_Flags & (FLAG_DIR_MASK | FLAG_HLINK_MASK | FLAG_SLINK_MASK)) == 0)) {
			if (!isRestoreMode || qbCode == QB_ID) {
				absolutePos += DELIMITER_SIZE + sizeof(ULONG);
				if (!processAll)
					absolutePos += nextFib->df_ActualSize;
				BumpPositionMarker();
			}
			else
				absolutePos += nextFib->df_Size;
		}
		levelFlag = FALSE;				/* Is reset to true if a dir follows */
		while (!done) {
			levelFlag = FALSE;			/* Yes, we DO need this here too. */
			if (nextFib != NULL) {
				nextFib = nextFib->df_Next;
			}
			if (nextFib == NULL) {
				if (changeDirs && (curLevel != &rootLevel)) {
					UnLock((Dir) curLevel->dl_DirLock);
					curLevel->dl_DirLock = NULL;
				}
/*
	If any links or files do not have errors in this directory, enable archive
	bit to be set if necessary.
*/
				hasArchive = curLevel->dl_FilCnt == 0;
				if (outputCompleted) {
					for (nextFib = curLevel->dl_CurFib ; nextFib != NULL ; nextFib = nextFib->df_Next) {
						if (nextFib != NULL) {
							temp = nextFib->df_Flags;
							if ((((temp & FLAG_DIR_MASK) == 0) ||
								((temp & (FLAG_HLINK_MASK | FLAG_SLINK_MASK)))) &&
								((temp & FLAG_ERR_MASK) == 0)) {
								hasArchive = TRUE;
								nextFib = NULL;
								break;
							}
						}
					}
				}
				curLevel = curLevel->dl_ParLevel;
				
				if (curLevel == NULL) {
					done = TRUE;			/* Completed root, exit from routine now. */
				} else {
					curFib = nextFib = curLevel->dl_CurFib;
/*
	Possibly update the file info for this file.
	If a restore, go ahead and update the date/comment/prot fields here, respecting
		the setting of empty folders (which are never created and thus ignored here).
	If a backup, there must be a need to set the archive bit in order to proceed.
*/
					if ((nextFib->df_Flags & FLAG_SEL_MASK) && operation == OPER_IN_PROG &&
						((isRestoreMode && prefs.OldPrefs.RSubdirs && !verifyMode && ((nextFib->df_SubLevel && (((DirLevelPtr)nextFib->df_SubLevel)->dl_ChildPtr)) || prefs.OldPrefs.REmptySubdirs)) ||
						(startMode == MODE_BACKUP && hasArchive))) {
						UpdateProtectionBits(strPath);
					}
					indentLevel--;
					DoTruncatePath();
					if (errFillLevelCount)
						errFillLevelCount--;
					if (changeDirs) {
						if (curLevel->dl_DirLock == NULL)
							curLevel->dl_DirLock = Lock(strPath, ACCESS_READ);
						if (curLevel->dl_DirLock != NULL)
							CurrentDir((Dir) curLevel->dl_DirLock);
					}
				}
			}
			else
				done = TRUE;				/* Stop, there is another entry at this level. */
		}
		if (curLevel != NULL) {
			done = FALSE;
			curFib = nextFib;				/* Check out the new current entry at this level. */
			curLevel->dl_CurFib = nextFib;
/*
	When skipping past an unselected file, the compression cache will no longer
	be valid (the delimiter length is longer than "useCache" can be.
*/	
			temp = nextFib->df_Flags;
			if ((temp & FLAG_DIR_MASK) == 0 && (temp & FLAG_SEL_MASK) == 0) {
				useCache = 0;
			}
			if ((temp & FLAG_DIR_MASK) == 0 ||
				(temp & (FLAG_HLINK_MASK | FLAG_SLINK_MASK))) {
				if (ignoreSel || (temp & FLAG_SEL_MASK))
					done = TRUE;			/* If file selected, we're done! */
			}
			else {
				namePtr = nextFib->df_Name;
				if (ignoreSel || (temp & FLAG_SEL_MASK) || (isRestoreMode && prefs.OldPrefs.REmptySubdirs)) {
					if (isRestoreMode && prefs.OldPrefs.RSubdirs && !verifyMode) {
						strcpy(strBuff, strPath);
						AppendDirPath(strBuff, namePtr);
						if ((temp & FLAG_SEL_MASK) && operation != OPER_SELECTION &&
							 ((nextFib->df_SubLevel && (((DirLevelPtr)nextFib->df_SubLevel)->dl_ChildPtr)) || prefs.OldPrefs.REmptySubdirs)) {
/*
	Create a new directory to restore into.
*/
							dir = Lock(strBuff, ACCESS_READ);
							if (dir == NULL) {
								dir = CreateDir(strBuff);
								if (dir == NULL) {
									dialogVarStr = strBuff;
									WarningDialog(ERR_CREATEDIR, FALSE);
									done = TRUE;
									abortFlag = TRUE;
									curLevel = NULL;
									nextFib = NULL;
								}
							}
							UnLock(dir);
						}
					}
					if (!done) {
						curFib = curLevel->dl_CurFib;
						curLevel = (DirLevelPtr) curFib->df_SubLevel;
						curLevel->dl_CurFib = (DirFibPtr) &curLevel->dl_ChildPtr;
						levelFlag = TRUE;
						AppendPath(namePtr);
						if ((curFib->df_Flags & FLAG_SEL_MASK) && (!isRestoreMode || curFib->df_FilCnt || verifyMode || prefs.OldPrefs.REmptySubdirs)) {
							if (prefs.LogOpts.LogLevel != LOG_RESULTS)
								DisplayNextFile(curFib);	/* Display drawer name */
							if (changeDirs) {
								curLevel->dl_DirLock = (ULONG) Lock(namePtr, ACCESS_READ);
								if (curLevel->dl_DirLock != NULL) {
									CurrentDir((Dir) curLevel->dl_DirLock);
								} else {
									if (operation != OPER_SELECTION) {
										UnableToProcess(curFib, GetIOErrStr(IoErr()), RC_WARN);
									}
									errFillLevelCount++;
								}
							}
						}
						indentLevel++;
						if (stopOnDirs) {
							return (nextFib);
						}
						nextFib = curFib;
					}				
				} else if (!processAll) {
					ScanUnselDir(nextFib);
				}
			}
		}
		if (curLevel != NULL) {
			nextFib = curLevel->dl_CurFib;
		}
	} while (!done);
	return ( nextFib);
}

/*
 * Upon detection of the first protection bit error, present a requester
 * which allows the skipping of the current file, the retrying of the
 * current file, or the canceling of subsequent warnings.
 * NOTE: The variable "firstProtErr" should be reset each loop operation!
 */
 
void UpdateProtectionBits(TextPtr fileName)
{
	register BOOL done;
	UBYTE choice; /* = CANCEL_BUTTON*/;
	
	choice = OK_BUTTON;
	dialogVarStr = curFib->df_Name;
	do {
		if (!(done = RestoreProtectionBits(fileName))) {
			if (!(done = firstProtErr)) {
				choice = WarningDialog(ERR_PROT_ERR, 2);
				done = choice != OK_BUTTON;
				firstProtErr = choice == CANCEL_BUTTON;
			}
		}
	} while (!done);
	if (choice != OK_BUTTON && !inputCompleted)
		UnableToProcess(curFib, (mode == MODE_BACKUP) ? strBackProtErr : strRestProtErr, RC_WARN - 2);
}
				
/* 
 * Scans an unselected dir, adding its files and subdirs to AbsolutePos.
 */
 
static void ScanUnselDir(register DirFibPtr dirFib)
{
	register BOOL done = FALSE;
	register DirLevelPtr level;
	register BOOL levelDone;
			
	while (!done) {
		level = (DirLevelPtr) dirFib->df_SubLevel;
		dirFib = level->dl_ChildPtr;
		for (levelDone = FALSE; !levelDone;) {
			if (dirFib != NULL) {
				level->dl_CurFib = dirFib;
				if ((dirFib->df_Flags & FLAG_DIR_MASK) == 0) {
					if ((dirFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK)) == 0) {
						if (!processAll) {
							useCache = 0;
							absolutePos += dirFib->df_ActualSize;
							if (mode == MODE_BACKUP || qbCode == QB_ID) {
								absolutePos += DELIMITER_SIZE + sizeof(ULONG);
								BumpPositionMarker();
							}
						}
					}
					dirFib = dirFib->df_Next;
				} else {
					levelDone = TRUE;
				}
			} else {
				level = level->dl_ParLevel;
				dirFib = level->dl_CurFib;
				if (curLevel == level) {
					levelDone = done = TRUE;
				} else {
					dirFib = dirFib->df_Next;
				}
			}
		}
	}
}
				
/*
 * Bump absolutePos variable to nearest longword boundary
 */
 		
void BumpPositionMarker()
{
	if (mode == MODE_BACKUP || qbCode == QB_ID) {
		if (absolutePos & 3) {
			absolutePos |= 3;
			absolutePos++;
		}
	}
}
	
/*
 * Initialize file directory structure
 */
 
void InitFileDir()
{
	curLevel = &rootLevel;
	curLevel->dl_CurFib = NULL;
}

/*
 * Gets next file or dir. Used for catalog writes only.
 */
 
DirFibPtr GetFileDir()
{
	register DirFibPtr nextFib;
	register BOOL done = FALSE;
	
	nextFib = curLevel->dl_CurFib;
	if (nextFib != NULL) {
		if (IS_DIR(nextFib->df_Flags)) {
			curLevel->dl_GNFib = nextFib;
			curLevel = (DirLevelPtr) nextFib->df_SubLevel;
			nextFib = curLevel->dl_ChildPtr;	/* First entry at new level */
		} else {
			nextFib = nextFib->df_Next;		/* Link to next in this chain */
		}
	} else {
		nextFib = curLevel->dl_ChildPtr;		/* First entry at new level */
	}
	while (!done) {
		if (nextFib != NULL) {
			if (IS_DIR(nextFib->df_Flags)) {
				nextFib->df_FilCnt = CountEntries((DirLevelPtr)nextFib->df_SubLevel);
			}
			curLevel->dl_CurFib = nextFib;
			done = TRUE;
		} else {
			if (curLevel->dl_ParLevel == NULL) {
				done = TRUE;						/* Root level, that's all folks! */
				nextFib = NULL;					/* Return NULL */
			} else {
				curLevel = curLevel->dl_ParLevel;
				nextFib = curLevel->dl_GNFib->df_Next;
			}
		}
	}
	return (nextFib);
}

/*
 * Counts entries in next level defined by the dir entry passed.
 */
 
UWORD CountEntries(DirLevelPtr level)
{
	register UWORD num = 0;
	register DirFibPtr nextFib;
	register UBYTE flags;
		
	nextFib = level->dl_ChildPtr;
	while (nextFib != NULL) {
		flags = nextFib->df_Flags;
		if ((IS_DIR(flags) && prefs.OldPrefs.BAllDirs) || (flags & FLAG_SEL_MASK)) {
			++num;
		}
		nextFib = nextFib->df_Next;
	}
	return (num);
}

/*
 * Call LoadDirectory() from a script. Search for volume "name" to select
 * before proceeding.
 */
 
BOOL DoLoadDirectory(UBYTE newMode, TextPtr name)
{
	register WORD i, numItems;
	
	mode = newMode;
	iconType = DRAWER_ICON;
	numItems = SLNumItems(scrollList);
	
	for (i = 0 ; i < numItems ; i++) {
		SLGetItem(scrollList, i, strBuff);
		if (stricmp(&strBuff[2], name) == 0) {
			SLSelectItem(scrollList, i, TRUE);
			iconType = strBuff[1];
		}
	}
	return ( LoadDirectory(mode, FALSE));
}

/*
 * Here we build the catalog. If backup, builds catalog from AmigaDOS dir.
 * If restore, loads catalog from first disk of QB-format backup diskette.
 */
 
BOOL LoadDirectory(register UBYTE newMode, BOOL altOn)
{
	BOOL success;
	register TextPtr strPtr;
	BOOL recalc;
/*
	Note: this means that if path exceeds 120 characters, you won't be
	able to save it in your preferences correctly.
*/
	startMode = newMode;
	
	SetModeOp(newMode, OPER_SCAN);
	strPtr = strPath;
	stccpy(prefs.OldPrefs.DevPathBuf, strPath, DEVICE_PATH_BUFFER_SIZE);
	if (newMode == MODE_RESTORE) {
		strPtr = strchr(strPtr, ':');
		stccpy(strSavePath, (strPtr == NULL) ? strPath : strPtr, DEVICE_PATH_BUFFER_SIZE);
		currDevPtr = &prefs.CurrRestoreDev[0];
		currUnit = prefs.CurrRestoreUnit;
		catErrFlag = altOn;
		success = LoadDiskCatalog();
		recalc = useRecalcCatSize;
	} else {
#ifdef QB_NEW_CATALOG
		useSortFormat = 2;
#endif
		if (!prefs.DisplayBackupOpts || (success = DoBackupOptions())) {
			if (success = BuildDir()) {
				backupType = prefs.OldPrefs.BackupType;
				strcpy(strSavePath, strPath);
				if ((strPtr = strchr(strSavePath, ':')) != NULL)
					*strPtr = 0;
				strSavePath[MAX_BACKUPNAME_LEN-1] = 0;
				strcpy(currBackupName, strSavePath);
				strcpy(currVolName, strSavePath);
				currComment[0] = 0;
				currDevPtr = &prefs.CurrBackupDev[0];
				currUnit = prefs.CurrBackupUnit;
			}
		}
		recalc = TRUE;
	}
	skipUpdate = success;
	if (success) {
		RecalcNumbers(recalc);
		TagFilter(FALSE, FALSE);
		SetModeOp(mode, OPER_SELECTION);
		if (operation == OPER_SELECTION) {
			skipUpdate = FALSE;
			success = BuildFileList(NULL);
		}
	}
	if (!success && mode != MODE_LIMBO) {
		SetModeOp(MODE_LIMBO, OPER_LIMBO);
		FreeDirBlocks();
		BuildVolumeList(prefs.OldPrefs.DevPathBuf);
	}
	abortFlag = FALSE;
	return (success);
}

/*
 * Returns TRUE if filename is not "*" and does not contain a slash
 */
 
static BOOL ValidFilename(TextPtr fileName)
{
	register BOOL valid;
	register TextPtr str = fileName;
	
	valid = strcmp(fileName, "*");
	while (*str && valid) {
		if (*str++ == '/')
			valid = FALSE;
	}
	return (valid);
}

/*
 * Builds the catalog of files from the selected device:path.
 */

BOOL BuildDir()
{
	BOOL success = FALSE;
	
	totalFiles = 0;
	curDBBase = (DirFibPtr) &baseDirFib;		/* Kludgy */
	curLevel = &rootLevel;
	BlockClear( (Ptr) &rootLevel, sizeof(DirLevel));
	BeginWait();
	if (!AllocateDirBlock(TRUE) || ( (requester = DoGetRequest(REQ_SCAN)) == NULL)) {
		FreeDirBlocks();
		Error(ERR_NO_MEM);
	} else {
		SetModeOp(mode, OPER_SCAN);
		InitScanRequester(requester);
		InitPath();
		if (PathLock()) {
			success = ScanLevel(FALSE);
		} else {
			DiskBad();
			ScanLevel(TRUE);		/* Unlocks the lock */
			success = FALSE;
		}
		DestroyRequest(requester);
	}
	EndWait();
	return (success);
}

/* 
 * Read files and subdirs at a given level, at previous level if "prev" TRUE.
 */
 
BOOL ScanLevel(BOOL prev)
{
	register BOOL done;
	register BOOL contScan;
	register DirFibPtr nextFib;
	TextPtr namePtr;
	register BOOL success = TRUE;
	register struct FileInfoBlock *fibPtr = (struct FileInfoBlock *) fib;
	BOOL isLink;
	
	do {
		if (prev) {
			if (curLevel->dl_DirLock != NULL) {
				UnLock(curLevel->dl_DirLock);
				curLevel->dl_DirLock = NULL;
			}
			done = curLevel->dl_ParLevel == NULL;
			if (!done) {
				curLevel = curLevel->dl_ParLevel;
				DoTruncatePath();
			}
		} else {
			done = TRUE;
			if (Examine(curLevel->dl_DirLock, fibPtr) == NULL || fibPtr->fib_DirEntryType < 0) {
				DiskBad();
				ScanLevel(TRUE);
/*
	Retry after warning - maybe volume is online now. Otherwise, abort!
				if (Examine(curLevel->dl_DirLock, fibPtr) == NULL) || ( fibPtr->fib_DirEntryType < 0)) {
*/
					BuildAbort();
					success = FALSE;
				/*} else {
					done = FALSE;
				}*/
			} else {
				if (!ScanDir()) {
					BuildAbort();
					success = FALSE;
				} else {
					curLevel->dl_CurFib = (DirFibPtr)&curLevel->dl_ChildPtr;
					done = FALSE;
				}
			}
		}
		if (!done) {
/*
	NextEntry() directory scan loop
*/
			nextFib = curLevel->dl_CurFib;
			contScan = TRUE;
			while (contScan) {
				nextFib = nextFib->df_Next;
				isLink = nextFib && (nextFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK));
				if (nextFib == NULL || ( (nextFib->df_Flags & FLAG_DIR_MASK) == 0) || isLink) {
					prev = TRUE;				/* Check out previous level */
					contScan = nextFib != NULL;
				} else {
					curLevel->dl_CurFib = nextFib;
					namePtr = nextFib->df_Name;
					if (AllocateDirBlock(TRUE) == NULL) {
						BuildAbort();
						success = FALSE;
						done = TRUE;
					} else {	
						BlockClear( curDBPtr, sizeof(DirLevel));
						curLevel->dl_CurFib->df_SubLevel = (LONG) curDBPtr;
						((DirLevelPtr)curDBPtr)->dl_ParLevel = curLevel;
						curLevel = (DirLevelPtr) curDBPtr;
						curLevel->dl_ChildPtr = NULL;
						curLevel->dl_CurFib = (DirFibPtr) &curLevel->dl_ChildPtr;
						AppendPath(namePtr);	/* Add new subdirectory to existing path */
						if (PathLock()) {
							prev = FALSE;		/* Successful, so check out this level */
							contScan = FALSE;
						} else {
							DiskBad();
							DoTruncatePath();
						}
						StuffScanDirText(strPath);
						StuffScanFileText(NULL);
					}
				}
			}
		}	
	} while(!done);
	return (success);
}

/*
 * Build entries of both types for current subdirectory. Return TRUE
 * if program should continue, FALSE if operation should abort.
 */
 
BOOL ScanDir()
{
	register BOOL continueScan = TRUE;
	/*register BOOL abort;*/
	register struct FileInfoBlock *fibPtr = (struct FileInfoBlock *)fib;
	register LONG entryType;
	register WORD len;
	
	while (continueScan &= (!(/*abort =*/ CheckAbortButton() || ExNext(curLevel->dl_DirLock, fibPtr) == NULL))) {
		if (ValidFilename(fibPtr->fib_FileName)) {
			if (continueScan = AllocateDirBlock(FALSE) != NULL) {
				curLevel->dl_CurFib = curDBPtr;
				entryType = fibPtr->fib_DirEntryType;
				curDBPtr->df_Next = NULL;
				curDBPtr->df_Size = (ULONG) fibPtr->fib_Size;
				curDBPtr->df_ActualSize = (ULONG) fibPtr->fib_Size;
				curDBPtr->df_DateStamp = fibPtr->fib_Date;
				curDBPtr->df_FilCnt = 0;
				curDBPtr->df_Prot = (UBYTE) fibPtr->fib_Protection;
				curDBPtr->df_Flags = FLAG_SEL_MASK |
				    ( ( (entryType == ST_SOFTLINK) ? FLAG_SLINK_MASK : 0) +
					( ( (entryType == ST_LINKDIR) || (entryType == ST_LINKFILE)) ? FLAG_HLINK_MASK : 0) +
					( (entryType < 0) ? 0 : FLAG_DIR_MASK));
				if ((curDBPtr->df_Name = MemAlloc(strlen(fibPtr->fib_FileName)+1, MEMF_CLEAR)) != NULL) {
					strcpy(curDBPtr->df_Name, fibPtr->fib_FileName);
					if (fibPtr->fib_Comment[0]) {
						if (fibPtr->fib_Comment[MAX_COMMENT_LEN-2]) {
							len = MAX_COMMENT_LEN-1;
						} else {
							len = strlen(fibPtr->fib_Comment);
						}
						if ((curDBPtr->df_Comment = NewPtr(len+1)) != NULL) {
							BlockMove(fibPtr->fib_Comment, curDBPtr->df_Comment, len);
							curDBPtr->df_Comment[len] = 0;
						}
					} else {
						curDBPtr->df_Comment = NULL;
					}
					GetLinkName(fibPtr->fib_FileName, &curDBPtr->df_LinkName);
					if (curLevel->dl_ChildPtr == NULL) {
						curLevel->dl_ChildPtr = curLevel->dl_CurFib;
					} else {
						SortInsert();
					}
					if (fibPtr->fib_DirEntryType < 0) {
						StuffScanFileText(fibPtr->fib_FileName);
						totalFiles++;
					}
					len = TextLength(cmdWindow->RPort, fibPtr->fib_FileName, strlen(fibPtr->fib_FileName)) ;
					if (tabbing[0] < len) {
						tabbing[0] = len;
					}
				} else {
					Error(ERR_NO_MEM);
					continueScan = FALSE;
					abortFlag = TRUE;
				}
			}
		} else {
			dialogVarStr = &fibPtr->fib_FileName[0];
			if (prefs.WarnInvalidFilenames) {
				WarningDialog( ( fibPtr->fib_DirEntryType < 0) ? ERR_INVALID_FILENAME :
					ERR_INVALID_DIRNAME, FALSE);
			}	
		}
	}
	return (!abortFlag);
}

/*
 * From the link filename passed, return pointer to its true name.
 * NOTE: Because a user could see the file from a network and not
 * be running OS V2.0.4 or later, we'd better check the OS version
 * and not stuff destination with anything if links not supported.
 */
 
static void GetLinkName(TextPtr fileName, TextPtr *dest)
{
	register TextPtr str = &strBuff[0];
	BPTR lock;
	register struct DevProc *devProcPtr;
	
	*str = 0;
	if ((curDBPtr->df_Flags & (FLAG_SLINK_MASK | FLAG_HLINK_MASK)) && intuiVersion >= OSVERSION_2_0_4) {
		strcpy(str, strPath);
		AppendDirPath(str, fileName);
		if (curDBPtr->df_Flags & (FLAG_SLINK_MASK)) {
		
			devProcPtr = GetDeviceProc(str, NULL);
			if (IoErr() == ERROR_OBJECT_NOT_FOUND && devProcPtr->dvp_Flags & DVPF_ASSIGN) {
				while ((devProcPtr = GetDeviceProc(str, devProcPtr)) == NULL);
			}
			if (devProcPtr != NULL) {
				strSavePath[0] = 0;			/* This routine is called from backup */
				ReadLink( devProcPtr->dvp_Port, devProcPtr->dvp_Lock, 
					str, strSavePath, PATHSIZE);
				str = strSavePath;			/* We can trash strSavePath therefore */
				FreeDeviceProc(devProcPtr);
				curDBPtr->df_Flags &= ~FLAG_DIR_MASK;
			}
		} else {
			if (lock = Lock(str, ACCESS_READ)) {
				if (NameFromLock(lock, str, PATHSIZE) == 0) {
					*str = 0;
				} else {
					while (*str++ != ':');
				}
				UnLock(lock);
			} else {
				*str = 0;
			}
		}
	}
	if (*str) {
		if (*dest = NewPtr(strlen(str)+1/*+MAX_FILENAME_LEN*/)) {
			strcpy(*dest, str);
			/*
			str = *dest;
			strcpy(&str[strlen(str)+1], curDBPtr->df_Name);
			*/
		}
	} else {
		*dest = NULL;
	}
}
					
/*
 * Inserts new FIB into chain at proper place, depending upon name. Directories
 * sort ahead of files. Replaces current level's FIB chain's head if necessary.
 */
 
void SortInsert()
{
	DirFibPtr *dirFib;
	DirFibPtr newFib;
#ifdef PRESORT
	register BOOL done;
#endif
	register DirFibPtr nextFib;
	register struct FileInfoBlock *fibPtr = (struct FileInfoBlock *)fib;
	
	dirFib = &(curLevel->dl_ChildPtr);					/* Head of chain */
	newFib = curLevel->dl_CurFib;							/* Points to new dirFib */
#ifdef PRESORT
	if (newFib->df_Flags & FLAG_DIR_MASK) {			/* Dirs sort ahead of files */
		do {
			nextFib = *dirFib;								/* Get first/next dir entry */
			if (nextFib != NULL) {							/* End of chain? */
				if (nextFib->df_Flags & FLAG_DIR_MASK) {
					done = stricmp(fibPtr->fib_FileName, nextFib->df_Name) <= 0;
					if (!done) {
						dirFib = &nextFib->df_Next;
					}
				} else {
					done = TRUE;
				}
			}
		} while (nextFib != NULL && !done);
	} else {
		do {
			nextFib = *dirFib;								/* Get first/next file entry */
			if (nextFib != NULL) {							/* End of chain? */
				if ((nextFib->df_Flags & FLAG_DIR_MASK) == 0)
					done = stricmp(fibPtr->fib_FileName, nextFib->df_Name) <= 0;
				else
					done = FALSE;							/* This one a dir, skip to next */
				if (!done)
					dirFib = &nextFib->df_Next;
			}
		} while (nextFib != NULL && !done);
	}
#else
	nextFib = *dirFib;										/* In QB V5.0+, we sort later! */
#endif
	if (nextFib != NULL) {
		newFib->df_Next = nextFib;
	} 
	*dirFib = newFib;
	return;
}

static BOOL IsCompressionExcludedFilter(TextPtr);

/*
 * Determine whether the filename passed is one that should be excluded given
 * the current compression filter.
 * NOTE: There is currently no compression filter. It would be easy to implement.
 */
 
static BOOL IsCompressionExcludedFilter(TextPtr name)
{
	/* FOR EXAMPLE, TO EXCLUDE COMPRESSING ALL .LZH FILES, YOU WOULD CODE:
	
	strcpy(strBuff, "#?.lzh");
	
	if (MatchFilename(strBuff, name)) {
		return (TRUE);
	}
	*/
	return (FALSE);
}

/*
 * Sort all directories
 */
 
void SortAllDirs()
{
	DirLevelPtr oldCurLevel = curLevel;
	register UBYTE oldOp;
	register DirFibPtr oldLevelCurFib = curLevel->dl_CurFib;
	TextChar buff[PATHSIZE];		/* It's alot of stack space. Be careful. */
	
	SetBusyPointer();
/*
	Prohibit output of NextFile() to display window by setting operation
*/
	oldOp = operation;
	strcpy(buff, strPath);
	
	InitList();
	SortDir();

	linkCount = 0L;
	do {
		operation = OPER_SELECTION;
		if ((curFib = NextFile(TRUE, TRUE)) != NULL) {
			operation = oldOp;
			if (IS_DIR(curFib->df_Flags)) {
				NextBusyPointer();
				SortDir();
			}
			else if ((curFib->df_Flags & (FLAG_SLINK_MASK | FLAG_HLINK_MASK)) && intuiVersion >= OSVERSION_2_0_4) {
				linkCount++;
			} else if (mode == MODE_BACKUP) {
				if (IsCompressionExcludedFilter(curFib->df_Name)) {
					curFib->df_Flags &= ~FLAG_BITS_MASK;
				} else {
					curFib->df_Flags |= FLAG_BITS_MASK;
				}
			}
		}
	} while (curFib != NULL);
	strcpy(strPath, buff);
	curLevel = oldCurLevel;
	curLevel->dl_CurFib = oldLevelCurFib;
	operation = oldOp;
	SetArrowPointer();
}

/*
 * Sort directory blocks according to preferences sort criteria
 */

void SortDir()
{
	register DirFibPtr dirFib = curLevel->dl_ChildPtr;
	register DirFibPtr nextFib;
	register DirFibPtr prevNextFib;
	DirFibPtr prevDirFib = NULL;
	
	while (dirFib != NULL) {
		prevNextFib = dirFib;
		nextFib = dirFib->df_Next;
		while (nextFib != NULL) {
			if (CompareDir(dirFib, nextFib)) {
				if (prevDirFib != NULL) {
					prevDirFib->df_Next = nextFib;
				} else {
					curLevel->dl_ChildPtr = nextFib;
				}
				prevNextFib->df_Next = nextFib->df_Next;
				nextFib->df_Next = dirFib;
				dirFib = nextFib;
				nextFib = prevNextFib->df_Next;
			} else {
				prevNextFib = nextFib;
				nextFib = nextFib->df_Next;
			}
		}
		prevDirFib = dirFib;
		dirFib = dirFib->df_Next;
	}
}

/*
 * Compare two directory/file info blocks according to preferences sort criteria
 */
 
static BOOL CompareDir(register DirFibPtr dirFib, register DirFibPtr nextFib)
{
	register BOOL swap;
	register BOOL isDir1, isDir2;
	BOOL similar;
	WORD format;
	UBYTE separateDirs;
	BOOL reverse = FALSE;

#ifdef QB_NEW_CATALOG	
	if ((/*(prefs.CatOpts.SortFormat == 3) ||*/ (operation != OPER_SELECTION && operation != OPER_LIMBO && operation != OPER_SCAN)) &&
		 (mode == MODE_BACKUP || qbCode != QB_ID || useSortFormat)) {
		reverse = (mode == MODE_BACKUP) || qbCode == QB_ID;
		separateDirs = TRUE;
		format = SORT_FORMAT_NAME;
	} else {
		separateDirs = prefs.CatOpts.SeparateDirs;
		format = prefs.CatOpts.SortFormat;
	}
#else
	separateDirs = prefs.CatOpts.SeparateDirs;
	format = prefs.CatOpts.SortFormat;
#endif

	isDir1 = dirFib->df_Flags & FLAG_DIR_MASK;
	isDir2 = nextFib->df_Flags & FLAG_DIR_MASK;
	similar = isDir1 == isDir2;
	if (similar) {
		if (isDir2 && ( format == SORT_FORMAT_SIZE)) {
			format = SORT_FORMAT_NAME;
		}
	} else if (separateDirs || (format == SORT_FORMAT_SIZE)) {
		return (reverse ? isDir1 : isDir2);
	}
	switch (format) {
	case SORT_FORMAT_NAME:
		swap = stricmp(dirFib->df_Name, nextFib->df_Name) > 0;
		break;
	case SORT_FORMAT_SIZE:
		swap = dirFib->df_Size < nextFib->df_Size;
		break;
	case SORT_FORMAT_DATE:
		swap = CompareDateStamps(&dirFib->df_DateStamp, &nextFib->df_DateStamp);
		break;
	}
	return (swap);
}

/*
 * Compares two datestamps, returns whether swapping is necessary
 */
 
BOOL CompareDateStamps(register struct DateStamp *ds1, register struct DateStamp *ds2)
{
	register BOOL swap;
	
	if (intuiVersion >= OSVERSION_2_0) {
		swap = CompareDates(ds1, ds2) < 0;
	} else {
		if (ds1->ds_Days == ds2->ds_Days) {
			if (ds1->ds_Minute == ds2->ds_Minute) {
				swap = ds1->ds_Tick < ds2->ds_Tick;
			} else {
				swap = ds1->ds_Minute < ds2->ds_Minute;
			}
		} else {
			swap = ds1->ds_Days < ds2->ds_Days;
		}
	}
	return (swap);
}

/* 
 * Locks the directory in the path variable, and stores lock in level
 * entry passed.
 */
 
BOOL PathLock()
{
	return ( (curLevel->dl_DirLock = Lock(strPath, ACCESS_READ)) != NULL);
}

/*
 * Unlock possible lock and display disk corrupted dialog
 */
 
void DiskBad()
{
	Error(ERR_OPEN_DIR);
}

/*
 * Error building file directory structure
 */
 
void BuildAbort()
{
	UnLockLocks(NULL);
	FreeDirBlocks();
}

/*
 * Unlock the locks obtained during directory scan or during backup operation.
 * Returns whether level passed is not a parent to the current level.
 */
 
BOOL UnLockLocks(DirLevelPtr lastLevel)
{
	DirLevelPtr abortLevel = lastLevel ? &rootLevel : NULL;
	
	if (curLevel != NULL) {
		while ((curLevel != lastLevel) && (curLevel != abortLevel)) {
			if (curLevel->dl_DirLock != NULL) {
				UnLock(curLevel->dl_DirLock);
				curLevel->dl_DirLock = NULL;
			}
			curLevel = curLevel->dl_ParLevel;
		}
	}
	return (curLevel != lastLevel);
}

/*
 * Allocate memory for a directory block and setup forward link.
 */
 
DirFibPtr AllocateDirBlock(BOOL dir)
{
	register DirFibPtr newFib;
	ULONG size = dir ? sizeof(DirLevel) : DIR_BLOCK_SIZE;
	
	newFib = NewPtr(size);
	curDBBase->df_Next = newFib;
	if (newFib == NULL) {
		Error(ERR_NO_MEM);
	} else {
		BlockClear(newFib, size);
		curDBBase = newFib;
		curDBPtr = (DirFibPtr) (((UBYTE *) newFib) + sizeof(DirFibPtr));
	}
	return (newFib);
}

/*
 * Free all blocks allocated by AllocateDirBlock()
 */

void FreeDirBlocks()
{
	register DirFibPtr dirFib;
	register ULONG *currFib;
	register ULONG nextDirFib;			/* We don't have three address registers */
	
	totalFiles = 0;
	currFib = (ULONG *) baseDirFib;
	while (currFib != NULL) {
		nextDirFib = (ULONG) *currFib;
		if (currFib != (ULONG *) baseDirFib && (*(currFib-1) == DIR_BLOCK_SIZE)) {
			dirFib = (DirFibPtr) (currFib + 1);
			if (dirFib->df_Name != NULL) {
				MemFree( dirFib->df_Name, strlen(dirFib->df_Name)+1);
			}
			if (dirFib->df_Comment != NULL) {
				DisposePtr( dirFib->df_Comment);
			}
			if (dirFib->df_LinkName != NULL) {
				DisposePtr( dirFib->df_LinkName);
			}
		}
		DisposePtr((Ptr)currFib);
		currFib = (ULONG *) nextDirFib;
	}
	baseDirFib = NULL;
	BlockClear((Ptr)&rootLevel, sizeof(DirLevel));
}
			
/*
 * Stores catalog on disk, either on first disk or last disk.
 */
 
BOOL WriteCatalog()
{
	register DirFibPtr nextFib;
	register TextPtr str;
	register TextChar ch;
	register UBYTE flags;
	BOOL isDir;
	WORD saveSessionCurrCylinder = sessionCurrCylinder;
	WORD saveActiveCylinder = activeCylinder;

RestartCatalog:

	sessionCurrCylinder = 0;		/* In case writing the alternate catalog */
	readErr = 0;
	
	catFlag = CAT_WRITING;
	PutWord( CountEntries(&rootLevel));
	while (( (nextFib = GetFileDir()) != NULL) && !abortFlag && !readErr) {
		flags = nextFib->df_Flags;
		isDir = IS_DIR(flags);
		if ((isDir && prefs.OldPrefs.BAllDirs) || (flags & FLAG_SEL_MASK)) {
			PutLong(isDir ? NULL : nextFib->df_Size);
#ifdef QB_NEW_CATALOG
			PutLong(nextFib->df_ActualSize);
#endif
			PutWord((UWORD)nextFib->df_DateStamp.ds_Days);
			PutWord((UWORD)nextFib->df_DateStamp.ds_Minute);
			PutWord((UWORD)nextFib->df_DateStamp.ds_Tick);
			PutWord(nextFib->df_FilCnt);
			PutChar(nextFib->df_Prot);
			PutChar(flags);
			str = nextFib->df_Name;
			do {
				ch = *str++;
				PutChar(ch);
			} while(ch);
			if (readErr == 0) {
				if ((str = nextFib->df_Comment) != NULL) {
					do {
						ch = *str++;
						PutChar(ch);
					} while(ch);
				} else {
					PutChar(0);
				}
/*
	Assume no link name if flags do not have link flags set
*/
				if (readErr == 0) {
					if ((str = nextFib->df_LinkName) != NULL) {
						do {
							ch = *str++;
							PutChar(ch);
						} while(ch && (readErr == 0));
					}
				}
			}
		}
	}
	sessionCurrCylinder = saveSessionCurrCylinder + (activeCylinder - saveActiveCylinder);
	if (readErr) {
		if (restartCatalog) {
			restartCatalog = earlyWarning = resetDisk = FALSE;
			sessionCurrDisk = 1;
			firstTape = activeDisk;
			firstTapeSession = sessionNumber;
			InitList();
			InitFirstCylinder();
			goto RestartCatalog;
		}
	} else {
		stats.rawCompletedBytes = stats.saveRawComplBytes = catSize + sizeof(QBNewFirstCylHdr) - (((ULONG)fillPtr) - ((ULONG)fillBuffer));
	}
	catFlag = CAT_DONE;
	return (readErr == 0);
}

/*
 * Save the originating dirFib and call BuildFileList().
 */
 
void EnterDirectory(DirFibPtr dirFib)
{
	if (dirFib != NULL && (IS_DIR(dirFib->df_Flags))) {
		curLevel = (DirLevelPtr) dirFib->df_SubLevel;
		curLevel->dl_ParFib = dirFib;
		BuildFileList(NULL);
	}
}

/*
 * Read the directory tree from the restore source media.
 */

BOOL ReadDir(DevParamsPtr devParam)
{
	WORD val;
	register BOOL done = FALSE;
	register DirFibPtr dirFib;
	DirFibPtr parentDirFib;
	BOOL corruptFlag;
	register BOOL commentCorruptFlag;
	BOOL reqOpened = FALSE;
	
	catFlag = CAT_READING;

ReReadCatalog:
	parentDirFib = NULL;
	catCorruptAddition = readErr = 0;
	useRecalcCatSize = TRUE;
	corruptFlag = commentCorruptFlag = FALSE;

	curDBBase = (DirFibPtr) &baseDirFib;
	if (AllocateDirBlock(TRUE)) {
		fillCount = 0;
		val = GetWord();
		if (!readErr) {
			strPath[0] = 0;
			curLevel = (DirLevelPtr) &rootLevel;
			BlockClear(&rootLevel, sizeof(DirLevel));
			curLevel->dl_FilCnt = val;
			totalFiles = totalBytes = 0;
			if (!reqOpened) {
				reqOpened = (requester = DoGetRequest(REQ_SCAN)) != NULL;
				InitScanRequester(requester);
			}
			do {											/* ReadNextEntry */
				if (curLevel->dl_FilCnt == 0) {
					curLevel = curLevel->dl_ParLevel;
					if (curLevel != NULL) {
					/*if (UpdateDirStatus(FALSE)) {*/
						TruncatePath();
						parentDirFib = curLevel->dl_ParFib;
					} else {
						done = TRUE;
					}
				} else {
					if ((dirFib = ReadEntry()) && !abortFlag) {
						if (restartCatalog) {
							readCount = useCache = stats.rawCompletedBytes = sessionCurrDisk = 0;
							activeCylinder = maxCylinders;
							sessionCurrCylinder = -1;
							firstTape = activeDisk+1;
							StuffScanDirText(NULL);	/* Zap previous pathname display */
							FreeDirBlocks();
							goto ReReadCatalog;
						}
						if (!(corruptFlag || commentCorruptFlag)) {
							if (strlen(dirFib->df_Name) > MAX_FILENAME_LEN) {
								corruptFlag = TRUE;
							}
							if (catCorruptAddition != 0) {
								commentCorruptFlag = TRUE;
							}
							if (corruptFlag || commentCorruptFlag) {
								readErr |= abortFlag = WarningDialog(ERR_CORRUPT_CAT, TRUE);
							}
						}
						if (curLevel->dl_ChildPtr == NULL) {
							curLevel->dl_ChildPtr = dirFib;
						} else {
							curLevel->dl_CurFib->df_Next = dirFib;
						}
						curLevel->dl_CurFib = dirFib;
						curLevel->dl_FilCnt--;
						if (!abortFlag && dirFib->df_Flags & FLAG_DIR_MASK) {
							if ((dirFib->df_Flags & (FLAG_SLINK_MASK | FLAG_HLINK_MASK)) == 0) {
								if (readErr |= !AppendDirPath(strPath, dirFib->df_Name)) {
									Error(ERR_PATH_TOO_LONG);
								} else {
									if (!AllocateDirBlock(TRUE)) {
										readErr = -1;
									} else { 
										StuffScanDirText(strPath);
										StuffScanFileText(NULL);
										BlockClear(curDBPtr, sizeof(DirLevel));
										dirFib->df_SubLevel = (ULONG) curDBPtr;
										((DirLevelPtr)curDBPtr)->dl_ParLevel = curLevel;
										((DirLevelPtr)curDBPtr)->dl_FilCnt = dirFib->df_FilCnt;
										curLevel = ((DirLevelPtr)curDBPtr);
										if (parentDirFib != NULL) {
												curLevel->dl_ParFib = parentDirFib;
										}
										parentDirFib = dirFib;
									}
/*
	Strange but true- if reading the catalog, it is IMPOSSIBLE to have a partially
	selected folder, so do not call UpdateDirStatus(), just always set to FALSE.
*/
								}
								dirFib->df_Flags &= ~FLAG_PART_MASK;
							} else {
								dirFib->df_Flags |= FLAG_SEL_MASK;
								StuffScanFileText(dirFib->df_Name);
							}
						} else {
							totalBytes += dirFib->df_Size;
							totalFiles++;
							StuffScanFileText(dirFib->df_Name);
						}
					}
				}
				abortFlag |= CheckAbortButton();
			} while (!abortFlag && !readErr && !done);
			if (requester != NULL) {
				DestroyRequest(requester);
			}
		}
	}
	if (readErr || corruptFlag) {
		useRecalcCatSize = FALSE;
		done = !(readErr = abortFlag = WarningDialog(ERR_INCOMPL_CAT, TRUE));
	}
	MotorOff(FALSE);
	catFlag = CAT_DONE;
	return (done);
}
			
			
/*
 * Reads next entry from disk and builds FIB entry, which is returned.
 */
 

static DirFibPtr ReadEntry()
{
	BOOL success;
	register BOOL cont1;
	register UBYTE *destPtr;
	register WORD i;
	register DirFibPtr dirFib = NULL;
	register ULONG dateStampField;
	
	success = AllocateDirBlock(FALSE) != NULL;
	if (success) {
		dirFib = curDBPtr;
		destPtr = (UBYTE *) dirFib;
		*((ULONG *)destPtr) = NULL;						/* next */
		destPtr += sizeof(ULONG);
		*((ULONG *)destPtr) = GetLong();					/* size */
		if (useSortFormat) {
			dirFib->df_ActualSize = GetLong();			/* actual size, if applic */
		} else {
			dirFib->df_ActualSize = *((ULONG *)destPtr);
		}
		destPtr += sizeof(ULONG);
		if (!readErr) {
			dateStampField = GetWord();
			*((ULONG *)destPtr) = dateStampField;		/* ...datestamp days */
			destPtr += sizeof(ULONG);
			if (!readErr) {
				dateStampField = GetWord();
				*((ULONG *)destPtr) = dateStampField;	/* datestamp mins */
				destPtr += sizeof(ULONG);	
				if (!readErr) {
					dateStampField = GetWord();
					*((ULONG *)destPtr) = dateStampField;	/* datestamp ticks */
					destPtr += sizeof(ULONG);
					if (!readErr) {
						*((ULONG *)destPtr) = GetLong();	/* count, prot, flags */
						if (!readErr) {
							for (cont1 = TRUE, i = 0 ; (i < MAX_FILENAME_LEN) && cont1 ; i++) {
								strBuff[i] = GetChar();
								cont1 = (strBuff[i] != 0) && (readErr == 0);
							}
							strBuff[MAX_FILENAME_LEN] = 0;
							i = MIN(MAX_FILENAME_LEN, strlen(strBuff)) + 1;
							if (dirFib->df_Flags & FLAG_DIR_MASK) {
								dirFib->df_Size = 0L;	/* Just in case corrupted */
							}
							if (!readErr) {
								if ((dirFib->df_Name = MemAlloc(i, MEMF_CLEAR)) == NULL) {
									return (NULL);
								}
								strcpy(dirFib->df_Name, strBuff);
								strBuff[MAX_COMMENT_LEN] = 0;/* Just in case corrupt */
								for (cont1 = TRUE, i = 0 ; (i < MAX_COMMENT_LEN) && cont1 ; i++) {
									strBuff[i] = GetChar();
									cont1 = (strBuff[i] != 0) && (readErr == 0);
									if (i == (MAX_COMMENT_LEN-1)) {
										i--;
										catCorruptAddition++;
									}
								}
								if ((dirFib->df_Comment = strBuff[0] ? NewPtr(strlen(strBuff)+1) : NULL) != NULL)
									strcpy(dirFib->df_Comment, strBuff);
								strBuff[0] = 0;
								strBuff[PATHSIZE + MAX_FILENAME_LEN - 1] = 0;
								if (!readErr && (dirFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK))) {
									for (cont1 = TRUE , i = 0 ; (i < (PATHSIZE + MAX_FILENAME_LEN-1)) && cont1 ; i++) {
										strBuff[i] = GetChar();
										cont1 = (strBuff[i] != 0) && (readErr == 0);
									}
									if ((dirFib->df_LinkName = strBuff[0] ? NewPtr(strlen(strBuff)+MAX_FILENAME_LEN+2) : NULL) != NULL) {
										strcpy(dirFib->df_LinkName, strBuff);
										stccpy(&dirFib->df_LinkName[strlen(strBuff)+1], dirFib->df_Name, MAX_FILENAME_LEN+1);
									}
								}
								dirFib = (DirFibPtr) curDBPtr;
							}
						}
					}
				}
			}
		}
	}
	return (dirFib);
}

/*
 * Close file identified by FHandle.
 */
 
void CloseFile()
{
	switch (fHandle) {
	case NULL:
	case -1L:
		break;
	default:
		Close(fHandle);
		break;
	}
	fHandle = NULL;
}

/*
 * Select/Deselect all files/dirs in a directory, recursively done.
 */

void SelectDir(register DirFibPtr dirFib, register BOOL onOff)
{
	register DirLevelPtr dirLevel;
	register BOOL isDir;
	
	while (dirFib != NULL) {
		isDir = IS_DIR(dirFib->df_Flags);
		if (isDir) {
			dirLevel = (DirLevelPtr) dirFib->df_SubLevel;
			if (dirLevel != NULL) {
				SelectDir(dirLevel->dl_ChildPtr, onOff);
			}
		}
/*
	Update the bytes/files count only if a file and sel status is different
*/
		SetResetTag(onOff, dirFib, (isDir == 0) && (onOff != ((dirFib->df_Flags & FLAG_SEL_MASK) != 0)));
/*
	The following can be redundant, but it's not terribly wasteful.
*/		
		if (onOff) {
			dirFib->df_Flags &= ~FLAG_PART_MASK;
			dirFib->df_Flags |= FLAG_SEL_MASK;
		} else {
			dirFib->df_Flags &= ~(FLAG_PART_MASK | FLAG_SEL_MASK);
		}
		dirFib = dirFib->df_Next;
	}
}

/*
 * Set or reset the flag field of the dirFib passed if necessary.
 */
 
BOOL SetResetTag(BOOL include, DirFibPtr dirFib, BOOL flag)
{
	BOOL change = FALSE;
	register BOOL isLink = (dirFib->df_Flags & (FLAG_SLINK_MASK | FLAG_HLINK_MASK)) != 0;
	register ULONG size = dirFib->df_Size;
	
	if (flag) {
		if (include) {
			if ((dirFib->df_Flags & FLAG_SEL_MASK) == 0) {
				if (!isLink) {
					selFiles++;
					selBytes += size;
				}
				dirFib->df_Flags |= FLAG_SEL_MASK;
				change = TRUE;
			}
		} else {
			if (dirFib->df_Flags & FLAG_SEL_MASK) {
				if (!isLink) {
					selBytes -= size;
					selFiles--;
				}
				dirFib->df_Flags &= ~FLAG_SEL_MASK;
				change = TRUE;
			}
		}
	}
	return (change);
}

/*
 * Call when aborting the backup/restore in progress.
 */
 
void CheckAbort()
{
	dialogVarStr = GetModeName(mode, FALSE);
	
	MotorOff(FALSE);
	abortFlag |= (WarningDialog(ERR_ABORT_CHECK, TRUE) == OK_BUTTON);
}

/*
 * Add the bytes written and update the stats.
 */
 
void AddToCompletedBytes(register DevParamsPtr devParam, register WORD index)
{
	if (catFlag != CAT_WRITING && !inputCompleted) {
		stats.rawCompletedBytes += devParam->BytesWritten[index] + stats.paddedBytes[index];
		if (backupCompress & COMP_ON_FLAG) {
			stats.completedBytes += stats.compressBytesInput;
			stats.compressBytesInput = 0;
		} else {
			stats.completedBytes += devParam->BytesWritten[index];
		}
		/*
		if (completedBytes > selBytes) {
			completedBytes = selBytes;
		}*/
		UpdateStats();
	}
	devParam->BytesWritten[index] = 0L;
	stats.paddedBytes[index] = 0L;
}

/*
 * At setup time, or when AppIcon/Menu file to be opened, call this.
 */

BOOL DoOpenFile(TextPtr fileName, Dir dir)
{
	SetCurrentDir(dir);
	if (!ReadProgPrefs(fileName)) {
		if (mode == MODE_LIMBO) {
			OpenBackupFile(fileName, dir);
		}
	}
	return (TRUE);
}

/*
 * Open the backup file created by Quarterback
 */
 
BOOL OpenBackupFile(TextPtr fileName, Dir dir)
{
	QBNewFirstCylHdr qbHdr;
	File fHandle;
	BOOL success = FALSE;
	register ULONG numToRead = 4;
	register BOOL found;
	register WORD i;
	QBNewFirstCylHdr buff;
	
	fHandle = Open(fileName, MODE_OLDFILE);
	if (fHandle != NULL) {
		if (Read(fHandle, &qbHdr, numToRead) == numToRead) {
			if (qbHdr.id == QB_ID) {
				prefs.Source = DEST_FILE;
				BuildPath(fileName, destFileName, dir, PATHSIZE - 2);
				strcpy(strPath, destFileName);
				useAmigaDOS = TRUE;
				if (success = InitOp()) {
					trkBufSize = sizeof(QBNewFirstCylHdr);
					readBuffer = (UBYTE *) &buff;
					success = DoReadBuffer(NULL, 0) == 0;
					readBuffer = NULL;
					EndOp();
					if (success) {
						buff.volName[MAX_BACKUPNAME_LEN-1] = 0;
						strcpy(currVolName, buff.volName);
						for (i = 0, found = FALSE; !found && i < SLNumItems(scrollList) ; i++) {
							SLGetItem(scrollList, i, strBuff);
							if (found = strcmp(currVolName, &strBuff[2]) == 0) {
								SLSelectItem(scrollList, i, TRUE);
								success = LoadDirectory(MODE_RESTORE, FALSE);
							}
						}
					}
				}
				if (!success) {
					strPath[0] = 0;
				}
			}
		}
		Close(fHandle);
	}
	return (success);
}
