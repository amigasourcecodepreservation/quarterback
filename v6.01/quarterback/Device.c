/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 * Low-level and device-specific routines
 */

#include <exec/types.h>
#include <exec/io.h>
#include <exec/memory.h>
#include <exec/interrupts.h>
#include <intuition/intuition.h>
#include <dos/dosextens.h>
#include <dos/filehandler.h>		/* For fssm_ fields */
#include <resources/disk.h>
#include <devices/trackdisk.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/intuition.h>

#include <clib/alib_protos.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/List.h>
#include <Toolbox/Request.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Packet.h>
#include <Toolbox/BarGraph.h>

#include <dos.h>
#include <string.h>

#include "QB.h"
#include "Proto.h"
#include "Tape.h"

/*
 * External variables
 */
 
extern ProcessPtr	process;
extern DevParamsPtr	firstDevice, activeDevice;

extern TextChar	strDF0[], strTrackdisk[];
extern APTR			SaveVec;
extern struct Library *DiskBase;
extern struct DosLibrary *DOSBase;

extern UBYTE		mode, startMode;
extern TextChar	strBuff[], strDeviceName[], strColon[], strTapeDrive[], strRemovable[];
extern TextPtr		strPath, destFileName;
extern ProgPrefs	prefs;
extern struct DateStamp	sessionDateStamp;
extern DirFibPtr	curFib;
extern UWORD		blockSize, surfaces, blocksPerTrack;
extern LONG			sessionCurrCylinder, activeCylinder;
extern LONG			lowCylinder, highCylinder, maxCylinders;
extern UWORD		reservedBlocks, numDevices;
extern ULONG		volumeOffset, volumeSize, trkBufSize, userTrkBufSize, bufMemType;
extern ULONG		selFiles;
extern UBYTE		floppyDrives[], floppyEnable[], floppy525[], floppyBusy[];
extern BOOL			useAmigaDOS, sequentialDevice, tapeDevice;
extern TextPtr		driveStrings[];

extern WindowPtr	cmdWindow;

extern WORD			intuiVersion;

extern UBYTE		operation;
extern WORD			numBufs;
extern TextPtr		dialogVarStr;
extern LONG			dialogVarNum;
extern TextChar	strA2090Dev1[], strA2090Dev2[], strSetArch[];
extern ReqTemplPtr	reqList[];
extern RequestPtr	requester;
extern ULONG		bugMask;
extern UWORD		sessionNumber;
extern ListHeadPtr	devList, devProcVolList;
extern MsgPortPtr	*devProcIDArray;
extern BOOL			abortFlag, outputCompleted;
extern UBYTE		verifyMode;
extern DirLevel	rootLevel;
extern MsgPortPtr	mainMsgPort;
extern ULONG		numSCSICyls;
extern WORD			statusIOBIndex;
extern BOOL			notSCSI;

static struct StandardPacket packet = {
	{ { 0L, 0L, 5, 0, 0 }, 0L, sizeof(struct StandardPacket) },
	{ 000L, 000L, 000L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L } 
};

static ULONG		busyLong = BUSY_ID;

#define SIZEOFJMPINST	2		/* On 680x0 machines, vector starts after two bytes */

/*
 * Local prototypes
 */

static BOOL			IsDiskInDrive(DevParamsPtr);
static void			CalcVolSize(void);
static DevParamsPtr	AllocTrackdisk(WORD);
static struct DevProc *NHGetDeviceProc(TextPtr, MsgPortPtr *, ULONG *);
static void			NHFreeDeviceProc(struct DevProc *);
static void 		ReadEnvPtr(struct DosEnvec *);
static struct DosEnvec *FindDosEnvFromExecDev(DevParamsPtr);

/*
 * Initialize floppy parameters - assumes disk.resource has been opened.
 */

BOOL FloppyInit(void)
{
	WORD	i;
	LONG	driveType;
	BOOL	result;

	Forbid();
	for (i = 0; i < 4; i++) {
		driveType = GetUnitID(i);
		floppyEnable[i] = (prefs.FloppyEnableMask & (1 << i)) != 0;
		floppyDrives[i] = (driveType != DRT_EMPTY);
		floppy525[i] = (driveType == DRT_37422D2S);
	}
	result = PatchGiveUnit();
	Permit();
	return (result);
}

/*
 * Patch the GiveUnit of the disk resource
 */

BOOL PatchGiveUnit()
{
	APTR	*funcPtr;

	if (intuiVersion < OSVERSION_2_0) {
		if (DiskBase) {
			funcPtr = (APTR *) ((char *) DiskBase + DR_GIVEUNIT + SIZEOFJMPINST);
			SaveVec = *funcPtr;
/*			intena = 0x4000;		Unknown reason */
			*funcPtr = MyGiveUnit;
/*			intena = -0x4000;		Unknown reason */
		}
	}

	return (DiskBase != NULL);
}

/*
 * Unpatch above routine
 */

void UnPatchGiveUnit()
{
	APTR	*funcPtr;

	if (intuiVersion < OSVERSION_2_0) {
		if (SaveVec) {
			funcPtr = (APTR *) ((UBYTE *) DiskBase + DR_GIVEUNIT + SIZEOFJMPINST);
			*funcPtr = SaveVec;
		}
	}
}

/*
 * Build a list of our own with just the exec devices
 */
 
ListHeadPtr BuildExecDeviceList()
{
	register struct DevInfo *dList;
	struct FileSysStartupMsg *startMsg;
	APTR old_window;
	ListHeadPtr list;
	BOOL exists;
	register WORD i;
	TextPtr name;

	list = CreateList();
	if (list) {
		old_window = process->pr_WindowPtr;
		process->pr_WindowPtr = (APTR)-1L;
		for (dList = NHLockDosList(DLT_DEVICE); dList; dList = NHNextDosEntry(dList, DLT_DEVICE)) {
			if (dList->dvi_Startup > 255) {
				name = (TextPtr) BADDR(dList->dvi_Name);
				if ((((intuiVersion < OSVERSION_2_0) || IsFileSystem(name)) )) {
					startMsg = (struct FileSysStartupMsg *)BADDR(dList->dvi_Startup);
				
					exists = FALSE;
					ConvertBSTR((BSTR) BADDR(startMsg->fssm_Device), strBuff);
					for (i = 0 ; i < NumListItems(list) ; i++) {
						if (strcmp( strBuff, GetListItem(list, i) ) == 0) {
							exists = TRUE;
						}
					}
					if (!exists) {
						if (AddListItem(list, strBuff, strlen(strBuff), FALSE) == -1) {
							break;		/* Stop right there if error! */
						}	
					}
				}
			}
		}	
		NHUnlockDosList(DLT_DEVICE);
		process->pr_WindowPtr = old_window;
	}
	return (list);
}

/*
 * Update device list
 */

void UpdateDeviceList()
{
	ListHeadPtr newList;

	newList = BuildExecDeviceList();
	if (newList) {
		if (devList != NULL)
			DisposeList(devList);
		devList = newList;
	}
}

/*
 * Return the DOS environment of something with the exec device name and unit passed.
 */
 
static struct DosEnvec *FindDosEnvFromExecDev(DevParamsPtr devParam)
{
	struct DosEnvec *envPtr = NULL;
	register struct DevInfo *dList;
	TextPtr name;
	WORD len = strlen(devParam->DeviceName);
	struct FileSysStartupMsg *startMsg;
	register WORD nameLen;
	
	for (dList = NHLockDosList(DLT_DEVICE); dList; dList = NHNextDosEntry(dList, DLT_DEVICE)) {	
		if (dList->dvi_Startup > 255) {
			startMsg = (struct FileSysStartupMsg *)BADDR(dList->dvi_Startup);
			name = (TextPtr) BADDR(startMsg->fssm_Device);
			nameLen = (WORD) *name;
			if (name[nameLen] == 0) {
				nameLen--;
			}
			if ((CmpString(devParam->DeviceName, name + 1, len, nameLen, FALSE) == 0) &&
				devParam->Unit == startMsg->fssm_Unit) {
				envPtr = (struct DosEnvec *) BADDR(startMsg->fssm_Environ);
				break;
			}
		}
	}
	NHUnlockDosList(DLT_DEVICE);
	return (envPtr);
}

/*
 * Checks ADOS device list for device in name passed. Then if first device, loads
 * params from environment block. If not first, compares params to make sure
 * devices are same capacity.
 */

BOOL CheckExecDeviceList(register DevParamsPtr devParam)
{
	struct InfoData infoData;			/* Must be longword aligned */
	WORD len;
	TextPtr name;
	register struct DevInfo *dList;
	struct FileSysStartupMsg *startMsg;
	register struct DosEnvec *envPtr;
	BOOL success = FALSE;
	APTR old_window;
	register BOOL found;
	WORD errID = ERR_NO_FLOPPY_DRIVE;
	
	if (!devParam->Floppy && !notSCSI) {
		return (TRUE);
	} else if (!devParam->Floppy) {
		if (envPtr = FindDosEnvFromExecDev(devParam)) {
			ReadEnvPtr(envPtr);
		} else {
			dialogVarStr = GetModeName(mode, FALSE);
			Error(ERR_INVALID_DEV);
		}
		return (envPtr != NULL);
	}
	len = strlen(devParam->DevName) - 1;
	
	old_window = process->pr_WindowPtr;
	process->pr_WindowPtr = (APTR)-1L;

	packet.sp_Pkt.dp_Arg1 = (ULONG) MKBADDR(&infoData);
	if (floppyBusy[devParam->Number] = (PacketIO(DeviceProc(devParam->DevName), ACTION_DISK_INFO) == 0) &&
			(CmpString((UBYTE *) &infoData.id_DiskType, (UBYTE *) &busyLong, sizeof(ULONG), sizeof(ULONG), TRUE) == 0)) {
		errID = ERR_DRIVE_BUSY;
	} else {
		for (found = FALSE, dList = NHLockDosList(DLT_DEVICE); dList && !found;
			dList = NHNextDosEntry(dList, DLT_DEVICE)) {	

			if (dList->dvi_Startup > 255) {
				if (((intuiVersion < OSVERSION_2_0) || IsFileSystem(devParam->DevName))) {
					startMsg = (struct FileSysStartupMsg *)BADDR(dList->dvi_Startup);
					
					name = (TextPtr) BADDR(dList->dvi_Name);
					if (CmpString(devParam->DevName, name + 1, len, (WORD) *name, FALSE) == 0) {
						found = TRUE;
						devParam->Unit = startMsg->fssm_Unit;
						ConvertBSTR( (BSTR) BADDR(startMsg->fssm_Device), devParam->DeviceName);
						devParam->Flags = startMsg->fssm_Flags;
						envPtr = (struct DosEnvec *) BADDR(startMsg->fssm_Environ);
						if (numDevices) {
							if (( ( envPtr->de_SizeBlock << 2) != blockSize ) ||
								( envPtr->de_Surfaces != surfaces ) ||
								( envPtr->de_BlocksPerTrack != blocksPerTrack ) ||
								( envPtr->de_Reserved != reservedBlocks ) ||
								( envPtr->de_LowCyl != lowCylinder ) ||
								( envPtr->de_HighCyl != highCylinder )) {
								errID = ERR_DRIVES_MIXED;
								abortFlag = TRUE;
							} else {
								success = TRUE;
							}
						} else {
							ReadEnvPtr(envPtr);
							success = TRUE;
						}
					}
				}
			}
		}
		NHUnlockDosList(DLT_DEVICE);
	}
	process->pr_WindowPtr = old_window;
	if (!success) {
		dialogVarStr = devParam->DevName;
		Error(errID);
	}
	return (success);
}

/*
 * Stuff globals from DOS environment
 */
 
static void ReadEnvPtr(register struct DosEnvec *envPtr)
{
	blockSize = envPtr->de_SizeBlock << 2;
	surfaces = envPtr->de_Surfaces;
	blocksPerTrack = envPtr->de_BlocksPerTrack;
	reservedBlocks = envPtr->de_Reserved;
	lowCylinder = envPtr->de_LowCyl;
	highCylinder = envPtr->de_HighCyl;
	bufMemType = envPtr->de_BufMemType;
}

/*
 * Converts and copies BSTR passed to a C string as destination
 */

void ConvertBSTR(BSTR bstr, TextPtr cstr)
{
	register WORD len;
	register TextPtr srcStr = (TextPtr) bstr;
	
	len = (WORD) *srcStr++;
	BlockMove(srcStr, cstr, len);
	cstr[len] = 0;
}
	
/*
 * Initialize disk interrupt vector
 * Creates IOB and sends to Trackdisk.device
 * Result zero if no error
 */

BOOL InitDiskInt(register DevParamsPtr devParam)
{
	BOOL success = TRUE;
	register IOTDPtr ioExtReq;
	
	if (devParam->Floppy) {
		BlockClear( (Ptr) &devParam->IntServer, sizeof(struct Interrupt));
		devParam->IntServer.is_Node.ln_Type = NT_INTERRUPT;
		devParam->IntServer.is_Data = devParam;
		devParam->IntServer.is_Code = (void (*)())DiskChangeInterrupt;
		if ((devParam->IntIOB = (IOTDPtr) CreateExtIO(devParam->MsgPort[0], sizeof(IOTD))) != NULL) {
			ioExtReq = devParam->IntIOB;
			ioExtReq->ioStdReq.io_Device = devParam->IOB[0]->ioStdReq.io_Device;
			ioExtReq->ioStdReq.io_Unit = devParam->IOB[0]->ioStdReq.io_Unit;
			ioExtReq->ioStdReq.io_Data = &devParam->IntServer;
			ioExtReq->ioStdReq.io_Command = TD_ADDCHANGEINT;
			SendIO((struct IORequest *)ioExtReq);
		} else {
			success = FALSE;
		}
	}
	return (success);
}

/*
 * Restore disk interrupt vector
 */
 
void RestoreDiskInt(DevParamsPtr devParam)
{
	register IOTDPtr ioExtReq;
	
	if (devParam->Floppy) {
		if ((ioExtReq = devParam->IntIOB) != NULL) {
			Disable();
			Remove((struct Node *)ioExtReq);
			Enable();
			DeleteExtIO((struct IORequest *)ioExtReq);
			devParam->IntIOB = NULL;
		}
	}
}

/*
 * Inhibit or enable the AmigaDOS device passed. A value of zero in "onoff"
 * keeps AmigaDOS from fooling around w/ the backup/restore device while we use it.
 */

BOOL OnOffDevice(DevParamsPtr devParam, BOOL onOff)
{
	register struct DevInfo *dList;
	struct DevInfo *dListStart;
	register WORD pass;
	struct FileSysStartupMsg *startMsg;
	BOOL result;
	struct DevProc *devProcPtr = NULL;
	MsgPortPtr procID;
	ULONG assignLock;
	TextChar partitionName[MAX_FILENAME_LEN+2];
	WORD item;
	WORD numVols;
	
/*
	For some reason, on the boot disk I think, multiple partitions do not get
	un-Inhibited because DeviceProc() and GetDeviceProc() want the volume to
	exist first. Therefore, they must be saved.
*/
	if (!onOff) {
		DisposeListItems(devProcVolList);
		if (devProcIDArray) {
			DisposePtr(devProcIDArray);
			devProcIDArray = NULL;
		}
	}
	dListStart = NHLockDosList(DLT_DEVICE);
	numVols = 0;
	for (pass = onOff; pass < 2; pass++) {
		for (dList = dListStart; dList; dList = NHNextDosEntry(dList, DLT_DEVICE)) {
			
			if (dList->dvi_Startup > 255) {
				startMsg = (struct FileSysStartupMsg *)BADDR(dList->dvi_Startup);
				strcpy(partitionName, ((TextPtr) BADDR(dList->dvi_Name)) + 1);
				strcat(partitionName, strColon);
				if ((strcmp( ((TextPtr)BADDR(startMsg->fssm_Device))+1, devParam->DeviceName) == 0) &&
				  (startMsg->fssm_Unit == devParam->Unit)) {
					if (pass == 0) {
						numVols++;
					} else {
						if (onOff) {
							item = FindListItem(devProcVolList, partitionName);
							if (item != -1) {
								procID = devProcIDArray[item];
							} else {
								devProcPtr = NHGetDeviceProc(partitionName, &procID, &assignLock);
							}
						} else {
							devProcPtr = NHGetDeviceProc(partitionName, &procID, &assignLock);
							if (InsertListItem(devProcVolList, partitionName, strlen(partitionName), 9999)) {
								devProcIDArray[NumListItems(devProcVolList)-1] = procID;
							}
						}
						if (procID) {
							packet.sp_Pkt.dp_Arg1 = (LONG) !onOff;
							PacketIO(procID, ACTION_INHIBIT);
							result = packet.sp_Pkt.dp_Res1 == DOSTRUE;
						}
						NHFreeDeviceProc(devProcPtr);
					}
				}
			}
		}
		if (pass == 0) {
			devProcIDArray = NewPtr(numVols * sizeof(MsgPortPtr));
		}
	}
	if (onOff) {
		DisposeListItems(devProcVolList);
		if (devProcIDArray) {
			DisposePtr(devProcIDArray);
			devProcIDArray = NULL;
		}
	}
	NHUnlockDosList(DLT_DEVICE);
	return (result);
}

/*
 * Send packet w/ specified action code
 */

LONG PacketIO( MsgPortPtr processID, LONG type)
{
	/* struct Process *process = (struct Process *) FindTask(NULL); */
	MsgPortPtr port = &process->pr_MsgPort;
	
	packet.sp_Msg.mn_Node.ln_Name = (char *) &packet.sp_Pkt.dp_Link; 
/*
	Apparently packet.sp_Msg.mn_ReplyPort needs no setup
*/
	packet.sp_Pkt.dp_Link = &packet.sp_Msg;
	packet.sp_Pkt.dp_Port = port;
	packet.sp_Pkt.dp_Type = type;
	PutMsg( processID, &packet.sp_Msg);
	WaitPort(port);
	Disable();
	Remove(&packet.sp_Msg.mn_Node);
	Enable();
	if (packet.sp_Pkt.dp_Res1 == DOSFALSE) {
		return (packet.sp_Pkt.dp_Res2);
	} else {
		return (0);
	}
}

/*
 * Calculate QB device max parameters from data from mountlist
 */

static void CalcVolSize()
{
	if (useAmigaDOS || tapeDevice) {
		volumeOffset = 0;
		volumeSize = 0;
		maxCylinders = 0x7FFFFFFFL;		// Choose a huge signed number
	}
	else {
		volumeOffset = lowCylinder * blocksPerTrack * surfaces * blockSize;
		maxCylinders = ((highCylinder - lowCylinder) + 1) * surfaces;
		volumeSize = (blocksPerTrack * maxCylinders) * blockSize;
	}
	if (firstDevice->Floppy)
		trkBufSize = blocksPerTrack * blockSize;
	else {
		trkBufSize = (mode == MODE_BACKUP) ?
				prefs.BuffOpts.BackupBufferSize : prefs.BuffOpts.RestoreBufferSize;
		if (trkBufSize == 0)
			trkBufSize++;
		trkBufSize <<= 10;				// Convert from KB to Bytes
		userTrkBufSize = trkBufSize;
	}
	numBufs = prefs.OldPrefs.SlowBackup ? 1 : 2;
}

/*
 * Opens all backup/restore devices, according to selection flags.
 */
 
BOOL OpenAllDevices()
{
	register BOOL success;
	register DevParamsPtr devParam;
	WORD i = 0;
	
	if (success = useAmigaDOS)
		InitPosition();
	else {
		for (devParam = firstDevice; devParam; devParam = devParam->Next) {
			if (devParam->IOB[0] == NULL) {
				bugMask = 0;
				success = InitDevice(devParam);
				if (success) {
					statusIOBIndex = 0;
					OnOffDevice(devParam, FALSE);
					devParam->Msg = ++i;
					if (!tapeDevice) {
						InitPosition();
						if (devParam->Floppy) {
							success = InitDiskInt(devParam);
						}
					}
					else {
						if ((strcmp(devParam->DeviceName, strA2090Dev1) == 0 ) ||
							(strcmp(devParam->DeviceName, strA2090Dev2) == 0)) {
							bugMask = 1 << 24;
							dialogVarStr = devParam->DevName;
							abortFlag = DiskSenseDialog(ERR_INSERT_A_TAPE, TRUE);
						}
						if (!abortFlag) {
							TapeIO(devParam, CMD_RESERVE_UNIT, NULL);
							if (prefs.TapeOpts.AutoRewind || (sessionNumber == 0)) {
								InitPosition();
								success = RewindTape(devParam, TRUE);
							}
						}
					}	
				}
			}
		}
		if (!success) {
			CloseAllDevices();
		}
	}
	statusIOBIndex = numBufs;
	return (success);
}

/*
 * Allocates a msg port and an IO block, then opens the device driver.
 * Drive param table in A2.
 */

BOOL InitDevice(register DevParamsPtr devParam)
{
	register BOOL success;
	register WORD i;

	devParam->Opened = FALSE;
	success = TRUE;
	for (i = 0 ; (i <= numBufs) && success ; i++) {
		if (i == numBufs) {
			statusIOBIndex = i;
		}
		if ((devParam->MsgPort[i] = CreatePort(0, 0)) != NULL) {
/*
 * This routine will corrupt memory if the IO Request block contains more than
 * 64 bytes of extra fields past the standard I/O request block.
 * One can find out if a certain device is corrupting memory by running
 * MungWall and typing in this device in the "DEV" on BackupOptions().
 */
			devParam->IOB[i] = (IOTDPtr) CreateExtIO(devParam->MsgPort[i], 48 + sizeof(IOTD));
		}
		success = (devParam->IOB[i] != NULL);
	}
	if (success) {
		if (success = devParam->Opened = OpenDevice(devParam->DeviceName, devParam->Unit, (struct IOStdReq *) devParam->IOB[0], devParam->Flags) == 0) {
			devParam->CurrIOB = 0;
		}
	}
	if (!success) {
		FreeDriveResources(devParam);
	} else {
		for (i = 1 ; i <= numBufs ; i++) {
			BlockMove(devParam->IOB[0], devParam->IOB[i], sizeof(IOTD));
			((struct IOStdReq *)devParam->IOB[i])->io_Message.mn_ReplyPort = devParam->MsgPort[i];
		}
	}
	return (success);
}

/*
 * Allocate a trackdisk unit.
 */
 
static DevParamsPtr AllocTrackdisk(WORD unit)
{
	DevParamsPtr ptr;
	
	ptr = (DevParamsPtr) NewPtr(sizeof(DevParams));
	if (ptr) {
		BlockClear(ptr, sizeof(DevParams));
		strcpy( ptr->DevName, driveStrings[unit]);
		ptr->Floppy = TRUE;
		ptr->Number = unit;
	}
	return (ptr);
}

/*
 * Allocate device parameter list entries, "firstDevice" points to head of list.
 * Return FALSE if not successful.
 */

BOOL AllocDeviceList()
{
	UBYTE ioType;
	register WORD i;
	register TextPtr ptr;
	register DevParamsPtr devParam = NULL;
	register BOOL success = TRUE;
	UBYTE data[INQ_LEN];
	BOOL retry;
	register TextPtr dev;
	register WORD unit;
	ULONG buffSize;
	WORD num, tries;

	numDevices = 0;
	if (firstDevice)
		FreeDeviceList();							/* Just in case an old list is around. */
	InitDriveType((BYTE)-1);
	ioType = (mode == MODE_BACKUP) ? prefs.Destination : prefs.Source;
	if (ioType == DEST_FLOPPY) {
		num = 1;
		for (i = 0 ; (i < 4) && !abortFlag ; i++) {
			if (floppyDrives[i] & floppyEnable[i]) {
				if ((ptr = (TextPtr) AllocTrackdisk(i)) != NULL) {
					if (success = CheckExecDeviceList( (DevParamsPtr) ptr)) {
						if (devParam)
							devParam->Next = (DevParamsPtr)ptr;
						devParam = (DevParamsPtr) ptr;
						numDevices++;
						if (firstDevice == NULL)
							firstDevice = devParam;
						devParam->Number = num++;
					}
					else
						DisposePtr(ptr);
				}
				else {
					Error(ERR_NO_MEM);
					success = FALSE;
				}
			}
		}
		if (numDevices == 0) {
			Error(ERR_NO_DRIVES);
			success = FALSE;
		}
	} else {
		activeDevice = firstDevice = devParam = NewPtr(sizeof(DevParams));
		if (devParam) {
			BlockClear(devParam, sizeof(DevParams));
			devParam->Number = 1;
			if (ioType == DEST_TAPE) {
				if (mode == MODE_BACKUP) {
					dev = &prefs.CurrBackupDev[0];
					unit = prefs.CurrBackupUnit;
					buffSize = prefs.BuffOpts.BackupBufferSize << 10;
				} else {
					dev = &prefs.CurrRestoreDev[0];
					unit = prefs.CurrRestoreUnit;
					buffSize = prefs.BuffOpts.RestoreBufferSize << 10;
				}
				devParam->Unit = unit;
				if (CheckSCSIDrive(dev, unit, &data[0], FALSE)) {
					if (notSCSI && (strcmp(strTrackdisk, devParam->DeviceName) == 0)) {
						strcpy(devParam->DevName, driveStrings[0]);
						devParam->DevName[2] += devParam->Unit;
					} else {
						ptr = strRemovable;
						if (!notSCSI) {
							InitDriveType(data[0]);
							if (tapeDevice) {
								ptr = strTapeDrive;
							}
						}
						strcpy(devParam->DeviceName, dev);
						strcpy(devParam->DevName, ptr);
					}
					if (notSCSI) {
						tapeDevice = sequentialDevice = FALSE;
					} else if (!sequentialDevice) {
						tries = 2;		/* Try again to reset UAT if necessary */
						while (tries--) {
							if (success = InitDevice(devParam)) {
								do {
									retry = FALSE;
									dialogVarStr = devParam->DevName;
									TapeIO(devParam, CMD_MODE_SENSE, (ULONG)data);	/* Reset UAT */
									if (!(success = TapeIO(devParam, CMD_READ_CAPACITY, (ULONG) &data[0]) == 0)) {
										success = DiskSenseDialog(tapeDevice ? ERR_INSERT_A_TAPE : ERR_INSERT_DISK, TRUE) == OK_BUTTON;
										if (!success) {
											tries = 0;
										}
										retry = success;
									}
								} while (retry);
								if (success) {
									surfaces = 1;
									lowCylinder = 0;
									numSCSICyls = *((ULONG *) &data[0]);
									blockSize = *((ULONG *) &data[4]);
									if (success = (blockSize != 0)) {
										blocksPerTrack = buffSize / blockSize;
										highCylinder = ((numSCSICyls * blockSize) / buffSize) - 1;
										tries = 0;
									}
								}
								FreeDriveResources(devParam);
							}
						}
					}
				}
				else {
					dialogVarNum = unit;
					dialogVarStr = dev;
					Error(ERR_BAD_DEVICE);
					success = FALSE;
				}
				if (success && !tapeDevice)
					success = CheckExecDeviceList(devParam);
			}
			else {
/*
	Copy filename portion if dest/src is AmigaDOS file
*/
				ptr = destFileName + strlen(destFileName);
				while ((--ptr > destFileName) && (*ptr != '/') && (*ptr != ':'));
				strcpy(devParam->DevName, ptr+1);
			}
			if (success)
				++numDevices;
		}
		else {
			Error(ERR_NO_MEM);
			success = FALSE;
		}
	}
	if (!success)
		FreeDeviceList();
	else
		CalcVolSize();
	activeDevice = firstDevice;
	return (success);
}

/*
 * Free allocated list entries of AllocDeviceParameters()
 */
 
void FreeDeviceList()
{
	register DevParamsPtr devParam = firstDevice;
	DevParamsPtr nextPtr;
	
	while (devParam) {
		nextPtr = devParam->Next;
		DisposePtr(devParam);
		devParam = nextPtr;
	}
	firstDevice = NULL;
}	

/*
 * Selects next (first) drive for transfer, based on current flags.
 */
 
DevParamsPtr SelectNextDrive(DevParamsPtr devParam)
{
	if (devParam)
		devParam = devParam->Next;
	if (devParam == NULL) {
		devParam = firstDevice;
		if ((activeCylinder != -1) && (!tapeDevice)) {
			sessionCurrCylinder = 0;
		}
	}
	if (activeCylinder != -1) {
		activeCylinder = 0;
	}
	DiskChangeBeep();
	return (devParam);
}

/*
 * Turns specified drive motor off
 */
 
BOOL MotorOff(BOOL eject)
{
	register struct IOStdReq *ioReq;
	/*register WORD error;*/
	BOOL onOff;
	
	ioReq = (struct IOStdReq *) activeDevice->IOB[statusIOBIndex];
	if (!useAmigaDOS && !tapeDevice) {
		if (IsDiskInDrive(activeDevice)) {
			ioReq->io_Command = CMD_UPDATE;
			ioReq->io_Length = 0L;
			/*error =*/ DoIO(ioReq);
			ioReq->io_Command = CMD_CLEAR;
			ioReq->io_Length = 0L;
			/*error =*/ DoIO(ioReq);
			ioReq->io_Command = TD_MOTOR;		/* Turn off the motor */
			ioReq->io_Length = 0L;
			/*error =*/ DoIO(ioReq);
			onOff = ioReq->io_Actual;
			if (eject) {
				ioReq->io_Command = TD_EJECT;
				ioReq->io_Length = 0L;
				/*error =*/ DoIO(ioReq);
			}
		}
	}
	return (onOff);
}
		
/*
 * Closes all backup/restore devices, according to selection flags.
 */
 
void CloseAllDevices()
{
	register DevParamsPtr devParam, nextDev;
	
	if (!useAmigaDOS) {
		SetStdPointer(cmdWindow, POINTER_WAIT);
		for (devParam = firstDevice; devParam; devParam = nextDev) {
			CloseDev(devParam);
			OnOffDevice(devParam, TRUE);
			nextDev = devParam->Next;
		}
		SetArrowPointer();
	}
}

/* 
 * Close and release a single floppy drive (passed to function).
 * Note: drive may never have been allocated.
 */
 
void CloseDev(register DevParamsPtr devParam)
{
	if (devParam->Floppy) {
		if (devParam->IOB[0])
			RestoreDiskInt(devParam);
	}
	else if (tapeDevice)
		TapeIO(devParam, CMD_RELEASE_UNIT, NULL);
	FreeDriveResources(devParam);
}

/*
 * Free drive resources
 */
 
void FreeDriveResources(register DevParamsPtr devParam)
{
	register WORD i;

	if (devParam->Opened) {
		CloseDevice((struct IORequest *)devParam->IOB[0]);
		devParam->Opened = FALSE;
	}	
	for (i = 0; i <= numBufs; i++) {
		if (devParam->IOB[i]) {
			DeleteExtIO((struct IORequest *)devParam->IOB[i]);
			devParam->IOB[i] = NULL;
		}
		if (devParam->MsgPort[i]) {
			DeletePort(devParam->MsgPort[i]);
			devParam->MsgPort[i] = NULL;
		}
	}
}

/*
 * Turn off active drive when trouble is found
 */
 
void ShutDevDown(DevParamsPtr devParam)
{
	DevParamsPtr saveDevParam = activeDevice;
	
	activeDevice = devParam;
	(void) MotorOff(TRUE);
	if ((devParam->Status == STATUS_READY) || (devParam->Status == STATUS_ACTIVE) || (devParam->Status == STATUS_CHECK)) {
		StatusRemove(devParam);
	}
	activeDevice = saveDevParam;
}

/*
 * Software interrupt on disk removed or inserted. Ptr to params in A1.
 */
 
void __asm DiskChangeInterrupt(register __a1 DevParamsPtr devParam)
{
	// DevParamsPtr devParam = (DevParamsPtr) getreg(9);
	
	devParam->Changed = TRUE;
	/*Signal(&process->pr_Task, sigBits);*/
}

/*
 * Is a disk in the disk drive? (5.25 and NON-SCSI drives always return TRUE)
 */
 
static BOOL IsDiskInDrive(DevParamsPtr devParam)
{
	struct IOStdReq *ioReq = (struct IOStdReq *) devParam->IOB[statusIOBIndex];
	BOOL success = TRUE;
	
	if (!((devParam->Unit < 4) && floppy525[devParam->Unit])) {
		ioReq->io_Command = TD_CHANGESTATE;
		ioReq->io_Length = 0L;
		if (success = DoIO(ioReq) == 0) {
			success = ioReq->io_Actual == 0;
		}
	}
	return (success);
}

/*
 * If any disk has changed, display appropriate info
 */

BOOL DiskChanged()
{
	register DevParamsPtr devParam;
	register BOOL changed = FALSE;
	
	if (!useAmigaDOS) {
		for (devParam = firstDevice; devParam; devParam = devParam->Next) {
			if (!notSCSI || (devParam->Floppy && ((devParam->Unit >= 4) || !floppy525[devParam->Unit])))
				changed |= DiskExamineForChange(devParam);
			else {
				if (devParam->Status == STATUS_REMOVE && requester)
					StatusNotReady(devParam);
				else if (devParam->Status == STATUS_NOT_READY && requester == NULL)
					StatusCheck(devParam);
			}
		}
	}
	return (changed);
}

/* 
 * Examine specified drive for changed disk and display appropriate info
 */
 
BOOL DiskExamineForChange(DevParamsPtr devParam)
{
	BOOL changed;
	register BOOL ready;
	register BOOL isFloppy = devParam->Floppy || notSCSI;
	register BOOL isTapeOrRemovable;
	
	if (mode == MODE_BACKUP) {
		isTapeOrRemovable = prefs.Destination == DEST_TAPE;
	} else {
		isTapeOrRemovable = prefs.Source == DEST_TAPE;
	}
	if ((!isFloppy) && isTapeOrRemovable) {
		TapeIO(devParam, CMD_TEST_UNIT_READY, NULL);	/* May set DevParam->Changed */
		/*if (!tapeDevice) {*/
		TapeIO(devParam, CMD_REQUEST_SENSE, NULL);	/* Clear sense for dumb floptical */
		/*}*/
	}
	if (changed = devParam->Changed) {
/*
	If not a floppy, check to see if disk loaded
*/
		if (isFloppy) {
			ready = IsDiskInDrive(devParam);
		} else {
			ready = (TapeIO(devParam, CMD_TEST_UNIT_READY, NULL) == 0) &&
				(((struct IOStdReq *) devParam->IOB[statusIOBIndex])->io_Error == 0);
			if (tapeDevice) {
				if (prefs.TapeOpts.AutoRetension && sessionNumber == 0) {
					RetensionTape(devParam, TRUE);
				}
			}
		}
		devParam->Changed = FALSE;
		if (ready) {
			if (devParam->Status != STATUS_ACTIVE) {	/* Already on? */
				if (operation == OPER_PAUSE && (activeDevice == devParam)) {
					StatusPaused(devParam);					/* Display paused status */
				} else {
					if (operation == OPER_COMPLETE) {
						StatusCompleted(devParam);
					} else {
						StatusReady(devParam);			/* Display readiness */
						if (devParam == activeDevice) {
							StatusCheck(devParam);		/* Needs track 0 check */
						}
					}
				}
			}
		} else {
 			StatusNotReady(devParam);						/* No disk yet */
			devParam->Changed = TRUE;
		}
	}
	return (changed);
}

/*
 * Restores a file date/time stamp, protection bits and comments. Called
 * during backup to set the archive bit on a file, if user wants to.
 * Returns the result of the packet IO.
 */
 
BOOL RestoreProtectionBits(TextPtr fileName)
{
	MsgPortPtr procID;
	register LONG result = 0;
	register BOOL doProt = FALSE;
	ULONG assignLock;
	struct DevProc *devProcPtr;
	TextChar buff[MAX_COMMENT_LEN + 7];
	TextPtr buffPtr;
	UBYTE protBits;
	register ULONG temp;
	
	if ((mode != MODE_BACKUP) || (outputCompleted && prefs.OldPrefs.BSetArchBit) || (curFib->df_Prot & FIBF_READ)) {
		result--;
		devProcPtr = NHGetDeviceProc(fileName, &procID, &assignLock);
		if (procID) {
/*
	Make into a BSTR here, and keep the NULL byte for the Lock() below...
*/
			temp = strlen(fileName);
			BlockMove(fileName, &strBuff[1], temp+1);
			strBuff[0] = (UBYTE) temp;
			packet.sp_Pkt.dp_Arg2 = assignLock;
			packet.sp_Pkt.dp_Arg3 = (ULONG) MKBADDR(strBuff);
			result = 0;
			protBits = curFib->df_Prot;
			if (startMode == MODE_RESTORE) {
				if (verifyMode == RMODE_RESTORE) {
					doProt = TRUE;
					if (prefs.RestoreDates != DATE_SYSTEM) {
						if (prefs.RestoreDates == DATE_ORIG) {
							packet.sp_Pkt.dp_Arg4 = (LONG) &curFib->df_DateStamp;
						} else {
							packet.sp_Pkt.dp_Arg4 = (LONG) &sessionDateStamp;
						}
						result |= PacketIO(procID, ACTION_SET_DATE);
					}
					if (curFib->df_Comment) {
/*
	Make sure buffPtr is longword aligned
*/
						buffPtr = (TextPtr) ((((ULONG)buff) & 0xFFFFFFFC) + 4);
						strcpy(buffPtr+1, curFib->df_Comment);
						*buffPtr = strlen(buffPtr+1);
						packet.sp_Pkt.dp_Arg4 = (ULONG) MKBADDR(buffPtr);
						result |= PacketIO(procID, ACTION_SET_COMMENT);
					}
					if (prefs.OldPrefs.RSetArchBit) {
						protBits |= FIBF_ARCHIVE;
					}
				}
			} else {
				if (prefs.OldPrefs.BSetArchBit && outputCompleted) {
					doProt = TRUE;
					protBits |= FIBF_ARCHIVE;
				} else {
					doProt = (curFib->df_Prot & FIBF_READ) != 0;
				}
			}
			if (doProt) {
				packet.sp_Pkt.dp_Arg4 = protBits;
				result |= PacketIO(procID, ACTION_SET_PROTECT);
			}
		}
		NHFreeDeviceProc(devProcPtr);
	}
	return (result == 0);
}

/*
 * New Horizons' API for calling DeviceProc() or GetDeviceProc().
 */
 
static struct DevProc *NHGetDeviceProc(TextPtr fileName, register MsgPortPtr *procID, ULONG *assignLock)
{
	register struct DevProc *devProcPtr = NULL;
	
	*procID = NULL;
	if (intuiVersion >= OSVERSION_2_0) {
		devProcPtr = GetDeviceProc(fileName, NULL);
		if (IoErr() == ERROR_OBJECT_NOT_FOUND && devProcPtr->dvp_Flags & DVPF_ASSIGN) {
			while ((devProcPtr = GetDeviceProc(fileName, devProcPtr)) == NULL);
		}
		if (devProcPtr) {
			*procID = devProcPtr->dvp_Port;
			*assignLock = devProcPtr->dvp_Lock;
		}
	} else {
		*procID = (MsgPortPtr) DeviceProc(fileName);
		*assignLock = IoErr();
	}
	return (devProcPtr);
}

/*
 * New Horizons' API for calling FreeDeviceProc() in V2.0 if allocated
 */
 
static void NHFreeDeviceProc(struct DevProc *devProcPtr)
{
	if (devProcPtr)
		FreeDeviceProc(devProcPtr);
}

/*
 * Update the archive bits at the end of a successful backup.
 */
 
BOOL ProcessAllArchiveBits()
{
	ULONG assignLock;
	MsgPortPtr procID = NULL;
	struct DevProc *devProcPtr = NULL;
	RequestPtr req;
	UWORD len;
	BarGraphPtr barGraph = NULL;
	ULONG num = 0;
	Rectangle rect;
	register IntuiMsgPtr intuiMsg;
	
	if (prefs.OldPrefs.BSetArchBit) {
		InitList();
		curFib = NextFile(FALSE, FALSE);
		if (curFib) {
			if (intuiVersion < OSVERSION_2_0) {
				devProcPtr = NHGetDeviceProc(strPath, &procID, &assignLock);
				if (procID == NULL) {
					return (FALSE);
				}
			}
			reqList[REQ_SEEKALTCAT]->Gadgets[0].Info = (Ptr)strSetArch;
			req = DoGetRequest(REQ_SEEKALTCAT);
			if (req) {
				GetGadgetRect(GadgetItem(req->ReqGadget, 2), cmdWindow, req, &rect);
				barGraph = NewBarGraph(&rect, selFiles, 16, BG_HORIZONTAL);
				DoDrawBarGraph(req, barGraph);
			}
			SetStdPointer(cmdWindow, POINTER_WAIT);
			do {
				if ((curFib->df_Flags & FLAG_ERR_MASK) == 0) {
/*
	Before V36, the Read and Write bits were not respected, so we have to do
	this the old-fashioned way.
*/
					if (procID) {
						strcpy(strBuff, strPath);
						AppendDirPath(strBuff, curFib->df_Name);
						len = strlen(strBuff);
						BlockMove(strBuff, &strBuff[1], len+1);
						strBuff[0] = len;
						packet.sp_Pkt.dp_Arg2 = assignLock;
						packet.sp_Pkt.dp_Arg3 = (ULONG) MKBADDR(strBuff);
						packet.sp_Pkt.dp_Arg4 = curFib->df_Prot | FIBF_ARCHIVE;
						(void) PacketIO(procID, ACTION_SET_PROTECT);
					} else {	
						SetProtection(curFib->df_Name, curFib->df_Prot | FIBF_ARCHIVE);
					}
				}
				if ((num & 3) == 0) {
					while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) {
						if (intuiMsg->Class == NEWSIZE) {
							DoIntuiMessage(intuiMsg);
							DoDrawBarGraph(req, barGraph);
						} else {
							ReplyMsg( (MsgPtr) intuiMsg);
						}
					}
				}
				DoSetBarGraph(req, barGraph, ++num);
			} while (curFib = NextFile(FALSE, FALSE));
			SetArrowPointer();
			if (barGraph)
				DisposeBarGraph(barGraph);
			DestroyRequest(req);
			NHFreeDeviceProc(devProcPtr);			// Handles NULL
		}
	}
	return (TRUE);
}
	
/*
 * Enable the reading of a file by clearing the read protection bit.
 */
 
LONG EnableFileAccess(TextPtr fileName)
{
	MsgPortPtr procID;
	register LONG result = -1;
	ULONG assignLock;
	struct DevProc *devProcPtr;
	TextChar buff[PATHSIZE];
	UWORD len;
	
	if (intuiVersion < OSVERSION_2_0) {
		devProcPtr = NHGetDeviceProc(fileName, &procID, &assignLock);
		if (procID) {
/*
	Make into a BSTR here, and keep the NULL byte for the Lock() below...
*/
			len = strlen(fileName);
			BlockMove(fileName, &buff[1], len+1);
			buff[0] = len;
			packet.sp_Pkt.dp_Arg2 = assignLock;
			packet.sp_Pkt.dp_Arg3 = (ULONG) MKBADDR(buff);
			packet.sp_Pkt.dp_Arg4 = curFib->df_Prot & (~(FIBF_READ | FIBF_WRITE | FIBF_DELETE));
			result = PacketIO(procID, ACTION_SET_PROTECT);
			NHFreeDeviceProc(devProcPtr);
		}
	}
	else
		result = SetProtection(fileName, curFib->df_Prot & (~(FIBF_READ | FIBF_WRITE | FIBF_DELETE)));
	return (result);
}
