/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Menu definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Language.h>
#include <Toolbox/Menu.h>

#include "QB.h"

extern TextChar	macroNames2[10][MACRONAME_LEN];
extern TextChar	strQuit[], strPrCat[], strSaveCatAs[], strTagAll[], strUntagAll[];

/*
 * Project settings submenu items
 */
 
static MenuItemTemplate defaultsItems[] = {
#if (AMERICAN | BRITISH)
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Save", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Save As...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Load...", NULL },
#elif GERMAN
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Speichern", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Speichern als...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Laden...", NULL },
#elif FRENCH
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Sauver", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Sauver sous...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Charger...", NULL },
#elif SWEDISH
#endif
	{ MENU_NO_ITEM }
};

/*
 * Project menu items
 */
 
static MenuItemTemplate projectItems[] = {
#if (AMERICAN | BRITISH)
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "About Quarterback...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, strSaveCatAs, NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Page Setup...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, strPrCat, NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
   { MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Settings", &defaultsItems[0] },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'Q', 0, 0, &strQuit[0], NULL },
#elif GERMAN
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Version, Copyright...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, strSaveCatAs, NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Druckseiteneinstellung...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, strPrCat, NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
   { MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Einstellungen", &defaultsItems[0] },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'B', 0, 0, &strQuit[0], NULL },
#elif FRENCH
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "A propos de Quarterback...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, strSaveCatAs, NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Format de page...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, strPrCat, NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
   { MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "R�glages", &defaultsItems[0] },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'Q', 0, 0, &strQuit[0], NULL },
#elif SWEDISH
#endif
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

/*
 * Options menu items
 */
 
static MenuItemTemplate optionsItems[] = {
#if (AMERICAN | BRITISH)
	{ MENU_TEXT_ITEM, MENU_ENABLED,'B', 0, 0, "Backup Options...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'R', 0, 0, "Restore Options...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Catalog Options...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Session Log Options...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Buffer Options...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Tape Options...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Preferences...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Reset All", NULL },
#elif GERMAN
	{ MENU_TEXT_ITEM, MENU_ENABLED,'S', 0, 0, "Datensicherung...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'W', 0, 0, "Daten wiederherstellen...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Inhaltsverzeichnis...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Sitzungsprotokoli...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Puffer...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Tape Options...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Allgemeines...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Alles auf Grundeinstellungen setzen", NULL },
#elif FRENCH
	{ MENU_TEXT_ITEM, MENU_ENABLED,'S', 0, 0, "Options de sauvegarde...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'R', 0, 0, "Options de restauration...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Options du catalogue...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Options du rapport...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Options m�moire...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Tape Options...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Pr�f�rences...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "D�fauts", NULL },
#elif SWEDISH
#endif
	{ MENU_NO_ITEM }
};
	
/*
 * Tag menu
 */
 
static MenuItemTemplate tagItems[] = {
#if (AMERICAN | BRITISH)
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0, 0, 0, "Tag Selection", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'T',0, 0, strTagAll, NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0, 0, 0, "Untag Selection", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'U',0, 0, strUntagAll, NULL },
	{ MENU_TEXT_ITEM,	0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'F', 0, 0,"Tag Filter...", NULL },
#elif GERMAN
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0, 0, 0, "ausgew�hlte Dateien markieren", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'M',0, 0, strTagAll, NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0, 0, 0, "ausgew�hlte Dateien entmarkieren", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'E',0, 0, strUntagAll, NULL },
	{ MENU_TEXT_ITEM,	0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'U', 0, 0,"Auswahtkriterium...", NULL },
#elif FRENCH
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0, 0, 0, "Marquer s�lection", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'M',0, 0, strTagAll, NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0, 0, 0, "Enlever s�lection", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'E',0, 0, strUntagAll, NULL },
	{ MENU_TEXT_ITEM,	0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'F', 0, 0,"Filtre de marquage...", NULL },
#elif SWEDISH
#endif
	{ MENU_NO_ITEM }
};

/*
 * Utilities menu
 */
 
static MenuItemTemplate utilitiesItems[] = {
#if (AMERICAN | BRITISH)
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0,  0, 0, "Tape Control...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'G', 0, 0, "Get Session Info...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'I', 0, 0, "Set Session Info...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0,  0, 0, "SCSI Interrogator...", NULL },
#elif GERMAN
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0,  0, 0, "Band-Steuerung...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'A', 0, 0, "Sitzungsinformation anzeigen...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'I', 0, 0, "Sitzungsinformation eingeben...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0,  0, 0, "SCSI-Bus abfragen...", NULL },
#elif FRENCH
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0,  0, 0, "Contr�le des bandes...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'L', 0, 0, "Lire infos sauvegarde...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'I', 0, 0, "Entrer infos sauvegarde...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0,  0, 0, "Infos sur unit�s SCSI...", NULL },
#elif SWEDISH
#endif
	{ MENU_NO_ITEM }
};

/*
 *	Macro menu
 */

static MenuItemTemplate macroItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[0][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[1][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[2][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[3][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[4][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[5][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[6][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[7][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[8][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[9][0], NULL },
	{ MENU_TEXT_ITEM },
#if (AMERICAN | BRITISH)
	{ MENU_TEXT_ITEM, MENU_ENABLED,'M',0, 0, "Other...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Customize...", NULL },
#elif GERMAN
	{ MENU_TEXT_ITEM, MENU_ENABLED,'K',0, 0, "Weitere Makros...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Makros installieren...", NULL },
#elif FRENCH
	{ MENU_TEXT_ITEM, MENU_ENABLED,'A',0, 0, "Autre...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Sp�cifier...", NULL },
#elif SWEDISH
#endif
	{ MENU_NO_ITEM }
};

/*
 *	Menu strip templates for main window
 */

MenuTemplate docWindMenus[] = {
#if (AMERICAN | BRITISH)
	{ " Project ", 	&projectItems[0] },
	{ " Options ", 	&optionsItems[0] },
	{ " Tag ", 		&tagItems[0] },
	{ " Utilities ",	&utilitiesItems[0] },
	{ " Macro ",	&macroItems[0] },
#elif GERMAN
	{ " Projekt ", 	&projectItems[0] },
	{ " Einstellungen ",&optionsItems[0] },
	{ " Markieren ",	&tagItems[0] },
	{ " Hilfsmittel ",&utilitiesItems[0] },
	{ " Makros ",	&macroItems[0] },
#elif FRENCH
	{ " Projet ",	&projectItems[0] },
	{ " Options ",	&optionsItems[0] },
	{ " Marquage ",	&tagItems[0] },
	{ " Utilitaires ",&utilitiesItems[0] },
	{ " Macro ", 	&macroItems[0] },
#elif SWEDISH
#endif
	{ NULL, NULL }
};
