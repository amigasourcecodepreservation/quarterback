/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1992-93 New Horizons Software, Inc.
 *
 *	Window, screen, and window gadget definitions
 *	(except for items that must be in chip memory)
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Image.h>
#include <Toolbox/Border.h>
#include <Toolbox/Language.h>

#include "QB.h"


/*
 *	External variables
 */

extern TextChar screenTitle[], strBackup[], strRestore[];
extern TextChar strTag[], strUntag[], strBack[], strEnter[];

/*
 *	Screen definition
 */

struct NewScreen newScreen = {
	0, 0, 640, STDSCREENHEIGHT, 2, 0, 1,
	HIRES, WBENCHSCREEN,
	NULL, screenTitle, NULL, NULL
};

/*
 *	Window definitions
 */

struct NewWindow newBackWindow = {
	0, 0, 640, 200, -1, -1,
	MOUSEBUTTONS | MENUPICK | REFRESHWINDOW | ACTIVEWINDOW | INACTIVEWINDOW |
		RAWKEY | NEWPREFS,
	SIMPLE_REFRESH | BACKDROP | BORDERLESS | ACTIVATE,
	NULL, NULL, NULL, NULL, NULL,
	0, 0, 0, 0,
	CUSTOMSCREEN
};

struct NewWindow newWindow = {
	0, 0, 480, 160, -1, -1,
	MOUSEBUTTONS | GADGETDOWN | GADGETUP | CLOSEWINDOW |
		MENUPICK | NEWSIZE | REFRESHWINDOW |
		ACTIVEWINDOW | INACTIVEWINDOW | RAWKEY | INTUITICKS |
		DISKINSERTED | DISKREMOVED,
	WINDOWSIZING | SIZEBRIGHT | SIZEBBOTTOM | WINDOWDEPTH | WINDOWCLOSE |
		WINDOWDRAG | SIMPLE_REFRESH | ACTIVATE,
	NULL, NULL, NULL, NULL, NULL,
	200, 86, 0xFFFF, 0xFFFF,
	CUSTOMSCREEN
};

GadgetTemplate windowGadgets[] = {
#if (AMERICAN | BRITISH)

	{ GADG_PUSH_BUTTON, 14, BUTTONS_HEIGHT, 0, 0, BUTTON_WIDTH, 20, 0, 0,'B',0, &strBackup[0] },
	{ GADG_PUSH_BUTTON, 14+BUTTON_WIDTH+11, BUTTONS_HEIGHT, 0, 0, BUTTON_WIDTH, 20, 0, 0,'R',0, &strRestore[0] },

#elif GERMAN
	
	{ GADG_PUSH_BUTTON, 14, BUTTONS_HEIGHT, 0, 0, BUTTON_WIDTH+25, 20, 0, 0,'S',0, &strBackup[0] },
	{ GADG_PUSH_BUTTON, 14+BUTTON_WIDTH+11+25, BUTTONS_HEIGHT, 0, 0, BUTTON_WIDTH+25, 20, 0, 0,'W',0, &strRestore[0] },

#elif FRENCH

	{ GADG_PUSH_BUTTON, 14, BUTTONS_HEIGHT, 0, 0, BUTTON_WIDTH+25, 20, 0, 0,'S',0, &strBackup[0] },
	{ GADG_PUSH_BUTTON, 14+BUTTON_WIDTH+11+25, BUTTONS_HEIGHT, 0, 0, BUTTON_WIDTH+25, 20, 0, 0,'R',0, &strRestore[0] },

#elif SWEDISH
#endif

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - ARROW_WIDTH, 1 - ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_LEFT },
	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - ARROW_WIDTH, 1 - ARROW_HEIGHT,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_RIGHT },
	{ GADG_PROP_HORIZ | GADG_PROP_NEWLOOK,
		BUTTONS_WIDTH, 0, 0, 000,
		-BUTTONS_WIDTH, 0, 000, 000, 0, 0, NULL },

	{ GADG_ACTIVE_BORDER,
		BUTTONS_WIDTH, 11, /*BUTTONS_WIDTH*/0, 3,
		-BUTTONS_WIDTH,-11, 1-/*BUTTONS_WIDTH-*/2*IMAGE_ARROW_HEIGHT, -3-IMAGE_ARROW_HEIGHT, 0, 0, NULL },
	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - IMAGE_ARROW_WIDTH, 1 - IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_UP },
	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - IMAGE_ARROW_WIDTH, 1 - IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_DOWN },
	{ GADG_PROP_VERT | GADG_PROP_NEWLOOK, 
		0, 0, 1 - IMAGE_ARROW_WIDTH + 4, 3+11,
		0, 0, ARROW_WIDTH - 8, -3*ARROW_HEIGHT-4, 0, 0, NULL },

	{ GADG_STAT_TEXT, 20, 27, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
/*	{ GADG_STAT_TEXT, 20, 39, 0, 0, 0, 0, 0, 0, 0, 0, NULL },*/
	{ GADG_STAT_STDBORDER, 14, 22, 0, 0, BUTTONS_WIDTH-26, 32, 0, 0, 0, 0, (Ptr) BORDER_SHADOWBOX },
		
/*
 	The next four buttons are the last two in normal mode, followed by two additional
	buttons for tagging/untagging in file selection mode.
*/

#if (AMERICAN | BRITISH)

	{ GADG_PUSH_BUTTON, 14, BUTTONS_HEIGHT + 28, 0, 0, BUTTON_WIDTH, 20, 0, 0, KEY_ENTER2, 0, strEnter },
	{ GADG_PUSH_BUTTON, 14+BUTTON_WIDTH+11, BUTTONS_HEIGHT + 28, 0, 0, BUTTON_WIDTH, 20, 0, 0, KEY_BACK2, 0, &strBack[0] },
	{ GADG_PUSH_BUTTON, 14, BUTTONS_HEIGHT + 56, 0, 0, BUTTON_WIDTH, 20, 0, 0, KEY_TAG, 0, &strTag[0] },
	{ GADG_PUSH_BUTTON, 14+BUTTON_WIDTH+11, BUTTONS_HEIGHT + 56, 0, 0, BUTTON_WIDTH, 20, 0, 0, KEY_UNTAG, 0, &strUntag[0] },

	{ GADG_STAT_TEXT, 70, 165,0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 70, 180,0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 70, 215,0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 70, 230,0, 0, 0, 0, 0, 0, 0, 0, NULL },
#else
	{ GADG_PUSH_BUTTON, 14, BUTTONS_HEIGHT + 28, 0, 0, BUTTON_WIDTH+25, 20, 0, 0, KEY_ENTER2, 0, &strEnter[0] },
	{ GADG_PUSH_BUTTON, 14+BUTTON_WIDTH+11+25, BUTTONS_HEIGHT + 28, 0, 0, BUTTON_WIDTH+25, 20, 0, 0, KEY_BACK2, 0, &strBack[0] },
	{ GADG_PUSH_BUTTON, 14, BUTTONS_HEIGHT + 56, 0, 0, BUTTON_WIDTH+25, 20, 0, 0, KEY_TAG, 0, &strTag[0] },
	{ GADG_PUSH_BUTTON, 14+BUTTON_WIDTH+11+25, BUTTONS_HEIGHT + 56, 0, 0, BUTTON_WIDTH+25, 20, 0, 0, KEY_UNTAG, 0, &strUntag[0] },

	{ GADG_STAT_TEXT, 90, 165,0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 90, 180,0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 90, 215,0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 90, 230,0, 0, 0, 0, 0, 0, 0, 0, NULL },
#endif
	
	{ GADG_STAT_TEXT, 20, 150,0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_BORDER, 14, 162, 0, 0, BUTTONS_WIDTH-26, 32, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 20, 165,0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 20, 180,0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 20, 200,0, 0, 0, 0, 0, 0, 0, 0, NULL },	
	{ GADG_STAT_BORDER, 14, 212, 0, 0, BUTTONS_WIDTH-26, 32, 0, 0, 0, 0, NULL },
		
	{ GADG_STAT_TEXT, 20, 215,0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 20, 230,0, 0, 0, 0, 0, 0, 0, 0, NULL },
		
/*
	These additional gadgets replace the four buttons above during backup/restore.
*/
	{ GADG_STAT_STDBORDER, 14, BUTTONS_HEIGHT + 27, 0, 0,
			BUTTONS_WIDTH-26, 54, 0, 0, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_TEXT,140, 150, 0, 0, 0, 0, 0, 0, 0, 0, NULL }, 
	{ GADG_STAT_BORDER, 14+18, BUTTONS_HEIGHT + 31, 3, 0,
			BUTTONS_WIDTH-50, 49, 0, 0, 0, 0, NULL },
	{ GADG_STAT_BORDER, 14, BUTTONS_HEIGHT + 31, 3, 0,
			16, 49, 0, 0, 0, 0, NULL },
	{ GADG_ITEM_NONE }
};
