/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Quarterback
 * Copyright (c) 1992-93 New Horizons Software, Inc.
 *
 * Text strings
 */
 
#include <exec/types.h>

#include <Toolbox/Language.h>
 
#include <TypeDefs.h>

#include "Version.h"

/*
 * PART ONE: Strings contained within structures
 */
 
TextChar strRAM[]	= "RAM";

TextChar strDF0[]	= "DF0:";
TextChar strDF1[]	= "DF1:";
TextChar strDF2[]	= "DF2:";
TextChar strDF3[]	= "DF3:";

TextPtr	driveStrings[4] = {
	strDF0, strDF1, strDF2, strDF3
};

TextPtr	tapeErrorStrs[] = {
	"(Medium Error)",
	"(Hardware Error)",
	"(Illegal Request)",
	"(Unit Attention)",
	"(Data Protect)",
};

#if (AMERICAN | BRITISH)

TextPtr initErrors[] = {
	"Bad system state",
	"Can't find icon.library",
	"Can't find diskfont.library",
	"Can't open screen",
	"Not enough memory",
	"Quarterback already running!",
	"Can't open window",
};

TextPtr strsErrors[] = {
	"Unknown internal error.",
	"Not enough memory.",
	"Unable to open file.",
	"Unable to save file.",
	"Unable to print.",
	"Improper number of copies.",
	"Improper page size.",
	"Unable to backup file\n\"@\".",
	"Unable to restore file\n\"@\".",
	"Unable to open drawer.",
	"Unable to create drawer\n\"@\".",
	"Invalid file name\n\"@\".",
	"Invalid drawer name\n\"@\".",
	"DOS error: %",
	"Disk in @ is write-protected. Select \"OK\" to try again.",
	"Error writing to @. To try again, insert another disk and hit \"OK\".",
	"Disk in @ is unusable. Try a different disk.",
	"Disk is bad. Operation aborted.",
	"Compression will be disabled due to insufficient memory.",
	"No files have been tagged.\nUnable to proceed.",
	"Warning: Disk in @ is an AmigaDOS volume. Proceed anyway?",
	"Unable to read catalog. Try again with the alternate catalog?",
	"Unable to read from @. Hit \"OK\" to retry, \"Skip\" to skip disk.",
	"Not a backup disk in @. Hit \"OK\" to retry, \"Skip\" to skip disk.",
	"Disk #% in @ is not the needed disk. Please replace with disk #~.",
	"Disk in @ does not belong to the same backup set. Try another disk.",
	"Sorry, without the correct password, read access is denied.",
	"Please load disk #% from this backup set into @.",
	"Load the LAST disk from this backup set into @.",
	"Disk is full. Operation aborted.",
	"Compressed data on @ is corrupt. Cannot process file.",
	"All tags you have made will be lost.\nIs this OK?",
	"No floppy drives are enabled.\nAt least one must be selected.",
	"Backups to non-removable media must be written as AmigaDOS files.",
	"Are you sure you want to reset all Quarterback options?",
	"Verify failure on @. Hit \"OK\" to rewrite disk.",
	"Invalid date entry. Please try again.",
	"The tape drive is busy. Please wait, or hit \"OK\" to abort.",
	"Unable to open device \"@\", unit %.",
	"Tape is write-protected.\nSelect \"OK\" to try again.",
	"Erasing will destroy all information on the tape. Continue anyway?",
	"Re-tensioning a tape may take several minutes. Continue anyway?",
	"This operation cannot be performed on non-removable media.",
	"Device not found.",
	"Unable to read catalog from tape. Select \"OK\" to try again.",
	"This tape does not contain a valid Quarterback backup.",
	"Tape % is not the needed tape. Please replace with tape #~.",
	"This tape does not belong to the same backup set. Try another tape.",
	"All selected drives must have the same capacity. Operation aborted.",
	"Tape error #% @has occurred. Operation aborted.",
	"Unable to set protection flags for @. Try again?",
	"Backup destination file already exists. Do you wish to replace it?",
	"Unable to erase tape.",
	"Warning: AmigaDOS partition was found.\nSelect \"OK\" to proceed anyway.", 
	"Please insert tape #% into the @.",
	"Please insert disk #% into @.",
	"Tape in @ is a backup tape. Insert a different tape.",
	"Disk in @ is a backup disk. Insert a different disk.",
	"Are you sure it is OK to abort the @?",
	"This pathname is too long.\nPlease use a shorter name.",
	"Unable to save session log file.",
	"Warning: This appears to be a non-removable tape drive. Proceed anyway?",
	"This block is unreadable. Hit \"OK\"\nto retry, or \"Skip\" to continue.",
	"This block is unreadable. Hit \"OK\"\nto try again.",
	"Warning: This catalog may be corrupt.\nDo you wish to continue?",
	"Are you sure you want to save current settings as default values?",
	"Insufficient memory. Please reduce the buffer size and try again.",
	"AmigaDOS file name is actually a drawer.\nPlease rename and try again.",
	"Sorry, the pathname is too long. Operation aborted.",
	"Backup appears to be corrupt.\nDo you wish to continue anyway?",
	"Could not decompress data on @. Attempt to continue?",
	"Filename in backup does not match entry in catalog. Proceed anyway?",
	"Unable to read catalog from this backup.",
	"Drive @ not found. Please check to see if it has been mounted.",
	"Error while spacing tape forward. There may be no more data on this tape.",
	"Please load tape #% from this backup set into the @.",
	"Unable to retrieve entire catalog. Use what was read anyway?",
	"This device does not support onboard compression. Compression disabled.",
	"Drive @ cannot be accessed, because the drive is already in use.",
	"SCSI timeout has occurred. Operation aborted.",
	"This is not a valid @ device.\nOperation aborted.",
	"Unable to read from tape drive. Hit \"OK\" to try again.",
	"Please insert a tape into the @.",
	"AREXX not present.",
	"Macro execution failed.",
};

TextPtr strDriveType[] = {
	"Direct-access",
	"Sequential",
	"Printer",
	"Processor",
	"WORM",
	"CD-ROM",
	"Scanner",
	"Optical",
	"Changer",
	"Comm",
};

TextPtr	aboutStrings[] = {
	" By Glen Merriman ",
	" and James Bayless "
};

#elif GERMAN

TextPtr initErrors[] = {
	"undefinierter Systemzustand",			/* Bad system state */
	"Kann die icon.library nicht �ffnen",	/* Can't find icon.library */
	"Kann die diskfont.library nicht �ffnen",/* Can't find diskfont.library */
	"Kann Schirm nicht �ffnen",				/* Can't open screen */
	"Nicht genug freier Speicher vorhanden",/* Not enough memory */
	"Es l�uft bereits eine Kopie von Quarterback!",/* Quarterback already running! */
	"Kann Fenster nicht �ffnen"				/* Can't open window */
};

TextPtr strsErrors[] = {
	"\nUnbekannter interner Fehler.",		/* Unknown internal error. */
	"\nNicht genung freier Speicher vorhanden.",/* Not enough memory. */
	"\nKann die Datei nicht �ffnen.",		/* Unable to open file. */
	"\nKann die Datei nicht abspeichern.",	/* Unable to save file. */
	"\nKann nicht drucken.",/* Unable to print. */
	"\nUnzul�ssige Anzahl von Kopien.",		/* Improper number of copies. */
	"\nDiese Seite existiert nicht.",		/* No such page. */
	"\nUnzul�ssige Seitengr��e.",				/* Improper page size. */
	"\nUnzul�ssige Seitenh�he.",				/* Improper page height. */
	"Kann die Datei nicht sichern\n\"@\".",/* Unable to backup file \"@\". */
	"Kann die Datei nicht wiederherstellen\n\"@\".",/* Unable to restore file \"@\". */
	"\nKann das Verzeichnis nicht �ffnen.",/* Unable to open drawer. */
	"Kann das Verzeichnis nicht anlegen\n\"@\".",/* Unable to create drawer\n\"@\". */
	"Ung�ltiger Dateiname\n\"@\".",/* Invalid file name\n\"@\". */
	"Ung�ltiger Verzeichnisname\n\"@\".",	/* Invalid drawer name\n\"@\". */
	"\nDOS Fehler: %",							/* DOS error: % */
	"Die Diskette in @ ist schreibgesch�tzt.\nDr�cken Sie \"OK\", um es nochmal zu\nversuchen.",/* Disk in @ is write-protected. Select \"OK\" to try again. */
	"Fehler beim Schreiben auf @.\nLegen Sie eine andere Diskette ein und\ndr�cken sie \"OK\", um es nochmal zu\nversuchen.",/* Error writing to @. To try again, insert another disk and hit \"OK\". */
	"Die Diskette ist unbrauchbar. Versuchen\nsie es mit einer anderen Diskette.",/* Disk in @ is unusable. Try a different disk. */
	"Die Diskette ist hat Fehlstellen.\nVorgang abgebrochen.",/* Disk is bad. Operation aborted. */
	"Die Komprimierung wird abgeschaltet, da\nzuwenig freier Speicher vorhanden ist.",/* Compression will be disabled due to insufficient memory. */
	"Es wurden keine Dateien markiert.\nWeiterbearbeitung nicht m�glich.",/* No files have been tagged. Unable to proceed. */
	"Achtung: Die Diskette in @ ist ein\nAmigaDOS-Datentr�ger. Trotzdem\nweitermachen?",/* Warning: Disk in @ is an AmigaDOS volume. Proceed anyway? */
	"Kann das Inhaltsverzeichnis nicht lesen.\nSoll ich versuchen das alternative\nInhaltsverzeichnis zu lesen.",/* Unable to read catalog. Try again with the alternate catalog? */
	"Kann von @ nicht lesen. Dr�cken\nSie \"OK\", um es nochmal zu versuchen, oder\n\"�bergehen\", um mit der n�chsten Diskette\nfortzufahren.",/* Unable to read from @. Hit \"OK\" to retry, \"Skip\" to skip disk. */
	"Die Diskette in @ ist keine Quarterback-\nDiskette. Dr�cken Sie \"OK\", um es\nnochmal zu versuchen, oder \"�bergehen\",\num mit der n�chsten Diskette fortzufahren.",/* Not a backup disk in @. Hit \"OK\" to retry, \"Skip\" to skip disk. */
	"Die Diskette % in @ ist nicht die\nben�tigte. Ersetzen Sie diese durch die\nrichtige Diskette.",/* Disk % in @ is not the needed disk. Please replace with disk ~. */
	"Die Diskette in @ geh�rt nicht zur selben\nDatensicherung. Versuche Sie es\nmit einer anderen Diskette.",/* Disk in @ does not belong to the same backup set. Try another disk. */
	"Tut mir leid, ohne das richtige Kennwort\nkann die Datensicherung nicht\neingelesen werden.",/* Sorry, without the correct password, read access is denied. */
	"Legen Sie Diskette % von der\nDatensicherung die Sie zur�cklesen\nin @ ein.",/* Load disk % from the backup set you wish to restore into @. */
	"Legen Sie die LETZTE Diskette dieser\nDatensicherung in @ ein.",/* Load the LAST disk from this backup set into @. */
	"Die Diskette ist voll. Vorgang\nabgebrochen.",/* Disk is full. Operation aborted. */
	"Die komprimierten Daten auf @ sind\nbesch�digt. Kann die Datei nicht\nwiederherstellen.",/* Compressed data on @ is corrupt. Cannot restore file. */
	"Alle Markierungen die Sie gesetzt haben\nwerden verloren gehen. Wollen Sie das?",/* All tags you have made will be lost.\nIs this OK? */
	"Es sind keine Diskettenlaufwerke\nausgew�hlt. Mindestens eines mu�\nausgew�hlt sein.",/* No floppy drives are enabled.\nAt least one must be selected. */
	"Datensicherungen auf nicht\nauswechselbare Datentr�ger m�ssen als\nAmigaDOS-Datei geschrieben werden.",/* Backups to non-removable media must be written as AmigaDOS files. */
	"Sind Sie sicher, da� sie alle\nQuarterback-Einstellungen zur�cksetzen\nwollen?",/* Are you sure you want to reset all Quarterback options? */
	"Pr�ffehler auf @. Dr�cken Sie \"OK\", um\ndie Diskette nochmal zu\nbeschreiben, oder \"�berspringen\", um\nden Fehler zu ignorieren.",/* Verify failure on @. Hit \"OK\" to rewrite disk, \"Skip\" to ignore. */
	"Ung�ltige Datumsangabe. Bitte versuchen\nSie es nochmal.",/* Invalid date entry. Please try again. */
	"Das Bandlaufwerk arbeitet noch. Bitte\nwarten Sie oder dr�cken Sie \"OK\" zum\nabbrechen.",/* The tape drive is busy. Please wait, or hit \"OK\" to abort. */
	"Ich kann das Ger�t \"@\"\nEinheit % nicht ansprechen.",/* Unable to open device \"@\", unit %. */
	"Das Band ist schreibgesch�tzt. Dr�cken\nSie \"OK\", um es nochmal zu versuchen.",/* Tape is write-protected.\nSelect \"OK\" to try again. */
	"Das L�schen wird alle Daten auf dem Band\nzerst�ren.\nWollen Sie trotzdem fortfahren?",/* Erasing will destroy all information on the tape. Continue anyway? */
	"Das Straffen des Bandes kann mehrere\nMinuten dauern.\nWollen Sie trotzdem fortfahren?",/* Re-tensioning a tape may take several minutes. Continue anyway? */
	"Dieser Vorgang kann nur auf\nauchwechselbare Datentr�ger angewandt\nwerden.",/* This operation cannot be performed on non-removable media. */
	"\nGer�t nicht gefunden.",/* Device not found. */
	"Kann das Inhaltsverzeichnis nicht vom\nBand lesen. Dr�cken Sie \"OK\", um es\nnochmal zu versuchen.",/* Unable to read catalog from tape. Select \"OK\" to try again. */
	"Dieses Band enth�lt keine g�ltige\nQuarterback-Datensicherung.",/* This tape does not contain a valid Quarterback backup. */
	"Das Band % ist nicht das ben�tigte.\nErsetzen Sie es durch das richtige Band.",/* Tape % is not the needed tape. Replace with correct tape. */
	"Dieses Band geh�rt nicht zur gleichen\nDatensicherung. Versuchen Sie es mit\neinem anderen Band.",/* This tape does not belong to the same backup set. Try another tape. */
	"Alle ausgew�hlten Laufwerke m�ssen die\ngleiche Speicherkapazit�t besitzen.\nVorgang abgebrochen.",/* All selected drives must have the same capacity. Operation aborted. */
	"Es ist der Band-Fehler Nr.%\n@aufgetreten.\nVorgang abgebrochen.",/* Tape error #% @has occurred. Operation aborted. */
	"Kann das Schutzbit f�r @ nicht setzen.\nNochmal versuchen?",/* Unable to set protection flags for @. Try again? */
	"Es existiert bereits eine Datei mit dem\nNamen des Backups. Wollen Sie diese\nersetzen?",/* Backup destination file already exists. Do you wish to replace it? */
	"\nKann das Band nicht l�schen.",/* Unable to erase tape. */
	"Achtung: AmigaDOS-Partition gefunden.\nDr�cken Sie \"OK\", um trotzdem\nweiterzumachen.",/* Warning: AmigaDOS partition was found.\nSelect \"OK\" to proceed anyway." */
	"Bitte legen Sie Band Nr.% in @ ein.",/* Please insert tape #% into the @. */
	"Bitte legen Sie Diskette Nr.% in @ ein.",/* Please insert disk #% into @. */
	"Das Band in @ ist ein\nDatensicherungsband. Bitte legen Sie\nein anderes Band ein.",/* Tape in @ is a backup tape. Insert a different tape. */
	"Die Diskette in @ ist eine\nDatensicherungsdiskette. Bitte legen Sie\neine andere Diskette ein.",/* Disk in @ is a backup disk. Insert a different disk. */
	"Sind Sie sicher, da� Sie diesen Vorgang\n@ wirklich abbrechen wollen ?",/* Are you sure it is OK to abort the @? */
	"Dieser Pfadname ist zu lang.\nBitte benutzen Sie einen k�rzeren\nPfadnamen.",/* This pathname is too long.\nPlease use a shorter name. */
	"Konnte Sitzungsprotokoll nicht\nabspeichern.",/* Unable to save session log file. */
	"Achtung: Dieses Inhaltsverzeichnis ist\nbesch�digt. Wollen Sie trotzdem\nweitermachen?",/* Warning: This appears to be a non-removable tape drive. Proceed anyway? */
	"Dieser Block ist nicht lesbar. Dr�cken\nSie \"OK\", um es nochmal zu versuchen, oder\n\"�berspringen\", um fortzufahren.",/* This block is unreadable. Hit \"OK\"\nto retry, or \"Skip\" to continue. */
	"Dieser Block ist nicht lesbar. Dr�cken\nSie \"OK\", um mit dem Lesen des Bandes\nfortzufahren.",/* This block is unreadable. Hit \"OK\"\nto try again. */
	"Achtung: Dieses Inhaltsverzeichnis ist\nbesch�digt. Wollen Sie trotzdem\nweitermachen?",/* Warning: This catalog is corrupt.\nDo you wish to continue? */
	"Sind Sie sicher, da� Sie die jetzigen\nEinstellungen als Voreinstellungen\nabspeichern wollen?",/* Are you sure you want to save current settings as default values? */
	"Zuwenig freier Speicher. Bitte\nreduzieren Sie die Puffergr��e und\nversuchen Sie es nochmal.",/* Insufficient memory. Please reduce the buffer size and try again. */
	"Der AmigaDOS-Dateiname ist ein\nVerzeichnisname. Bitte bennenen Sie die\nDatei um und versuchen es nochmal.",/* AmigaDOS file name is actually a drawer.\nPlease rename and try again. */
	"Tut mir leid, der Pfadname ist zu lang.\nVorgang abgebrochen.",/* Sorry, the pathname is too long. Operation aborted. */
	"Die Dateinsicherung scheint besch�digt\nzu sein.\nWollen Sie trotzdem weitermachen?",/* Backup appears to be corrupt.\nDo you wish to continue anyway? */
	"Konnte die Daten auf @ nicht\ndekomprimieren. Soll ich versuchen,\ndie n�chste Datei zu finden?",/* Could not decompress data on @. Attempt to continue? */
	"Der Dateiname in der Datensicherung\nstimmt nicht mit dem Namen im\nInhaltsverzeichnis �berein. Soll ich den\nNamen im Inhaltsverzeichnis?",/* Filename in backup does not match entry in catalog. Proceed anyway? */
	"Konnte das Inhaltsverzeichnis von dieser\nDatensicherung nicht lesen.",/* Unable to read catalog from this backup. */
	"Konnte Laufwerk @ nicht finden. Bitte\n�berpr�fen Sie ob dieses Laufwerk auch\nwirklich angemeldet ist.",/* Drive @ not found. Please check to see if it has been mounted. */
	"Beim Vorw�rtsbewegen des Bandes ist ein\nFehler aufgetreten. M�glicherweise\nbefinden keine weiteren Daten auf diesem\nBand.",/* Error while spacing tape forward. There may be no more data on this tape." */
	"\nAREXX ist nicht gestartet.",/* AREXX not present. */
	"\nUng�ltiger Makroname.",/* Improper macro name. */
	"Die Ausf�hrung des Makros ist\nfehlgeschlagen."/* Macro execution failed. */
};

TextPtr strDriveType[] = {
	"Direktzugriff",	/*Direct-access*/
	"Sequentiell", 	/*Sequential*/
	"Drucker",		/*Printer*/
	"Rechner",		/*Processor*/
	"WORM",			/*WORM*/
	"CD-ROM",		/*CD-ROM*/
	"Scanner",		/*Scanner*/
	"Optisch",		/*Optical*/
	"Wechsler",		/*Changer*/
	"Schnittstelle"	/*Comm*/
};

TextPtr	aboutStrings[] = {
	" Von Glen Merriman ",
	" und James Bayless "
};

#elif FRENCH

TextPtr initErrors[] = {
	"Probl�mes avec le syst�me",		/* "Bad system state" */
	"Icon.library introuvable",		/* "Can't find icon.library" */
	"Diskfont.library introuvable",	/* "Can't find diskfont.library" */
	"Impossible d'ouvrir l'�cran",	/* "Can't open screen" */
	"M�moire insuffisante",				/* "Not enough memory" */
	"Quarterback tourne d�j� !",		/* "Quarterback already running!" */
	"Impossible d'ouvrir la fen�tre"	/* "Can't open window" */
};

TextPtr strsErrors[] = {
	"Erreur interne inconnue.",			/* "Unknown internal error." */
	"M�moire insuffisante.",				/* "Not enough memory." */
	"Impossible d'ouvrir le fichier.",	/* "Unable to open file." */
	"Impossible de sauver le fichier.",	/* "Unable to save file." */
	"Impression impossible.",				/* "Unable to print." */
	"Nombre de copies incorrect.",		/* "Improper number of copies." */
	"Page inexistante.",						/* "No such page." */
	"Taille de page incorrecte.",			/* "Improper page size." */
	"Hauteur de page incorrecte.",		/* "Improper page height." */
	"Impossible de sauvegarder le fichier\n\"@\".", /* "Unable to backup file\n\"@\"." */
	"Impossible de restaurer le fichier\n\"@\".", /* "Unable to restore file\n\"@\"." */
	"Impossible d'ouvrir le tiroir.",	/* "Unable to open drawer." */
	"Impossible de cr�er le tiroir\n\"@\".", /* "Unable to create drawer\n\"@\"." */
	"Nom de fichier incorrect:\n\"@\".",/* "Invalid file name\n\"@\"." */
	"Nom de tiroir incorrect:\n\"@\".", /* "Invalid drawer name\n\"@\"." */
	"Erreur DOS: %",							/* "DOS error: %" */
	"Le disque dans @ est prot�g� en �criture. \"OK\" pour r�essayer.", /* "Disk in @ is write-protected. Select \"OK\" to try again." */
	"Erreur d'�criture sur @. Ins�rez un autre disque et \"OK\" pour r�essayer.", /* "Error writing to @. To try again, insert another disk and hit \"OK\"." */
	"Le disque dans @ est inutilisable. Essayez un autre disque.", /* "Disk in @ is unusable. Try a different disk." */
	"Mauvais disque. Op�ration abandonn�e.", /* "Disk is bad. Operation aborted." */
	"La compression ne sera pas utilis�e car la m�moire est insuffisante.", /* "Compression will be disabled due to insufficient memory." */
	"Pas de fichier marqu�. Impossible de continuer.", /* "No files have been tagged.\nUnable to proceed." */
	"Attention: le disque dans @ est un volume AmigaDOS. Continuer ?", /* "Warning: Disk in @ is an AmigaDOS volume. Proceed anyway?" */
	"Impossible de lire le catalogue. R�essayer avec le catalogue secondaire ?", /* "Unable to read catalog. Try again with the alternate catalog?" */
	"Impossible de lire sur @. \"OK\" pour r�essayer, ou \"Suivant\".", /* "Unable to read from @. Hit \"OK\" to retry, \"Skip\" to skip disk." */
	"@ ne contient pas un disque de sauvegarde. \"OK\" pour r�essayer, ou \"Suivant\".", /* "Not a backup disk in @. Hit \"OK\" to retry, \"Skip\" to skip disk." */
	"Le disque % dans @ n'est pas le bon. Remplacez-le par le bon disque.", /* "Disk % in @ is not the needed disk. Please replace with disk ~." */
	"Le disque dans @ n'appartient pas � ce jeu de sauvegarde. Essayez un autre disque.", /* "Disk in @ does not belong to the same backup set. Try another disk." */
	"D�sol�, mais sans la bonne cl�, l'acc�s en lecture est refus�.", /* "Sorry, without the correct password, read access is denied." */
	"Ins�rez le disque % du jeu de sauvegarde � restaurer dans @.", /* "Load disk % from the backup set you wish to restore into @." */
	"Ins�rez le DERNIER disque de ce jeu de sauvegarde dans @.", /* "Load the LAST disk from this backup set into @." */
	"Le disque est plein. Op�ration abandonn�e.", /* "Disk is full. Operation aborted." */
	"Les donn�es compress�es de @ sont endommag�es. Impossible de restaurer le fichier.", /* "Compressed data on @ is corrupt. Cannot restore file." */
	"Toutes les marques vont dispara�tre. Etes-vous d'accord ?", /* "All tags you have made will be lost.\nIs this OK?" */
	"Aucun lecteur de disquettes n'est actif. Il faut en s�lectionner au moins un.", /* "No floppy drives are enabled.\nAt least one must be selected." */
	"Les sauvegardes sur disques non-amovibles doivent �tre des fichiers AmigaDOS.", /* "Backups to non-removable media must be written as AmigaDOS files." */
	"Voulez-vous vraiment remettre les options de Quarterback � leur valeur par d�faut ?", /* "Are you sure you want to reset all Quarterback options?" */
	"Erreur de v�rification sur @. \"OK\" pour r��crire, \"Suivant\" pour ignorer.", /* "Verify failure on @. Hit \"OK\" to rewrite disk, \"Skip\" to ignore." */
	"La date indiqu�e est incorrecte. Recommencez, SVP.", /* "Invalid date entry. Please try again." */
	"Le lecteur de bandes est occup�. Attendez ou \"OK\" pour abandonner.", /* "The tape drive is busy. Please wait, or hit \"OK\" to abort." */
	"Impossible d'ouvrir le device \"@\", unit� %.", /* "Unable to open device \"@\", unit %." */
	"La bande est prot�g�e en �criture. \"OK\" pour r�essayer.", /* "Tape is write-protected.\nSelect \"OK\" to try again." */
	"L'effacement va d�truire toutes les donn�es de la bande. Continuer ?", /* "Erasing will destroy all information on the tape. Continue anyway?" */
	"Retendre une bande peut prendre plusieurs minutes. Continuer ?", /* "Re-tensioning a tape may take several minutes. Continue anyway?" */
	"Cette op�ration ne peut pas �tre r�alis�e sur un disque non-amovible.", /* "This operation cannot be performed on non-removable media." */
	"Unit� introuvable",						/* "Device not found." */
	"Impossible de lire le catalogue sur la bande. \"OK\" pour r�essayer.", /* "Unable to read catalog from tape. Select \"OK\" to try again." */
	"Cette bande ne contient pas une sauvegarde Quarterback.", /* "This tape does not contain a valid Quarterback backup." */
	"La bande % n'est pas la bonne. Remplacez-la par la bonne bande.", /* "Tape % is not the needed tape. Replace with correct tape." */
	"Cette bande n'appartient pas � ce jeu de sauvegarde. Essayez une autre bande.", /* "This tape does not belong to the same backup set. Try another tape." */
	"Tous les lecteurs doivent �tre de m�me capacit�. Op�ration abandonn�e.", /* "All selected drives must have the same capacity. Operation aborted." */
	"Erreur #% @de la bande. Op�ration abandonn�e.", /* "Tape error #% @has occurred. Operation aborted." */
	"Impossible de modifier les bits de protection de \"@\". R�essayer ?", /* "Unable to set protection flags for @. Try again?" */
	"Le fichier de sauvegarde existe d�j�. Voulez-vous le remplacer ?", /* "Backup destination file already exists. Do you wish to replace it?" */
	"Impossible d'effacer la bande.",	/* "Unable to erase tape." */
	"Attention : une partition AmigaDOS a �t� trouv�e. \"OK\" pour continuer.", /* "Warning: AmigaDOS partition was found.\nSelect \"OK\" to proceed anyway." */
	"Ins�rez la bande n�% dans le @, SVP.", /* "Please insert tape #% into the @." */
	"Ins�rez le disque n�% dans @, SVP.", /* "Please insert disk #% into @." */
	"La bande dans @ contient une sauvegarde. Essayez une autre bande.", /* "Tape in @ is a backup tape. Insert a different tape." */
	"Le disque dans @ contient une sauvegarde. Essayez un autre disque.", /* "Disk in @ is a backup disk. Insert a different disk." */
	"Voulez-vous vraiment abandonner la @ ?", /* "Are you sure it is OK to abort the @?" */
	"Ce chemin d'acc�s est trop long. Utilisez un nom plus court, SVP.", /* "This pathname is too long.\nPlease use a shorter name." */
	"Impossible de sauvegarder le fichier rapport.", /* "Unable to save session log file." */
	"Attention: ceci semble �tre un lecteur de bandes non-amovibles.  Continuer ?", /* "Warning: This appears to be a non-removable tape drive. Proceed anyway?" */
	"Ce bloc est illisible. \"OK\" pour r�essayer, \"Suivant\" pour continuer.", /* "This block is unreadable. Hit \"OK\"\nto retry, or \"Skip\" to continue." */
	"Ce bloc est illisible. \"OK\" pour r�essayer.", /* "This block is unreadable. Hit \"OK\"\nto try again." */
	"Attention: le catalogue est endommag�. Voulez-vous continuer ?", /* "Warning: This catalog is corrupt.\nDo you wish to continue?" */
	"Voulez-vous vraiment sauver vos r�glages comme valeurs par d�faut ?", /* "Are you sure you want to save current settings as default values?" */
	"M�moire insuffisante. R�duisez la taille du buffer et r�essayez.", /* "Insufficient memory. Please reduce the buffer size and try again." */
	"Ce nom de fichier correspond � un tiroir. Renommez-le et r�essayez.", /* "AmigaDOS file name is actually a drawer.\nPlease rename and try again." */
	"Le chemin d'acc�s est trop long. Op�ration abandonn�e.", /* "Sorry, the pathname is too long. Operation aborted." */
	"La sauvegarde semble endommag�e. Voulez-vous continuer ?", /* "Backup appears to be corrupt.\nDo you wish to continue anyway?" */
	"Impossible de d�compresser les donn�es de @. Faut-il r�essayer ?", /* "Could not decompress data on @. Attempt to continue?" */
	"Ce nom de fichier sauvegard� ne figure pas dans le catalogue. Continuer ?", /* "Filename in backup does not match entry in catalog. Proceed anyway?" */
	"Impossible de lire le catalogue de cette sauvegarde.", /* "Unable to read catalog from this backup." */
	"Unit� @ introuvable. V�rifiez qu'elle a bien �t� mont�e.", /* "Drive @ not found. Please check to see if it has been mounted." */
	"Erreur pendant l'avancement de la bande. Il n'y a peut-�tre plus de donn�es sur cette bande.", /* "Error while spacing tape forward. There may be no more data on this tape." */
	"AREXX n'est pas pr�sent.",			/* "AREXX not present." */
	"Nom de macro incorrect.",				/* "Improper macro name." */
	"Erreur dans l'ex�cution de la macro.", /* "Macro execution failed." */
};

TextPtr strDriveType[] = {
	"Acc�s direct",	/* "Direct-access" */
	"S�quentiel",	/* "Sequential" */
	"Imprimante",	/* "Printer" */
	"Processeur",	/* "Processor" */
	"WORM",			/* "WORM" */
	"CD-ROM",		/* "CD-ROM" */
	"Scanner",		/* "Scanner" */
	"Optique",		/* "Optical" */
	"Changer",		/* "Changer" */
	"Comm",			/* "Comm" */
};

TextPtr	aboutStrings[] = {
	" Par Glen Merriman ",
	" and James Bayless "
};

#elif SWEDISH
#endif


/*
 * PART TWO: Miscellaneous strings
 * First, the language-independent strings.
 */

TextChar strPRT[]		= "PRT:";

TextChar strscsi[]		= "scsi";
TextChar strSCSI[]		= "SCSI";
TextChar strTrackdisk[]	= "trackdisk.device";
TextChar strSCSIDevice[]= "scsi.device";
TextChar strA2090Dev1[]	= "hddisk.device";
TextChar strA2090Dev2[]	= "iddisk.device"; 

TextChar strScreenName[]	= "Quarterback";
TextChar strProgName[] 		= "Quarterback";
TextChar strPortName[]		= "Quarterback Port";
TextChar strPrefsName[]		= "QB Defaults";
TextChar strFKeysName[]		= "QB FKeys";
TextChar strAutoExecName[]	= "QB Startup";
TextChar strAppIconName[]	= "QB Deposit";
TextChar strDefaultDir[]	= "QB:";
TextChar strSessionTitle[]	= "QB ";
TextChar strCommentBegin[]	= "* ";
TextChar macroDrawerName[]	= "Macros";

TextChar strLF[]			= "\n";
TextChar strSpace[]			= " ";

/*
 * Now the language-dependent strings.
 */
 
#if (AMERICAN | BRITISH)
TextChar strDescLimbo[]		= "Select volume to\nbackup or restore.";
TextChar strDescLimboDir[]	= "Select drawer to\nbackup or restore.";
TextChar strDescOpScan[]	= "Please wait while\n@ is scanned.";
TextChar strDescOpSel[]		= "Now tag all desired\nfiles to @.";
TextChar strDescOpPrep[]	= "Click \"Start\" to\nbegin the @.";
TextChar strDescOpProg[]	= "@ in progress.";
TextChar strDescOpPause[]	= "@ paused.";
TextChar strDescOpDone[]	= "@ complete.";

TextChar strFileNotFound[]	= "File not found.";
TextChar strDiskLocked[]	= "Disk is locked.";
TextChar strFileNoDelete[]	= "File is delete-protected.";
TextChar strFileNoRead[]	= "File is read-protected.";

TextChar strDemo[]			= "Demo version.";
TextChar strBadFile[]		= "Bad file contents.";

TextChar strBackupFilter[]	= "QB Backup Filter";
TextChar strRestoreFilter[]	= "QB Restore Filter";
TextChar strCompFilter[]	= "QB Compression Filter";
TextChar strDefCatName[]	= "QB Catalog";
TextChar strDefLogName[]	= "QB Session Log";

TextChar strSession[]		= " session";
TextChar strSessions[]		= " sessions";
TextChar strAt[]			= " at ";
TextChar strOpBegin[]		= " started ";
TextChar strOpAborted[]		= " aborted ";
TextChar strOpCompleted[]	= " finished ";

TextChar strInUseErr[]		= " is in use and could not be opened";
TextChar strExistsErr[]		= " already exists";
TextChar strDelProtErr[]	= " is delete-protected";
TextChar strWriteProtErr[]	= " is write-protected";
TextChar strReadProtErr[]	= " is read-protected";
TextChar strNotFoundErr[]	= " not found";
TextChar strDiskFullErr[]	= " failed, disk full";
TextChar strGenIOErr[]		= " failed, error = %";
TextChar strBackProtErr[]	= " backed up, archive flag not set";
TextChar strRestProtErr[]	= " restored, protection flags not set";
TextChar strReadErr[]		= " could not be read";
TextChar strWriteErr[]		= " could not be written";
TextChar strCreateErr[]		= " could not be created";
TextChar strSizeDiff[]		= " is different length";
TextChar strCmpDiff[]		= " has different contents";
TextChar strNotLinkDiff[]	= " not a link on source";
TextChar strIncomplErr[]	= " was not completely backed up";
TextChar strSkippedErr[]	= " was skipped";
TextChar strPartReadErr[]	= " shorter than expected";
TextChar strDecompFail[]	= " could not be decompressed";

TextChar strQuit[]			= "Quit";
TextChar strComma[]			= ", ";
TextChar strQuote[]			= "\"";
TextChar strQuestionMark[]	= "?";
TextChar strColon[]			= ":";
TextChar strPeriod[]		= ".";

TextChar strScanVol[]	= "Scanning volume ";
TextChar strScanDir[]	= "Scanning drawer ";
TextChar strScanDrive[] = "Reading catalog from ";
TextChar strScanFile[] 	= "File: ";
TextChar	strDrawer[]		= "drawer";
TextChar strVolume[]		= "volume";
TextChar strCatalog[]	= "catalog";

TextChar strDone[]		= "Done";
TextChar strStart[]		= "Start";
TextChar strSkip[]		= "Skip";
TextChar strAbort[]		= "Abort";
TextChar strPause[]		= "Pause";
TextChar strResume[]	= "Resume";

TextChar strOK[]		= "OK";
TextChar strProceed[]	= "Proceed";
TextChar strCancel[]	= "Cancel";
TextChar strSave[]		= "Save";
TextChar strPrint[]		= "Print";

TextChar strTapeDrive[]	= "tape drive";
TextChar strRemovable[]	= "removable drive";

TextChar strBackup[]	= "Backup";
TextChar strRestore[]	= "Restore";
TextChar strCompare[]	= "Compare";
TextChar strTest[]		= "Test";
TextChar strScan[]		= "Scan";
TextChar strBackupL[]	= "backup";
TextChar strRestoreL[]	= "restore";
TextChar strCompareL[]	= "compare";
TextChar strTestL[]		= "test";
TextChar strScanL[]		= "scan";

TextChar strTag[]		= "Tag";
TextChar strTagAll[]	= "Tag All";
TextChar strUntag[]		= "Untag";
TextChar strUntagAll[]	= "Untag All";
TextChar strBack[]		= "Back";
TextChar strRoot[]		= "Root";
TextChar strEnter[]		= "Enter";
TextChar strDisks[]		= "Disks";

TextChar strSaveADOS[]	= "Write backup as file:";
TextChar strOpenADOS[]	= "Read backup from file:";
TextChar strSaveLog[]	= "Save session log as:";
TextChar strSaveCat[]	= "Save catalog as:";

TextChar strWrongPass[]	= "Incorrect password. Try again.";

TextChar strArchive0[]	= "Files without Archive flag";
TextChar strArchive1[]	= "Files with Archive flag";

TextChar strRenameFile[]= "File \"@\" already exists.";
TextChar strRename[]	= "Rename";
TextChar strReplace[]	= "Replace";
TextChar strOpenFilter[]= "Choose a filter file:";
TextChar strSaveFilter[]= "Save filter as:";

TextChar strTpDrVendor[]= "Vendor: ";
TextChar strTpDrModel[]	= "Model: ";
TextChar strTpDrRev[]	= "Revision: ";
TextChar strTpDrType[]	= "Device type: ";
TextChar strTpDrBlkSz[] = "Block size:";
TextChar strTpDrMedSz[]	= "Media size:";
TextChar strUnknown[]	= "Unknown";
TextChar strTpDrDATape[]= "Direct-acc tape";
TextChar strTpDrVar[]	= "Variable";
TextChar strTpDrFixed[]	= "Fixed";
TextChar strM[]			= "M";

TextChar strRewinding[]	= "Rewinding tape...";
TextChar strErase[]		= "Erasing tape...";
TextChar strRetension[]	= "Re-tensioning tape...";
TextChar strSpacing[]	= "Advancing tape...";
TextChar strFastSpacing[]="Fast-Advancing tape...";
TextChar strFindAltCat[]= "Seeking alternate catalog...";
TextChar strSetArch[]	= "Setting archive flags...";

TextChar strFiles[]		= "Files:";
TextChar strBytes[]		= "Bytes:";
TextChar strTotal[]		= "Total:";
TextChar strSel[]		= "Tagged:";
TextChar strCompleted[]	= "Completed:";

TextChar strSubdir[]	= "Dir ";
TextChar strHardLink[]	= "Hard-Link ";
TextChar strSoftLink[]	= "Soft-Link ";

TextChar strLoadSettings[]		= "Select the settings to use";
TextChar strSaveAsSettings[]	= "Save settings as:";

TextChar strQBLogfile[]	= "Quarterback Session Log, ";
TextChar strQBCatalog[]	= "Quarterback Catalog, ";
TextChar strDirOf[]		= "Directory of ";
TextChar strPage[]		= "Page ";

TextChar strBitLevel[]	= "-bit";

TextChar strPrCat[]		= "Print Catalog...";
TextChar strPrLog[]		= "Print Session Log...";
TextChar strSaveCatAs[]	= "Save Catalog As...";
TextChar strSaveLogAs[]	= "Save Session Log As...";

TextChar strDevNotSel[]	= "Not selected";
TextChar strDevNAvail[]	= "Not available";
TextChar strDevNReady[]	= "Insert #%";
TextChar strDevReady[]	= "Ready";
TextChar strDevCheck[]	= "Checking #%";
TextChar strDevActiveB[]= "Writing #%";
TextChar strDevActiveR[]= "Reading #%";
TextChar strDevDone[]	= "Done";
TextChar strDevPaused[]	= "Paused";
TextChar strDevRemove[]	= "Remove #%";
TextChar strDevCheckF[] = "Checking file";
TextChar strDevActiveBF[]="Writing file";
TextChar strDevActiveRF[]="Reading file";
TextChar strSessionNum[]= "Session #";

TextChar strPreparing[]	= "* Ready to @ ";
TextChar strEstNum[]	= "* Number of disks required: ";
TextChar strNA[]		= "?";
TextChar strBackupOf[]	= "Backup of ";
TextChar strBackupOld[]	= "Backup from an older version of Quarterback";
TextChar strBackupCr[]	= "\nCreated on ";
TextChar strCompEff[]	= "* Compression reduction: ";
TextChar strCompEff2[]	= "%";
TextChar strCompOff[]	= "* Compression off";
TextChar strRate[]		= "* @ rate: ";
TextChar strRate2[]		= "@ megabytes per minute";
TextChar strNumErrors[]	= "* Number of errors: %";

TextChar strDevNotFnd[]	= "Device not found";

#elif GERMAN

TextChar strDescLimbo[]		= "Laufw. zum Sichern\nEinlesen w�hlen.";
TextChar strDescLimboDir[]	= "Verz. zum Sichern\nEinlesen w�hlen.";
TextChar strDescOpScan[]	= "Warten Sie, durch-\nsuche @.";
TextChar strDescOpSel[]		= "Alle Dateien zum\n@ markieren.";
TextChar strDescOpPrep[]	= "\"Start\" dr�ken zum\nBeginn des @.";
TextChar strDescOpProg[]	= "@ l�uft.";
TextChar strDescOpPause[]	= "@\nangehalten.";
TextChar strDescOpDone[]	= "@ beendet.";

TextChar strFileNotFound[] = "Datei nicht gefunden.";
TextChar strDiskLocked[]	= "Datentr�ger ist schreibgesch�tzt";
TextChar strFileNoDelete[]	= "Die Datei ist vor L�schen gesch�tzt";
TextChar strFileNoRead[]	= "Die Datei ist vor Lesen gesch�tzt.";

TextChar strDemo[]			= "Vorf�hr-Version.";
TextChar strBadFile[]		= "Fehlerhafter Dateiinhalt.";

TextChar	strBackupFilter[]	= "QB Backup Filter";
TextChar strRestoreFilter[]= "QB Restore Filter";
TextChar strDefCatName[]	= "QB Catalog";
TextChar strDefLogName[]	= "QB Session Log";
TextChar strSession[]		= " Sitzung - ";
TextChar strAt[]			= " am ";
TextChar strOpBegin[]		= " begonnen ";
TextChar strOpAborted[]		= " abgebrochen ";
TextChar strOpCompleted[]	= " Beendet ";

TextChar strInUseErr[]		= " ist in Gebrauch und konnte nicht ge�ffnet werden";
TextChar strExistsErr[]		= " existiert bereits";
TextChar strDelProtErr[]	= " ist l�schgesch�tzt";
TextChar strWriteProtErr[]	= " ist schreibgsch�tzt";
TextChar strReadProtErr[]	= " ist vor Lesen gesch�tzt";
TextChar strNotFoundErr[]	= " nicht gefunden";
TextChar strDiskFullErr[]	= " nicht geklappt, Diskette ist voll";
TextChar strGenIOErr[]		= " nicht geklappt, Fehler = %";
TextChar strBackProtErr[]	= " gesichert, Archivierungsbit nicht gesetzt";
TextChar strRestProtErr[]	= " wiederhergestellt, Schutzbits nicht gesetzt";
TextChar strReadErr[]		= " konnte nicht gelesen werden";
TextChar strWriteErr[]		= " konnte nicht geschrieben werden";
TextChar strCreateErr[]		= " konnte nicht erzeugt werden";
TextChar strSizeDiff[]		= " hat eine andere L�nge";
TextChar strCmpDiff[]		= " hat einen anderen Inhalt";
TextChar	strNotLinkDiff[]	= " ist auf dem Quell-Laufwerk kein Link";
TextChar strIncomplErr[]	= " konnte nicht erfolgreich gesichert werden";
TextChar strSkippedErr[]	= " wurde �bersprungen";

TextChar strQuit[]			= "Beenden";
TextChar strEllipsis[]		= "...";
TextChar strComma[]			= ", ";
TextChar strQuote[]			= "\"";
TextChar strQuestionMark[]	= "?";
TextChar strColon[]			= ":";
TextChar strPeriod[]			= ".";

TextChar strScanVol[]	= "Durchsuche Datentr�ger";
TextChar	strScanDir[]	= "Durchsuche Verzeichnis";
TextChar strScanDrive[] = "Lese Inhaltsverzeichnis von";
TextChar strScanFile[] 	= "Datei:";
TextChar	strDrawer[]		= "Verzeichnis";
TextChar strVolume[]		= "Datentr�ger";
TextChar strCatalog[]	= "Inhaltsverzeichnis";

TextChar strDone[]		= "Fertig";
TextChar strStart[]		= "Starten";
TextChar strSkip[]		= "�bergehen";
TextChar strAbort[]		= "Abbrechen";
TextChar strPause[]		= "Unterbrechen";
TextChar strResume[]		= "Weitermachen";

TextChar strOK[]		= "OK";
TextChar strProceed[]	= "Fortfahren";
TextChar strCancel[]	= "Abbr.";
TextChar strSave[]		= "Speichern";
TextChar strPrint[]		= "Drucken";

TextChar strTapeDrive[]	= "Bandlaufwerk";
TextChar strRemovable[]	= "Wechselplattenlaufwerk";
TextChar strBackup[]	= "Sichern";
TextChar strRestore[]	= "Wiederh.";
TextChar strCompare[]	= "Vergleichen";
TextChar strTest[]		= "Testen";
TextChar strBackupL[]	= "sichern";
TextChar strRestoreL[]	= "wiederh.";
TextChar strCompareL[]	= "vergleichen";
TextChar strTestL[]		= "testen";
TextChar strTag[]		= "Markieren";
TextChar strTagAll[]		= "Alles markieren";
TextChar strUntag[]		= "Entfernen"/*"Markierung entfernen"*/;
TextChar strUntagAll[]	= "Alle Markierungen entfernen";
TextChar strBack[]		= "Mutterv.";
TextChar strRoot[]		= "Hauptv.";
TextChar strEnter[]		= "Unterv.";
TextChar strDisks[]		= "Laufwerke";

TextChar strSaveADOS[]	= "Datensicherung in eine\nDatei schreiben:";
TextChar strOpenADOS[]	= "Datensicherung aus einer\nDatei lesen:";
TextChar strSaveLog[]	= "Sitzungsprotokoll\nspeichern als:";
TextChar strSaveCat[]	= "Inhaltsverzeichnis\nspeichern als:";

TextChar strWrongPass[]	= "Falsches Kennwort. Versuchen Sie es nochmal.";

TextChar strArchive0[]	= "Dateien mit nicht gesetztem Archivierungsbit";
TextChar strArchive1[]	= "Dateien mit gesetztem Archivierungsbit";

TextChar	strRenameFile[]= "Die Datei \"@\" ist bereits vorhanden.";
TextChar strRename[]		= "Umbenennen";
TextChar strReplace[]	= "Ersetzen";
TextChar strOpenFilter[]= "W�hlen Sie eine Datei mit\nAuswahlkriterien:";
TextChar	strSaveFilter[]= "Auswahlkriterien\nspeichern unter:";

TextChar strTpDrVendor[]= "Hersteller:";
TextChar strTpDrModel[]	= "Modell:";
TextChar	strTpDrRev[]	= "Version:";
TextChar strTpDrType[]	= "Art des Ger�ts:";
TextChar strTpDrBlkSz[] = "Blockgr��e:";
TextChar strTpDrNonStd[]= "Unbekannt";
TextChar strTpDrDATape[]= "Bandlaufwerk mit direktem Zugriff";
TextChar strTpDrVar[]	= "ver�nderbar";
TextChar strTpDrFixed[]	= "fest";

TextChar strRewinding[]	= "Spule Band zur�ck...";
TextChar strErase[]		= "L�sche Band...";
TextChar strRetension[]	= "Straffe Band...";
TextChar strSpacing[]	= "Bewege Band vorw�rts...";
TextChar strFindAltCat[]= "Suche das alternative Inhaltsverzeichnis...";
TextChar strSetArch[]	= "Setze die Archivierungs-Bits.";

TextChar strFiles[]		= "Dateien:";
TextChar	strBytes[]		= "Bytes:";
TextChar strTotal[]		= "Gesamt:";
TextChar strSel[]		= "Markiert:";
TextChar strCompleted[]	= "Bearbeitet:"; 

TextChar strSubdir[]		= "Verzeichn ";
TextChar strHardLink[]	= "Hard-Link ";
TextChar strSoftLink[]	= "Soft-Link ";

TextChar strLoadSettings[]		= "W�hlen Sie die zu benutzenden Einstell.";/*ungen aus.";*/
TextChar strSaveAsSettings[]	= "Einstell. speichern als:";

TextChar strQBLogfile[]	= "Quarterback Sitzungsprotokoll,";
TextChar strQBCatalog[]	= "Quarterback Inhaltsverzeichnis,";
TextChar strDirOf[]		= "Inhaltsverzeichnis von";
TextChar strPage[]		= "Seite";

TextChar strBitLevel[]	= "-Bit";

TextChar strPrCat[]		= "Inhaltsverzeichnis drucken";
TextChar strPrLog[]		= "Sitzungsprotokoll drucken";
TextChar strSaveCatAs[]	= "Inhaltsverzeichnis speichern als";
TextChar strSaveLogAs[]	= "Sitzungsprotokoll speichern als";

TextChar strDevNotSel[]	= "Nicht ausgew�hlt";
TextChar strDevNAvail[]	= "Nicht verf�gbar";
TextChar strDevNReady[]	= "Nr.% einlegen";
TextChar strDevReady[]	= "Bereit";
TextChar strDevCheck[]	= "�berpr�fe Nr.%";
TextChar strDevActiveB[]= "Schreibe Nr.%";
TextChar strDevActiveR[]= "Lese Nr.%";
TextChar strDevDone[]	= "Fertig";
TextChar strDevPaused[]	= "Angehalten";
TextChar strDevRemove[]	= "Nr.% entfernen";
TextChar strDevCheckF[] = "�berpr�fe Datei";
TextChar strDevActiveBF[]="Schreibe Datei";
TextChar strDevActiveRF[]="Lese die Datei";

TextChar strPreparing[]	= "* Bereit zum @ ";
TextChar strEstNum[]		= "* Ben�tigte Anzahl Disketten: ";
TextChar strNA[]		= "?";
TextChar	strBackupOf[]	= "Datensicherung von ";
TextChar strBackupOld[]	= "Datensicherung von einer �lteren Version von Quarterback";
TextChar strBackupCr[]	= "\nErzeugt am";
TextChar strCompEff[]	= "* Komprimierungsrate:";
TextChar strCompEff2[]	= "%";
TextChar strCompOff[]	= "* Komprimierung aus";
TextChar strRate[]		= "@ rate:";
TextChar strRate2[]		= "@ megabytes per minute";

#elif FRENCH

TextChar strDescLimbo[]		= "Choisissez le volume �\nsauvegarder ou � restaurer.";
TextChar strDescLimboDir[]	= "Choisissez le tiroir �\nsauvegarder ou � restaurer.";
TextChar strDescOpScan[]	= "Attendez pendant la\nlecture de @.";
TextChar strDescOpSel[]		= "Marquez les fichiers\npour la @.";
TextChar strDescOpPrep[]	= "Cliquez sur \"D�bute\" pour\ncommencer la @.";
TextChar strDescOpProg[]	= "@ en cours.";
TextChar strDescOpPause[]	= "@ arr�t�e.";
TextChar strDescOpDone[]	= "@ termin�e.";

TextChar strFileNotFound[]	= "Fichier introuvable.";
TextChar strDiskLocked[]	= "Disque verrouill�.";
TextChar strFileNoDelete[]	= "Fichier prot�g� contre l'effacement.";
TextChar strFileNoRead[]	= "Fichier prot�g� contre la lecture.";

TextChar strDemo[]			= "Version de d�mo.";
TextChar strBadFile[]		= "Contenu du fichier incorrect.";

TextChar strBackupFilter[]	= "Filtre Sauvegarde QB";
TextChar strRestoreFilter[]= "Filtre Restauration QB";
TextChar strDefCatName[]	= "Catalogue QB";
TextChar strDefLogName[]	= "Rapport QB";
TextChar strSession[]		= " session - ";
TextChar strAt[]			= " � ";
TextChar strOpBegin[]		= " commenc�e le ";
TextChar strOpAborted[]		= " abandonn�e le ";
TextChar strOpCompleted[]	= " termin�e le ";

TextChar strInUseErr[]		= " est utilis� et n'a pas pu �tre ouvert";
TextChar strExistsErr[]		= " existe d�j�";
TextChar strDelProtErr[]	= " est prot�g� contre l'effacement";
TextChar strWriteProtErr[]	= " est prot�g� contre l'�criture";
TextChar strReadProtErr[]	= " est prot�g� contre la lecture";
TextChar strNotFoundErr[]	= " n'existe pas" /* " not found";
TextChar strDiskFullErr[]	= " a �chou�, disque plein";
TextChar strGenIOErr[]		= " a �chou�, erreur = %";
TextChar strBackProtErr[]	= " sauvegard�, bit archive non actif";
TextChar strRestProtErr[]	= " restaur�, bits de protection non actifs";
TextChar strReadErr[]		= " n'a pas pu �tre lu";
TextChar strWriteErr[]		= " n'a pas pu �tre �crit";
TextChar strCreateErr[]		= " n'a pas pu �tre cr��";
TextChar strSizeDiff[]		= " n'a pas la m�me taille";
TextChar strCmpDiff[]		= " n'a pas le m�me contenu";
TextChar strNotLinkDiff[]	= " n'a pas de lien sur la source";
TextChar strIncomplErr[]	= " n'a pas �t� sauvegard� correctement";
TextChar strSkippedErr[]	= " a �t� omis";

TextChar strQuit[]			= "Quitter";
TextChar strEllipsis[]		= "...";
TextChar strComma[]			= ", ";
TextChar strQuote[]			= "\""
TextChar strQuestionMark[]	= "?"
TextChar strColon[]			= ":";
TextChar strPeriod[]			= ".";

TextChar strScanVol[]		= "Lecture du volume ";
TextChar strScanDir[]		= "Lecture du tiroir ";
TextChar strScanDrive[]		= "Lecture du catalogue de ";
TextChar strScanFile[] 		= "Fichier: ";
TextChar strDrawer[]			= "tiroir";
TextChar strVolume[]			= "volume";
TextChar strCatalog[]		= "catalogue";

TextChar strDone[]		= "Quitte";
TextChar strStart[]		= "D�bute";
TextChar strSkip[]		= "Suivant";
TextChar strAbort[]		= "Abandon";
TextChar strPause[]		= "Pause";
TextChar strResume[]		= "Continue";

TextChar strOK[]		= "OK";
TextChar strProceed[]	= "Continue";
TextChar strCancel[]	= "Annule";
TextChar strSave[]		= "Sauve";
TextChar strPrint[]		= "Imprime";

TextChar strTapeDrive[]	= "lecteur de bandes";
TextChar strRemovable[]	= "unit� amovible";
TextChar strBackup[]	= "Sauvegarde";
TextChar strRestore[]	= "Restauration";
TextChar strCompare[]	= "Comparaison";
TextChar strTest[]		= "V�rification";
TextChar strBackupL[]	= "sauvegarde";
TextChar strRestoreL[]	= "restauration";
TextChar strCompareL[]	= "comparaison";
TextChar strTestL[]		= "v�rification";
TextChar strTag[]		= "Marque";
TextChar strTagAll[]	= "Marque tout";
TextChar strUntag[]		= "Enl�ve";
TextChar strUntagAll[]	= "Enl�ve tout";
TextChar strBack[]		= "Retour";
TextChar strRoot[]		= "Racine";
TextChar strEnter[]		= "Entre";
TextChar strDisks[]		= "Volumes";

TextChar strSaveADOS[]	= "Sauvegarder sous fichier:";
TextChar strOpenADOS[]	= "Restaurer depuis fichier:";
TextChar strSaveLog[]	= "Sauver rapport sous:";
TextChar strSaveCat[]	= "Sauver catalogue sous:";

TextChar strWrongPass[]	= "Cl� incorrecte. R�essayez.";

TextChar strArchive0[]	= "Fichiers avec bit archive non actif";
TextChar strArchive1[]	= "Fichiers avec bit archive actif";

TextChar strRenameFile[]= "Le fichier \"@\" existe d�j�.";
TextChar strRename[]		= "Renomme";
TextChar strReplace[]	= "Remplace";
TextChar strOpenFilter[]= "Choisir fichier filtre:";
TextChar strSaveFilter[]= "Sauver filtre sous:";

TextChar strTpDrVendor[]= "Marque:";
TextChar strTpDrModel[]	= "Mod�le:";
TextChar strTpDrRev[]	= "R�vision:";
TextChar strTpDrType[]	= "Type unit�:";
TextChar strTpDrBlkSz[] = "Taille bloc:";
TextChar strTpDrNonStd[]= "Inconnue";
TextChar strTpDrDATape[]= "Bande � acc�s direct";
TextChar strTpDrVar[]	= "Variable";
TextChar strTpDrFixed[]	= "Fixe";

TextChar strRewinding[]	= "Rembobinage de la bande...";
TextChar strErase[]		= "Effacement de la bande...";
TextChar strRetension[]	= "Tension de la bande...";
TextChar strSpacing[]	= "Avancement de la bande...";
TextChar strFindAltCat[]= "Recherche du catalogue secondaire...";
TextChar strSetArch[]	= "Mise en place des bits archives...";

TextChar strFiles[]		= "Fichiers:";
TextChar strBytes[]		= "Octets:";
TextChar strTotal[]		= "Total:";
TextChar strSel[]		= "Marqu�s:";
TextChar strCompleted[]	= "Trait�s:";

TextChar strSubdir[]		= "R�p ";
TextChar strHardLink[]	= "Hard-Link ";
TextChar strSoftLink[]	= "Soft-Link ";

TextChar strLoadSettings[]		= "Choisir les r�glages � utiliser";
TextChar strSaveAsSettings[]	= "Sauver r�glages sous:";

TextChar strQBLogfile[]	= "Rapport de Quarterback, ";
TextChar strQBCatalog[]	= "Catalogue de Quarterback, ";
TextChar strDirOf[]		= "Contenu de ";
TextChar strPage[]		= "Page ";

TextChar strBitLevel[]	= "-bits";

TextChar strPrCat[]		= "Imprimer catalogue...";
TextChar strPrLog[]		= "Imprimer rapport...";
TextChar strSaveCatAs[]	= "Sauver catalogue sous...";
TextChar strSaveLogAs[]	= "Sauver rapport sous...";

TextChar strDevNotSel[]	= "Non s�lectionn�";
TextChar strDevNAvail[]	= "Non disponible";
TextChar strDevNReady[]	= "Ins�rez n�%";
TextChar strDevReady[]	= "Pr�t";
TextChar strDevCheck[]	= "V�rifie n�%";
TextChar strDevActiveB[]= "Ecrit n�%";
TextChar strDevActiveR[]= "Lit n�%";
TextChar strDevDone[]	= "Termin�";
TextChar strDevPaused[]	= "Pause";
TextChar strDevRemove[]	= "Enlevez n�%";
TextChar strDevCheckF[] = "V�rifie fichier";
TextChar strDevActiveBF[]="Ecrit fichier";
TextChar strDevActiveRF[]="Lit fichier";

TextChar strPreparing[]	= "* Pr�t pour la @ ";
TextChar strEstNum[]	= "* Nombre de disques n�cessaires: ";
TextChar strNA[]		= "?";
TextChar strBackupOf[]	= "Sauvegarde de ";
TextChar strBackupOld[]	= "Sauvegard� avec une ancienne version de Quarterback";
TextChar strBackupCr[]	= "\nCr�� le ";
TextChar strCompEff[]	= "* Taux de compression: ";
TextChar strCompEff2[]	= "%";
TextChar strCompOff[]	= "* Pas de compression";
TextChar strRate[]		= "* Vitesse de @: ";
TextChar strRate2[]		= "@ m�ga-octets par minute";

#elif SWEDISH
#endif

/*
 *	Macro strings
 */

#if (GERMAN | SWEDISH)
TextChar	strMacroNames2[10][32] = {
	"Makro_1", "Makro_2", "Makro_3", "Makro_4", "Makro_5",
	"Makro_6", "Makro_7", "Makro_8", "Makro_9", "Makro_10"
};

#endif
