/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Command parser
 */

#include <exec/types.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/intuition.h>

#include <rexx/errors.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/DOS.h>

#include <string.h>

#include "QB.h"
#include "Proto.h"
#include "Version.h"

/*
 * External variables
 */
 
extern MsgPortPtr	mainMsgPort;

extern ScreenPtr	screen;

extern WindowPtr	cmdWindow;

extern TextChar	strProgName[], strBuff[], strPeriod[];
extern UBYTE		mode, operation;

extern BOOL			force;
extern DlgTemplPtr	dlgList[];
extern Ptr			fib;
extern UBYTE		scriptError;
extern ProgPrefs	prefs;

#define MENUCMD_NULL		MENUITEM(NOMENU, NOITEM, NOSUB)
#define MENUCMD_SAVEAS	MENUITEM(PROJECT_MENU, SAVEAS_ITEM, NOSUB)
#define MENUCMD_QUIT		MENUITEM(PROJECT_MENU, QUIT_ITEM, NOSUB)
#define MENUCMD_RESETALL MENUITEM(OPTIONS_MENU, RESET_ITEM, NOSUB) 

#define USERREQ_EDIT	2
#define USERREQ_TEXT	3

/*
 * AREXX force options
 */

#define NUM_FORCE_OPTIONS		(sizeof(forceNames)/sizeof(TextPtr))

static TextPtr forceNames[] = {
	"Force"
};

static TextChar strQuick[]		= "Quick";

typedef struct MenuCmdList {
	TextPtr	Name;
	UWORD		Num;
} MenuCmdList;


static MenuCmdList menuCmds[] = {
	{ "About",			MENUITEM(PROJECT_MENU,	ABOUT_ITEM,		NOSUB) },
	{ "SaveAs",			MENUITEM(PROJECT_MENU,	SAVEAS_ITEM,		NOSUB) },
	{ "PageSetup",		MENUITEM(PROJECT_MENU,	PAGESETUP_ITEM,		NOSUB) },
	{ "Print",			MENUITEM(PROJECT_MENU,	PRINT_ITEM,		NOSUB) },
	{ "SaveSettings",		MENUITEM(PROJECT_MENU,	SETTINGS_ITEM,		SAVESETTINGS_SUBITEM) },
	{ "LoadSettings",		MENUITEM(PROJECT_MENU,	SETTINGS_ITEM,		LOADSETTINGS_SUBITEM) },
	{ "Quit",			MENUITEM(PROJECT_MENU,	QUIT_ITEM,		NOSUB) },
		
	{ "BackupOptions",	MENUITEM(OPTIONS_MENU,	BACKUP_OPTS_ITEM,	NOSUB) },
	{ "RestoreOptions",	MENUITEM(OPTIONS_MENU,	RESTORE_OPTS_ITEM,	NOSUB) },
	{ "CatalogOptions",	MENUITEM(OPTIONS_MENU,	CATALOG_OPTS_ITEM,	NOSUB) },
	{ "SessionLogOptions",MENUITEM(OPTIONS_MENU,	LOGFILE_OPTS_ITEM,	NOSUB) },
	{ "BufferOptions",	MENUITEM(OPTIONS_MENU,	BUFF_OPTS_ITEM,		NOSUB) },
	{ "TapeOptions",		MENUITEM(OPTIONS_MENU,	TAPE_OPTS_ITEM,		NOSUB) },
	{ "ResetAll",		MENUITEM(OPTIONS_MENU,	RESET_ITEM,		NOSUB) },
	{ "Preferences",		MENUITEM(OPTIONS_MENU,	PREFERENCES_ITEM,	NOSUB) },
		
	{ "Tag",				MENUITEM(TAG_MENU,		TAG_ITEM,		NOSUB) },
	{ "TagAll",			MENUITEM(TAG_MENU,		TAG_ALL_ITEM,		NOSUB) },
	{ "Untag",			MENUITEM(TAG_MENU,		UNTAG_ITEM,		NOSUB) },
	{ "UntagAll",		MENUITEM(TAG_MENU,		UNTAG_ALL_ITEM,		NOSUB) },
	{ "TagFilter",		MENUITEM(TAG_MENU,		TAG_FILTER_ITEM,	NOSUB) },
		
	{ "TapeControl",		MENUITEM(UTILITIES_MENU,TAPE_UTILITIES_ITEM,	NOSUB) },
	{ "GetSessionInfo",	MENUITEM(UTILITIES_MENU,GET_SESSINFO_ITEM,	NOSUB) },
	{ "SetSessionInfo",	MENUITEM(UTILITIES_MENU,SET_SESSINFO_ITEM,	NOSUB) },
	{ "SCSIInfo",		MENUITEM(UTILITIES_MENU,SCSI_INTERROGATOR_ITEM,	NOSUB) },
};

#define NUM_MENU_COMMANDS	(sizeof(menuCmds)/sizeof(MenuCmdList))
#define NUM_MISC_COMMANDS	(sizeof(miscCmdNames)/sizeof(TextPtr))

static TextPtr miscCmdNames[] = {
	"ProgName", "ProgVersion",
	"ScreenToFront", "ScreenToBack",
	"SetPrint", 
	"SetBackupOptions", "SetBackupDestination",
	"SetRestoreOptions", "SetRestoreSource",
	"Proceed", "Backup", "Restore",
	"TapeErase","TapeRetension", "TapeRewind", "TapeSpace", "TapeAdvance", "SetTapeOptions",
	"SetCatalogOptions", "SetSessionLogOptions", "SetBufferOptions",
	"ResetAll", "Quit", 	"GetRate",
	"Request1", "Request2", "Request3", "RequestText", "SetPath"
};


enum {
	CMD_PROGNAME,		CMD_PROGVERSION,
	CMD_SCREENFRONT,	CMD_SCREENBACK,
	CMD_SETPRINT,		CMD_SETBACKUPOPTS,	CMD_SETBACKUPDEST,
	CMD_SETRESTOREOPTS,	CMD_SETRESTORESRC,
	CMD_PROCEED,		CMD_BACKUP,			CMD_RESTORE,
	CMD_ERASE,			CMD_RETENSION,		CMD_REWIND,	CMD_SPACE,	CMD_SPACE2,	CMD_SETTAPEOPTS,
	CMD_SETCATOPTS,		CMD_SETSESSOPTS,	CMD_SETBUFFOPTS,
	CMD_RESETALL,		CMD_QUIT,			CMD_GETRATE,
	CMD_REQUEST1,		CMD_REQUEST2,		CMD_REQUEST3,	CMD_REQUESTTEXT, CMD_SETPATH
};


static LONG	UserRequest(WORD, TextPtr, LONG *);
static BOOL	MatchCommand(TextPtr, UWORD, TextPtr);
static LONG DoMenuCommand(WindowPtr, UWORD, TextPtr, UWORD);
static BOOL CheckForForce(TextPtr);

/*
 *	Check for a match to a command string
 *	Return TRUE if equal
 */

static BOOL MatchCommand(TextPtr cmdText, UWORD cmdLen, TextPtr text)
{
	return (CmpString(cmdText, text, cmdLen, strlen(text), FALSE) == 0);
}

/*
 * Process the macro command text
 */

LONG ProcessCommandText(register TextPtr cmdText, UWORD modifiers, BOOL resultEnabled, LONG *result2)
{
	register WORD		i, cmdLen, menuCmd;
	register LONG		result = RC_OK;
	register TextPtr	argText;
	LONG				(*func)(TextPtr) = NULL;
	Dir					dir;

/*	menuCmd = -1; */
	*result2 = 0L;

/*
	Get the command and data portions of argument
*/	
	while (*cmdText == ' ')
		cmdText++;
	argText = cmdText;
	while (*argText && *argText != ' ')
		argText++;
	cmdLen = (UWORD) (argText - cmdText);
	while (*argText == ' ')
		argText++;
/*
	Check for menu commands
*/
	for (i = 0; i < NUM_MENU_COMMANDS; i++) {
		if (MatchCommand(cmdText, cmdLen, menuCmds[i].Name))
			break;
	}
/*
	Handle menu commands
*/
	if (i < NUM_MENU_COMMANDS) {
		menuCmd = menuCmds[i].Num;

		if (menuCmd == MENUCMD_NULL)
			goto Error;

		result = DoMenuCommand(cmdWindow, menuCmd, argText, modifiers);
		goto Done;
	}
/*
	Check for non-menu commands
*/
	for (i = 0; i < NUM_MISC_COMMANDS; i++) {
		if (MatchCommand(cmdText, cmdLen, miscCmdNames[i]))
			break;
	}
	if (i >= NUM_MISC_COMMANDS)
		goto Error;
/*
	Handle non-menu commands
*/
	switch (i) {
	case CMD_PROGNAME:
		if (!resultEnabled) {
			goto Error;
		}
		*result2 = (LONG) strProgName;
		break;
	case CMD_PROGVERSION:
		if (!resultEnabled) {
			goto Error;
		}
		NumToString((LONG) ((QB_VERSION >> 8) & 0xFF), strBuff);
		strcat(strBuff, strPeriod);
		NumToString((LONG) ((QB_VERSION >> 4) & 0x0F), strBuff + strlen(strBuff));
		*result2 = (LONG) strBuff;
		break;
	case CMD_SCREENFRONT:
		ScreenToFront(screen);
		break;
	case CMD_SCREENBACK:
		ScreenToBack(screen);
		break;
	case CMD_SETBACKUPDEST:
		if (!SetBackupDestination(argText) )
			result = RC_ERROR;
		break;
	case CMD_SETRESTORESRC:
		if (!SetRestoreSource(argText))
			result = RC_ERROR;
		break;
	case CMD_SETBACKUPOPTS:
		func = SetBackupOptions;
		break;
	case CMD_SETRESTOREOPTS:
		func = SetRestoreOptions;
		break;
	case CMD_SETCATOPTS:
		func = SetCatalogOptions;
		break;
	case CMD_SETSESSOPTS:
		func = SetSessionLogOptions;
		break;
	case CMD_SETTAPEOPTS:
		func = SetTapeOptions;
		break;
	case CMD_SETBUFFOPTS:
		func = SetBufferOptions;
		break;
	case CMD_SETPRINT:
		func = SetPrintOption;
		break;
	case CMD_RESTORE:
		if (!DoScriptRestore(argText) )
			result = RC_ERROR;
		break;		
	case CMD_BACKUP:
		if (prefs.OldPrefs.BackupType == 1) {
			if (!DoScriptBackup(argText) )
				result = RC_ERROR;
			break;
		} else {
			if (!DoScriptBackup(argText)) {
				result = RC_ERROR;
				break;
			}
		}
	case CMD_PROCEED:
		if (operation == OPER_PREP || (operation == OPER_SELECTION && HandleOperationButton(TRUE, FALSE))) {
			scriptError = 0;
			result = HandleOperationButton(TRUE, FALSE) ? scriptError : RC_ERROR;
		}
		else {
			result = RC_ERROR;
		}
		break;
	case CMD_ERASE:
		if (!EraseTape(NULL, FALSE, CmpString(argText, strQuick, strlen(argText), (WORD)strlen(strQuick), FALSE) == 0)) {
			result = RC_ERROR;
		}
		break;
	case CMD_RETENSION:
		if (!RetensionTape(NULL, FALSE)) {
			result = RC_ERROR;
		}
		break;
	case CMD_REWIND:
		if (!RewindTape(NULL, FALSE)) {
			result = RC_ERROR;
		}
		break;
	case CMD_SPACE:
	case CMD_SPACE2:
		i = StringToNum(argText);
		if (i < 1) {
			i = 1;
		}
		if (!SpaceTape(NULL, i))
			result = RC_ERROR;
		break;
	case CMD_GETRATE:
		if (!resultEnabled || operation != OPER_COMPLETE)
			goto Error;
		GetRateString(strBuff);
		*result2 = (LONG) strBuff;
		break;
	case CMD_REQUESTTEXT:
		if (!resultEnabled) {
			goto Error;
		}
	case CMD_REQUEST1:
	case CMD_REQUEST2:
	case CMD_REQUEST3:
		result = UserRequest(i, argText, result2);
		break;
	case CMD_SETPATH:
		if (strlen(argText) == 0 || (dir = Lock(argText, ACCESS_READ)) == NULL)
			goto Error;
		SetCurrentDir(dir);
		UnLock(dir);
		break;
	}
	if (func)
		result = (*func)(argText);
	goto Done;
/*
	Command not found
*/
Error:
	result = RC_FATAL;
/*
	Update window display
*/
Done:
	force = FALSE;
	return (result);
}

/*
	Handle menu command, possibly a variation with parameters
*/

static LONG DoMenuCommand(WindowPtr cmdWindow, UWORD menuCmd, TextPtr argText, UWORD modifiers)
{
	LONG result = RC_OK;
	UWORD len;
	TextChar cmdBuff[256];
/*
	If there are arguments, do not go through the menu command, but through here
	instead. If arguments are not proper, return RC_WARN.
*/
	if (*argText) {
		GetNextWord(argText, cmdBuff, &len);
		switch(menuCmd) {
		case MENUCMD_SAVEAS:
			if (!SaveFileAs(cmdBuff) )
				result = RC_ERROR;
			break;
		case MENUCMD_RESETALL:
		case MENUCMD_QUIT:
			if (force = CheckForForce(argText)) {
				break;
			}
		default:
			result = RC_WARN;
			break;
		}
	} else {
		result = DoMenu(cmdWindow, menuCmd, modifiers, FALSE) ? RC_OK : RC_ERROR;
	}
	return(result);
}

/*
 * Check for FORCE option
 */
 
static BOOL CheckForForce(TextPtr optName)
{
	BOOL found = FALSE;
	register WORD i;
	
	for( i = 0; i < NUM_FORCE_OPTIONS; i++) {
		if (CmpString(optName, forceNames[i], strlen(optName), (WORD) strlen(forceNames[i]), FALSE) == 0) {
			found = TRUE;
		}
	}
	return(found);
}

/*
 * Read next parameter and stuff into the "word" paramter passed.
 * Bumps length by whatever it takes to read the next paramter.
 */
 
void GetNextWord(register TextPtr text, TextPtr word, UWORD *length)
{
	register UWORD len;
	register UWORD start = 0;
	register UBYTE endChar;

	switch (text[0]) {
		case DQUOTE :
			endChar = DQUOTE;
			start = 1;
			break;
		case QUOTE :
			endChar = QUOTE;
			start = 1;
			break;
		default:
			endChar = ' ';
			break;
	}

	for(len = start; text[len] && text[len] != endChar; len++);

	BlockMove(&text[start], word, len-start);	/* set up first word */
	word[len-start] = '\0';

	if (text[len])
		for (len++;text[len] && text[len] == ' '; len++);

	*length = len;
}

/*
 *	Handle user request dialogs
 */

static LONG UserRequest(WORD cmdNum, TextPtr prompt, LONG *result2)
{
	LONG result;
	WORD dlgNum;
	DialogPtr dlg;

	switch(cmdNum) {
	case CMD_REQUEST1:
		dlgNum = DLG_USERREQ1;
		break;
	case CMD_REQUEST2:
		dlgNum = DLG_USERREQ2;
		break;
	case CMD_REQUEST3:
		dlgNum = DLG_USERREQ3;
		break;
	case CMD_REQUESTTEXT:
		dlgNum = DLG_USERREQTEXT;
		break;
	default:
		return(RC_FATAL);
	}
	dlgList[dlgNum]->Gadgets[USERREQ_TEXT].Info = prompt;
	BeginWait();
	if ((dlg = GetDialog(dlgList[dlgNum], screen, mainMsgPort)) == NULL) {
		Error(ERR_NO_MEM);
		EndWait();
		return(RC_ERROR);
	}
	OutlineOKButton(dlg);
	result = ModalDialog(mainMsgPort, dlg, DialogFilter);
	if (cmdNum == CMD_REQUESTTEXT && result == RC_OK) {
		GetEditItemText(dlg->FirstGadget, USERREQ_EDIT, strBuff);
		*result2 = (LONG) strBuff;
	}
	DisposeDialog(dlg);
	EndWait();
	return(result);
}

#ifdef FUTURE
/*
 * Load and process a command file could look something like this.
 */
 
BOOL LoadCommandFile(TextPtr fileName)
{
	register struct FileInfoBlock *fibPtr = (struct FileInfoBlock *)fib;
	File file;
	BPTR lock;
	UBYTE *buff;
	register UBYTE *ptr;
	BOOL success = TRUE;
	ULONG size;
	
	lock = Lock(fileName, ACCESS_READ);
	if (lock != NULL) {
		if (Examine(lock, fibPtr)) {
			buff = NewPtr(fibPtr->fib_Size);
			if (buff == NULL) {
				if (fibPtr->fib_Size) {
					Error(ERR_NO_MEM);
				}
			} else {
				ptr = buff;
				file = Open(fileName, MODE_OLDFILE);
				if (file) {
					size = Read(file, buff, fibPtr->fib_Size);
					if (size == fibPtr->fib_Size) {
						/*
						ProcessCommandText(ptr, 
						newPtr = BuildCommand(ptr);
						*/
					}
					Close(file);
				}
				DisposePtr(buff);
			}
		}
		UnLock(lock);
	}
	return(success);
}
#endif