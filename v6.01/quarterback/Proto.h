/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Assembler routines
 */

void	MyGiveUnit(void);
ULONG	ReadUnitID(LONG);
ULONG	GetUnitID(LONG);
void	PutChar(UBYTE);
void	PutWord(UWORD);
void	PutLong(ULONG);
void	PutGroup(ULONG);
UBYTE	GetChar(void);
UWORD	GetWord(void);
ULONG	GetLong(void);
BOOL	CompareTracks(Ptr, Ptr, ULONG);
void	ClearHash(ULONG *, ULONG);
void	ClearPrefixTable(ULONG *);
WORD	ReadChar(void);

/*
 *	Init.c
 */

void	Init(int, char **);
void	ShutDown(void);
void	SetUp(int, char **);
BOOL	LoadFileOrDrawer(TextPtr, Dir);

/*
 *	Main.c
 */

void	main(int, char **);
void	DoIntuiMessage(IntuiMsgPtr);
BOOL  DoMenu(WindowPtr, UWORD, UWORD, BOOL);
BOOL	CheckAbortButton(void);
BOOL	GetAltKeyStatus(WORD);

/*
 *	Monitor.c
 */

BOOL	InitOp(void);
void	EndOp(void);
void	WaitForPreviousSendIO(DevParamsPtr);
BYTE	DoWriteBuffer(DevParamsPtr, Ptr, ULONG);
BYTE	WriteBuffer(DevParamsPtr, Ptr, ULONG);
BYTE	DoReadBuffer(DevParamsPtr, LONG);
BYTE	ReadBuffer(DevParamsPtr, LONG);

/*
 *	PrHandler.c
 */

void	InitPrintHandler(void);
void	PrValidate(PrintRecPtr);
void	PrintDefault(PrintRecPtr);
BOOL	PageSetupRequest(void);
BOOL	PrintRequest(void);
LONG	SetPrintOption(TextPtr);

/*
 *	Print.c
 */

BOOL  GetSysPrefs(void);
void  GetPrinterType(void);
BOOL	WriteHeader(File, BOOL);
WORD	WriteDirHeading(File, TextPtr, BOOL);
WORD	WriteEntry(File, WORD *, BOOL *);
void	InitCatalogPrint(TextPtr);
void	EndCatalogPrint(TextPtr);
BOOL	PrintList(void);

/*
 *	Macro.c
 */

void	InitRexx(void);
void	ShutDownRexx(void);
void	DoRexxMsg(struct RexxMsg *);
BOOL	IsMacroFile(TextPtr);
void	DoMacro(TextPtr, WORD, TextPtr *);
BOOL	DoMacroMenu(WindowPtr, UWORD, UWORD);
void	DoAutoExec(int, char *[]);
void	LoadFKeys(void);

/*
 *	Icon.c
 */

void	InitIcons(void);
void	ShutDownIcons(void);
void	DoAppMsg(struct AppMessage *);
void	SaveIcon(TextPtr, WORD);
void	DrawMiniIcon(RastPtr, RectPtr);

/*
 *	Error.c
 */

TextPtr	ParseDialogString(TextPtr, TextPtr);
void	Error(WORD);
void	FixTitle(void);
void	DOSError(WORD, LONG);
BYTE	DiskSenseDialog(WORD, BYTE);
BYTE	WarningDialog(WORD, BYTE);
void	DoHelp(WindowPtr);

/*
 *	Misc.c
 */

BOOL	DiskSenseFilter(IntuiMsgPtr, WORD *);
BOOL	DialogFilter(IntuiMsgPtr, WORD *);
void  OutlineOKButton(WindowPtr);
/*void  DrawArrowBorder(WindowPtr, WORD);*/
void	BeginWait(void);
void	EndWait(void);

void  SetBusyPointer(void);
void  NextBusyPointer(void);
void	SetArrowPointer(void);

void	ErrBeep(void);
void	DiskChangeBeep(void);
void  StdBeep(void);

WORD	StdDialog(WORD);
BOOL	CheckNumber(TextPtr);

WORD	CompareText(TextPtr, TextPtr, WORD, WORD);
BOOL	DevMounted(TextPtr);
BOOL	DirAssigned(TextPtr);

void	FlushMsgPort(MsgPortPtr);
void	ToggleCheckbox(BOOL *, WORD, WindowPtr);
void	GadgetOn(WindowPtr, WORD);
void	GadgetOff(WindowPtr, WORD);
void	GadgetOnOff(WindowPtr, WORD, BOOL);
void	EnableGadget(WindowPtr, WORD, BOOL);
void	XDateString(struct DateStamp *, WORD, TextPtr);
void	DestroyRequest(RequestPtr);
BOOL	BuildPath(TextPtr, TextPtr, Dir, WORD);

void	NumToCommaString(LONG, TextPtr);
BOOL	TestStrLen(TextPtr);

#ifdef DOS_DOS_H
ULONG	GetTickDiff(struct DateStamp *, struct DateStamp *);
#endif

#ifdef TOOLBOX_STDFILE_H
BOOL	DoStdGetFile(SFReply *, TextPtr);
BOOL	DoStdPutFile(SFReply *, TextPtr, TextPtr);
#endif

#ifdef TOOLBOX_REQUEST_H
RequestPtr DoGetRequest(WORD);
#endif

#ifdef DOS_DOSEXTENS_H
LONG	DosFlagType(LONG);
struct DevInfo *NHLockDosList(LONG);
struct DevInfo *NHNextDosEntry(struct DevInfo *, LONG);
void	NHUnlockDosList(LONG);
#endif

#ifdef TOOLBOX_BARGRAPH_H
void	DoDrawBarGraph(RequestPtr, BarGraphPtr);
void	DoSetBarGraph(RequestPtr, BarGraphPtr, ULONG);
#endif

LONG	DiffTimeMilli(LONG, LONG, LONG, LONG);

/*
 *	Memory.c
 */

Ptr		NewPtr(LONG);
void	DisposePtr(Ptr);
LONG	GetPtrSize(Ptr);

/*
 * Prefs.c
 */

void	SetPathName(TextPtr, TextPtr, BOOL);
BOOL	ReadProgPrefs(TextPtr);
BOOL	IsPrefsFile(TextPtr);
BOOL	GetProgPrefs(TextPtr);
void	GetStdProgPrefs(void);
BOOL	SaveProgPrefs(TextPtr);
void	SetStdDefaults(void);

/*
 *	Window.c
 */

WindowPtr	OpenBackWindow(void);

void	SetWindowClip(WindowPtr);

WindowPtr	OpenOperationWindow(void);
void	RemoveOperationWindow(WindowPtr);

void	RefreshButtonStrip(BOOL);
void	DoWindowActivate(WindowPtr, BOOL);
void	DoNewSize(WindowPtr);
void	DoWindowUpdate(WindowPtr);
void	UpdateWindows(void);
void  RefreshWindow(WindowPtr);
void	DoWindowRefresh(WindowPtr);
void	SelectWindow(WindowPtr);
void	CauseUpdate(WindowPtr);
void	BuildSigBits(void);
void	WipeStrip(BOOL);

/*
 * View.c
 */

void  AddWindowItem(WindowPtr);
void  RemoveWindowItem(WindowPtr);

/*
 *	Menu.c
 */

void	OnOffMenu(WindowPtr, LONG, BOOL);
void  SetProjectMenu(void);
void	SetOptionsMenu(void);
void	SetUtilitiesMenu(void);
void	SetMacroMenu(void);
void	OnOffTapeMenuItem(void);
void	SetAllMenus(void);

/* 
 * Gadget.c
 */

void	AdjustScrollBars(WindowPtr);
void	AdjustToHorizScroll(WindowPtr);
void	ScrollLeft(WindowPtr);
void	ScrollRight(WindowPtr);
void	DoCursorKey(UWORD, UWORD);
void	DoEnterDirectory(void);
void	DoParentDirectory(BOOL);
void	DoGadgetDown(WindowPtr, IntuiMsgPtr);
void	DoGadgetUp(WindowPtr, IntuiMsgPtr);
void	DoGadgetItem(WindowPtr, IntuiMsgPtr, WORD);

/*
 * Project.c
 */

BOOL	DoSaveAs(void);
BOOL	DoSaveCatalog(void);
BOOL	DoSaveSessionLog(void);
BOOL	SaveFileAs(TextPtr);
BOOL	DoPageSetup(void);
BOOL	DoPrint(WindowPtr, UWORD, BOOL);
BOOL	DoSaveSettings(void);
BOOL	DoSaveAsSettings(void);
BOOL	DoLoadSettings(void);
BOOL	DoSettingsMenu(UWORD);
BOOL	DoShutDown(void);
BOOL	DoProjectMenu(WindowPtr, UWORD, UWORD, UWORD);
#ifdef TOOLBOX_STDFILE_H
BOOL	OpenSaveAsDialog(SFReplyPtr, BOOL);
#endif

/*
 * Options.c
 */

BOOL	DoOptionsMenu(WindowPtr, UWORD, UWORD, UWORD);
BOOL	DifferentDev(GadgetPtr, TextPtr, WORD *);
BOOL	DoCheckSCSIDrive(GadgetPtr, WORD *, TextPtr, UBYTE *, BOOL);
BOOL	DoBackupOptions(void);
BOOL	DoRestoreOptions(void);
BOOL	DoCatalogOptions(void);
BOOL	DoLogfileOptions(void);
BOOL	DoPreferences(void);
BOOL	DoTapeOptions(void);
BOOL	DoBufferOptions(void);
BOOL	DoResetOptions(void);
BOOL	SetBackupDestination(TextPtr);
BOOL	SetRestoreSource(TextPtr);
LONG	SetBackupOptions(TextPtr);
LONG	SetRestoreOptions(TextPtr);
LONG	SetCatalogOptions(TextPtr);
LONG	SetSessionLogOptions(TextPtr);
LONG	SetBufferOptions(TextPtr);
LONG	SetTapeOptions(TextPtr);
ULONG	GetBufferMemTypeFromID(UWORD);
UWORD	GetIDFromBufferMemType(ULONG);

/*
 * Utilities.c
 */
 
BOOL	DoTapeUtilities(void);
BOOL	DoGetSessionInfo(void);
BOOL	DoSetSessionInfo(void);
BOOL	DoSCSIInterrogator(void);
BOOL	DoUtilitiesMenu(WindowPtr, UWORD, UWORD);

/*
 * Report.c
 */

BOOL	OutputReport(TextPtr);
BOOL	DoOutputReport(void);
BOOL	WriteStr(TextPtr, File);
BOOL	WriteStrNow(TextPtr);
void	FirstRuntimeStrings(void);
void	FinalRuntimeStrings(void);
void	DoFinalRuntimeStrings(void);
TextPtr GetModeName(UBYTE, BOOL);
void	GetRateString(TextPtr);

/*
 * Files.c
 */

void	InitStats(void);
void	CloseFile(void);
BOOL	HandleOperationButton(BOOL, BOOL);
BOOL	BeginOp(void);
void	SetModeOp(UBYTE, UBYTE);
void	BumpPositionMarker(void);
DirFibPtr NextListEntry(DirFibPtr);
BOOL	DoScriptBackup(TextPtr);
BOOL	DoScriptRestore(TextPtr);
BOOL	EnterScanMode(UBYTE, BOOL);
TextPtr GetIOErrStr(WORD);
void	InitPosition(void);
void	UnableToProcess(DirFibPtr, TextPtr, WORD);
void	InitDirectory(void);
void	RecalcNumbers(BOOL);
BOOL	UpdateDirStatus(BOOL);
void	InitPath(void);
void	InitList(void);
BOOL	AppendPath(TextPtr);
BOOL	AppendDirPath(TextPtr, TextPtr);
void	TruncatePath(void);
DirFibPtr NextFile(BOOL, BOOL);
void	InitFileDir(void);
DirFibPtr GetFileDir(void);
UWORD	CountEntries(DirLevelPtr);
BOOL	DoLoadDirectory(UBYTE, TextPtr);
BOOL	LoadDirectory(UBYTE, BOOL);
BOOL	BuildDir(void);
BOOL	ScanLevel(BOOL);
BOOL	NextEntry(void);
BOOL	ScanDir(void);
void	SortInsert(void);
void	SortAllDirs(void);
BOOL	CompareDateStamps(struct DateStamp *, struct DateStamp *);
void 	SortDir(void);
BOOL	PathLock(void);
void	DiskBad(void);
void	PrevLevel(void);
void	BuildAbort(void);
BOOL	UnLockLocks(DirLevelPtr);
DirFibPtr AllocateDirBlock(BOOL);
void	FreeDirBlocks(void);
BOOL	WriteCatalog(void);
BOOL	ReadDir(DevParamsPtr);
DirFibPtr DetermineLevel(void);
void	EnterDirectory(DirFibPtr);
void	SelectDir(DirFibPtr, BOOL);
BOOL	SetResetTag(BOOL, DirFibPtr, BOOL);
void	UpdateProtectionBits(TextPtr);
void	CheckAbort(void);
void	AddToCompletedBytes(DevParamsPtr, WORD);
BOOL	DoOpenFile(TextPtr, Dir);
BOOL	OpenBackupFile(TextPtr, Dir);

/*
 * Buffer.c
 */

BOOL	AllocateBuffers(ULONG);
void	FreeBuffers(void);

/*
 * Device.c
 */

BOOL	FloppyInit(void);
BOOL	PatchGiveUnit(void);
void	UnPatchGiveUnit(void);
void	UpdateDeviceList(void);
BOOL	CheckExecDeviceList(DevParamsPtr);
void	ConvertBSTR(BSTR, TextPtr);
BOOL	InitDiskInt(DevParamsPtr);
void	RestoreDiskInt(DevParamsPtr);
BOOL	OnOffDevice(DevParamsPtr, BOOL);
LONG	PacketIO(MsgPortPtr, LONG);
BOOL	OpenAllDevices(void);
BOOL	InitDevice(DevParamsPtr);
BOOL	AllocDeviceList(void);
void	FreeDeviceList(void);
DevParamsPtr SelectNextDrive(DevParamsPtr);
BOOL	MotorOff(BOOL);
void	CloseAllDevices(void);
void	CloseDev(DevParamsPtr);
void	FreeDriveResources(DevParamsPtr);
void	ShutDevDown(DevParamsPtr);
void __asm	DiskChangeInterrupt(register __a1 DevParamsPtr devParam);
BOOL	DiskChanged(void);
BOOL	DiskExamineForChange(DevParamsPtr);
BOOL	RestoreProtectionBits(TextPtr);
BOOL	ProcessAllArchiveBits(void);
LONG	EnableFileAccess(TextPtr);

#ifdef TOOLBOX_LIST_H
ListHeadPtr BuildExecDeviceList(void);
#endif

/*
 * Backup.c
 */

BOOL	InitLoops(void);
void	DoRewind(BOOL, BOOL);
void 	DoEndLoops(void);
void	EndLoops(BOOL);
BOOL	DoBackup(void);
BYTE	WriteFullBuffer(ULONG);
BYTE	WaitIOReq(WORD);
BYTE	WaitIORequest(WORD);
BYTE	WaitAllIOReq(void);
void	InitFirstCylinder(void);
void	InitDriveStates(BOOL);
void	DoInitDriveStates(BOOL);
BOOL	ReadAfterWrite(DevParamsPtr, Ptr, ULONG);
BOOL	CheckDiskFormat(DevParamsPtr);
BOOL	InsertDisk(DevParamsPtr, WORD);
void	SaveLevelFib(void);
void	RestoreLevelFib(void);

/*
 * Restore.c
 */
 
BOOL	DoRestore(BOOL);
BOOL	LoadDiskCatalog(void);
BOOL	DoLoadFirst(BOOL);
BOOL	LoadNextDisk(void);
BYTE	ReadNextTrack(void);
BOOL	TryToFindNextFile(WORD);
BOOL	GetCurrDisk(void);
void	RecalcDisk(DevParamsPtr, BOOL);
void	CheckCurrDisk(void);

/*
 * Display.c
 */

void	UpdateVolumeList(void);
BOOL	BuildVolumeList(TextPtr);
BOOL	BuildFileList(DirFibPtr);
void	BuildListEntry(DirFibPtr, TextPtr);
void	QBDrawProc(RastPtr, TextPtr, RectPtr);
void	DisplayAllDevicesStatus(void);
void	UpdateFilesStat(void);
void	UpdateBytesStat(void);
void	UpdateStats(void);
void	DisplayStats(BOOL);
void	DriveStatus(DevParamsPtr);
void	ShowDeviceStatus(DevParamsPtr);
void	StatusCompleted(DevParamsPtr);
void	StatusNotSelected(DevParamsPtr);
void	StatusNotReady(DevParamsPtr);
void	StatusReady(DevParamsPtr);
void	StatusActive(DevParamsPtr);
void	StatusRemove(DevParamsPtr);
void	StatusPaused(DevParamsPtr);
void	StatusCheck(DevParamsPtr);
void	RefreshDeviceStatus(DevParamsPtr, WORD);
void	AdjustWidth(void);
void	StuffScanDirText(TextPtr);
void	StuffScanFileText(TextPtr);
void	InitScanRequester(RequestPtr);
void	RestoreLookAhead(void);
BOOL	AllocMiniIcons(void);
void	FreeMiniIcons(void);
void	SetAltButtons(BOOL);
void	UpdateEnterGadget(void);
void	DisplayNextFile(DirFibPtr);
void	DisplayNextVol(TextPtr);
void	DisplayStr(TextPtr);
void	DisplayErrStr(TextPtr, TextPtr);
void	DisableScrollList(void);
void	EnableScrollList(void);
void	RefreshScrollList(void);
void	ResetScrollListAfterDiskError(void);
void	InitRuntimeDisplay(TextPtr);
void	AddRuntimeItem(TextPtr, WORD);
void	ConstructRuntimeItem(TextPtr, UBYTE, TextPtr);
void	AppendToRuntimeItem(TextPtr);
void	RemoveRuntimeItem(WORD);
void	DoPasswordRequest(TextPtr);
BOOL	DoAskPasswordRequest(TextPtr);
TextPtr	RenameFileRequest(TextPtr);

/*
 * Compress.c
 */

BOOL	CompressInit(WORD);
void	CompressDeInit(void);
BOOL	CompressEOF(void);
ULONG	Compress(UBYTE *, ULONG, BOOL);
BOOL	DecompressInit(WORD);
void	DecompressDeInit(void);
ULONG	Decompress(ULONG *, ULONG, BOOL);

/*
 * About.c
 */

BOOL	DoAboutBox(void);

/*
 * Tape.c
 */

void	SetCurrentDev(void);
BOOL	CheckSCSIDrive(TextPtr, WORD, TextPtr, BOOL);
ULONG	GetBlockSizes(BOOL, ULONG *, UWORD *, ULONG *);
void	ClearSense(DevParamsPtr);
BOOL	ReadTapeHeader(DevParamsPtr, UBYTE *, ULONG);
BYTE	TapeIO(DevParamsPtr, UBYTE, ULONG);
BYTE	ProcessSenseData(DevParamsPtr, WORD);
void	WriteFilemark(DevParamsPtr);
BOOL	RewindTape(DevParamsPtr, BOOL);
BOOL	EraseTape(DevParamsPtr, BOOL, BOOL);
BOOL	RetensionTape(DevParamsPtr, BOOL);
BOOL	SpaceTape(DevParamsPtr, UWORD);
BOOL	DoSpaceTape(DevParamsPtr, UWORD);
BOOL	InitTapeParameters(DevParamsPtr, BOOL);
void	InitDriveType(BYTE);
BOOL	TapeWaitRequester(DevParamsPtr, UBYTE *, UBYTE);

/*
 * Tag.c
 */

void	DoTag(BOOL, BOOL);
BOOL	DoTagFilterRequest(void);
BOOL	TagFilter(BOOL, BOOL);
void	DoDoubleClick(WORD);
BOOL	DoTagMenu(WindowPtr, UWORD, UWORD);
#ifdef TOOLBOX_LIST_H
BOOL	LoadFilter(ListHeadPtr, TextPtr);
#endif

/*
 * Command.c
 */
 
LONG	ProcessCommandText(TextPtr, UWORD, BOOL, LONG *);
void	GetNextWord(TextPtr, TextPtr, UWORD *);
/*BOOL	LoadCommandFile(TextPtr);*/
