/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Window operations
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <graphics/gfxmacros.h>
#include <graphics/regions.h>
#include <intuition/intuition.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>
#include <Toolbox/Screen.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Image.h>
#include <Toolbox/Request.h>

#include "QB.h"
#include "Proto.h"

/*
 *	External variables
 */
 
extern UBYTE			_tbPenLight, _tbPenWhite, _tbPenBlack;
extern WORD				_tbXSize, _tbYSize;
extern BOOL				_tbSmartWindows, _tbOnPubScreen;

extern WORD				intuiVersion;

extern struct NewWindow	newBackWindow, newWindow;

extern ScreenPtr		screen;
extern MsgPortPtr		mainMsgPort, appIconMsgPort;
extern MsgPort			rexxMsgPort;
extern WindowPtr		backWindow;
extern MenuPtr			menuStrip;	

extern WindowPtr		cmdWindow;

extern BOOL				quitFlag;

extern DlgTemplPtr		dlgList[];
extern GadgetTemplate	windowGadgets[];

extern TextChar		screenTitle[], strProgName[];

extern UWORD			grayPat[];

extern BOOL				tapeDevice;

extern struct RexxLib	*RexxSysBase;
extern ULONG			sigBits;

extern ScrollListPtr		scrollList;

extern UBYTE			operation;
extern GadgetPtr		mainSelGadgets, tagGadgets, runtimeGadgets;
extern BOOL				disableDisplay, skipUpdate;
extern WORD				currListItem, numColors;
extern ProgPrefs		prefs;

/*
 *	Local variables and definitions
 */

typedef struct  {
	LayerPtr	Layer;
	Rectangle	Bounds;
	LONG		OffsetX, OffsetY;
} BackFillMsg, *BFMsgPtr;

static struct Hook	backFillHook, docBackFillHook;

/*
 *	Local prototypes
 */

static void __saveds __asm BackFill(register __a0 struct Hook *, register __a2 RastPtr, register __a1 BFMsgPtr);
static void __saveds __asm DocBackFill(register __a0 struct Hook *, register __a2 RastPtr, register __a1 BFMsgPtr);
static ULONG GetPortSigBit(MsgPortPtr);
static void	ClearBackWindow(RastPtr, Rectangle *);
static void	DrawScrollList(void);


/*
 *	Back fill routine for backWindow
 */

static void __saveds __asm BackFill(register __a0 struct Hook *hook,
									register __a2 RastPtr rPort,
									register __a1 BFMsgPtr bfMsg)
{
	RastPort newRPort;

	newRPort = *rPort;			/* Make a copy so we can modify it */
	newRPort.Layer = NULL;
	ClearBackWindow(&newRPort, &bfMsg->Bounds);
}

/*
 *	Back fill routine for backWindow
 */

static void __saveds __asm DocBackFill(register __a0 struct Hook *hook,
										register __a2 RastPtr rPort,
										register __a1 BFMsgPtr bfMsg)
{
	RastPort newRPort;

	newRPort = *rPort;			/* Make a copy so we can modify it */
	newRPort.Layer = NULL;
	SetAPen(&newRPort, _tbPenLight);
	SetDrMd(&newRPort, JAM1);
	RectFill(&newRPort, bfMsg->Bounds.MinX, bfMsg->Bounds.MinY,
			 bfMsg->Bounds.MaxX, bfMsg->Bounds.MaxY);
}

/*
 *	Clear backWindow
 */

static void ClearBackWindow(RastPtr rPort, Rectangle *rect)
{
	if( numColors >= 4 ) {
		SetAPen(rPort, _tbPenLight);
		SetDrMd(rPort, JAM1);
	} else {
		SetAPen(rPort, _tbPenBlack);
		SetBPen(rPort, _tbPenWhite);
		SetDrMd(rPort, JAM2);
		SetAfPt(rPort, grayPat, 1);
	}
	RectFill(rPort, rect->MinX, rect->MinY, rect->MaxX, rect->MaxY);
}

/*
 *	Open backWindow
 */

WindowPtr OpenBackWindow()
{
	ULONG oldIDCMPFlags;
	Rectangle rect;

	newBackWindow.Screen = screen;
	newBackWindow.Type = CUSTOMSCREEN;
	if (_tbOnPubScreen) {
		newBackWindow.Width = newBackWindow.Height = 1;
		newBackWindow.Flags |= NOCAREREFRESH;
	}
	else {
		newBackWindow.Width = screen->Width;
		newBackWindow.Height = screen->Height;
	}
	oldIDCMPFlags = newBackWindow.IDCMPFlags;
	newBackWindow.IDCMPFlags = 0;
	if (intuiVersion < OSVERSION_2_0)
		backWindow = OpenWindow(&newBackWindow);
	else {
		if (_tbOnPubScreen)
			newBackWindow.Type = PUBLICSCREEN;
		newBackWindow.Flags |= NOCAREREFRESH;
		backFillHook.h_Entry = (ULONG (*)()) BackFill;
		backWindow = OpenWindowTags(&newBackWindow,
									WA_BackFill, &backFillHook,
									TAG_END);
	}
	if (backWindow != NULL) {
		backWindow->UserPort = mainMsgPort;		/* Was monitorMsgPort in ProWrite */
		ModifyIDCMP(backWindow, oldIDCMPFlags);
		if(_tbOnPubScreen) {
			ScreenToFront(screen);	  /* In case it is not in front */
			WindowToBack(backWindow);
			SetWindowTitles(backWindow, (TextPtr) -1, screenTitle);
		}
		else if (intuiVersion < OSVERSION_2_0) {
			SetRect(&rect, 0, 0, (WORD) (backWindow->Width - 1), (WORD) (backWindow->Height - 1));
			ClearBackWindow(backWindow->RPort, &rect);
		}
		SetStdPointer(backWindow, POINTER_ARROW);
	}
	return (backWindow);
}

/*
 *	Install clip region for window
 */

void SetWindowClip(WindowPtr window)
{
	Rectangle rect;

	GetWindowRect(window, &rect);
	SetRectClip(window->WLayer, &rect);
}

/*
 *	Open the main operation window
 *	Return pointer to window if successful, NULL if not
 */

WindowPtr OpenOperationWindow()
{
	register WindowPtr window = NULL;
	register GadgetPtr gadgList;
	WORD zoomSize[4];
	register ULONG oldIDCMPFlags;
	GadgetPtr gadget;
	WORD topOffset;
	Rectangle rect, rect2, rect3;
	
/*
	Create and attach window gadgets
*/
	if ((gadgList = GetGadgets(windowGadgets)) != NULL) {
		GadgetItem(gadgList, UP_ARROW)->Activation	|= GACT_RIGHTBORDER;
		GadgetItem(gadgList, DOWN_ARROW)->Activation	|= GACT_RIGHTBORDER;
		GadgetItem(gadgList, VERT_SCROLL)->Activation	|= GACT_RIGHTBORDER;
		GadgetItem(gadgList, LEFT_ARROW)->Activation	|= GACT_BOTTOMBORDER;
		GadgetItem(gadgList, RIGHT_ARROW)->Activation	|= GACT_BOTTOMBORDER;
		GadgetItem(gadgList, HORIZ_SCROLL)->Activation	|= GACT_BOTTOMBORDER;
		if( (scrollList = NewScrollList(SL_SINGLESELECT)) == NULL) {
			DisposeGadgets(gadgList);
			Error(ERR_NO_MEM);
		} else {
			newWindow.FirstGadget = gadgList;
/*
	Open window and set message port to correct port
	All windows are SIMPLEREFRESH windows
*/
			newWindow.Screen = screen;
			newWindow.Type = CUSTOMSCREEN;
			newWindow.Flags = newWindow.Flags & ~REFRESHBITS |
			 (_tbSmartWindows ? SMART_REFRESH : SIMPLE_REFRESH );

			rect = *((Rectangle *) (&prefs.OldPrefs.WindowPos[0]));
			GetScreenViewRect(screen, &rect2);
			UnionRect(&rect, &rect2, &rect3);
			if( EmptyRect(&rect) || !EqualRect(&rect2, &rect3) ||
				((rect.MaxY - rect.MinY) < screen->BarHeight) || ((rect.MaxX - rect.MinX) < 200) ) {
				rect = rect2;
				topOffset = screen->BarHeight + 1;
				if( screen->ViewPort.Modes & LACE ) {
					topOffset++;
				}
				newWindow.LeftEdge = rect.MinX;
				newWindow.TopEdge = screen->BarHeight + 2;
				newWindow.Width = screen->Width;
				newWindow.Height = screen->Height - newWindow.TopEdge;
				if( _tbOnPubScreen ) {
					newWindow.Width = MIN( (((480 + IMAGE_ARROW_WIDTH) * _tbXSize) >> 3), (rect.MaxX - rect.MinX) + 1);
					newWindow.Height = MIN( (((256 + screen->BarHeight) * _tbYSize) / 11), ((rect.MaxY - rect.MinY) + 1) - topOffset);
					newWindow.TopEdge = MAX(newWindow.TopEdge,
						(((rect.MaxY - rect.MinY)+1) - newWindow.Height) >> 1);
					newWindow.LeftEdge += (((rect.MaxX - rect.MinX)+1) - newWindow.Width) >> 1;
				}
			} else {
				newWindow.LeftEdge = rect.MinX;
				newWindow.TopEdge = rect.MinY;
				newWindow.Width = (rect.MaxX - rect.MinX) + 1;
				newWindow.Height = (rect.MaxY - rect.MinY) + 1;
			}
/*
	Set up MinHeight and MinWidth for growbox's sake. Make sure all buttons visible.
*/
			gadget = GadgetItem(gadgList, BUTTON_UNTAG);
			newWindow.MinHeight = gadget->TopEdge + gadget->Height + 3 + IMAGE_ARROW_HEIGHT;
			if( newWindow.MinHeight > newWindow.Height ) {
				newWindow.Height = newWindow.MinHeight = MIN(newWindow.MinHeight, screen->Height);
			}
/*
 	Try to make sure it fits in window. Sacrifice title bar space if necessary.
*/
			if( newWindow.TopEdge > (screen->Height - newWindow.Height) ) {
				newWindow.TopEdge = screen->Height - newWindow.Height;
			}
			newWindow.MinWidth = gadget->LeftEdge + gadget->Width + 14 + 80;
			if( newWindow.MinWidth > newWindow.Width ) {
				newWindow.Width = newWindow.MinWidth = MIN(newWindow.MinWidth, screen->Width);
			}
			
			oldIDCMPFlags = newWindow.IDCMPFlags;
			newWindow.IDCMPFlags = 0;
/*
	Remember gadget placement for later gadget shuffling (between operation modes)
*/
			mainSelGadgets = GadgetItem(gadgList, BUTTON_ENTER);
			tagGadgets = GadgetItem(gadgList, BUTTON_TAG);
			runtimeGadgets = GadgetItem(gadgList, RUNTIME_ITEMS);
			GadgetItem(gadgList, DRIVE_ICON_BORDER)->NextGadget = tagGadgets->NextGadget->NextGadget;
			GadgetItem(gadgList, BUTTON_PARENT)->NextGadget = NULL;
/*
	Now open the window.
*/
			if (LibraryVersion(IntuitionBase) < OSVERSION_2_0)
				window = OpenWindow(&newWindow);
			else {
				zoomSize[0] = zoomSize[1] = 0;
				zoomSize[2] = screen->Width;
				zoomSize[3] = screen->Height;
				docBackFillHook.h_Entry = (ULONG (*)()) DocBackFill;
				window = OpenWindowTags(&newWindow,
						WA_Zoom, zoomSize, WA_BackFill, &docBackFillHook, WA_NewLookMenus, TRUE, TAG_END);
			}
			cmdWindow = window;
			newWindow.IDCMPFlags = oldIDCMPFlags;		/* For next CreateWindow */
			if (window == NULL) {
				(GadgetItem(gadgList, BUTTON_PARENT))->NextGadget = tagGadgets;
				DisposeScrollList(scrollList);
				DisposeGadgets(gadgList);
			} else {
				cmdWindow = window;
				SetWindowClip(window);
				RefreshButtonStrip(TRUE);
				EnableGadgetItem(gadgList, BUTTON_PARENT, window, NULL, FALSE);
				SetStdPointer(window, POINTER_ARROW);
				SetWTitle(window, strProgName);			/* Set to copy of title */
				if (_tbOnPubScreen)
					SetWindowTitles(window, (TextPtr) -1, screenTitle);
				window->UserPort = mainMsgPort;	/* Was monitorMsgPort in ProWrite */
				InsertMenuStrip(window, menuStrip);
				ModifyIDCMP(window, oldIDCMPFlags);
				InitScrollList(scrollList, GadgetItem(gadgList, SCROLL_LIST), window, NULL);
				BuildSigBits();
				BuildVolumeList(NULL);
			}
		}
	}
	return(window);
}

/*
 *	Remove the specified outline window and free all memory it allocated
 */

void RemoveOperationWindow(WindowPtr window)
{
	GadgetPtr gadgList;
	RegionPtr clipRgn;
	
	if( window != NULL ) {
		SetStdPointer(window, POINTER_WAIT);
		if( tapeDevice ) {
			quitFlag = TRUE;
			RewindTape(NULL, FALSE);
		}
		DisposeScrollList(scrollList);
		scrollList = NULL;
		ClearMenuStrip(window);
		SetModeOp(MODE_QUIT, OPER_LIMBO);
		gadgList = GadgetItem(window->FirstGadget, 0);
		window->UserPort = mainMsgPort;
		SetWTitle(window, NULL);
		clipRgn = InstallClipRegion(window->WLayer, NULL);
		if( clipRgn != NULL ) {
			DisposeRegion(clipRgn);
		}
		CloseWindowSafely(window, mainMsgPort);
		(GadgetItem(gadgList, BUTTON_ENTER-1))->NextGadget = mainSelGadgets;
		(GadgetItem(gadgList, BUTTON_TAG-1))->NextGadget = tagGadgets;
		(GadgetItem(gadgList, RUNTIME_ITEMS-1))->NextGadget = runtimeGadgets;
		(GadgetItem(gadgList, LAST_WINDOW_GADGET-1))->NextGadget = NULL;
		DisposeGadgets(gadgList);
	}
	cmdWindow = NULL;
}

/*
 *	Handle window activate/inactivate events
 */

void DoWindowActivate(WindowPtr window, BOOL activate)
{
	if( window != NULL ) {
		if( scrollList != NULL ) {
			SLDrawBorder(scrollList);
		}
		if( _tbSmartWindows ) {						/* This is needed to update bottom border */
			RefreshButtonStrip(FALSE);
		}
	}
	return;
}

/*
 *	Handle resizing of window - resize scrollable list
 */

void DoNewSize(WindowPtr window)
{
	register RequestPtr req = window->FirstRequest;

	SetWindowClip(window);
	if( _tbSmartWindows && (!skipUpdate || (req == NULL)) ) {
		RefreshButtonStrip(TRUE);			/* Bottom left needs to be painted */
		DrawScrollList();
	}
	while( req != NULL ) {
		if( req->ReqLayer != NULL ) {
			OutlineOKButton(window);
		}
		if( scrollList != NULL ) {
			SLDrawBorder(scrollList);
		}
		req = req->OlderRequest;
	}
	AdjustScrollBars(window);
}

/*
 * Draw scroll list
 */
  
static void DrawScrollList()
{
	if( scrollList != NULL ) {
		if( disableDisplay ) {
			EnableScrollList();
		}
		if( disableDisplay || (operation == OPER_IN_PROG) || (operation == OPER_PAUSE) ) {
			SLAutoScroll(scrollList, currListItem-1);
		}
		SLDrawBorder(scrollList);
		SLDrawList(scrollList);
		if( disableDisplay ) {
			DisableScrollList();
		}
	}
}

/*
 *	Refresh window contents (intuition generated events)
 */

void DoWindowRefresh(WindowPtr window)
{
	Rectangle rect;
	
	GetWindowRect(window, &rect);
	if( window == backWindow ) {
		if( intuiVersion < OSVERSION_2_0 ) {
			BeginRefresh(window);
			ClearBackWindow(window->RPort, &rect);
			EndRefresh(window, TRUE);
		}
	} else if( window == cmdWindow ) {
		if( !_tbSmartWindows ) {
			BeginRefresh(window);
			WipeStrip(TRUE);
			DisplayStats(TRUE);
	
			OutlineButton( GadgetItem(window->FirstGadget, BUTTON_1), window, NULL, TRUE);
			if( scrollList != NULL ) {
				SLDrawBorder(scrollList);
			}

			EndRefresh(window, TRUE);
			if( intuiVersion <= OSVERSION_2_0 ) {
				RefreshGadgets(window->FirstGadget, window, NULL);
			}
			if( scrollList != NULL && operation != OPER_SCAN && !skipUpdate ) {
				if( !disableDisplay ) {
					SLDrawList(scrollList);
				}
				if( disableDisplay && ((operation == OPER_PAUSE) || (operation == OPER_IN_PROG)) ) {
					SLAutoScroll(scrollList, currListItem);
				}
			}
			skipUpdate = FALSE;
		} else {
			BeginRefresh(window);
			if( scrollList != NULL ) {
				SLDrawBorder(scrollList);
				if( operation != OPER_SCAN && !skipUpdate ) {
					if( disableDisplay && ((operation == OPER_PAUSE) || (operation == OPER_IN_PROG)) ) {
						SLAutoScroll(scrollList, currListItem);
					}
				}
			}
			EndRefresh(window, TRUE);
		}
	}
}

/*
 * For pre-V2.0 system, erase the strip on left completely.
 */
 
void WipeStrip(BOOL fullStrip)
{
	register WindowPtr window = cmdWindow;
	Rectangle rect;
	
	if( intuiVersion < OSVERSION_2_0 ) {
		GetGadgetRect(GadgetItem(window->FirstGadget, SCROLL_LIST), window, NULL, &rect);
		rect.MaxX = rect.MinX - 2;
		rect.MinX = window->BorderLeft;
		rect.MaxY = window->Height - 2;
		if( !fullStrip ) {
			rect.MinY = window->MinHeight - window->BorderTop;
		}
 		SetAPen(window->RPort, _tbPenLight);
		SetDrMd(window->RPort, JAM1);
		FillRect(window->RPort, &rect);
	}
}

/*
 * Refresh rectangular button strip on left. If OSVERSION_2_0, then
 * the only action needed is to outline the OK button manually, because
 * of the backfill supplied by the 2.0 OS.
 *
 * If _tbSmartWindows is on, there will be no need to ever update the gadgets,
 * except at the beginning when the whole strip needs to be painted, in
 * which case they must be redrawn.
 */
 
void RefreshButtonStrip(BOOL fullStrip)
{
	GadgetPtr gadgList;
	register WindowPtr window = cmdWindow;

	if( window != NULL ) {
		gadgList = GadgetItem(window->FirstGadget, OK_BUTTON);	
		if( intuiVersion < OSVERSION_2_0 ) {
			if( (!_tbSmartWindows) || fullStrip ) {
				WipeStrip(fullStrip);
				/*if( _tbSmartWindows && fullStrip ) {
					SLDrawList(scrollList);
				}*/
				RefreshGadgets(gadgList, window, NULL);
			}
		}
		if( (!_tbSmartWindows) || fullStrip ) {
			DisplayStats(TRUE);
		}
		OutlineButton( GadgetItem(gadgList, BUTTON_1), window, NULL, TRUE);
	}
}

/*
	Select the window (bring to front, activate)
*/

void SelectWindow(WindowPtr window)
{
	if( window->WLayer->front)
		WindowToFront(window);
	ActivateWindow(window);
	Delay(5);
}	

/*
 * Builds signal bits for use by Wait() during operation loops
 */

void BuildSigBits()
{
	sigBits = GetPortSigBit(mainMsgPort);
	sigBits |= GetPortSigBit(appIconMsgPort);
	if( RexxSysBase )
		sigBits |= GetPortSigBit(&rexxMsgPort);
}

/*
 * Utility routine to return the mask of the signal bit of this port
 */
 
static ULONG GetPortSigBit(MsgPortPtr port)
{
	ULONG result = 0L;
	
	if( port != NULL ) {
		result = 1 << port->mp_SigBit;
	}
	return(result);
}
