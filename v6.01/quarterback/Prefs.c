/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Quarterback
 * Copyright (c) 1991 New Horizons Software, Inc.
 *
 * QB preferences routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <workbench/workbench.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/icon.h>

#include <string.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Memory.h>
#include <Toolbox/List.h>
#include <Toolbox/LocalData.h>
#include <Toolbox/Graphics.h>

#include "QB.h"
#include "Proto.h"

/*
 *	External variables
 */

extern TextChar	progPathName[], strPrefsName[], strCompFilter[];
extern TextChar	strscsi[], strSCSI[], strSCSIDevice[];
extern TextPtr		driveStrings[];

extern ProgPrefs	prefs;
extern UBYTE		floppyEnable[];

extern ListHeadPtr	devList;

extern WindowPtr	cmdWindow;

extern Ptr			fib;
extern BOOL			noSCSI;

static TextChar	oldPrefsFileName[] = "s:QB2.OPT";


/*
 * Definitions
 */
 
#define	QB_PREFS_VERSION	1

/*
 * Local prototypes
 */

static void ValidatePrefs(void);
 
/*
 *	Create path name to given file at same directory as program
 *	If abbrev is TRUE, then it's OK to use short-cut in AmigaDOS 2.0
 */

void SetPathName(TextPtr pathNameBuff, TextPtr fileName, BOOL abbrev)
{
	register WORD i;

	if (SystemVersion() >= OSVERSION_2_0 && abbrev)
		strcpy(pathNameBuff, "ProgDir:");
	else {
		strcpy(pathNameBuff, progPathName);
		for (i = strlen(pathNameBuff); i; i--) {
			if (pathNameBuff[i - 1] == '/' || pathNameBuff[i - 1] == ':')
				break;
		}
		pathNameBuff[i] = '\0';
	}
	strcat(pathNameBuff, fileName);
}

/*
 *	Set built-in preferences defaults
 */

void SetStdDefaults()
{
	register WORD i;
	UBYTE *ptr;
	
	/* Only non-zero assignments needed now */
	
	PrintDefault(&prefs.PrintInfo.PrintRec);

	prefs.PrintInfo.MeasureUnit	= MEASURE_DEFAULT;
	
	prefs.WarnInvalidFilenames		= TRUE;
	prefs.SaveIcons					= TRUE;
	prefs.DisplayBackupOpts			= TRUE;
	prefs.DisplayRestoreOpts		= TRUE;
	strcpy(prefs.OldPrefs.DeviceBuf1, driveStrings[0]);
	prefs.OldPrefs.BReport			= 1;
	prefs.OldPrefs.RReport			= 1;
	prefs.OldPrefs.BeepSound		= 1;
	prefs.OldPrefs.BeepFlash		= 1;
	prefs.OldPrefs.BAllDirs			= TRUE;
	prefs.OldPrefs.RSubdirs			= TRUE;
	prefs.OldPrefs.REmptySubdirs 	= TRUE;
	prefs.OldPrefs.Verify			= TRUE;
	prefs.OldPrefs.ReplaceEarlier	= REPLACE_YES;
	prefs.OldPrefs.ReplaceLater	= REPLACE_YES;
	prefs.CatOpts.IncludeSize		= TRUE;
	prefs.CatOpts.IncludeDate		= TRUE;
	prefs.CatOpts.IncludeTime		= TRUE;
	prefs.CatOpts.ShowSecs			= TRUE;
	prefs.CatOpts.SeparateDirs		= TRUE;
	prefs.LogOpts.LogLevel			= LOG_DRAWERS;
	prefs.Compress					= 12;
	prefs.BuffOpts.MemType			= MEMF_PUBLIC | MEMF_24BITDMA;
	prefs.BuffOpts.BackupBufferSize	= 32;
	prefs.BuffOpts.RestoreBufferSize	= 32;
	prefs.TapeOpts.AutoRewind		= TRUE;
	prefs.FloppyEnableMask			= 255;
	for( i = 0 ; i < 4 ; i++ ) {
		floppyEnable[i] = TRUE;
	}
	if( FindListItem(devList, strSCSIDevice) != -1 ) {
		strcpy(prefs.CurrBackupDev, strSCSIDevice);
	} else {
		ptr = NULL;
		for( i = 0 ; i < NumListItems(devList) ; i++ ) {
			if( stcpm(GetListItem(devList, i), strSCSI, &ptr) ) {
				break;
			}
			if( stcpm(GetListItem(devList, i), strscsi, &ptr) ) {
				break;
			}
		}
		if( ptr == NULL ) {
			i = 0;
		}
		strcpy(prefs.CurrBackupDev, GetListItem(devList, i));
	}	
	strcpy(prefs.CurrRestoreDev, prefs.CurrBackupDev);
	strcpy(prefs.CompFilterName, strCompFilter);
}

/*
 * Make sure preference values cannot mess us up if invalid
 */
 
static void ValidatePrefs()
{
	prefs.OldPrefs.ReplaceEarlier		= MIN(2, prefs.OldPrefs.ReplaceEarlier);
	prefs.OldPrefs.ReplaceLater		= MIN(2, prefs.OldPrefs.ReplaceLater);
	prefs.OldPrefs.BackupType			= MIN(1, prefs.OldPrefs.BackupType);
	prefs.OldPrefs.TestMode				= MIN(RMODE_MAX, prefs.OldPrefs.TestMode);
	prefs.LogOpts.LogLevel				= MIN(2, prefs.LogOpts.LogLevel);
	prefs.BuffOpts.BackupBufferSize	= MAX(1, prefs.BuffOpts.BackupBufferSize);
	prefs.BuffOpts.RestoreBufferSize	= MAX(1, prefs.BuffOpts.RestoreBufferSize);
}

/*
 * Examine the preferences file and read the appropriate bytes.
 */
 
BOOL ReadProgPrefs(fileName)
register TextPtr fileName;
{
	struct FileInfoBlock *fibPtr = (struct FileInfoBlock *) fib;
	BOOL success = FALSE;
	register LONG size;
	BPTR lock;
	File file;
	UBYTE prefsHeader[6];
	
	if( (lock = Lock(fileName, MODE_OLDFILE)) != NULL ) {
		
/*
 * Because CCS versions did not keep a version number field...
 */
		if( Examine( lock, fibPtr ) ) {
			size = fibPtr->fib_Size;
			file = Open(fileName, MODE_OLDFILE);
			if( file != NULL ) {
				if( size == sizeof( Params ) ) {
/*
 * Old version
 */
 					if( stricmp(fileName, oldPrefsFileName) == 0 ) {
						success = Read( file, (BYTE *) &prefs.OldPrefs, size ) == size;
					}
				} else {
					size = MIN(sizeof(ProgPrefs), size);
/*
 * New version, so check to make sure it's a prefs file
 */				
					if( (Read( file, &prefsHeader[0], 6) == 6 ) && 
						(*((ULONG *)&prefsHeader[0]) == ID_PREFSFILE) &&
						(*((UWORD *)&prefsHeader[4]) == QB_PREFS_VERSION) ) {
						BlockMove(prefsHeader, &prefs, 6);
						success = Read( file, &prefs.OldPrefs, size - 6) == (size - 6);
					}
				}
				if( success ) {
					ValidatePrefs();
					PrValidate(&prefs.PrintInfo.PrintRec);
				}
				Close(file);
			}
		}
		UnLock(lock);
	}
	return (success);
}

/*
 *	Return TRUE if file is a defaults file
 */

BOOL IsPrefsFile(TextPtr fileName)
{
	ULONG fileID;
	UWORD version;
	BOOL success;
	File file;

	if ((file = Open(fileName, MODE_OLDFILE)) == NULL)
		return (FALSE);
	success = FALSE;
	if (Read(file, &fileID, sizeof(ULONG)) == sizeof(ULONG) &&
		fileID == ID_PREFSFILE &&
		Read(file, &version, sizeof(UWORD)) == sizeof(UWORD) &&
		version == QB_PREFS_VERSION)
		success = TRUE;
	Close(file);
	return (success);
}
/*
 *	Load QB preference settings
 */

BOOL GetProgPrefs(TextPtr fileName)
{
	TextChar pathNameBuff[DEVICE_PATH_BUFFER_SIZE];
	BOOL success;
	
	SetPathName(pathNameBuff, fileName, TRUE);
	success = ReadProgPrefs(pathNameBuff);
	return(success);
}

/*
 * Load QB preference settings
 * If no prefs file found, set to standard defaults
 */
 
void GetStdProgPrefs()
{
	TextChar pathNameBuff[DEVICE_PATH_BUFFER_SIZE];
	
/*
	In case old prefs exist, make sure new fields contain good defaults.
*/
	SetStdDefaults();
	SetPathName(pathNameBuff, strPrefsName, TRUE);
	if( !ReadProgPrefs(pathNameBuff) ) {
		SetStdDefaults();						/* In case partial read was done */
		ReadProgPrefs(oldPrefsFileName);
	}
	if( noSCSI ) {
		if( prefs.Source == DEST_TAPE ) {
			prefs.Source = DEST_FLOPPY;
		}
		if( prefs.Destination == DEST_TAPE ) {
			prefs.Destination = DEST_FLOPPY;
		}
	}
}

/*
 *	Save QB preferences settings from given document
 *	Return success status
 */

BOOL SaveProgPrefs(TextPtr fileName)
{
	register File file;
	register BOOL success;
	UBYTE pathNameBuff[DEVICE_PATH_BUFFER_SIZE];

	SetStdPointer(cmdWindow, POINTER_WAIT);	
	prefs.FileID = ID_PREFSFILE;
	prefs.Version = QB_PREFS_VERSION;
	SetRect( (Rectangle *)&prefs.OldPrefs.WindowPos[0], 0, 0, cmdWindow->Width-1, cmdWindow->Height-1);
	OffsetRect( (Rectangle *)&prefs.OldPrefs.WindowPos[0], cmdWindow->LeftEdge, cmdWindow->TopEdge);

	success = FALSE;
	if( fileName != NULL ) {
		strcpy(pathNameBuff, fileName);
	} else {
		SetPathName(pathNameBuff, strPrefsName, TRUE);
	}
	if ((file = Open(pathNameBuff, MODE_NEWFILE)) != NULL) {
		success = Write(file, (BYTE *) &prefs, sizeof(prefs)) == sizeof(prefs);
		Close(file);
		if (success) {
			SaveIcon(pathNameBuff, ICON_PREFS);
		}
	}
	SetArrowPointer();
	return (success);
}
