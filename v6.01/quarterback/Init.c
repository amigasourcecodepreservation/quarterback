/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1992-93 New Horizons Software, Inc.
 *
 *	Initialization
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <graphics/gfxbase.h>
#include <graphics/displayinfo.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>
#include <workbench/startup.h>
#include <workbench/workbench.h>
#include <libraries/dos.h>
#include <libraries/dosextens.h>
#include <resources/disk.h>
#include <exec/memory.h>
#include <exec/tasks.h>
#include <dos/dostags.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>
#include <proto/icon.h>

#include <clib/alib_protos.h>

#include <string.h>
#include <stdlib.h>				// For exit() prototype

#include <Toolbox/Globals.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/DOS.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Image.h>
#include <Toolbox/List.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Screen.h>
#include <Toolbox/SerialInfo.h>
#include <Toolbox/StdFile.h>	// For ConvertFileName() prototype
#include <Toolbox/StdInit.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>

#include "QB.h"
#include "DlogDefs.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct Library	*DiskBase;

extern TextChar		progPathName[];

extern BOOL				fromCLI;

extern WORD				intuiVersion;

extern struct WBStartup	*WBenchMsg;
extern struct NewScreen	newScreen;

extern MenuTemplate		docWindMenus[];

extern MenuPtr			menuStrip;
extern ScreenPtr		screen;
extern WindowPtr		cmdWindow, backWindow;
extern MsgPortPtr		mainMsgPort;

extern GadgetTemplate	windowGadgets[];

extern TextChar		strProgName[];

extern TextPtr			initErrors[];

extern DlgTemplPtr		dlgList[];

extern WORD				charHeight, charWidth, charBaseline;
extern WORD				numColors;

extern ProcessPtr 		process;

extern MsgPortPtr		keyPort;
extern struct IOStdReq *keyRequest;

extern TextPtr			strPath, strSavePath, destFileName;
extern TextChar		strBuff[], strPrefsName[], strDefLogName[], strPortName[];
extern TextChar		strBackupFilter[], strRestoreFilter[];
extern ListHeadPtr		devList, backupFilterList, restoreFilterList, volumeList;
extern ListHeadPtr		devProcVolList;
extern MsgPortPtr		*devProcIDArray;

extern ProgPrefs		prefs;

extern UBYTE			mode, operation;
extern Ptr				fib;
extern BOOL				noSCSI, noAutoSense;

/*
 *	Local variables and definitions
 */

static struct IOStdReq	consoleIOReq;

static struct Task 		*monitorTask;

/*
 *	Local prototypes
 */

static void FindActiveSCSIDevice(void);
static void	DoInitError(WORD);
static void	WaitVisitor(void);
static BOOL	IsPubScreen(ScreenPtr);
static RGBColor MixColors2(RGBColor, RGBColor);
static void	ReadQBOptions(int, char **);
static void	DoQBOption(TextPtr);
static BOOL	MatchOptions(TextPtr, TextPtr, TextPtr);	
#ifdef USE_MY_COLOR_TABLE
static void BuildDefaultColorTable(void);

/*
 *	Actual colors for dot-matrix printer ribbon (to match screen colors)
 *	Screen colors for red, green, and blue are derived from these
 */

#define PRT_RGBCOLOR_CYAN		0x3AC
#define PRT_RGBCOLOR_MAGENTA	0xE07
#define PRT_RGBCOLOR_YE LLOW	0xFE5
#endif

#define INIT_DLG_WAIT	3000

/*
 *	Call Toolbox InitError() and abort mission
 *	This routine does not return
 */
 
static void DoInitError(WORD errNum)
{
	InitError(initErrors[errNum]);
	ShutDown();
	exit(RETURN_FAIL);
}

/*
 *	Initialization routine
 *	Open necessary libraries and devices, and open background window
 */

void Init(int argc, char *argv[])
{
	register WORD	i, arrowWidth, arrowHeight;
	WORD			barHeight;
	LONG			delay;
	ULONG			sec1, micro1, sec2, micro2;
	TextPtr			progName, userName, companyName, serialNum;
	register Dir	dirLock;
	struct TagItem	tag;
	DialogPtr		initDlg;
	GadgetPtr		gadgList;

	fromCLI = (argc > 0);
	process = (ProcessPtr) FindTask(NULL);
/*
	Build path name to program
	(Used to find program icon, prefs file and dictionary)
*/
	if (!StdInit(OSVERSION_1_2)) {
		ShutDown();
		exit(RETURN_FAIL);
	}
	progName = (argc) ? argv[0] : WBenchMsg->sm_ArgList->wa_Name;
	dirLock = ConvertFileName(progName);
	BuildPath(progName, progPathName, dirLock, GADG_MAX_STRING-2);
	UnLock(dirLock);

	strPath = NewPtr(PATHSIZE);
	strSavePath = NewPtr(PATHSIZE);
	destFileName = NewPtr(PATHSIZE);

	if (strPath == NULL || strSavePath == NULL || destFileName == NULL)
		DoInitError(INIT_NOMEM);

	strPath[0] = 0;

	if ((mainMsgPort = CreatePort(strPortName, 0)) == NULL)
		DoInitError(INIT_NOMEM);

	BlockClear(&prefs, sizeof(ProgPrefs));
	dirLock = DupLock(GetCurrentDir());
	BuildPath(strDefLogName, prefs.LogOpts.LogFileName, dirLock, GADG_MAX_STRING-2);
	BuildPath("QB Backup", destFileName, dirLock, GADG_MAX_STRING-2);
	UnLock(dirLock);
/*
	Open needed libraries
*/
	DiskBase = (struct Library *) OpenResource(DISKNAME);
	if (DiskBase == NULL)
		DoInitError(INIT_BADSYS);
	intuiVersion = LibraryVersion(IntuitionBase);
/*
	For pre-2.0 system, need to get "ALT" keys through keyboard device.
*/	
	if (intuiVersion < OSVERSION_2_0) {
		if ((keyPort = CreatePort(NULL, NULL)) == NULL ||
			(keyRequest = (struct IOStdReq *) CreateExtIO(keyPort, sizeof(struct IOStdReq))) == NULL)
			DoInitError(INIT_NOMEM);
		if (OpenDevice("keyboard.device", NULL, (IOReqPtr) keyRequest, NULL)) {
			DeleteExtIO((IOReqPtr) keyRequest);
			keyRequest = NULL;
			DoInitError(INIT_NOMEM);
		}
	}
	ReadQBOptions(argc, argv);

#ifdef USE_MY_COLOR_TABLE	
	BuildDefaultColorTable();
#endif
/*
	Open screen (or get pointer to workbench screen)
*/
	_tbSmartWindows = TRUE;
	if ((screen = GetScreen(argc, argv, &newScreen, NULL, NUM_STDCOLORS, strProgName)) == NULL)
		DoInitError(INIT_NOSCREEN);
	InitToolbox(screen);
/*
	Open background window and display init dialog
*/
	numColors = 1 << screen->BitMap.Depth;
	if ((backWindow = OpenBackWindow()) == NULL)
		DoInitError(INIT_NOMEM);
/*
	Adjust parameters for document windows and gadgets
*/	
	charHeight = screen->RastPort.Font->tf_YSize;
	charBaseline = screen->RastPort.Font->tf_Baseline;
	charWidth = screen->RastPort.Font->tf_XSize;
	barHeight = screen->WBorTop + screen->Font->ta_YSize + 1;
	arrowWidth = ARROW_WIDTH;
	arrowHeight = ARROW_HEIGHT;
	if (intuiVersion < OSVERSION_2_0) {
		arrowWidth -=2 ;
		arrowHeight -= 1;
		fib = NewPtr(sizeof(struct FileInfoBlock));
	}
	else {
		tag.ti_Tag = TAG_END;
		fib = AllocDosObject(DOS_FIB, &tag);
	}

	if (fib == NULL									||
		!AllocMiniIcons()							||
		(backupFilterList = CreateList()) == NULL	||
		(restoreFilterList = CreateList()) == NULL	||
		(volumeList = CreateList()) == NULL			||
		(devProcVolList = CreateList()) == NULL) {
		DoInitError(INIT_NOMEM);
	}
	for (i = UP_ARROW; i <= RIGHT_ARROW; i++) {
		windowGadgets[i].LeftOffset	= 1 - arrowWidth;
		windowGadgets[i].TopOffset		= 1 - arrowHeight;
		windowGadgets[i].WidthOffset	= arrowWidth;
		windowGadgets[i].HeightOffset	= arrowHeight;
	}
	windowGadgets[UP_ARROW].TopOffset		-= 2*arrowHeight;
	windowGadgets[DOWN_ARROW].TopOffset		-= arrowHeight;
	windowGadgets[LEFT_ARROW].LeftOffset	-= 2*arrowWidth;
	windowGadgets[RIGHT_ARROW].LeftOffset	-= arrowWidth;

	windowGadgets[VERT_SCROLL].LeftOffset		= 1 - arrowWidth;
	windowGadgets[VERT_SCROLL].TopOffset		= barHeight;
	windowGadgets[VERT_SCROLL].WidthOffset		= arrowWidth;
	windowGadgets[VERT_SCROLL].HeightOffset	= -3*arrowHeight - barHeight;
//	windowGadgets[HORIZ_SCROLL].LeftOffset		= BUTTONS_WIDTH;
	windowGadgets[HORIZ_SCROLL].TopOffset		= 1 - arrowHeight;
	windowGadgets[HORIZ_SCROLL].WidthOffset	= -3*arrowWidth /*- BUTTONS_WIDTH*/;
	windowGadgets[HORIZ_SCROLL].HeightOffset	= arrowHeight;
	if (intuiVersion >= OSVERSION_2_0) {
		windowGadgets[VERT_SCROLL].LeftOffset	+= 4;
		windowGadgets[VERT_SCROLL].TopOffset	+= 1;
		windowGadgets[VERT_SCROLL].WidthOffset	-= 8;
		windowGadgets[VERT_SCROLL].HeightOffset-= 2;
		windowGadgets[HORIZ_SCROLL].LeftOffset	+= 2;
		windowGadgets[HORIZ_SCROLL].TopOffset	+= 2;
		windowGadgets[HORIZ_SCROLL].WidthOffset-= 4;
		windowGadgets[HORIZ_SCROLL].HeightOffset-= 4;
		if (numColors > 2) {
			windowGadgets[SCROLL_LIST].TopOffset += 1;
			windowGadgets[SCROLL_LIST].HeightOffset -= 1;
		}
	}
	else {
		windowGadgets[VERT_SCROLL].TopOffset	-= 1;
		windowGadgets[VERT_SCROLL].HeightOffset	+= 1;
	}
/*
	Have system requesters appear in this screen
*/
	SetSysRequestWindow(backWindow);
/*
	Set up menus
*/
	if ((menuStrip = GetMenuStrip(docWindMenus)) == NULL)
		DoInitError(INIT_NOMEM);
	InsertMenuStrip(backWindow, menuStrip);

	if (_tbOnPubScreen && intuiVersion >= OSVERSION_2_0)
		UnlockPubScreen(NULL, screen);				// Recommended procedure

#if 0
	if (!GetSerialInfo(&userName, &companyName, &serialNum, screen, mainMsgPort,
		strProgName, PROGRAM_VERSION, progPathName)) {
		ShutDown();
		exit(RETURN_FAIL);
	}
#else
	userName = companyName = serialNum = "-";
#endif

	dlgList[DLG_ABOUT]->Gadgets++;							// Past OK button
	initDlg = GetDialog(dlgList[DLG_ABOUT], screen, mainMsgPort);
	dlgList[DLG_ABOUT]->Gadgets--;							// Restore...
	if (initDlg == NULL)
		DoInitError(INIT_NOMEM);
	gadgList = initDlg->FirstGadget;
	SetGadgetItemText(gadgList, USERNAME_STATTEXT-1, initDlg, NULL, userName);
	SetGadgetItemText(gadgList, COMPANYNAME_STATTEXT-1, initDlg, NULL, companyName);
	SetGadgetItemText(gadgList, SERIALNUM_STATTEXT-1, initDlg, NULL, serialNum);
	SetStdPointer(initDlg, POINTER_WAIT);
	CurrentTime(&sec1, &micro1);
/*
	Build our own list of possible backup/restore exec devices.
*/
	if ((devList = BuildExecDeviceList()) == NULL) {
		DisposeDialog(initDlg);
		DoInitError(INIT_NOMEM);
	}
/*
	Misc initialization
*/
	mode = operation = MODE_LIMBO;
	SetAllMenus();					// Must do before BeginWait()
	mode--;
	BeginWait();
	GetSysPrefs();
	LoadFKeys();					// Load Macro function key definition file

	InitPrintHandler();
	GetStdProgPrefs();				// Get Prefs file here to minimize disk swapping
	InitRexx();
	SetMacroMenu();
	InitIcons();

	FindActiveSCSIDevice();
	(void) FloppyInit();

	LoadFilter(backupFilterList, strBackupFilter);
	LoadFilter(restoreFilterList, strRestoreFilter);

	CurrentTime(&sec2, &micro2);
	delay = (TICKS_PER_SECOND*(INIT_DLG_WAIT - DiffTimeMilli(sec1, micro1, sec2, micro2)))/1000L;
	if (delay > 0)
		Delay(delay);
	DisposeDialog(initDlg);
	InitPosition();
	SetStdPointer(backWindow, POINTER_ARROW);
	EndWait();
	if ((cmdWindow = OpenOperationWindow()) == NULL)
		DoInitError(INIT_NOWIN);
	SetModeOp(MODE_LIMBO, OPER_LIMBO);
}

/*
 * Find the first SCSI device connected, initialize tape drive parameters.
 */
 
static void FindActiveSCSIDevice()
{
	UBYTE	dType = 255;
	WORD	i;
	
	if (!(CheckSCSIDrive(prefs.CurrBackupDev, prefs.CurrBackupUnit, strBuff, FALSE) && IS_REMOVABLE(strBuff))) {
		for (i = 0; i < 6; i++) {
			if (CheckSCSIDrive(prefs.CurrBackupDev, i, strBuff, FALSE) && IS_REMOVABLE(strBuff)) {
				prefs.CurrBackupUnit = i;
				dType = strBuff[0];
				break;
			}
		}
	}
	else
		dType = strBuff[0];
/*
	Make the restore set the same as the backup if the restore parameters are invalid,
	but only do the check if the parameters are different than backup (to save time).
*/
	if ((strcmp(prefs.CurrBackupDev, prefs.CurrRestoreDev)) ||
		(prefs.CurrBackupUnit != prefs.CurrRestoreUnit)) {
		if (!CheckSCSIDrive(prefs.CurrRestoreDev, prefs.CurrRestoreUnit, strBuff, FALSE)) {
			strcpy(prefs.CurrRestoreDev, prefs.CurrBackupDev);
			prefs.CurrRestoreUnit = prefs.CurrBackupUnit;
		}
	}
	InitDriveType(dType);
}
	
/*
 *	Wait until all visitor windows are closed on screen
 */

static void WaitVisitor()
{
	register WindowPtr window;

	if (screen != NULL && backWindow != NULL && (!_tbOnPubScreen)) {
		ModifyIDCMP(backWindow, REFRESHWINDOW);
		for (;;) {
			FlushMsgPort(mainMsgPort);
			for (window = screen->FirstWindow; window; window = window->NextWindow) {
				if (window != backWindow)
					break;
			}
			if (window == NULL) {		/* No visitor windows found */
				if (intuiVersion < OSVERSION_2_0 ||
					!IsPubScreen(screen) ||
					(PubScreenStatus(screen, PSNF_PRIVATE) & PSNF_PRIVATE))
					break;
			}
			Delay(10L);					 /* Wait 1/5 second before checking again */
		}
	}
}

/*
 *	Shut down program
 *	Close backWindow, close libraries, and quit
 */

void ShutDown()
{
	MsgPtr	msg;

	if (devList)
		DisposeList(devList);
	if (backupFilterList)
		DisposeList(backupFilterList);
	if (restoreFilterList)
		DisposeList(restoreFilterList);
	if (volumeList)
		DisposeList(volumeList);
	if (devProcVolList)
		DisposeList(devProcVolList);
	if (devProcIDArray)
		DisposePtr(devProcIDArray);
/*
 *	Perform pre-2.0 keyboard deallocation...
 */
	if (keyRequest) {
		CloseDevice((IOReqPtr) keyRequest);
		DeleteExtIO((IOReqPtr) keyRequest);
	}
	if (keyPort) {
		DeletePort(keyPort);
	}
	FreeMiniIcons();
	FreeBuffers();
	UnPatchGiveUnit();

	ShutDownIcons();
	ShutDownRexx();
	
	if (backWindow) {
		ClearSysRequestWindow();
		ClearMenuStrip(backWindow);
		WaitVisitor();
		CloseWindowSafely(backWindow, mainMsgPort);
	}
	if (strPath)
		DisposePtr(strPath);
	if (strSavePath)
		DisposePtr(strSavePath);
	if (destFileName)
		DisposePtr(destFileName);
	
	if (menuStrip)
		DisposeMenuStrip(menuStrip);
	if (screen)
		DisposeScreen(screen);
	
	if (fib) {
		if (intuiVersion >= OSVERSION_2_0)
			FreeDosObject(DOS_FIB, fib);
		else
			DisposePtr(fib);
	}
	if (mainMsgPort) {
		while((msg = GetMsg(mainMsgPort)) != NULL)
			ReplyMsg(msg);
		DeletePort(mainMsgPort);
	}
	StdShutDown();					// Call Toolbox to shut down all other stuff
}

/*
 * Load a file or drawer from the initial invocation or during an AppMsg
 * Pass NULL for the argList, if this is from a startup msg from a CLI prompt.
 */
 
BOOL LoadFileOrDrawer(TextPtr fileName, Dir dir)
{
	register WORD	len;
	BOOL			success;
	
	len = strlen(fileName);
	if (success = len <= MAX_FILENAME_LEN) {
		if (mode != MODE_LIMBO) {
			if (operation == OPER_SELECTION) {
				HandleOperationButton(FALSE, FALSE);
			}
		}
		if (mode == MODE_LIMBO) {
			if (len == 0) {
				BuildPath(fileName, strBuff, dir, PATHSIZE);
				strcpy(strPath, strBuff);
				LoadDirectory(MODE_BACKUP, FALSE);
				success = FALSE;
			}
			else
				DoOpenFile(fileName, dir);
		}
	}
	UnLock(dir);
	return(success);
}

/*
 *	Open initial files, or open new window if none given
 */

void SetUp(int argc, char *argv[])
{
	register WORD		i, numFiles;
	struct WBArg		*wbArgList;
	register TextPtr	fileName;
	Dir					dir, startupDir;
/*
	Get number of files to open
*/
	if (argc) {						// Running under CLI
		numFiles = argc;
		wbArgList = NULL;
	}
	else {							// Running under Workbench
		numFiles = WBenchMsg->sm_NumArgs;
		wbArgList = WBenchMsg->sm_ArgList;
	}
	startupDir = DupLock(GetCurrentDir());
/*
	Open files
	First check to see if this is a prefs file, if so then load prefs
*/
	for (i = 1; i < numFiles; i++) {
		if (wbArgList == NULL) {
			fileName = argv[i];
			if (*fileName == '-') {			// Ignore program options
				continue;
			}
			else if (IsMacroFile(fileName))
				break;						// All following params are for macro
			else {
				SetCurrentDir(startupDir);	// Path is relative to startupDir
				dir = ConvertFileName(fileName);
			}
		}
		else {								// Running under Workbench
			fileName = wbArgList[i].wa_Name;
			dir = DupLock(wbArgList[i].wa_Lock);
		}
		LoadFileOrDrawer(fileName, dir);
	}
	UnLock(startupDir);
}

#ifdef USE_MY_COLOR_TABLE
/*
 *	Build default screen color tables
 */

static void BuildDefaultColorTable()
{
/*
	Here, we just need 8 colors -
	Dark gray, white, green, yellow, red, brown, black, light gray.
*/
	stdColors[0]				= RGBCOLOR_BLACK;
	stdColors[1]				= RGBCOLOR_WHITE;
	stdColors[2]				= MixColors2(PRT_RGBCOLOR_CYAN, PRT_RGBCOLOR_YELLOW);
	stdColors[3]				= PRT_RGBCOLOR_YELLOW;	/* For yellow icons */
	stdColors[4]				= MixColors2(PRT_RGBCOLOR_MAGENTA, PRT_RGBCOLOR_YELLOW);
	stdColors[5]				= RGBCOLOR(6,  2,  0);	/* Brown football */
	stdColors[6]				= /*RGBCOLOR_BLACK;*/ RGBCOLOR(4,6,8);
	stdColors[7]				= /*RGBCOLOR(10, 10, 10);*/RGBCOLOR(11,10,9);
}
#endif

/*
 *	Do subtractive mix of two colors, returning result
 */

static RGBColor MixColors2(register RGBColor color1, register RGBColor color2)
{
	register WORD red, green, blue;

	red		= (RED(color1)  *RED(color2)   + 7)/15;
	green	= (GREEN(color1)*GREEN(color2) + 7)/15;
	blue	= (BLUE(color1) *BLUE(color2)  + 7)/15;
	return (RGBCOLOR(red, green, blue));
}

/*
 * Read all QB-specific tooltype parameters
 */
 
static void ReadQBOptions(int argc, char *argv[])
{
	register WORD i;
	register TextPtr text;
	struct DiskObject *icon;
	struct WBStartup *wbMsg;
	
	if (argc) {						// Running under CLI
		for (i = 1; i < argc; i++) {
			text = argv[i];
			if (*text++ == '-')
				DoQBOption(text);
		}
	}
	else {						// Running under Workbench
		wbMsg = (struct WBStartup *) argv;
		if ((icon = GetDiskObject(wbMsg->sm_ArgList->wa_Name)) != NULL) {
			if (icon->do_ToolTypes) {
				for (i = 0; (text = icon->do_ToolTypes[i]) != NULL; i++)
					DoQBOption(text);
			}
			FreeDiskObject(icon);
		}
	}
}

/*
 * Initialize QB-specific tooltype parameters.
 */
 
static void DoQBOption(TextPtr text)
{
	if (MatchOptions(text, "NoSCSI", "ns"))
		noSCSI = TRUE;
	if (MatchOptions(text, "NoAutoSense", "na"))
		noAutoSense = TRUE;
}

/*
 *	Match screen options
 */

static BOOL MatchOptions(register TextPtr text, TextPtr opt1, TextPtr opt2)
{
	register WORD len;
/*
	Get command length
*/
	for (len = 0; text[len] && text[len] != ':' && text[len] != '='; len++) ;
	if (text[len])
		len++;
/*
	Check for match
*/
	return ((BOOL) (CmpString(text, opt1, len, strlen(opt1), FALSE) == 0 ||
					CmpString(text, opt2, len, strlen(opt2), FALSE) == 0));
}
