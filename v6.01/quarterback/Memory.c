/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Memory handling routines
 */

#include <Toolbox/Memory.h>

#include <proto/exec.h>

/*
 *	Local variables and definitions
 */

#define CHUNK_SIZE	8	/* Physical alloc chunks when allocating handles */

typedef struct {
	LONG	Size;		/* Actual data follows */
} PtrHdr;

/*
 *	NewPtr
 *	Allocate a new Ptr, return NULL if size <= 0 or error
 */

Ptr NewPtr(LONG size)
{
	register PtrHdr *ptrHdr;

	if (size > 0 &&
		(ptrHdr = MemAlloc(size + sizeof(PtrHdr), 0)) != NULL) {
		ptrHdr->Size = size;
		return ((Ptr) (ptrHdr + 1));
	}
	return (NULL);
}

/*
 *	DisposePtr
 *	Free memory allocated by Ptr
 */

void DisposePtr(Ptr ptr)
{
	register PtrHdr *ptrHdr = ptr;

	if (ptrHdr--)
		MemFree(ptrHdr, ptrHdr->Size + sizeof(PtrHdr));
}

/*
 *	GetPtrSize
 *	Return the logical size of Ptr
 *	Return 0 if Ptr is NULL
 */

LONG GetPtrSize(Ptr ptr)
{
	register PtrHdr *ptrHdr = ptr;

	return ((ptrHdr--) ? ptrHdr->Size : 0);
}
