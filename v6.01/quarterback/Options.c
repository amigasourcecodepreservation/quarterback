/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1992-93 New Horizons Software
 *
 *	Options menu functions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <rexx/errors.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/Request.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/List.h>
#include <Toolbox/Language.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>

#include <string.h>

#include "QB.h"
#include "Proto.h"
#include "Tape.h"
#include "DlogDefs.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;
extern ProcessPtr	process;

extern ProgPrefs	prefs;
extern DlgTemplPtr	dlgList[];
extern ReqTemplPtr	reqList[];

extern RequestPtr	requester;
extern UWORD		matchDateFormat[];
extern TextChar		strBuff[], strSaveADOS[], strOpenADOS[], strSaveLog[], strBitLevel[];
extern TextPtr		strsErrors[];
extern TextPtr		destFileName;

extern DirLevelPtr	curLevel;

extern WindowPtr	cmdWindow;
extern UBYTE		mode, operation, startMode, verifyMode, backupType;

extern BOOL			skipUpdate;
extern UBYTE		floppyDrives[], floppyEnable[];
extern ListHeadPtr	devList;

extern ULONG		bufMemType;
extern TextPtr		strDriveType[];
extern TextChar	strTpDrDATape[], strTpDrNonStd[];
extern TextChar	strComma[], strTpDrVar[], strTpDrFixed[];
extern TextChar	strTrackdisk[];

extern BOOL			tapeDevice, sequentialDevice;
extern WORD			numBufs;
extern WORD			checkVolCounter;
extern ScrollListPtr	scrollList;
extern BOOL			disableDisplay;
extern UWORD		sessionNumber;
extern BOOL			noSCSI;

/*
 *	Local variables and definitions
 */

#define NUM_BACKUP_DEST_OPTIONS (sizeof(backupDestNames)/sizeof(TextPtr))
#define NUM_BACKUP_OPTIONS		(sizeof(backupOptNames)/sizeof(TextPtr))
#define NUM_RESTORE_OPTIONS		(sizeof(restoreOptNames)/sizeof(TextPtr))
#define NUM_BUFFER_OPTIONS		(sizeof(buffOptNames)/sizeof(TextPtr))
#define NUM_TAPE_OPTIONS		(sizeof(tapeOptNames)/sizeof(TextPtr))
#define NUM_CATALOG_OPTIONS		(sizeof(catalogOptNames)/sizeof(TextPtr))
#define NUM_SESSION_LOG_OPTIONS	(sizeof(sessionOptNames)/sizeof(TextPtr))

/*
 * AREXX Backup/Restore Source/Destination Options
 */

static TextPtr backupDestNames[] = {
	"Floppy",
	"Tape",
	"File",
};

enum {
	OPT_FLOPPY_ON,	OPT_TAPE_ON,	OPT_FILE_ON
};

static TextPtr floppyEnableNames[] = {
	"DF0Off", "DF0On", "DF1Off", "DF1On", "DF2Off", "DF2On", "DF3Off", "DF3On",
};

TextChar strDisplayOptsOn[]	= "DisplayOptionsOn";
TextChar strDisplayOptsOff[]	= "DisplayOptionsOff";
TextChar strArchiveFlagOn[]	= "ArchiveFlagOn";
TextChar strArchiveFlagOff[]	= "ArchiveFlagOff";

/* 
 * AREXX Backup Options
 */

static TextPtr backupOptNames[] = {
	strDisplayOptsOn,	strDisplayOptsOff,
	strArchiveFlagOn, strArchiveFlagOff,
	"CompressOff",	"CompressOn", "CompressDevice", "CompressLevel", 
	"PasswordOn",	"PasswordOff",
	"VerifyDataOn",	"VerifyDataOff",
	"WarnDOSOn",	"WarnDOSOff",
	"EntireStructOn",	"EntireStructOff",
	"Complete",		"Selective",
};
 
enum {
	OPT_DISPLAY_OPTS_ON,	OPT_DISPLAY_OPTS_OFF,
	OPT_ARCHIVE_ON,		OPT_ARCHIVE_OFF,
	OPT_COMP_OFF,		OPT_COMP_ON,		OPT_COMP_DEVICE,	OPT_COMP_LEVEL,
	OPT_PASSWORD_ON,	OPT_PASSWORD_OFF,
	OPT_VERIFY_ON,		OPT_VERIFY_OFF,
	OPT_WARN_ON,		OPT_WARN_OFF,
	OPT_ENTIRE_ON,		OPT_ENTIRE_OFF,
	OPT_COMPLETE,		OPT_SELECTIVE,
};

/*
 * AREXX Restore options
 */
 
static TextPtr restoreOptNames[] = {
	strDisplayOptsOn,	strDisplayOptsOff,
	strArchiveFlagOn, strArchiveFlagOff,
	"Restore", "Test", "Compare",
	"ReplaceOn",	"ReplaceOff", "ReplacePrompt",
	"ReplaceEarlierOn", "ReplaceEarlierOff", "ReplaceEarlierPrompt",
	"ReplaceLaterOn",	"ReplaceLaterOff", "ReplaceLaterPrompt",
	"DateOriginal", "DateBackup", "DateCurrent",
	"EmptyDrawersOn", "EmptyDrawersOff",
	"KeepStructureOn", "KeepStructureOff",
};

enum {
	OPT_RESTORE = OPT_ARCHIVE_OFF + 1,
	OPT_TEST,	OPT_COMPARE,
	OPT_REPLACE_ON,	OPT_REPLACE_OFF,	OPT_REPLACE_PROMPT,
	OPT_REPLACE_EARLIER_ON, OPT_REPLACE_EARLIER_OFF, OPT_REPLACE_EARLIER_PROMPT,
	OPT_REPLACE_LATER_ON, OPT_REPLACE_LATER_OFF, OPT_REPLACE_LATER_PROMPT,
	OPT_DATE_ORIG,	OPT_DATE_BACKUP,	OPT_DATE_CURRENT,
	OPT_EMPTY_DIRS_ON,OPT_EMPTY_DIRS_OFF,
	OPT_KEEP_DIR_STRUCT_ON,	OPT_KEEP_DIR_STRUCT_OFF,
};

/*
 * AREXX Buffer options
 */
 
static TextPtr buffOptNames[] = {
	"BackupSize",		"RestoreSize",
	"AnyMem",			"Any24BitMem",		"GraphicsMem",
	"AsyncOn",			"AsyncOff",
};

enum {
	OPT_BACKUPBUFFSIZE,	OPT_RESTOREBUFFSIZE,
	OPT_ANYMEM,			OPT_ANY24BITMEM,	OPT_GFXMEM,
	OPT_ASYNC_ON,		OPT_ASYNC_OFF,
};

/*
 * AREXX Tape options
 */
 
static TextPtr tapeOptNames[] = {
	"AutoRetensionOn",	"AutoRetensionOff",
	"AutoRewindOn",		"AutoRewindOff",
};

enum {
	OPT_AUTO_RETENS_ON,	OPT_AUTO_RETENS_OFF,
	OPT_AUTO_REWIND_ON,	OPT_AUTO_REWIND_OFF
};

/*
 * AREXX Catalog options
 */
 
static TextPtr catalogOptNames[] = {
	"SizeOn",	"SizeOff",
	"ProtOn",	"ProtOff",
	"DateOn",	"DateOff",
	"TimeOn",	"TimeOff",
	"SecsOn",	"SecsOff",
	"AMPMOn",	"AMPMOff",
	"DateFormat",
	"SortByName",	"SortBySize",	"SortByDate",
	"DrawersFirstOn",	"DrawersFirstOff",
};

enum {
	OPT_SIZE_ON,	OPT_SIZE_OFF,
	OPT_PROT_ON,	OPT_PROT_OFF,
	OPT_DATE_ON,	OPT_DATE_OFF,
	OPT_TIME_ON,	OPT_TIME_OFF,
	OPT_SECS_ON,	OPT_SECS_OFF,
	OPT_AMPM_ON,	OPT_AMPM_OFF,
	OPT_DATE_FORMAT,
	OPT_SORT_BY_NAME,	OPT_SORT_BY_SIZE,	OPT_SORT_BY_DATE, /*OPT_SORT_NORMAL,*/
	OPT_DRAWERS_FIRST_ON,	OPT_DRAWERS_FIRST_OFF
};

/*
 * AREXX Session log options
 */
 
static TextPtr sessionOptNames[] = {
	"ErrorsOnly",	"ErrorsDrawersOnly",		"AllInfo",
	"NoDisplayOn",	"NoDisplayOff",
	"RateOn",		"RateOff",
	"CompressEffOn",	"CompressEffOff",
	"NoLogfile",	"AutoSaveLogfile",		"SaveLogfileAs",
};

enum {
	OPT_ERRORS_ONLY,	OPT_ERRORS_DIRS_ONLY,	OPT_FULL_INFO,
	OPT_NO_DISP_ON,	OPT_NO_DISP_OFF,
	OPT_RATE_ON,	OPT_RATE_OFF,
	OPT_COMP_EFF_ON,	OPT_COMP_EFF_OFF,
	OPT_NO_LOGFILE,	OPT_AUTO_LOGFILE,			OPT_SAVE_LOGFILE_AS
};

/*
 *	Local prototypes
 */

static BOOL	DoBackupSettings(void);
static BOOL DoRestoreSettings(void);
static BOOL	UpdateSCSIDrive(GadgetPtr, WORD);
static WORD	NumDrivesSelected(BOOL *);
static BOOL	OptionsRequestFilter(IntuiMsgPtr, WORD *);
static BOOL	LogfileRequestFilter(IntuiMsgPtr, WORD *);
static void	UpdateBufferArrows(BuffParamsPtr);
static void	UpdateCompBitArrows(WORD);
static BOOL	SetSourceOrDestination(TextPtr, BOOL);
static BOOL IsStrNumeric(TextPtr);

/*
 *	Simply return the number of floppy drives which can be used at the moment.
 */
 
static WORD NumDrivesSelected(BOOL *floppyEnableArray)
{
	register WORD	i, num;
	
	for (i = num = 0 ; i < 4 ; i++) {
		if (floppyEnableArray[i] && floppyDrives[i])
			num++;
	}
	return (num);
}
	
/*
 *	Test for different DEV or Unit # for tape/removables field.
 *	Return boolean TRUE if different, and replace variables.
 */
 
BOOL DifferentDev(GadgetPtr gadgList, TextPtr dev, WORD *unit)
{
	WORD		newUnit;
	BOOL		diff;
	TextChar	buff[MAX_FILENAME_LEN + 1];

	diff = FALSE;
	GetEditItemText(gadgList, UNIT_TEXT, buff);
	newUnit = StringToNum(buff);
	if (newUnit != *unit) {
		diff = TRUE;
		*unit = newUnit;
	}
	GetEditItemText(gadgList, DEVICE_TEXT, buff);
	if (strcmp(buff, dev)) {
		diff = TRUE;
		strcpy(dev, buff);
	}
	return (diff);
}
		
/*
 *	Backup/Restore options filter
 */
 
static BOOL OptionsRequestFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD	itemHit;
	register ULONG	class = intuiMsg->Class;

	if (intuiMsg->IDCMPWindow == cmdWindow && class == GADGETDOWN) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		switch (itemHit) {
		case DEVICE_TEXT:
		case UNIT_TEXT:
			ReplyMsg(intuiMsg);
			*item = TAPE_RADBTN;
			return (TRUE);
		case FILE_TEXT:
			ReplyMsg(intuiMsg);
			*item = FILE_RADBTN;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 * Adjust backup options
 */

BOOL DoBackupOptions()
{
	register WORD	item, dest, devListItem, count, temp;
	WORD			numListItems, oldUnit, backupTypeItem;
	BOOL			done, success, tooLate, display, newDest;
	BOOL			localFloppyEnable[4];
	RequestPtr		req;
	GadgetPtr		gadgList;
	SFReply			sfReply;
	TextChar		oldDev[MAX_FILENAME_LEN + 1];

	if (mode == MODE_RESTORE)
		return (FALSE);
	tooLate = (operation == OPER_PREP) || (operation == OPER_IN_PROG) || (operation == OPER_PAUSE);
	BeginWait();
	AutoActivateEnable(FALSE);
	req = DoGetRequest(REQ_BACKOPTS);
	AutoActivateEnable(TRUE);
	if (req == NULL) {
		Error(ERR_NO_MEM);
		EndWait();
		return (FALSE);
	}
	requester = req;
	OutlineOKButton(cmdWindow);
	gadgList = req->ReqGadget;
/*
	If it's too late to change the destination, disable those gadgets.
*/
	if (tooLate)
		OffGList(GadgetItem(gadgList, FLOPPY_RADBTN), cmdWindow, req, DISPLAY_BOX - FLOPPY_RADBTN);
	else if (noSCSI) {
		OffGList(GadgetItem(gadgList, TAPE_RADBTN), cmdWindow, req, 1);
		OffGList(GadgetItem(gadgList, DEVICE_TEXT), cmdWindow, req, FILE_TEXT - DEVICE_TEXT);
	}
/*
	Set current settings
*/
	if ((operation != OPER_LIMBO) && (operation != OPER_SCAN))
		OffGList(GadgetItem(gadgList, COMPLETE_RADBTN), cmdWindow, req, 2);
	UpdateDeviceList();
	numListItems = NumListItems(devList);
	if (numListItems < 2)
		OffGList(GadgetItem(gadgList, DEV_UPARROW), cmdWindow, req, 2);
	devListItem = FindListItem(devList, prefs.CurrBackupDev);
	if (devListItem == -1)
		devListItem = 0;
	count = -1;
	for (item = 0 ; item < 4 ; item++) {
		localFloppyEnable[item] = (BOOL) floppyEnable[item];
		if (!floppyDrives[item])
			OffGList(GadgetItem(gadgList, DF0_BOX + item), cmdWindow, req, 1);
		else if (floppyEnable[item])
			SetGadgetItemValue(gadgList, DF0_BOX + item, cmdWindow, req, TRUE);
	}
	backupTypeItem = prefs.OldPrefs.BackupType + COMPLETE_RADBTN;
	SetGadgetItemValue(gadgList, backupTypeItem, cmdWindow, req, TRUE);
	SetEditItemText(gadgList, DEVICE_TEXT, cmdWindow, req, prefs.CurrBackupDev);
	NumToString(prefs.CurrBackupUnit, strBuff);
	SetEditItemText(gadgList, UNIT_TEXT, cmdWindow, req, strBuff);
	SetEditItemText(gadgList, FILE_TEXT, cmdWindow, req, destFileName);
	oldDev[0] = 0;						// Let DifferentDev() initialize old vals
	item = dest = FLOPPY_RADBTN + prefs.Destination;
	newDest = TRUE;
	display = prefs.DisplayBackupOpts;
	SetGadgetItemValue(gadgList, DISPLAY_BOX, cmdWindow, req, display);
/*
	Handle requester
*/
	done = success = FALSE;
	do {
		if (newDest) {
			if (!tooLate) {
				SetGadgetItemValue(gadgList, dest, cmdWindow, req, FALSE);
				dest = item;
				SetGadgetItemValue(gadgList, dest, cmdWindow, req, TRUE);
			}
			SetStdPointer(cmdWindow, POINTER_WAIT);
			DifferentDev(gadgList, oldDev, &oldUnit);
			UpdateSCSIDrive(gadgList, item);
			SetArrowPointer();
			newDest = FALSE;
		}
		WaitPort(mainMsgPort);
		item = CheckRequest(mainMsgPort, cmdWindow, OptionsRequestFilter);
		switch (item) {
		case -1:
			if (DifferentDev(gadgList, oldDev, &oldUnit))
				count = 5;
			else if (count >= 0) {
				if (count == 0)
					UpdateSCSIDrive(gadgList, dest);
				count--;
			}
			break;
		case OK_BUTTON:
			if (dest == TAPE_RADBTN && UpdateSCSIDrive(gadgList, dest)) {
				Error(ERR_DEATH_WISH);
				item = FILE_RADBTN;
				newDest = TRUE;
			} 
			else if (dest == FLOPPY_RADBTN &&
					 (NumDrivesSelected(&localFloppyEnable[0]) == 0))
				Error(ERR_NO_DRIVES);
			else
				success = done = TRUE;
			break;
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case OPTIONS_BUTTON:
			DoBackupSettings();
			break;
		case DISPLAY_BOX:
			ToggleCheckbox(&display, item, cmdWindow);
			break;
		case FLOPPY_RADBTN:
		case TAPE_RADBTN:
		case FILE_RADBTN:
			newDest = TRUE;
			break;
		case UNIT_UPARROW:
		case UNIT_DNARROW:
			GetEditItemText(gadgList, UNIT_TEXT, strBuff);
			temp = StringToNum(strBuff);
			if (item == UNIT_DNARROW) {
				if (temp)
					temp--;
				else
					temp = 999;
			}
			else {
				if (temp < 999)
					temp++;
				else
					temp = 0;
			}
			NumToString(temp, strBuff);
			SetEditItemText(gadgList, UNIT_TEXT, cmdWindow, req, strBuff);
			item = TAPE_RADBTN;
			newDest = TRUE;
			break;
		case DEV_UPARROW:
		case DEV_DNARROW:
			if (item == DEV_UPARROW) {
				devListItem++;
				if (devListItem == numListItems)
					devListItem = 0;
			}
			else {
				if (devListItem == 0)
					devListItem = numListItems;
				devListItem--;
			}
			SetEditItemText(gadgList, DEVICE_TEXT, cmdWindow, req, GetListItem(devList, devListItem));
			item = TAPE_RADBTN;
			newDest = TRUE;
			break;
		case DF0_BOX:
		case DF1_BOX:
		case DF2_BOX:
		case DF3_BOX:
			if (!tooLate) {
				ToggleCheckbox(&localFloppyEnable[item-DF0_BOX], item, cmdWindow);
				if (dest != FLOPPY_RADBTN) {
					item = FLOPPY_RADBTN;
					newDest = TRUE;
				}
			}
			break;
		case CHOOSE_BTN:
			if (!tooLate) {
				GetEditItemText(gadgList, FILE_TEXT, strBuff);
				if (DoStdPutFile(&sfReply, strSaveADOS, strBuff)) {
					SetEditItemText(gadgList, FILE_TEXT, cmdWindow, req, strBuff);
					item = FILE_RADBTN;
					newDest = TRUE;
				}
			}
			break;
		case SELECTIVE_RADBTN:
		case COMPLETE_RADBTN:
			SetGadgetItemValue(gadgList, backupTypeItem, cmdWindow, req, FALSE);
			backupTypeItem = item;
			SetGadgetItemValue(gadgList, backupTypeItem, cmdWindow, req, TRUE);
			break;
		}
	} while (!done);
	if (success) {
		prefs.FloppyEnableMask &= 0xF0;
		for (item = 0 ; item < 4 ; item++) {
			if (floppyEnable[item] = (UBYTE) localFloppyEnable[item])
				prefs.FloppyEnableMask |= (1 << item);
		}
		prefs.Destination = dest - FLOPPY_RADBTN;
		prefs.OldPrefs.BackupType = backupTypeItem - COMPLETE_RADBTN;
		if (operation == OPER_SCAN)
			backupType = prefs.OldPrefs.BackupType;
		prefs.DisplayBackupOpts = display;
		GetEditItemText(gadgList, FILE_TEXT, destFileName);
		GetEditItemText(gadgList, DEVICE_TEXT, prefs.CurrBackupDev);
		GetEditItemText(gadgList, UNIT_TEXT, strBuff);
		prefs.CurrBackupUnit = StringToNum(strBuff);
	}
	DestroyRequest(req);
	EndWait();
	return (success);
}

/*
 *	Invoke requester to do backup settings
 */
 
static BOOL DoBackupSettings()
{
	register WORD item;
	register WORD compressItem;
	register RequestPtr req;
	register DialogPtr window = cmdWindow;
	register BOOL success = FALSE;
	register WORD compressBitLevel;
	register UBYTE compress;
	BOOL done = FALSE;
	BOOL reformat, password, archive, verify, empty;
	
	req = DoGetRequest(REQ_BACKOPTS2);
	if (req == NULL)
		Error(ERR_NO_MEM);
	else {
		requester = req;
		OutlineOKButton(window);
/*
	Set compression bit level setting
*/
		item = prefs.Compress & COMP_BIT_MASK;
		if (item > MAX_COMPBIT)
			item = MAX_COMPBIT;
		else if (item < MIN_COMPBIT)
			item = MIN_COMPBIT;
		compressBitLevel = item;
		UpdateCompBitArrows(item);

		if (reformat = !prefs.OldPrefs.Reformat)
			GadgetOn(window, REFORMAT_BOX);
		if (password = prefs.Password)
			GadgetOn(window, PASSWORD_BOX);
		if (archive = prefs.OldPrefs.BSetArchBit)
			GadgetOn(window, ARCHIVE_BOX);
		if (empty = prefs.OldPrefs.BAllDirs)
			GadgetOn(window, ALLDIRS_BOX);
		compress = prefs.Compress;
		if (compress & COMP_ON_FLAG)
			compressItem = COMP_LEVEL;
		else if (compress & COMP_DEV_FLAG)
			compressItem = COMP_DEV;
		else
			compressItem = COMP_OFF;
		GadgetOn(window, compressItem);
		
		if (tapeDevice && !sequentialDevice && sessionNumber)
			EnableGadgetItem(req->ReqGadget, VERIFY_BOX, window, req, FALSE);
		else {
			if (verify = prefs.OldPrefs.Verify)
				GadgetOn(window, VERIFY_BOX);
		}
		
		do {
			item = ModalRequest(mainMsgPort, window, DialogFilter);
			switch (item) {
				case OK_BUTTON:
					success = TRUE;
				case CANCEL_BUTTON:
					done = TRUE;
					break;
				case COMPBIT_UPARROW:
				case COMPBIT_DNARROW:
					if (item == COMPBIT_UPARROW) {
						if (compressBitLevel < MAX_COMPBIT)
							compressBitLevel++;
					}
					else {
						if (compressBitLevel > MIN_COMPBIT)
							compressBitLevel--;
					}
					UpdateCompBitArrows(compressBitLevel);
					if (compressItem == COMP_LEVEL) {
						break;
					}
					item = COMP_LEVEL;
				case COMP_OFF:
				case COMP_DEV:
				case COMP_LEVEL:
					GadgetOff(window, compressItem);
					compressItem = item;
					GadgetOn(window, item);
					break;
				case ARCHIVE_BOX:
					ToggleCheckbox(&archive, item, window);
					break;
				case REFORMAT_BOX:
					ToggleCheckbox(&reformat, item, window);
					break;
				case VERIFY_BOX:
					ToggleCheckbox(&verify, item, window);
					break;
				case PASSWORD_BOX:
					ToggleCheckbox(&password, item, window);
					break;
				case ALLDIRS_BOX:
					ToggleCheckbox(&empty, item, window);
					break;
			}
		} while (!done);
		if (success) {
			compress = compressBitLevel;
			if (compressItem != COMP_OFF) {
				if (compressItem == COMP_DEV)
					compress |= COMP_DEV_FLAG;
				else
					compress |= COMP_ON_FLAG;
			}
			prefs.Compress = compress;
			prefs.Password = password;
			prefs.OldPrefs.BSetArchBit = archive;
			prefs.OldPrefs.Reformat = !reformat;
			prefs.OldPrefs.Verify = verify;
			prefs.OldPrefs.BAllDirs = empty;
		}
		DestroyRequest(req);
	}
	return (success);
}

/*
 * Update the text field which contains the name of the SCSI device.
 */

static BOOL UpdateSCSIDrive(GadgetPtr gadgList, WORD item)
{
	TextPtr str;
	BOOL forbiddenDest = FALSE;
	TextChar buff[GADG_MAX_STRING];
	WORD unit;
	
	InitDriveType((BYTE) -1);
	if (item != TAPE_RADBTN)
		str = NULL;
	else {
		if (DoCheckSCSIDrive(gadgList, &unit, buff, strBuff, TRUE)) {
			str = &strBuff[8];
/*
	Put a space between the vendor and model if none there already.
*/
			if (strBuff[15] != ' ') {
				BlockMove(&strBuff[16], &strBuff[17], strlen(&strBuff[16])+1);
				strBuff[16] = ' ';
			}
			if (*str) {
				if (forbiddenDest = !IS_REMOVABLE(strBuff)) {
					if ((strBuff[0] == 1) || (strBuff[0] == 16)) {
						forbiddenDest = WarningDialog(ERR_STRANGE_DRIVE, TRUE);
					}
				}
				if (!forbiddenDest) {
					InitDriveType(strBuff[0]);
				}
			}
		}
		else
			str = strsErrors[ERR_NO_SCSI_DRIVE];
	}
/*
	This extra code just eliminates all the unnecessary flicker.
	We could have just blindly called SetGadgetItemText().
*/
	GetGadgetItemText(gadgList, DRIVE_TEXT, buff);
	if ((str == NULL) || (*str == 0) || strcmp(buff, str))
		SetGadgetItemText(gadgList, DRIVE_TEXT, cmdWindow, requester, str);
	return (forbiddenDest);
}

/*
 * Reads current tape option requester values and calls CheckSCSIDrive().
 * NOTE: "devPtr" must hold GADG_MAX_STRING worth, just in case, and
 * "devPtr" must not be the same as "destPtr"!
 */
 
BOOL DoCheckSCSIDrive(GadgetPtr gadgList, WORD *unitPtr, TextPtr devPtr, UBYTE *destPtr, BOOL noRev)
{
	TextChar buff[GADG_MAX_STRING];
	
	GetEditItemText(gadgList, UNIT_TEXT, buff);
	*unitPtr = (WORD) StringToNum(buff);
	GetEditItemText(gadgList, DEVICE_TEXT, devPtr);
	return (CheckSCSIDrive(devPtr, *unitPtr, destPtr, noRev));
}

/*
 *	Adjust restore options
 */

BOOL DoRestoreOptions()
{
	register WORD	item, temp, devListItem, count;
	WORD			src, restoreMode, numListItems, oldUnit;
	BOOL			done, success, tooLate, display, newSrc;
	BOOL			localFloppyEnable[4];
	RequestPtr		req;
	GadgetPtr		gadgList;
	SFReply			sfReply;
	TextChar		oldDev[MAX_FILENAME_LEN + 1];
	UBYTE			oldVerifyMode = verifyMode;
	
	if (mode == MODE_BACKUP)
		return (FALSE);
	tooLate = (operation == OPER_PREP) || (operation == OPER_PAUSE) || (operation == OPER_IN_PROG) ||
				((mode == MODE_RESTORE) && (operation == OPER_SELECTION));
	BeginWait();
	AutoActivateEnable(FALSE);
	req = DoGetRequest(REQ_RESTOPTS);
	AutoActivateEnable(TRUE);
	if (req == NULL) {
		Error(ERR_NO_MEM);
		EndWait();
		return (FALSE);
	}
	requester = req;
	OutlineOKButton(cmdWindow);
	gadgList = req->ReqGadget;
/*
	If it's too late to change the source, disable those gadgets.
*/
	if (tooLate)
		OffGList(GadgetItem(gadgList, FLOPPY_RADBTN), cmdWindow, req, UNIT_LABEL - FLOPPY_RADBTN + 1);
	else if (noSCSI) {
		OffGList(GadgetItem(gadgList, TAPE_RADBTN), cmdWindow, req, 1);
		OffGList(GadgetItem(gadgList, DEVICE_TEXT), cmdWindow, req, FILE_TEXT - DEVICE_TEXT);
	}
/*
	Set current settings
*/
	UpdateDeviceList();
	numListItems = NumListItems(devList);
	if (numListItems < 2)
		OffGList(GadgetItem(gadgList, DEV_UPARROW), cmdWindow, req, 2);
	devListItem = FindListItem(devList, prefs.CurrRestoreDev);
	if (devListItem == -1)
		devListItem = 0;
	for (item = 0 ; item < 4 ; item++) {
		localFloppyEnable[item] = (BOOL) floppyEnable[item];
		if (!floppyDrives[item])
			OffGList(GadgetItem(gadgList, DF0_BOX + item), cmdWindow, req, 1);
		else if (floppyEnable[item])
			GadgetOn(cmdWindow, DF0_BOX + item);
	}
	SetEditItemText(gadgList, DEVICE_TEXT, cmdWindow, req, prefs.CurrRestoreDev);
	NumToString(prefs.CurrRestoreUnit, strBuff);
	SetEditItemText(gadgList, UNIT_TEXT, cmdWindow, req, strBuff);
	SetEditItemText(gadgList, FILE_TEXT, cmdWindow, req, destFileName);
	oldDev[0] = 0;
	count = -1;
	restoreMode = prefs.OldPrefs.TestMode + R_RESTORE_RADBTN;
	GadgetOn(cmdWindow, restoreMode);
	if (display = prefs.DisplayRestoreOpts)
		GadgetOn(cmdWindow, DISPLAY_BOX);
	EnableGadgetItem(gadgList, OPTIONS_BUTTON, cmdWindow, req, restoreMode == R_RESTORE_RADBTN);
	item = src = prefs.Source + FLOPPY_RADBTN;
	newSrc = TRUE;
/*
	Handle requester
*/
	done = success = FALSE;
	do {
		if (newSrc) {
			if (!tooLate) {
				SetGadgetItemValue(gadgList, src, cmdWindow, req, FALSE);
				src = item;
				SetGadgetItemValue(gadgList, src, cmdWindow, req, TRUE);
			}
			SetStdPointer(cmdWindow, POINTER_WAIT);
			DifferentDev(gadgList, oldDev, &oldUnit);
			UpdateSCSIDrive(gadgList, item);
			SetArrowPointer();
			newSrc = FALSE;
		}
		WaitPort(mainMsgPort);
		item = CheckRequest(mainMsgPort, cmdWindow, OptionsRequestFilter);
		switch (item) {
		case -1:
			if (tooLate || DifferentDev(gadgList, oldDev, &oldUnit))
				count = 5;
			else if (count >= 0) {
				if (count == 0)
					UpdateSCSIDrive(gadgList, src);
				count--;
			}
			break;
		case OK_BUTTON:
			if (src == FLOPPY_RADBTN &&
				NumDrivesSelected(&localFloppyEnable[0]) == 0)
				Error(ERR_NO_DRIVES);
			else
				success = done = TRUE;
			break;
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case OPTIONS_BUTTON:
			DoRestoreSettings();
			break;
		case DISPLAY_BOX:
			ToggleCheckbox(&display, item, cmdWindow);
			break;
		case R_RESTORE_RADBTN:
		case R_COMPARE_RADBTN:
		case R_TEST_RADBTN:
			SetGadgetItemValue(gadgList, restoreMode, cmdWindow, req, FALSE);
			restoreMode = item;
			SetGadgetItemValue(gadgList, restoreMode, cmdWindow, req, TRUE);
			EnableGadgetItem(gadgList, OPTIONS_BUTTON, cmdWindow, req, restoreMode == R_RESTORE_RADBTN);
			break;
		case FLOPPY_RADBTN:
		case TAPE_RADBTN:
		case FILE_RADBTN:
			newSrc = TRUE;
			break;
		case DF0_BOX:
		case DF1_BOX:
		case DF2_BOX:
		case DF3_BOX:
			if (!tooLate) {
				ToggleCheckbox(&localFloppyEnable[item - DF0_BOX], item, cmdWindow);
				if (src != FLOPPY_RADBTN) {
					item = FLOPPY_RADBTN;
					newSrc = TRUE;
				}
			}
			break;
		case UNIT_UPARROW:
		case UNIT_DNARROW:
			GetEditItemText(gadgList, UNIT_TEXT, strBuff);
			temp = StringToNum(strBuff);
			if (item == UNIT_DNARROW) {
				if (temp)
					temp--;
				else
					temp = 999;
			}
			else {
				if (temp < 999)
					temp++;
				else
					temp = 0;
			}
			NumToString(temp, strBuff);
			SetEditItemText(gadgList, UNIT_TEXT, cmdWindow, req, strBuff);
			item = TAPE_RADBTN;
			newSrc = TRUE;
			break;
		case DEV_UPARROW:
		case DEV_DNARROW:
			if (item == DEV_UPARROW) {
				devListItem++;
				if (devListItem == numListItems)
					devListItem = 0;
			}
			else {
				if (devListItem == 0)
					devListItem = numListItems;
				devListItem--;
			}
			SetEditItemText(gadgList, DEVICE_TEXT, cmdWindow, req, GetListItem(devList, devListItem));
			item = TAPE_RADBTN;
			newSrc = TRUE;
			break;
		case CHOOSE_BTN:
			if (!tooLate) {
				if (DoStdGetFile(&sfReply, strOpenADOS)) {
					BuildPath(sfReply.Name, strBuff, process->pr_CurrentDir, PATHSIZE - 2);
					SetEditItemText(gadgList, FILE_TEXT, cmdWindow, req, strBuff);
					item = FILE_RADBTN;
					newSrc = TRUE;
				}
			}
			break;
		}	
	} while (!done);
	if (success) {
		prefs.FloppyEnableMask &= 0xF0;
		for (item = 0 ; item < 4 ; item++) {
			if (floppyEnable[item] = (UBYTE) localFloppyEnable[item]) {
				prefs.FloppyEnableMask |= (1 << item);
			}
		}
		prefs.Source = src - FLOPPY_RADBTN;
		prefs.OldPrefs.TestMode = restoreMode - R_RESTORE_RADBTN;
		prefs.DisplayRestoreOpts = display;
		GetEditItemText(gadgList, FILE_TEXT, destFileName);
		GetEditItemText(gadgList, DEVICE_TEXT, prefs.CurrRestoreDev);
		GetEditItemText(gadgList, UNIT_TEXT, strBuff);
		if ((startMode == mode) || (mode != MODE_RESTORE)) {
			verifyMode = prefs.OldPrefs.TestMode;
		}
		prefs.CurrRestoreUnit = StringToNum(strBuff);
	}
	DestroyRequest(req);
	EndWait();
	if (oldVerifyMode != verifyMode) {
		temp = operation;
		operation = -1;
		SetModeOp(mode, temp);
		skipUpdate = FALSE;
		EnableScrollList();
	}
	return (success);
}

/*
 * Invoke requester to do restore settings
 */
 
static BOOL DoRestoreSettings()
{
	register WORD item;
	register RequestPtr req;
	register DialogPtr window = cmdWindow;
	register GadgetPtr gadgList;
	register BOOL success = FALSE;
	BOOL done = FALSE;
	BOOL empty, maintain, archive;
	UBYTE dates;
	WORD earlier, later;
	
	reqList[REQ_RESTOPTS2]->Gadgets[R_EARLIER_POPUP].Value = (earlier = prefs.OldPrefs.ReplaceEarlier);
	reqList[REQ_RESTOPTS2]->Gadgets[R_LATER_POPUP].Value = (later = prefs.OldPrefs.ReplaceLater);
	
	req = DoGetRequest(REQ_RESTOPTS2);
	if (req == NULL)
		Error(ERR_NO_MEM);
	else {
		requester = req;
		OutlineOKButton(window);
		gadgList = req->ReqGadget;
		
		if (empty = prefs.OldPrefs.REmptySubdirs)
			GadgetOn(window, R_EMPTY_BOX);
		if (archive = prefs.OldPrefs.RSetArchBit)
			GadgetOn(window, R_ARCHIVE_BOX);
		if (maintain = prefs.OldPrefs.RSubdirs)
			GadgetOn(window, R_MAINTAIN_BOX);
		
		dates = prefs.RestoreDates + R_DATES_ORIG_RADBTN;
		GadgetOn(window, dates);
		do {
			item = ModalRequest(mainMsgPort, window, DialogFilter);
			switch (item) {
				case OK_BUTTON:
					success = TRUE;
				case CANCEL_BUTTON:
					done = TRUE;
					break;
				case R_EARLIER_POPUP:
					earlier = GetGadgetValue(GadgetItem(gadgList, item));
					break;
				case R_LATER_POPUP:
					later = GetGadgetValue(GadgetItem(gadgList, item));
					break;
				case R_DATES_ORIG_RADBTN:
				case R_DATES_BACK_RADBTN:
				case R_DATES_SYS_RADBTN:
					GadgetOff(window, dates);
					dates = item;
					GadgetOn(window, item);
					break;
				case R_EMPTY_BOX:
					ToggleCheckbox(&empty, item, window);
					break;
				case R_ARCHIVE_BOX:
					ToggleCheckbox(&archive, item, window);
					break;
				case R_MAINTAIN_BOX:
					ToggleCheckbox(&maintain, item, window);
					break;
				default:
					break;
			}
		} while (!done);
		if (success) {
			prefs.OldPrefs.REmptySubdirs = empty;
			prefs.OldPrefs.RSetArchBit = archive;
			prefs.OldPrefs.ReplaceEarlier = earlier;
			prefs.OldPrefs.ReplaceLater = later;
			prefs.OldPrefs.RSubdirs = maintain;
			prefs.RestoreDates = dates - R_DATES_ORIG_RADBTN;
		}
		DestroyRequest(req);
	}
	return (success);
}

/*
 *	Adjust catalog options
 */
 
BOOL DoCatalogOptions()
{
	register WORD item;
	register GadgetPtr gadgList;
	register DialogPtr window = cmdWindow;
	register RequestPtr req;
	BOOL done = FALSE;
	BOOL success = FALSE;
	CatParams saveCatOpts = prefs.CatOpts;
	struct DateStamp dateStamp;
	
	BeginWait();
	req = DoGetRequest(REQ_CATOPTS);
	if (req == NULL)
		Error(ERR_NO_MEM);
	else {
		requester = req;
		gadgList = req->ReqGadget;
		OutlineOKButton(window);
/*
	Set current settings
*/
		DateStamp(&dateStamp);
		for (item = DATE1_RADBTN ; item <= DATE4_RADBTN ; item++) {
			XDateString(&dateStamp, item - DATE1_RADBTN, strBuff);
			SetGadgetItemText(gadgList, item, window, req, strBuff);
		}
		
		if (saveCatOpts.IncludeSize)
			GadgetOn(window, FILESIZE_BOX);
		if (saveCatOpts.IncludeProt)
			GadgetOn(window, PROTECT_BOX);
		if (saveCatOpts.IncludeDate)
			GadgetOn(window, DATEMOD_BOX);
		if (saveCatOpts.IncludeTime)
			GadgetOn(window, TIMEMOD_BOX);
		if (saveCatOpts.ShowSecs)
			GadgetOn(window, SHOWSECS_BOX);
#if (AMERICAN | BRITISH | FRENCH | SWEDISH)
		if (saveCatOpts.ShowAMPM)
			GadgetOn(window, SHOWAMPM_BOX);
#endif
		if (saveCatOpts.SeparateDirs)
			GadgetOn(window, SEPARATE_DIRS_BOX);
		
		GadgetOn(window, saveCatOpts.DateFormat + DATE1_RADBTN);
		GadgetOn(window, saveCatOpts.SortFormat + SORT_NAME_RADBTN);
/*
		if (saveCatOpts.SortFormat == SORT_NORMAL_RADBTN - SORT_NAME_RADBTN) {
			EnableGadgetItem(gadgList, SEPARATE_DIRS_BOX, window, req, FALSE);
		}
*/

/*
	Handle requester
*/
		do {
			item = ModalRequest(mainMsgPort, window, DialogFilter);
			switch (item) {
			case OK_BUTTON:
				success = TRUE;
			case CANCEL_BUTTON:
				done = TRUE;
				break;
			case FILESIZE_BOX:
				ToggleCheckbox(&saveCatOpts.IncludeSize, item, window);
				break;
			case PROTECT_BOX:
				ToggleCheckbox(&saveCatOpts.IncludeProt, item, window);
				break;
			case DATEMOD_BOX:
				ToggleCheckbox(&saveCatOpts.IncludeDate, item, window);
				break;
			case TIMEMOD_BOX:
				ToggleCheckbox(&saveCatOpts.IncludeTime, item, window);
				break;
			case SHOWSECS_BOX:
				ToggleCheckbox(&saveCatOpts.ShowSecs, item, window);
				break;
#if (AMERICAN | BRITISH | FRENCH | SWEDISH)
			case SHOWAMPM_BOX:
				ToggleCheckbox(&saveCatOpts.ShowAMPM, item, window);
				break;
#endif
			case DATE1_RADBTN:
			case DATE2_RADBTN:
			case DATE3_RADBTN:
			case DATE4_RADBTN:
				GadgetOff(window, saveCatOpts.DateFormat + DATE1_RADBTN);
				GadgetOn(window, item);
				saveCatOpts.DateFormat = item - DATE1_RADBTN;
				break;
			case SORT_NAME_RADBTN:
			case SORT_SIZE_RADBTN:
			case SORT_DATE_RADBTN:
				GadgetOff(window, saveCatOpts.SortFormat + SORT_NAME_RADBTN);
				GadgetOn(window, item);
				saveCatOpts.SortFormat = item - SORT_NAME_RADBTN;
				break;
			case SEPARATE_DIRS_BOX:
				ToggleCheckbox(&saveCatOpts.SeparateDirs, item, window);
				break;
			default:
				break;
			}	
		} while (!done);
		DestroyRequest(req);
	}
	if(success) {
		prefs.CatOpts = saveCatOpts;
		if (operation == OPER_SELECTION) {
			SortAllDirs();
			BuildFileList(NULL);
			skipUpdate = TRUE;
		}
	}
	EndWait();
	return (success);
}

/*
 *	Request filter routine for the session log requester
 */
 
static BOOL LogfileRequestFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD	itemHit;
	register ULONG	class = intuiMsg->Class;

	if (intuiMsg->IDCMPWindow == cmdWindow && class == GADGETDOWN) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == LOG_NAME_TEXT) {
			ReplyMsg(intuiMsg);
			*item = LOG_USERNAME_RADBTN;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Adjust logfile options
 */
 
BOOL DoLogfileOptions()
{
	register WORD item;
	register GadgetPtr gadgList;
	register DialogPtr window = cmdWindow;
	register RequestPtr req;
	BOOL done, success;
	SFReply sfReply;
	LogParams logOpts = prefs.LogOpts;
	BOOL redo;

	BeginWait();
	AutoActivateEnable(FALSE);
	req = DoGetRequest(REQ_LOGOPTS);
	AutoActivateEnable(TRUE);
	if (req == NULL) {
		Error(ERR_NO_MEM);
		EndWait();
		return (FALSE);
	}
	requester = req;
	gadgList = req->ReqGadget;
	OutlineOKButton(window);
/*
	Set current settings
*/
	GadgetOn(window, logOpts.LogNameMode + LOG_NONE_RADBTN);	
	if (logOpts.LogRate)
		GadgetOn(window, LOG_RATE_BOX);
	if (logOpts.LogCompEff)
		GadgetOn(window, LOG_COMP_EFF_BOX);
	if (logOpts.LogDisplayOff)
		GadgetOn(window, LOG_DISPLAY_ON_BOX);
	GadgetOn(window, logOpts.LogLevel + LOG_LEVEL1_RADBTN);
	SetEditItemText(gadgList, LOG_NAME_TEXT, window, req, prefs.LogOpts.LogFileName);
/*
	Handle requester
*/
	success = done = FALSE;
	do {
		item = ModalRequest(mainMsgPort, window, LogfileRequestFilter);
		switch (item) {
		case OK_BUTTON:
			GetEditItemText(gadgList, LOG_NAME_TEXT, strBuff);
			if (TestStrLen(strBuff)) {
				strcpy(logOpts.LogFileName, strBuff);
				success = done = TRUE;
			}
			break;
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case LOG_LEVEL1_RADBTN:
		case LOG_LEVEL2_RADBTN:
		case LOG_LEVEL3_RADBTN:
			GadgetOff(window, logOpts.LogLevel + LOG_LEVEL1_RADBTN);
			GadgetOn(window, item);
			logOpts.LogLevel = item - LOG_LEVEL1_RADBTN;
			break;
		case LOG_RATE_BOX:
			ToggleCheckbox(&logOpts.LogRate, item, window);
			break;
		case LOG_COMP_EFF_BOX:
			ToggleCheckbox(&logOpts.LogCompEff, item, window);
			break;
		case LOG_CHOOSE_BTN:
			GetEditItemText(gadgList, LOG_NAME_TEXT, strBuff);
			if (!DoStdPutFile(&sfReply, strSaveLog, strBuff))
				break;
			SetEditItemText(gadgList, LOG_NAME_TEXT, window, req, strBuff);
			item = LOG_USERNAME_RADBTN;		// Fall through
		case LOG_NONE_RADBTN:
		case LOG_DEFAULTNAME_RADBTN:
		case LOG_USERNAME_RADBTN:
			GadgetOff(window, logOpts.LogNameMode + LOG_NONE_RADBTN);
			GadgetOn(window, item);
			logOpts.LogNameMode = item - LOG_NONE_RADBTN;
			if (item == LOG_USERNAME_RADBTN)
				ActivateGadget(GadgetItem(gadgList, LOG_NAME_TEXT), window, req);
			break;
		case LOG_DISPLAY_ON_BOX:
			ToggleCheckbox(&logOpts.LogDisplayOff, item, window);
			break;
		default:
			break;
		}	
	} while (!done);
	DestroyRequest(req);
	EndWait();
	if (success) {
		redo = FALSE;
		item = SLNumItems(scrollList) - 1;
		if (operation == OPER_COMPLETE) {
			if (redo = (prefs.LogOpts.LogCompEff != logOpts.LogCompEff) ||
					(prefs.LogOpts.LogRate != logOpts.LogRate)) {
				
				DisableScrollList();
				if (prefs.LogOpts.LogCompEff) {
					RemoveRuntimeItem(item--);
				}
				if (prefs.LogOpts.LogRate) {
					RemoveRuntimeItem(item--);
				}
/*
	Get rid of the new "# of errors" string
*/
				RemoveRuntimeItem(item--);
				RemoveRuntimeItem(item);
			}
		}
		prefs.LogOpts = logOpts;
		if (redo) {
			FinalRuntimeStrings();
			EnableScrollList();
		}
		if ((operation == OPER_IN_PROG) || (operation == OPER_PAUSE)) {
			disableDisplay = prefs.LogOpts.LogDisplayOff && (operation != OPER_COMPLETE);
			RefreshScrollList();
		}
	}
	return (success);
}

/*
 * Adjust user options
 */
 
BOOL DoPreferences()
{
	register RequestPtr req;
	register DialogPtr window = cmdWindow;
	register WORD item;
	BOOL icons, sound1, flash1, sound2, flash2, warn, includeDirs;
	BOOL done = FALSE;
	BOOL success = FALSE;

	icons = prefs.SaveIcons;
	flash1 = prefs.OldPrefs.BeepFlash & 1;
	flash2 = prefs.OldPrefs.BeepFlash & 0x80;
	sound1 = prefs.OldPrefs.BeepSound & 1;
	sound2 = prefs.OldPrefs.BeepSound & 0x80;
	warn = prefs.WarnInvalidFilenames;
	includeDirs = prefs.IncludeAssignDirs;

	BeginWait();
	if ((req = DoGetRequest(REQ_PREFS)) == NULL)
		Error(ERR_NO_MEM);
	else {
		requester = req;
		OutlineOKButton(window);
/*
	Set current settings
*/
		if (sound1)
			GadgetOn(window, SOUND1_BOX);
		if (flash1)
			GadgetOn(window, FLASH1_BOX);
		if (sound2)
			GadgetOn(window, SOUND2_BOX);
		if (flash2)
			GadgetOn(window, FLASH2_BOX);
		if (icons)
			GadgetOn(window, SAVEICONS_BOX);
		if (warn)
			GadgetOn(window, WARN_BOX);
		if (includeDirs)
			GadgetOn(window, INCLUDE_DIRS_BOX);
/*
	Handle requester
*/
		do {
			item = ModalRequest(mainMsgPort, window, DialogFilter);
			switch (item) {
			case OK_BUTTON:
				success = TRUE;
			case CANCEL_BUTTON:
				done = TRUE;
				break;
			case FLASH1_BOX:
				ToggleCheckbox(&flash1, item, window);
				break;
			case SOUND1_BOX:
				ToggleCheckbox(&sound1, item, window);
				break;
			case FLASH2_BOX:
				ToggleCheckbox(&flash2, item, window);
				break;
			case SOUND2_BOX:
				ToggleCheckbox(&sound2, item, window);
				break;
			case SAVEICONS_BOX:
				ToggleCheckbox(&icons, item, window);
				break;
			case WARN_BOX:
				ToggleCheckbox(&warn, item, window);
				break;
			case INCLUDE_DIRS_BOX:
				ToggleCheckbox(&includeDirs, item, window);
				break;
			default:
				break;
			}	
		} while (!done);
		DestroyRequest(req);
	}
	EndWait();
	if(success) {
		prefs.OldPrefs.BeepFlash = (flash2 ? 0x80 : 0) + flash1;
		prefs.OldPrefs.BeepSound = (sound2 ? 0x80 : 0) + sound1;
		prefs.SaveIcons = icons;
		prefs.WarnInvalidFilenames = warn;
		if (prefs.IncludeAssignDirs != includeDirs) {
			prefs.IncludeAssignDirs = includeDirs;
			checkVolCounter = 0;
		}
	}
	return (success);
}

/*
 *	Adjust tape options
 */
 
BOOL DoTapeOptions()
{
	register WORD item;
	register DialogPtr window = cmdWindow;
	register RequestPtr req;
	BOOL done = FALSE;
	BOOL success = FALSE;
	TapeParams tapeOpts = prefs.TapeOpts;

	BeginWait();
	req = DoGetRequest(REQ_TAPEOPTS);
	if (req == NULL)
		Error(ERR_NO_MEM);
	else {
		requester = req;
		if (tapeOpts.AutoRetension)
			GadgetOn(window, AUTO_RET_BOX);
		if (tapeOpts.AutoRewind)
			GadgetOn(window, REWIND_BOX);
		OutlineOKButton(window);
/*
	Handle requester
*/
		do {
			item = ModalRequest(mainMsgPort, window, DialogFilter);
			switch (item) {
			case OK_BUTTON:
				success = TRUE;
			case CANCEL_BUTTON:
				done = TRUE;
				break;
			case AUTO_RET_BOX:
				ToggleCheckbox(&tapeOpts.AutoRetension, item, window);
				break;
			case REWIND_BOX:
				ToggleCheckbox(&tapeOpts.AutoRewind, item, window);
				break;
			default:
				break;
			}	
		} while (!done);
		DestroyRequest(req);
	}
	EndWait();
	if (success) {
		prefs.TapeOpts = tapeOpts;
	}
	return (success);
}
/*
 *	Adjust buffer options
 */
 
BOOL DoBufferOptions()
{
	register WORD item;
	register GadgetPtr gadgList;
	register DialogPtr window = cmdWindow;
	register RequestPtr req;
	BOOL done = FALSE;
	BOOL success = FALSE;
	BuffParams buffOpts = prefs.BuffOpts;
	BOOL async;
	register WORD memItem;
	
	if (((operation == OPER_LIMBO) || (operation == OPER_COMPLETE) || ((mode == MODE_BACKUP) && (operation == OPER_SELECTION))) && ((sessionNumber == 0) || sequentialDevice || !tapeDevice)) {
		BeginWait();
		AutoActivateEnable(FALSE);
		req = DoGetRequest(REQ_BUFFOPTS);
		AutoActivateEnable(TRUE);
		if (req == NULL)
			Error(ERR_NO_MEM);
		else {
			requester = req;
			gadgList = req->ReqGadget;
			
			UpdateBufferArrows(&buffOpts);
			OutlineOKButton(window);
/*
	Set current settings
*/
			NumToString(buffOpts.BackupBufferSize, strBuff);
			SetEditItemText(gadgList, BACKBUFFSIZE_TEXT, window, req, strBuff);
			NumToString(buffOpts.RestoreBufferSize, strBuff);
			SetEditItemText(gadgList, RESTBUFFSIZE_TEXT, window, req, strBuff);
			memItem = FAST_RADBTN + GetIDFromBufferMemType(buffOpts.MemType);
			if ((async = !prefs.OldPrefs.SlowBackup)) {
				GadgetOn(window, ASYNC_BOX);
			}
			GadgetOn(window, memItem);
/*
	Handle requester
*/
			do {
				WaitPort(mainMsgPort);
				item = CheckRequest(mainMsgPort, window, DialogFilter);
				switch (item) {
				case -1:
					UpdateBufferArrows(&buffOpts);
					break;
				case OK_BUTTON:
					success = TRUE;
				case CANCEL_BUTTON:
					done = TRUE;
					break;
				case BACKBUFFSIZE_UPARROW:
				case BACKBUFFSIZE_DNARROW:
					GetEditItemText(gadgList, BACKBUFFSIZE_TEXT, strBuff);
					buffOpts.BackupBufferSize = StringToNum(strBuff);
					if (item == BACKBUFFSIZE_UPARROW) {
						if (buffOpts.BackupBufferSize <= 0)	// Zero is invalid
							buffOpts.BackupBufferSize = 1;	// Now we have a valid one
						else
							buffOpts.BackupBufferSize <<= 1;
					}
					else {
						if (buffOpts.BackupBufferSize < 2)	// Test for invalid string first
							buffOpts.BackupBufferSize = prefs.BuffOpts.BackupBufferSize;
						else
							buffOpts.BackupBufferSize >>= 1;
					}
					UpdateBufferArrows(&buffOpts);
					NumToString(buffOpts.BackupBufferSize, strBuff);
					SetEditItemText(gadgList, BACKBUFFSIZE_TEXT, window, req, strBuff);
					break;
				case RESTBUFFSIZE_UPARROW:
				case RESTBUFFSIZE_DNARROW:
					GetEditItemText(gadgList, RESTBUFFSIZE_TEXT, strBuff);
					buffOpts.RestoreBufferSize = StringToNum(strBuff);
					if (item == RESTBUFFSIZE_UPARROW) {
						if (buffOpts.RestoreBufferSize <= 0)// Zero is invalid
							buffOpts.RestoreBufferSize = 1;	// Now we have a valid one! *
						else
							buffOpts.RestoreBufferSize <<= 1;
					}
					else {
						if (buffOpts.RestoreBufferSize < 2)	// Test for invalid string first
							buffOpts.RestoreBufferSize = prefs.BuffOpts.RestoreBufferSize;
						else
							buffOpts.RestoreBufferSize >>= 1;
					}
					UpdateBufferArrows(&buffOpts);
					NumToString(buffOpts.RestoreBufferSize, strBuff);
					SetEditItemText(gadgList, RESTBUFFSIZE_TEXT, window, req, strBuff);
					break;
				case FAST_RADBTN:
				case ANY24BIT_RADBTN:
				case CHIP_RADBTN:
					GadgetOff(window, memItem);
					GadgetOn(window, item);
					memItem = item;
					break;
				case ASYNC_BOX:
					ToggleCheckbox(&async, item, window);
					break;
				default:
					break;
				}	
			} while (!done);
			GetEditItemText(gadgList, BACKBUFFSIZE_TEXT, strBuff);
			buffOpts.BackupBufferSize = StringToNum(strBuff);
			GetEditItemText(gadgList, RESTBUFFSIZE_TEXT, strBuff);
			buffOpts.RestoreBufferSize = StringToNum(strBuff);
			buffOpts.MemType = GetBufferMemTypeFromID(memItem-FAST_RADBTN);
			DestroyRequest(req);
		}
		EndWait();
		if (success) {
			if (buffOpts.BackupBufferSize == 0) {
				buffOpts.BackupBufferSize = prefs.BuffOpts.BackupBufferSize;
			}
			if (buffOpts.RestoreBufferSize == 0) {
				buffOpts.RestoreBufferSize = prefs.BuffOpts.RestoreBufferSize;
			}
			prefs.BuffOpts = buffOpts;
			prefs.OldPrefs.SlowBackup = !async;
			numBufs = async ? 2 : 1;
		}
	}
	return (success);
}

/*
 * Update the up/down arrows of the buffer gadget set.
 */
 
static void UpdateBufferArrows(register BuffParamsPtr buffOpts)
{
	register RequestPtr req = requester;
	register GadgetPtr gadgList = req->ReqGadget;

	EnableGadgetItem(gadgList, BACKBUFFSIZE_UPARROW, cmdWindow, req, buffOpts->BackupBufferSize < 8192);
	EnableGadgetItem(gadgList, BACKBUFFSIZE_DNARROW, cmdWindow, req, buffOpts->BackupBufferSize > 1);
	EnableGadgetItem(gadgList, RESTBUFFSIZE_UPARROW, cmdWindow, req, buffOpts->RestoreBufferSize < 8192);
	EnableGadgetItem(gadgList, RESTBUFFSIZE_DNARROW, cmdWindow, req, buffOpts->RestoreBufferSize > 1);
}

/*
 * Update the up/down arrows of the compression bit level gadget set.
 */
 
static void UpdateCompBitArrows(WORD num)
{
	RequestPtr req = requester;
	GadgetPtr gadgList = req->ReqGadget;
	
	EnableGadgetItem(gadgList, COMPBIT_UPARROW, cmdWindow, req, num < MAX_COMPBIT);
	EnableGadgetItem(gadgList, COMPBIT_DNARROW, cmdWindow, req, num > MIN_COMPBIT);
	NumToString(num, strBuff);
	strcat(strBuff, strBitLevel);
	SetGadgetItemText(gadgList, COMPBIT_TEXT, cmdWindow, req, strBuff);
}

/*
 * Bring up a simple alert warning the user that reset will occur.
 */
 
BOOL DoResetOptions()
{
	BOOL success = !WarningDialog(ERR_RESET_OPTS, TRUE);
	TextChar buff[MAX_LOG_STRLEN];
	BOOL runtime;
	UBYTE saveSrc, saveDest;
		
	if (success) {
/*
 	Leave untouched the FileID, version, and the closing information (the current
	backup/restore device and unit numbers.
*/
		if (runtime = (operation == OPER_PAUSE) || (operation == OPER_IN_PROG)) {
			saveDest = prefs.Destination;	
			saveSrc = prefs.Source;
		}
		BlockClear(&prefs.OldPrefs, ((ULONG)prefs.CurrBackupDev) - ((ULONG)&prefs.OldPrefs));
		strcpy(buff, prefs.LogOpts.LogFileName);
		SetStdDefaults();
		if (runtime) {
			if (mode == MODE_BACKUP)
				prefs.Destination = saveDest;
			else {
				prefs.OldPrefs.TestMode = verifyMode;
				prefs.Source = saveSrc;
			}
		}
		strcpy(prefs.LogOpts.LogFileName, buff);
	}
	return (success);
}

/*
 *	Process Edit menu selection
 */

BOOL DoOptionsMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifiers)
{
	BOOL success = FALSE;
	
	switch (item) {
	case BACKUP_OPTS_ITEM:
		success = DoBackupOptions();
		break;
	case RESTORE_OPTS_ITEM:
		success = DoRestoreOptions();
		break;
	case CATALOG_OPTS_ITEM:
		success = DoCatalogOptions();
		break;
	case LOGFILE_OPTS_ITEM:
		success = DoLogfileOptions();
		break;
	case TAPE_OPTS_ITEM:
		success = DoTapeOptions();
		break;
	case BUFF_OPTS_ITEM:
		success = DoBufferOptions();
		break;
	case PREFERENCES_ITEM:
		success = DoPreferences();
		break;
	case RESET_ITEM:
		success = DoResetOptions();
	default:
		break;
	}
	return (success);
}

/*
 * Setup Backup destination
 * Used for AREXX macros
 */
 
BOOL SetBackupDestination(TextPtr optName)
{
	return (SetSourceOrDestination(optName, FALSE));
}

/*
 * Setup Restore source
 * Used for AREXX macros
 */
 
BOOL SetRestoreSource(TextPtr optName)
{
	return (SetSourceOrDestination(optName, TRUE));
}

/*
 * Sets backup destination or restore source
 * Used for AREXX macros
 */
 
static BOOL SetSourceOrDestination(register TextPtr optName, BOOL restoreMode)
{
	register WORD i;
	UBYTE ioType;
	TextPtr dev;
	WORD *unit;
	BOOL success;
	TextChar buff[256];
	WORD len;

	GetNextWord(optName, buff, &len);
	for (i = 0; i < NUM_BACKUP_DEST_OPTIONS; i++) {
		if (CmpString(buff, backupDestNames[i], strlen(buff), (WORD) strlen(backupDestNames[i]), FALSE) == 0)
			break;
	}
	if (i >= NUM_BACKUP_DEST_OPTIONS || operation == OPER_IN_PROG || operation == OPER_PREP) {
		return (FALSE);
	}
	optName += len;
	success = TRUE;
	switch (i) {
	case OPT_FLOPPY_ON:
		ioType = DEST_FLOPPY;
		for (;;) {
			GetNextWord(optName, buff, &len);
			if (buff[0] == '\0')
				break;
			success = FALSE;
			for (i = 0 ; i < 8 ; i++) {
				if (CmpString(buff, floppyEnableNames[i], strlen(buff), strlen(floppyEnableNames[i]), FALSE) == 0) {
					floppyEnable[i >> 1] = i & 1;
					success = TRUE;
					break;
				}
			}
			optName += len;
		}
		break;
	case OPT_TAPE_ON:
		if (success = !noSCSI) {
			ioType = DEST_TAPE;
			if (restoreMode) {
				unit = &prefs.CurrRestoreUnit;
				dev = prefs.CurrRestoreDev;
			}
			else {
				unit = &prefs.CurrBackupUnit;
				dev = prefs.CurrBackupDev;
			}
			GetNextWord(optName, buff, &len);
			optName += len;
			if (buff[0]) {
				if (IsStrNumeric(buff)) {
/*
	It's completely numeric, so this is the unit parameter. Anything that follows
	is the device driver name.
*/
					*unit = StringToNum(buff);
					GetNextWord(optName, buff, &len);
					if (buff[0]) {
						if (strlen(buff) > MAX_FILENAME_LEN)
							success = FALSE;
						else
							strcpy(dev, buff);
					}
				}
				else {
/*
	Not completely numeric, so assume device driver name.
*/
					if (strlen(buff) > MAX_FILENAME_LEN)
						success = FALSE;
					else
						strcpy(dev, buff);
					GetNextWord(optName, buff, &len);
					i = StringToNum(buff);
					if ((buff[0] >= '0') && (buff[0] <= '9'))
						*unit = i;
				}
			}
		}
		break;
	case OPT_FILE_ON:
		ioType = DEST_FILE;
		GetNextWord(optName, buff, &len);
		if (buff[0]) {
			strcpy(destFileName, buff);
		}
		break;
	}
	if (restoreMode)
		prefs.Source = ioType;
	else
		prefs.Destination = ioType;
	return (success);
}

/*
 * Quick and dirty function to test whether all chars are numeric chars in a string.
 */
 
static BOOL IsStrNumeric(register TextPtr buf)
{
	register TextChar ch;
	
	while ((ch = *buf++) != '\0') {
		if ((ch < '0') || (ch > '9')) {
			return (FALSE);
		}
	}
	return (TRUE);
}
	
/*
 * Sets backup options
 * Used for AREXX macros
 */
 
LONG SetBackupOptions(register TextPtr optName)
{
	register WORD i;
	LONG result;
	TextChar buff[256];
	UWORD len;

	if (operation == OPER_IN_PROG || operation == OPER_PAUSE || operation == OPER_PREP)
		return (RC_ERROR);
	result = RC_OK;
	do {
		GetNextWord(optName, buff, &len);
		for (i = 0; i < NUM_BACKUP_OPTIONS; i++) {
			if (CmpString(buff, backupOptNames[i], strlen(buff), (WORD) strlen(backupOptNames[i]), FALSE) == 0)
				break;
		}
		optName += len;
		if (i >= NUM_BACKUP_OPTIONS)
			result = RC_WARN;
		else {
			switch (i) {
			case OPT_COMP_OFF:
				prefs.Compress &= COMP_BIT_MASK;	// Turns off both flags
				break;
			case OPT_COMP_ON:
				prefs.Compress = (prefs.Compress & COMP_BIT_MASK) | COMP_ON_FLAG;
				break;
			case OPT_COMP_DEVICE:
				prefs.Compress = (prefs.Compress & COMP_BIT_MASK) | COMP_DEV_FLAG;
				break;
			case OPT_COMP_LEVEL:
				GetNextWord(optName, buff, &len);
				i = StringToNum(buff);
				if (i >= MIN_COMPBIT && i <= MAX_COMPBIT)
					prefs.Compress = i | (prefs.Compress & ~COMP_BIT_MASK);
				else
					result = RC_WARN;
				optName += len;
				break;
			case OPT_PASSWORD_ON:
			case OPT_PASSWORD_OFF:
				prefs.Password = i == OPT_PASSWORD_ON;
				break;
			case OPT_ARCHIVE_ON:
			case OPT_ARCHIVE_OFF:
				prefs.OldPrefs.BSetArchBit = i == OPT_ARCHIVE_ON;
				break;
			case OPT_VERIFY_ON:
			case OPT_VERIFY_OFF:
				prefs.OldPrefs.Verify = i == OPT_VERIFY_ON;
				break;
			case OPT_WARN_ON:
			case OPT_WARN_OFF:
				prefs.OldPrefs.Reformat = i != OPT_WARN_ON;
				break;
			case OPT_DISPLAY_OPTS_ON:
			case OPT_DISPLAY_OPTS_OFF:
				prefs.DisplayBackupOpts = i == OPT_DISPLAY_OPTS_ON;
				break;
			case OPT_ENTIRE_ON:
			case OPT_ENTIRE_OFF:
				prefs.OldPrefs.BAllDirs = i == OPT_ENTIRE_ON;
				break;
			case OPT_COMPLETE:
			case OPT_SELECTIVE:
				prefs.OldPrefs.BackupType = i - OPT_COMPLETE;
			default:
				break;
			}
		}
	} while (*optName);
	return (result);
}

/*
 * Sets restore options 
 * Used for AREXX macros
 */
 
LONG SetRestoreOptions(register TextPtr optName)
{
	LONG result;
	register WORD i;
	BOOL doBreak;
	UWORD len;
	TextChar buff[256];
	
	if (operation == OPER_IN_PROG || operation == OPER_PREP || operation == OPER_PAUSE)
		return (RC_ERROR);
	result = RC_OK;
	do {
		GetNextWord(optName, buff, &len);
		for (i = 0; i < NUM_RESTORE_OPTIONS; i++) {
			if (CmpString(buff, restoreOptNames[i], strlen(buff), (WORD) strlen(restoreOptNames[i]), FALSE) == 0)
				break;
		}
		optName += len;
		if (i >= NUM_RESTORE_OPTIONS)
			result = RC_WARN;
		else {
			doBreak = TRUE;
			switch (i) {
			case OPT_DISPLAY_OPTS_ON:
			case OPT_DISPLAY_OPTS_OFF:
				prefs.DisplayRestoreOpts = i == OPT_DISPLAY_OPTS_ON;
				break;
			case OPT_ARCHIVE_ON:
			case OPT_ARCHIVE_OFF:
				prefs.OldPrefs.RSetArchBit = i == OPT_ARCHIVE_ON;
				break;
			case OPT_RESTORE:
			case OPT_COMPARE:
			case OPT_TEST:
				prefs.OldPrefs.TestMode = i - OPT_RESTORE;
				break;
			case OPT_REPLACE_ON:
			case OPT_REPLACE_OFF:
			case OPT_REPLACE_PROMPT:
				doBreak = FALSE;
			case OPT_REPLACE_EARLIER_ON:
			case OPT_REPLACE_EARLIER_OFF:
			case OPT_REPLACE_EARLIER_PROMPT:
				prefs.OldPrefs.ReplaceEarlier = i - OPT_REPLACE_ON;
				if (doBreak)
					break;
			case OPT_REPLACE_LATER_ON:
			case OPT_REPLACE_LATER_OFF:
			case OPT_REPLACE_LATER_PROMPT:
				prefs.OldPrefs.ReplaceLater = i - OPT_REPLACE_ON;
				break;
			case OPT_DATE_ORIG:
			case OPT_DATE_CURRENT:
			case OPT_DATE_BACKUP:
				prefs.RestoreDates = i - OPT_DATE_ORIG;
				break;
			case OPT_EMPTY_DIRS_ON:
			case OPT_EMPTY_DIRS_OFF:
				prefs.OldPrefs.REmptySubdirs = i == OPT_EMPTY_DIRS_ON;
				break;
			case OPT_KEEP_DIR_STRUCT_ON:
			case OPT_KEEP_DIR_STRUCT_OFF:
				prefs.OldPrefs.RSubdirs = i == OPT_KEEP_DIR_STRUCT_ON;
				break;
			default:
				break;
			}
		}
	} while (*optName);
	return (result);
}

/*
 * Sets catalog options
 * Used for AREXX macros
 */

LONG SetCatalogOptions(register TextPtr optName)
{
	register WORD i;
	ULONG n;
	TextChar buff[256];
	UWORD len;
	LONG result = RC_OK;
		
	do {
		GetNextWord(optName, buff, &len);
		for (i = 0; i < NUM_CATALOG_OPTIONS; i++) {
			if (CmpString(buff, catalogOptNames[i], strlen(buff), (WORD) strlen(catalogOptNames[i]), FALSE) == 0)
				break;
		}
		optName += len;
		if (i >= NUM_CATALOG_OPTIONS)
			result = RC_WARN;
		else {
			switch (i) {
			case OPT_SIZE_ON:
			case OPT_SIZE_OFF:
				prefs.CatOpts.IncludeSize = (i == OPT_SIZE_ON);
				break;
			case OPT_PROT_ON:
			case OPT_PROT_OFF:
				prefs.CatOpts.IncludeProt = (i == OPT_PROT_ON);
				break;
			case OPT_DATE_ON:
			case OPT_DATE_OFF:
				prefs.CatOpts.IncludeDate = (i == OPT_DATE_ON);
				break;
			case OPT_TIME_ON:
			case OPT_TIME_OFF:
				prefs.CatOpts.IncludeTime = (i == OPT_TIME_ON);
				break;
			case OPT_SECS_ON:
			case OPT_SECS_OFF:
				prefs.CatOpts.ShowSecs = (i == OPT_SECS_ON);
				break;
			case OPT_AMPM_ON:
			case OPT_AMPM_OFF:
#if (AMERICAN | BRITISH | FRENCH | SWEDISH)
				prefs.CatOpts.ShowAMPM = (i == OPT_AMPM_ON);
#elif GERMAN
				prefs.CatOpts.ShowAMPM = FALSE;
#endif
				break;
			case OPT_DATE_FORMAT:
				GetNextWord(optName, buff, &len);
				optName += len;
				n = StringToNum(buff);
				if ((n > 0) && (n < 5))
					prefs.CatOpts.DateFormat = n - 1;
				else
					result = RC_WARN;
				break;
			case OPT_SORT_BY_NAME:
			case OPT_SORT_BY_SIZE:
			case OPT_SORT_BY_DATE:
				prefs.CatOpts.SortFormat = i - OPT_SORT_BY_NAME;
				break;
			case OPT_DRAWERS_FIRST_ON:
			case OPT_DRAWERS_FIRST_OFF:
				prefs.CatOpts.SeparateDirs = i == OPT_DRAWERS_FIRST_ON;
				break;
			}
		}
	} while (*optName);
	return (result);
}

/*
 * Sets session log options
 * Used for AREXX macros
 */

LONG SetSessionLogOptions(register TextPtr optName)
{
	register WORD i;
	TextChar buff[256];
	UWORD len;
	LONG result;

	result = RC_OK;
	do {
		GetNextWord(optName, buff, &len);
		for (i = 0; i < NUM_SESSION_LOG_OPTIONS; i++) {
		if (CmpString(buff, sessionOptNames[i], strlen(buff), (WORD) strlen(sessionOptNames[i]), FALSE) == 0)
			break;
		}
		optName += len;
		if (i >= NUM_SESSION_LOG_OPTIONS)
			result = RC_WARN;
		else {
			switch (i) {
			case OPT_ERRORS_ONLY:
			case OPT_ERRORS_DIRS_ONLY:
			case OPT_FULL_INFO:
				prefs.LogOpts.LogLevel = i - OPT_ERRORS_ONLY;
				break;
			case OPT_NO_DISP_ON:
			case OPT_NO_DISP_OFF:
				prefs.LogOpts.LogDisplayOff = i == OPT_NO_DISP_ON;
				break;
			case OPT_RATE_ON:
			case OPT_RATE_OFF:
				prefs.LogOpts.LogRate = i == OPT_RATE_ON;
				break;
			case OPT_COMP_EFF_ON:
			case OPT_COMP_EFF_OFF:
				prefs.LogOpts.LogCompEff = i == OPT_COMP_EFF_ON;
				break;
			case OPT_SAVE_LOGFILE_AS:
				GetNextWord(optName, buff, &len);
				optName += len;
				stccpy(prefs.LogOpts.LogFileName, buff, MAX_LOG_STRLEN);
				if (strlen(buff) > (MAX_LOG_STRLEN-1)) {
					result = RC_WARN;
				}
			case OPT_NO_LOGFILE:
			case OPT_AUTO_LOGFILE:
				prefs.LogOpts.LogNameMode = i - OPT_NO_LOGFILE;
				break;
			}
		}
	} while (*optName);
	return (result);
}

/*
 * Sets buffer options
 * Used for AREXX macros
 */
 
LONG SetBufferOptions(register TextPtr optName)
{
	register WORD i;
	register ULONG n;
	TextChar buff[256];
	UWORD len;
	LONG result;
	
	if ((operation != OPER_COMPLETE) && (operation != OPER_LIMBO) && (operation != OPER_SELECTION || mode != MODE_BACKUP)) {
		return (RC_ERROR);
	}
	result = RC_OK;
	do {
		GetNextWord(optName, buff, &len);
		for (i = 0; i < NUM_BUFFER_OPTIONS; i++) {
			if (CmpString(buff, buffOptNames[i], strlen(buff), (WORD) strlen(buffOptNames[i]), FALSE) == 0)
				break;
		}
		optName += len;
		if (i >= NUM_BUFFER_OPTIONS)
			result = RC_WARN;
		else {
			switch (i) {
			case OPT_ANYMEM:	
			case OPT_ANY24BITMEM:
			case OPT_GFXMEM:
				prefs.BuffOpts.MemType = GetBufferMemTypeFromID(i - OPT_ANYMEM);
				break;
			case OPT_ASYNC_OFF:
			case OPT_ASYNC_ON:
				prefs.OldPrefs.SlowBackup = i == OPT_ASYNC_OFF;
				break;
			case OPT_BACKUPBUFFSIZE:
			case OPT_RESTOREBUFFSIZE:
				GetNextWord(optName, buff, &len);
				optName += len;
				n = StringToNum(buff);
				if (n > 8192)
					result = RC_WARN;
				else {
					if (i == OPT_BACKUPBUFFSIZE)
						prefs.BuffOpts.BackupBufferSize = n;
					else
						prefs.BuffOpts.RestoreBufferSize = n;
				}
				break;
			}
		}
	} while (*optName);
	return (result);
}

/*
 *	Given an id (0=fast, 1=24bitDMA, 2=chip), return its memory flags.
 */
 
ULONG GetBufferMemTypeFromID(UWORD id)
{
	register ULONG	memType;

	switch (id) {
	case 0:
		memType = MEMF_ANY;
		break;
	case 1:
		memType = (SystemVersion() >= OSVERSION_2_0_4) ? MEMF_24BITDMA : MEMF_CHIP;
		break;
	case 2:
		memType = MEMF_CHIP;
		break;
	}
	return (memType);
}

/*
 *	Given a memory type, determine the id for it.
 */
 		
UWORD GetIDFromBufferMemType(register ULONG memType)
{
	register UWORD	id;
	
	if (memType & MEMF_CHIP)
		id = 2;
	else if (memType & MEMF_24BITDMA)
		id = 1;
	else
		id = 0;
	return (id);
}

/*
 * Sets tape options
 * Used for AREXX macros
 */
 
LONG SetTapeOptions(register TextPtr optName)
{
	register WORD i;
	TextChar buff[256];
	UWORD len;
	LONG result;
	
	if ((operation == OPER_IN_PROG) || (operation == OPER_PAUSE)) {
		return (RC_ERROR);
	}
	result = RC_OK;
	do {
		GetNextWord(optName, buff, &len);
		for (i = 0; i < NUM_TAPE_OPTIONS; i++) {
			if (CmpString(buff, tapeOptNames[i], strlen(buff), (WORD) strlen(tapeOptNames[i]), FALSE) == 0)
				break;
		}
		optName += len;
		if (i >= NUM_TAPE_OPTIONS)
			result = RC_WARN;
		else {
			switch (i) {
			case OPT_AUTO_REWIND_ON:
			case OPT_AUTO_REWIND_OFF:
				prefs.TapeOpts.AutoRewind = i == OPT_AUTO_REWIND_ON;
				break;
			case OPT_AUTO_RETENS_ON:
			case OPT_AUTO_RETENS_OFF:
				prefs.TapeOpts.AutoRetension = i == OPT_AUTO_RETENS_ON;
				break;
			}
		}
	} while (*optName);
	return (result);
}
