/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	High-priority message monitor
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <exec/tasks.h>
#include <dos/dosextens.h>
#include <devices/trackdisk.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/intuition.h>
#include <proto/icon.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Utility.h>

#include <string.h>

#include "QB.h"
#include "Tape.h"
#include "Proto.h"

/*
 *	External variables
 */

extern UBYTE		*bufferQueue[];

extern LONG			trkBufSize, userTrkBufSize, currTrkBufSize, volumeOffset;
extern ProgPrefs	prefs;

extern LONG			activeCylinder, maxCylinders, sessionCurrCylinder;
extern BYTE			lastReadBufferResult;
extern Ptr			readBuffer, readPtr;
extern ULONG		readCount, tapeSize;
extern TextPtr		destFileName;

extern MsgPortPtr	mainMsgPort;
extern UBYTE		mode, startMode;
extern BOOL			useAmigaDOS, tapeDevice, sequentialDevice;
extern DevParamsPtr	activeDevice;
extern UWORD		/*activeDisk,*/sessionCurrDisk;
extern File			destFile;
extern UBYTE		encryptVal, backupCompress;
extern ULONG		encryptSize;
extern TextChar	sessionPassword[], strBuff[];
extern WORD			numBufs;
extern BOOL			abortFlag, inputCompleted;
extern Ptr			fib;
extern UWORD		intuiVersion;
extern BOOL			catErrFlag, async, attemptCompressionPage;
extern UBYTE		*startReadPtr;
extern WORD			verifyIOB, statusIOBIndex;
extern StatStruct	stats;
extern ULONG			tranLen, tapeBufLen;

/*
 * Encryption table
 */

static UBYTE encryptTable[] = { 
	 47,  13, 191,  82, 234, 210,  59, 186,  65, 204,  94,   3, 115,  12, 254, 101,
    64, 150,  19, 222, 121,   6, 199, 134, 138,  55, 220,  44, 213,  76, 198,  98,
	  1, 123, 190,  88,  43, 231, 156,  77, 120, 187, 100,  33,  80, 228, 102,  75,
	 24, 215, 235, 177,  50, 166,  93, 140,  17,  79, 252, 181, 163, 149,  35, 144,
	135,  89, 145,   8, 229, 119, 130,  62, 184,  97, 167, 155,  36, 148, 217, 133,
	201, 178, 165,  54, 132,  51, 200, 158,  25, 116, 172, 207, 151,  70, 141, 245,
	 83, 160, 157, 244, 179, 112, 219,  16, 203, 142, 111, 255,  72, 249,   7,  28,
	221, 122, 195, 146, 189, 196, 128,  85,  32, 253, 208, 107, 233,  66, 114,   2,
	 96, 105,  18, 239, 193, 137,  21,  42, 243, 216,  61, 202,  30, 129, 154,  46,
	194,  78,  23, 162,  68,  39, 224,   0,  58, 185,  11, 173, 108, 237, 110,  56,
	250, 212,  48,   9, 226, 248, 182,  27, 218,  67, 159,  14,  91, 232,   5,  87,
	174,  20, 206,  69, 106,  41,  37,  92, 168, 223,  74, 192,  22, 152, 188, 241,
	 52,  40, 251, 126,  53,  95, 153,  60, 246,  99, 117,  10, 197,  86, 230, 143,
	 63, 136, 242,  26, 113,  15, 214, 205,  84, 118,  73, 236,  49, 171, 139, 124,
	103, 183, 225,  29, 247,  45,  81, 211, 125, 209,   4, 127, 109, 175, 131, 180,
	238, 147, 176,  31, 104, 164,  57, 169,  71, 240,  34, 170, 161,  38,  90, 227
};

static UBYTE decryptTable[] = {
	151,  32, 127,  11, 234, 174,  21, 110,  67, 163, 203, 154,  13,   1, 171, 213,
	103,  56, 130,  18, 177, 134, 188, 146,  48,  88, 211, 167, 111, 227, 140, 243,
	120,  43, 250,  62,  76, 182, 253, 149, 193, 181, 135,  36,  27, 229, 143,   0,
	162, 220,  52,  85, 192, 196,  83,  25, 159, 246, 152,   6, 199, 138,  71, 208,
	 16,   8, 125, 169, 148, 179,  93, 248, 108, 218, 186,  47,  29,  39, 145,  57,
	 44, 230,   3,  96, 216, 119, 205, 175,  35,  65, 254, 172, 183,  54,  10, 197,
	128,  73,  31, 201,  42,  15,  46, 224, 244, 129, 180, 123, 156, 236, 158, 106,
	101, 212, 126,  12,  89, 202, 217,  69,  40,  20, 113,  33, 223, 232, 195, 235,
	118, 141,  70, 238,  84,  79,  23,  64, 209, 133,  24, 222,  55,  94, 105, 207,
	 63,  66, 115, 241,  77,  61,  17,  92, 189, 198, 142,  75,  38,  98,  87, 170,
	 97, 252, 147,  60, 245,  82,  53,  74, 184, 247, 251, 221,  90, 155, 176, 237,
	242,  51,  81, 100, 239,  59, 166, 225,  72, 153,   7,  41, 190, 116,  34,   2,
	187, 132, 144, 114, 117, 204,  30,  22,  86,  80, 139, 104,   9, 215, 178,  91,
	122, 233,   5, 231, 161,  28, 214,  49, 137,  78, 168, 102,  26, 112,  19, 185,
	150, 226, 164, 255,  45,  68, 206,  37, 173, 124,   4,  50, 219, 157, 240, 131,
	249, 191, 210, 136,  99,  95, 200, 228, 165, 109, 160, 194,  58, 121,  14, 107
};

#define ENCRYPT_CHAR(x)	encryptTable[x] + encryptVal
#define DECRYPT_CHAR(x) decryptTable[(x - encryptVal) & 255]

/*
 * Open I/O operations, call from the task that will do the I/O!
 */
 
BOOL InitOp()
{
	register BOOL success = TRUE;
	DevParamsPtr devParam = activeDevice;
	register LONG openMode;
	BPTR lock;
	struct FileInfoBlock *fibPtr = (struct FileInfoBlock *) fib;
	
	if( mode == MODE_BACKUP ) {
		openMode = MODE_NEWFILE;
		if( useAmigaDOS ) {
			if( (lock = Lock(destFileName, openMode)) != NULL ) {
				if( Examine(lock, fibPtr) && (fibPtr->fib_DirEntryType >= 0) ) {
					WarningDialog(ERR_IS_DRAWER, FALSE);
					success = FALSE;
				} else {
					success = WarningDialog(ERR_FILE_EXISTS, TRUE) == 0;
				}
			}
			UnLock(lock);
		}
	} else {
		openMode = MODE_OLDFILE;
	}
	if( useAmigaDOS ) {
		(void) OpenAllDevices();
		if( success ) {
			destFile = Open(destFileName, openMode);
			success = destFile != NULL;
		}
	} else {
		success = OpenAllDevices();
		if( success ) {
			backupCompress = prefs.Compress;
			attemptCompressionPage = (mode == MODE_BACKUP);
			if( tapeDevice ) {
				success = InitTapeParameters(devParam, mode == MODE_BACKUP);
			}
			if( !success ) {
				EndOp();
			} else {
				statusIOBIndex = numBufs;
			}
		}
	}
	return(success);
}

/*
 * Perform closing of I/O operations
 */
 
void EndOp()
{
	if( useAmigaDOS ) {
		if( destFile != NULL ) {
			Close(destFile);
			if( startMode == MODE_BACKUP ) {
				if( abortFlag ) {
					DeleteFile(destFileName);
					if( intuiVersion >= OSVERSION_2_0_4 ) {
						DeleteDiskObject(destFileName);
					} else {
						strcpy(strBuff, destFileName);
						strcat(strBuff, ".info");
						DeleteFile(strBuff);
					}
				} else {
					SaveIcon(destFileName, ICON_BACKUP);
				}
			}
			destFile = NULL;
		}
	}
	attemptCompressionPage = FALSE;
	CloseAllDevices();
}

/*
 * Calls WriteBuffer(), then verifies data if necessary.
 */
 
BYTE DoWriteBuffer(DevParamsPtr devParam, Ptr buffer, ULONG size)
{
	register BYTE error;
	register ULONG i;
	register UBYTE *ptr;
	register ULONG eSize;
	register WORD index;
	BOOL encrypt;
	LONG saveActiveCylinder;
	
	verifyIOB = index = devParam->CurrIOB;
	if( sessionCurrCylinder == 0 ) {
		encrypt = (sessionCurrDisk == 1) || inputCompleted;
	} else {
		encrypt = encryptSize != 0;
	}
	if( sessionPassword[0] || encrypt ) {
		if( sessionCurrCylinder && activeCylinder ) {
			i = 0;						/* Not the first cylinder of tape or session */
		} else {
			i = sizeof(QBFirstCylHdr);
			encryptSize = 0;
			if( sessionCurrCylinder == 0 ) {
				encryptVal = ((QBNewFirstCylHdrPtr)buffer)->time;
			}
			if( ((QBFirstCylHdrPtr)buffer)->extraLen ) {
				i += 8;
				encryptSize = (sizeof(QBNewFirstCylHdr) - i)
				 + (((QBNewFirstCylHdrPtr)buffer)->catSize);
			} else {
				stats.padBytes -= i;
			}
		}
		ptr = ((UBYTE *) buffer) + i;
		eSize = size - i;
		if( !sessionPassword[0] && (encryptSize < eSize) ) {
			eSize = encryptSize;			/* Encrypt only the catalog */
		}
		stats.padBytes += (encryptSize + i);
		if( eSize ) {
			encryptSize -= MIN(encryptSize, eSize);
			while( eSize-- ) {
				*ptr++ = ENCRYPT_CHAR(*ptr);
			}
		}
	}
	devParam->BytesWritten[index] = (stats.padBytes > size) ? 0 : size - stats.padBytes;
	if( (devParam->Status == STATUS_READY) || (devParam->Status == STATUS_CHECK) ) {
		devParam->Status = STATUS_READY;
		StatusCheck(devParam);
	}
	error = WriteBuffer(devParam, buffer, size);
/*
 * If the last cylinder on disk, read-after-write immediately rather than later.
 */
 	saveActiveCylinder = activeCylinder++;
	if( (!tapeDevice) && (mode == MODE_BACKUP) && prefs.OldPrefs.Verify ) {
		if( (!error && (activeCylinder == maxCylinders)) || (saveActiveCylinder && !async) ) {
			verifyIOB = -1;
			error = !ReadAfterWrite(devParam, buffer, size);
		}
	}
/*
 * Swap current buffer
 */
	if( ++devParam->CurrIOB == numBufs ) {
		devParam->CurrIOB = 0;
	}
	if( error == 0 ) {
		stats.paddedBytes[index] = stats.padBytes;
		sessionCurrCylinder++;
		/*
		if( useAmigaDOS ) {
			AddToCompletedBytes(devParam, index);
		}
		*/
		stats.padBytes = 0;
	}
/*
	Write last partial buffer to direct access tapes.
*/
	if( ((activeCylinder+1) == maxCylinders) && tapeDevice && (!sequentialDevice) ) {
		trkBufSize = tapeSize - (activeCylinder * trkBufSize);
		tranLen = trkBufSize / tapeBufLen;
	}
	return(error);
}

/*
 * Issues write I/O command to driver for buffer
 */
 
BYTE WriteBuffer(DevParamsPtr devParam, Ptr buffer, ULONG size)
{
	register struct IOStdReq *iobPtr;
	register BYTE error;
	LONG written;
	
	if( destFile != NULL ) {
		StatusActive(devParam);				/* Just in case */
		written = Write(destFile, buffer, size);
		error = written == -1;		
	} else {
		error = -1;
		if( devParam != NULL && devParam->Status != STATUS_NOT_READY ) {
			if( (devParam->Status != STATUS_CHECK) || CheckDiskFormat(devParam) ) {
				iobPtr = (struct IOStdReq *) devParam->IOB[devParam->CurrIOB];
				StatusActive(devParam);
				if( !tapeDevice ) {
					if( trkBufSize != size ) {
						if( error = ReadBuffer(devParam, activeCylinder) ) {
							return(error);
						}
						BlockMove(((UBYTE *)readBuffer) + size, ((UBYTE *)buffer) + size, trkBufSize - size);
					}
					iobPtr->io_Command = devParam->Floppy ? TD_FORMAT : CMD_WRITE;
					iobPtr->io_Length = trkBufSize;
					iobPtr->io_Offset = (trkBufSize * activeCylinder) + volumeOffset;
					iobPtr->io_Data = buffer;
					if( async ) {
						error = 0;
						SendIO(iobPtr);
					} else {
						error = DoIO(iobPtr);
					}
				} else {
					error = TapeIO(devParam, CMD_SCSI_WRITE, (ULONG) buffer);
				}
			}
		}
	}
	return(error);
}

/*
 * Calls ReadBuffer(), retrying on error if necessary.
 */
 
BYTE DoReadBuffer(DevParamsPtr devParam, LONG cylinder)
{
	register BYTE error;
	register WORD readRetry = tapeDevice ? 1 : 3;
	register UBYTE *ptr;
	register UBYTE *tempPtr;
	register ULONG i;
	register ULONG eSize = 0;
			
	do {
		error = ReadBuffer(devParam, cylinder);
		if( error ) {
			readRetry--;
		}
	} while( error && readRetry );
	if( error == 0 ) {
	
		ptr = (UBYTE *) startReadPtr;
		i = sizeof(QBFirstCylHdr);
		if( (sessionCurrCylinder == 0) && (catErrFlag || (sessionCurrDisk == 1) || ((!tapeDevice) && (((QBOldFirstCylHdrPtr)ptr)->altCatWrappedFlag)) ) ) {
			if( (*((UWORD *) &ptr[0])) == 0x5162 ) {
				eSize = encryptSize = 0;
				if( sessionCurrDisk == 1 ) {
					encryptVal = ((QBNewFirstCylHdrPtr) ptr)->time;
					if( ((QBFirstCylHdrPtr)ptr)->extraLen ) {
						encryptSize = ((QBNewFirstCylHdrPtr)ptr)->catSize;
						i += 8;
						tempPtr = ptr + i;
						for( ; i < sizeof(QBNewFirstCylHdr); i++ ) {
							*tempPtr++ = DECRYPT_CHAR(*tempPtr);
						}
					}
					eSize = trkBufSize - i;
				} else {
					if( ((QBFirstCylHdrPtr)ptr)->extraLen == 0 ) {
						eSize = trkBufSize - i;
					} else if( ((QBOldFirstCylHdrPtr)ptr)->altCatWrappedFlag ) {
						encryptSize = 0xFFFFFFFFL;
						eSize = trkBufSize - i;
					}
				}
				if( encryptSize && (!((QBNewFirstCylHdrPtr) ptr)->password[0]) && (encryptSize < eSize) ) {
					eSize = encryptSize;
				}
				ptr += i;
			}
		} else {
			if( sessionPassword[0] ) {
				if( sessionCurrCylinder && activeCylinder ) {
					i = 0;					/* Not the first cylinder of tape or session */
				}
				ptr += i;
				eSize = trkBufSize - i;
			} else if( encryptSize ) {
				eSize = MIN(trkBufSize, encryptSize);
			}
		}
		if( eSize ) {
			encryptSize -= MIN(encryptSize, eSize);
			while( eSize-- ) {
				*ptr++ = DECRYPT_CHAR(*ptr);
			}
		}
	}
/*
	Read last partial buffer to direct access tapes.
*/
	currTrkBufSize = readCount = trkBufSize;
	if( ((activeCylinder+2) == maxCylinders) && tapeDevice && (!sequentialDevice) ) {
		trkBufSize = tapeSize - ((activeCylinder+1) * trkBufSize);
		tranLen = trkBufSize / tapeBufLen;
	}
	return(error);
}

/*
 * Reads cylinder passed into ReadBuffer of drive identified by devParam
 * This is synchronous I/O
 */
 
BYTE ReadBuffer(DevParamsPtr devParam, LONG cylinder)
{
	register struct IOStdReq *iobPtr;
	register ULONG read;
	register BYTE error;
	
	if( error = startReadPtr == readBuffer ) {
		if( destFile != NULL ) {
			read = Read(destFile, readBuffer, trkBufSize);
			error = read == -1;
		} else if( !useAmigaDOS ) {
			iobPtr = (struct IOStdReq *) devParam->IOB[devParam->CurrIOB];
			if( !tapeDevice ) {
				iobPtr->io_Command = CMD_READ;
				iobPtr->io_Offset = ((ULONG)(trkBufSize * cylinder)) + volumeOffset;
				iobPtr->io_Length = trkBufSize;
				iobPtr->io_Data = readBuffer;
				error = DoIO(iobPtr);
			} else {
				error = TapeIO( devParam, CMD_SCSI_READ, (ULONG) readBuffer );
			}
		}
		if( !error ) {
			readCount = trkBufSize;
			readPtr = readBuffer;
		}
		lastReadBufferResult = error;
	}
	return(error);
}
