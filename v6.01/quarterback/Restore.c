/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Restore loop and support routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <dos/dos.h>
#include <devices/trackdisk.h>
#include <devices/scsidisk.h>
#include <rexx/errors.h>

#include <proto/exec.h>
#include <proto/dos.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>
#include <Toolbox/BarGraph.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/DOS.h>

#include <string.h>

#include "QB.h"
#include "Proto.h"
#include "Tape.h"

/*
 *	External variables
 */

extern WindowPtr	backWindow, cmdWindow;
extern MsgPortPtr	mainMsgPort;
extern TextPtr		strPath;
extern ULONG		readCount, trkBufSize, tapeTrkBufSize, absolutePos, volumeSize;
extern ULONG		selFiles;
extern File			fHandle, destFile;
extern TextChar	strBuff[], strOpBegin[];
extern ProgPrefs	prefs;
extern LONG			activeCylinder, maxCylinders, sessionCurrCylinder;
extern ULONG		totalFiles;
extern UWORD		activeDisk, sessionCurrDisk, firstTape, firstTapeSession, selVols;
extern DevParamsPtr	firstDevice, activeDevice;
extern BOOL			abortFlag, catErrFlag, noSeqCheck;
extern UBYTE		*readPtr, *readBuffer, *startReadPtr, *compressBuffer, *bufferQueue[];
extern DirFibPtr	curFib;
extern TextPtr		dialogVarStr;
extern LONG			dialogVarNum, dialogVarNum2;
extern BYTE			readErr, lastReadBufferResult;
extern struct DateStamp sessionDateStamp, firstSessionDateStamp;
extern BOOL			altCatFlag, inputCompleted, outputCompleted;
extern UBYTE		restoreCompress, startMode, prevMode, mode, operation, verifyMode;
extern BOOL			useAmigaDOS, tapeDevice, sequentialDevice;
extern WORD			extraLen, currListItem;
extern TextChar	sessionPassword[];
extern TextChar	strWriteErr[], strReadErr[], strCreateErr[], strNoMemErr[];
extern TextChar	strCmpDiff[], strSizeDiff[], strNotLinkDiff[], strIncomplErr[];
extern TextChar	strDecompFail[];
extern TextChar	currBackupName[], currVolName[], currComment[], strFindAltCat[];
extern WORD			qbCode;
extern Ptr			fib;
extern ULONG		linkCount, lastTick;
extern WORD			useCache;
extern ULONG		bufferSize;
extern UWORD		blockSize;
extern UWORD		numVols, numDevices;
extern BOOL			processAll, needSpace, badDiskFlag, rewritingCat, firstProtErr;
extern LONG			diskID;
extern ReqTemplPtr	reqList[];
extern ULONG		encryptSize;
extern WORD			sessionNumber, catFlag;
extern ULONG		tapeBufLen, catSize;
extern UBYTE		floppy525[];
extern WORD			useSortFormat, intuiVersion;
extern ScrollListPtr	scrollList;
extern ULONG		numSCSICyls;
extern ULONG		restoreVolumeSize, numSCSICyls;
extern BOOL			supportsSetMark, restartCatalog;
extern StatStruct	stats;
extern ULONG		writtenCount, currTrkBufSize, tranLen;
extern BOOL			skipToNext;

/*
 * Local prototypes
 */
 
static void	OpenRestoreFile(DirFibPtr);
static BOOL	ReadDelimiter(BOOL);
static ULONG	ReadEndMark(void);
static BOOL	RestoreLoop(void);
static BOOL	ReadBackupData(void);
static BOOL	SetNewFileSize(ULONG);
static BOOL	RestoreBuffer(ULONG, ULONG, BOOL);
static UBYTE	WrongFormatOrSeqErr(WORD);
static UWORD	CalcDisk(ULONG, UBYTE);
static BOOL	LoadFirst(BOOL);
static BOOL	DoLoadNextDisk(void);
static WORD	ValidateDisk(void);
static WORD	ValidateBackupSetData(void);
static BOOL	SkipToDisk(void);
static void	SkipFile(void);
static void	ProcessLinks(void);
static BOOL	NextDiskReq(DevParamsPtr, BOOL);
static void	SetDiskChanged(BOOL);
static BYTE ReadTrack(BOOL);

/*
 * Loads catalog from disk (this is done before restore can begin). Returns
 * TRUE if successful, otherwise we should return to LIMBO mode.
 */
 
BOOL LoadDiskCatalog()
{
	register BOOL success;

	useSortFormat = 0;
	verifyMode = prefs.OldPrefs.TestMode;
	if (success = InitLoops()) {
		catErrFlag &= !useAmigaDOS && !tapeDevice;
		activeDevice = firstDevice;
		BeginWait();
		if (tapeDevice) {
			if (success &= TapeWaitRequester(activeDevice, NULL, CMD_TEST_UNIT_READY)) {
/*
	Disable oper buttons, menus, input...
*/
				if (needSpace && sequentialDevice && prevMode == MODE_BACKUP)
					TapeIO(activeDevice, CMD_SPACE, 0x00000100);
			}
		}
		if (success) {
			prevMode = MODE_RESTORE;
			success = DoLoadFirst(FALSE);
		}
		if (!success)
			EndLoops(!success);		// If failed, then we can deallocate everything
		EndWait();					// Enable buttons once back in correct mode
	}
	abortFlag = FALSE;
	return (success);
}

/*
 * Load catalog
 */
 
BOOL DoLoadFirst(BOOL prompt)
{
	BOOL success;
	
	success = LoadFirst(prompt);
	if( success ) {
		do {
			success = ReadDir(firstDevice);
			if( success ) {
				if( catErrFlag ) {
					activeCylinder = sessionCurrCylinder = -1;
				}
				InitDirectory();
				if( qbCode == QB_ID ) {
					while( ((ULONG)readPtr) & 3 ) {
						(void) GetChar();
					}
				}
			} else {
				if( !abortFlag ) {
					Error(ERR_READING_CATALOG);
					abortFlag = TRUE;
				}
			}
		} while( !success && !abortFlag );
	}
	return(success);
}

/*
 * Prompt to load first disk of backup set, return TRUE if successful,
 * sets abortFlag on abort. Validates volume in active drive.
 * Do not abort restore unless abortFlag is set!
 */
 
static BOOL LoadFirst(BOOL prompt)
{
	register WORD code;
	register BYTE choice = OK_BUTTON;
	register BOOL validated;
	register BOOL success = FALSE;
	RequestPtr req;
	register WORD num;
	Rectangle rect;
	BarGraphPtr barGraph = NULL;
	register IntuiMsgPtr intuiMsg;
	ULONG saveTrkBufSize;
	
	SetDiskChanged(TRUE);
	while( firstDevice->Status == STATUS_NOT_READY ) {
		dialogVarStr = firstDevice->DevName;
		if( tapeDevice ) {
			num = ERR_INSERT_TAPE;
		} else {
			dialogVarNum = 1;
			num = catErrFlag ? ERR_LOAD_DISK_LAST : ERR_LOAD_DISK_N;
		}
		if( DiskSenseDialog(num, TRUE) ) {
			return(FALSE);
		}
	}
	do {
		validated = FALSE;
		noSeqCheck = catErrFlag;
		if( !noSeqCheck ) {
			do {
				activeDisk = tapeDevice ? firstTape : !noSeqCheck;
				StatusCheck(activeDevice);
				if( code = ValidateDisk() ) {
					choice = WrongFormatOrSeqErr(code);
				} else {
					validated = TRUE;
				}
			} while( (choice == OK_BUTTON) && !validated );
		}
		if( choice != CANCEL_BUTTON ) {
			SetDiskChanged(FALSE);
			if( !validated || catErrFlag ) {			/* Find alt cat if error */
				if( !useAmigaDOS ) {
					if( !catErrFlag ) {
						DriveStatus(activeDevice);
						if( !(noSeqCheck = NextDiskReq(activeDevice, TRUE)) ) {
							choice = CANCEL_BUTTON;
						}
					}
					catErrFlag |= noSeqCheck;
					if( catErrFlag ) {
						reqList[REQ_SEEKALTCAT]->Gadgets[0].Info = strFindAltCat;
						req = DoGetRequest(REQ_SEEKALTCAT);
						saveTrkBufSize = trkBufSize;
						if( req != NULL ) {
							GetGadgetRect(GadgetItem(req->ReqGadget, 2), cmdWindow, req, &rect);
							barGraph = NewBarGraph(&rect, maxCylinders, 16, BG_HORIZONTAL);
							DoDrawBarGraph(req, barGraph);
						}
						SetStdPointer(cmdWindow, POINTER_WAIT);
						altCatFlag = FALSE;
						activeCylinder = 0;
						if( GetCurrDisk() && (activeDisk == 1) ) {
							activeCylinder++;
						}
						sessionCurrCylinder = 0;
						readPtr = readBuffer;
						do {
							while( intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort) ) {
								if( intuiMsg->Class == NEWSIZE ) {
									DoIntuiMessage(intuiMsg);
									DoDrawBarGraph(req, barGraph);
								} else {
									ReplyMsg( (MsgPtr) intuiMsg);
								}
							}
							DoSetBarGraph(req, barGraph, activeCylinder);
							if( activeCylinder < maxCylinders ) {
								startReadPtr = readPtr;
								if( (code = ValidateBackupSetData()) == 0 ) {
									if( activeCylinder || altCatFlag ) {
										success = TRUE;
										choice = OK_BUTTON;
									}
								} else if( (code == -1) || (code == 5) ) {
									choice = CANCEL_BUTTON;
								}
								if( !success ) {
									if( activeDevice->Floppy ) {
										readPtr = readBuffer;
										activeCylinder++;
									} else {
										num = readCount & 0x3FF;
										readPtr += num;
										readCount -= num;		
										trkBufSize -= 1024;
										if( readCount == 0 ) {
											readPtr = readBuffer;
											trkBufSize = saveTrkBufSize;
											activeCylinder++;
										}
									}
								}
							} else {
								MotorOff(TRUE);
								noSeqCheck = TRUE;
								SetArrowPointer();
								if( !NextDiskReq(activeDevice, TRUE) ) {
									choice = CANCEL_BUTTON;
									activeCylinder = maxCylinders;
								} else {
									activeCylinder = 0;
									if( GetCurrDisk() && (activeDisk == 1) ) {
										activeCylinder++;
									}
									SetStdPointer(cmdWindow, POINTER_WAIT);
									readPtr = readBuffer;
								}
							}
						} while( !success && (choice != CANCEL_BUTTON) );
						SetArrowPointer();
						startReadPtr = readBuffer;
						trkBufSize = saveTrkBufSize;
						if( barGraph != NULL ) {
							DisposeBarGraph(barGraph);
						}
						DestroyRequest(req);		/* Can handle NULL */
					}
				}
			} else {
				success = TRUE;
			}
		} else {
			encryptSize = 0;
		}
	} while( choice > CANCEL_BUTTON );
	return(success);
}

/*
 * Recalculate the disk # for this drive.
 */
 
void RecalcDisk(register DevParamsPtr devParam, BOOL readNext)
{
	register WORD num;
	register BOOL done;
	register BOOL first;
	register ULONG temp;
	register ULONG pos;
	BOOL diskSpan;
	UWORD temp2;
	UBYTE saveOper;
	ULONG volSize;
	DirFibPtr dirFib = curFib;
	register DevParamsPtr nextDev;
	ULONG saveAbs;
		
	if( (devParam != NULL) && (operation != OPER_SCAN) ) {
		if( (mode == MODE_BACKUP) || processAll || tapeDevice ) {
			num = devParam->Msg + numDevices;
			if( (!tapeDevice) && (num > selVols) && ((prefs.Compress & COMP_ON_FLAG) == 0) ) {
				num = 0;
			}
		} else {
			volSize = volumeSize - sizeof(QBOldFirstCylHdr);
			if( qbCode == QB_ID ) {
				volSize -= sizeof(UWORD);
			}
			saveAbs = absolutePos;
			SaveLevelFib();
			saveOper = operation;
			operation = OPER_PREP;
			nextDev = activeDevice;
			first = done = FALSE;
			num = devParam->Msg;
			if( readNext ) {
				done = (dirFib = NextFile(FALSE, FALSE)) == NULL;
				temp = 0;
			} else {
				temp = ((restoreCompress & COMP_ON_FLAG) ? stats.compressBytesInFile : writtenCount);
			}
			pos = absolutePos + stats.finishedTapeBytes + useCache;
			while( !done ) {
				temp = ((pos + temp) / volSize) + 1;
				temp2 = ((pos + ((restoreCompress & COMP_ON_FLAG) ?
					dirFib->df_ActualSize : dirFib->df_Size)) / volSize) + 1;
				diskSpan = FALSE;
				do {
					if( (num != temp) || diskSpan ) {
						if( diskSpan ) {
							temp++;
						}
						if( !first ) {
							first = TRUE;
						} else {
							nextDev = SelectNextDrive(nextDev);
						}
						done = nextDev == devParam;
					}
					diskSpan = temp < temp2;
				} while( diskSpan && !done);
				num = temp;
				if( !done ) {
					if( done = (dirFib = NextFile(FALSE, FALSE)) == NULL ) {
						num = 0;
					}
				}
				pos = absolutePos + useCache + stats.finishedTapeBytes;
				temp = 0;
			}
			operation = saveOper;
			RestoreLevelFib();
			absolutePos = saveAbs;
		}
		devParam->NextMsg = num;
	}
}	


/*
 * Just make sure the disk in current drive is the desired disk.
 */
 
void CheckCurrDisk()
{
	register DevParamsPtr devParam = activeDevice;
	
	while( !(abortFlag || badDiskFlag) && ((devParam->Status == STATUS_REMOVE) ||
		(devParam->Status == STATUS_NOT_READY)) ) {
		if( !(abortFlag |= !InsertDisk(devParam, activeDisk)) ) {
			if( GetCurrDisk() ) {			/* Get which disk this is */
				StatusActive(devParam);
			} else {
				DiskChanged();
			}
		}
	}
}

/*
 * Positions to correct disk, track, and offset based on absolutePos.
 */

static BOOL SkipToDisk()
{
	register BOOL success = TRUE;
	UWORD newDisk = 0;
	LONG newCylinder = -1;
	register LONG count = absolutePos + stats.finishedTapeBytes + useCache;
	register LONG volSize;
	register WORD strSize = sizeof(QBOldFirstCylHdr);
	BOOL forceLoad = FALSE;
	register DevParamsPtr devParam = activeDevice;
	ULONG trackBufferSize;
	
	if( qbCode == QB_ID ) {
		strSize += sizeof(UWORD);
	}
	if( tapeDevice ) {
		if( sessionCurrDisk ) {
			count += sessionCurrDisk * strSize;
		}
		trackBufferSize = tapeTrkBufSize;
	} else {
		trackBufferSize = trkBufSize;
		count += strSize;
		if( !useAmigaDOS ) {
			volSize = restoreVolumeSize - strSize;
			if( volSize > 0 ) {
				newDisk = (count / volSize)+1;
				count = count % volSize;
				if( activeDisk != newDisk ) {
					ShutDevDown(devParam);
					if( badDiskFlag ) {
						devParam->Msg = devParam->NextMsg;
						}
					if( skipToNext ) {
						activeDevice = SelectNextDrive(devParam);
						RecalcDisk(devParam, FALSE);
						if( !badDiskFlag ) {
								DriveStatus(devParam);
						}
						skipToNext = FALSE;
					} 
					sessionCurrDisk = activeDisk = newDisk;
					catErrFlag &= (activeDisk == 1);
					success = LoadNextDisk();
					if( count > encryptSize ) {
						encryptSize = 0;
					} else {
						encryptSize -= count;
					}
					forceLoad = TRUE;
				}
				if( badDiskFlag ) {
					stats.completedFiles++;
					stats.completedBytes += curFib->df_Size;
					DisplayNextFile(curFib);
					return(FALSE);
				}
				if( forceLoad && skipToNext ) {
					RecalcDisk(devParam, FALSE);
				}
				sessionCurrDisk = activeDisk;
			}
		}
	}
	if( trkBufSize ) {
		newCylinder = count / trackBufferSize;
		count = count % trackBufferSize;
	}
	
	if( success ) {
		if( !forceLoad && (sessionCurrCylinder == newCylinder) &&
			( (newDisk == 0) || (sessionCurrDisk == newDisk) ) ) {
			success = !lastReadBufferResult;
		} else {
/*
	If a tape drive, then skip past the unwanted blocks here.
*/
			if( sessionCurrCylinder < newCylinder ) {
				++sessionCurrCylinder;
			}
			while( (sessionCurrCylinder < newCylinder) && success ) {
				++activeCylinder;
				++sessionCurrCylinder;
				if( tapeDevice ) {
					success = (readErr = ReadTrack(FALSE)) == 0;
				}
			}
			if( activeCylinder < newCylinder ) {
				activeCylinder++;
			}
			if( useAmigaDOS ) {
				Seek(destFile, activeCylinder * trackBufferSize, OFFSET_BEGINNING);
			}
			if( success ) {
				success = (readErr = ReadTrack(FALSE)) == 0;
			}
		}
	}
	if( (curFib != NULL) && (curFib->df_Flags & FLAG_SEL_MASK) ) {
		DisplayNextFile(curFib);
	}
	if( !success && !abortFlag ) {
		stats.completedFiles++;
		stats.completedBytes += curFib->df_Size;
		UnableToProcess(curFib, strReadErr, RC_WARN + 2);
		SkipFile();
	} else {
		readPtr = readBuffer + count;
		readCount = currTrkBufSize - count;
	}
	return(success);
}

/*
 * Go to next device and load disk.
 */
 
static BOOL DoLoadNextDisk()
{
	DevParamsPtr devParam = activeDevice;
	
	ShutDevDown(activeDevice);
	activeDevice = SelectNextDrive(activeDevice);
	RecalcDisk(devParam, FALSE);
	return( LoadNextDisk() );
}

/*
 * Asks for next floppy, then validates it. Returns TRUE if successful.
 */
 
BOOL LoadNextDisk()
{
	register UBYTE response = OK_BUTTON;
	WORD code;
	register BOOL checked = FALSE;
	register DevParamsPtr devParam = activeDevice;
	UBYTE saveUseCache = useCache;
	
	useCache = 0;									/* Temporarily clear compression cache */
	if( badDiskFlag ) {
		badDiskFlag = FALSE;
		SLAutoScroll(scrollList, currListItem-1);
	}
	if( (devParam->Status != STATUS_REMOVE) && (devParam->Status != STATUS_NOT_READY) ) {
volCheck:
		checked = TRUE;
		noSeqCheck = FALSE;
		if( !catErrFlag || activeDisk != 1 ) {	/* If alt cat, don't validate first */
			if( code = ValidateDisk() ) {			/* Is this proper volume? */
				response = WrongFormatOrSeqErr( code );
				if( response == OK_BUTTON ) {
					goto volCheck;
				}
			}
		}
	}
	if( response == SKIP_BUTTON ) {
		badDiskFlag = skipToNext = TRUE;
	} else {
 		if( (response != CANCEL_BUTTON) && !checked ) {
			if( (response = !NextDiskReq(devParam, FALSE)) == OK_BUTTON ) {
				goto volCheck;
			}
		}
	}
	if( !(abortFlag |= (response == CANCEL_BUTTON)) ) {
		if( response == SKIP_BUTTON ) {
			StatusRemove(devParam);
		} else {
			useCache = saveUseCache;
			StatusCheck(devParam);
			/*
			if( tapeDevice && (sessionCurrDisk > 1) ) {
				stats.finishedTapeBytes = stats.completedBytes;
			}
			*/
		}
	}
	return(response == OK_BUTTON);
}

/*
 * Gets the current disk number of active device
 */
 
BOOL GetCurrDisk()
{
	register UWORD id;
	BOOL success = FALSE;
	ULONG saveEncryptSize;
	
	if( !tapeDevice && !useAmigaDOS ) {
		saveEncryptSize = encryptSize;
		sessionCurrCylinder = activeCylinder = 0;
		readPtr = readBuffer;
		if( activeDevice->Status != STATUS_NOT_READY ) {
			if( DoReadBuffer(activeDevice, activeCylinder) == 0 ) {
				id = *((UWORD *)readPtr);
				activeDisk = 0;
				if( success = (id == QB_OLD_ID) || (id == 0x4E44) || (id == QB_ID) ) {
					if( !rewritingCat || (( ((QBNewFirstCylHdrPtr)readBuffer)->date == sessionDateStamp.ds_Days ) &&
					 ( ((QBNewFirstCylHdrPtr)readBuffer)->time == sessionDateStamp.ds_Minute )) ) {
						activeDevice->Msg = activeDisk = CalcDisk(((ULONG)(*(((UWORD *)readBuffer)+1))) << 16, readBuffer[4]);
					}
				}
			}
		}
		encryptSize = saveEncryptSize;
	}
	return(success);
}

/*
 * Convert the 8-bit representation into a 16-bit one by peeking at the disk "id"
 * to see if it matches the value "val" passed, and if not, adding 256.
 * This makes QB able to handle backups bigger than 255 disks.
 * Currently this only handles up to 1024 disk backups now, saw no need
 * in looking any more past that.
 */
 
static UWORD CalcDisk(ULONG idStr, UBYTE valChar)
{
	register UWORD val = valChar;
	register UWORD iterations = 0;
	TextChar buff[16];
	register TextPtr bufPtr = &buff[0];
	register BOOL cont = FALSE;
	
	if( (idStr >> 16) != 0x7632 ) {
		do {
			if( val < 10 ) {
				*bufPtr++ = '0';		/* Put leading zero in if necessary */
			}
			NumToString(val % 100, bufPtr);
			bufPtr = &buff[0];
			if( cont = *((UWORD *)&idStr) != *((UWORD *)bufPtr) ) {
				val += 256;
			}
		} while( cont && (iterations++ < 4) );
	}
	return( cont ? 0 : val );
}

/* 
 * Sets active cylinder to zero and calls ValidateBackupSetData().
 */
 
static WORD ValidateDisk()
{
	readPtr = readBuffer;
	if( (!tapeDevice) || prefs.TapeOpts.AutoRewind ) {
		sessionCurrCylinder = 0;
		activeCylinder = 0;
	}
	encryptSize = 0;
	return( ValidateBackupSetData() );
}
	
/*
 * Reads backup set data to determine if a valid sequence number. Results are:
 *-1 = Disk not present
 * 0 = OK
 * 1 = Read Error
 * 2 = QB id check failed
 * 3 = Sequence error
 * 4 = Date/Time stamp error
 * 5 = Password error
 */
 
static WORD ValidateBackupSetData()
{
	register WORD valErrCode = 1;
	register WORD i;
	register ULONG days;
	register ULONG mins;
	BOOL rightBackup;
	register UBYTE *oldReadPtr;
	WORD newExtraLen = 0;
	UWORD valDisk = 0;
	UBYTE temp;
	
/*
	Read first buffer to get backup set data.
*/
	if( (readErr = DoReadBuffer(activeDevice, activeCylinder)) == 0 ) {
		valErrCode++;
		diskID = GetLong();
		qbCode = diskID >> 16;
/*
	Are first two characters 'QB', 'Qb' or 'ND'?
*/
		if( ((qbCode == QB_OLD_ID) || (qbCode == 0x4E44) || (qbCode == QB_ID)) &&
			 (!catErrFlag || ((((USHORT) diskID) == (QB_CAT_ID & 0xFFFF)) ||
								(((USHORT) diskID) == (QB_O_CAT_ID & 0xFFFF)))) ) {
			valDisk = CalcDisk(diskID << 16, GetChar());
			if( noSeqCheck ) {
				activeDisk = valDisk;
			}
			altCatFlag = GetChar();		/* Supposed to be for LoadFirst() */
			days = GetLong();
			mins = GetLong();
			
/*
	Another sanity check, just in case. High words of days & minutes should be zero. 
*/
			if( catErrFlag && ((days | mins) & 0xFFFF0000) ) {
				encryptSize = 0;
				return(valErrCode);
			}
			valErrCode++;
			rightBackup = (sessionCurrDisk == 1) ||
				(firstSessionDateStamp.ds_Days == days && firstSessionDateStamp.ds_Minute == mins);
			
			if( valDisk == activeDisk ) {
				if( sessionCurrDisk != 1 ) {
					valErrCode = (rightBackup || altCatFlag) ? 0 : valErrCode+1;
				} else {
					sessionDateStamp.ds_Days = days;
					sessionDateStamp.ds_Minute = mins;
					valErrCode = 0;
					restoreCompress = FALSE;
				}
				if( qbCode == QB_ID ) {
					oldReadPtr = readPtr;
					newExtraLen = GetWord();
					if( newExtraLen ) {
						bufferSize = GetLong();
						catSize = GetLong();		/* Only needed for incomplete catalog */
						temp = GetChar();
						useSortFormat = temp & 0x7F;
						supportsSetMark = (temp & 0x80) != 0;
						(void) GetChar();
						numVols = GetWord();
						restoreCompress = GetChar();
						for( i = 0 ; (i < MAX_PASSWORD_LEN+1); i++ ) {
							sessionPassword[i] = GetChar();
						}
/*
	Ask for password when scanning disks/tapes and on disk one or alternate catalog
*/
						if( sessionPassword[0] && (operation == OPER_SCAN) && (catErrFlag || (sessionCurrDisk == 1)) &&
								!DoAskPasswordRequest(sessionPassword) ) {
							valErrCode = 5;
							if( useAmigaDOS ) {
								Seek(destFile, 0, OFFSET_BEGINNING);
							}
						}
						for( i = 0 ; i < MAX_BACKUPCOMM_LEN ; i++ ) {
							currComment[i] = GetChar();
						}
						for( i = 0 ; i < MAX_BACKUPNAME_LEN ; i++ ) {
							currBackupName[i] = GetChar();
						}
						for( i = 0 ; i < MAX_BACKUPNAME_LEN ; i++ ) {
							currVolName[i] = GetChar();
						}
/*
	For future readability, just ignore any of the remaining bytes we don't understand.
*/
						for( i = 0 ; i < ((oldReadPtr - readPtr) - extraLen); i++ ) {
							GetChar();
						}
					}
				} else {
					currComment[0] = 0;
					currBackupName[0] = 0;
				}
			} else {
				activeDevice->Msg = valDisk;
				if( !rightBackup ) {
					valErrCode++;
				}
			}
		} else {
			encryptSize = 0;
		}
	}
	if( readErr == TDERR_DiskChanged ) {
		valErrCode = -1;
	} else {
		if( ((sessionCurrDisk == 1) && (valDisk == activeDisk) ) || (catErrFlag && ((diskID == QB_O_CAT_ID) || (diskID == QB_CAT_ID))) ) {
			extraLen = newExtraLen;
		}
	}
	dialogVarNum = valDisk;			/* In case wrong format requester brought up */
	return(valErrCode);
}
					
	
/*
 * Prompt for next disk, return TRUE if user complied.
 */
 
static BOOL NextDiskReq(DevParamsPtr devParam, BOOL lastDisk)
{
	if( !devParam->Floppy && !useAmigaDOS ) {
		devParam->Status = TapeIO(devParam, CMD_TEST_UNIT_READY, NULL) == 0;
	}
	activeDevice->Msg = activeDevice->NextMsg;
	if( prefs.TapeOpts.AutoRewind || (activeCylinder <= sessionCurrCylinder) ) {
		devParam->Changed = FALSE;
		DiskChanged();
		dialogVarStr = devParam->DevName;
		if( devParam == activeDevice ) {
			if( !lastDisk ) {
				dialogVarNum = activeDisk;
				abortFlag = DiskSenseDialog(tapeDevice ? ERR_LOAD_TAPE_N : ERR_LOAD_DISK_N, TRUE);
			} else {
				abortFlag = DiskSenseDialog(ERR_LOAD_DISK_LAST, TRUE);
			}
		}
		devParam->Status = abortFlag ? STATUS_NOT_READY : STATUS_CHECK;
		DriveStatus(devParam);
	} else {
		abortFlag = TRUE;					/* In case this is on a hard drive (oops!) */
	}
	return(!abortFlag);
}

/*
 * Uses result from ValidateDisk to warn operator that this disk cannot be used
 * for the restore in progress. There are several errors which can occur.
 */
 
static UBYTE WrongFormatOrSeqErr( WORD code )
{
	register WORD err = 0;
	UBYTE response;
	UBYTE choices = CANCEL_BUTTON;
	register BOOL isTape = tapeDevice;
	/*WORD num;*/
	
	ShutDevDown(activeDevice);	
	dialogVarStr = activeDevice->DevName;
	switch(code) {
	case 1:
		if( !isTape ) {
			err = ERR_RD_TRACK_ZERO;
			choices = SKIP_BUTTON;
		} else {
			err = (catFlag == CAT_READING) ? ERR_TP_TRACK_ZERO : ERR_READ_TAPE;
		}
		break;
	case 2:
		err = ERR_TP_NOT_QB;
		if( !isTape ) {
			err = ERR_RD_NOT_QB;
			choices = SKIP_BUTTON;
		}
		break;
	case 3:
		err = isTape ? ERR_TP_BAD_SEQ : ERR_RD_BAD_SEQ;
		if( operation == OPER_SCAN ) {
			err = isTape ? ERR_INSERT_TAPE : ERR_LOAD_DISK_N;
			dialogVarNum = 1;
		} else {
			if( !tapeDevice ) {
				choices = SKIP_BUTTON;
			}
			dialogVarNum2 = activeDisk;
		}
		break;
	case 4:
		err = isTape ? ERR_TP_BAD_SET : ERR_RD_BAD_SET;
		break;
	case 5:
		err = ERR_RD_PASSWORD;
		choices = 0;
		break;
	case -1:
		if( catErrFlag ) {
			return(CANCEL_BUTTON);
		} else {
			err = isTape ? ERR_INSERT_TAPE : ERR_LOAD_DISK_N;
			dialogVarNum = activeDisk;
		}
	}
	DiskChanged();
	response = DiskSenseDialog(err, choices);
	if( err == ERR_RD_PASSWORD ) {
		response = CANCEL_BUTTON;
	}
	if( (response == CANCEL_BUTTON) && tapeDevice ) {
		/*
		num = sessionNumber;
		*/
		RewindTape(activeDevice, FALSE);
		sessionNumber = 0;
		/*
		DoSpaceTape(activeDevice, num);
		*/
	}
	return(response);
}
/*
 * The main call! Call this when you are ready to roll.
 */
 
BOOL DoRestore(BOOL fromBackup)
{
	BOOL success;
	
	if( tapeDevice ) {
		firstDevice->Msg = firstDevice->NextMsg = activeDisk = firstTape;
		sessionCurrDisk = 1;
	}
	
	restoreVolumeSize = firstDevice->Floppy || tapeDevice || useAmigaDOS ? volumeSize :
		(bufferSize / blockSize) * (((numSCSICyls * blockSize)+1) / bufferSize) * blockSize;

	readCount = 0;
	firstProtErr = badDiskFlag = skipToNext = FALSE;
	
	if( !fromBackup ) {
		verifyMode = prefs.OldPrefs.TestMode;
	}
	InitList();
	if( restoreCompress && !DecompressInit((WORD)((UWORD)restoreCompress)) ) {
		Error(ERR_NO_MEM);
		abortFlag = TRUE;
	}
	UpdateStats();
	
	FirstRuntimeStrings();
	
	if( useAmigaDOS ) {
		StatusActive(firstDevice);
	} else {
		activeDisk = firstDevice->NextMsg;
		if( GetCurrDisk() ) {
			StatusCheck(firstDevice);
		}
		if( verifyMode == RMODE_SCAN ) {
			SkipToDisk();
		}
	}
	
	currTrkBufSize = trkBufSize;

	success = RestoreLoop();
	
	if( restoreCompress ) {
		DecompressDeInit();
	}
	DoFinalRuntimeStrings();
	
	SetStdPointer(cmdWindow, POINTER_WAIT);
	DoRewind(abortFlag, abortFlag || prefs.TapeOpts.AutoRewind);
	SetArrowPointer();
	
	if( tapeDevice && !abortFlag ) {
		activeCylinder++;							/* For direct-access tape drive. */
	}
	if( startMode != MODE_BACKUP ) {
		EndLoops(success);
	}
	return(success);
}

/*
 * Restore loop
 */

static BOOL RestoreLoop()
{ 
	register BOOL done = FALSE;
	register DirFibPtr dirFib;
	ULONG read;
	register ULONG written;
	BOOL newFile, err;
	WORD saveSessionNumber;
	register ULONG linksToDo = linkCount;
	ULONG num;
	BOOL fileCompressed;
		
	while( (!(abortFlag |= CheckAbortButton())) && !done ) {
		readErr = 0;
		if( fHandle == NULL ) {
			CheckCurrDisk();
			if( verifyMode == RMODE_SCAN ) {
				num = activeCylinder;
				do {
					ReadDelimiter(TRUE);
				} while( !inputCompleted && (num == activeCylinder) );
				done = inputCompleted;
			} else {
				curFib = dirFib = NextFile(tapeDevice || processAll, FALSE);
				if( dirFib != NULL && ((stats.completedFiles != selFiles) || (linksToDo)) ) {
					if( !abortFlag && SkipToDisk() ) {	/* Position readPtr to correct spot */
						skipToNext = TRUE;
						if( dirFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK) ) {
							linksToDo--;
						} else if( dirFib->df_Flags & FLAG_SEL_MASK ) {
							stats.completedFiles++;
						}
						OpenRestoreFile(dirFib);			/* Open the file on dest volume */
						newFile = TRUE;						/* Tell "Decompress()" new file */
					} else {
							done = outputCompleted;
					}
				} else {
					done = TRUE;
					lastTick = 0L;
					UpdateStats();
					if( sequentialDevice && (!prefs.TapeOpts.AutoRewind) && (startMode == MODE_RESTORE) && (stats.completedFiles != totalFiles) ) {
						saveSessionNumber = sessionNumber;
						DoSpaceTape(activeDevice, 1);/* Early ending of restore needs to space */
						sessionNumber = saveSessionNumber;
					}
				}
			}
		}
		if( fHandle != NULL && !abortFlag ) {
			fileCompressed = restoreCompress && (dirFib->df_Flags & FLAG_BITS_MASK);
			if( ReadBackupData() ) {
				read = readCount;
				if( /*restoreCompress*/fileCompressed ) {
					written = Decompress(&read, stats.byteCount, newFile);
					newFile = FALSE;
				} else {
					if( stats.byteCount < readCount ) {
						read = stats.byteCount;
					}
					written = read;
				}
				if( !(err = (written == 0) && (dirFib->df_ActualSize)) ) {
					(void) RestoreBuffer(written, read, fileCompressed);
				}
			}
		}
	}
	if( abortFlag ) {
		CloseFile();
		WaitIORequest(activeDevice->CurrIOB);
		MotorOff(FALSE);
	} else {
		if( qbCode == QB_ID ) {
			while( ((ULONG)readPtr) & 3 ) {
				GetChar();
			}
			GetLong();					/* Should be ENDD */
			GetLong();					/* Should be ENDB */
			if( tapeDevice ) {
				written = readCount % bufferSize;
				while( written ) {
					GetChar();
					written--;
				}
			}
		}
		outputCompleted = inputCompleted = TRUE;
		InitList();
		ProcessLinks();

		if( sequentialDevice && !prefs.TapeOpts.AutoRewind /*&& (startMode == MODE_BACKUP)*/ ) {
			needSpace = TRUE;
			prevMode = MODE_RESTORE;
		}
	}
	stats.rawCompletedBytes += currTrkBufSize - readCount;
	return(done);
}

/*
	Resolve links now that directories are in place
*/

static void ProcessLinks()
{
	register DirFibPtr dirFib;
	TextChar hardLinkBuff[DEVICE_PATH_BUFFER_SIZE+MAX_FILENAME_LEN];
	register BPTR lock;
	register BOOL link;
	
	if( verifyMode == RMODE_RESTORE ) {
		while( linkCount ) {
			dirFib = NextFile(FALSE, FALSE);
			if( dirFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK) ) {
				strcpy(strBuff, strPath);
				AppendDirPath(strBuff, &dirFib->df_LinkName[strlen(dirFib->df_LinkName)+1]);
				link = FALSE;
				if( dirFib->df_Flags & FLAG_HLINK_MASK ) {
					strcpy(hardLinkBuff, prefs.OldPrefs.DevPathBuf);
					AppendDirPath(hardLinkBuff, dirFib->df_LinkName);
					lock = Lock(hardLinkBuff, ACCESS_READ);
					if( lock != NULL ) {
						link = MakeLink(strBuff, (LONG) lock, FALSE);
						UnLock(lock);
					}
				} else {
					link = MakeLink(strBuff, (LONG) dirFib->df_LinkName, TRUE);
				}
/*
	Restore date & prot bits for this link.
	Currently, this does not work correctly. This could be AmigaDOS' fault.
*/
				if( link ) {
					RestoreProtectionBits(strBuff);
				}	
				linkCount--;
			}
		}
	}
}

/*
 * Writes full or partial buffer to open file.
 */
 
static BOOL RestoreBuffer(ULONG numToWrite, ULONG numRead, BOOL fileCompressed)
{
	register DirFibPtr dirFib = curFib;
	register ULONG trueLen = numToWrite;
	register ULONG numWritten = 0;
	register ULONG currLen;
	register BOOL success = !badDiskFlag;
	register WORD err;
	BOOL temp;
	UBYTE *ptr;
	File oldFHandle;
	TextChar buff[PATHSIZE+MAX_FILENAME_LEN+1];
	ULONG realSize;
	
	if( success ) {
		if( writtenCount + numToWrite > dirFib->df_Size ) {
			trueLen -= ((writtenCount + numToWrite) - dirFib->df_Size);
		}
		if( dirFib->df_Flags & FLAG_SEL_MASK ) {
			ptr = fileCompressed ? compressBuffer : readPtr;
			err = 0;
			if( verifyMode == RMODE_COMPARE ) {
/*	
	Because more bytes might be read than can fit in bufferQueue[0],
	only read what CAN fit, and then loop until all done.
*/
				temp = TRUE;
				while( trueLen && temp ) {
					currLen = MIN(trkBufSize, trueLen);
					if( (fHandle != NULL) && (fHandle != -1) ) {
						numWritten = Read(fHandle, bufferQueue[0], currLen);
						if( numWritten != currLen ) {
							err = IoErr();
							if( err == ERROR_READ_PROTECTED ) {
								strcpy(buff, strPath);
								AppendDirPath(buff, dirFib->df_Name);
								dirFib->df_Flags |= FLAG_BITS_MASK;
								EnableFileAccess(buff);
								numWritten = Read(fHandle, bufferQueue[0], currLen);
								err = IoErr();
							}
							temp = err == 0;
						}
						if( numWritten != currLen ) {
							UnableToProcess(dirFib, GetIOErrStr(err), RC_WARN - 2);
						} else {
							writtenCount += numWritten;
							if( CompareTracks(bufferQueue[0], ptr, currLen) ) {
								UnableToProcess(dirFib, strCmpDiff, RC_WARN);
								err = -1;
								currLen = trueLen;
							}
						}
					}
					if( readErr == 0 ) {
						stats.completedBytes += currLen;
					}
					trueLen -= currLen;
					ptr += currLen;
				}
			} else {
				if( verifyMode == RMODE_RESTORE ) {
					if( (fHandle != NULL) && (fHandle != -1L) ) {
/*
	Make sure non-zero on Write(), or else DoubleTalk will crash.
*/
						if( trueLen ) {
							numWritten = Write(fHandle, ptr, trueLen);
							if( numWritten == -1 ) {
								err = IoErr();
/*
	I have seen "err" be zero even when disk was full.
*/
								if( err == ERROR_DISK_FULL ) {
									UnableToProcess(dirFib, GetIOErrStr(err), RC_WARN - 2);
									abortFlag = TRUE;
									Error(ERR_DISK_FULL);
								} else {
									UnableToProcess(dirFib, strWriteErr, RC_WARN - 2);
									if( err == 0 ) {
										err = TRUE;
									}
								}
							} else {
								/* trueLen -= numWritten; */
								stats.completedBytes += numWritten;
							}
						}
					}	
				} else {
					numWritten = trueLen;
					if( readErr == 0 ) {
						stats.completedBytes += numWritten;
					}
				}
				writtenCount += numWritten;
			}
			success = (err == 0);
/*	If this is a direct-access compare, we can close the file and just
	SkipToDisk() to the next file, but otherwise we just have to read
	past the file, in which case we'll close it later.
*/
			if( !success ) {
				if( !tapeDevice ) {
					stats.completedBytes += dirFib->df_Size - writtenCount;
					CloseFile();
				}
			}
			UpdateStats();
		}
		if( !fileCompressed ) {
			readPtr += numWritten;
			readCount -= numWritten;
		}
		if( (restoreCompress & COMP_ON_FLAG) && processAll ) {
			absolutePos += numRead;
		}
		if( stats.byteCount <= numToWrite ) {
			stats.byteCount = 0;
			oldFHandle = fHandle;
			if( readErr == 0 ) {
				if( (qbCode == QB_ID) && ((realSize = ReadEndMark()) != dirFib->df_Size) ) {
					if( dirFib->df_Flags & FLAG_SEL_MASK ) {
						stats.completedBytes += dirFib->df_ActualSize - dirFib->df_Size;
						if( (realSize < dirFib->df_Size) && (verifyMode == RMODE_RESTORE) ) {
							if( SetNewFileSize(realSize) ) {
								realSize = dirFib->df_Size;
							}
						}
						if( realSize != dirFib->df_Size ) {
							UnableToProcess(dirFib, strIncomplErr, RC_WARN);
						}
					}
				}
				if( restoreCompress ) {
					while( (readCount + useCache) & 3 ) {
						(void) GetChar();
					}
				}
				CloseFile();
				if( (oldFHandle != NULL) && (oldFHandle != -1) &&
					((verifyMode == RMODE_RESTORE) || (dirFib->df_Flags & FLAG_BITS_MASK)) ) {
					dirFib->df_Flags &= ~FLAG_BITS_MASK;
					strcpy(buff, strPath);
					AppendDirPath(buff, dirFib->df_Name);
					UpdateProtectionBits(buff);
				}
			} else {
				CloseFile();
			}
		} else {	
			stats.byteCount -= numToWrite;
		}
	}
	return(success);
}

/*
 * If DOSV2.0 or greater, make the new call SetFileSize(), otherwise
 * truncate the bad-old-fashioned way.
 */
 
static TextChar tempFileName[] = "t:QBTemp";

static BOOL SetNewFileSize(ULONG newSize)
{
	register BOOL result;
	register ULONG size, remainingSize;
	register ULONG read, written;
	register File newFHandle;
	
	if( intuiVersion < OSVERSION_2_0 ) {
		newFHandle = Open(tempFileName, MODE_NEWFILE);
		if( result = newFHandle != NULL ) {
			remainingSize = newSize;
			Seek(fHandle, 0, OFFSET_BEGINNING);
			do {
				size = MIN(PATHSIZE + MAX_FILENAME_LEN + 1, remainingSize);
				read = Read(fHandle, strBuff, size);
				written = Write(newFHandle, strBuff, read);
				remainingSize -= read;
			} while( (written == size) && remainingSize );
			CloseFile();
			strcpy(strBuff, strPath);
			AppendDirPath(strBuff, curFib->df_Name);
			DeleteFile(strBuff);
			if( result = (remainingSize == 0) && (written == read) ) {
				if( result = (fHandle = Open(strBuff, MODE_NEWFILE)) != NULL ) {
					remainingSize = newSize;
					Seek(newFHandle, 0, OFFSET_BEGINNING);
					do {
						size = MIN(PATHSIZE + MAX_FILENAME_LEN + 1, remainingSize);
						read = Read(newFHandle, strBuff, size);
						written = Write(fHandle, strBuff, read);
						remainingSize -= read;
					} while( (written == size) && remainingSize );
					result = remainingSize == 0;
				}
			}
			Close(newFHandle);
			DeleteFile(tempFileName);
		}
	} else {
		result = (SetFileSize(fHandle, newSize, OFFSET_BEGINNING)) == newSize;
	}
	return(result);
}

/*
 * Reads past the data delimiter, returns TRUE if recognized as valid.
 */
 
static BOOL ReadDelimiter(BOOL anyFile)
{
	LONG id;
	TextChar buff[MAX_FILENAME_LEN+1];
	register WORD i;
	register TextPtr bufPtr;
	register BOOL success = TRUE;
	register DirFibPtr dirFib;
	register ULONG newSize;

	if( qbCode == QB_ID ) {
		dirFib = curFib;
		bufPtr = &buff[0];
		do {
			id = GetLong();
			if( id == (CFIL_ID | restoreCompress) ) {
				success = TRUE;
				/*dirFib->df_Flags |= FLAG_BITS_MASK;*/
			} else {
				switch(id) {
				case ENDBACKUP_ID:
					inputCompleted = TRUE;
				case FILE_ID:
					success = TRUE;
					/*dirFib->df_Flags &= ~FLAG_BITS_MASK;*/
					break;
				default:
					success = FALSE;
					break;
				}
			}
		} while( id == ENDDATA_ID );
		if( !inputCompleted ) {
			if( success ) {
				for( i = 0 ; i < MAX_FILENAME_LEN+1 ; i++ ) {
					*bufPtr++ = GetChar();
				}
				buff[MAX_FILENAME_LEN] = 0;
				newSize = GetLong();
				if( !anyFile ) {
					success &= strcmp(buff, dirFib->df_Name) == 0;
					if( !success ) {
						newSize = dirFib->df_Size;
						stats.completedBytes += newSize;
					}
					if( (restoreCompress & COMP_ON_FLAG) == 0 ) {
						dirFib->df_ActualSize = newSize;
						absolutePos += dirFib->df_Size - newSize;
					}
				} else {
					ConstructRuntimeItem(buff, 0, strBuff);
					AddRuntimeItem(strBuff, (WORD) strlen(strBuff));
				}
			}
		}
	}
	return(success);
}

/*
 * Attempts to find the next file in a corrupt backup
 * No longer tries to "find", just reports back FALSE.
 */
 
BOOL TryToFindNextFile(WORD id)
{
	BOOL success = FALSE;
/*
	register UBYTE i = 0;
	ULONG read;
	
	if( id ) {
*/
		MotorOff(FALSE);
		dialogVarStr = activeDevice->DevName;
		UnableToProcess(curFib, strDecompFail, RC_WARN + 2);
		abortFlag |= WarningDialog(id, TRUE);
		stats.completedBytes += curFib->df_Size - writtenCount;
		SkipFile();
/*
	}
	if( !abortFlag ) {
		success = stats.completedFiles < selFiles;
		if( processAll ) {
			while( ((ULONG)readPtr) & 3 ) {
				GetChar();
				absolutePos++;
			}
		}
		while( !abortFlag ) {
			success = ReadDelimiter(TRUE, &read);
			if( processAll ) {
				absolutePos += read;
			}
			if( success ) {
				break;
			}
			if( !i++ ) {
				abortFlag = CheckAbortButton();
			}
		}
		skipDelimiterCheck = TRUE;
	}
*/
	return(success);
}

/*
 * Returns the flags at the end of each file, currently a boolean indicating
 * whether file is corrupt, zero (FALSE) meaning it is corrupt.
 */
 
static ULONG ReadEndMark()
{
	return(GetLong());
}

/*
 * Open file to be restored. If NULL returned, don't do this file!
 * Assume that we are already in current dir for this file (setup elsewhere).
 * Looks for existing file and checks some option flags depending on what it finds.
 */
 
static void OpenRestoreFile(register DirFibPtr dirFib)
{
	BPTR lock;
	register TextPtr str = strBuff;
	register WORD err = FALSE;
	struct FileInfoBlock *fibPtr = (struct FileInfoBlock *) fib;	
	UBYTE buff[PATHSIZE + MAX_FILENAME_LEN + 1];
	UBYTE *buffPtr;
	register BOOL isLink;
		
	fHandle = NULL;
	
	if( !(isLink = (dirFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK)) != 0) ) {
		--fHandle;
		if( !ReadDelimiter(FALSE) ) {
			UnableToProcess(dirFib, strReadErr, RC_WARN + 2);
			CloseFile();
			if( abortFlag = WarningDialog(ERR_CORRUPT_CONT, TRUE) ) {
				return;
			}
		}
	}
	stats.byteCount = processAll ? dirFib->df_ActualSize : dirFib->df_Size;
	if( (!(restoreCompress && dirFib->df_Flags & FLAG_BITS_MASK)) && (qbCode == QB_ID) ) {
		while( stats.byteCount & 3 ) {
			stats.byteCount++;
		}
	}
	writtenCount = 0;	
	
	if( dirFib->df_Flags & FLAG_SEL_MASK ) {

		strcpy(strBuff, strPath);
		AppendDirPath(strBuff, dirFib->df_Name);
	
		if( verifyMode == RMODE_COMPARE ) {
			fHandle = NULL;
			if( (dirFib->df_Flags & FLAG_SLINK_MASK) == 0 ) {
				if( (lock = Lock(str, MODE_OLDFILE)) && Examine(lock, fibPtr) ) {
					if( !isLink ) {
						fHandle = Open(str, MODE_OLDFILE);
						if( fHandle != NULL ) {
							if( fibPtr->fib_Size != dirFib->df_Size ) {
								err = TRUE;
								UnableToProcess(dirFib, dirFib->df_ActualSize ? strSizeDiff : strIncomplErr, RC_WARN);
							}
						} else {
							err = (IoErr() == ERROR_OBJECT_NOT_FOUND) ? RC_WARN : RC_WARN - 2;
							UnableToProcess(dirFib, GetIOErrStr(IoErr()), err);
						}
					} else {
						buffPtr = buff;
						NameFromLock1(lock, buff, PATHSIZE + MAX_FILENAME_LEN);
						if( dirFib->df_Flags & FLAG_HLINK_MASK ) {
							buffPtr = strchr(buffPtr, ':');
							str = strchr(str, ':');
						}
						if( (str == NULL) || (buffPtr == NULL) || (strcmp(str, buffPtr) == 0) ) {
							UnableToProcess(dirFib, strNotLinkDiff, RC_WARN);
						}
					}
				} else {
					err = (IoErr() == ERROR_OBJECT_NOT_FOUND) ? RC_WARN : RC_WARN - 2;
					UnableToProcess(dirFib, GetIOErrStr(IoErr()), err);
				}
				UnLock(lock);
			}
		} else {
			if( verifyMode == RMODE_RESTORE ) {
				fHandle = NULL;
				str = strBuff;
				if( (prefs.OldPrefs.ReplaceEarlier != REPLACE_YES) ||
					(prefs.OldPrefs.ReplaceLater != REPLACE_YES) ) {
					lock = Lock(strBuff, ACCESS_READ);
					if( lock != NULL && Examine(lock, fibPtr) ) {	/* Find file? */
						switch( CompareDateStamps(&dirFib->df_DateStamp, &fibPtr->fib_Date) ? prefs.OldPrefs.ReplaceEarlier : prefs.OldPrefs.ReplaceLater ) {
						case REPLACE_ASK:
							str = RenameFileRequest( isLink ?
								&dirFib->df_LinkName[strlen(dirFib->df_LinkName)+1] :
								dirFib->df_Name);
							break;
						case REPLACE_NO:
							str = NULL;
							break;
						case REPLACE_YES:
						default:
							break;
						}
					}
					UnLock(lock);
				}
				if( str != NULL ) {
#ifndef DEMO
					if( !isLink ) {
						fHandle = Open(str, MODE_NEWFILE);
						if( fHandle == NULL ) {
							err = IoErr();
							if( (err == ERROR_DELETE_PROTECTED) || (err == ERROR_WRITE_PROTECTED) ) {
								if( EnableFileAccess(str) == 0 ) {
									fHandle = Open(str, MODE_NEWFILE);
									err = IoErr();
								}
							}
						}
						if( fHandle == NULL ) {
							UnableToProcess(dirFib, GetIOErrStr(err), RC_WARN - 2);
							if( err == ERROR_DISK_FULL ) {
								abortFlag = TRUE;
								Error(ERR_DISK_FULL);
							}
						}
					}
				}
#endif
			}
		}
		if( (str == NULL) || err ) {
			SkipFile();
			if( !processAll ) {
				stats.completedBytes += dirFib->df_Size;
			} else {
				if( fHandle == NULL ) {
					fHandle--;
				}
			}
		}
	}
	return;
}

/*
 * Set disk changed status to a given value
 */
 
static void SetDiskChanged(BOOL value)
{
	register DevParamsPtr devParam;
	
	for( devParam = firstDevice ; devParam != NULL ; devParam = devParam->Next ) {
		devParam->Changed = value;
	}
	DiskChanged();
}

/*
 * Reads data from backup into read buffer.
 */
 
static BOOL ReadBackupData()
{
	BOOL success = TRUE;
	
	if( readCount == 0L ) {					/* Something in read buffer? */
		if( stats.byteCount != 0L ) {		/* No need to read anything? */
			success = ReadNextTrack() == 0;
		}
	}
	return(success);
}

/*
 * Skip rest of file
 */
 
static void SkipFile()
{
	if( ((curFib->df_Flags & (FLAG_HLINK_MASK | FLAG_SLINK_MASK)) == 0) ) {
/*
	Test "processAll", not tapeDevice, because...
	If Open() fails, fHandle is -1L, to indicate to zip past the compression.
*/
		if( !processAll || ((restoreCompress & COMP_ON_FLAG) == 0) ) {
			CloseFile();
			stats.byteCount = 0;
		}
		UpdateStats();
	}
}

/*
 * Reads next track (activeCylinder) of current disk, or switches disks.
 * Returns error code.
 */

BYTE ReadNextTrack()
{
	stats.rawCompletedBytes += currTrkBufSize;
	activeCylinder++;
	sessionCurrCylinder++;
	return(ReadTrack(TRUE));
}

/*
 * Read a track from the disk.
 * If "fileStarted" is TRUE, the file has been opened and its name is onscreen,
 * meaning that it is the responsibility of this routine to update statistics.
 * Otherwise, it is the responsibility of the caller.
 */
 
static BYTE ReadTrack(BOOL fileStarted)
{
	WORD saveActiveCylinder, saveSessionCylinder;
	register DevParamsPtr devParam = activeDevice;
	ULONG maxCyl = maxCylinders;
	register BOOL temp;
	register BOOL retry;
	register UBYTE choice;

	do {
		DiskChanged();
/*
	Arrange to prompt for next tape if end of tape.
*/
		if( devParam->EOMBufs ) {
			maxCyl = activeCylinder;
			stats.finishedTapeBytes += devParam->EOMBufs * tapeBufLen;
			devParam->EOMBufs = 0;
			firstTapeSession = sessionNumber;
		}
		if( temp = (devParam->Status == STATUS_NOT_READY) || (!useAmigaDOS && activeCylinder >= maxCyl) ) {
			if( activeCylinder < maxCyl ) {
				saveActiveCylinder = activeCylinder;
				saveSessionCylinder = sessionCurrCylinder;
				ShutDevDown(devParam);
				temp = readErr = !LoadNextDisk();
				encryptSize = 0L;
				activeCylinder = saveActiveCylinder;
				sessionCurrCylinder = saveSessionCylinder;
			} else {
NewDisk:		activeDisk++;
				sessionCurrDisk++;
				catErrFlag &= (activeDisk == 1);
				if( (restartCatalog = tapeDevice) && (!sequentialDevice) ) {
					stats.finishedTapeBytes += tapeTrkBufSize - trkBufSize;
					trkBufSize = tapeTrkBufSize;
					tranLen = trkBufSize / tapeBufLen;
					readErr = (!DoLoadNextDisk()) || (!InitTapeParameters(activeDevice, FALSE));
/*
	Recalibrate readCount to account for a possibly new trkBufSize.
	Remember, readBuffer COULD be at a different location now, but it'd be rare.
*/
					readCount = trkBufSize - sizeof(QBFirstCylHdr);
					readPtr = &readBuffer[sizeof(QBFirstCylHdr)];
				} else {
					readErr = !DoLoadNextDisk();
				}
			}
			restartCatalog = FALSE;
		}
		devParam = activeDevice;
		if( !temp ) {
			readErr = DoReadBuffer(devParam, activeCylinder);
			StatusActive(devParam);
		}
		if( !abortFlag && (retry = readErr) ) {
			if( retry = !(temp = badDiskFlag) ) {
				if( tapeDevice ) {
					if( readErr == HFERR_SelTimeout ) {
						choice = !WarningDialog(ERR_SCSI_TIMEOUT, FALSE);
					} else {
						choice = WarningDialog(ERR_TP_RD_FAIL, TRUE);
					}
					temp = choice == OK_BUTTON;
				} else {
					do {
						if( operation == OPER_SCAN ) {
							choice = WarningDialog(ERR_TP_RD_FAIL, CANCEL_BUTTON);
						} else {
							choice = WarningDialog(ERR_READ_FAIL, SKIP_BUTTON);
						}
						temp = choice == SKIP_BUTTON;
						if( retry = choice == OK_BUTTON ) {
							retry = readErr = DoReadBuffer(devParam, activeCylinder);
						}
					} while( retry );
				}
				abortFlag = choice == CANCEL_BUTTON;
			} else {
				devParam->Msg = devParam->NextMsg;
			}
			if( temp ) {
				if( verifyMode != RMODE_SCAN ) {
					if( fileStarted ) {
						UnableToProcess(curFib, strReadErr, RC_WARN + 2);
						stats.completedBytes += curFib->df_Size - writtenCount;
						SkipFile();
					}
				} else {
					goto NewDisk;
				}
			}
		}
	} while( retry && !abortFlag );
	if( devParam->EOMBufs ) {
		if( catFlag == CAT_READING ) {
			readErr = 1;
			readCount = devParam->EOMBufs = currTrkBufSize = 0;
			firstTapeSession = sessionNumber;
		} else {
			if( !(currTrkBufSize = readCount = trkBufSize - (devParam->EOMBufs * tapeBufLen)) ) {
/*
	If no bytes were actually read last time, then just ask for the new disk NOW.
*/
				firstTapeSession = sessionNumber;
				devParam->EOMBufs = 0;
				temp = TRUE;
				goto NewDisk;
			}
		}
	/*
	} else {
		currTrkBufSize = trkBufSize;
	*/
	}
	return(readErr);
}
