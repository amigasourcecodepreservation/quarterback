/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Report generator for user logs
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <dos/dos.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/icon.h>

#include <string.h>

#include <Toolbox/DateTime.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>
#include <Toolbox/LocalData.h>
#include <Toolbox/Language.h>

#include "QB.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr	cmdWindow, backWindow;
extern MsgPortPtr	mainMsgPort;

extern TextChar	strBuff[];
extern TextChar	strProgName[], strAt[], strCommentBegin[]/*, strCommentEnd[]*/;
extern TextChar	strReportName[], strNewReportName[];
extern TextChar	strBackup[], strRestore[], strCompare[], strTest[], strScan[];
extern TextChar	strBackupL[], strRestoreL[], strCompareL[], strTestL[], strScanL[];
extern TextChar	strSessionTitle[], strOpBegin[];
extern TextChar	strCompEff[], strCompEff2[], strCompOff[], strRate[], strRate2[];
extern TextChar	strOpAborted[], strOpCompleted[], strNumErrors[];
extern UBYTE		backupCompress, restoreCompress;

extern ScrollListPtr	scrollList;
extern ProgPrefs	prefs;
extern UBYTE		verifyMode, mode;
extern BOOL			abortFlag;
extern struct DateStamp sessionDateStamp, firstSessionDateStamp, endDateStamp;

extern TextPtr		_dateFormats[];
extern TextPtr		dialogVarStr, strPath;
extern LONG			dialogVarNum;
extern ULONG		pauseTicks;
extern BOOL			disableDisplay;
extern UWORD		sessionNumber;
extern StatStruct	stats;

/*
 * Local prototypes
 */
 
static void RuntimeEventString(struct DateStamp *, TextPtr, TextPtr);
static void EventString(struct DateStamp *, TextPtr, TextPtr, BOOL);

/*
 * Output session log report if necessary.
 */
 
BOOL DoOutputReport()
{
	BPTR lock;
	register BOOL success = TRUE;
	register WORD i;
	TextChar buff[20];		
	struct DateStamp dateStamp;
	TextChar tempBuff[MAX_FILENAME_LEN+1];
	TextPtr ptr;
		
	if( prefs.LogOpts.LogNameMode != LOGNAME_NONE ) {
		strcpy(strBuff, prefs.LogOpts.LogFileName);
		if( prefs.LogOpts.LogNameMode == LOGNAME_DEF ) {
			strcpy(strBuff, strSessionTitle);
			strcat(strBuff, GetModeName(mode, TRUE));
			DateStamp(&dateStamp);
			buff[0] = ' ';
			strcpy(&buff[1], _dateFormats[DATE_SHORT]);
			for( i = 1 ; buff[i] ; i++ ) {
				if( buff[i] == '/' || buff[i] == ':' ) {
					buff[i] = '-';
				}
			}
			DateString(&dateStamp, DATE_CUSTOM, &buff[1]);
			strcat(strBuff, buff);
			buff[0] = ' ';
			TimeString(&dateStamp, FALSE, prefs.CatOpts.ShowAMPM, &buff[1]);
			if( ptr = strchr(&buff[1], ':') ) {		/* Eliminate ':' for AmigaDOS */
				BlockMove(ptr+1, ptr, strlen(ptr));
			}
			strcat(strBuff, buff);
			do {
				if( (lock = Lock(strBuff, ACCESS_READ)) != NULL ) {
					stccpy(tempBuff, strBuff, MAX_FILENAME_LEN);
					BumpRevision(strBuff, tempBuff);
					UnLock(lock);
				}
			} while( lock != NULL );
		}
		success = OutputReport(strBuff);
	}
	if( !success ) {
		Error(ERR_SAVELOG_FAIL);
	}
	return(success);
}

/*
 * Outputs the scrollable list to a file, with tabs
 */

BOOL OutputReport(TextPtr fileName)
{
	register BOOL success = FALSE;
	register WORD tabs;
	register WORD i;
	register WORD numItems = SLNumItems(scrollList);
	register File file;
	TextChar buff[256];
			
	SetBusyPointer();
	if( (file = Open(fileName, MODE_NEWFILE) ) != NULL ) {
		for( i = 0, success = TRUE ; (i < numItems) && success; i++ ) {
			
			SLGetItem(scrollList, i, buff);
			tabs = ((buff[0] & LFLAG_INDENT_MASK)-1);
			BlockMove(&buff[2], &buff[2+tabs], strlen(&buff[2])+1);
			memset(&buff[2], TAB, tabs);
				
			success = WriteStr(&buff[2], file);
			NextBusyPointer();
		}
		Close(file);
		if( success ) {
			SaveIcon(fileName, ICON_TEXT);
		}
	}
	SetArrowPointer();
	return(success);
}

/*
 *	Write a string to the file lock specified
 */

BOOL WriteStr(TextPtr str, File file)
{
	register UWORD len;
	BOOL success;
	
	len = strlen(str);
	str[len] = 0x0A;
	success = (Write(file, str, len+1) == len+1);
	str[len] = 0x00;
	return(success);
}

/*
 * Generate the inital string during backup/restore.
 */
 
void FirstRuntimeStrings()
{
	struct DateStamp *dsPtr;
	struct DateStamp dateStamp;
	
	dsPtr = (mode == MODE_RESTORE) ? &dateStamp : &sessionDateStamp;
	
	DateStamp(dsPtr);
	pauseTicks = GetTickDiff(dsPtr, &sessionDateStamp);
	
	if( sessionNumber == 0 ) {
		firstSessionDateStamp = sessionDateStamp;
	}
	RuntimeEventString(dsPtr, strOpBegin, strBuff);
	disableDisplay = prefs.LogOpts.LogDisplayOff;
	EnableScrollList();							/* Make sure all above is visible */
	RefreshScrollList();
	DisplayNextVol(strPath);
}

/*
 * Get the rate string, used in session log and AREXX macro.
 */
 
void GetRateString(TextPtr rateStr)
{
	register ULONG ticks;
	register WORD i;
	register LONG bytes = stats.completedBytes;
	
	ticks = GetTickDiff(&endDateStamp, &sessionDateStamp) - pauseTicks;
	for( i = 0 ; bytes & 0xFFE00000 ; i++ ) {
		bytes >>= 1;
	}
	ticks >>= i;
	if( ticks == 0 ) {
		ticks = 1;
	}
	bytes *= (30000 >> 4);
	bytes /= ticks;
	bytes >>= 15;
/*
	Round up!
*/
	i = bytes & 1;
	bytes >>= 1;
	bytes += i;
/*
	Now make the string!
*/ 
	NumToString(bytes, rateStr);
	if( bytes < 10 ) {
		rateStr[1] = DECIMAL_CHAR;
		rateStr[2] = rateStr[0];
		rateStr[3] = 0;
		rateStr[0] = '0';
	} else {
		i = strlen(rateStr);
		rateStr[i] = rateStr[i-1];
		rateStr[i+1] = 0;
		rateStr[i-1] = DECIMAL_CHAR;
	}
	return;
}

/*
 * Generate the final strings during backup/restore.
 */
 
void FinalRuntimeStrings()
{
	register ULONG bytes = stats.completedBytes;
	TextChar buff[40];
	TextChar numBuff[12];
	LONG compressBytes = stats.totalCompressBytes;
	
	disableDisplay = FALSE;
	EnableScrollList();
	if( prefs.LogOpts.LogRate ) {
		GetRateString(numBuff);
#if FRENCH
		dialogVarStr = GetModeName(mode, FALSE);
#else
		dialogVarStr = GetModeName(mode, TRUE);
#endif
		ParseDialogString(strRate, strBuff);
		dialogVarStr = numBuff;
		ParseDialogString(strRate2, buff);
		strcat(strBuff, buff);
		DisplayStr(strBuff);
	}
	if( prefs.LogOpts.LogCompEff ) {
		if( bytes & 0xFF000000 ) {
			bytes >>= 8;
			compressBytes >>= 8;
		}
		if( ((mode == MODE_BACKUP) ? backupCompress : restoreCompress) & COMP_ON_FLAG) {
			if( bytes ) {
				bytes = (compressBytes * 100) / bytes;
			}
			NumToString(100 - bytes, buff);
			strcpy(strBuff, strCompEff);
			strcat(strBuff, buff);
			strcat(strBuff, strCompEff2);
			DisplayStr(strBuff);
		} else {
			DisplayStr(strCompOff);
		}
	}
	dialogVarNum = stats.numErrors;
	ParseDialogString(strNumErrors, strBuff);
	DisplayStr(strBuff);
	RuntimeEventString(&endDateStamp, abortFlag ? strOpAborted : strOpCompleted, strBuff);
}

/*
 * Stuff the finishing time into the date stamp and output final strings.
 */

void DoFinalRuntimeStrings()
{
	DateStamp(&endDateStamp);
	FinalRuntimeStrings();
}

/*
 * Construct a runtime string (which requires seconds), appending to the
 * string passed either to "Backup" or "Restore", automatically.
 */
 
static void RuntimeEventString(struct DateStamp *dateStamp, TextPtr str, TextPtr buffStr)
{
	TextChar tempBuff[150];			/* Enough for now...be careful */
	
	strcpy(tempBuff, GetModeName(mode, TRUE));
	strcat(tempBuff, str);
	EventString(dateStamp, tempBuff, buffStr, TRUE);
	DisplayStr(buffStr);
}

/*
 * Return the string (read-only!) of the current mode of operation.
 * Usually used as an argument to ParseDialogString().
 */
 
TextPtr GetModeName(UBYTE currMode, register BOOL upper)
{
	register TextPtr result;
	
	if( currMode == MODE_BACKUP ) { 
		result = upper ? strBackup : strBackupL;
	} else {
		if( verifyMode == RMODE_RESTORE ) {
			result = upper ? strRestore : strRestoreL;
		} else if( verifyMode == RMODE_TEST ) {
			result = upper ? strTest : strTestL;
		} else if( verifyMode == RMODE_SCAN ) {
			result = upper ? strScan : strScanL;
		} else {
 			result = upper ?  strCompare : strCompareL;
		}
	}
	return(result);
}

/*
 * Create a string in "buffStr", constructing a sentence for output based on time.
 */
 
static void EventString(struct DateStamp *dateStamp, TextPtr str, TextPtr buffStr, BOOL secs)
{
	TextChar buff[50];
	
	strcat(strcpy(buffStr, strCommentBegin), str);
	DateString(dateStamp, DATE_ABBR, buff);
	strcat(strcat(buffStr, buff), strAt);
	TimeString(dateStamp, secs, TIME_12HOUR, buff);
	strcat(buffStr, buff);
	/*strcat(buffStr, strCommentEnd);*/
}
	
/*
 * Write a string to the report file immediately to disk
 */
 
BOOL WriteStrNow(TextPtr str)
{
	register BOOL success = FALSE;
	return(success);
}

