/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991-93 New Horizons Software, Inc.
 *
 *	Main Program
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <workbench/workbench.h>
#include <dos/dos.h>
#include <devices/keyboard.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>

#include "QB.h"
#include "Proto.h"

/*
 *	External variables
 */

extern BOOL			inMacro;

extern WORD			intuiVersion;

extern ScreenPtr	screen;
extern WindowPtr	backWindow;
extern MsgPortPtr	appIconMsgPort;
extern MsgPortPtr	mainMsgPort;
extern MsgPort		rexxMsgPort;

extern BOOL			closeFlag, quitFlag;

extern BOOL	titleChanged;

extern struct RexxLib *RexxSysBase;

extern FKey fKeyTable1[], fKeyTable2[] ;

extern WindowPtr	cmdWindow, backWindow;

extern ProgPrefs	prefs;
extern ULONG		sigBits;
extern BOOL			abortFlag;
extern UBYTE		mode, operation;
extern WORD			waitCount;

extern struct IOStdReq *keyRequest;
extern WORD			checkVolCounter;

/*
 *	Local variables and definitions
 */

#define RAWKEY_TAB		0x42
#define RAWKEY_F1		0x50
#define RAWKEY_F10		0x59
#define RAWKEY_HELP		0x5F

static BOOL newPrefs;	/* Inited to FALSE */
static BOOL oldAlt = FALSE;

#define MOUSEBUTTON(q)	\
	((q & (IEQUALIFIER_LEFTBUTTON | IEQUALIFIER_RBUTTON)) || ((q & AMIGAKEYS) && (q & (IEQUALIFIER_LALT | IEQUALIFIER_RALT))))

/*
 *	Local prototypes
 */

static void	DoFunctionKey(WindowPtr, UWORD, UWORD);
static void	DoRawKey(WindowPtr, UWORD, UWORD);
static BOOL	GetAltKeyStatus(WORD);

/*
 *	Main program
 */

void main(int argc, char *argv[])
{
	register IntuiMsgPtr	intuiMsg;
	struct RexxMsg			*rexxMsg;
	struct AppMessage		*appMsg;
	
/*
	Initialize, open operation window.
*/
	Init(argc, argv);
	SetUp(argc, argv);
/*
	Do auto exec macro
*/
	DoAutoExec(argc, argv);	

	quitFlag = FALSE;

	do {
/*
	Process intuition message
*/
		while ((intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) != NULL)
			DoIntuiMessage(intuiMsg);
/*
	Respond to NEWPrefs message (don't want to do this in requesters)
*/
		if (newPrefs && (operation != OPER_IN_PROG)) {
			if (GetSysPrefs()) {
				PrValidate(&prefs.PrintInfo.PrintRec);
			}
			newPrefs = FALSE;
		}
/*		
	Process REXX messages
	Only do one Rexx message per pass through main loop, so that all windows
		are opened/closed and program is in correct state
*/
		if (RexxSysBase &&
			(rexxMsg = (struct RexxMsg *) GetMsg(&rexxMsgPort)) != NULL)
			DoRexxMsg(rexxMsg);
/*
	Handle AppIcon messages
*/
		if (!inMacro && appIconMsgPort) {
			while ((appMsg = (struct AppMessage *) GetMsg(appIconMsgPort)) != NULL)
				DoAppMsg(appMsg);
		}
/*
	Respond to close window message
*/
		if (closeFlag || (quitFlag && cmdWindow)) {
			closeFlag = FALSE;
			RemoveOperationWindow(cmdWindow);
		}
		if (cmdWindow == NULL /*&& _tbOnPubScreen*/) {
			quitFlag = TRUE;	/* Quit QB only if on public screen normally. */
		}
/*
	Continue with main loop if closing all windows or quitting
*/
		if (!quitFlag) {
			Wait(sigBits);
		}
	} while (!quitFlag || inMacro);
/*
	Quitting
*/
	SetStdPointer(backWindow, POINTER_WAIT);
	ShutDown();
}

/*
 *	Process intuition messages
 */

void DoIntuiMessage(IntuiMsgPtr intuiMsg)
{
	register ULONG class;
	register UWORD code;
	WORD item;
	register UWORD modifier;
	register WindowPtr window;
	DialogPtr dlg;
	BOOL dialogMsg;
	BOOL newAlt;
	
	if (intuiMsg->Class == IDCMP_RAWKEY) {
		ConvertKeyMsg(intuiMsg);		/* Convert some RAWKEY to VANILLAKEY */
	}
	class	= intuiMsg->Class;
	code	 = intuiMsg->Code;
	
	modifier = intuiMsg->Qualifier;
	window	= intuiMsg->IDCMPWindow;

	/*
	if (inMacro) {	
		ReplyMsg((MsgPtr) intuiMsg);
	} else
	*/
	if (cmdWindow != NULL) {
		dialogMsg = IsDialogMsg(intuiMsg) || class == IDCMP_VANILLAKEY;
		if (dialogMsg && DialogSelect(intuiMsg, &dlg, &item)) {
			DoGadgetItem(dlg, intuiMsg, item);
			class = IDCMP_GADGETUP;
		} else {
			if (class == IDCMP_GADGETUP) {
				DoGadgetUp(window, intuiMsg);
			} else if (class == GADGETDOWN) {
				DoGadgetDown(window, intuiMsg);
			} else {
				/*
				if (class == MENUVERIFY) {
					if (code == MENUHOT) {
						if (waitCount) {
							intuiMsg->Code = MENUCANCEL;
						} else {
							if (modifier & IEQUALIFIER_RBUTTON) {
								SetStdPointer(window, POINTER_ARROW);
							}
						}
					}
				}
				*/
				ReplyMsg((MsgPtr) intuiMsg);
			}
		}
	}
/*
	Process the message
*/
	if (class == RAWKEY && (code & IECODE_UP_PREFIX))
		return;
	
/*
	Ignore user input if processing macro

	if (inMacro && class != NEWSIZE && class != REFRESHWINDOW &&
		class != ACTIVEWINDOW && class != INACTIVEWINDOW && class != NEWPREFS)
		return;
*/

/*
	Process the message
*/
	switch (class) {
	case INTUITICKS:
		if (!inMacro) {
			if ((operation == OPER_SELECTION) || (operation == OPER_LIMBO)) {
				newAlt = GetAltKeyStatus(modifier);
				if (newAlt != oldAlt) {
					SetAltButtons(newAlt);
					oldAlt = newAlt;
				}
			}
			if (operation == OPER_LIMBO) {
				if (--checkVolCounter < 0) {
					UpdateVolumeList();
				}
			} else if ((operation == OPER_PREP) || (operation == OPER_PAUSE)) {
				if (--checkVolCounter < 0) {
					checkVolCounter = 8;
					DiskChanged();
				}
			}
		}
		break;
	case NEWSIZE:
		DoNewSize(window);
		break;
	case REFRESHWINDOW:
		DoWindowRefresh(window);
		break;
	case ACTIVEWINDOW:
		DoWindowActivate(window, TRUE);
		break;
	case INACTIVEWINDOW:
		DoWindowActivate(window, FALSE);
		break;
	case CLOSEWINDOW:
		closeFlag = DoShutDown();
		break;
	case MENUPICK:
		(void) DoMenu(window, code, modifier, TRUE);
		break;
	case RAWKEY:
		if (operation == OPER_SELECTION) {
			newAlt = GetAltKeyStatus(modifier);
			if (newAlt != oldAlt) {
				SetAltButtons(newAlt);
				oldAlt = newAlt;
			}
		}
		DoRawKey(window, code, modifier);
		break;
	case VANILLAKEY:
		if ((mode == MODE_LIMBO) && (modifier & ALTKEYS) && (code == 0x00AE)) {
			HandleOperationButton(FALSE, TRUE);
		}
	case DISKINSERTED:
	case DISKREMOVED:
		UpdateVolumeList();				/* Not really necessary, since done above too */
		break;
	case NEWPREFS:
		newPrefs = TRUE;
		break;
	}
}

/*
 * Read the current key matrix to determine the ALT keys' status. Return TRUE if down.
 */
 
BOOL GetAltKeyStatus(WORD modifier)
{
	BOOL altStatus;
	UBYTE keyMatrix[16];	/* Should be in PUBLIC memory really, but it's only for 1.3 */

	if (intuiVersion < OSVERSION_2_0) {
		if (keyRequest != NULL) {
			keyRequest->io_Command = KBD_READMATRIX;
			keyRequest->io_Data = (APTR) keyMatrix;
			keyRequest->io_Length = 13;
			DoIO((struct IORequest *) keyRequest);
			altStatus = (keyMatrix[12] & 0x30) != 0;
		}
	} else {
		altStatus = (modifier & ALTKEYS) != 0;
	}
	return (altStatus);
}

/*
 * Check for abort button, return TRUE and set abortFlag if abort button hit.
 */
 
BOOL CheckAbortButton()
{
	register IntuiMsgPtr intuiMsg;
	ULONG saveFlags;
	
	if (operation != OPER_LIMBO && operation != OPER_SELECTION) {
/*
	Enable INTUITICKS once again, so that DiskChanged() can do its work
	with floppies should the disk be popped out during a pause.
*/
		saveFlags = cmdWindow->IDCMPFlags;
		ModifyIDCMP(cmdWindow, saveFlags | IDCMP_INTUITICKS);
		do {
			if (operation == OPER_PAUSE) {
				Wait(sigBits);
			}
			while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) {
				/*
				if (intuiMsg->Class == SIZEVERIFY) {
					ReplyMsg((MsgPtr) intuiMsg);
				} else {*/
					DoIntuiMessage(intuiMsg);
				/*}*/
			}
		} while ((operation == OPER_PAUSE) && (!abortFlag));
		ModifyIDCMP(cmdWindow, saveFlags);
	}
	return (abortFlag);
}

/*
 *	Handle menu events
 */

BOOL DoMenu(WindowPtr window, UWORD menuNumber, UWORD qualifier, BOOL multiSel )
{
	register UWORD menu, item, sub;
	BOOL success;
	
	success = FALSE;	 
	while (menuNumber != MENUNULL) {
		menu = MENUNUM(menuNumber);
		item = ITEMNUM(menuNumber);
		sub = SUBNUM(menuNumber);
		switch (menu) {
		case PROJECT_MENU:
			success = DoProjectMenu(window, item, sub, qualifier);
			break;
		case OPTIONS_MENU:
			success = DoOptionsMenu(window, item, sub, qualifier);
			break;
		case TAG_MENU:
			success = DoTagMenu(window, item, sub);
			break;
		case UTILITIES_MENU:
			success = DoUtilitiesMenu(window, item, sub);
			break;
		case MACRO_MENU:
			success = DoMacroMenu(window, item, sub);
			break;
		}
		menuNumber = multiSel ? ItemAddress(window->MenuStrip, menuNumber)->NextSelect : MENUNULL;
	}
	return (success);
}

/*
 *	Process function key
 */

static void DoFunctionKey(WindowPtr window, UWORD code, UWORD modifier)
{
	FKey *fKeyTable;
	
	code -= RAWKEY_F1;
	fKeyTable = (modifier & SHIFTKEYS) ? fKeyTable2 : fKeyTable1;
	if (fKeyTable[code].Type == FKEY_MENU)
		(void) DoMenu(window, (UWORD) fKeyTable[code].Data, (UWORD) (modifier & ~SHIFTKEYS), FALSE);
	else if ((fKeyTable[code].Type == FKEY_MACRO) && RexxSysBase)
		DoMacro(fKeyTable[code].Data, 0, NULL);
}

/*
 *	Handle RAWKEY messages
 *	ASCII keys will have already been stripped by ConvertKeyMsg()
 */

static void DoRawKey(WindowPtr window, register UWORD code, register UWORD modifier)
{
	if ((code & IECODE_UP_PREFIX) || (modifier & AMIGAKEYS))
		return;
	SetAltButtons((modifier & ALTKEYS) != 0);
/*
	Check for function keys
*/
	if (code >= RAWKEY_F1 && code <= RAWKEY_F10) {
		if ((modifier & IEQUALIFIER_REPEAT) == 0)
			DoFunctionKey(window, code, modifier);
	}
/*
	Check for HELP key
*/
	else if (code == RAWKEY_HELP) {
		if (modifier & SHIFTKEYS )
			DoPreferences();
		else 
			DoHelp(window);
	} else if (code >= CURSORUP || code <= CURSORLEFT) {
		DoCursorKey(code, modifier);
	}
/*
	Ignore all others for now
*/
}
