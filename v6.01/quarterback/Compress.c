/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/* 
 * Compress.c - data compression/decompression routines.
 * This is an adaptation of the public domain Un*x compress v4.0.
 */

#include <dos/dos.h>

#include <Toolbox/Memory.h>

#include <proto/dos.h>
#include <proto/intuition.h>		/* Only done for DisplayBeep() */

#include <string.h>

#include "QB.h"
#include "Proto.h"

extern UBYTE		*compressBuffer;
extern ULONG		compressBufSize;
extern BYTE			readErr;
extern ULONG		absolutePos;
extern BOOL			abortFlag, processAll;
extern Ptr 			usePtr;
extern WORD			useCache;
extern DirFibPtr	curFib;
extern StatStruct	stats;

#define EOF			-1
#define BITS		MAX_COMPBIT

/*
 * a code_int must be able to hold 2**BITS values of type int, and also -1
 */

typedef LONG	count_int;

#if BITS > 14
typedef LONG	code_int;
#else
typedef SHORT	code_int;			// Alas, we cannot do this
#endif

#define INIT_BITS 9					// Initial number of bits/code

/*
 * compress.c - File compression ala IEEE Computer, June 1984.
 *
 * Authors:	
 *		Spencer W. Thomas	(decvax!harpo!utah-cs!utah-gr!thomas)
 *		Jim McKie			(decvax!mcvax!jim)
 *		Steve Davies		(decvax!vax135!petsd!peora!srd)
 *		Ken Turkowski		(decvax!decwrl!turtlevax!ken)
 *		James A. Woods		(decvax!ihnp4!ames!jaw)
 *		Joe Orost			(decvax!vax135!petsd!joe)
 *		Mark R. Rinfret		(mods) (mark@unisec.USI.COM)
 */
 
/*
 * Greatly enhanced and optimized by Glen Merriman
 */

static LONG			n_bits;			// number of bits/code
static WORD			maxBits;		// user settable max # bits/code
static code_int		maxCode;		// maximum code, given n_bits
static code_int		maxMaxCode;		// NEVER generate this code

static count_int	*hashTable;
static code_int		*codeTable;

#define MAXCODE(n)	((1<<(n)) - 1)

static code_int		hSize;			// for dynamic table sizing

static WORD 		codeOffset;
static WORD			codeSize;

static UBYTE		*de_stack;
static ULONG		decompBufSize;
static code_int		ent;			// Last character read during Compress() -GM
static WORD			blockCompress = COMP_ON_FLAG;

static code_int		free_ent;		// first unused entry

/*
 * To save much memory, we overlay the table used by Compress() with those
 * used by decompress().  The tab_prefix table is the same size and type
 * as the codeTable.  The tab_suffix table needs 2**BITS characters.  We
 * get this from the beginning of hashTable.  The output stack uses the rest
 * of hashTable, and contains characters.  There is plenty of room for any
 * possible stack (stack used to be 8000 characters).
 */

#define tab_prefixof(i)	codeTable[i]
/*#define tab_suffixof(i)	((UBYTE *)(hashTable))[i]*/
/*#define de_stack		((UBYTE *)&tab_suffixof(1 << BITS))*/

/*
 * block compression parameters -- after all codes are used up,
 * and compression rate changes, start over.
 */
 
static BOOL clearFlag;
/*static LONG ratio;*/

#define CHECK_GAP 10000					/* ratio check interval */
static count_int checkpoint;

/*
 * These codes should not be changed lightly, as they must not
 * lie within the contiguous general code space.
 */

#define FIRST_CODE	258				/* First free entry */
#define CLEAR_CODE	257				/* Table clear output code */
#define EOF_CODE 	256				/* Last entry of file */

static LONG		offset;

/*
 * The following variables are used in BuffIO.asm- the order MUST NOT BE CHANGED!
 */
 
ULONG	bufLen;							/* # of bytes in input buffer */
ULONG	in_count;						/* Length of input read so far */
UBYTE	*srcPtr;							/* Current source ptr on Compress() */

BOOL	earlyWarning;						/* Early warning reached on tape yet? */
BOOL	useAsync;						/* Are we allowed to do async I/O? */
BOOL	async;							/* Write async I/O this buffer? */
UBYTE	buf[BITS];						/* Buffer for GetCode() and OutputCode() */
static UBYTE	saveBuf[BITS];			/* Store where GetChar() get it */

/*
 * Local prototypes
 */

static void			OutputCode(code_int); 
static code_int 	GetCode(void);
static void			ResetForBlockCompress(void);
static void			DoTryToFindNextFile(WORD);
/*static WORD		ReadChar(void);*/
/*static void		OldClearHash(count_int);*/

/*****************************************************************
 *
 * Algorithm from "A Technique for High Performance Data Compression",
 * Terry A. Welch, IEEE Computer Vol 17, No 6 (June 1984), pp 8-19.
 *
 * Algorithm:
 * Modified Lempel-Ziv method (LZW).  Basically finds common
 * substrings and replaces them with a variable size code.  This is
 * deterministic, and can be done on the fly.  Thus, the decompression
 * procedure needs no input table, but tracks the way the table was built.
 */

BOOL CompressInit(WORD compressBits)
{
	register LONG	size;
	BOOL			success = FALSE;
	
	maxBits = compressBits & COMP_BIT_MASK;
	if (maxBits < INIT_BITS)
		maxBits = INIT_BITS;
	if (maxBits > BITS) 
		maxBits = BITS;
	maxMaxCode = (1 << maxBits);
/*
 * tune hash table size for small files -- ad hoc,
 * but the sizes match earlier #defines, which
 * serve as upper bounds on the number of output codes. 
 */
	switch (maxBits) {
	case 12:
		hSize = 5003L;				// 80% occupancy
		break;
	case 13:
		hSize = 9001L;				// 91% occupancy
		break;
	case 14:
		hSize = 18013L;				// 91% occupancy
		break;
	case 15:
		hSize = 35023L;				// 94% occupancy
		break;
	case 16:
		hSize = 69001L;				// 95% occupancy
		break;
	case 17:
		hSize = 138799L;
		break;
	case 18:
		hSize = 273001L;
		break;
	}
	if ((hashTable = NewPtr(hSize*sizeof(count_int))) != NULL) {
		if ((codeTable = NewPtr(hSize*sizeof(code_int))) != NULL) {
			for(size = COMPRESS_BUFSIZE;
				(compressBuffer == NULL && size > MIN_COMPRESS_BUFSIZE);
				size >> 1) {
				compressBuffer = NewPtr(size);
				compressBufSize = size;
			}
			success = (compressBuffer != NULL);
		}
	}
	if (!success)
		CompressDeInit();
	return (success);
}

/*
 * Deallocate memory allocated in CompressInit().
 */
 
void CompressDeInit()
{
	if (hashTable) {
		DisposePtr(hashTable);
		hashTable = NULL;
	}
	if (codeTable) {
		DisposePtr(codeTable);
		codeTable = NULL;
	}
	if (compressBuffer) {
		DisposePtr(compressBuffer);
		compressBuffer = NULL;
	}
}

/*
 * Write out final data - do only when completely finished.
 */
 
BOOL CompressEOF()
{
	OutputCode((code_int) ent);
	if (readErr == 0)
		OutputCode((code_int) EOF_CODE);
	return (readErr == 0);
}

/*
 * Compress source up to length "len", outputting through PutChar().
 * If this is the first invocation for a given file, "first" is TRUE,
 * thereby initializing its internal tables.
 *
 * Algorithm:  use open addressing double hashing (no chaining) on the 
 * prefix cgode / next character combination.  We do a variant of Knuth's
 * algorithm D (vol. 3, sec. 6.4) along with G. Knott's relatively-prime
 * secondary probe.  Here, the modular division first probe is gives way
 * to a faster exclusive-or manipulation.  Also do block compression with
 * an adaptive reset, whereby the code table is cleared when the compression
 * ratio decreases, but after the table fills.  The variable-length output
 * codes are re-sized at this point, and a special CLEAR code is generated
 * for the decompressor.  Late addition:  construct the table according to
 * file size for noticeable speed improvement on small files.  Please direct
 * questions about this implementation to ames!jaw.
 */

ULONG Compress(UBYTE *src, ULONG len, BOOL first)
{
	static LONG			hshift;
	register code_int	i;
	register code_int	hsize_reg = hSize;
	register LONG		fcode, temp;
	register code_int	*codeTablePtr = codeTable;
	register count_int	*hashTablePtr = hashTable;
	LONG				lastChar;

	if (len == 0)
		return (0);
	srcPtr = src;
	bufLen = len;
	
	stats.bytes_out = 0;
	in_count = 0;
	
	if (first) {
#ifdef TUNE
/*
 * tune hash table size for small files -- ad hoc,
 * but the sizes match earlier #defines, which
 * serve as upper bounds on the number of output codes. 
 */
		if (len < (1L << 12))
			hsize_reg = MIN(5003L,hSize);
		else if (len < (1L << 13))
			hsize_reg = MIN(9001L,hSize);
		else if (len < (1L << 14))
			hsize_reg = MIN(18013L,hSize);
		else if (len < (1L << 15))
			hsize_reg = MIN(35023L,hSize);
		else 
			hsize_reg = 69001;
#endif		
		/*ratio = 0L;*/
		checkpoint = CHECK_GAP;
		hshift = 8;								/* Hash code range bound */
		for (fcode = (long) hsize_reg; fcode < 65536L; fcode <<= 1)
			hshift--;

		ClearHash(hashTablePtr, hsize_reg);	/* clear hash table */
		ent = ReadChar();
		offset = 0;
		clearFlag = FALSE;
		maxCode = MAXCODE(n_bits = INIT_BITS);
		free_ent = FIRST_CODE;
		stats.compressBytesInFile = stats.bytesCompressedInFile = 0L;
	}
	while ((temp = ReadChar()) >= 0) {	/* Actually, while not EOF, but this faster */
		fcode = (temp << maxBits) + ent;
		i = (temp << hshift) ^ ent;			/* xor hashing */
		lastChar = temp;
		
		if (hashTablePtr[i] == fcode) {
			ent = codeTablePtr[i];
			continue;
		} else 
			if ((long) hashTablePtr[i] < 0)	/* empty slot */
				goto nomatch;
		temp = hsize_reg - i;					/* secondary hash (after G. Knott) */
		if (i == 0)
			temp = 1;
probe:
		if ((i -= temp) < 0)
			i += hsize_reg;
		if (hashTablePtr[i] == fcode) {
			ent = codeTablePtr[i];
			continue;
		}
		if ((long) hashTablePtr[i] > 0)
			goto probe;
nomatch: 
		OutputCode((code_int) ent);
		if (readErr == 0) {
			ent = lastChar;
			if (free_ent < maxMaxCode) {
				codeTablePtr[i] = free_ent++;	/* code -> hashtable */
				hashTablePtr[i] = fcode;
			} else {
				if ((count_int) in_count >= checkpoint && blockCompress)
					ResetForBlockCompress();
					if (readErr)
						break;
			}
		} else {
			break;
		}
	}
	stats.totalCompressBytes += stats.bytes_out;
	return (stats.bytes_out);
}

/*****************************************************************
 *
 * Output the given code.
 * Inputs:
 * 	code:	A n_bits-bit integer.  If == -1, then EOF.  This assumes
 *		that n_bits =< (long)wordsize - 1.
 * Outputs:
 * 	TRUE if success
 * Assumptions:
 *	Chars are 8 bits long.
 * Algorithm:
 * 	Maintain a BITS character long buffer (so that 8 codes will
 * fit in it exactly).  Use the VAX insv instruction to insert each
 * code in turn.  When the buffer fills up empty it and start over.
 */

UBYTE lmask[9]= {
	0xff,0xfe,0xfc,0xf8,0xf0,0xe0,0xc0,0x80,0x00
};
UBYTE rmask[9]= {
	0x00,0x01,0x03,0x07,0x0f,0x1f,0x3f,0x7f,0xff
};

static void OutputCode(register code_int code)
{
	register LONG	r_off = offset;
	register LONG	bits = n_bits;
	register LONG	count;
	register UBYTE	*bp = buf;
	
	if (code != EOF_CODE) {
		bp += (r_off >> 3);					/* Get to the first byte. */
		r_off &= 7;
/*
	Since code is always >= 8 bits, only need to mask the first
	hunk on the left.
*/
		*bp = (*bp & rmask[r_off]) | (code << r_off) & lmask[r_off];
		bp++;	/* Putting this as part of above line will cause Optimizer to screw up */

		count = 8 - r_off;
		bits -= count;
		code >>= count;
		if (bits >= 8) {						/* Get any 8-bit parts in the middle */
			*bp++ = code;						/* (<=1 for up to 16 bits). */
			code >>= 8;
			bits -= 8;
		}
		if (bits)							/* Last bits. */
			*bp = code;
		offset += n_bits;
		if (offset == (n_bits << 3)) {
			PutGroup(n_bits);
			offset = 0;
		}
/* 
	If the next entry is going to be too big for the code size,
	then increase it, if possible.
*/
		if (free_ent > maxCode || clearFlag) {
/* 
	Write the whole buffer, because the input side won't
	discover the size increase until after it has read it.
*/
			if (offset > 0) {
				PutGroup(n_bits);
			}
			offset = 0;

			if (clearFlag) {
				maxCode = MAXCODE(n_bits = INIT_BITS);
				clearFlag = FALSE;
			} else {
				++n_bits;
				if (n_bits == maxBits)
					maxCode = maxMaxCode;
				else
					maxCode = MAXCODE(n_bits);
			}
		}
	} else {
	 	count = (offset + 7) >> 3;			/* At EOF, write the rest of the buffer. */
		if (count > 0) {
			PutGroup(count);
		}
		offset = 0;
	}
}

/*
 * Call this before any decompression takes place
 */
 
BOOL DecompressInit(WORD compressBits)
{
	BOOL	success = FALSE;

	blockCompress = compressBits & COMP_ON_FLAG;
	decompBufSize = 32768;
	de_stack = NewPtr(decompBufSize);
	if (de_stack != NULL) {
		success = CompressInit(compressBits);
		if (!success) {
			DecompressDeInit();
		}
	}
	return (success);
}

/*
 * Call this upon dispensing with decompression
 */
 	
void DecompressDeInit()
{
	CompressDeInit();
	if (de_stack != NULL) {
		DisposePtr(de_stack);
		de_stack = NULL;
	}
}

#define WriteChar(y) (*destPtr++=y)

/*
 * Decompress input buffer to output.  This routine adapts to the codes in the
 * file building the "string" table on-the-fly; requiring no table to
 * be stored in the compressed file.  The tables used herein are shared
 * with those of the compress() routine.  See the definitions above.
 */

ULONG Decompress(ULONG *len, ULONG lenFile, BOOL first)
{
	static UBYTE		*stackp;
	static LONG			finchar;
	static code_int		oldcode;
	register WORD		temp;
	register code_int	code;
	register UBYTE		*destPtr;
	register UBYTE		*tablePtr = (UBYTE *)hashTable;
	code_int			incode;
	UBYTE				*newStack;
	WORD				backTrack;
	
	if (lenFile == 0) {
		*len = 0;
		return (0);
	}
	destPtr = compressBuffer;
	in_count = 0;
	bufLen = *len;
	stats.bytes_out = 0;

	if (first) {
		stats.compressBytesInFile = 0L;
		if (lenFile == 1) {
			*len = 1;
			stats.compressBytesInFile = 2L;
			stats.bytes_out++;
			WriteChar(GetChar());
			GetChar();					/* Snag terminating zero. */
			return (1L);
		}
		maxCode = MAXCODE(n_bits = INIT_BITS);
		free_ent = FIRST_CODE;
		clearFlag = FALSE;

		temp = 0;
		do {
			*tablePtr++ = (UBYTE)temp;
		} while (++temp < 256);
		ClearPrefixTable(codeTable);
		tablePtr = (UBYTE *)hashTable;
		
		codeSize = codeOffset = 0;
		finchar = oldcode = GetCode();
		if (oldcode == EOF_CODE) {
			return (0L);
		}
		stats.bytes_out++;
		WriteChar(finchar);
	} else {
		if (processAll) {
			absolutePos += useCache;
		}
		useCache = 0;
		while (stackp > de_stack) {
			stats.bytes_out++;
			WriteChar(*--stackp);
		}
	}
	stackp = de_stack;
	while ((code = GetCode()) != EOF_CODE) {
		if (code >= hSize) {
			DoTryToFindNextFile(ERR_CORRUPT_COMPRESS);
			return (0);
		}
		if ((code == CLEAR_CODE) && blockCompress) {
			ClearPrefixTable(codeTable);
			clearFlag = TRUE;
			free_ent = FIRST_CODE - 1;
			if ((code = GetCode()) == EOF_CODE) 
				break;
		}
		incode = code;
		if (code >= free_ent) {			/* Special case for KwKwK string. */
			*stackp++ = finchar;
			code = oldcode;
		}
		while (code >= 256) {				/* Generate output in reverse order. */
			if (((ULONG) code) >= hSize) {
				DoTryToFindNextFile(ERR_CORRUPT_COMPRESS);
				return (0);
			}
			*stackp++ = tablePtr[code];
			code = tab_prefixof(code);
			if (stackp - de_stack == decompBufSize) {
				newStack = NewPtr(decompBufSize + decompBufSize);
				if (newStack != NULL) {
					BlockMove(de_stack, newStack, decompBufSize);
					DisposePtr(de_stack);
					de_stack = newStack;
					stackp = de_stack + decompBufSize;
					decompBufSize += decompBufSize;
				} else {
					DoTryToFindNextFile(ERR_CORRUPT_OR_NOMEM);
					return (0);					/* Could not enlarge stack! */
				}
			}
		}
		if (((ULONG) code) >= hSize) {
			DoTryToFindNextFile(ERR_CORRUPT_COMPRESS);
			return (0);
		}
		*stackp++ = finchar = tablePtr[code];
		do {									/* Output codes in forward order. */
			stats.bytes_out++;
			WriteChar(*--stackp);
			if (stats.bytes_out == lenFile) {
				if (stackp != de_stack) {
					break;
				}
			} else if (stats.bytes_out > compressBufSize) {
				Error(ERR_UNKNOWN);
				return (0);						/* OOPS, if this happens, it's my bug. -GM */
			}
		} while (stackp > de_stack);
/*
	Generate the new entry.
*/
		if ((code = free_ent) < maxMaxCode) {
			tab_prefixof(code) = oldcode;
			tablePtr[code] = finchar;
			free_ent = code+1;
		}
		if (lenFile - stats.bytes_out < 16) {
			useCache = 0;
		}
		oldcode = incode;						/* Remember previous code */
		if (in_count > bufLen ||
			stats.bytes_out > (compressBufSize - (MIN_COMPRESS_BUFSIZE*3)/4) ||
			stats.bytes_out == lenFile) {
			if (temp = codeSize - (codeOffset - n_bits)) {
				temp--;
				if (buf[n_bits - (temp >> 3)]) {
					if (stats.bytes_out == lenFile) {
						temp = 0;
					}
				}
			}
			backTrack = temp >> 3;
			in_count -= backTrack - useCache;
			useCache = backTrack;
			if (curFib->df_Flags & FLAG_SEL_MASK) {
				stats.totalCompressBytes += in_count;
			}
			stats.compressBytesInFile += in_count;
			BlockMove(buf, saveBuf, BITS);
			usePtr = &saveBuf[n_bits - backTrack];
			break;							/* Stop if more than one buffer read */
		}	
	}
	*len = in_count;
	return (stats.bytes_out);
}

/*
 * Read one code from the input.  If EOF, return EOF_CODE.
 */

static code_int GetCode()
{
	register code_int	code;
	register LONG		bits = n_bits;
	register WORD		r_off;
	register UWORD		c;
	register UBYTE		*bp = buf;
	
	if (clearFlag || codeOffset >= codeSize || free_ent > maxCode) {
/* 
	If the next entry will be too big for the current code
	size, then we must increase the size.  This implies reading
	a new buffer full, too
*/
		if (free_ent > maxCode) {
			bits++;
			if (bits == maxBits)
				maxCode = maxMaxCode;		/* won't get any bigger now */
			else
				maxCode = MAXCODE(bits);
		}
		if (clearFlag) {
			maxCode = MAXCODE(bits = INIT_BITS);
			clearFlag = FALSE;
		}
		codeSize = 0;
		while (codeSize < bits) {
			c = GetChar();
			if (readErr) {
				n_bits = bits;
				return (EOF_CODE);
			}
			in_count++;
			buf[codeSize++] = c;
		}
		codeOffset = 0;
		codeSize = (codeSize << 3)-(bits - 1);	/* Round down to integral # of codes. */
	}
	r_off = codeOffset;
	codeOffset += bits;
	n_bits = bits;
	bp += (r_off >> 3);						/* Get to the first byte. */
	r_off &= 7;
	code = (*bp++ >> r_off);				/* Get first part (low order bits) */
	r_off = 8 - r_off;
	bits -= r_off;

	if (bits >= 8) {							/* Get any 8-bit parts in the middle */
		code |= *bp++ << r_off;				/* (<=1 for up to 16 bits). */
		r_off += 8;
		bits -= 8;
	}
	return (code | ((*bp & rmask[bits]) << r_off));
}

/*
 * Find next delimiter if lost.
 */
 
static void DoTryToFindNextFile(WORD id)
{
	TryToFindNextFile(id);
	readErr = TRUE;
	if (processAll)
		abortFlag = TRUE;
}

/*
 * Clear table for block compress 
 */

static void ResetForBlockCompress()
{	
#ifdef BLAH
	register LONG	rat;
	
	checkpoint = in_count + CHECK_GAP;
	
	if (in_count > 0x007fffff) {			/* shift will overflow */
		rat = stats.bytes_out >> 8;
		if (rat == 0)						/* Don't divide by zero */
			rat = 0x7fffffff;
		else
			rat = in_count/rat;
	} else {
		rat = (in_count<<8)/stats.bytes_out;	/* 8 fractional bits */
	}
	if (rat > ratio) {
		ratio = rat;
	} else {
		/*ratio = 0;*/
		ClearHash(hashTable, hSize);
		free_ent = FIRST_CODE;
		clearFlag = TRUE;
		OutputCode((code_int) CLEAR_CODE);
	}
#endif
}

#ifdef WE_LIKE_SLOW_BACKUPS_AND_SLOW_MEMSETS
/*
 * Reset code table
 */
 
static void OldClearHash(register count_int hashSize)
{
	register count_int	*hashTablePtr = hashTable + hashSize;
	register LONG		i;
	register LONG		m1 = 0xDEAFFEED;

	i = hashSize - 16;
	do {
		memset(hashTablePtr-16, m1, sizeof(count_int) << 4);
		hashTablePtr -= 16;
	} while ((i -= 16) >= 0);

	for (i += 16; i > 0; i--) {
		*--hashTablePtr = m1;
	}
}

/*
 * Reads a character from the source, if no more, return End Of File
 */
 
static WORD ReadChar()
{
	WORD	ch;
	
	if (in_count != bufLen) {
		in_count++;
		if (sourceBytesReset == 0) {
			compressBytesInput++;
			compressBytesInFile++;
		} else {
			sourceBytesReset--;
		}
		ch = *srcPtr++;
	} else {
		ch = EOF;
	}
	return (ch);
}
#endif
