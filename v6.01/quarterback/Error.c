/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Error/Help report routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>

#include <string.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Image.h>
#include <Toolbox/Language.h>

#include "QB.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ProcessPtr	process;
extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;
extern WindowPtr	backWindow, cmdWindow;

extern TextChar	screenTitle[];

extern TextPtr		strsErrors[];
extern TextChar	strDemo[], strBadFile[], strMemory[];
extern TextChar	strDOSError[], strFileNotFound[], strDiskLocked[];
extern TextChar	strFileNoDelete[], strFileNoRead[]/*, strKilo[]*/;
extern TextChar	strBuff[];
extern TextPtr		dialogVarStr;
extern LONG			dialogVarNum, dialogVarNum2;

extern ReqTemplPtr	reqList[];
extern DlgTemplPtr	dlgList[];
extern RequestPtr	requester;

extern BOOL			tapeDevice, force;

extern DevParamsPtr	activeDevice;

/*
 *	Local variables and definitions
 */

#define ERROR1_TEXT		3
#define ERROR2_TEXT		4
#define ICON_ITEM		5

#define ABOUT_CHIPMEM	1
#define ABOUT_FASTMEM	2
#define ABOUT_TITLE		3

/*
 * Local prototypes
 */
 
static BYTE		DoWarningDialog(WORD, BYTE, BOOL);

/*
 *	Error report routine
 *	Put error message in dialog, or if no memory, on screen title bar
 */

void Error(WORD errNum)
{
	(void) WarningDialog(errNum, FALSE);
	return;
}

/*
 *	Return screen titles to default setting
 */

void FixTitle()
{
	SetWindowTitles(backWindow, (TextPtr) -1, screenTitle);
	SetWindowTitles(cmdWindow, (TextPtr) -1, screenTitle);
}

/*
 *	Display DOS error message dialog in specified window
 *	If not enough memory, then use Error() routine
 *	If dosError is 0, then cause was not enough memory
 */

void DOSError( WORD errNum, LONG dosError)
{
	register TextPtr errText = NULL;
	
	switch( dosError ) {
	case -2:
		errText = strDemo;
		break;
	case -1:
		errText = strBadFile;
		break;
	case 0:
	case ERROR_NO_FREE_STORE:
		errText = strsErrors[ERR_NO_MEM];
		break;
	case ERROR_OBJECT_NOT_FOUND:
	case ERROR_DIR_NOT_FOUND:
	case ERROR_DEVICE_NOT_MOUNTED:
	case ERROR_OBJECT_WRONG_TYPE:
		errText = strFileNotFound;
		break;
	case ERROR_DELETE_PROTECTED:
		errText = strFileNoDelete;
		break;
	case ERROR_DISK_WRITE_PROTECTED:
		errText = strDiskLocked;
		break;
	case ERROR_READ_PROTECTED:
		errText = strFileNoRead;
		break;
	default:
		if( dosError >= 0 && dosError <= 999 ) {
			dialogVarNum = dosError;
			(void) WarningDialog(ERR_DOS_ERROR, FALSE);
/*
			NumToString(dosError, strDOSError + strlen(strDOSError) - 3);
			errText = strDOSError;
*/
		} else
			errText = strsErrors[ERR_UNKNOWN];
		break;
	}
	if( errText != NULL ) {
		reqList[REQ_ERROR]->Gadgets[ERROR2_TEXT].Info = errText;
		Error( errNum );
		reqList[REQ_ERROR]->Gadgets[ERROR2_TEXT].Info = NULL;
	}
	return;
}

/*
 * Copy source string to destination string, replacing "@" with the
 * dialog string variable and "%" with the dialog numeric variable.
 * If "\n" is present, returns where it was, otherwise returns NULL.
 */
 
TextPtr ParseDialogString(register TextPtr srcStr, register TextPtr destStr)
{
	TextChar buff[10];
	register BOOL terminate = FALSE;
	register TextChar ch;
	TextPtr secondStr = NULL;

	while( srcStr != NULL && !terminate ) {
		ch = *srcStr++;
		switch(ch) {
		case '%':
		case '~':
			NumToString((ch == '%') ? dialogVarNum : dialogVarNum2, buff);
			strcpy(destStr, buff);
			destStr += strlen(buff);
			break;
		case '@':
			if( dialogVarStr != NULL ) {
				strcpy(destStr, dialogVarStr);
				destStr += strlen(dialogVarStr);
			}
			break;
/*
	If in German, we have to do word-wrapping manually, since the simple parser
	is not designed to do more than two lines' worth.
*/

#if (AMERICAN | BRITISH | FRENCH | SWEDISH)
		case '\n':
			secondStr = destStr;
#endif
		case 0:
			terminate = ch == 0;
		default:
			*destStr++ = ch;
		}
	}
	return( secondStr );
}

/*
 * Call DoWarningDialog(), paying no attention to disk inserted/removed interrupt.
 */
 
BYTE WarningDialog(WORD errNum, BYTE choice)
{
	return( DoWarningDialog(errNum, choice, FALSE) );
}

/*
 * Same as above, only this routine returns OK_BUTTON if disk inserted/removed.
 */
 
BYTE DiskSenseDialog(WORD errNum, BYTE choice)
{
	return( DoWarningDialog(errNum, choice, TRUE) );
}	

/*
 *	Open dialog with "choice"+1 number of buttons, up to three buttons.
 * Returns the button number depressed, which is zero for OK.
 *	Replace @s with contents of "dlgName", and % with number in "dlgNum".
 * Word wrap, but if "\n is present, automatically go to next line.
 * The next three lines apply only when "autoDisk" is TRUE:
 *
 * If choice is negative, then this routine will return -2 when a disk is
 * inserted. Otherwise, a disk insertion will result in "OK_BUTTON".
 */

static BYTE DoWarningDialog(WORD errNum, BYTE choice, BOOL autoDisk)
{	
	register WORD item = OK_BUTTON;
	WORD reqNum;
	register TextPtr str2;
	register WORD len, pixelWidth;
	Rectangle rect;
	GadgetPtr gadgList;
	register BOOL changed = FALSE;
	register RequestPtr req;
	register WORD i;
	TextChar buf[TMPSIZE];
	Ptr saveImage;
	
	if( force ) {
		force = FALSE;
		return(OK_BUTTON);
	}
	
	switch( ABS(choice) ) {
	case TRUE:
		reqNum = REQ_WARN;
		break;
	case 2:
		reqNum = REQ_SKIP;
		break;
	default:
	case FALSE:
		reqNum = REQ_ERROR;
		break;
	}
	if (errNum > ERR_MAX_ERROR) {
		errNum = ERR_UNKNOWN;
	}
	str2 = ParseDialogString(strsErrors[errNum], buf);
	len = strlen(buf);
	saveImage = reqList[reqNum]->Gadgets[ICON_ITEM].Info;
	reqList[reqNum]->Gadgets[ICON_ITEM].Info = (Ptr) ((len && buf[len-1] == '?') ? IMAGE_ICON_CAUTION : IMAGE_ICON_STOP);
	BeginWait();
	if( (req = DoGetRequest(reqNum)) != NULL ) {
		gadgList = req->ReqGadget;
		requester = req;
		GetGadgetRect(GadgetItem(gadgList, ERROR1_TEXT), cmdWindow, req, &rect);
		pixelWidth = (req->Width - rect.MinX) - 4;
/*
	See note above about the German language.
*/
#if (AMERICAN | BRITISH | FRENCH | SWEDISH)		
		if( str2 == NULL ) {
			str2 = &buf[len];
/*
	Find last character that can fit in one line
*/		
			while( TextLength(req->RWindow->RPort, buf, len) > pixelWidth ) {
				--len;
				--str2;
				changed = TRUE;
			}
/*
	Word wrap if we changed anything
*/
			if( changed ) {
				while( (str2 != buf) && (str2[-1] != '-') && (str2[-1] != ' ') ) {
					--str2;
				}
			}
		} else {
			changed = TRUE;
		}
		if( changed ) {
			len = strlen(str2);
			if( *str2 == '\n' ) {
				str2++;
				len--;
			}
			while( TextLength(req->RWindow->RPort, str2, len) > pixelWidth ) {
				--len;
				str2[len] = 0;
			}
			SetGadgetItemText(gadgList, ERROR2_TEXT, cmdWindow, req, str2);
			*str2 = 0;
		}
#endif
		SetGadgetItemText(gadgList, ERROR1_TEXT, cmdWindow, req, buf);
		OutlineOKButton(cmdWindow);
		ErrBeep();
		do {
			if( autoDisk ) {
				item = -1;
				for( i = 0 ; (i < 9) && (item == -1) ; i++ ) {
					Delay(5);
					item = CheckRequest(mainMsgPort, cmdWindow, DialogFilter);
				}
			} else {
				WaitPort(mainMsgPort);
				item = CheckRequest(mainMsgPort, cmdWindow, DialogFilter) ;
			}
			if( autoDisk && DiskChanged() ) {
				if( activeDevice->Status == STATUS_CHECK ) {
					if( item != CANCEL_BUTTON ) {
						item = choice > 0 ? OK_BUTTON : -2;
						DepressGadget(GadgetItem(req->ReqGadget, OK_BUTTON), cmdWindow, requester);
					}
				}
			}
		} while( item == -1 );
		DestroyRequest(req);
	} else {
		SetWindowTitles(backWindow, (TextPtr) -1, buf);
		SetWindowTitles(cmdWindow, (TextPtr) -1, buf);
		ErrBeep();
	}
	reqList[reqNum]->Gadgets[ICON_ITEM].Info = saveImage;
	EndWait();
	DoWindowRefresh(cmdWindow);
	reqList[reqNum]->Gadgets[ERROR1_TEXT].Info = NULL;
	reqList[reqNum]->Gadgets[ERROR2_TEXT].Info = NULL;
	return( (BYTE) item );
}
	
/*
 *	Display help dialog in specified window
 *	If cannot show full help dialog, just show memory info
 */

void DoHelp(WindowPtr window)
{
	/*
	register WORD item;
	register DialogPtr dlg;
	TextChar chipMem[10], fastMem[10];

	BeginWait();
	NumToString(AvailMem(MEMF_CHIP)/1024, chipMem);
	NumToString(AvailMem(MEMF_FAST)/1024, fastMem);
	strcat(chipMem, strKilo);
	strcat(fastMem, strKilo);
	dlgList[DLG_ABOUT]->Gadgets[ABOUT_CHIPMEM].Info =
		dlgList[DLG_MEMORY]->Gadgets[ABOUT_CHIPMEM].Info = chipMem;
	dlgList[DLG_ABOUT]->Gadgets[ABOUT_FASTMEM].Info =
		dlgList[DLG_MEMORY]->Gadgets[ABOUT_FASTMEM].Info = fastMem;
	if ((dlg = GetDialog(dlgList[DLG_ABOUT], screen, mainMsgPort)) == NULL ) {
		EndWait();
		Error(ERR_NO_MEM);
		return;
	}
	OutlineOKButton(dlg);
	do {
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
	} while( item != OK_BUTTON);
	DisposeDialog(dlg);
	EndWait();
	*/
	DoAboutBox();
}

#ifdef CHECK_MEM
/*
 *	Check remaining memory and give warning if getting low
 */

void CheckMemory()
{
	register WORD num;

	if (AvailMem(0) < 0x3FFFL) {
		SetWindowTitles(backWindow, (TextPtr) -1, strMemory);
		for (num = 0; num < numWindows; num++)
			SetWindowTitles(windowList[num], (TextPtr) -1, strMemory);
		ErrBeep();
	}
}

#endif