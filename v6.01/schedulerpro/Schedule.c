/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Scheduler
 *
 *	Schedule functions
 */

#include <exec/types.h>

#include <proto/dos.h>
#include <proto/icon.h>

#include <string.h>

#include <stdio.h>
#include <Toolbox/DateTime.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Utility.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/ScrollList.h>

#include "Schedule.h"
#include "Proto.h"

/*
	external variables
*/

extern TextChar	defaultSchedName[], scheduleFile[];

extern UBYTE			daysPerMonth[];
extern ScrollListPtr	schedList;

/*
 	local variables and definitions
*/

static SchedulePtr	scheduleList;

#define MAX(a,b)	((a)>(b)?(a):(b))
#define MIN(a,b)	((a)<(b)?(a):(b))

#define DAYS_IN_YEAR(yr)	((yr % 4) ? 365 : 366)	// Valid till 2100
#define MINUTES_PER_DAY		1440

/*
 *	Local prototypes
 */

static BOOL	ReadData(File, Ptr, WORD);
static BOOL	ReadString(File, TextPtr);
static BOOL	WriteData(File, Ptr, WORD);
static BOOL WriteString(File, TextPtr);
static BOOL IsREXXFile(TextPtr);
static WORD CompareDate(DateStampPtr, DateStampPtr);

/*
 *	Read data from events file
 */

static BOOL ReadData(File file, Ptr data, WORD size)
{
	return (Read(file, data, size) == size);
}

/*
 *	Read string from events file
 */

static BOOL	ReadString(File file, TextPtr str)
{
	WORD	len;
	BOOL	success = FALSE;

	if (ReadData(file, &len, sizeof(WORD))) {
		if (len == 0 || ReadData(file, str, len)) {
			str[len] = '\0';
			success = TRUE;
		}
	}
	return (success);
}

/*
 *	Write data to events file
 */

static BOOL WriteData(File file, Ptr data, WORD size)
{
	return (Write(file, data, size) == size);
}

/*
 *	Write string to events file
 */

static BOOL WriteString(File file, TextPtr str)
{
	WORD	len;
	BOOL	success = FALSE;

	len = (str) ? strlen(str) : 0;
	if (WriteData(file, &len, sizeof(WORD))) {
		if (len == 0 || WriteData(file, str, len))
			success = TRUE;
	}
	return (success);
}

/*
 *	Read list of schedules
 */

BOOL InitScheduleList()
{
	BOOL		success, active;
	File		file;
	SchedulePtr	prevSched, sched;
	TextChar	buffer[256];

	prevSched = scheduleList = NULL;
	success = TRUE;
	if ((file = Open(scheduleFile, MODE_OLDFILE)) != NULL) {
		while (success && ReadData(file, &active, sizeof(BOOL))) {
			if ((sched = NewSchedule()) == NULL) {
				Error(ERR_NO_MEM, 0);
				success = FALSE;
			}
			if (success)
				success = ReadData(file, &sched->Type, sizeof(WORD));
			if (success) {
				sched->Active = active;
				success = ReadData(file, &sched->Year, sizeof(Year));
			}
			if (success)
				success = ReadData(file, &sched->NextDate, sizeof(struct DateStamp));
			if (success)
				success = ReadString(file, buffer);
			if (success)
				success = SetScheduleName(sched, buffer);
			if (success)
				success = ReadString(file, buffer);
			if (success)
				success = SetScheduleProgram(sched, buffer);
			if (success)
				success = ReadString(file, buffer);
			if (success)
				success = SetScheduleArgs(sched, buffer);
			if (success) {
				if (scheduleList)
					prevSched->Next = sched;
				else
					scheduleList = sched;
				prevSched = sched;
			}
		}
		Close(file);
	}
	return (success);
}

/*
 *	Save list of schedules
 */

void SaveScheduleList()
{
	BOOL		success;
	File		file;
	SchedulePtr	sched;

	success = FALSE;
	if ((file = Open(scheduleFile, MODE_NEWFILE)) != NULL) {
		success = TRUE;
		for (sched = scheduleList; sched && success; sched = sched->Next) {
			success = WriteData(file, &sched->Active, sizeof(BOOL));
			if (success)
				success = WriteData(file, &sched->Type, sizeof(WORD));
			if (success)
				success = WriteData(file, &sched->Year, sizeof(Year));
			if (success)
				success = WriteData(file, &sched->NextDate, sizeof(struct DateStamp));
			if (success)
				success = WriteString(file, sched->SchedName);
			if (success)
				success = WriteString(file, sched->Program);
			if (success)
				success = WriteString(file, sched->Args);
		}
		Close(file);
	}
	if (!success)
		Error(ERR_SAVE, 0);
}

/*
 *	Return TRUE if first two bytes of file are comment start
 *	File name must have complete path
 */

static BOOL IsREXXFile(TextPtr fileName)
{
	BOOL		isREXX;
	File		file;
	TextChar	text[3];

	isREXX = FALSE;
	if ((file = Open(fileName, MODE_OLDFILE)) != NULL) {
		if (Read(file, text, 2) == 2) {
			if (CmpString(text, "/*", 2, 2, TRUE) == 0)
				isREXX = TRUE;
		}
		Close(file);
	}
	return (isREXX);
}

/*
 *	check schedulelist for any schedule that needs to start
 */

void ExecuteSchedules()
{
	struct DateStamp	currentDate;
	SchedulePtr			sched;
	BOOL				success, isREXX, schedStart;
	TextChar			command[256], strBuff[256];
	WORD				num, index;
	File 				nilFile;

	DateStamp(&currentDate);
	num = 0;
	success = FALSE;
	schedStart = FALSE;

	for (sched = scheduleList; sched; sched = sched->Next) {
		if (sched->Active && CompareDate(&sched->NextDate, &currentDate) <= 0) {
			if (sched->Program == NULL)
				continue;
			if (sched->Type == TYPE_PROGRAM) {
/*
	Build command line
	When running from Workbench, Execute() doesn't inherit current dir,
		so we do a CD command first
*/
				isREXX = IsREXXFile(sched->Program);
				BlockClear(command, 256);
				strcpy(command, "CD \"");
				strcpy(strBuff, sched->Program);
				for (index = strlen(strBuff); index >= 0; index--) {
					if (strBuff[index] == '/' || strBuff[index] == ':')
						break;
				}
				strBuff[index + 1] = '\0';
				strcat(command, strBuff);
				strcat(command, "\"\n");
				strcat(command, "c:Run <NIL: >NIL: ");
				if (isREXX)
					strcat(command, "SYS:RexxC/RX ");
				else
					strcat(command, "\"");			// Enclose in quotes
				strcat(command, sched->Program + index + 1);
				if (!isREXX)
					strcat(command, "\"");
				if (sched->Args) {
					strcat(command, " ");
					strcat(command, sched->Args);
				}
#ifdef DEBUG
				printf("Executing command: %s\n", command);
#endif
/*
	Execute command
*/
				success = FALSE;
				if ((nilFile = Open("NIL:", MODE_NEWFILE)) != NULL) {
					success = Execute(command, NULL, nilFile);
					Close(nilFile);
				}
			}
			else
				success = DoReminderDialog(sched->Program);
			CalculateNextDate(sched);
			if (!sched->Active && schedList) {
				SLDoDraw(schedList, FALSE);
				SLSetItemStyle(schedList, num, FSF_ITALIC);
				SLDoDraw(schedList, TRUE);
			}
			schedStart = TRUE;
			if (!success)
				Error(ERR_CANT_RUN, 0);
		}
		num++;
	}
	if (schedStart)
		SaveScheduleList();
}

/*
 *	allocate memory and set name to next default
 */

SchedulePtr	NewSchedule()
{
	SchedulePtr			newSched;
	WORD				numScheds;
	TextChar			numBuff[5];
	struct DateStamp	dateStamp;
	register WORD 		day, month, daysInYear;

	if ((newSched = MemAlloc(sizeof(Schedule), MEMF_CLEAR)) == NULL)
		return (NULL);

	numScheds = NumSchedules();
	NumToString(numScheds + 1, numBuff);

	if ((newSched->SchedName = MemAlloc(strlen(defaultSchedName) + strlen(numBuff) + 1, MEMF_CLEAR)) == NULL) {
		DisposeSchedule(newSched);
		return (NULL);
	}
	strcpy(newSched->SchedName, defaultSchedName);
	strcat(newSched->SchedName, numBuff);
	DateStamp(&dateStamp);

	day = dateStamp.ds_Days;
	newSched->Year.Year = 1978;
	while (day >= (daysInYear = DAYS_IN_YEAR(newSched->Year.Year))) {
		day -= daysInYear;
		newSched->Year.Year++;
	}
	daysPerMonth[1] = (newSched->Year.Year % 4) ? 28 : 29;		// Valid till 2100
	for (month = 0; month < 12; month++) {
		if (day < daysPerMonth[month])
			break;
		day -= daysPerMonth[month];
	}
	daysPerMonth[1] = 28;			// reset array
	newSched->Year.Time = ((dateStamp.ds_Minute / 60) + 1) * 60;
	if (newSched->Year.Time >= MINUTES_PER_DAY) {
		newSched->Year.Time = 0;
		day++;
		if (day >= daysPerMonth[month]) {
			month++;
			day = 0;
		}
		if (month >= NUM_MONTHS) {
			month = 0;
			newSched->Year.Year++;
		}
	}
	newSched->Year.Month[month] = 1;
	newSched->Year.Day[day] = 1;
	newSched->Active = TRUE;
	return (newSched);
}

/*
 *	Dispose of memory used by schedule
 */

void DisposeSchedule(SchedulePtr sched)
{
	if (sched) {
		if (sched == scheduleList)
			scheduleList = sched->Next;

		if (sched->SchedName)
			MemFree(sched->SchedName, strlen(sched->SchedName) + 1);
		if (sched->Program)
			MemFree(sched->Program, strlen(sched->Program) + 1);
		if (sched->Args)
			MemFree(sched->Args, strlen(sched->Args) + 1);
		MemFree(sched, sizeof(Schedule));
	}
}

/*
 *	dispose of memory used by all schedules
 */

void DisposeScheduleList()
{
	SchedulePtr	sched, nextSched;

	sched = scheduleList;
	while (sched) {
		nextSched = sched->Next;
		DisposeSchedule(sched);
		sched = nextSched;
	}
	scheduleList = NULL;
}

/*
 *	return number of schedules in schedule list
 */

WORD NumSchedules()
{
	WORD 		num = 0;
	SchedulePtr	sched;

	sched = scheduleList;
	while (sched) {
		num++;
		sched = sched->Next;
	}
	return (num);
}

/*
 *	return TRUE if there are no schedules
 */

void SetFirstSchedule(SchedulePtr schedule)
{
	scheduleList = schedule;
}

/*
 *	return pointer to first schedule in list
 */

SchedulePtr	FirstSchedule()
{
	return (scheduleList);
}

/*
 *	return pointer to last schedule in list
 */

SchedulePtr	LastSchedule()
{
	SchedulePtr	sched;

	for (sched = scheduleList; sched->Next; sched = sched->Next);

	return (sched);
}

/*
 *	return pointer to schedule number in list; number range 0..NumSchedules() - 1
 */

SchedulePtr GetSchedule(WORD number)
{
	SchedulePtr	sched;

	sched = scheduleList;
	while (sched && number) {
		sched = sched->Next;
		number--;
	}
	return (sched);
}

/*
 *  Set schedule name
 *	Return FALSE if couldn't allocate name
 */

BOOL SetScheduleName(SchedulePtr schedule, TextPtr name)
{
	if (schedule->SchedName) {
		MemFree(schedule->SchedName, strlen(schedule->SchedName) + 1);
		schedule->SchedName = NULL;
	}
	if (name && strlen(name) != 0) {
		if ((schedule->SchedName = MemAlloc(strlen(name) + 1, MEMF_CLEAR)) == NULL)
			return (FALSE);
		name[0] = toUpper[name[0]];
		strcpy(schedule->SchedName, name);
	}
	return (TRUE);
}

/*
 *  Set schedule program
 *	Return FALSE if couldn't allocate string
 */

BOOL SetScheduleProgram(SchedulePtr schedule, TextPtr program)
{
	if (schedule->Program) {
		MemFree(schedule->Program, strlen(schedule->Program) + 1);
		schedule->Program = NULL;
	}
	if (program && strlen(program) != 0) {
		if ((schedule->Program = MemAlloc(strlen(program) + 1, MEMF_CLEAR)) == NULL)
			return (FALSE);
		strcpy(schedule->Program, program);
	}
	return (TRUE);
}

/*
 *  Set schedule arguments
 *	Return FALSE if couldn't allocate string
 */

BOOL SetScheduleArgs(SchedulePtr schedule, TextPtr args)
{
	if (schedule->Args) {
		MemFree(schedule->Args, strlen(schedule->Args) + 1);
		schedule->Args = NULL;
	}
	if (args && strlen(args) != 0) {
		if ((schedule->Args = MemAlloc(strlen(args) + 1, MEMF_CLEAR)) == NULL)
			return (FALSE);
		strcpy(schedule->Args, args);
	}
	return (TRUE);
}

/*
 *	return TRUE if any place is set
 */

BOOL AnyPlaceSet(SchedulePtr sched)
{
	WORD	i;

	for (i = 0; i < NUM_PLACES; i++)
		if (sched->Year.Place[i])
			return (TRUE);

	return (FALSE);
}

/*
 *	return TRUE if any weekday is set
 */

BOOL AnyWeekDaySet(SchedulePtr sched)
{
	WORD 	i;

	for (i = 0; i < NUM_WEEKDAYS; i++)
		if (sched->Year.WeekDay[i])
			return (TRUE);

	return (FALSE);
}

/*
 *	return TRUE if any weekday or place is set
 */

BOOL AbstractSettings(SchedulePtr sched)
{
	return (AnyWeekDaySet(sched) || AnyPlaceSet(sched));
}

/*
 *	return TRUE if schedule is valid
 */

WORD ValidSchedule(SchedulePtr sched)
{
	register WORD	month, day;
	Year			year = sched->Year;

	if (sched->Year.Time < 0 || sched->Year.Time > MINUTES_PER_DAY - 1) {
		sched->Year.Time = 0;
		return (SCHED_BAD_TIME);
	}

	for (month = 0; month < NUM_MONTHS && !year.Month[month]; month++);
	if (month == NUM_MONTHS && !year.EveryMonth)
		return (SCHED_BAD_MONTH);

	for (day = 0; day < NUM_DAYS && !year.Day[day]; day++);
	if (day == NUM_DAYS && !year.EveryDay)
		return (SCHED_BAD_DAY);


	if (year.Year < MIN_YEAR || year.Year > MAX_YEAR)
		return (SCHED_BAD_YEAR);

	return (SCHED_OK);
}

/*
 *	traverse array till maxVal or byte is set
 *	return TRUE if later value found, else set to firstVal
 */

static BOOL GetNext(WORD *value, WORD maxVal, UBYTE array[], WORD firstVal, BOOL all)
{
	WORD	newVal;
	BOOL	new = FALSE;

	newVal = *value + 1;
	if (!all)
		for (; newVal < maxVal && !array[newVal]; newVal++);
	if (newVal < maxVal)
		new = TRUE;

	*value = (!new) ? firstVal : newVal;

	return (new);
}

/*
 *	compare two datestamps: return -1 if first date earlier than second date;
 *	1 if first date is later than second date; or 0 if the dates are the same
 */

static WORD CompareDate(DateStampPtr dateA, DateStampPtr dateB)
{
	if (dateA->ds_Days < dateB->ds_Days)
		return (-1);
	if (dateA->ds_Days > dateB->ds_Days)
		return (1);

	if (dateA->ds_Minute < dateB->ds_Minute)
		return (-1);
	if (dateA->ds_Minute > dateB->ds_Minute)
		return (1);

	if (dateA->ds_Tick < dateB->ds_Tick)
		return (-1);
	if (dateA->ds_Tick > dateB->ds_Tick)
		return (1);

	return (0);
}

/*
 *	create datestamp of date given day, month, and year or weekday and place
 */

static void GetDateStamp(DateStampPtr date, BOOL relative, BOOL abstract,
						 WORD day, WORD month, WORD year, WORD weekday, WORD place)
{
	WORD	convertDay, lastDay, firstDay, endDay;

	lastDay = (month == 1 && (year % 4) == 0) ? 29 : daysPerMonth[month];

	convertDay = (relative && !abstract) ? MAX(1, lastDay - day) : day + 1;		// same for abstract
	ConvertToDateStamp(date, convertDay, month, year);
	if (abstract) {
		if (place == NUM_PLACES - 1)	{		// last place go from end
			convertDay = lastDay - 1;
			endDay = (date->ds_Days + lastDay - 1) % 7;		// weekday of the last day
			for (; endDay != weekday; endDay--) {
				convertDay--;
				if (endDay == 0)
					endDay = NUM_WEEKDAYS;
			}
		}
		else {									// else go from beginning
			firstDay = date->ds_Days % 7;			// weekday of the first
			convertDay--;
			for (; firstDay != weekday; firstDay++) {
				convertDay++;
				if (firstDay == NUM_WEEKDAYS - 1)
					firstDay = -1;
			}
			convertDay += (place * NUM_WEEKDAYS);
		}
		ConvertToDateStamp(date, convertDay + 1, month, year);
	}
}

/*
 *	calculate next time schedule should be activated
 */

void CalculateNextDate(SchedulePtr schedule)
{
	struct DateStamp	currentDate, nextDate;
	Year				year = schedule->Year;
	WORD				month, day, yearNum, lastDay, firstDay, firstMonth;
	WORD				weekday, place, firstWeekday, firstPlace;
	BOOL				newDate, relative, abstract, everyWeek, everyWeekday;
	ULONG				time;
#ifdef DEBUG
	TextChar			date[100];
#endif

	if (!schedule->Active)
		return;

	relative = year.RelativeTime && !year.EveryDay;
	abstract = AbstractSettings(schedule);
	everyWeek = (abstract && !AnyPlaceSet(schedule));
	everyWeekday = (abstract && !AnyWeekDaySet(schedule));

	DateStamp(&currentDate);

	month = 0;
	if (!year.EveryMonth)
		for (; month < NUM_MONTHS && !year.Month[month]; month++);
	firstMonth = month;

	day = 0;
	if (!abstract) {
		if (!year.EveryDay)
			for (; day < NUM_DAYS && !year.Day[day]; day++);
		firstDay = day;
	}
	else {
		weekday = firstWeekday = 0;
		if (!everyWeekday) {
			for (; weekday < NUM_WEEKDAYS && !year.WeekDay[weekday]; weekday++);
			firstWeekday = weekday;
		}
		place = firstPlace = 0;
		if (!everyWeek) {
			for (; place < NUM_PLACES && !year.Place[place]; place++);
			firstPlace = place;
		}
	}

	yearNum = year.Year;
	lastDay = (month == 1 && (yearNum % 4) == 0) ? 29 : daysPerMonth[month];
	GetDateStamp(&nextDate, relative, abstract, day, month, yearNum, weekday, place);
	time = nextDate.ds_Minute = year.Time;

	newDate = TRUE;
	while (newDate && CompareDate(&nextDate, &currentDate) < 0) {	// nextDate is earlier than currentDate
		newDate = FALSE;
		if (year.EveryHour) {					// check next time
			if (time < MINUTES_PER_DAY - 61)	{ // still time left
				time += 60;
				newDate = TRUE;
			}
			else
				time = year.Time;
		}
		if (!newDate) {							// time didn't change
			if (!abstract)
				newDate = GetNext(&day, lastDay, year.Day, firstDay, year.EveryDay);
			else {
				newDate = GetNext(&weekday, NUM_WEEKDAYS, year.WeekDay, firstWeekday, everyWeekday);
				if (!newDate)
					newDate = GetNext(&place, NUM_PLACES, year.Place, firstPlace, everyWeek);
			}
		}
		if (!newDate) {							// time or day didn't change
			newDate = GetNext(&month, NUM_MONTHS, year.Month, firstMonth, year.EveryMonth);
			lastDay = (month == 1 && (year.Year % 4) == 0) ? 29 : daysPerMonth[month];
		}
		if (!newDate && year.Annual) {			// time/day/month didn't change
			yearNum++;
			newDate = TRUE;
		}
		if (newDate) {
			GetDateStamp(&nextDate, relative, abstract, day, month, yearNum, weekday, place);
			nextDate.ds_Minute = time;
		}
	}

	if (!newDate)
		schedule->Active = FALSE;
	schedule->NextDate = nextDate;
/*
	check date
*/
#ifdef DEBUG
	DateString(&nextDate, DATE_SHORT, date);
	printf("next activate is %s ", date);
	TimeString(&nextDate, TRUE, FALSE, date);
	printf("%s\n", date);
#endif
}
