/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Schedule Pro
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Main procedure
 */

#include <workbench/workbench.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/DOS.h>

#include "Schedule.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr 		window;
extern DialogPtr		editDialog;
extern MsgPortPtr		mainMsgPort;
extern MsgPortPtr		appIconMsgPort;
extern MsgPortPtr		timerMsgPort;

extern BOOL 			titleChanged;

extern ScreenPtr		_tbScreen;

extern TextChar			strSchedProg[];

extern ScrollListPtr	schedList;

extern UBYTE			daysPerMonth[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

static BOOL 	quitFlag;

#define RAWKEY_HELP	0x5F

#define SELECTBUTTON(q)	\
	((q&IEQUALIFIER_LEFTBUTTON)||((q&AMIGAKEYS)&&(q&IEQUALIFIER_LALT)))

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)

static WORD		currentField;	// last active field of time
static WORD		currentMonth;	// currentMonth being displayed

static WORD		delay;

/*
 *	Prototypes
 */

static BOOL MainDialogFilter(IntuiMsgPtr, WORD *);
static void DoMainDialogItem(WORD);
static void DeleteEvent(WORD);
static BOOL	EditDialogFilter(IntuiMsgPtr, WORD *);
static void EnableButtons(void);
static void NumToString1(WORD, TextPtr);
static BOOL DoEditDialog(SchedulePtr, BOOL);
static void DoEditDialogItem(SchedulePtr, WORD);
static WORD DoDayItem(SchedulePtr);
static WORD DoUnitItem(SchedulePtr, WORD, WORD, WORD);

/*
 *	Main routine
 */

void main(int argc, char *argv[])
{
	register IntuiMsgPtr	intuiMsg;
	MsgPtr					timerMsg;
	struct AppMessage 		*appMsg;
	WORD					itemHit;
	LONG					waitSigs;

	Init(argc, argv);
	if (window)
		SetButtons();
	ExecuteSchedules();
	SetTimer();
/*
	Wait for messages
*/
	waitSigs = (1 << mainMsgPort->mp_SigBit);
	if (appIconMsgPort)
		waitSigs |= (1 << appIconMsgPort->mp_SigBit);
	if (timerMsgPort)
		waitSigs |= (1 << timerMsgPort->mp_SigBit);

	quitFlag = FALSE;
	while (!quitFlag) {
		while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) {
			if (window) {
				if (MainDialogFilter(intuiMsg, &itemHit))
					DoMainDialogItem(itemHit);
				else if (DialogSelect(intuiMsg, &window, &itemHit)) {
					ReplyMsg((MsgPtr) intuiMsg);
					DoMainDialogItem(itemHit);
				}
				else
					ReplyMsg((MsgPtr) intuiMsg);
			}
			else
				ReplyMsg((MsgPtr) intuiMsg);
		}
/*
	Handle AppIcon messages
*/
		if (appIconMsgPort) {
			while (appMsg = (struct AppMessage *) GetMsg(appIconMsgPort)) {
				if (appMsg->am_Type == AMTYPE_APPICON || appMsg->am_Type == AMTYPE_APPMENUITEM) {
					if (window) {
						WindowToFront(window);
						ActivateWindow(window);
					}
					else
						CreateWindow();
				}
				ReplyMsg((MsgPtr) appMsg);
			}
		}
		if (timerMsgPort) {
			while (timerMsg = GetMsg(timerMsgPort)) {
				if (IsSchedulerMsg(timerMsg)) {
					SetTimer();
					ExecuteSchedules();
				}
			}
		}
		if (!quitFlag)
			Wait(waitSigs);
	}
/*
	All done
*/
	ShutDown();
}

/*
 *	Change wipe value in options requester
 */

static void ChangeValue(WORD gadgNum, LONG *value, WORD inc, WORD min, WORD max)
{
	GadgetPtr	gadgList = editDialog->FirstGadget;
	TextChar	strBuff[5];

	*value += inc;
	if (gadgNum == YEAR_TEXT) {
		if (*value >= max)
			*value = max;
		if (*value <= min)
			*value = min;
	}
	else {
		if (*value >= max)
			*value -= max;
		if (*value < min)
			*value = max + *value;		// value is neg
	}
	NumToString1(*value, strBuff);
	SetEditItemText(gadgList, gadgNum, editDialog, NULL, strBuff);
/*
	Do enable/disable after displaying new value, since these will be blocked
		if gadgets are selected
*/
	if (gadgNum == YEAR_TEXT) {
		EnableGadgetItem(gadgList, YEAR_UP, editDialog, NULL, *value < max);
		EnableGadgetItem(gadgList, YEAR_DOWN, editDialog, NULL, *value > min);
	}
}

/*
 *	Handle raw intuiMsgs's in main window
 */

static BOOL MainDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register ULONG 	class = intuiMsg->Class;
	register UWORD 	code = intuiMsg->Code;
	register APTR  	iAddress = intuiMsg->IAddress;
	WORD			gadgNum;
	register BOOL	msgReply = FALSE;

	switch (class) {
	case GADGETDOWN:
	case GADGETUP:
		gadgNum = GadgetNumber(iAddress);
		switch (gadgNum) {
			case SCHED_BOX :
			case SCHED_UP :
			case SCHED_DOWN :
			case SCHED_SCROLL :
				SLGadgetMessage(schedList, mainMsgPort, intuiMsg);
				SetButtons();
				msgReply = TRUE;
				*item = gadgNum;
				break;
		}
		break;
	case RAWKEY:
		if (code == CURSORUP || code == CURSORDOWN) {
			SLCursorKey(schedList, code);
			SetButtons();
			msgReply = TRUE;
			*item = -1;
			break;
		}
		else if (code == RAWKEY_HELP) {
			ReplyMsg((MsgPtr) intuiMsg);
			msgReply = TRUE;
			DoHelp();
		}
		break;
	case REFRESHWINDOW:
		ReplyMsg((MsgPtr) intuiMsg);
		msgReply = TRUE;
		*item = -1;
		RefreshWindow();
		break;
	}
	return (msgReply);
}

/*
 *	handle item from main dialog
 */

static void DoMainDialogItem(WORD item)
{
	SchedulePtr	sched;
	WORD		num, style;
	BOOL		oldActive, newName;
	TextChar	oldName[100];

	switch (item) {
		case DLG_CLOSE_BOX:
			RemoveWindow();
			break;
		case SCHED_BOX:
			if (!SLIsDoubleClick(schedList))
				break;
		case CHANGE_BUTTON:
			num = SLNextSelect(schedList, -1);
			if (num == -1) {
				SetButtons();
				break;
			}
			sched = GetSchedule(num);
			strcpy(oldName, sched->SchedName);
			oldActive = sched->Active;
			(void) DoEditDialog(sched, FALSE);
			newName = (CmpString(oldName, sched->SchedName, strlen(oldName), strlen(sched->SchedName), TRUE) != 0);
			if (newName || sched->Active != oldActive) {
				SLDoDraw(schedList, FALSE);
				if (newName) {
					SLRemoveItem(schedList, num);
					SLAddItem(schedList, sched->SchedName, strlen(sched->SchedName), num);
				}
				if (sched->Active != oldActive) {
					style = (sched->Active) ? FS_NORMAL : FSF_ITALIC;
					SLSetItemStyle(schedList, num, style);
				}
				SLDoDraw(schedList, TRUE);
			}
			break;
		case NEW_BUTTON:
			if ((sched = NewSchedule()) == NULL) {
				Error(ERR_NO_MEM, 0);
				break;
			}
			if (DoEditDialog(sched, TRUE)) {
				SLAddItem(schedList, sched->SchedName, strlen(sched->SchedName), SLNumItems(schedList));
				if (!sched->Active)
					SLSetItemStyle(schedList, SLNumItems(schedList) - 1, FSF_ITALIC);
				SetButtons();
			}
			else
				DisposeSchedule(sched);
			break;
		case DELETE_BUTTON:
			num = SLNextSelect(schedList, -1);
			if (num == -1) {
				SetButtons();
				break;
			}
			DeleteEvent(num);
			break;
		case QUIT_BUTTON:
			quitFlag = TRUE;
			break;
	}
}

/*
 *	Delete event entry
 */

static void DeleteEvent(WORD eventNum)
{
	WORD		item;
	SchedulePtr	prevSched, sched;

/*
	Confirm delete
*/
	SetDefaultButtons(OK_BUTTON, -1);
	item = StdAlert(dlgList[DLG_DELETE], NULL, mainMsgPort, DialogFilter);
	SetButtons();
	if (item != OK_BUTTON)
		return;
/*
	Delete event
*/
	sched = GetSchedule(eventNum);
	prevSched = (eventNum == 0) ? NULL : GetSchedule(eventNum - 1);
	if (prevSched)
		prevSched->Next = sched->Next;
	else
		SetFirstSchedule(sched->Next);
	DisposeSchedule(sched);
	SLRemoveItem(schedList, eventNum);
	SetButtons();
	SaveScheduleList();
}

/*
 *	Handle raw intuiMsgs's in main window
 */

static BOOL EditDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register ULONG 	class = intuiMsg->Class;
	register UWORD	code = intuiMsg->Code;
	register APTR  	iAddress = intuiMsg->IAddress;
	register WORD	modifier = intuiMsg->Qualifier;
	WORD			gadgNum;

	switch (class) {
	case INTUITICKS:
		EnableButtons();
		break;
	case GADGETDOWN:
	case GADGETUP:
		gadgNum = GadgetNumber(iAddress);
		switch (gadgNum) {
		case HOUR_TEXT:
		case MINUTE_TEXT:
			if (class == GADGETDOWN)
				currentField = gadgNum;
			break;
		case MONTH_RECT:
		case DAY_RECT:
		case PLACE_RECT:
		case WEEKDAY_RECT:
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? gadgNum : -1;
			return (TRUE);
		case YEAR_UP:
		case YEAR_DOWN:
		case TIME_UP:
		case TIME_DOWN:
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? gadgNum : -1;
			return (TRUE);
		}
		break;
	case RAWKEY:
		if (code == CURSORUP || code == CURSORDOWN) {
			ReplyMsg((MsgPtr) intuiMsg);
			if (modifier & SHIFTKEYS)
				*item = (code == CURSORUP) ? TIME_UP : TIME_DOWN;
			else
				*item = (code == CURSORUP) ? YEAR_UP : YEAR_DOWN;
			if (!DepressGadget(GadgetItem(editDialog->FirstGadget, *item), editDialog, NULL))
				*item = -1;
			return (TRUE);
		}
		break;
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	enable buttons according to current schedule
 */

static void EnableButtons()
{
	GadgetPtr	gadgList = editDialog->FirstGadget;
	TextChar	name[100], prog[100];
	ULONG		year;
	WORD		scheduleType;

	GetEditItemText(gadgList, PROGRAM_TEXT, prog);
	GetEditItemText(gadgList, NAME_TEXT, name);
	EnableGadgetItem(gadgList, SAVE_BUTTON, editDialog, NULL, prog[0] != NULL && name[0] != NULL);

	GetEditItemText(gadgList, YEAR_TEXT, name);
	year = StringToNum(name);
	EnableGadgetItem(gadgList, YEAR_UP, editDialog, NULL, year < MAX_YEAR);
	EnableGadgetItem(gadgList, YEAR_DOWN, editDialog, NULL, year > MIN_YEAR);

	scheduleType = GetGadgetValue(GadgetItem(gadgList, TYPE_POPUP));
	EnableGadgetItem(gadgList, ARGS_TEXT, editDialog, NULL, scheduleType == TYPE_PROGRAM);
	EnableGadgetItem(gadgList, BROWSE_BUTTON, editDialog, NULL, scheduleType == TYPE_PROGRAM);
}

/*
 *	create string of exactly 2 characters of number
 */

static void NumToString1(WORD num, TextPtr text)
{
	TextChar	buff[3];

	NumToString(num, text);
	if (num < 10) {
		strcpy(buff, "0");
		strcat(buff, text);
		strcpy(text, buff);
	}
}

/*
 *	do edit schedule
 */

static BOOL DoEditDialog(SchedulePtr schedule, BOOL new)
{
	BOOL		done;
	WORD		item, hour, minute, error;
	Schedule	sched = *schedule;
	SchedulePtr	prevSched;
	TextChar	buff[100];
	GadgetPtr	gadgList;

	currentField = HOUR_TEXT;
	if (schedule->Year.EveryMonth)
		currentMonth = 0;
	else
		for (currentMonth = 0;
				currentMonth < NUM_MONTHS && !schedule->Year.Month[currentMonth];
				currentMonth++);

	if (!OpenEditWindow(&sched))
		return (FALSE);

	SetDefaultButtons(OK_BUTTON, CANCEL_BUTTON);	// May have been changed by SetButtons()
	gadgList = editDialog->FirstGadget;

	SetEditItemText(gadgList, NAME_TEXT, editDialog, NULL, sched.SchedName);
	SetEditItemText(gadgList, PROGRAM_TEXT, editDialog, NULL, sched.Program);
	SetEditItemText(gadgList, ARGS_TEXT, editDialog, NULL, sched.Args);

	NumToString(sched.Year.Year, buff);
	SetEditItemText(gadgList, YEAR_TEXT, editDialog, NULL, buff);

	hour = sched.Year.Time/60;
	NumToString1(hour, buff);
	SetEditItemText(gadgList, HOUR_TEXT, editDialog, NULL, buff);
	minute = sched.Year.Time - hour * 60;
	NumToString1(minute, buff);
	SetEditItemText(gadgList, MINUTE_TEXT, editDialog, NULL, buff);

	SetGadgetItemValue(gadgList, ACTIVE_RADBTN, editDialog, NULL, sched.Active);
	SetGadgetItemValue(gadgList, INACTIVE_RADBTN, editDialog, NULL, !sched.Active);

	SetGadgetItemValue(gadgList, TYPE_POPUP, editDialog, NULL, sched.Type);

	SetGadgetItemValue(gadgList, ANNUAL_BOX, editDialog, NULL, sched.Year.Annual);
	SetGadgetItemValue(gadgList, RELATIVE_BOX, editDialog, NULL, sched.Year.RelativeTime);
	SetGadgetItemValue(gadgList, DAILY_BOX, editDialog, NULL, sched.Year.EveryDay);
	SetGadgetItemValue(gadgList, MONTHLY_BOX, editDialog, NULL, sched.Year.EveryMonth);
	SetGadgetItemValue(gadgList, HOURLY_BOX, editDialog, NULL, sched.Year.EveryHour);

	EnableButtons();

	delay = 0;
	done = FALSE;
	while (!done) {
		WaitPort(mainMsgPort);
		item = CheckDialog(mainMsgPort, editDialog, EditDialogFilter);
		switch (item) {
		case SAVE_BUTTON:
			GetEditItemText(gadgList, NAME_TEXT, buff);
			SetScheduleName(&sched, buff);
			GetEditItemText(gadgList, PROGRAM_TEXT, buff);
			SetScheduleProgram(&sched, buff);
			GetEditItemText(gadgList, ARGS_TEXT, buff);
			SetScheduleArgs(&sched, buff);
			GetEditItemText(gadgList, YEAR_TEXT, buff);
			sched.Year.Year = StringToNum(buff);
			GetEditItemText(gadgList, HOUR_TEXT, buff);
			hour = StringToNum(buff);
			GetEditItemText(gadgList, MINUTE_TEXT, buff);
			minute = StringToNum(buff);
			if (hour < 0 || hour > NUM_HOURS - 1) {
				Error(ERR_BAD_SCHED, SCHED_BAD_TIME);
				hour = (hour < 0) ? 0 : NUM_HOURS - 1;
				NumToString1(hour, buff);
				SetEditItemText(gadgList, HOUR_TEXT, editDialog, NULL, buff);
				break;
			}
			if (minute < 0 || minute > NUM_MINUTES - 1) {
				Error(ERR_BAD_SCHED, SCHED_BAD_TIME);
				minute = (minute < 0) ? 0 : NUM_MINUTES - 1;
				NumToString1(minute, buff);
				SetEditItemText(gadgList, MINUTE_TEXT, editDialog, NULL, buff);
				break;
			}
			sched.Year.Time = hour * 60 + minute;
			sched.Type = GetGadgetValue(GadgetItem(gadgList, TYPE_POPUP));
			if ((error = ValidSchedule(&sched)) != SCHED_OK) {
				Error(ERR_BAD_SCHED, error);
				break;
			}
			done = TRUE;
			break;
		case CANCEL_BUTTON:
			done = TRUE;
		default:
			DoEditDialogItem(&sched, item);
			break;
		}
	}

	CloseEditWindow();

	if (item == SAVE_BUTTON) {
		*schedule = sched;
		if (new) {
			if (FirstSchedule() == NULL)
				SetFirstSchedule(schedule);
			else {
				prevSched = LastSchedule();
				prevSched->Next = schedule;
			}
		}
		CalculateNextDate(schedule);
		SaveScheduleList();
	}
	SetButtons();					// Restore old default buttons
	return (item == SAVE_BUTTON);
}

/*
 *	Handle gadget click in main editDialog
 */

static void DoEditDialogItem(SchedulePtr sched, WORD item)
{
	GadgetPtr		gadgList = editDialog->FirstGadget;
	SFReply			sfReply;
	TextChar		buff[256];
	WORD			unitNum, firstMonth, inc;
	LONG			value;
	YearPtr			year = &sched->Year;

	switch (item) {
	case -1:
		if (delay > 0)
			delay--;
		else {
			if (delay > -9) {
				delay--;
				inc = 1;
			}
			else
				inc = 5;
			if (GadgetSelected(gadgList, YEAR_UP))
				ChangeValue(YEAR_TEXT, &year->Year, inc, MIN_YEAR, MAX_YEAR);
			else if (GadgetSelected(gadgList, YEAR_DOWN))
				ChangeValue(YEAR_TEXT, &year->Year, (WORD) -inc, MIN_YEAR, MAX_YEAR);
			else if (GadgetSelected(gadgList, TIME_UP)) {
				GetEditItemText(gadgList, currentField, buff);
				value = StringToNum(buff);
				ChangeValue(currentField, &value, inc, 0,
					(currentField == HOUR_TEXT) ? NUM_HOURS : NUM_MINUTES);
			}
			else if (GadgetSelected(gadgList, TIME_DOWN)) {
				GetEditItemText(gadgList, currentField, buff);
				value = StringToNum(buff);
				ChangeValue(currentField, &value, (WORD) -inc,  0,
					(currentField == HOUR_TEXT) ? NUM_HOURS : NUM_MINUTES);
			}
		}
		break;
	case BROWSE_BUTTON:
		SFPGetFile(_tbScreen, mainMsgPort, strSchedProg, DialogFilter, NULL,
						NULL, 0, NULL, NULL, &sfReply);
		if (sfReply.Result == SFP_OK) {
			NameFromLock1(sfReply.DirLock, buff, 256);
			if (buff[strlen(buff) - 1] != ':')
				strcat(buff, "/");
			strcat(buff, sfReply.Name);
			SetScheduleProgram(sched, buff);
			SetEditItemText(gadgList, PROGRAM_TEXT, editDialog, NULL,
								buff);
			EnableButtons();
		}
		break;
	case YEAR_UP:
	case YEAR_DOWN:
		GetEditItemText(gadgList, YEAR_TEXT, buff);
		year->Year = StringToNum(buff);
		ChangeValue(YEAR_TEXT, &year->Year, (item == YEAR_UP) ? +1 : -1, MIN_YEAR, MAX_YEAR);
		EnableButtons();
		delay = 5;
		break;
	case TIME_UP:
	case TIME_DOWN:
		GetEditItemText(gadgList, currentField, buff);
		value = StringToNum(buff);
		ChangeValue(currentField, &value, (item == TIME_UP) ? +1 : -1,  0,
							(currentField == HOUR_TEXT) ? NUM_HOURS : NUM_MINUTES);
		delay = 5;
		break;
	case ACTIVE_RADBTN:
	case INACTIVE_RADBTN:
		sched->Active = (item == ACTIVE_RADBTN);
		SetGadgetItemValue(gadgList, ACTIVE_RADBTN, editDialog, NULL, sched->Active);
		SetGadgetItemValue(gadgList, INACTIVE_RADBTN, editDialog, NULL, !sched->Active);
		break;
	case MONTH_RECT:
		if ((unitNum = DoUnitItem(sched, MONTH_RECT, MONTH_NUMACROSS, MONTH_NUMDOWN)) != -1) {
			for (firstMonth = 0; firstMonth < NUM_MONTHS && !year->Month[firstMonth]; firstMonth++);
			year->Month[unitNum] = (!year->EveryMonth && year->Month[unitNum]) ? 0 : 1;
			if (year->EveryMonth) {
				year->EveryMonth = FALSE;
				SetGadgetItemValue(gadgList, MONTHLY_BOX, editDialog, NULL, year->EveryMonth);
				if (unitNum != 0)
					currentMonth = DrawDayGadget(sched);
				DrawMonthGadget(sched);
				break;
			}
			if (unitNum < currentMonth ||
				(unitNum == currentMonth && !year->Month[unitNum]) ||
				firstMonth == NUM_MONTHS)
					currentMonth = DrawDayGadget(sched);
		}
		break;
	case DAY_RECT:
		if ((unitNum = DoDayItem(sched)) != -1) {
			year->Day[unitNum] = (!year->EveryDay &&
											year->Day[unitNum]) ? 0 : 1;
			if (year->EveryDay) {
				year->EveryDay = FALSE;
				currentMonth = DrawDayGadget(sched);
				SetGadgetItemValue(gadgList, DAILY_BOX, editDialog, NULL, year->EveryDay);
			}
		}
		break;
	case PLACE_RECT:
		if ((unitNum = DoUnitItem(sched, PLACE_RECT, PLACE_NUMACROSS, PLACE_NUMDOWN)) != -1) {
			year->Place[unitNum] = (year->Place[unitNum]) ? 0 : 1;
			(void) DrawDayGadget(sched);
			EnableGadgetItem(gadgList, RELATIVE_BOX, editDialog, NULL, !AbstractSettings(sched));
			EnableGadgetItem(gadgList, DAILY_BOX, editDialog, NULL, !AbstractSettings(sched));
		}
		break;
	case WEEKDAY_RECT:
		if ((unitNum = DoUnitItem(sched, WEEKDAY_RECT, WEEKDAY_NUMACROSS, WEEKDAY_NUMDOWN)) != -1) {
			year->WeekDay[unitNum] = (year->WeekDay[unitNum]) ? 0 : 1;
			(void)DrawDayGadget(sched);
			EnableGadgetItem(gadgList, RELATIVE_BOX, editDialog, NULL, !AbstractSettings(sched));
			EnableGadgetItem(gadgList, DAILY_BOX, editDialog, NULL, !AbstractSettings(sched));
		}
		break;
	case ANNUAL_BOX:
		year->Annual = !year->Annual;
		SetGadgetItemValue(gadgList, ANNUAL_BOX, editDialog, NULL, year->Annual);
		break;
	case RELATIVE_BOX:
		year->RelativeTime = !year->RelativeTime;
		SetGadgetItemValue(gadgList, RELATIVE_BOX, editDialog, NULL, year->RelativeTime);
		break;
	case DAILY_BOX:
		year->EveryDay = !year->EveryDay;
		SetGadgetItemValue(gadgList, DAILY_BOX, editDialog, NULL, year->EveryDay);
		EnableGadgetItem(gadgList, RELATIVE_BOX, editDialog, NULL, !year->EveryDay);
		currentMonth = DrawDayGadget(sched);
		break;
	case MONTHLY_BOX:
		year->EveryMonth = !year->EveryMonth;
		SetGadgetItemValue(gadgList, MONTHLY_BOX, editDialog, NULL, year->EveryMonth);
		DrawMonthGadget(sched);
		if (!sched->Year.Month[0])
			currentMonth = DrawDayGadget(sched);
		break;
	case HOURLY_BOX:
		year->EveryHour = !year->EveryHour;
		SetGadgetItemValue(gadgList, HOURLY_BOX, editDialog, NULL, year->EveryHour);
	}
}

static BOOL MyWaitMouseUp(void)
{
	register BOOL			mouseDown;
	register ULONG			class;
	register UWORD			code, modifier;
	register IntuiMsgPtr	intuiMsg;

	mouseDown = TRUE;		/* Assume mouse down (for INTUITICKS msgs) */
	(void) WaitPort(mainMsgPort);
	while (mouseDown && (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) != NULL) {
		class = intuiMsg->Class;
		code = intuiMsg->Code;
		modifier = intuiMsg->Qualifier;
		if (intuiMsg->IDCMPWindow == editDialog &&
			(class != MOUSEMOVE && class != MOUSEBUTTONS &&
			 class != RAWKEY && class != INTUITICKS)) {
			PutMsg(mainMsgPort, (MsgPtr) intuiMsg);
			return (FALSE);
		}
		ReplyMsg((MsgPtr) intuiMsg);
		if (class == MOUSEBUTTONS && code == SELECTUP)
			mouseDown = FALSE;
		else if (class != INTUITICKS ||
				 LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0)
			mouseDown = SELECTBUTTON(modifier);
		if (class == MOUSEMOVE)
			return (mouseDown);			/* Return immediately with MOUSEMOVE */
	}
	return (mouseDown);
}

/*
 *	select or deselect cell in day rectangle
 */

static WORD DoDayItem(SchedulePtr sched)
{
	WORD				num, xPos, yPos, cellWidth, cellHeight, row, column;
	WORD				first, weekday, endDay;
	BOOL				selected, select, startSel;
	Rectangle			rect;
	GadgetPtr			gadget;
	struct	DateStamp	dateStamp;

	if (AbstractSettings(sched))
		return (-1);
/*
	Find selected cell and highlight it
*/
	gadget = GadgetItem(editDialog->FirstGadget, DAY_RECT);
	xPos = editDialog->MouseX;
	yPos = editDialog->MouseY;
	cellWidth = gadget->Width/DAY_NUMACROSS;
	cellHeight = gadget->Height/DAY_NUMDOWN;
	column = (xPos - gadget->LeftEdge)/cellWidth;
	row = (yPos - gadget->TopEdge)/cellHeight;

	ConvertToDateStamp(&dateStamp, 1, currentMonth, sched->Year.Year);

	first = dateStamp.ds_Days % DAY_NUMACROSS - 1;
	endDay = (currentMonth == 1 && (sched->Year.Year % 4) == 0) ? 29 : daysPerMonth[currentMonth];
	num = row * DAY_NUMACROSS + column - first - 1;
	if (num < 0 || num > endDay - 1)
		return (-1);

	weekday = (dateStamp.ds_Days + num) % DAY_NUMACROSS;
	selected = sched->Year.Day[num];

	if ((yPos - gadget->TopEdge) < (DAY_NUMDOWN * cellHeight)) {
		rect.MinY = gadget->TopEdge + (((num + first + 1) / DAY_NUMACROSS) * gadget->Height)/ DAY_NUMDOWN;
		rect.MaxY = rect.MinY + gadget->Height/DAY_NUMDOWN - 1;
		rect.MinX = gadget->LeftEdge + (weekday * gadget->Width)/ DAY_NUMACROSS;
		rect.MaxX = rect.MinX + gadget->Width/DAY_NUMACROSS - 1;
		DrawDayCell(num + 1, first, weekday, !selected);
		selected = !selected;
		startSel = TRUE;
/*
	Track mouse and see if it stays in cell
*/
		while (MyWaitMouseUp()) {
			xPos = editDialog->MouseX;
			yPos = editDialog->MouseY;
			select = xPos >= rect.MinX && xPos <= rect.MaxX &&
				  yPos >= rect.MinY && yPos <= rect.MaxY;
			if (select != startSel) {
				selected = !selected;
				startSel = select;
				DrawDayCell(num + 1, first, weekday, selected);
			}
		}
		if (select)
			return (num);
	}

	return (-1);
}

/*
 *	select or deselect cell in unit rectangle
 */

static WORD DoUnitItem(SchedulePtr sched, WORD gadgNum, WORD numAcross, WORD numDown)
{
	WORD				num, xPos, yPos, cellWidth, cellHeight, row, column;
	BOOL				selected, select, startSel;
	Rectangle			rect;
	GadgetPtr			gadget;
	YearPtr				year = &sched->Year;

/*
	Find selected cell and highlight it
*/
	gadget = GadgetItem(editDialog->FirstGadget, gadgNum);
	xPos = editDialog->MouseX;
	yPos = editDialog->MouseY;
	cellWidth = gadget->Width/numAcross;
	cellHeight = gadget->Height/numDown;
	column = (xPos - gadget->LeftEdge)/cellWidth;
	row = (yPos - gadget->TopEdge)/cellHeight;
	num = row * numAcross + column;
	switch (gadgNum) {
		case MONTH_RECT:
			selected = year->Month[num];
			break;
		case WEEKDAY_RECT:
			selected = year->WeekDay[num];
			break;
		case PLACE_RECT:
			selected = year->Place[num];
			break;
	}
	if ((yPos - gadget->TopEdge) < (numDown*cellHeight)) {
		GetCellRect(gadgNum, column, row, numAcross, numDown, &rect);
		DrawCellItem(sched, gadgNum, column, row, numAcross, numDown, !selected);
		selected = !selected;
		startSel = TRUE;
/*
	Track mouse and see if it stays in cell
*/
		while (MyWaitMouseUp()) {
			xPos = editDialog->MouseX;
			yPos = editDialog->MouseY;
			select = xPos >= rect.MinX && xPos <= rect.MaxX &&
				  yPos >= rect.MinY && yPos <= rect.MaxY;
			if (select != startSel) {
				selected = !selected;
				startSel = select;
				DrawCellItem(sched, gadgNum, column, row, numAcross, numDown, selected);
			}
		}
		if (select)
			return (num);
	}

	return (-1);
}

