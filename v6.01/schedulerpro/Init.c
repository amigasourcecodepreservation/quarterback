/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Schedule Pro
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Initialization
 */

#include <exec/types.h>
#include <exec/libraries.h>
#include <workbench/startup.h>

#include <graphics/gfxmacros.h>
#include <graphics/gfxbase.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>
#include <libraries/dos.h>

#include <devices/timer.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>
#include <proto/icon.h>
#include <proto/wb.h>

#include <clib/alib_protos.h>

#include <TypeDefs.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/StdInit.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/DOS.h>
#include <Toolbox/Window.h>

#include <stdlib.h>
#include <string.h>

#include "Schedule.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct IntuitionBase	*IntuitionBase;
extern struct GfxBase		*GfxBase;
extern struct Library		*LayersBase;
extern struct Library		*IconBase;
extern struct Device		*ConsoleDevice;
extern struct Library		*WorkbenchBase;

extern WindowPtr	window;
extern MsgPortPtr	mainMsgPort;
extern MsgPortPtr	appIconMsgPort;
extern MsgPortPtr	timerMsgPort;

extern TextChar		strAppIconName[];
extern TextPtr		initError[];

extern TextChar		scheduleFile[];
extern SchedulePtr	scheduleList;
extern WORD			defaultButton;

/*
 *	Local variables and definitions
 */

static struct timerequest *timerIO;

static struct IOStdReq	consoleIOReq;

static ScreenPtr	screen;

static UWORD __chip appIconImageData[] = {
	0xFFFF, 0xFFF8, 0xFFFF, 0xFFF8, 0xFFFF, 0xFFF8, 0xFFFF, 0xFFF8,
	0x8888, 0x8888, 0x8888, 0x8888, 0x8888, 0x8888, 0xFFFF, 0xFFF8,
	0x8888, 0x8888, 0x8888, 0x8888, 0x8888, 0x8888, 0xFFFF, 0xFFF8,
	0x8888, 0x8888, 0x8888, 0x8888, 0x8888, 0x8888, 0xFFFF, 0xFFF8,
	0x8888, 0x8888, 0x8888, 0x8888, 0x8888, 0x8888, 0xFFFF, 0xFFF8,
	0x8888, 0x8888, 0x8888, 0x8888, 0x8888, 0x8888, 0xFFFF, 0xFFF8,

	0x0000, 0x0000, 0x7FFF, 0xFFF0, 0x7FFF, 0xFFF0, 0x0000, 0x0000,
	0x7777, 0x7770, 0x7777, 0x7770, 0x7777, 0x7770, 0x0000, 0x0000,
	0x7777, 0x7770, 0x7777, 0x7770, 0x7777, 0x7770, 0x0000, 0x0000,
	0x7777, 0x7770, 0x7777, 0x7770, 0x7777, 0x7770, 0x0000, 0x0000,
	0x7777, 0x7770, 0x7777, 0x7770, 0x7777, 0x7770, 0x0000, 0x0000,
	0x7777, 0x7770, 0x7777, 0x7770, 0x7777, 0x7770, 0x0000, 0x0000,
};

static Image appIconImage = {
	0, 0, 29, 24, 2, &appIconImageData[0], 3, 0, NULL
};

static struct DiskObject appIconObj = {
	0, 0,
		NULL,
		0, 0, 29, 25,
		GADGIMAGE | GADGBACKFILL, 0, 0,
		(APTR) &appIconImage, NULL, NULL, 0, NULL, 0, NULL,
	0, NULL, NULL, NO_ICON_POSITION, NO_ICON_POSITION,
	NULL, NULL, 0
};

static struct AppIcon		*appIcon;
static struct AppMenuItem	*appMenu;

/*
	local prototypes
 */

static ScreenPtr 	GetPublicScreen(TextPtr);
static BOOL 		MatchOptions(TextPtr, TextPtr, TextPtr);
static void			ShutDownIcons(void);

/*
 *	Return pointer to specified public screen
 *	If not running under 2.0 then only NULL is valid for screen name
 */

static ScreenPtr GetPublicScreen(TextPtr scrnName)
{
	LONG				intuiLock;
	register ScreenPtr	pubScreen;

	if (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0) {
		pubScreen = LockPubScreen(scrnName);
	}
	else {
		if (scrnName == NULL) {
			intuiLock = LockIBase(0);
			for (pubScreen = IntuitionBase->FirstScreen; pubScreen; pubScreen = pubScreen->NextScreen) {
				if ((pubScreen->Flags & SCREENTYPE) == WBENCHSCREEN)
					break;
			}
			UnlockIBase(intuiLock);
		}
		else {
			pubScreen = NULL;
		}
	}
	return (pubScreen);
}

/*
 *	Match screen options
 */

static BOOL MatchOptions(register TextPtr text, TextPtr opt1, TextPtr opt2)
{
	register WORD	len;

/*
	Get command length
*/
	for (len = 0; text[len] && text[len] != ':' && text[len] != '='; len++) ;
	if (text[len])
		len++;
/*
	Check for match
*/
	return ((BOOL) (CmpString(text, opt1, len, strlen(opt1), FALSE) == 0 ||
					CmpString(text, opt2, len, strlen(opt2), FALSE) == 0));
}

/*
 *	Initialization routine
 *	Open necessary libraries and devices, and open background window
 */

void Init(int argc, char *argv[])
{
	WORD				i;
	BOOL				fromCLI, doOpenWindow;
	TextPtr				text;
	struct WBStartup	*wbMsg;
	struct DiskObject	*icon;
/*
	Open needed libraries
*/
	if (!StdInit(OSVERSION_1_2)) {
		ShutDown();
		exit(RETURN_FAIL);
	}
	WorkbenchBase = OpenLibrary("workbench.library", OSVERSION_2_0);
	if (WorkbenchBase == NULL) {
		InitError(initError[INIT_NEED_2_0]);
		ShutDown();
		exit (RETURN_FAIL);
	}
/*
	Open main window
*/
	screen = GetPublicScreen(NULL);
	InitToolbox(screen);
/*
	Create message port
*/
	if ((mainMsgPort = CreatePort(NULL, 0)) == NULL) {
		InitError(initError[INIT_ERR_PORT]);
		ShutDown();
		exit(RETURN_FAIL);
	}
/*
	Must do this before creating the new window
*/
	if (!InitScheduleList()) {
		InitError(initError[INIT_ERR_LOAD]);
		ShutDown();
		exit(RETURN_FAIL);
	}
/*
	Setup tooltypes
*/
	fromCLI = (argc > 0);
	doOpenWindow = TRUE;
	if (fromCLI) {				// Running under CLI
		for (i = 1; i < argc; i++) {
			text = argv[i];
			if (*text++ == '-') {
				if (MatchOptions(text, "NoWindow", "NW"))
					doOpenWindow = FALSE;
			}
		}
	}
	else {						// Running under Workbench
		wbMsg = (struct WBStartup *) argv;
		if ((icon = GetDiskObject(wbMsg->sm_ArgList->wa_Name)) != NULL) {
			if (icon->do_ToolTypes) {
				for (i = 0; (text = icon->do_ToolTypes[i]) != NULL; i++) {
					if (MatchOptions(text, "NoWindow", "NW"))
						doOpenWindow = FALSE;
				}
			}
			FreeDiskObject(icon);
		}
	}
/*
	Open main window
*/
	if (doOpenWindow) {
		CreateWindow();
		if (window == NULL) {
			InitError(initError[INIT_ERR_WINDOW]);
			ShutDown();
			exit(RETURN_FAIL);
		}
	}
	else {
		window = NULL;			// Just to make sure

	}
/*
	Now that the window is open, unlock the screen
*/
	if (screen && (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0)) {
		UnlockPubScreen(NULL, screen);
		screen = NULL;
	}
/*
	Complete initialization
*/
/*
	Put up AppIcon
*/
	if (WorkbenchBase == NULL ||
		(appIconMsgPort = CreatePort(NULL, 0)) == NULL ||
		(appIcon = AddAppIcon(0, NULL, strAppIconName, appIconMsgPort, NULL, &appIconObj, TAG_END)) == NULL ||
		(appMenu = AddAppMenuItem(0, NULL, strAppIconName, appIconMsgPort, TAG_END)) == NULL)
		ShutDownIcons();

/*
	Open timer device and request signal once a minute
*/
	if ((timerMsgPort = CreatePort(NULL, 0)) == NULL ||
		(timerIO = (struct timerequest *) CreateExtIO(timerMsgPort, sizeof(struct timerequest))) == NULL ||
		OpenDevice(TIMERNAME, UNIT_VBLANK, (IOReqPtr) timerIO, 0)) {
		ShutDown();
		exit(RETURN_FAIL);
	}
}

/*
 *	Set next scheduler timer delay
 */

void SetTimer()
{
	LONG				secs;
	struct DateStamp	date;

	DateStamp(&date);
	secs = 61L - date.ds_Tick/TICKS_PER_SECOND;
	timerIO->tr_node.io_Command = TR_ADDREQUEST;
	timerIO->tr_time.tv_secs = secs;
	timerIO->tr_time.tv_micro = 0;
	SendIO((IOReqPtr) timerIO);
}

/*
 *	return TRUE if message is notification message from Scheduler
 */

BOOL IsSchedulerMsg(MsgPtr msg)
{
	return (msg == (MsgPtr) timerIO);
}

/*
 *	Shut down program
 *	Close window and all openned libraries
 */

void ShutDown()
{
	MsgPtr	msg;

	DisposeScheduleList();
	ShutDownIcons();
	if (timerIO) {
		if (!CheckIO((IOReqPtr) timerIO))
			AbortIO((IOReqPtr) timerIO);
		WaitIO((IOReqPtr) timerIO);
		CloseDevice((IOReqPtr) timerIO);
		DeleteExtIO((IOReqPtr) timerIO);
	}
	if (timerMsgPort) {
		while ((msg = GetMsg(timerMsgPort)) != NULL);
		DeletePort(timerMsgPort);
	}
	if (window)
		RemoveWindow();
	if (screen && (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0)) {
		UnlockPubScreen(NULL, screen);
		screen = NULL;
	}
	if (mainMsgPort) {
		while ((msg = GetMsg(mainMsgPort)) != NULL)
			ReplyMsg(msg);
		DeletePort(mainMsgPort);
	}
	StdShutDown();
}

/*
 *	Shut down icon system
 */

static void ShutDownIcons()
{
	MsgPtr	msg;

	if (appMenu || appIcon) {
		while (OpenWorkBench() == NULL) ;		// Must have Workbench screen open
	}
	if (appMenu)
		RemoveAppMenuItem(appMenu);
	if (appIcon)
		RemoveAppIcon(appIcon);
	if (appIconMsgPort) {
		while ((msg = GetMsg(appIconMsgPort)) != NULL)
			ReplyMsg(msg);
		DeletePort(appIconMsgPort);
	}
	if (WorkbenchBase)
		CloseLibrary(WorkbenchBase);
/*
	We could be called twice, so null out pointers
*/
	appMenu = NULL;
	appIcon = NULL;
	appIconMsgPort = NULL;
	WorkbenchBase = NULL;
}
