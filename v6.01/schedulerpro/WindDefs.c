/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Floppy Copy
 *
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Window Definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Language.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>

/*
 	external variables
*/

extern TextPtr	monthNames[];

/*
 *	Cursor key equivalents
 */

#define CURSORKEY_UP	0x1C
#define CURSORKEY_DOWN	0x1D
#define CURSORKEY_RIGHT	0x1E
#define CURSORKEY_LEFT	0x1F

/*
 *	Standard gadget definitions
 */

#define GT_ADJUST_UPARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,													\
		left, (top)+5-ARROW_HEIGHT, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		CURSORKEY_UP, 0, (Ptr) IMAGE_ARROW_UP }

#define GT_ADJUST_DOWNARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,										\
		left, (top)+5, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		CURSORKEY_DOWN, 0, (Ptr) IMAGE_ARROW_DOWN }

#define GT_ADJUST_TEXT(left, top)	\
	{ GADG_STAT_TEXT, (left)+10+ARROW_WIDTH, top, 0, 0, 0, 0, 0, 0, 0, 0, NULL }

/*
 *  Main dialog
 */

#define SCHED_WIDTH		8*25
#define SCHED_NUM		9

static GadgetTemplate mainGadgets[] = {
	{ GADG_PUSH_BUTTON, -80,  40, 0, 0, 60, 20, 0, 0, 'C', 0, "Change" },
	{ GADG_PUSH_BUTTON, -80,  10, 0, 0, 60, 20, 0, 0, 'N', 0, "New" },
	{ GADG_PUSH_BUTTON, -80,  70, 0, 0, 60, 20, 0, 0, 'D', 0, "Delete" },
	{ GADG_PUSH_BUTTON, -80, -40, 0, 0, 60, 20, 0, 0, 'Q', 0, "Quit" },

	SL_GADG_BOX(20, 30, SCHED_WIDTH, SCHED_NUM),
	SL_GADG_UPARROW(20, 30, SCHED_WIDTH, SCHED_NUM),
	SL_GADG_DOWNARROW(20, 30, SCHED_WIDTH, SCHED_NUM),
	SL_GADG_SLIDER(20, 30, SCHED_WIDTH, SCHED_NUM),

	{ GADG_STAT_TEXT, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Schedule list: " },

	{ GADG_ITEM_NONE },
};

static DialogTemplate mainDialog = {
	DLG_TYPE_WINDOW, DLG_FLAG_CLOSE | DLG_FLAG_DEPTH | DLG_FLAG_ZOOM,
	-1, -1, SCHED_WIDTH + 120, 11*SCHED_NUM + 60, mainGadgets, "Schedule Pro"
};

/*
 *	edit schedule dialog
 */

static TextPtr	typeList[] = { "Program", "Reminder",	NULL };

static GadgetTemplate editGadgets[] = {
	{ GADG_PUSH_BUTTON, -70, 10, 0, 0, 60, 20, 0, 0, 'S', 0, "Save" },
	{ GADG_PUSH_BUTTON, -70, 40, 0, 0, 60, 20, 0, 0, 'C', 0, "Cancel" },

	{ GADG_PUSH_BUTTON, 100, 35, 0, -3, 15, 11, 0, 6, '?', 0, "?" },

	{ GADG_RADIO_BUTTON, 295, 30, 0, 0, 0, 0, 0, 0, 'A', 0, "Active" },
	{ GADG_RADIO_BUTTON, 295, 45, 0, 0, 0, 0, 0, 0, 'I', 0, "Inactive" },

	{ GADG_POPUP,      10, 35, 0, 0, 80, 11, 0, 0, 0, 0, &typeList },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 100,  10, 0, 0, 22*8, 11, 0, 0, 0, 0, NULL },	// name box
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 124,  35, 0, 0, 19*8, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 100,  60, 0, 0, 22*8, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE | GADG_EDIT_NUMONLY,  355, 115, 0, 0,  40, 11, 0, 0, 0, 0, NULL },	// year

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE | GADG_EDIT_NUMONLY,  355, 195, 0, 0,  24, 11, 0, 0, 0, 0, NULL },	// hour
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE | GADG_EDIT_NUMONLY,  392, 195, 0, 0,  24, 11, 0, 0, 0, 0, NULL },	// minute

	GT_ADJUST_UPARROW(400, 116),					// year up
	GT_ADJUST_DOWNARROW(400, 116),					// year down

	GT_ADJUST_UPARROW(422, 196),					// time up
	GT_ADJUST_DOWNARROW(422, 196),					// time down

	{ GADG_ACTIVE_BORDER,  10, 110, 0, 0,  60, 120, 0, 0, 0, 0, NULL }, // month gadgets
	{ GADG_ACTIVE_BORDER, 110, 135, 0, 0, 140, 120, 0, 0, 0, 0, NULL }, // day gadgets
	{ GADG_ACTIVE_BORDER, 270, 135, 0, 0,  60, 100, 0, 0, 0, 0, NULL }, // place gadgets
	{ GADG_ACTIVE_BORDER, 110, 110, 0, 0, 140,  20, 0, 0, 0, 0, NULL }, // weekday gadgets

	{ GADG_CHECK_BOX, 352, 140, 0, 0, 0, 0, 0, 0, 'n', 0, "Annually" },
	{ GADG_CHECK_BOX, 110, 260, 0, 0, 0, 0, 0, 0, 'D', 0, "Daily" },
	{ GADG_CHECK_BOX, 180, 260, 0, 0, 0, 0, 0, 0, 'F', 0, "From End of Month" },
	{ GADG_CHECK_BOX,  10, 240, 0, 0, 0, 0, 0, 0, 'M', 0, "Monthly" },
	{ GADG_CHECK_BOX, 352, 220, 0, 0, 0, 0, 0, 0, 'H', 0, "Hourly" },

	{ GADG_STAT_STDBORDER,   10,  85, 0, 0, 455 - 20, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT,  285, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Status:" },
	{ GADG_STAT_TEXT,   10, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Schedule:" },
	{ GADG_STAT_TEXT,   10, 60, 0, 0, 0, 0, 0, 0, 0, 0, "Arguments:" },

	{ GADG_STAT_TEXT,  350,  95, 0, 0, 0, 0, 0, 0, 0, 0, "Year:" },
	{ GADG_STAT_TEXT,   10,  95, 0, 0, 0, 0, 0, 0, 0, 0, "Month:" },
	{ GADG_STAT_TEXT,  110,  95, 0, 0, 0, 0, 0, 0, 0, 0, "Day:" },
	{ GADG_STAT_TEXT,  270,  95, 0, 0, 0, 0, 0, 0, 0, 0, "Week:" },
	{ GADG_STAT_TEXT,  350, 175, 0, 0, 0, 0, 0, 0, 0, 0, "Time:" },
	{ GADG_STAT_TEXT,  382, 195, 0, 0, 0, 0, 0, 0, 0, 0, ":" },

	{ GADG_ITEM_NONE },
};

static DialogTemplate editDialog = {
	DLG_TYPE_ALERT, 0,	-1, -1, 455, 280, editGadgets, NULL
};

/*
 *  Reminder dialog
 */

static GadgetTemplate remindGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT, 55, 30, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 55, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Schedule Pro Reminder" },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_NOTE },

	{ GADG_ITEM_NONE }
};

static DialogTemplate remindDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 345, 100, remindGadgets, NULL
};


/*
 *  About dialog
 */

static GadgetTemplate aboutGadgets[] = {
	{ GADG_PUSH_BUTTON, 100, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT, 82, 15, 0, 0, 0, 0, 0, 0, 0, 0, "Schedule Pro" },
	{ GADG_STAT_TEXT, 42, 40, 0, 0, 0, 0, 0, 0, 0, 0, "Designed and developed" },
	{ GADG_STAT_TEXT, 78, 55, 0, 0, 0, 0, 0, 0, 0, 0, "by Beth Henry" },
	{ GADG_STAT_TEXT, 62, 70, 0, 0, 0, 0, 0, 0, 0, 0, "and James Bayless" },
	{ GADG_STAT_TEXT, 66, 95, 0, 0, 0, 0, 0, 0, 0, 0, "Copyright \251 1993" },
	{ GADG_STAT_TEXT, 42,110, 0, 0, 0, 0, 0, 0, 0, 0, "Central Coast Software" },
	{ GADG_STAT_TEXT, 78,125, 0, 0, 0, 0, 0, 0, 0, 0, "A division of" },
	{ GADG_STAT_TEXT, 22,140, 0, 0, 0, 0, 0, 0, 0, 0, "New Horizons Software, Inc."},
	{ GADG_STAT_TEXT, 54,155, 0, 0, 0, 0, 0, 0, 0, 0, "All Rights Reserved" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboutDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 210, aboutGadgets, NULL
};

/*
 *  Error dialog
 */

static GadgetTemplate errorGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT, 55, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },

	{ GADG_ITEM_NONE }
};

static DialogTemplate errorDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 345, 100, errorGadgets, NULL
};

/*
 *	Delete Confirm
 */

static GadgetTemplate deleteGadgets[] = {
	{ GADG_PUSH_BUTTON,  -80, -30, 0, 0, 60, 20, 0, 0, 'Y', 0, "Yes" },
	{ GADG_PUSH_BUTTON, -160, -30, 0, 0, 60, 20, 0, 0, 'N', 0, "No" },

	{ GADG_STAT_TEXT, 55, 15, 0, 0, 0, 0, 0, 0, 0, 0, "Do you really want to delete\nthe selected event?" },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_CAUTION },

	{ GADG_ITEM_NONE }
};

static DialogTemplate deleteDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 345, 100, deleteGadgets, NULL
};

/*
 *	Dialog list
 */

DlgTemplPtr dlgList[] = {
	&mainDialog,
	&editDialog,
	&remindDialog,
	&aboutDialog,
	&errorDialog,
	&deleteDialog
};
