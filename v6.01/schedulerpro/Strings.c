/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Schedule Pro
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Text strings
 */

#include <exec/types.h>

#include <Typedefs.h>

#include <Toolbox/Language.h>

static TextChar version[] = "$VER: SchedulePro 1.0";

TextChar screenTitle[] = " Schedule Pro 1.0 - \251 1993 Central Coast Software";

TextPtr	initError[] = {
	" This program requires AmigaDOS 2.0 or later.",
	" Can't open window.",
	" Can't create message port.",
	" Not enough memory.",
	" Unable to load events file."
};

TextPtr	errMessage[] = {
	"Unknown internal error.",
	"Not enough memory.",
	"Schedule is not valid.\n",
	"Unable to save schedule file.",
	"Unable to execute program."
};

TextPtr schedErrors[] = {
	"There is not a month selected.",
	"There is not a day selected.",
	"The time selected is not valid.",
	"The initial year must be \nbetween 1978 and 2100."
};

TextPtr	monthNames[] = {
	"Jan",	"Feb",	"Mar",	"Apr",	"May",	"Jun",
	"Jul",	"Aug",	"Sep",	"Oct",	"Nov",	"Dec"
};

TextPtr places[] = {
	"1st",	"2nd",	"3rd",	"4th",	"Last"
};

TextPtr weekdays[] = {
	"Su",	"M",	"Tu",	"W",	"Th",	"F",	"Sa"
};

TextChar	strHourly[] = "Hourly";
TextChar	strDaily[] = "Daily";

TextChar	strAll[] = "All";

TextChar	strAppIconName[] = "Schedule Pro";
TextChar	scheduleFile[] = "s:Schedule Pro Events";
TextChar	defaultSchedName[] = "Event #";

TextChar	strSchedProg[] = "Select program to run at activation";
