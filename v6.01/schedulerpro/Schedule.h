/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Schedule Pro
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Scheduler Data Objects
 */

#include <dos/dos.h>

typedef struct DateStamp	*DateStampPtr;

/*
 *	Dialog and requester definitions
 */

enum {
	DLG_MAIN, 	DLG_EDIT,	DLG_REMIND,		DLG_ABOUT,		DLG_ERROR,
	DLG_DELETE
};

/*
 *	Error definitions
 */

enum {
	INIT_NEED_2_0, INIT_ERR_WINDOW, INIT_ERR_PORT, INIT_ERR_MEM, INIT_ERR_LOAD
};

enum {
	ERR_UNKNOWN,
	ERR_NO_MEM,
	ERR_BAD_SCHED,
	ERR_SAVE,
	ERR_CANT_RUN,
	ERR_MAX_ERROR
};

enum {
	SCHED_OK,
	SCHED_BAD_MONTH,
	SCHED_BAD_DAY,
	SCHED_BAD_TIME,
	SCHED_BAD_YEAR
};

/*
 *	Window definitions
 */

/*
	main dialog
*/

enum {
	CHANGE_BUTTON,
	NEW_BUTTON,
	DELETE_BUTTON,
	QUIT_BUTTON,
	SCHED_BOX,
	SCHED_UP,
	SCHED_DOWN,
	SCHED_SCROLL,
};

/*
	edit dialog
*/

enum {
	TYPE_PROGRAM,
	TYPE_REMINDER
};

#define SAVE_BUTTON		0

enum {
	BROWSE_BUTTON = 2,
	ACTIVE_RADBTN,
	INACTIVE_RADBTN,
	TYPE_POPUP,
	NAME_TEXT,
	PROGRAM_TEXT,
	ARGS_TEXT,
	YEAR_TEXT,
	HOUR_TEXT,
	MINUTE_TEXT,
	YEAR_UP,
	YEAR_DOWN,
	TIME_UP,
	TIME_DOWN,
	MONTH_RECT,
	DAY_RECT,
	PLACE_RECT,
	WEEKDAY_RECT,
	ANNUAL_BOX,
	DAILY_BOX,
	RELATIVE_BOX,
	MONTHLY_BOX,
	HOURLY_BOX
};

/*
 *	Unit definitions
 */


#define MIN_YEAR		1978
#define MAX_YEAR		2100

#define NUM_MINUTES		60
#define NUM_HOURS		24
#define NUM_DAYS		31
#define NUM_PLACES		5
#define NUM_WEEKDAYS	7
#define NUM_MONTHS		12

/*
 *	gadget definitions
 */

#define MONTH_NUMACROSS		2
#define MONTH_NUMDOWN		6

#define DAY_NUMACROSS		7
#define DAY_NUMDOWN			6

#define WEEKDAY_NUMACROSS	DAY_NUMACROSS
#define WEEKDAY_NUMDOWN		1

#define PLACE_NUMACROSS		1
#define PLACE_NUMDOWN		5

/*
 *	local structures
 */

/*
	year structure
*/

typedef struct Year {
	ULONG			Time;
	LONG			Year;
	UBYTE			Month[NUM_MONTHS];
	UBYTE			Day[NUM_DAYS];
	UBYTE			WeekDay[NUM_WEEKDAYS];
	UBYTE			Place[NUM_PLACES];
	BOOL			EveryMonth;
	BOOL			EveryDay;
	BOOL			EveryHour;
	BOOL			EveryMinute;
	BOOL			Annual;
	BOOL			RelativeTime;
} Year, *YearPtr;

/*
	general schedule structure
*/

typedef struct Schedule {
	struct Schedule		*Next;
	BOOL				Active;			// schedule is good
	WORD				Type;			// program schedule or reminder
	Year				Year;			// first year info
	struct DateStamp	NextDate;		// next time of activation
	TextPtr				SchedName;		// name of schedule
	TextPtr				Program;		// program to be run
	TextPtr				Args;			// arguments to program to be run
} Schedule, *SchedulePtr;

