/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Scheduler
 *
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Window routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <graphics/gfxmacros.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/layers.h>
#include <proto/dos.h>
#include <proto/graphics.h>

#include <string.h>

#include <devices/trackdisk.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Bargraph.h>
#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>
#include <Toolbox/DOS.h>
#include <Toolbox/Border.h>
#include <Toolbox/StdInit.h>

#include "Schedule.h"
#include "Proto.h"

/*
 *  External variables
 */

extern WindowPtr		window;
extern DialogPtr		editDialog;
extern MsgPortPtr		mainMsgPort;

extern BOOL	titleChanged;

extern TextChar screenTitle[];

extern TextPtr	errMessage[], schedErrors[];

extern DlgTemplPtr	dlgList[];

extern TextPtr	monthNames[], places[], weekdays[];

extern TextChar	strAll[], strDaily[], strHourly[];

extern ScrollListPtr	schedList;

extern UBYTE daysPerMonth[];

extern ScreenPtr	_tbScreen;

/*
 *	Local variables and definitions
 */

#define DAYS_IN_YEAR(yr)	((yr % 4) ? 365 : 366)	/* Valid till 2100 */

#define ERROR_TEXT	1

#define REMIND_TEXT	1

static TextAttr	topaz8Attr = {
	"topaz.font", 8, FS_NORMAL, FPF_ROMFONT | FPF_DISKFONT | FPF_PROPORTIONAL
};

#define CHARS_PER_LINE	34

/*
 *	Local prototypes
 */

static void CellText(WORD gadgNum, WORD, TextPtr);
static void DrawWindow(void);

/*
 *  Create and draw main window
 */

void CreateWindow()
{
	SchedulePtr	sched;
	WORD		i, style;

	if (window)
		return;

	if ((schedList = NewScrollList(SL_SINGLESELECT)) == NULL)
		return;

	if ((window = GetDialog(dlgList[DLG_MAIN], NULL, mainMsgPort)) == NULL) {
		DisposeScrollList(schedList);
		return;
	}

    SetStdPointer(window, POINTER_ARROW);
    SetWindowTitles(window, (UBYTE *) -1, screenTitle);

	InitScrollList(schedList, GadgetItem(window->FirstGadget, SCHED_BOX), window, NULL);
	DrawWindow();
	SLDoDraw(schedList, FALSE);
	i = 0;
	for (sched = FirstSchedule(); sched; sched = sched->Next) {
		SLAddItem(schedList, sched->SchedName, strlen(sched->SchedName), i);
		style = (sched->Active) ? FS_NORMAL : FSF_ITALIC;
		SLSetItemStyle(schedList, i, style);
		i++;
	}
	SLDoDraw(schedList, TRUE);
}

/*
 *	Remove main window
 */

void RemoveWindow()
{
	if (window) {
		SetClip(window->WLayer, NULL);
		if (schedList)
			DisposeScrollList(schedList);
		schedList = NULL;
		DisposeDialog(window);
		window = NULL;
	}
}

/*
 *	enable buttons in main window
 */

void SetButtons()
{
	WORD		num, selNum, offItem, onItem;
	GadgetPtr	gadgList = window->FirstGadget;

	if (window) {
		num = SLNextSelect(schedList, -1);
		selNum = SLNextSelect(schedList, -1);
		EnableGadgetItem(gadgList, CHANGE_BUTTON, window, NULL, num != -1);
		EnableGadgetItem(gadgList, DELETE_BUTTON, window, NULL, selNum != -1);
		if (selNum == -1) {
			offItem = CHANGE_BUTTON;
			onItem = NEW_BUTTON;
		}
		else {
			offItem = NEW_BUTTON;
			onItem = CHANGE_BUTTON;
		}
		OutlineButton(GadgetItem(gadgList, offItem), window, NULL, FALSE);
		OutlineButton(GadgetItem(gadgList, onItem), window, NULL, TRUE);
		SetDefaultButtons(onItem, -1);
	}
}

/*
 *	Get cell rectangle at given position
 */

void GetCellRect(WORD gadgNum, WORD i, WORD j, WORD numAcross, WORD numDown,
							register RectPtr rect)
{
	WORD width, height;
	GadgetPtr itemsGadg;

	itemsGadg = GadgetItem(editDialog->FirstGadget, gadgNum);
	width = itemsGadg->Width;
	height = itemsGadg->Height;
/*
	calc height
*/
	rect->MinY = itemsGadg->TopEdge + (j*height)/numDown;
	rect->MaxY = rect->MinY + height/numDown - 1;
/*
	calc width
*/
	rect->MinX = itemsGadg->LeftEdge + (i*width)/numAcross;
	rect->MaxX = rect->MinX + width/numAcross - 1;
}

/*
 *	return textptr to appropriate text given cell num and gadget
 */

static void CellText(WORD gadgNum, WORD num, TextPtr buff)
{
	buff[0] = '\0';

	switch (gadgNum) {
		case MONTH_RECT:
			strcpy(buff, monthNames[num]);
			break;
		case PLACE_RECT:
			strcpy(buff, places[num]);
			break;
		case WEEKDAY_RECT:
			strcpy(buff, weekdays[num]);
			break;
	}
}

/*
 *	Draw specified text at given location into items array
 */

void DrawCellItem(SchedulePtr sched, WORD gadgNum, WORD i, WORD j, WORD numAcross, WORD numDown,
							BOOL selected)
{
	TextChar 	text[10];
	WORD 		cellWidth, cellHeight, charWidth, charHeight, baseline;
	RastPtr 	rPort = editDialog->RPort;
	TextFontPtr font,oldFont;
	Rectangle 	rect;

	if (i < 0 || i >= numAcross || j < 0 || j >= numDown)
		return;

	CellText(gadgNum, j * numAcross + i, text);
	if (text[0] == '\0')
		return;

	if ((font = GetFont(&topaz8Attr)) == NULL)
		return;
	oldFont = rPort->Font;
	SetFont(rPort, font);

	charWidth = TextLength(rPort, text, strlen(text));
	charHeight = font->tf_YSize;
/*
	Clear the cell contents
*/
	GetCellRect(gadgNum, i, j, numAcross, numDown, &rect);
	SetAPen(rPort, (selected) ? _tbPenDark : _tbPenLight);
	FillRect(rPort, &rect);
/*
	Display the text
*/
	cellWidth  = rect.MaxX - rect.MinX + 1;
	cellHeight = rect.MaxY - rect.MinY + 1;
	baseline = (cellHeight - charHeight)/2 + font->tf_Baseline;
	if (baseline < font->tf_Baseline)
		baseline = font->tf_Baseline;
	if (baseline >= cellHeight)
		baseline = cellHeight - 1;
	SetAPen(rPort, (selected) ? _tbPenWhite : _tbPenBlack);
	Move(rPort, rect.MinX + (cellWidth - charWidth)/2, rect.MinY + baseline);
	Text(rPort, text, strlen(text));
	DrawShadowBox(rPort, &rect, 0, !selected);
	if (font) {
		CloseFont(font);
		SetFont(rPort, oldFont);
	}
}

/*
 *	Draw main window contents
 */

static void DrawWindow()
{
	SetButtons();
	SLDrawBorder(schedList);
	SLDrawList(schedList);
}

/*
 *	draw a cell in the day gadget
 */

void DrawDayCell(WORD num, WORD firstDay, WORD weekday, BOOL selected)
{
	GadgetPtr	dayGadget;
	Rectangle	rect;
	TextChar	text[3];
	RastPtr		rPort = editDialog->RPort;
	WORD		charWidth, charHeight, cellWidth, cellHeight, baseline;
	WORD		width, height;
	TextFontPtr	font, oldFont;

	if ((font = GetFont(&topaz8Attr)) == NULL)
		return;
	oldFont = rPort->Font;
	SetFont(rPort, font);

	dayGadget = GadgetItem(editDialog->FirstGadget, DAY_RECT);
	width = dayGadget->Width;
	height = dayGadget->Height;

	charHeight = font->tf_YSize;
	cellWidth  = width / 7;
	cellHeight = height / 6;
	baseline = (cellHeight - charHeight)/2 + font->tf_Baseline;
	if (baseline < font->tf_Baseline)
		baseline = font->tf_Baseline;
	if (baseline >= cellHeight)
		baseline = cellHeight - 1;

	rect.MinY = dayGadget->TopEdge + (((num + firstDay) / 7) * height)/ 6;
	rect.MaxY = rect.MinY + height/6 - 1;
	rect.MinX = dayGadget->LeftEdge + (weekday * width)/ 7;
	rect.MaxX = rect.MinX + width/7 - 1;
/*
	Clear the cell contents
*/
	SetAPen(rPort, selected ? _tbPenDark : _tbPenLight);
	FillRect(rPort, &rect);
/*
	Display the character
*/
	NumToString(num, text);
	charWidth = TextLength(rPort, text, strlen(text));
	SetAPen(rPort, selected ? _tbPenWhite : _tbPenBlack);
	Move(rPort, rect.MinX + (cellWidth - charWidth)/2, rect.MinY + baseline);
	Text(rPort, text, strlen(text));
	DrawShadowBox(rPort, &rect, 0, !selected);

	if (font) {
		CloseFont(font);
		SetFont(rPort, oldFont);
	}
}

/*
 *	Draw day gadget
 *	return month number of month drawn
 */

WORD DrawDayGadget(SchedulePtr sched)
{
	struct DateStamp	dateStamp;
	WORD 				i, j;
	WORD 				month, weekday, first, endDay;
	Rectangle			rect;
	BOOL				selected;

/*
	clear gadget rectangle
*/
	GetGadgetRect(GadgetItem(editDialog->FirstGadget, DAY_RECT), editDialog, NULL, &rect);
	SetAPen(editDialog->RPort, _tbPenLight);
	FillRect(editDialog->RPort, &rect);

	if (AbstractSettings(sched)) {
		for (i = 0; i < DAY_NUMACROSS; i++) {
			for (j = 0; j < DAY_NUMDOWN - 1; j++) {
				GetCellRect(DAY_RECT, i, j, DAY_NUMACROSS, DAY_NUMDOWN, &rect);
				selected = (sched->Year.Place[j] &&
					(sched->Year.WeekDay[i] || !AnyWeekDaySet(sched))) ||
					(sched->Year.WeekDay[i] && !AnyPlaceSet(sched));
				SetAPen(editDialog->RPort, (selected) ? _tbPenBlack : _tbPenLight);
				FillRect(editDialog->RPort, &rect);
				DrawShadowBox(editDialog->RPort, &rect, 0, TRUE);
			}
		}
		return (-1);
	}

	if (sched->Year.EveryMonth)
		month = 0;
	else {
		for (month = 0; month < NUM_MONTHS; month++)
			if (sched->Year.Month[month])
				break;
		if (month == NUM_MONTHS)
			month = 0;
	}
	ConvertToDateStamp(&dateStamp, 1, month, sched->Year.Year);

	first = dateStamp.ds_Days % 7 - 1;			// weekday of the the 1st
	endDay = (month == 1 && (sched->Year.Year % 4) == 0) ? 29 : daysPerMonth[month];

	for (i = 1; i <= endDay; i++) {
		weekday = (dateStamp.ds_Days + i - 1) % 7;
		DrawDayCell(i, first, weekday, sched->Year.Day[i - 1] && !sched->Year.EveryDay);
	}

	return (month);
}

/*
 *	draw list with appropriate months selected
 */

void DrawMonthGadget(SchedulePtr sched)
{
	WORD	i, j, num;

	for (i = 0; i < MONTH_NUMACROSS; i++) {
		for (j = 0; j < MONTH_NUMDOWN; j++) {
			num = j * MONTH_NUMACROSS + i;
			DrawCellItem(sched, MONTH_RECT, i, j, MONTH_NUMACROSS, MONTH_NUMDOWN,
								(BOOL) (!sched->Year.EveryMonth &&
										sched->Year.Month[num]));
		}
	}
}

/*
 *	draw all the individual buttons based on current schedule
 */

void DrawSchedule(SchedulePtr sched)
{
	WORD 	i, j, num;

	DrawMonthGadget(sched);
	(void)DrawDayGadget(sched);

	for (i = 0; i < PLACE_NUMACROSS; i++) {
		for (j = 0; j < PLACE_NUMDOWN; j++) {
			num = j * PLACE_NUMACROSS + i;
			DrawCellItem(sched, PLACE_RECT, i, j, PLACE_NUMACROSS, PLACE_NUMDOWN,
								(BOOL) sched->Year.Place[num]);
		}
	}
	for (i = 0; i < WEEKDAY_NUMACROSS; i++) {
		for (j = 0; j < WEEKDAY_NUMDOWN; j++) {
			num = j * WEEKDAY_NUMACROSS + i;
			DrawCellItem(sched, WEEKDAY_RECT, i, j, WEEKDAY_NUMACROSS, WEEKDAY_NUMDOWN,
								(BOOL) sched->Year.WeekDay[num]);
		}
	}

}

/*
 *	open edit schedule window
 */

BOOL OpenEditWindow(SchedulePtr sched)
{
	if ((editDialog = GetDialog(dlgList[DLG_EDIT], window->WScreen, mainMsgPort)) == NULL) {
		Error(ERR_NO_MEM, 0);
		return (FALSE);
	}

    SetStdPointer(editDialog, POINTER_ARROW);
    SetWindowTitles(editDialog, (UBYTE *) -1, screenTitle);
	OutlineButton(GadgetItem(editDialog->FirstGadget, SAVE_BUTTON), editDialog, NULL, TRUE);
	DrawSchedule(sched);

	return (TRUE);
}

/*
 *	Remove edit window
 */

void CloseEditWindow()
{
	if (editDialog) {
		SetClip(editDialog->WLayer, NULL);
		DisposeDialog(editDialog);
		editDialog = NULL;
	}
}

/*
 *	Refresh window (and status dialog if it is present)
 */

void RefreshWindow()
{
	RastPtr		rPort;
	Rectangle	rect;

	GetWindowRect(window, &rect);
	if (!EmptyRect(&rect)) {
/*
	If old color scheme, fill with _tbPenLight
	Note: Cannot call RefreshGadgets() within Begin/EndRefresh()
*/
		if (_tbPenLight != 0) {
			rPort = window->RPort;
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &rect);
			RefreshGadgets(GadgetItem(window->FirstGadget, 0), window, NULL);
		}
		BeginRefresh(window);
		DrawWindow();
		EndRefresh(window, TRUE);
	}
}

/*
 *	Return screen titles to default setting
 */

void FixTitle()
{
	SetWindowTitles(window, (TextPtr) -1, screenTitle);
	titleChanged = FALSE;
}

/*
 *  Error report routine
 *  Put error message on screen title bar
 */

void Error(register WORD errNum, WORD schedErrNum)
{
	TextChar	error[256];

	if (errNum >= ERR_MAX_ERROR)
		errNum = ERR_UNKNOWN;

	strcpy(error, errMessage[errNum]);
	if (errNum == ERR_BAD_SCHED)
		strcat(error, schedErrors[schedErrNum - 1]);

	dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = error;

	SetDefaultButtons(OK_BUTTON, -1);
	if (StdAlert(dlgList[DLG_ERROR], window->WScreen, mainMsgPort, NULL) == -1) {
		SetWindowTitles(window, (UBYTE *) -1, error);
   		DisplayBeep(window->WScreen);
    	SysBeep(5);
		titleChanged = TRUE;
	}
	SetButtons();
}

/*
 *	Handle HELP key
 */

void DoHelp()
{
	DialogPtr	dlg;

	if ((dlg = GetDialog(dlgList[DLG_ABOUT], window->WScreen, mainMsgPort)) == NULL)
		Error(ERR_NO_MEM, 0);
	else {
		SetDefaultButtons(OK_BUTTON, CANCEL_BUTTON);	// May have been changed by SetButtons()
		OutlineButton(GadgetItem(dlg->FirstGadget, OK_BUTTON), dlg, NULL, TRUE);
		(void) ModalDialog(mainMsgPort, dlg, DialogFilter);
		DisposeDialog(dlg);
		SetButtons();			// Restore old default buttons
	}
}

/*
 *	Filter function for ModalDialog/ModalRequest
 *	Handles window updates
 */

BOOL DialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	RequestPtr	req;

	if (intuiMsg->IDCMPWindow == window && intuiMsg->Class == REFRESHWINDOW) {
		ReplyMsg((MsgPtr) intuiMsg);
		if ((req = window->FirstRequest) != NULL)
			OutlineButton(GadgetItem(req->ReqGadget, OK_BUTTON), window, req, TRUE);
		RefreshWindow();
		*item = -1;
		return (TRUE);
	}
	return (FALSE);
}

/*
 * Converts day, month, and year into a DateStamp structure
 */

void ConvertToDateStamp(struct DateStamp *dateStamp, register UWORD days,
			UWORD month, register UWORD year)
{
	register WORD i;

	if( year < 1977 ) {
		year += 100;
	}
	for( i = 1978 ; i < year ; i++ ) {
		days += DAYS_IN_YEAR(i);
	}
	for( i = 0 ; i < (month) ; i++ ) {
		days += daysPerMonth[i];
	}
/*
 * Unless we're into a leap year past Feb, we're going to have subtract one
 * to compensate for the origin of "days" being one rather than zero.
 */
	if( !( (month > 1) && (((year % 4) == 0) && (year % 400)) ) ) {
		--days;
	}
	dateStamp->ds_Days = days;
	dateStamp->ds_Minute = 0;
	dateStamp->ds_Tick = 0;
}

/*
 *	open dialog with reminder
 */

BOOL DoReminderDialog(TextPtr reminder)
{
	WORD		i;
	WORD		len = strlen(reminder);
	DialogPtr	dlg;
	TextChar	text[200];

	strcpy(text, reminder);
	if (len > CHARS_PER_LINE) {
		for (i = CHARS_PER_LINE; reminder[i] != ' ' && i > 0; i--) ;
		if (i == 0) {
			for (i = CHARS_PER_LINE; reminder[i] != ' ' && i < CHARS_PER_LINE + 3; i++);
			if (i == CHARS_PER_LINE + 3) {
				for (i = len; i > CHARS_PER_LINE - 1; i--)
					text[i] = text[i - 1];
				i++;
				len++;
			}
		}
		text[i - 1] = '\n';
	}
	if (len > 2 * CHARS_PER_LINE) {
		for (i = 2 * CHARS_PER_LINE; reminder[i] != ' ' && i > CHARS_PER_LINE + 1; i--);
		if (i == CHARS_PER_LINE + 1) {
			for (i= 2 * CHARS_PER_LINE + 1; reminder[i] != ' ' && i < 2 * CHARS_PER_LINE + 4; i++);
			if (i == 2 * CHARS_PER_LINE + 4) {
				for (i = len; i > 2 * CHARS_PER_LINE; i--)
					text[i] = text[i - 1];
				i++;
			}
		}
		text[i - 1] = '\n';
	}

	dlgList[DLG_REMIND]->Gadgets[REMIND_TEXT].Info = text;

	if ((dlg = GetDialog(dlgList[DLG_REMIND], NULL, mainMsgPort)) == NULL)
		Error(ERR_NO_MEM, 0);
	else {
		OutlineButton(GadgetItem(dlg->FirstGadget, OK_BUTTON), dlg, NULL, TRUE);
		(void) ModalDialog(mainMsgPort, dlg, DialogFilter);
		DisposeDialog(dlg);
	}

	return (TRUE);
}