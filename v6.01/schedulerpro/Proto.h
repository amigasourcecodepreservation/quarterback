/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *  Scheduler
 *
 *  Global function Prototypes
 */

/*
 *  Main.c
 */

void 	main(int, char **);

/*
 *  Init.c
 */

void 	Init(int, char **);
void 	ShutDown(void);
void	SetTimer(void);
BOOL	IsSchedulerMsg(MsgPtr);

/*
 *  Window.c
 */

void 	CreateWindow(void);
void	RemoveWindow(void);
void	SetButtons(void);
void 	GetCellRect(WORD, WORD, WORD, WORD, WORD, RectPtr);
void 	DrawCellItem(SchedulePtr, WORD, WORD, WORD, WORD, WORD, BOOL);
void 	DrawDayCell(WORD, WORD, WORD, BOOL);
WORD	DrawDayGadget(SchedulePtr);
void	DrawHourGadget(SchedulePtr);
void	DrawMinuteGadget(SchedulePtr);
void	DrawMonthGadget(SchedulePtr);
void	DrawSchedule(SchedulePtr);
BOOL	OpenEditWindow(SchedulePtr);
void	CloseEditWindow(void);
void	RefreshWindow(void);
void 	FixTitle(void);
void	Error(WORD, WORD);
void	DoHelp(void);
BOOL	DialogFilter(IntuiMsgPtr, WORD *);
void 	ConvertToDateStamp(struct DateStamp *, UWORD, UWORD, UWORD);
BOOL	DoReminderDialog(TextPtr);

/*
 *	Schedule.c
 */

BOOL		InitScheduleList(void);
void		SaveScheduleList(void);
void		ExecuteSchedules(void);
SchedulePtr	NewSchedule(void);
void		DisposeSchedule(SchedulePtr);
void		DisposeScheduleList(void);
WORD		NumSchedules(void);
void		SetFirstSchedule(SchedulePtr);
SchedulePtr	FirstSchedule(void);
SchedulePtr	LastSchedule(void);
SchedulePtr	GetSchedule(WORD);
BOOL		SetScheduleName(SchedulePtr, TextPtr);
BOOL		SetScheduleProgram(SchedulePtr, TextPtr);
BOOL		SetScheduleArgs(SchedulePtr, TextPtr);
BOOL		AnyPlaceSet(SchedulePtr);
BOOL		AnyWeekDaySet(SchedulePtr);
BOOL		AbstractSettings(SchedulePtr);
WORD		ValidSchedule(SchedulePtr);
void		CalculateNextDate(SchedulePtr);
