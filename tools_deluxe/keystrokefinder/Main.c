/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	KeyFinder
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	find sequence of keys for literal
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <dos/dos.h>

#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <proto/console.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/intuition.h>
#include <proto/keymap.h>

#include <Typedefs.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/DOS.h>

#include <string.h>

#include "Proto.h"
#include "KeyFinder.h"

/*
 *	External variables
 */

extern BOOL	titleChanged;

extern TextChar	strShift[], strAlt[], strCtrl[], strSpace[], strUnknown[];

extern WindowPtr 	window;
extern MsgPortPtr	mainMsgPort;

/*
 *	Local variables and definitions
 */

static BOOL		quitFlag;

#define RAWKEY_SPACE	0x40
#define RAWKEY_HELP		0x5F
#define CR				0x0D

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)
#define ALTKEYS		(IEQUALIFIER_LALT | IEQUALIFIER_RALT)

/*
 *	Prototypes
 */

static BOOL	MainDialogFilter(IntuiMsgPtr, WORD *);
static void DoMainDialogItem(WORD);

static void	AppendKeySequence(TextPtr, UBYTE, UBYTE);
static void	DisplayKeySequence(void);

static void ByteToHexString(UBYTE, TextPtr);
static void	DisplayKeyCodes(void);

/*
 *	Main routine
 */

void main(int argc, char *argv[])
{
	IntuiMsgPtr	intuiMsg;
	WORD		itemHit;

	Init(argc, argv);
/*
	Get and process events
*/
	quitFlag = FALSE;
	while (!quitFlag) {
		while (intuiMsg = (IntuiMsgPtr)GetMsg(mainMsgPort)) {
			if (MainDialogFilter(intuiMsg, &itemHit))
				DoMainDialogItem(itemHit);
			else if (DialogSelect(intuiMsg, &window, &itemHit)) {
				ReplyMsg((MsgPtr) intuiMsg);
				DoMainDialogItem(itemHit);
			}
			else
				ReplyMsg((MsgPtr) intuiMsg);
		}
		if (!quitFlag)
			Wait(1L << mainMsgPort->mp_SigBit);
	}
	ShutDown();
}

/*
 *	Handle raw intuiMsgs's in main window
 */

static BOOL MainDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register ULONG 	class;
	register UWORD 	code;
	register BOOL	msgReply = FALSE;
	register WORD	itemHit;

	if (intuiMsg->Class == RAWKEY)
		ConvertKeyMsg(intuiMsg);	/* Convert some RAWKEY to VANILLAKEY */

	class = intuiMsg->Class;
	code = intuiMsg->Code;

	switch (class) {
	case GADGETDOWN:
	case GADGETUP:
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == KEY_BOX) {
			ReplyMsg((MsgPtr)intuiMsg);
			msgReply = TRUE;
			if (class == GADGETDOWN) {
				DoBoxClick();
				*item = KEY_BOX;
			}
			else
				*item = -1;
		}
		break;
	case RAWKEY:
		if (code == CURSORUP || code == CURSORDOWN || code == CURSORLEFT ||
			code == CURSORRIGHT) {
			ReplyMsg((MsgPtr) intuiMsg);
			msgReply = TRUE;
			DoCursorKey(code);
			*item = KEY_BOX;
		}
		else if (code == RAWKEY_HELP) {
			ReplyMsg((MsgPtr)intuiMsg);
			msgReply = TRUE;
			DoHelp();
		}
		break;
	case VANILLAKEY:
		if (code == CR) {
			ReplyMsg((MsgPtr)intuiMsg);
			msgReply = TRUE;
			*item = CHANGEWINDOW_ICON;
		}
		break;
	case REFRESHWINDOW:
		ReplyMsg((MsgPtr)intuiMsg);
		msgReply = TRUE;
		*item = -1;
		RefreshWindow();
		break;
	}
	return (msgReply);
}

/*
 *	Handle gadget click in main window
 */

static void DoMainDialogItem(WORD item)
{
	switch (item) {
	case DLG_CLOSE_BOX:
		quitFlag = TRUE;
		break;
	case KEY_BOX:
		DisplayKeySequence();
		DisplayKeyCodes();
		break;
	case CHANGEWINDOW_ICON:
		ChangeWindow();
		break;
	}
}

/*
 *	Append key sequence description to text
 */

static void AppendKeySequence(TextPtr sequence, UBYTE rawKey, UBYTE qualifier)
{
	TextChar			ch;
	WORD				len;
	struct InputEvent	keyEvent;

/*
	Convert raw key
*/
	BlockClear(&keyEvent, sizeof(struct InputEvent));
	keyEvent.ie_Class = IECLASS_RAWKEY;
	keyEvent.ie_Code = rawKey;
	len = RawKeyConvert(&keyEvent, &ch, 1, NULL);
	if (len == 0) {								// A naked dead key
		keyEvent.ie_Prev1DownCode = rawKey;
		keyEvent.ie_Code = RAWKEY_SPACE;
		len = RawKeyConvert(&keyEvent, &ch, 1, NULL);
	}
/*
	Add qualifiers
*/
	if (qualifier & IEQUALIFIER_CONTROL)
		strcat(sequence, strCtrl);
	if (qualifier & ALTKEYS)
		strcat(sequence, strAlt);
	if (qualifier & SHIFTKEYS)
		strcat(sequence, strShift);
/*
	Add key name (as shown on keyboard)
*/
	if (len != 1)
		strcat(sequence, strUnknown);	// Unable to convert naked dead key
	else if (ch == ' ')
		strcat(sequence, strSpace);
	else {
		len = strlen(sequence);
		sequence[len] = toUpper[ch];
		sequence[len + 1] = '\0';
	}
}

/*
 *	Display the key sequence for currently selected character
 */

static void DisplayKeySequence()
{
	TextChar	ch;
	WORD		i, numKeys;
	TextPtr		rawKeyData;
	WORD		rawKeyBuff[3];		// Must be WORD-aligned
	TextChar	sequence[50];

/*
	Get raw key sequence for selected key
*/
	ch = GetCurrentChar();
	if (ch == DEL) {
		SetGadgetItemText(window->FirstGadget, KEY_SEQ_TXT, window, NULL, NULL);
		return;
	}
	rawKeyData = (TextPtr) rawKeyBuff;
	numKeys = MapANSI(&ch, 1, rawKeyData, 3, NULL);
/*
	Convert to readable description and display
*/
	i = 0;
	sequence[0] = '\0';
	switch (numKeys) {
	case 3:
		AppendKeySequence(sequence, rawKeyData[i], rawKeyData[i + 1]);
		strcat(sequence, ", ");
		i += 2;				// Fall through
	case 2:
		AppendKeySequence(sequence, rawKeyData[i], rawKeyData[i + 1]);
		strcat(sequence, ", ");
		i += 2;				// Fall through
	case 1:
		AppendKeySequence(sequence, rawKeyData[i], rawKeyData[i + 1]);
		break;
	case -1:				// Should not happen
		SysBeep(5);
		break;
	}
	SetGadgetItemText(window->FirstGadget, KEY_SEQ_TXT, window, NULL, sequence);
}

/*
 * Convert an unsigned longword into a hex string eight(+1) characters long.
 * Terminate with a space, but no null-terminator needed.
 */

static UBYTE hexChars[] = {
	'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'
};

static void ByteToHexString(register UBYTE val, register TextPtr buff)
{
	buff[0] = '$';
	buff[2] = hexChars[val & 0xF];
	val >>=	4;
	buff[1] = hexChars[val & 0xF];
	buff[3] = '\0';
}

/*
 *	Display decimal and hex codes for currently selected character
 */

static void DisplayKeyCodes()
{
	GadgetPtr	gadgList = window->FirstGadget;
	TextChar	ch, code[10];

	ch = GetCurrentChar();
	if (ch == DEL) {
		SetGadgetItemText(gadgList, DECCODE_TXT, window, NULL, NULL);
		SetGadgetItemText(gadgList, HEXCODE_TXT, window, NULL, NULL);
	}
	else {
		NumToString(ch, code);
		SetGadgetItemText(gadgList, DECCODE_TXT, window, NULL, code);
		ByteToHexString(ch, code);
		SetGadgetItemText(gadgList, HEXCODE_TXT, window, NULL, code);
	}
}
