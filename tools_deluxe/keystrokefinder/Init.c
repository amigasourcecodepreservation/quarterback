/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	KeyFinder
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Initialization
 */

#include <exec/types.h>
#include <exec/libraries.h>
#include <workbench/startup.h>

#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>
#include <proto/icon.h>

#include <clib/alib_protos.h>

#include <Typedefs.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/StdInit.h>
#include <Toolbox/DOS.h>

#include <stdlib.h>
#include <string.h>

#include "Proto.h"
#include "KeyFinder.h"

/*
 *	Global variables
 */

extern struct IntuitionBase	*IntuitionBase;
extern struct GfxBase			*GfxBase;
extern struct Library			*LayersBase, *IconBase, *DiskfontBase, *KeymapBase;
extern struct Device			*ConsoleDevice;

/*
 *	External variables
 */

extern WindowPtr	window;
extern MsgPortPtr	mainMsgPort;
extern TextChar		volumeName[];

extern TextPtr	initError[];

/*
 *	Local variables and definitions
 */

#define INIT_BADSYS 	0

static struct IOStdReq	consoleIOReq;

static ScreenPtr	screen;

/*
 *	Local prototypes
 */

static BOOL MatchOptions(TextPtr, TextPtr, TextPtr);
static void	SetOption(TextPtr);

static ScreenPtr GetPublicScreen(TextPtr);

/*
 *	Match window option
 */

static BOOL MatchOptions(register TextPtr text, TextPtr opt1, TextPtr opt2)
{
	register WORD	len;

/*
	Get command length
*/
	for (len = 0; text[len] && text[len] != ':' && text[len] != '='; len++) ;
	if (text[len])
		len++;
/*
	Check for match
*/
	return ((BOOL) (CmpString(text, opt1, len, strlen(opt1), FALSE) == 0 ||
					CmpString(text, opt2, len, strlen(opt2), FALSE) == 0));
}


/*
 *	Set appropriate option from string
 */

static void SetOption(TextPtr text)
{
	if (MatchOptions(text, "horizontal", "h"))
		InitWindow(TRUE);
}

/*
 *	Return pointer to specified public screen
 *	If not running under 2.0 then only NULL is valid for screen name
 */

static ScreenPtr GetPublicScreen(TextPtr scrnName)
{
	LONG intuiLock;
	register ScreenPtr	pubScreen;

	if (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0)
		pubScreen = LockPubScreen(scrnName);
	else {
		if (scrnName == NULL) {
			intuiLock = LockIBase(0);
			for (pubScreen = IntuitionBase->FirstScreen; pubScreen; pubScreen = pubScreen->NextScreen) {
				if ((pubScreen->Flags & SCREENTYPE) == WBENCHSCREEN)
					break;
			}
			UnlockIBase(intuiLock);
		}
		else
			pubScreen = NULL;
	}
	return (pubScreen);
}

/*
 *	Initialization routine
 *	Open necessary libraries and devices, and open background window
 */

void Init(int argc, char *argv[])
{
	register TextPtr	text;
	struct DiskObject	*icon;
	struct WBStartup	*wbMsg;
	WORD				i;

/*
	Open needed libraries (this program requires Kickstart 2.0 or later)
*/
	if (!StdInit(OSVERSION_1_2)) {
		ShutDown();
		exit(RETURN_FAIL);
	}
	if ((KeymapBase = OpenLibrary("keymap.library", OSVERSION_2_0_4)) == NULL) {
		InitError(initError[INIT_NEED_2_0]);
		ShutDown();
		exit (RETURN_FAIL);
	}
/*
	Setup for tooltype options
*/
	InitWindow(FALSE);		// Set initial window defaults
	if (argc) {				// Running under CLI
		for (i = 1; i < argc; i++) {
			text = argv[i];
			if (*text++ == '-')
				SetOption(text);
		}
	}
	else {					// Running under Workbench
		wbMsg = (struct WBStartup *) argv;
		if ((icon = GetDiskObject(wbMsg->sm_ArgList->wa_Name)) != NULL) {
			if (icon->do_ToolTypes) {
				for (i = 0; (text = icon->do_ToolTypes[i]) != NULL; i++)
					SetOption(text);
			}
			FreeDiskObject(icon);
		}
	}
/*
	Initialize toolbox
*/
	screen = GetPublicScreen(NULL);
	InitToolbox(screen);
/*
	Create message port
*/
	if ((mainMsgPort = CreatePort(NULL, 0)) == NULL) {
		InitError(initError[INIT_ERR_PORT]);
		ShutDown();
		exit(RETURN_FAIL);
	}
/*
	Open main window
*/
	CreateWindow();
	if (window == NULL) {
		InitError(initError[INIT_ERR_WINDOW]);
		ShutDown();
		exit(RETURN_FAIL);
	}
/*
	Now that the window is open, unlock the screen
*/
	if (screen && (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0)) {
		UnlockPubScreen(NULL, screen);
		screen = NULL;
	}
	mainMsgPort = window->UserPort;
}

/*
 *	Shut down program
 *	Close window and all openned libraries
 */

void ShutDown()
{
	register MsgPtr	msg;

	if (window) {
		RemoveWindow();
	}
	if (screen && (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0)) {
		UnlockPubScreen(NULL, screen);
		screen = NULL;
	}
	if (mainMsgPort) {
		while ((msg = GetMsg(mainMsgPort)) != NULL)
			ReplyMsg(msg);
		DeletePort(mainMsgPort);
	}
	if (KeymapBase)
		CloseLibrary(KeymapBase);
	StdShutDown();
}
