/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	KeyFinder
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Window routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/layers.h>
#include <proto/dos.h>
#include <proto/graphics.h>

#include <string.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "KeyFinder.h"
#include "Proto.h"

/*
 *  External variables
 */

extern WindowPtr	window;
extern MsgPortPtr	mainMsgPort;

extern TextChar screenTitle[];

extern TextPtr	errMessage[];

extern DlgTemplPtr	dlgList[];

extern BOOL	titleChanged;

/*
 *	Local variables and definitions
 */

static BOOL	horizWindow;

static WORD numAcross;
static WORD numDown;

static WORD	selColumn;
static WORD	selRow;

static ImagePtr	images[2];

static TextFontPtr	windowFont;

#define ERROR_TEXT 	1
#define ERROR_WIDTH	375

/*
 *	Icons for layout dialog
 */

static ULONG __chip horizIconData[] = {
	0x00000000, 0x00000000, 0x00000000, 0x00000000,
	0x00820000, 0x01830000, 0x03838000, 0x07FFC000,
	0x0FFFE000, 0x1FFFF000, 0x0FFFE000, 0x07FFC000,
	0x03838000, 0x01830000, 0x00820000, 0x00000000,
	0x00000000, 0x00000000, 0x00000000, 0x00000000
};

static ULONG __chip vertIconData[] = {
	0x00000000, 0x00000000, 0x00100000, 0x00380000,
	0x007C0000, 0x00FE0000, 0x01FF0000, 0x03FF8000,
	0x007C0000, 0x007C0000, 0x007C0000, 0x007C0000,
	0x03FF8000, 0x01FF0000, 0x00FE0000, 0x007C0000,
	0x00380000, 0x00100000, 0x00000000, 0x00000000
};

static Icon horizIcon = {
	23, 20, 1, NULL, (PLANEPTR) &horizIconData[0], NULL
};

static Icon vertIcon = {
	23, 20, 1, NULL, (PLANEPTR) &vertIconData[0], NULL
};

static ULONG __chip mrHorizIconData[] = {
	0x00000000, 0x00000000, 0x00000000, 0x00000000,
	0x00C60000, 0x03C78000, 0x0FFFE000, 0x3FFFF800,
	0x0FFFE000, 0x03C78000, 0x00C60000, 0x00000000,
	0x00000000, 0x00000000, 0x00000000
};

static ULONG __chip mrVertIconData[] = {
	0x00000000, 0x00000000, 0x00100000, 0x007C0000,
	0x01FF0000, 0x07FFC000, 0x007C0000, 0x007C0000,
	0x07FFC000, 0x01FF0000, 0x007C0000, 0x00100000,
	0x00000000, 0x00000000, 0x00000000
};

static Icon mrHorizIcon = {
	23, 15, 1, NULL, (PLANEPTR) &mrHorizIconData[0], NULL
};

static Icon mrVertIcon = {
	23, 15, 1, NULL, (PLANEPTR) &mrVertIconData[0], NULL
};

/*
 *	Local prototypes
 */

static BOOL 	IsTopaz8WindowFont(void);
static void		GetIconImage(Icon *, Icon *);
static void		SetIconImage(void);
static void		DisposeIconImage(void);
static void		DrawWindow(void);
static TextChar	CellChar(WORD, WORD);
static void		GetCellRect(WORD, WORD, RectPtr);
static void		DrawCharItem(WORD, WORD, BOOL);

/*
 *	Return TRUE if window font is topaz-8 or equivalent
 */

static BOOL IsTopaz8WindowFont()
{
	if (windowFont == NULL)
		return (FALSE);
	return (windowFont->tf_YSize == 8 && windowFont->tf_XSize == 8 &&
			(windowFont->tf_Flags & FPF_PROPORTIONAL) == 0);
}

/*
 *	Set gadget images for icons
 */

static void GetIconImage(Icon *icon,Icon *mrIcon)
{
	ColorMapPtr	colorMap = _tbScreen->ViewPort.ColorMap;
	UWORD		iconColors[2];

	icon->ColorTable = mrIcon->ColorTable = iconColors;

	iconColors[0] = GetRGB4(colorMap, _tbPenLight);
	iconColors[1] = GetRGB4(colorMap, _tbPenBlack);

	if (IsTopaz8WindowFont())
		images[0] = MakeIconImage(mrIcon, mrIcon->Width, mrIcon->Height,
								  ICON_BOX_SHADOWOUT);
	else
		images[0] = MakeIconImage(icon, 0, 0, ICON_BOX_SHADOWOUT);

	iconColors[0] = GetRGB4(colorMap, _tbPenDark);
	iconColors[1] = GetRGB4(colorMap, _tbPenWhite);
	if (IsTopaz8WindowFont())
		images[1] = MakeIconImage(mrIcon, mrIcon->Width, mrIcon->Height,
								  ICON_BOX_SHADOWIN);
	else
		images[1] = MakeIconImage(icon, 0, 0, ICON_BOX_SHADOWIN);
}

/*
 *	Set icon images for icons
 */

static void SetIconImage()
{
	register GadgetPtr gadget;

	gadget = GadgetItem(window->FirstGadget, CHANGEWINDOW_ICON);
	gadget->GadgetRender = images[0];
	gadget->SelectRender = images[1];
	gadget->Flags = (gadget->Flags & ~GFLG_GADGHIGHBITS) | GFLG_GADGHIMAGE;
	RefreshGList(gadget, window, NULL, 1);
}

/*
 *	dispose of search indic icons
 */

static void DisposeIconImage()
{
	register WORD i;

	for (i = 0; i < 1; i++) {
		if (images[i]) {
			FreeImage(images[i]);
			images[i] = NULL;
		}
	}
}

/*
 *	Initialize local window defs
 */

void InitWindow(BOOL horiz)
{
	horizWindow = horiz;
	if (horizWindow) {
		numAcross = 16;
		numDown = 12;
	}
	else {
		numAcross = 24;
		numDown = 8;
	}
}


/*
 * 	change the window definition
 */

void ChangeWindow()
{
	RemoveWindow();
	InitWindow(!horizWindow);
	CreateWindow();
}

/*
 *  Create and draw main window
 */

void CreateWindow()
{
	windowFont = GetFont(&_tbTextAttr);		// Needed by GetIconImage()
	if (horizWindow) {
		GetIconImage(&vertIcon,&mrVertIcon);
		window = GetDialog(dlgList[DLG_HORIZWINDOW], NULL, mainMsgPort);
	}
	else {
		GetIconImage(&horizIcon,&mrHorizIcon);
		window = GetDialog(dlgList[DLG_VERTWINDOW], NULL, mainMsgPort);
	}

	if (window == NULL) {
		DisposeIconImage();
		return;
	}

    SetStdPointer(window, POINTER_ARROW);
    SetWindowTitles(window, (UBYTE *) -1, screenTitle);
	ModifyIDCMP(window, window->IDCMPFlags | DISKINSERTED | DISKREMOVED);
	selColumn = selRow = -1;
	SetIconImage();
	if (windowFont)
		SetFont(window->RPort, windowFont);
	DrawWindow();
}

/*
 *  Remove main window
 */

void RemoveWindow()
{
	if (window) {
		SetStdPointer(window, POINTER_WAITDELAY);
		SetClip(window->WLayer, NULL);
		DisposeIconImage();
		DisposeDialog(window);
		if (windowFont) {
			CloseFont(windowFont);
			windowFont = NULL;
		}
		window = NULL;
	}
}

/*
 *	Draw window contents
 */

static void DrawWindow()
{
	WORD	i, j;

	if (horizWindow) {
		for (j = 0; j < numDown; j++) {
			for (i = 0; i < numAcross; i++) {
				DrawCharItem(i, j, (i == selColumn && j == selRow));
			}
		}
	}
	else {
		for (i = 0; i < numAcross; i++) {
			for (j = 0; j < numDown; j++) {
				DrawCharItem(i, j, (i == selColumn && j == selRow));
			}
		}
	}
}

/*
 *	Refresh window
 */

void RefreshWindow()
{
	RastPtr		rPort;
	Rectangle	rect;

	GetWindowRect(window, &rect);
	if (!EmptyRect(&rect)) {
/*
	If old color scheme, fill with _tbPenLight
	Note: Cannot call RefreshGadgets() within Begin/EndRefresh()
*/
		if (_tbPenLight != 0) {
			rPort = window->RPort;
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &rect);
			RefreshGadgets(GadgetItem(window->FirstGadget, 0), window, NULL);
		}
		BeginRefresh(window);
		DrawWindow();
		EndRefresh(window, TRUE);
	}
}

/*
 *	Return screen titles to default setting
 */

void FixTitle()
{
	SetWindowTitles(window, (TextPtr) -1, screenTitle);
	titleChanged = FALSE;
}

/*
 *  Error report routine
 *  Put error message on screen title bar
 */

void Error(register WORD errNum)
{
	if (errNum > ERR_MAX_ERROR)
		errNum = ERR_UNKNOWN;
	dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = errMessage[errNum];
	if (StdAlert(dlgList[DLG_ERROR], window->WScreen, mainMsgPort, NULL) == -1) {
		SetWindowTitles(window, (UBYTE *) -1, errMessage[errNum]);
   		DisplayBeep(window->WScreen);
    	SysBeep(5);
		titleChanged = TRUE;
	}
}

void DoHelp()
{
	DialogPtr dlg;

	if ((dlg = GetDialog(dlgList[DLG_ABOUT], window->WScreen, mainMsgPort)) == NULL)
		Error(ERR_NO_MEM);
	else {
		OutlineButton(GadgetItem(dlg->FirstGadget, OK_BUTTON), dlg, NULL, TRUE);
		(void) ModalDialog(mainMsgPort, dlg, NULL);
		DisposeDialog(dlg);
	}
}

/*
 *	Return character in specified cell
 */

static TextChar CellChar(WORD i, WORD j)
{
	register TextChar	ch;

	ch = (horizWindow) ? j*numAcross + i + 0x20 : i*numDown + j + 0x20;
	if (ch >= 0x80)
		ch += 0x20;
	return (ch);
}

/*
 *	Get cell rectangle at given position
 */

static void GetCellRect(WORD i, WORD j, register RectPtr rect)
{
	WORD		width, height;
	GadgetPtr	itemsGadg;

	itemsGadg = GadgetItem(window->FirstGadget, KEY_BOX);
	width = itemsGadg->Width;
	height = itemsGadg->Height;
	rect->MinX = itemsGadg->LeftEdge + (i*width)/numAcross;
	rect->MinY = itemsGadg->TopEdge + (j*height)/numDown;
	rect->MaxX = rect->MinX + width/numAcross - 1;
	rect->MaxY = rect->MinY + height/numDown - 1;
}

/*
 *	Draw specified character at given location into items array
 */

static void DrawCharItem(WORD i, WORD j, BOOL selected)
{
	TextChar	ch;
	WORD		cellWidth, cellHeight, charWidth, charHeight, baseline;
	BOOL		updating;
	RastPtr		rPort = window->RPort;
	LayerPtr	layer = window->WLayer;
	TextFontPtr	font = rPort->Font;
	RegionPtr	clipRgn, oldRgn;
	Rectangle	rect;

	if (i < 0 || i >= numAcross || j < 0 || j >= numDown)
		return;
	ch = CellChar(i, j);
	if (ch == NBSP)
		ch = ' ';
	charWidth = TextLength(rPort, &ch, 1);
	charHeight = font->tf_YSize;
/*
	Clear the cell contents
*/
	GetCellRect(i, j, &rect);
	cellWidth  = rect.MaxX - rect.MinX + 1;
	cellHeight = rect.MaxY - rect.MinY + 1;
	if (ch == DEL) {
		SetAPen(rPort, _tbPenLight);
		FillRect(rPort, &rect);
		DrawShadowBox(rPort, &rect, 0, FALSE);
		return;
	}
	SetAPen(rPort, (selected) ? _tbPenDark : _tbPenLight);
	FillRect(rPort, &rect);
/*
	Display the character, clipping if it is larger than cell rect
*/
	baseline = (cellHeight - charHeight)/2 + font->tf_Baseline;
	if (baseline < font->tf_Baseline)
		baseline = font->tf_Baseline;
	if (baseline >= cellHeight)
		baseline = cellHeight - 1;
	if (baseline - font->tf_Baseline < 0 ||
		baseline - font->tf_Baseline + charHeight >= cellHeight ||
		charWidth >= cellWidth) {
		if ((clipRgn = NewRegion()) != NULL) {
			(void) OrRectRegion(clipRgn, &rect);
			updating = (layer->Flags & LAYERUPDATING);
			if (updating)
				EndUpdate(layer, FALSE);
			oldRgn = InstallClipRegion(layer, clipRgn);
			if (updating)
				BeginUpdate(layer);
		}
	}
	else
		clipRgn = NULL;
	SetAPen(rPort, (selected) ? _tbPenWhite : _tbPenBlack);
	Move(rPort, rect.MinX + (cellWidth - charWidth)/2, rect.MinY + baseline);
	Text(rPort, &ch, 1);
	if (clipRgn) {
		if (updating)
			EndUpdate(layer, FALSE);
		clipRgn = InstallClipRegion(layer, oldRgn);
		if (updating)
			BeginUpdate(layer);
		DisposeRegion(clipRgn);
	}
	DrawShadowBox(rPort, &rect, 0, !selected);
}

/*
 *	Handle rendering of click in character box
 */

void DoBoxClick()
{
	WORD		xPos, yPos, row, column;
	TextChar	ch;
	GadgetPtr	gadget;
	Rectangle	rect;

	if (selColumn != -1 && selRow != -1)
		DrawCharItem(selColumn, selRow, FALSE);
/*
	Find selected cell and highlight it
	(Can't use cellWidth and cellHeight due to possible truncation error)
*/
	gadget = GadgetItem(window->FirstGadget, KEY_BOX);
	xPos = window->MouseX;
	yPos = window->MouseY;
	if ((yPos - gadget->TopEdge) < gadget->Height) {
		column = (xPos - gadget->LeftEdge)*numAcross/gadget->Width;
		row = (yPos - gadget->TopEdge)*numDown/gadget->Height;
		ch = CellChar(column, row);
		if (ch == DEL)						// Not an insertable character
			row = column = -1;
		else {
			GetCellRect(column, row, &rect);
	  		DrawCharItem(column, row, TRUE);
		}
	}
	selColumn = column;
	selRow = row;
}

/*
 *	Handle cursor key
 */

void DoCursorKey(WORD code)
{
	WORD	column, row;
/*
	First cursor key goes to 0,0
*/
	column = (selColumn == -1) ? numAcross + 1 : selColumn;
	row	= (selRow == -1) ? numDown + 1 : selRow;
	if (selColumn != -1 && selRow != -1)
		DrawCharItem(selColumn, selRow, FALSE);
/*
	Loop to skip over DEL char
*/
	do {
		switch (code) {
		case CURSORUP:
			row--;
			break;
		case CURSORDOWN:
			row++;
			break;
		case CURSORLEFT:
			column--;
			break;
		case CURSORRIGHT:
			column++;
			break;
		}
/*
	Make sure still in range wrapping around
*/
		if (column < 0)
			column = numAcross - 1;
		else if (column >= numAcross)
			column = 0;
		if (row < 0)
			row = numDown - 1;
		else if (row >= numDown)
			row = 0;
	} while (CellChar(column, row) == DEL);
/*
	Draw selection
*/
	DrawCharItem(column, row, TRUE);
	selColumn = column;
	selRow = row;
}

TextChar GetCurrentChar()
{
	if (selColumn == -1 || selRow == -1)
		return (DEL);						// Invalid character
	return (CellChar(selColumn, selRow));
}
