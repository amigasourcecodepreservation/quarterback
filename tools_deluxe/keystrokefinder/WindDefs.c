/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	KeyFinder
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Window routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Image.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>

/*
 *	Main dialog window definition
 */

#define WIDTH	15
#define HEIGHT	18

static GadgetTemplate vertWindowGadgets[] = {
	{ GADG_ACTIVE_BORDER,  10,  10, 0, 0, 24*WIDTH, 8*HEIGHT, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  122, -45, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  122, -25, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  260, -25, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ACTIVE_IMAGE, -10-23-4, -45, 0, 0, 23, 20, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  10, -45, 0, 0, 0, 0, 0, 0, 0, 0, "Key sequence:" },
	{ GADG_STAT_TEXT,  10, -25, 0, 0, 0, 0, 0, 0, 0, 0, "Decimal code:" },
	{ GADG_STAT_TEXT, 180, -25, 0, 0, 0, 0, 0, 0, 0, 0, "Hex code:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate vertMainDialog = {
	DLG_TYPE_WINDOW, DLG_FLAG_CLOSE | DLG_FLAG_DEPTH | DLG_FLAG_ZOOM,
	-1, -1, 10 + 24*WIDTH + 10, 10 + 8*HEIGHT + 50, vertWindowGadgets, "Keystroke Finder"
};

static GadgetTemplate horizWindowGadgets[] = {
	{ GADG_ACTIVE_BORDER,  10,  10, 0, 0, 16*WIDTH, 12*HEIGHT, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 270,  25, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 340,  50, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 340,  70, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ACTIVE_IMAGE, -10-23-4, -40, 0, 0, 23, 20, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 260,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Key sequence:" },
	{ GADG_STAT_TEXT, 260,  50, 0, 0, 0, 0, 0, 0, 0, 0, "Dec code:" },
	{ GADG_STAT_TEXT, 260,  70, 0, 0, 0, 0, 0, 0, 0, 0, "Hex code:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate horizMainDialog = {
	DLG_TYPE_WINDOW, DLG_FLAG_CLOSE | DLG_FLAG_DEPTH | DLG_FLAG_ZOOM,
	-1, -1, 10 + 16*WIDTH + 150, 10 + 12*HEIGHT + 10, horizWindowGadgets, "Keystroke Finder"
};

/*
 *  Error dialog
 */

static GadgetTemplate errorGadgets[] = {
	{ GADG_PUSH_BUTTON, -90, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT, 55, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },

	{ GADG_ITEM_NONE }
};

DialogTemplate errorDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 370, 80, errorGadgets, NULL
};

/*
 *  About dialog
 */

static GadgetTemplate aboutGadgets[] = {
	{ GADG_PUSH_BUTTON, 100, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT, 50, 15, 0, 0, 0, 0, 0, 0, 0, 0, "Keystroke Finder 1.0" },
	{ GADG_STAT_TEXT, 42, 40, 0, 0, 0, 0, 0, 0, 0, 0, "Designed and developed" },
	{ GADG_STAT_TEXT, 78, 55, 0, 0, 0, 0, 0, 0, 0, 0, "by Beth Henry" },
	{ GADG_STAT_TEXT, 66, 80, 0, 0, 0, 0, 0, 0, 0, 0, "Copyright \251 1993" },
	{ GADG_STAT_TEXT, 42, 95, 0, 0, 0, 0, 0, 0, 0, 0, "Central Coast Software" },
	{ GADG_STAT_TEXT, 78,110, 0, 0, 0, 0, 0, 0, 0, 0, "A division of" },
	{ GADG_STAT_TEXT, 22,125, 0, 0, 0, 0, 0, 0, 0, 0, "New Horizons Software, Inc."},
	{ GADG_STAT_TEXT, 54,140, 0, 0, 0, 0, 0, 0, 0, 0, "All Rights Reserved" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboutDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 195, aboutGadgets, NULL
};

DlgTemplPtr	dlgList[] = {
	&vertMainDialog,
	&horizMainDialog,
	&errorDialog,
	&aboutDialog
};
