/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	BrainCloud
 *	Copyright (c) 1993 New Horizons Software,
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <dos/dos.h>

#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/intuition.h>

#include <Typedefs.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/DOS.h>

#include <string.h>
#include <stdio.h>

#include "Proto.h"
#include "BrainCloud.h"

/*
 *	External variables
 */

extern BOOL	titleChanged, fromCLI;

extern WindowPtr 	window;
extern MsgPortPtr	mainMsgPort;
extern TextChar		strOn[], strOff[], strUsage[];

extern TextPtr			diskList[];

/*
 *	Local variables and definitions
 */

static BOOL quitFlag;		// Inited to FALSE

#define RAWKEY_HELP	0x5F

/*
 *	Prototypes
 */

static void DoCLIBrainCloud(int, char **);
static BOOL MainDialogFilter(IntuiMsgPtr, WORD *);
static void DoMainDialogItem(WORD);

/*
 *	Main routine
 */

void main(int argc,char *argv[])
{
	WORD					itemHit;
	register IntuiMsgPtr	intuiMsg;

	if (argc > 1) {			// Running from CLI
		fromCLI = TRUE;
		DoCLIBrainCloud(argc,argv);
		return;
	}
	fromCLI = FALSE;
	Init(argc,argv);
/*
	Main event loop
*/
	while (!quitFlag) {
		while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) {
			if (MainDialogFilter(intuiMsg, &itemHit))
				DoMainDialogItem(itemHit);
			else if (DialogSelect(intuiMsg, &window, &itemHit)) {
				ReplyMsg((MsgPtr) intuiMsg);
				DoMainDialogItem(itemHit);
			}
			else
				ReplyMsg((MsgPtr) intuiMsg);
		}
		if (!quitFlag)
			Wait(1L << mainMsgPort->mp_SigBit);
	}
	ShutDown();
}

/*
 *	Handle BrianCloud when run from CLI
 */

static void DoCLIBrainCloud(int argc, char *argv[])
{
	register TextPtr	param1, param2, devName;
	register WORD		i;
	register BOOL		busyOn;

	if (argc < 3)  {
		printf(strUsage);
		return;
	}
/*
	Get parameters
*/
	param1 = argv[1];
	param2 = argv[2];
	for (i = 0; i < strlen(param1); i++)
		param1[i] = toLower[param1[i]];
	for (i = 0; i < strlen(param2); i++)
		param2[i] = toLower[param2[i]];
	if (!strcmp(strOn, param1)) {
		busyOn = TRUE;
		devName = argv[2];
	}
	else if (!strcmp(strOff, param1)) {
		busyOn = FALSE;
		devName = argv[2];
	}
	else if (!strcmp(strOn, param2)) {
		busyOn = TRUE;
		devName = argv[1];
	}
	else if (!strcmp(strOff, param2)) {
		busyOn = FALSE;
		devName = argv[1];
	}
	else {
		printf(strUsage);
		return;
	}
/*
	Do clouding
*/
	DoBrainCloud(devName, busyOn);
}

/*
 *	Main window dialog filter
 */

static BOOL MainDialogFilter(register IntuiMsgPtr intuiMsg,WORD *item)
{
	register ULONG 	class = intuiMsg->Class;
	register UWORD 	code = intuiMsg->Code;

	switch (class) {
	case RAWKEY:
		if (code == RAWKEY_HELP)
			DoHelp();
		break;
	case REFRESHWINDOW:
		RefreshWindow();
		break;
	}
	return (FALSE);
}

/*
 *	Handle gadget down in main window
 */

static void DoMainDialogItem(WORD item)
{
	WORD	diskNum;

	switch (item) {
	case DLG_CLOSE_BOX:
		quitFlag = TRUE;
		break;
	case ON_BUTTON:
	case OFF_BUTTON:
		SetStdPointer(window, POINTER_WAIT);
		diskNum = GetGadgetValue(GadgetItem(window->FirstGadget, DISK_POPUP));
		DoBrainCloud(diskList[diskNum], item == ON_BUTTON);
		SetStdPointer(window, POINTER_ARROW);
		break;
	}
}
