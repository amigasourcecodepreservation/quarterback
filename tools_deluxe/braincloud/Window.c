/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	BrainCloud
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Window routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/intuition.h>
#include <proto/layers.h>
#include <proto/dos.h>
#include <proto/graphics.h>

#include <string.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Image.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Graphics.h>

#include <stdio.h>

#include "BrainCloud.h"
#include "Proto.h"

/*
 *	Global variables
 */

extern WindowPtr	window;
extern MsgPortPtr	mainMsgPort;

extern TextPtr	diskList[];

extern TextChar	screenTitle[];

extern UBYTE	*errMessage[];

extern BOOL	fromCLI, titleChanged;

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

#define ERROR_TEXT 	1

/*
 *	Local prototypes
 */

static void DrawWindow(void);

/*
 *  Create and draw main window
 */

void CreateWindow()
{
	GetDiskList();

	dlgList[DLG_MAIN]->Gadgets[DISK_POPUP].Info = diskList;
	if ((window = GetDialog(dlgList[DLG_MAIN], NULL, mainMsgPort)) == NULL)
		return;

    SetStdPointer(window, POINTER_ARROW);
    SetWindowTitles(window, (UBYTE *) -1, screenTitle);
	ModifyIDCMP(window, window->IDCMPFlags | MENUPICK | DISKINSERTED | DISKREMOVED);

	OutlineButton(GadgetItem(window->FirstGadget, ON_BUTTON), window, NULL, TRUE);
}

/*
 *  Remove main window
 */

void RemoveWindow()
{
	if (window) {
		SetClip(window->WLayer, NULL);
		DisposeDialog(window);
		window = NULL;
	}
}

/*
 *	Draw window contents
 */

static void DrawWindow()
{
	OutlineButton(GadgetItem(window->FirstGadget, OK_BUTTON), window, NULL, TRUE);
}

/*
 *	Refresh window
 */

void RefreshWindow()
{
	RastPtr		rPort;
	Rectangle	rect;

	GetWindowRect(window, &rect);
	if (!EmptyRect(&rect)) {
/*
	If old color scheme, fill with _tbPenLight
	Note: Cannot call RefreshGadgets() within Begin/EndRefresh()
*/
		if (_tbPenLight != 0) {
			rPort = window->RPort;
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &rect);
			RefreshGadgets(GadgetItem(window->FirstGadget, 0), window, NULL);
		}
		BeginRefresh(window);
		DrawWindow();
		EndRefresh(window, TRUE);
	}
}

/*
 *	Return screen titles to default setting
 */

void FixTitle()
{
	SetWindowTitles(window, (TextPtr) -1, screenTitle);
	titleChanged = FALSE;
}

/*
 *  Error report routine
 *  Put error message on screen title bar
 */

void Error(register WORD errNum, TextPtr more)
{
	TextChar	error[256];

	if (errNum > ERR_MAX_ERROR)
		errNum = ERR_UNKNOWN;

	strcpy(error, errMessage[errNum]);
	if (more)
		strcat(error, more);

	if (fromCLI) {
		printf("%s\n", error);
		return;
	}
	dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = error;

	if (StdAlert(dlgList[DLG_ERROR], window->WScreen, mainMsgPort, NULL) == -1) {
		SetWindowTitles(window, (UBYTE *) -1, error);
   		DisplayBeep(window->WScreen);
    	SysBeep(5);
		titleChanged = TRUE;
	}
}

void DoHelp()
{
	DialogPtr dlg;

	if ((dlg = GetDialog(dlgList[DLG_ABOUT], window->WScreen, mainMsgPort)) == NULL)
		Error(ERR_NO_MEM, NULL);
	else {
		OutlineButton(GadgetItem(dlg->FirstGadget,0), dlg, NULL, TRUE);
		(void) ModalDialog(mainMsgPort, dlg, NULL);
		DisposeDialog(dlg);
	}
}
