/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	BrainCloud Definitions
 */

/*
 *	Error definitions
 */

enum {
	INIT_ERR_WINDOW, 	INIT_ERR_PORT
};

enum {
	ERR_UNKNOWN,	ERR_NO_MEM,		ERR_BAD_NAME,	ERR_DEVICE,		ERR_DISK_ACC,
	ERR_NO_DISK,	ERR_MAX_ERROR
};

/*
 *	Window definitions
 */

enum {
	DLG_MAIN,	DLG_ERROR,	DLG_ABOUT
};

enum {
	ON_BUTTON,		OFF_BUTTON, DISK_POPUP
};
