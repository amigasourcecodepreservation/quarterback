/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	BrainCloud
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Cloud routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <devices/trackdisk.h>
#include <dos/dos.h>

#include <proto/exec.h>
#include <proto/dos.h>

#include <clib/alib_protos.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Utility.h>
#include <Toolbox/DOS.h>
#include <Toolbox/Packet.h>

#include <Typedefs.h>

#include <string.h>

#include "BrainCloud.h"
#include "Proto.h"

/*
 *	External variables
 */

extern TextPtr diskList[];

/*
 *	Local variables and definitions
 */

typedef struct FileSysStartupMsg	FileSysStartupMsg, *FSSMsgPtr;
typedef struct IOExtTD				IOExtTD, *IOExtTDPtr;

#define ID_BUSY	(0x42555359L)		// BUSY

static TextPtr	devListNames[] = { "DF0", "DF1", "DF2", "DF3" };
static TextPtr	diskListNames[] = { "DF0:", "DF1:", "DF2:", "DF3:" };
	
static WORD	numDevices;

/*
 *	Local prototypes
 */

static BOOL SetBrainCloud(TextPtr, IOExtTDPtr, BOOL);

static void 		ConvertBSTR(BSTR bstr, TextPtr);
static DosListPtr	FindDosEntry1(DosListPtr, TextPtr, ULONG);

/*
 *	Change first LONG word in boot block
 */

static BOOL SetBrainCloud(TextPtr devName, IOExtTDPtr ioReq, BOOL on)
{
	UBYTE 		*buffer;
	BOOL		success;
	LONG		args[1];
	MsgPortPtr	devPort;

	success = FALSE;
/*
	Allocate buffer for one sector
*/
	if ((buffer = AllocMem(TD_SECTOR, MEMF_CHIP)) != NULL) {
/*
	Inhibit DOS
*/
		args[0] = DOSTRUE;
		if ((devPort = DeviceProc(devName)) != NULL &&
			DoPacket(devPort, ACTION_INHIBIT, args, 1)) {
/*
	Read the first sector and check for BUSY or DOS\0 on disk
*/
			ioReq->iotd_Req.io_Command = CMD_READ;
			ioReq->iotd_Req.io_Offset = 0;
			ioReq->iotd_Req.io_Length = TD_SECTOR;
			ioReq->iotd_Req.io_Data = buffer;
			if (DoIO((IOReqPtr) ioReq) == 0 &&
				(((ULONG *) buffer)[0] == ID_DOS_DISK ||
				 ((ULONG *) buffer)[0] == ID_BUSY)) {
/*
	Change first long word and write back to disk
*/
				((ULONG *) buffer)[0] = (on) ? ID_BUSY : ID_DOS_DISK;
				ioReq->iotd_Req.io_Command = CMD_WRITE;
				if (DoIO((IOReqPtr) ioReq) == 0) {
					ioReq->iotd_Req.io_Command = CMD_UPDATE;
					ioReq->iotd_Req.io_Length = 0;
					success = (DoIO((IOReqPtr) ioReq) == 0);	// Do actual write
				}
			}
			ioReq->iotd_Req.io_Command = TD_MOTOR;		// Turn off the motor
			ioReq->iotd_Req.io_Length = 0;
			(void) DoIO((IOReqPtr) ioReq);
			ioReq->iotd_Req.io_Command = CMD_CLEAR;
			(void) DoIO((IOReqPtr) ioReq);
			args[0] = DOSFALSE;
			(void) DoPacket(devPort, ACTION_INHIBIT, args, 1);
		}
		FreeMem(buffer, TD_SECTOR);
	}
/*
	All done
*/
	return (success);
}

/*
 *	Set or remove BrainCloud from device
 *	Note: Must keep error checks for this procedure if called from CLI
 */

void DoBrainCloud(TextPtr devName, BOOL cloudOn)
{
	WORD		unit;
	MsgPortPtr	replyPort;
	IOExtTDPtr	ioReq;

/*
	Get unit number
*/
	for (unit = 0; unit < NUMUNITS; unit++) {
		if (CmpString(devName, diskListNames[unit], strlen(devName), strlen(diskListNames[unit]), FALSE) == 0)
			break;
	}
	if (unit >= NUMUNITS) {
		Error(ERR_BAD_NAME, devName);
		return;
	}
/*
	Create I/O request
*/
	if ((replyPort = CreatePort(0, 0)) == NULL)
		Error(ERR_NO_MEM, NULL);
	else {
		if ((ioReq = (IOExtTDPtr) CreateExtIO(replyPort, sizeof(IOExtTD))) == NULL)
			Error(ERR_NO_MEM, NULL);
		else {
/*
	Open trackdisk.device
*/
			if (OpenDevice(TD_NAME, unit, (IOReqPtr) ioReq, 0) != 0)
				Error(ERR_DEVICE, TD_NAME);
			else {
/*
	Check for disk in drive, and set cloud
*/
				ioReq->iotd_Req.io_Command = TD_CHANGESTATE;
				if (DoIO((IOReqPtr) ioReq) != 0)
					Error(ERR_DISK_ACC, devName);
				else if (ioReq->iotd_Req.io_Actual != 0)
					Error(ERR_NO_DISK, devName);
				else if (!SetBrainCloud(devName, ioReq, cloudOn))
					Error(ERR_DISK_ACC, devName);
				CloseDevice((IOReqPtr) ioReq);
			}
			DeleteExtIO((IOReqPtr) ioReq);
		}
		DeletePort(replyPort);
	}
}

/*
 *	Convert B-string to C-string
 */

static void ConvertBSTR(BSTR bStr, TextPtr cStr)
{
	register TextPtr	str;
	register UWORD		len;

	str = (TextPtr) BADDR(bStr);
	len = str[0];
	BlockMove(str + 1, cStr, len);
	cStr[len] = '\0';
}

/*
 *	Find entry in DOS list
 *	Note: Currently only searches for DLT_DEVICE entries
 */

static DosListPtr FindDosEntry1(DosListPtr dosList, TextPtr devName, ULONG flags)
{
	WORD		nameLen;
	DosListPtr	dosEntry;
	TextChar	name[30];

	dosEntry = dosList;
	nameLen = strlen(devName);
	while ((dosEntry = NextDosEntry1(dosEntry, flags | LDF_READ)) != NULL) {
		if (dosEntry->dol_Type == DLT_DEVICE) {
			ConvertBSTR(dosEntry->dol_Name, name);
			if (CmpString(devName, name, nameLen, strlen(name), FALSE) == 0)
				return (dosEntry);
		}
	}
	return (NULL);
}

/*
 * 	Build list of DOS trackdisk devices and place in popup
 */

void GetDiskList()
{
	WORD		i;
	DosListPtr	dosEntry;

/*
	Add available devices
*/
	dosEntry = LockDosList1(LDF_DEVICES | LDF_READ);
	numDevices = 0;
	for (i = 0; i < 4; i++) {
		if (FindDosEntry1(dosEntry, devListNames[i], LDF_DEVICES) != NULL)
			diskList[numDevices++] = diskListNames[i];
	}
	diskList[numDevices] = NULL;
	UnLockDosList1(LDF_DEVICES | LDF_READ);
}
