/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	FloppyCopy
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Text strings
 */

#include <exec/types.h>

#include <Typedefs.h>

#include <Toolbox/Language.h>

static TextChar version[] = "$VER: Replicator 1.0";

TextChar screenTitle[] = " Replicator 1.0.1 - \251 1993 Central Coast Software";

TextPtr	initError[] = {
	" Can't open window.",
	" Can't create message port.",
};

TextPtr	errMessage[] = {
	"Not enough memory.",
	"Can't get information on drive\n",
	"Unable to copy source disk.",
	"Unable to access device ",
	"Unknown DOS type.",
	"Unable to copy from ",
	"Unable to load image\n",
	"Unable to save image ",
	"Can't access device: ",
	"Error while copying.",
	"Unable to copy.\nDrives are not of same type.",
	"Read/Write error on drive ",
	"Unknown internal error."
};

TextPtr promptMessage[] = {
	"Please insert source disk.",
	"Please insert destination disks.",
	"Please remove replicated disks\nand insert the next destination\ndisks."
};

TextChar	strWriteProtect[] = "There is a write-protected\ndisk in drive ";
TextChar	strNoDisk[] = "There is no disk in drive ";
TextChar	strReadError[] = "Read/Write error on drive ";

TextChar	strFile[] = "File";
TextChar	strDisks[] = "disks";

TextChar	strSelectFile[] = "Select file to copy from:";
TextChar	strSaveFile[] = "Save image as:";

TextChar	strPass[] = "Pass ";
TextChar	strOf[] = " of ";

TextChar	strQuarterback[] = "Quarterback #";
TextChar	strKickstart[] = "Kickstart";
TextChar	strUnknownName[] = "Unknown";