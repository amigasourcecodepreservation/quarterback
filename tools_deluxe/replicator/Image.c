/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Floppy Copy
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Image procedures
 */

#include <exec/types.h>
#include <dos/filehandler.h>

#include <proto/intuition.h>
#include <proto/exec.h>
#include <proto/icon.h>

#include <Typedefs.h>
#include <string.h>

#include <Toolbox/Request.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/DOS.h>
#include <Toolbox/Packet.h>
#include <Toolbox/Utility.h>

#include "FloppyCopy.h"
#include "Proto.h"

/*
	external variables
 */

extern WindowPtr	window;
extern MsgPortPtr	mainMsgPort;

extern ReqTemplPtr	reqList[];

extern TextPtr		diskList[];

extern ScreenPtr	_tbScreen;

extern TextChar		strSelectFile[], strSaveFile[], strQuarterback[], strKickstart[];
extern TextChar		strUnknownName[];

extern Destination	destination;

extern UBYTE	*currentImage;
extern ULONG	currentImageSize;
extern TextChar	currentImageName[];

extern TextChar		progPathName[];

/*
	local variables and definitions
 */

typedef struct FileInfoBlock		FIB, *FIBPtr;

static RequestPtr	loadReq;
static BOOL			fromDisk;

#define LOAD_BUTTON	OK_BUTTON

enum {
	DISK_RADBTN 	= 2,
	FILE_RADBTN,
	DISK_POPUP,
	FILE_TEXT,
	FILE_BUTTON
};

#define ID_KICK  ((LONG)'K'<<24L | (LONG)'I'<<16L | 'C'<<8 | 'K')

static UBYTE	imageID[] = { 'D', 'I', 'S', 'K', 0x00, 0x33, 0x55, 0x77 };

/*
	image icons
 */

static UWORD imageData[] = {
	0x007F,	0xFFE0,	0x0000,	0x007F,	0xF8E0,	0x0000,	0x007F,	0xF8E0,
    0x0000,	0x007F,	0xF8E0,	0x0000,	0x007F,	0xF8E0,	0x0000,	0x003F,
    0xFFC0,	0x0000,	0x0000,	0x0000,	0x0000,	0x0000,	0x0000,	0x0000,
    0x07FF,	0xFFFC,	0x0000,	0x0FFF,	0xFFFE,	0x0000,	0x0FFF,	0xE1BE,
    0x0000,	0x0FFF,	0xC37E,	0x0000,	0x0FFF,	0x86FE,	0x0000,	0x0FF1,
    0x0DFE,	0x0000,	0x0FF8,	0x1BFE,	0x0000,	0x0FFC,	0x37FE,	0x0000,
    0x0FFF,	0xFFFE,	0x0000,

    0x7F80,	0x001F,	0x8000,	0xFF80,	0x071F,	0xE000,	0xFF80,	0x071F,
    0xE000,	0xFF80,	0x071F,	0xE000,	0xFF80,	0x071F,	0xE000,	0xFFC0,
    0x003F,	0xE000,	0xFFFF,	0xFFFF,	0xE000,	0xFFFF,	0xFFFF,	0xE000,
    0xF800,	0x0003,	0xE000,	0xF000,	0x0001,	0xE000,	0xF000,	0x13C1,
    0xE000,	0xF000,	0x2781,	0xE000,	0xF000,	0x4F01,	0xE000,	0xF00B,
    0x9E01,	0xE000,	0xF005,	0x3C01,	0xE000,	0x9002,	0x7801,	0xE000,
    0xF000,	0x0001,	0xE000,
};

static struct Image imageIconImage = {
	0, 0, 35, 17, 2, &imageData[0], 3, 0, NULL
};

static UWORD image2_0Data[] = {
	0x7F80,	0x001F,	0x8000,	0xFF80,	0x071F,	0xE000,	0xFF80,	0x071F,
    0xE000,	0xFF80,	0x071F,	0xE000,	0xFF80,	0x071F,	0xE000,	0xFFC0,
    0x003F,	0xE000,	0xFFFF,	0xFFFF,	0xE000,	0xFFFF,	0xFFFF,	0xE000,
    0xF800,	0x0003,	0xE000,	0xF000,	0x0001,	0xE000,	0xF000,	0x13C1,
    0xE000,	0xF000,	0x2781,	0xE000,	0xF000,	0x4F01,	0xE000,	0xF00B,
    0x9E01,	0xE000,	0xF005,	0x3C01,	0xE000,	0x9002,	0x7801,	0xE000,
    0xF000,	0x0001,	0xE000,

    0x007F,	0xFFE0,	0x0000,	0x007F,	0xF8E0,	0x0000,	0x007F,	0xF8E0,
    0x0000,	0x007F,	0xF8E0,	0x0000,	0x007F,	0xF8E0,	0x0000,	0x003F,
    0xFFC0,	0x0000,	0x0000,	0x0000,	0x0000,	0x0000,	0x0000,	0x0000,
    0x07FF,	0xFFFC,	0x0000,	0x0FFF,	0xFFFE,	0x0000,	0x0FFF,	0xE1BE,
    0x0000,	0x0FFF,	0xC37E,	0x0000,	0x0FFF,	0x86FE,	0x0000,	0x0FF1,
    0x0DFE,	0x0000,	0x0FF8,	0x1BFE,	0x0000,	0x0FFC,	0x37FE,	0x0000,
    0x0FFF,	0xFFFE,	0x0000,
};

static struct Image image2_0Image = {
	0, 0, 35, 17, 2, &image2_0Data[0], 3, 0, NULL
};

static struct DiskObject imageIcon = {
	WB_DISKMAGIC, WB_DISKVERSION,
		NULL,
		0, 0, 35, 18,
		GADGIMAGE | GADGBACKFILL, GADGIMMEDIATE | RELVERIFY, BOOLGADGET,
		(APTR) &imageIconImage, NULL, NULL, 0, NULL, 0, NULL,
	WBPROJECT, NULL, NULL, NO_ICON_POSITION, NO_ICON_POSITION,
	NULL, NULL, 0
};

static struct DiskObject image2_0Icon = {
	WB_DISKMAGIC, WB_DISKVERSION,
		NULL,
		0, 0, 35, 18,
		GADGIMAGE | GADGBACKFILL, GADGIMMEDIATE | RELVERIFY, BOOLGADGET,
		(APTR) &image2_0Image, NULL, NULL, 0, NULL, 0, NULL,
	WBPROJECT, NULL, NULL, NO_ICON_POSITION, NO_ICON_POSITION,
	NULL, NULL, 0
};

/*
	local prototypes
 */

static UBYTE 	*AllocBuffer(ULONG);
static WORD		FillImage(UBYTE *, ULONG, TextPtr, WORD);
static void 	TranslateID(ULONG , TextPtr);
static BOOL 	LoadDialogFilter(IntuiMsgPtr, WORD *);
static BOOL 	ImageFilter(TextPtr);
static void 	SetLoadImageButtons(RequestPtr	,BOOL);
static void		SaveIcon(TextPtr);

/*
 *	deallocate memory from current image
 */

void FreeCurrentImage()
{
	if (currentImage)
		MemFree(currentImage,currentImageSize);
	currentImageSize = 0;
	currentImage = NULL;
	currentImageName[0] = NULL;
}

/*
 *	allocate memory to hold image
 */

static UBYTE *AllocBuffer(ULONG bufferSize)
{
	UBYTE 	*buffer;

	if ((buffer = MemAlloc(bufferSize,0)) == NULL)
		Error(ERR_NO_MEM,NULL);

	return (buffer);
}

/*
 *	fill buffer with info from source
 */

static WORD FillImage(UBYTE *image, ULONG size, TextPtr source, WORD diskNum)
{
	WORD	error = LOAD_FAIL;
	File	file;

	file = NULL;

	if (diskNum == LOAD_FROMFILE) {
		if ((file = Open(source,MODE_OLDFILE)) == NULL)
			goto Exit;
		Seek(file,8,OFFSET_CURRENT);
		error =  (Read(file,image,size) == size);
		if (error == LOAD_SUCCESS)
			strcpy(currentImageName,source);
	}
	else
		error = CopyDiskToImage(diskList[diskNum],image);

Exit:
	if (file)
		Close(file);

	return (error);
}

/*
 *	load image into current image buffer
 */

WORD GetImage(TextPtr source, WORD diskNum)
{
	ULONG	size;
	UBYTE	*image;
	WORD	error;

	if ((size = GetSize(source,diskNum)) == 0)
		return (LOAD_FAIL);

	if (diskNum == LOAD_FROMFILE)			// subtract image header
		size -= 8;

	FreeCurrentImage();
	if ((image = AllocBuffer(size)) == NULL)
		return (LOAD_FAIL);

	if ((error = FillImage(image,size,source,diskNum)) != LOAD_SUCCESS) {
		MemFree(image,size);
		return (error);
	}

	currentImageSize = size;
	currentImage = image;

	return (LOAD_SUCCESS);
}

/*
 *	translate a LONG value to recognizable text string
 */

static void TranslateID(ULONG id, TextPtr volName)
{
	TextChar	num;
	WORD		len;

	if ((UBYTE)(id >> 24) == 'Q' && (UBYTE)(id >> 16) == 'b') {
		strcpy(volName, strQuarterback);
		len = strlen(volName);
		num = (UBYTE)(id >> 8);
		volName[len++] = num;
		num = (UBYTE)(id);
		volName[len++] = num;
		volName[len] = '\0';
	}
	else if (id == ID_KICK)
		strcpy(volName, strKickstart);
	else
		strcpy(volName,strUnknownName);
}

/*
 *	Get the volume name for a given device
 *	Return TRUE if mounted volume is found
 *	Note: Can't do an Examine() because that returns success even if it doesn't put
 *		  the volume name into the FileInfoBlock (which happens with corrupted root)
 */

BOOL GetVolumeName(TextPtr devName, TextPtr volName)
{
	BOOL				success;
	LONG				args[1];
	struct InfoData		*infoData;
	struct DeviceNode	*devNode;
	FIBPtr				fib;
	Dir					lock;

	if ((infoData = MemAlloc(sizeof(struct InfoData), 0)) == NULL)
		return (FALSE);
	if ((fib = MemAlloc(sizeof(FIB), 0)) == NULL)
		goto Exit;

	success = FALSE;
	volName[0] = '\0';
	args[0] = MKBADDR(infoData);
	(void) LockDosList1(LDF_DEVICES | LDF_READ);
	if (DoPacket(DeviceProc(devName), ACTION_DISK_INFO, args, 1)) {
		if ((devNode = (struct DeviceNode *) BADDR(infoData->id_VolumeNode)) != NULL) {
/*
	NOTE: cannot just use info in volume node to get volume name
	because if 2 volumes with shared icon AmigaDOS might fill the
	volume node with garbage
 */
			lock = Lock(devName, ACCESS_READ);
			if (Examine(lock, fib))
				strcpy(volName, fib->fib_FileName);
			UnLock(lock);
		}
		else
			TranslateID(infoData->id_DiskType,volName);
		success = TRUE;
	}
	UnLockDosList1(LDF_DEVICES | LDF_READ);
Exit:
	if (infoData)
		MemFree(infoData, sizeof(struct InfoData));
	if (fib)
		MemFree(fib, sizeof(FIB));

	return(success);
}

/*
 *	filter for message for load requester
 */

static BOOL LoadDialogFilter(register IntuiMsgPtr intuiMsg, register WORD *item)
{
	register ULONG	class = intuiMsg->Class;
	register WORD	gadgNum;

	if (intuiMsg->IDCMPWindow == window) {
		switch (class) {
		case GADGETDOWN:
		case GADGETUP:
			gadgNum = GadgetNumber(intuiMsg->IAddress);
			if (gadgNum == FILE_TEXT) {
				SetLoadImageButtons(loadReq,FALSE);
				fromDisk = FALSE;
			}
			else if (gadgNum == DISK_POPUP) {
				SetLoadImageButtons(loadReq,TRUE);
				fromDisk = TRUE;
			}
			break;
		}
	}

	return (DialogFilter(intuiMsg,item));
}

/*
 *	file filter for images
 */

static BOOL ImageFilter(TextPtr fileName)
{
	File 	file;
	UBYTE	buffer[8];
	BOOL	success = FALSE;

	if ((file = Open(fileName, MODE_OLDFILE)) == NULL)
		return (FALSE);

	if (Read(file,&buffer,8) == 8 && !strcmp(buffer,imageID))
		success = TRUE;
	Close(file);

	return (success);
}

/*
 *	set buttons for load image requester
 */

static void SetLoadImageButtons(RequestPtr	req,BOOL fromDisk)
{
	GadgetPtr	gadgList = req->ReqGadget;

	SetGadgetItemValue(gadgList, DISK_RADBTN, window, req, fromDisk);
	SetGadgetItemValue(gadgList, FILE_RADBTN, window, req, !fromDisk);
}

/*
 *	open load image requester and get new image
 */

BOOL DoLoadImage(TextPtr imageName)
{
	GadgetPtr	gadgList;
	WORD		item, diskSelected, error;
	BOOL		done;
	SFReply		sfReply;
	TextChar	sourceName[GADG_MAX_STRING];
	Dir			dir;

	reqList[REQ_LOAD]->Gadgets[DISK_POPUP].Info = diskList;

	AutoActivateEnable(FALSE);
	if ((loadReq = GetRequest(reqList[REQ_LOAD], window, TRUE)) == NULL) {
		Error(ERR_NO_MEM,NULL);
		AutoActivateEnable(TRUE);
		return (FALSE);
	}
	AutoActivateEnable(TRUE);
	gadgList = loadReq->ReqGadget;
	OutlineButton(GadgetItem(gadgList, LOAD_BUTTON), window, loadReq, TRUE);

	SetLoadImageButtons(loadReq, TRUE);

	fromDisk = TRUE;
	done = FALSE;
	do {
		WaitPort(mainMsgPort);
		item = CheckRequest(mainMsgPort, window, LoadDialogFilter);
		switch (item) {
		case LOAD_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case DISK_RADBTN:
		case FILE_RADBTN:
			fromDisk = (item == DISK_RADBTN);
			break;
		case FILE_BUTTON:
			fromDisk = FALSE;
			SFPGetFile(_tbScreen, mainMsgPort, strSelectFile,
				   NULL, NULL, NULL, 0, NULL,ImageFilter, &sfReply);
			if (sfReply.Result == SFP_OK) {
				NameFromLock1(sfReply.DirLock,sourceName,GADG_MAX_STRING);
				if (sourceName[strlen(sourceName) - 1] != ':')
					strcat(sourceName,"/");
				strcat(sourceName,sfReply.Name);
				SetEditItemText(gadgList,FILE_TEXT,window,loadReq,sourceName);
			}
		}
		if (!done)
			SetLoadImageButtons(loadReq, fromDisk);
	} while (!done);

	diskSelected = (fromDisk) ? GetGadgetValue(GadgetItem(gadgList,DISK_POPUP))
								: LOAD_FROMFILE;
	if (!fromDisk)
		GetEditItemText(gadgList,FILE_TEXT,imageName);

	EndRequest(loadReq, window);
	DisposeRequest(loadReq);

	if (item == CANCEL_BUTTON)
		return (FALSE);

	error = LOAD_SUCCESS;

	if (fromDisk) {
		if (!PromptDisk(PROMPT_SOURCE)) {
			imageName[0] = '\0';
			return (FALSE);
		}
		else
			GetVolumeName(diskList[diskSelected],imageName);
	}
	else {
		if ((dir = ConvertFileName(imageName)) == NULL)
			error = LOAD_FAIL;
		else {
			SetStdPointer(window,POINTER_WAIT);
			SetCurrentDir(dir);
			UnLock(dir);
		}
	}

	if (error == LOAD_SUCCESS)
		error = GetImage(imageName,diskSelected);
	if (error == LOAD_FAIL)
		Error(ERR_CANT_LOAD,imageName);

	SetStdPointer(window,POINTER_ARROW);

	strcpy(imageName, currentImageName);
	return (error == LOAD_SUCCESS);
}

/*
 *	Save icon with same name as imagename
 */

static void SaveIcon(TextPtr imageName)
{
	register struct DiskObject *icon, *oldIcon;
/*
	Set up icon information
*/
	if (SystemVersion() < OSVERSION_2_0)
		icon = &imageIcon;
	else
		icon = &image2_0Icon;

	icon->do_DefaultTool = progPathName;
/*
	Set icon position to that of previous icon (if it exists)
*/
	if (oldIcon = GetDiskObject(imageName)) {
		icon->do_CurrentX = oldIcon->do_CurrentX;
		icon->do_CurrentY = oldIcon->do_CurrentY;
		FreeDiskObject(oldIcon);
	}
	else
		icon->do_CurrentX = icon->do_CurrentY = NO_ICON_POSITION;
/*
	Save the icon
*/
	PutDiskObject(imageName, icon);
}

/*
 *	get name and save image currently in memory
 */

BOOL DoSaveImage(TextPtr imageName)
{
	SFReply		sfReply;
	File		dstFile;
	BOOL		success;

	SFPPutFile(_tbScreen, mainMsgPort, strSaveFile, imageName, NULL,
					NULL, NULL, &sfReply);

	if (sfReply.Result != SFP_OK)
		return (FALSE);

	SetStdPointer(window,POINTER_WAIT);

	success = FALSE;
	SetCurrentDir(sfReply.DirLock);
	if ((dstFile = Open(sfReply.Name,MODE_NEWFILE)) == NULL)
		goto Exit;
/*
	copy file contents
*/
	if (Write(dstFile, imageID, 8) < 8)
		goto Exit;

	success = (Write(dstFile, currentImage, currentImageSize) == currentImageSize);

Exit:
	if (!success)
		Error(ERR_CANT_SAVE,sfReply.Name);
	else
		strcpy(currentImageName, sfReply.Name);

	SetStdPointer(window,POINTER_ARROW);
	if (dstFile)
		Close(dstFile);

	strcpy(imageName, currentImageName);

	if (success)
		SaveIcon(imageName);
	return (success);
}