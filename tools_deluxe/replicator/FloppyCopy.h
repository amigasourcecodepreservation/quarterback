/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	FloppyCopy Definitions
 */

/*
 *	Dialog and requester definitions
 */

enum {
	DLG_MAIN, 	DLG_ERROR,		DLG_DISKERROR,	DLG_READERROR,
	DLG_ABOUT,	DLG_PROMPT,		DLG_REMOVEPROMPT
};

enum {
	REQ_STATREAD,	REQ_STATWRITE,	REQ_LOAD,	REQ_OPTIONS
};

/*
 *	Error definitions
 */

enum {
	INIT_ERR_WINDOW, 	INIT_ERR_PORT
};

enum {
	ERR_NO_MEM,
	ERR_NO_INFO,
	ERR_BAD_DOS,
	ERR_DEV_ACC,
	ERR_OPEN_DEST,
	ERR_DIFF_DEST,
	ERR_CANT_LOAD,
	ERR_CANT_SAVE,
	ERR_BAD_NAME,
	ERR_COPYING,
	ERR_COMPAT,
	ERR_READ,
	ERR_UNKNOWN,
	ERR_MAX_ERROR
};

#define PROMPT_REMOVE	-1

enum {
	PROMPT_SOURCE,
	PROMPT_FIRSTDEST,
	PROMPT_NEXTDEST
};

/*
 *	Window definitions
 */

enum {
	LOADIMGE_BUTTON,
	SAVEIMGE_BUTTON,
	START_BUTTON,
	OPTIONS_BUTTON,
	NUMBER_RADBTN,
	UNLTD_RADBTN,
	DF0_BOX,
	DF1_BOX,
	DF2_BOX,
	DF3_BOX,
	PASSUP_ARROW,
	PASSDOWN_ARROW,
	PASS_TEXT,
	IMAGE_TEXT,
};

#define MAX_FILENAME_LEN 512

#define MAX_PASSES	100
#define MIN_PASSES	1

/*
 *	status definitions
 */

#define LOAD_ABORT		-1
#define LOAD_FAIL		0
#define LOAD_SUCCESS	1

#define COPY_ABORT		-1
#define COPY_FAIL		0
#define COPY_SUCCESS	1

#define LOAD_FROMFILE	-1

/*
 *	Macros
 */

#define MAX(a,b)	((a)>(b)?(a):(b))
#define MIN(a,b)	((a)<(b)?(a):(b))

/*
	dostype definitions
 */

#define ID_DOS0		(0x444F5300L)	// 'DOS\0'
#define ID_DOS1		(0x444F5301L)
#define ID_DOS2		(0x444F5302L)
#define ID_DOS3		(0x444F5303L)
#define ID_DOS4		(0x444F5304L)
#define ID_DOS5		(0x444F5305L)

/*
	destination structure
 */

typedef struct {
	UBYTE		FloppyDrives[4];
	BOOL		Unlimited;
	WORD		NumPasses;
} Destination, *DestPtr;

/*
	Options structure
 */

typedef struct {
	BOOL	Verify;
	BOOL	UniqueCopy;
	BOOL	AllTracks;
	ULONG	StartTrack;
	ULONG	EndTrack;
} Options,	*OptionPtr;