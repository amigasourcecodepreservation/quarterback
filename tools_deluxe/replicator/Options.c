/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Floppy Copy
 *
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Options routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Request.h>

#include "FloppyCopy.h"
#include "Proto.h"

/*
 *  External variables
 */

extern WindowPtr	window;

extern MsgPortPtr mainMsgPort;

extern ReqTemplPtr reqList[];

extern Options	options;

/*
 	Local variables and definitions
 */

static RequestPtr	optionsReq;

enum {
	VERIFY_BOX = 2,
	UNIQUE_BOX,
	FROM_TEXT,
	TO_TEXT,
	ALL_RADBTN,
	FROM_RADBTN
};

/*
	local prototypes
 */

static BOOL MatchOptions(TextPtr, TextPtr, TextPtr);
static void ByteToHexString(UBYTE, TextPtr);
static void	SetOptionsDisplay(RequestPtr, Options *);

/*
 *	Match screen options
 */

static BOOL MatchOptions(register TextPtr text, TextPtr opt1, TextPtr opt2)
{
	register WORD	len;

/*
	Get command length
*/
	for (len = 0; text[len] && text[len] != ':' && text[len] != '='; len++) ;
	if (text[len])
		len++;
/*
	Check for match
*/
	return ((BOOL) (CmpString(text, opt1, len, strlen(opt1), FALSE) == 0 ||
					CmpString(text, opt2, len, strlen(opt2), FALSE) == 0));
}


/*
 *	Set appropriate option from string
 */

void SetOption(TextPtr text)
{
	if (MatchOptions(text, "noverify", "nv")) {
		options.Verify = FALSE;
		return;
	}
	if (MatchOptions(text, "clone", "c")) {
		options.UniqueCopy = FALSE;
		return;
	}
}

/*
 *	Hex character array.
 */

static UBYTE hexChars[] = {
	'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'
};

/*
 * Convert an unsigned longword into a hex string eight(+1) characters long.
 * Terminate with a space, but no null-terminator needed.
 */

static void ByteToHexString(register UBYTE val, register TextPtr buff)
{
	buff[0] = '$';
	buff[2] = hexChars[val & 0xF];
	val >>=	4;
	buff[1] = hexChars[val & 0xF];
	buff[3] = '\0';
}

/*
 *	Init option structure
 */

void InitOptions()
{
	options.Verify = TRUE;
	options.AllTracks = TRUE;
	options.StartTrack = 0;
	options.EndTrack = 79;
	options.UniqueCopy = TRUE;
}

/*
 *	Options dialog filter
 */

static BOOL OptionsDialogFilter(register IntuiMsgPtr intuiMsg, register WORD *item)
{
	register ULONG	class = intuiMsg->Class;
	register WORD	gadgNum;

	if (intuiMsg->IDCMPWindow == window) {
		switch (class) {
		case GADGETDOWN:
		case GADGETUP:
			gadgNum = GadgetNumber(intuiMsg->IAddress);
			if (class == GADGETDOWN && (gadgNum == FROM_TEXT || gadgNum == TO_TEXT)) {
				ReplyMsg((MsgPtr) intuiMsg);
				*item = FROM_RADBTN;
				return (TRUE);
			}
			break;
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Set all items in options requester to correct values
 */

static void SetOptionsDisplay(RequestPtr req, Options *opts)
{
	GadgetPtr gadgList = req->ReqGadget;

	SetGadgetItemValue(gadgList,VERIFY_BOX, window, req, opts->Verify);
	SetGadgetItemValue(gadgList,UNIQUE_BOX, window, req, opts->UniqueCopy);
	SetGadgetItemValue(gadgList,ALL_RADBTN, window, req, opts->AllTracks);
	SetGadgetItemValue(gadgList,FROM_RADBTN, window, req, !opts->AllTracks);
}

/*
 *	Display and handle options requester
 */

void DoOptions()
{
	GadgetPtr	gadgList;
	BOOL		done;
	WORD		item;
	Options		opts;
	TextChar	num[4];

	opts = options;			// Save local copy to work on
/*
	Bring up request
*/
	AutoActivateEnable(FALSE);
	if ((optionsReq = GetRequest(reqList[REQ_OPTIONS], window, TRUE)) == NULL) {
		Error(ERR_NO_MEM, NULL);
		AutoActivateEnable(TRUE);
		return;
	}
	AutoActivateEnable(TRUE);
	gadgList = optionsReq->ReqGadget;
	OutlineButton(GadgetItem(gadgList, OK_BUTTON), window, optionsReq, TRUE);
	SetOptionsDisplay(optionsReq, &opts);
	NumToString(opts.StartTrack,num);
	SetEditItemText(gadgList, FROM_TEXT, window, optionsReq, num);
	NumToString(opts.EndTrack,num);
	SetEditItemText(gadgList, TO_TEXT, window, optionsReq, num);
	if (!opts.AllTracks)
		ActivateGadget(GadgetItem(gadgList, FROM_TEXT), window, optionsReq);

/*
	Handle requester
*/
	done = FALSE;
	do {
		WaitPort(mainMsgPort);
		item = CheckRequest(mainMsgPort, window, OptionsDialogFilter);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case VERIFY_BOX:
			opts.Verify = !opts.Verify;
			break;
		case UNIQUE_BOX:
			opts.UniqueCopy = !opts.UniqueCopy;
			break;
		case FROM_RADBTN:
		case ALL_RADBTN:
			opts.AllTracks = (item == ALL_RADBTN);
			break;
		}
		if (!done)
			SetOptionsDisplay(optionsReq,&opts);
	} while (!done);

	GetEditItemText(gadgList, FROM_TEXT, num);
	opts.StartTrack = StringToNum(num);
	GetEditItemText(gadgList, TO_TEXT, num);
	opts.EndTrack = StringToNum(num);

	EndRequest(optionsReq, window);
	DisposeRequest(optionsReq);
	if (item == CANCEL_BUTTON)
		return;
/*
	Save new values
*/
	options = opts;
}
