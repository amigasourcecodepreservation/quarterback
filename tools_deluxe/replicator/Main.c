/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Floppy Copy
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Main procedure
 */

#include <proto/exec.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>
#include <Toolbox/StdFile.h>

#include "FloppyCopy.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr 		window;
extern MsgPortPtr		mainMsgPort;
extern BOOL 			titleChanged;

extern Destination		destination;

extern TextPtr			diskList[];

extern WORD				defaultButton;

/*
 *	Local variables and definitions
 */

static BOOL 	quitFlag;

static WORD		delay, inc;

#define RAWKEY_HELP	0x5F

/*
 *	Prototypes
 */

static BOOL	MainDialogFilter(IntuiMsgPtr, WORD *);
static void ChangePasses(WORD);
static void DoMainDialogItem(WORD);

/*
 *	Main routine
 */

void main(int argc,char *argv[])
{
	register IntuiMsgPtr	intuiMsg;
	WORD					itemHit;

	Init(argc, argv);
	SetUp(argc, argv);

	quitFlag = FALSE;
	while (!quitFlag) {
		while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) {
		if (MainDialogFilter(intuiMsg, &itemHit))
				DoMainDialogItem(itemHit);
			else if (DialogSelect(intuiMsg, &window, &itemHit)) {
				ReplyMsg((MsgPtr) intuiMsg);
				DoMainDialogItem(itemHit);
			}
			else
				ReplyMsg((MsgPtr) intuiMsg);
		}
		if (!quitFlag)
			Wait(1L << mainMsgPort->mp_SigBit);
	}

	ShutDown();
}

/*
 *	Handle raw intuiMsgs's in main window
 */

static BOOL MainDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register ULONG 	class = intuiMsg->Class;
	register UWORD 	code = intuiMsg->Code;
	register BOOL	msgReply = FALSE;
	register WORD	gadgNum;
	GadgetPtr		gadgList = window->FirstGadget;

	switch (class) {
	case INTUITICKS:
		if (delay > 0)
			delay--;
		else {
			if (delay > -9) {
				delay--;
				inc = 1;
			}
			else
				inc = 5;
			if (GadgetSelected(gadgList, PASSUP_ARROW))
				ChangePasses(inc);
			else if (GadgetSelected(gadgList, PASSDOWN_ARROW))
				ChangePasses((WORD) -inc);
		}
		break;
	case GADGETDOWN:
	case GADGETUP:
		gadgNum = GadgetNumber(intuiMsg->IAddress);
		if (gadgNum == PASSUP_ARROW || gadgNum == PASSDOWN_ARROW) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? gadgNum : -1;
			msgReply = TRUE;
		}
		break;
	case RAWKEY:
		if (code == CURSORUP || code == CURSORDOWN) {
			ReplyMsg((MsgPtr) intuiMsg);
			gadgNum = (code == CURSORUP) ? PASSUP_ARROW : PASSDOWN_ARROW;
			if (DepressGadget(GadgetItem(gadgList, gadgNum), window, NULL))
				*item = gadgNum;
			msgReply = TRUE;
		}
		else if (code == RAWKEY_HELP) {
			ReplyMsg((MsgPtr) intuiMsg);
			msgReply = TRUE;
			DoHelp();
		}
		break;
	case REFRESHWINDOW:
		ReplyMsg((MsgPtr) intuiMsg);
		msgReply = TRUE;
		*item = -1;
		RefreshWindow();
		break;
	}
	return (msgReply);
}

/*
 *	Change wipe value in options requester
 */

static void ChangePasses(WORD inc)
{
	GadgetPtr	gadgList = window->FirstGadget;
	WORD		passes = destination.NumPasses;

	passes += inc;
	if (passes >= MAX_PASSES)
		passes = MAX_PASSES;
	if (passes <= MIN_PASSES)
		passes = MIN_PASSES;
/*
	Display new pass number and number of disks
*/
	destination.NumPasses = passes;
	SetDiskNums();
/*
	Do enable/disable after displaying new passes, since these will be blocked
		if gadgets are selected
*/
	EnableGadgetItem(gadgList, PASSUP_ARROW, window, NULL, passes < MAX_PASSES);
	EnableGadgetItem(gadgList, PASSDOWN_ARROW, window, NULL, passes > MIN_PASSES);
}

/*
 *	Handle gadget click in main window
 */

static void DoMainDialogItem(WORD item)
{
	GadgetPtr	gadgList = GadgetItem(window->FirstGadget,0);
	TextChar	imageName[MAX_FILENAME_LEN];
	BOOL		success;

	SetDefaultButtons(OK_BUTTON, -1);
	switch (item) {
	case DLG_CLOSE_BOX:
		quitFlag = TRUE;
		break;
	case LOADIMGE_BUTTON:
		GetGadgetItemText(gadgList, IMAGE_TEXT, imageName);
		success = DoLoadImage(imageName);
		SetGadgetItemText(gadgList, IMAGE_TEXT, window, NULL, imageName);
		if (success) {
			OutlineButton(GadgetItem(gadgList, OK_BUTTON), window, NULL, FALSE);
			defaultButton = START_BUTTON;
			SetButtons();
		}
		break;
	case SAVEIMGE_BUTTON:
		GetGadgetItemText(gadgList, IMAGE_TEXT, imageName);
		if (DoSaveImage(imageName))
			SetGadgetItemText(gadgList, IMAGE_TEXT, window, NULL, imageName);;
		break;
	case START_BUTTON:
		(void)DoCopy();
		break;
	case OPTIONS_BUTTON:
		DoOptions();
		break;
	case DF0_BOX:
	case DF1_BOX:
	case DF2_BOX:
	case DF3_BOX:
		destination.FloppyDrives[item - DF0_BOX] =
							!destination.FloppyDrives[item - DF0_BOX];
		SetButtons();
		break;
	case PASSUP_ARROW:
	case PASSDOWN_ARROW:
		ChangePasses((item == PASSUP_ARROW) ? +1 : -1);
		delay = 5;
		break;
	case NUMBER_RADBTN:
	case UNLTD_RADBTN:
		destination.Unlimited = (item == UNLTD_RADBTN);
		SetButtons();
		break;
	}
	SetDefaultButtons(defaultButton, -1);
}

BOOL DestinationSelected()
{
	WORD		i;

	for (i=0; i<4; i++)
		if (destination.FloppyDrives[i])
			return (TRUE);

	return (FALSE);
}
