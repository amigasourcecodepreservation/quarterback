/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Floppy Copy
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Copy procedures
 */

#include <dos/dos.h>

#include <proto/exec.h>
#include <proto/dos.h>

#include <clib/alib_protos.h>

#include <devices/trackdisk.h>

#include <Typedefs.h>
#include <stdio.h>
#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/DOS.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Packet.h>

#include "FloppyCopy.h"
#include "Proto.h"

/*
 *	External variables
 */

extern Destination 	destination;

extern Options		options;

extern TextPtr		diskList[];

extern UBYTE		*currentImage;
extern ULONG		currentImageSize;
extern TextChar		currentImageName[];

extern TextPtr	diskListNames[];
extern TextPtr	devListNames[];

/*
 *	Local definitions
 */

typedef struct DriveGeometry	DriveGeo, *DriveGeoPtr;
typedef struct FileInfoBlock	FIB, *FIBPtr;
typedef struct IOExtTD			IOExtTD, *IOExtTDPtr;

#define CHECKSUM_INDEX	5		// LONG index

/*
 *	Local prototypes
 */

static BOOL		GetDriveGeometry(TextPtr, DriveGeoPtr);
static BOOL		CompatableDriveGeo(DriveGeo, DriveGeo);

static ULONG	BlockSize(DriveGeoPtr);
static ULONG	TotalBlocks(DriveGeoPtr);
static ULONG	TrackSize(DriveGeoPtr);
static ULONG	TotalTracks(DriveGeoPtr);
static ULONG	RootLocation(DriveGeoPtr);

static void 	SetDiskBusy(MsgPortPtr, BOOL);
static void 	MotorOff(IOExtTDPtr);
static void		WaitAllIO(IOExtTDPtr *);
static void		ClearTrack(IOExtTDPtr);
static WORD		CanUseDisk(IOExtTDPtr);

static BOOL		ReadTrack(IOExtTDPtr, UBYTE *, ULONG, DriveGeoPtr);
static BOOL		WriteTrack(IOExtTDPtr, UBYTE *, ULONG, DriveGeoPtr);
static BOOL		ReadBlock(IOExtTDPtr, UBYTE *, ULONG, DriveGeoPtr);
static BOOL		WriteBlock(IOExtTDPtr, UBYTE *, ULONG, DriveGeoPtr);

static WORD		DeviceUnit(TextPtr);
static BOOL 	SetUpDevice(TextPtr, DriveGeoPtr, ULONG *);
static BOOL		CompareTracks(ULONG *, ULONG *, ULONG);
static ULONG	CalcChecksum(ULONG *, DriveGeo);
static BOOL		PutDateStamp(IOExtTDPtr , DriveGeo);
static void		WriteBAD(IOExtTDPtr , DriveGeo);

/*
 *	initialize values to destination
 */

void InitDestination()
{
	destination.FloppyDrives[0] = InDiskList("DF0:");
	destination.FloppyDrives[1] = InDiskList("DF1:");
	destination.FloppyDrives[2] = InDiskList("DF2:");
	destination.FloppyDrives[3] = InDiskList("DF3:");
	destination.NumPasses = 1;
	destination.Unlimited = FALSE;
}

/*
 *	Convert B-string to C-string
 */

void ConvertBSTR(BSTR bStr, TextPtr cStr)
{
	register TextPtr	str;
	register UWORD		len;

	str = (TextPtr) BADDR(bStr);
	len = str[0];
	BlockMove(str + 1, cStr, len);
	cStr[len] = '\0';
}

/*
 *	Get Geometry for drive devName
 *	Note:	devname is alway "DFx"
 */

static BOOL GetDriveGeometry(TextPtr devName, DriveGeoPtr driveGeo)
{
	IOExtTDPtr	ioReq;
	MsgPortPtr	ioMsgPort;
	BOOL		success;
	ULONG		unit;

	success = FALSE;
	if ((unit = (ULONG) DeviceUnit(devName)) == -1)
		return (FALSE);
/*
	Try TD_GETGEOMETRY command first
*/
	if ((ioMsgPort = CreatePort(0, 0)) != NULL) {
		if ((ioReq = (IOExtTDPtr) CreateExtIO(ioMsgPort, sizeof(IOExtTD))) != NULL) {
			if (!OpenDevice(TD_NAME, unit, ioReq, TDF_ALLOW_NON_3_5)) {
				ioReq->iotd_Req.io_Data = driveGeo;
				ioReq->iotd_Req.io_Command = TD_GETGEOMETRY;
				ioReq->iotd_Req.io_Message.mn_ReplyPort = ioMsgPort;
				success = (DoIO((IOReqPtr) ioReq) == 0);
/*
	If failure, try TD_GETNUMTRACKS command
		(heads*cylinders makes total tracks return correct value)
*/
				if (!success) {
					ioReq->iotd_Req.io_Command = TD_GETNUMTRACKS;
					success = (DoIO((IOReqPtr) ioReq) == 0);
					if (success) {
						driveGeo->dg_Cylinders = ioReq->iotd_Req.io_Actual;
						driveGeo->dg_Heads = 1;
						driveGeo->dg_SectorSize = TD_SECTOR;
						driveGeo->dg_TotalSectors = driveGeo->dg_Cylinders * NUMSECS;
						driveGeo->dg_CylSectors = NUMSECS;
						driveGeo->dg_TrackSectors = NUMSECS;
						driveGeo->dg_Reserved = 0;
					}
				}
				CloseDevice(ioReq);
			}
			DeleteExtIO((IOReqPtr) ioReq);
		}
		DeletePort(ioMsgPort);
	}
/*
	All done
*/
	return (success);
}

/*
 *	Return TRUE if driveGeoA is compatable with driveGeoB
 */

static BOOL CompatableDriveGeo(DriveGeo driveGeoA, DriveGeo driveGeoB)
{
	if (driveGeoA.dg_SectorSize		!= driveGeoB.dg_SectorSize		||
		driveGeoA.dg_TotalSectors	!= driveGeoB.dg_TotalSectors	||
		driveGeoA.dg_Cylinders		!= driveGeoB.dg_Cylinders		||
		driveGeoA.dg_CylSectors		!= driveGeoB.dg_CylSectors		||
		driveGeoA.dg_Heads			!= driveGeoB.dg_Heads			||
		driveGeoA.dg_TrackSectors	!= driveGeoB.dg_TrackSectors)
		return (FALSE);
	if (currentImageSize != TotalTracks(&driveGeoA)*TrackSize(&driveGeoA))
		return (FALSE);

	return (TRUE);
}

/*
 *	Return block size of volume in bytes
 */

static ULONG BlockSize(DriveGeoPtr driveGeo)
{
	return (driveGeo->dg_SectorSize);
}

/*
 *	Return total number of blocks in volume
 */

static ULONG TotalBlocks(register DriveGeoPtr driveGeo)
{
	return (driveGeo->dg_TotalSectors);
}

/*
 *	Return block size of volume in bytes
 */

static ULONG TrackSize(DriveGeoPtr driveGeo)
{
	return (driveGeo->dg_SectorSize*driveGeo->dg_TrackSectors);
}

/*
 *	Return total number of blocks in volume
 */

static ULONG TotalTracks(register DriveGeoPtr driveGeo)
{
	return (driveGeo->dg_Cylinders*driveGeo->dg_Heads);
}

/*
 *	Return root location of volume, relative to start of volume
 */

static ULONG RootLocation(DriveGeoPtr driveGeo)
{
	return ((TotalBlocks(driveGeo) + driveGeo->dg_Reserved) >> 1);
}

/*
 *	Set disk to busy or free
 */

static void SetDiskBusy(MsgPortPtr procID, BOOL busy)
{
	LONG	args[1];

	args[0] = (busy) ? DOSTRUE: DOSFALSE;
	DoPacket(procID, ACTION_INHIBIT, args, 1);
}

/*
 *	Turn drive motor off
 */

static void MotorOff(IOExtTDPtr ioReq)
{
	ioReq->iotd_Req.io_Command = CMD_UPDATE;
	ioReq->iotd_Req.io_Length = 0;
	DoIO((IOReqPtr) ioReq);
	ioReq->iotd_Req.io_Command = TD_MOTOR;		// Turn off the motor
	DoIO((IOReqPtr) ioReq);
	ioReq->iotd_Req.io_Command = CMD_CLEAR;
	DoIO((IOReqPtr) ioReq);
}

/*
 *	Update and clear track buffer
 *	Note:  Apparently we don't need to do this when using TD_FORMAT, but it makes
 *		virtually no difference in speed to leave it in
 */

static void ClearTrack(IOExtTDPtr ioReq)
{
	ioReq->iotd_Req.io_Command = CMD_UPDATE;
	ioReq->iotd_Req.io_Length = 0;
	DoIO((IOReqPtr) ioReq);
	ioReq->iotd_Req.io_Command = CMD_CLEAR;
	ioReq->iotd_Req.io_Length = 0;
	DoIO((IOReqPtr) ioReq);
}

/*
 *	Clear buffer for drive
 */

static WORD	CanUseDisk(IOExtTDPtr ioReq)
{
	ioReq->iotd_Req.io_Flags = IOF_QUICK;
	ioReq->iotd_Req.io_Length = 0;
	ioReq->iotd_Req.io_Command = TD_CHANGESTATE;
	DoIO((IOReqPtr) ioReq);
	if (ioReq->iotd_Req.io_Actual)
		return (TDERR_DiskChanged);

	ioReq->iotd_Req.io_Command = TD_PROTSTATUS;
	DoIO((IOReqPtr) ioReq);
	if (ioReq->iotd_Req.io_Actual)
		return (TDERR_WriteProt);

	return (0);
}

/*
 * 	Wait for all io to finish for all drives being used
 */

static void WaitAllIO(IOExtTDPtr ioReq[])
{
	WORD	driveNum;

	for (driveNum = 0; driveNum < NUMUNITS; driveNum++) {
		if (destination.FloppyDrives[driveNum])
			WaitIO(ioReq[driveNum]);
	}
}

/*
 *	Read a single track from drive
 */

static BOOL ReadTrack(IOExtTDPtr ioReq, UBYTE *buff, ULONG trackNum,
						DriveGeoPtr driveGeo)
{
	LONG	trackSize = TrackSize(driveGeo);

	ioReq->iotd_Req.io_Command	= CMD_READ;
	ioReq->iotd_Req.io_Offset	= trackNum*trackSize;
	ioReq->iotd_Req.io_Data		= buff;
	ioReq->iotd_Req.io_Length	= trackSize;
	return (DoIO((IOReqPtr) ioReq) == 0);
}

/*
 *  Write a single track to drive
 *	Does not wait for completion (always returns TRUE)
 */

static BOOL WriteTrack(IOExtTDPtr ioReq, UBYTE *buff, ULONG trackNum,
							DriveGeoPtr driveGeo)
{
	LONG	trackSize = TrackSize(driveGeo);

	ioReq->iotd_Req.io_Command	= TD_FORMAT;
	ioReq->iotd_Req.io_Offset	= trackNum*trackSize;
	ioReq->iotd_Req.io_Data		= buff;
	ioReq->iotd_Req.io_Length	= trackSize;
	SendIO((IOReqPtr) ioReq);
	return (TRUE);
}

/*
 *	Read a single block from drive
 */

static BOOL ReadBlock(IOExtTDPtr ioReq, UBYTE *buff, ULONG blockNum,
						DriveGeoPtr driveGeo)
{
	LONG	blockSize = BlockSize(driveGeo);

	ioReq->iotd_Req.io_Command	= CMD_READ;
	ioReq->iotd_Req.io_Offset	= blockNum*blockSize;
	ioReq->iotd_Req.io_Data		= buff;
	ioReq->iotd_Req.io_Length	= blockSize;
	return (DoIO((IOReqPtr) ioReq) == 0);
}

/*
 *  Write a single block to drive
 */

static BOOL WriteBlock(IOExtTDPtr ioReq, UBYTE *buff, ULONG blockNum,
							DriveGeoPtr driveGeo)
{
	LONG	blockSize = BlockSize(driveGeo);

	ioReq->iotd_Req.io_Command	= CMD_WRITE;
	ioReq->iotd_Req.io_Offset	= blockNum*blockSize;
	ioReq->iotd_Req.io_Data		= buff;
	ioReq->iotd_Req.io_Length	= blockSize;
	return (DoIO((IOReqPtr) ioReq) == 0);
}

ULONG GetSize(TextPtr source, WORD diskNum)
{
	ULONG		size = 0;
	Dir			lock = NULL;
	FIBPtr		fib = NULL;
	DriveGeo	driveGeo;
	ULONG		unit;

	if (diskNum >= 0) {
		if (SetUpDevice(diskList[diskNum], &driveGeo, &unit))
			size = TotalTracks(&driveGeo)*TrackSize(&driveGeo);
	}
	else {
		if ((lock = Lock(source, ACCESS_READ)) == NULL ||
			(fib = MemAlloc(sizeof(FIB), 0)) == NULL || !Examine(lock, fib))
				goto Exit;
		size = fib->fib_Size;
	}
Exit:
	if (lock)
		UnLock(lock);
	if (fib)
		MemFree(fib,sizeof(FIB));

	return (size);
}

/*
 *	Return unit number of given device name
 */

static WORD	DeviceUnit(TextPtr devName)
{
	WORD	unit;

	for (unit = 0; unit < NUMUNITS; unit++) {
		if (CmpString(devName, devListNames[unit], strlen(devName), strlen(devListNames[unit]), FALSE) == 0)
			break;
	}
	if (unit >= NUMUNITS) {
		Error(ERR_BAD_NAME, devName);
		return (-1);
	}
	return (unit);
}

/*
 *	Get all information and open device for given drive
 */

static BOOL SetUpDevice(TextPtr devName, DriveGeoPtr driveGeo, ULONG *unit)
{
	WORD	len = strlen(devName);
	BOOL	nameChange = FALSE;
	BOOL	success = TRUE;

	if (devName[len - 1] == ':') {
		devName[len - 1] = '\0';
		nameChange = TRUE;
	}
/*
	Get unit number
*/
	if ((*unit = (ULONG) DeviceUnit(devName)) == -1)
		success = FALSE;
/*
	Get drive info
*/
	if (success && !GetDriveGeometry(devName, driveGeo)) {
		Error(ERR_NO_INFO, devName);
		success = FALSE;
	}
	if (nameChange)
		devName[len - 1] = ':';

	return (success);
}

/*
 *	Copy from source device to destination file
 */

WORD CopyDiskToImage(TextPtr sourceDev, UBYTE *image)
{
	DriveGeo	srcDriveGeo;
	IOExtTDPtr	ioReq;
	MsgPortPtr	ioMsgPort, procID;
	BOOL		devOpen;
	ULONG		trackNum, unit, index;
	ULONG		trackSize;
	WORD		success, error;
	TextChar	imageName[MAX_FILENAME_LEN];
	UBYTE		*readBuffer = NULL;

	ioMsgPort	= NULL;
	ioReq		= NULL;
	devOpen		= FALSE;
	success 	= LOAD_FAIL;

	if (!SetUpDevice(sourceDev,&srcDriveGeo,&unit))
		goto Exit;
/*
	Open device
*/
	if ((ioMsgPort = CreatePort(0, 0)) == NULL ||
		(ioReq = (IOExtTDPtr) CreateExtIO(ioMsgPort, sizeof(IOExtTD))) == NULL) {
		Error(ERR_NO_MEM, NULL);
		goto Exit;
	}
	if (OpenDevice(TD_NAME, unit, (IOReqPtr) ioReq, TDF_ALLOW_NON_3_5)) {
		Error(ERR_DEV_ACC, sourceDev);
		goto Exit;
	}
	ioReq->iotd_Req.io_Message.mn_ReplyPort = ioMsgPort;
	devOpen = TRUE;

	while ((error = CanUseDisk(ioReq)) == TDERR_DiskChanged) {
		if ((success = FixError(error, unit)) == LOAD_ABORT)
			goto Exit;
	}
/*
	Do actual copy
*/
	trackSize = TrackSize(&srcDriveGeo);
	if ((readBuffer = MemAlloc(trackSize,(MEMF_CHIP | MEMF_PUBLIC))) == NULL) {
		Error(ERR_NO_MEM, NULL);
		goto Exit;
	}
	GetVolumeName(sourceDev,imageName);

	procID = DeviceProc(sourceDev);
	SetDiskBusy(procID, TRUE);

	if (!GetStatusRequest(imageName, TotalBlocks(&srcDriveGeo),TRUE))
		goto Exit1;

	index = 0;
	success = LOAD_SUCCESS;
	SetStatusBarGraph(0);
	for (trackNum = 0; trackNum < TotalTracks(&srcDriveGeo); trackNum++) {
		if (StatusCheckAbort()) {
			success = LOAD_ABORT;
			break;
		}
		if (!ReadTrack(ioReq, readBuffer, trackNum, &srcDriveGeo)) {
			success = LOAD_FAIL;
			DisposeStatusReq();
			Error(ERR_READ,diskListNames[unit]);
			break;
		}
		BlockMove(readBuffer, &image[index], trackSize);
		SetStatusBarGraph(trackNum*srcDriveGeo.dg_TrackSectors);
		index += TrackSize(&srcDriveGeo);
	}
	if (trackNum == TotalTracks(&srcDriveGeo))
		SetStatusBarGraph(trackNum*srcDriveGeo.dg_TrackSectors);
/*
	Finish up
*/
	StatusEnableAbort(FALSE);
Exit1:
	MotorOff(ioReq);
	SetDiskBusy(procID, FALSE);
	DisposeStatusReq();
/*
	All done
*/
Exit:
	if (devOpen)
		CloseDevice((IOReqPtr) ioReq);
	if (ioReq)
		DeleteExtIO((IOReqPtr) ioReq);
	if (ioMsgPort)
		DeletePort(ioMsgPort);
	if (readBuffer)
		MemFree(readBuffer,trackSize);
	if (success == LOAD_SUCCESS)
		strcpy(currentImageName,imageName);
	return (success);
}

/*
 *	Do byte by byte compare of two buffers
 */

static BOOL CompareTracks(register ULONG *track1, register ULONG *track2, register ULONG len)
{
	len >>= 2;			// Change from byte to long count
	while (len--) {
		if (*track1++ != *track2++)
			return (FALSE);
	}
	return (TRUE);
}

/*
 *	Calculate checksum for a block
 */

static ULONG CalcChecksum(ULONG *block, DriveGeo driveGeo)
{
	register ULONG	numLongs = BlockSize(&driveGeo) >> 5;		//bytes->longs/8
	register ULONG	sum = 0L;

	while (numLongs--) {
		sum += *block++;		sum += *block++;
		sum += *block++;		sum += *block++;
		sum += *block++;		sum += *block++;
		sum += *block++;		sum += *block++;
	}
	return((ULONG) (-1 * (LONG) sum));
}

/*
 *	Write current datestamp to disk
 */

static BOOL	PutDateStamp(IOExtTDPtr ioReq, DriveGeo driveGeo)
{
	BOOL				success = FALSE;
	UBYTE				*block;
	ULONG				blockSize = BlockSize(&driveGeo);
	ULONG				dateIndex, rootLoc;
	ULONG				checksum;
	struct DateStamp	dateStamp;

	if ((block = MemAlloc(blockSize, MEMF_CHIP | MEMF_PUBLIC)) == NULL)
		return (FALSE);
/*
	Make date stamp and write to root block
*/
	dateIndex = blockSize - 28;
	DateStamp(&dateStamp);
	rootLoc = RootLocation(&driveGeo);
	if (ReadBlock(ioReq, block, rootLoc, &driveGeo)) {
		BlockMove((UBYTE *) &dateStamp, &block[dateIndex], sizeof(struct DateStamp));
		((ULONG *) block)[CHECKSUM_INDEX] = 0L;
		checksum = CalcChecksum((ULONG *) block, driveGeo);
		((ULONG *) block)[CHECKSUM_INDEX] = checksum;
		if (WriteBlock(ioReq, block, rootLoc, &driveGeo))
			success = TRUE;
	}
/*
	All done
*/
	MemFree(block,blockSize);
	return (success);
}

/*
 *	Set disk to BAD
 */

static void WriteBAD(IOExtTDPtr ioReq,DriveGeo driveGeo)
{
	UBYTE	*block;
	ULONG	blockSize = BlockSize(&driveGeo);

	if ((block = MemAlloc(blockSize, MEMF_CHIP | MEMF_PUBLIC)) == NULL)
		return;
/*
	Read first block and set type to BAD
	Ignore errors
*/
	if (ReadBlock(ioReq, block, 0, &driveGeo)) {
		((ULONG *) block)[0] = ID_UNREADABLE_DISK;
		WriteBlock(ioReq, block, 0, &driveGeo);
	}
/*
	All done
*/
	MemFree(block,blockSize);
}

/*
 *	Copy from currentImage to disk number of times in destination structure
 */

BOOL DoCopy()
{
	IOExtTDPtr	ioReq[NUMUNITS];
	MsgPortPtr	ioMsgPort[NUMUNITS],
				procID[NUMUNITS];
	BOOL		devOpen[NUMUNITS], putDate,
				trackWritten[NUMUNITS];
	ULONG		trackNum, startTrack, endTrack;
	WORD		passes, driveNum,firstDrive;
	DriveGeo	driveGeo, newDriveGeo;
	ULONG		index, trackSize;
	WORD		success, error;
	UBYTE		*readBuffer, *writeBuffer;
	LONG		dosType;

	readBuffer = NULL;
	writeBuffer = NULL;
	dosType = *((LONG *) currentImage);
	putDate = (dosType >= ID_DOS0 && dosType <= ID_DOS5);
	firstDrive = -1;
	while (destination.FloppyDrives[++firstDrive] == 0);
/*
	open destination drives
*/
	for (driveNum = 0; driveNum < NUMUNITS; driveNum++) {
		ioMsgPort[driveNum]	= NULL;
		ioReq[driveNum]		= NULL;
		devOpen[driveNum]	= FALSE;
	}

	for (driveNum = 0; driveNum < NUMUNITS; driveNum++) {
		if (destination.FloppyDrives[driveNum]) {
			if (!GetDriveGeometry(devListNames[driveNum], &newDriveGeo)) {
				Error(ERR_NO_INFO, diskListNames[driveNum]);
				goto Exit;
			}
			if (driveNum == firstDrive)
				BlockMove(&newDriveGeo,&driveGeo,sizeof(DriveGeo));
			if (!CompatableDriveGeo(driveGeo,newDriveGeo)) {
				Error(ERR_COMPAT,devListNames[driveNum]);
				goto Exit;
			}
/*
	Open device
*/
			if ((ioMsgPort[driveNum] = CreatePort(0, 0)) == NULL ||
				(ioReq[driveNum] = (IOExtTDPtr) CreateExtIO(ioMsgPort[driveNum], sizeof(IOExtTD))) == NULL) {
				Error(ERR_NO_MEM, NULL);
				goto Exit;
			}
			if (OpenDevice(TD_NAME, driveNum, (IOReqPtr) ioReq[driveNum], TDF_ALLOW_NON_3_5)) {
				Error(ERR_DEV_ACC, diskListNames[driveNum]);
				goto Exit;
			}
			ioReq[driveNum]->iotd_Req.io_Message.mn_ReplyPort = ioMsgPort[driveNum];
			devOpen[driveNum] = TRUE;
		}
	}

	trackSize = TrackSize(&driveGeo);
	if ((readBuffer = MemAlloc(trackSize,MEMF_CHIP)) == NULL) {
		Error(ERR_NO_MEM,NULL);
		goto Exit;
	}

	if ((writeBuffer = MemAlloc(trackSize,MEMF_CHIP)) == NULL) {
		Error(ERR_NO_MEM,NULL);
		goto Exit;
	}

/*
	Do actual copy
*/
	for (driveNum = 0; driveNum < NUMUNITS; driveNum++) {
		if (destination.FloppyDrives[driveNum]) {
			procID[driveNum] = DeviceProc(diskListNames[driveNum]);
			SetDiskBusy(procID[driveNum], TRUE);
		}
	}

	if (!PromptDisk(PROMPT_FIRSTDEST))
		goto Exit1;

	if (options.AllTracks) {
		startTrack = 0;
		endTrack = TotalTracks(&driveGeo);
	}
	else {
		startTrack = options.StartTrack;
		endTrack = options.EndTrack;
	}
/*
	Main loop for multiple passes
*/
	for (passes = 0; passes < destination.NumPasses || destination.Unlimited; passes++) {
TryAgain:
		for (driveNum = 0; driveNum < NUMUNITS; driveNum++) {		// check for usable disk
			trackWritten[driveNum] = FALSE;
			if (destination.FloppyDrives[driveNum]) {
				if (error = CanUseDisk(ioReq[driveNum])) {
					if ((success = FixError(error, driveNum)) == COPY_SUCCESS)
						goto TryAgain;
					goto Exit1;
				}
			}
		}

		success = COPY_SUCCESS;
		index = 0;
		if (!GetStatusRequest(currentImageName, TotalBlocks(&driveGeo), FALSE))
			goto Exit1;
		SetStatusPassNum(passes + 1, destination.NumPasses);
/*
	Write tracks
*/
		for (trackNum = startTrack;
			 trackNum < endTrack && success == COPY_SUCCESS;
			 trackNum++) {
			BlockMove(&currentImage[index], writeBuffer, trackSize);
			for (driveNum = 0; driveNum < NUMUNITS; driveNum++) {
				if (StatusCheckAbort()) {
					success = COPY_ABORT;
					WaitAllIO(ioReq);
					goto Exit1;
				}
				if (destination.FloppyDrives[driveNum]) {
					WriteTrack(ioReq[driveNum], writeBuffer, trackNum, &driveGeo);
					trackWritten[driveNum] = TRUE;
				}
			}
/*
	Verify tracks were written
*/
			if (!options.Verify)
				WaitAllIO(ioReq);
			else {
				for (driveNum = 0; driveNum < NUMUNITS; driveNum++) {
					if (StatusCheckAbort()) {
						success = COPY_ABORT;
						WaitAllIO(ioReq);
						goto Exit1;
					}
					if (destination.FloppyDrives[driveNum]) {
						WaitIO(ioReq[driveNum]);
						ClearTrack(ioReq[driveNum]);		// Waits for completion
						if (!ReadTrack(ioReq[driveNum], readBuffer, trackNum, &driveGeo)) {
							DisposeStatusReq();
							MotorOff(ioReq[driveNum]);
							if ((success = FixError(ioReq[driveNum]->iotd_Req.io_Error,driveNum)) == COPY_SUCCESS)
								goto TryAgain;
							if (success == COPY_FAIL) {
								if (ReadError(diskListNames[driveNum]))
									goto TryAgain;
								else
									success = COPY_ABORT;
							}
							goto Exit1;
						}
						if (!CompareTracks((ULONG *) readBuffer, (ULONG *) writeBuffer,
											trackSize)) {
							success = COPY_FAIL;
							goto Exit1;
						}
					}
				}
			}
			index += trackSize;
			SetStatusBarGraph(trackNum*driveGeo.dg_TrackSectors);
		}
		if (trackNum == TotalTracks(&driveGeo))
			SetStatusBarGraph(trackNum*driveGeo.dg_TrackSectors);
		if (options.UniqueCopy && putDate) {
			for (driveNum = 0; driveNum < NUMUNITS; driveNum++)
				if (destination.FloppyDrives[driveNum])
					PutDateStamp(ioReq[driveNum],driveGeo);
		}
		for (driveNum = 0; driveNum < NUMUNITS; driveNum++) {
			if (destination.FloppyDrives[driveNum])
				MotorOff(ioReq[driveNum]);
		}
		DisposeStatusReq();
		if ((passes + 1 < destination.NumPasses || destination.Unlimited) &&
					!PromptDisk(PROMPT_NEXTDEST))
			goto Exit1;
	}
	PromptDisk(PROMPT_REMOVE);
/*
	Finish up
*/
Exit1:
	StatusEnableAbort(FALSE);
	for (driveNum = 0; driveNum < NUMUNITS; driveNum++) {
		if (destination.FloppyDrives[driveNum]) {
			if (success != COPY_SUCCESS && trackWritten[driveNum])
				WriteBAD(ioReq[driveNum],driveGeo);
			MotorOff(ioReq[driveNum]);
			SetDiskBusy(procID[driveNum], FALSE);
		}
	}
	DisposeStatusReq();

	if (success == COPY_FAIL)
		Error(ERR_COPYING,NULL);
/*
	All done
*/
Exit:
	for (driveNum = 0; driveNum < NUMUNITS; driveNum++) {
		if (devOpen[driveNum])
			CloseDevice((IOReqPtr) ioReq[driveNum]);
		if (ioReq[driveNum])
			DeleteExtIO((IOReqPtr) ioReq[driveNum]);
		if (ioMsgPort[driveNum])
			DeletePort(ioMsgPort[driveNum]);
	}

	if (readBuffer)
		MemFree(readBuffer, trackSize);
	if (writeBuffer)
		MemFree(writeBuffer, trackSize);
	return (success == COPY_SUCCESS);
}

