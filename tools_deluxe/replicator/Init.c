/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	FloppyCopy
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Initialization
 */

#include <exec/types.h>
#include <exec/libraries.h>
#include <workbench/startup.h>

#include <graphics/gfxmacros.h>
#include <graphics/gfxbase.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>
#include <proto/icon.h>

#include <clib/alib_protos.h>

#include <TypeDefs.h>

#include <Toolbox/Menu.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/StdInit.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/DOS.h>

#include <stdlib.h>
#include <string.h>

#include "FloppyCopy.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct IntuitionBase	*IntuitionBase;
extern struct GfxBase		*GfxBase;
extern struct Library		*LayersBase;
extern struct Library		*IconBase;
extern struct Device		*ConsoleDevice;

extern WindowPtr	window;
extern MsgPortPtr	mainMsgPort;
extern TextChar		progName[];
extern TextPtr		initError[];

extern TextChar		progPathName[];
extern WORD			defaultButton;

/*
 *	Local variables and definitions
 */


static struct IOStdReq	consoleIOReq;

static ScreenPtr	screen;

/*
	local prototypes
 */

static void	BuildProgPathName(TextPtr, Dir);
static ScreenPtr GetPublicScreen(TextPtr);

/*
 *	Build path name to program from given parameters
 *	progPathName[] is 100 bytes in size
 *	Note: Cannot use ProgDir: in system 2.0, since this path & name is used for
 *		document icon default tools
 */

static void BuildProgPathName(TextPtr fileName, Dir dirLock)
{
	register TextPtr		newName;
	register WORD			nameLen, newLen;
	register Dir			dir, parentDir;
	struct FileInfoBlock	*fib;

	strcpy(progPathName, fileName);
	if ((fib = MemAlloc(sizeof(struct FileInfoBlock), 0)) == NULL)
		return;
	newName = fib->fib_FileName;
	dir = DupLock(dirLock);
	while (dir) {
		if (!Examine(dir, fib))
			break;
		nameLen = strlen(progPathName);
		newLen = strlen(newName);
		if (nameLen + newLen > 98)
			break;
		BlockMove(progPathName, progPathName + newLen + 1, nameLen + 1);
		BlockMove(newName, progPathName, newLen);
		parentDir = ParentDir(dir);
		progPathName[newLen] = (parentDir) ? '/' : ':';
		UnLock(dir);
		dir = parentDir;
	}
	if (dir)
		UnLock(dir);
	MemFree(fib, sizeof(struct FileInfoBlock));
}

/*
 *	Return pointer to specified public screen
 *	If not running under 2.0 then only NULL is valid for screen name
 */

static ScreenPtr GetPublicScreen(TextPtr scrnName)
{
	LONG intuiLock;
	register ScreenPtr pubScreen;

	if (LibraryVersion((struct Library *)IntuitionBase) >= OSVERSION_2_0)
	{	pubScreen = LockPubScreen(scrnName);
	}
	else
	{	if (scrnName == NULL)
		{	intuiLock = LockIBase(0);
			for (pubScreen = IntuitionBase->FirstScreen; pubScreen; pubScreen = pubScreen->NextScreen)
			{	if ((pubScreen->Flags & SCREENTYPE) == WBENCHSCREEN)
					break;
			}
			UnlockIBase(intuiLock);
		}
		else
		{	pubScreen = NULL;
		}
	}
	return (pubScreen);
}

/*
 *	Initialization routine
 *	Open necessary libraries and devices, and open background window
 */

void Init(int argc, char *argv[])
{
	Dir					dir;
	register TextPtr	text;
	struct DiskObject	*icon;
	struct WBStartup	*wbMsg;
	WORD				i;
	TextPtr				progName;
	BOOL				fromCLI;
/*
	Open needed libraries
*/
	if (!StdInit(OSVERSION_1_2)) {
		ShutDown();
		exit(RETURN_FAIL);
	}
/*
	Open main window
*/
	screen = GetPublicScreen(NULL);
	InitToolbox(screen);
/*
	Create message port
*/
	if ((mainMsgPort = CreatePort(NULL, 0)) == NULL) {
		InitError(initError[INIT_ERR_PORT]);
		ShutDown();
		exit(RETURN_FAIL);
	}
/*
	Open main window
*/
	CreateWindow();
	if (window == NULL) {
		InitError(initError[INIT_ERR_WINDOW]);
		ShutDown();
		exit(RETURN_FAIL);
	}
/*
	Build path name to program
*/
	fromCLI = (argc > 0);

	if (!fromCLI)
		wbMsg = (struct WBStartup *) argv;

	progName = (fromCLI) ? argv[0] : wbMsg->sm_ArgList->wa_Name;
	dir = ConvertFileName(progName);
	BuildProgPathName(progName, dir);
	UnLock(dir);
/*
	Now that the window is open, unlock the screen
*/
	if (screen && (LibraryVersion((struct Library *)IntuitionBase) >= OSVERSION_2_0)) {
		UnlockPubScreen(NULL, screen);
		screen = NULL;
	}
/*
	Complete initialization
*/
	InitOptions();
/*
	Setup for tooltype options
*/
	if (fromCLI) {				// Running under CLI
		for (i = 1; i < argc; i++) {
			text = argv[i];
			if (*text++ == '-')
				SetOption(text);
		}
	}
	else {					// Running under Workbench
		if ((icon = GetDiskObject(wbMsg->sm_ArgList->wa_Name)) != NULL) {
			if (icon->do_ToolTypes) {
				for (i = 0; (text = icon->do_ToolTypes[i]) != NULL; i++)
					SetOption(text);
			}
			FreeDiskObject(icon);
		}
	}
}

/*
 *	Open initial image if one given
 */

void SetUp(int argc, char *argv[])
{
	WORD			i, numFiles, len;
	BOOL			fromCLI;
	TextPtr			fileName;
	Dir				dir, startupDir;
	ProcessPtr		process;
	struct WBArg	*wbArgList;
	struct WBStartup	*wbMsg;
/*
	Open image
*/
	fromCLI = (argc > 0);
	if (!fromCLI)
		wbMsg = (struct WBStartup *) argv;

	numFiles = (fromCLI) ? argc : wbMsg->sm_NumArgs;
	process = (ProcessPtr) FindTask(NULL);
	startupDir = DupLock(process->pr_CurrentDir);
	for (i = 1; i < numFiles; i++) {
		if (fromCLI) {
			fileName = argv[i];
			if (*fileName == '-')			// Ignore program options
				continue;
			SetCurrentDir(startupDir);		// Path is relative to startupDir
			dir = ConvertFileName(fileName);
		}
		else {
			wbArgList = wbMsg->sm_ArgList;
			fileName = wbArgList[i].wa_Name;
			dir = DupLock(wbArgList[i].wa_Lock);
		}
		len = strlen(fileName);
		if (len == 0 || len > MAX_FILENAME_LEN)
			UnLock(dir);
		else {
			SetCurrentDir(dir);
			SetStdPointer(window, POINTER_WAIT);
			if (GetImage(fileName,LOAD_FROMFILE) == LOAD_SUCCESS) {
				SetGadgetItemText(window->FirstGadget, IMAGE_TEXT, window, NULL, fileName);
				OutlineButton(GadgetItem(window->FirstGadget, OK_BUTTON), window, NULL, FALSE);
				defaultButton = START_BUTTON;
				SetButtons();
			}
			SetStdPointer(window, POINTER_ARROW);
		}
	}
	UnLock(startupDir);
}

/*
 *	Shut down program
 *	Close window and all openned libraries
 */

void ShutDown()
{
	register MsgPtr msg;

	FreeCurrentImage();
	if (window) {
		RemoveWindow();
	}
	if (screen && (LibraryVersion((struct Library *)IntuitionBase) >= OSVERSION_2_0))
	{	UnlockPubScreen(NULL, screen);
		screen = NULL;
	}
	if (mainMsgPort) {
		while ((msg = GetMsg(mainMsgPort)) != NULL)
			ReplyMsg(msg);
		DeletePort(mainMsgPort);
	}
	StdShutDown();
}
