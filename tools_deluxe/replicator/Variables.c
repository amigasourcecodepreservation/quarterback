/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	FloppyCopy
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Global Variables
 */

#include <exec/types.h>

#include <Toolbox/Window.h>

#include "FloppyCopy.h"

struct IntuitionBase	*IntuitionBase;
struct GfxBase		*GfxBase;
struct LayersBase	*LayersBase;
struct Library		*IconBase;
struct Device		*ConsoleDevice;

WindowPtr		window;
MsgPortPtr		mainMsgPort;

BOOL	titleChanged;

TextPtr	diskListNames[] = { "DF0:", "DF1:", "DF2:", "DF3:" };
TextPtr	devListNames[] = { "DF0", "DF1", "DF2", "DF3" };
TextPtr	diskList[5];

Destination	destination;

Options		options;

TextChar	currentImageName[MAX_FILENAME_LEN];
UBYTE		*currentImage;
ULONG		currentImageSize;

WORD		defaultButton;

TextChar		progPathName[100];