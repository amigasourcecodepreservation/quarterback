/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Floppy Copy
 *
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Window routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/layers.h>
#include <proto/dos.h>
#include <proto/graphics.h>

#include <string.h>

#include <devices/trackdisk.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Bargraph.h>
#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>
#include <Toolbox/DOS.h>
#include <Toolbox/Border.h>

#include "FloppyCopy.h"
#include "Proto.h"

/*
 *  External variables
 */

extern WindowPtr		window;
extern MsgPortPtr		mainMsgPort;

extern TextPtr			diskList[];

extern BOOL	titleChanged;

extern TextChar screenTitle[], strFile[], strPass[], strOf[], strDisks[], strReadError[];
extern TextChar	strWriteProtect[],	strNoDisk[];

extern TextPtr	errMessage[];
extern TextPtr	promptMessage[];

extern DlgTemplPtr	dlgList[];
extern ReqTemplPtr	reqList[];

extern Destination	destination;

extern TextChar		currentImageName[];

extern WORD			defaultButton;

extern TextPtr	diskListNames[];
extern TextPtr	devListNames[];

/*
 *	Local variables and definitions
 */

static WORD	numDevices;

static RequestPtr	statusReq;
static BarGraphPtr	barGraph;

#define SHIFTKEYS        (IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)

#define ERROR_TEXT 		1		// Error requester
#define DISKERROR_TEXT	2
#define PROMPT_TEXT		2		// Prompt requester
#define FIRST_LINE		2		// Read/Write dialog

enum {				// Status requester
	ABORT_BUTTON,
	STAT_BARGRAPH,
	DISK_NAME,
	STATNUM_TEXT
};

enum {				// Confirm wipe requesters
	NO_BUTTON,
	YES_BUTTON,
	CONFIRMNAME_TEXT
};

/*
 *	Local prototypes
 */

static void ClearImageNameBox(void);
static void	DrawWindow(void);
static void	DrawStatusRequest(void);
static BOOL	StatusRequestFilter(IntuiMsgPtr, WORD *);

static DosListPtr	FindDosEntry1(DosListPtr, TextPtr, ULONG);

/*
 *	Clear image name box
 */

static void ClearImageNameBox()
{
	RastPtr		rPort = window->RPort;
	Rectangle	rect;

	GetGadgetRect(GadgetItem(window->FirstGadget, IMAGE_TEXT), window, NULL, &rect);
	DrawShadowBox(rPort, &rect, -2, FALSE);
	SetAPen(rPort, _tbPenLight);
	SetDrMd(rPort, JAM1);
	FillRect(rPort, &rect);
}

/*
 *  Create and draw main window
 */

void CreateWindow()
{
	GetDiskList();

	if ((window = GetDialog(dlgList[DLG_MAIN], NULL, mainMsgPort)) == NULL)
		return;

    SetStdPointer(window, POINTER_ARROW);
    SetWindowTitles(window, (UBYTE *) -1, screenTitle);
	ModifyIDCMP(window, window->IDCMPFlags | DISKINSERTED | DISKREMOVED);
	InitDestination();
	DrawWindow();
	ModifyIDCMP(window, window->IDCMPFlags | (DISKINSERTED | DISKREMOVED));
}

/*
 *	Remove main window
 */

void RemoveWindow()
{
	if (window) {
		SetClip(window->WLayer, NULL);
		DisposeDialog(window);
		window = NULL;
	}
}

/*
 *	Draw main window contents
 */

static void DrawWindow()
{
	GadgetPtr	gadgList = window->FirstGadget;

	ClearImageNameBox();
	SetGadgetItemText(gadgList, IMAGE_TEXT, window, NULL, currentImageName);

	EnableGadgetItem(gadgList, DF0_BOX, window, NULL, InDiskList("DF0:"));
	EnableGadgetItem(gadgList, DF1_BOX, window, NULL, InDiskList("DF1:"));
	EnableGadgetItem(gadgList, DF2_BOX, window, NULL, InDiskList("DF2:"));
	EnableGadgetItem(gadgList, DF3_BOX, window, NULL, InDiskList("DF3:"));
	SetButtons();
}

/*
 *	Refresh window (and status dialog if it is present)
 */

void RefreshWindow()
{
	RastPtr		rPort;
	Rectangle	rect;

	GetWindowRect(window, &rect);
	if (!EmptyRect(&rect)) {
/*
	If old color scheme, fill with _tbPenLight
	Note: Cannot call RefreshGadgets() within Begin/EndRefresh()
*/
		if (_tbPenLight != 0) {
			rPort = window->RPort;
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &rect);
			RefreshGadgets(GadgetItem(window->FirstGadget, 0), window, NULL);
		}
		BeginRefresh(window);
		DrawWindow();
		EndRefresh(window, TRUE);
	}
}

/*
 *	Return screen titles to default setting
 */

void FixTitle()
{
	SetWindowTitles(window, (TextPtr) -1, screenTitle);
	titleChanged = FALSE;
}

/*
 *  Error report routine
 *  Put error message on screen title bar
 */

void Error(register WORD errNum, TextPtr more)
{
	TextChar	error[256];

	SetDefaultButtons(OK_BUTTON, -1);
	if (errNum >= ERR_MAX_ERROR)
		errNum = ERR_UNKNOWN;

	strcpy(error, errMessage[errNum]);
	if (more)
		strcat(error, more);

	dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = error;

	if (StdAlert(dlgList[DLG_ERROR], window->WScreen, mainMsgPort, NULL) == -1) {
		SetWindowTitles(window, (UBYTE *) -1, error);
   		DisplayBeep(window->WScreen);
    	SysBeep(5);
		titleChanged = TRUE;
	}
	SetDefaultButtons(defaultButton, -1);
}

/*
 *	Handle HELP key
 */

void DoHelp()
{
	DialogPtr	dlg;

	SetDefaultButtons(OK_BUTTON, -1);
	if ((dlg = GetDialog(dlgList[DLG_ABOUT], window->WScreen, mainMsgPort)) == NULL)
		Error(ERR_NO_MEM,NULL);
	else {
		OutlineButton(GadgetItem(dlg->FirstGadget, OK_BUTTON), dlg, NULL, TRUE);
		(void) ModalDialog(mainMsgPort, dlg, DialogFilter);
		DisposeDialog(dlg);
	}
	SetDefaultButtons(defaultButton, -1);
}

/*
 *	Prompt for destination/source disk
 */

BOOL PromptDisk(WORD textNum)
{
	WORD		dlgNum;
	WORD		item = OK_BUTTON;
	DialogPtr	dlg;

	dlgNum = (textNum == PROMPT_REMOVE) ? DLG_REMOVEPROMPT : DLG_PROMPT;

	if (textNum != PROMPT_REMOVE)
		dlgList[dlgNum]->Gadgets[PROMPT_TEXT].Info = promptMessage[textNum];

	if ((dlg = GetDialog(dlgList[dlgNum], window->WScreen, mainMsgPort)) == NULL)
		Error(ERR_NO_MEM,NULL);
	else {
		SysBeep(5);
		OutlineButton(GadgetItem(dlg->FirstGadget, OK_BUTTON), dlg, NULL, TRUE);
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
		DisposeDialog(dlg);
	}

	return (item == OK_BUTTON);
}

/*
 *	Bring up wipe status requester, and initialize bar graph
 */

BOOL GetStatusRequest(TextPtr diskName, ULONG totalBlocks, BOOL reading)
{
	GadgetPtr	gadgList;
	Rectangle	rect;
	TextChar	quotedName[30];
	WORD		reqNum;

	reqNum = (reading) ? REQ_STATREAD : REQ_STATWRITE;

	strcpy(quotedName, "\"");
	strcat(quotedName, diskName);
	strcat(quotedName,"\"");
	reqList[reqNum]->Gadgets[DISK_NAME].Info = quotedName;
	if ((statusReq = GetRequest(reqList[reqNum], window, TRUE)) == NULL) {
		Error(ERR_NO_MEM,NULL);
		return (FALSE);
	}
	gadgList = statusReq->ReqGadget;
	GetGadgetRect(GadgetItem(gadgList, STAT_BARGRAPH), window, statusReq, &rect);
	barGraph = NewBarGraph(&rect, totalBlocks, 8, (BG_HORIZONTAL | BG_SHOWPERCENT));
	DrawStatusRequest();
	return (TRUE);
}

/*
 *	Draw status request contents
 */

static void DrawStatusRequest()
{
	if (statusReq && statusReq->ReqLayer) {
		DrawBarGraph(statusReq->ReqLayer->rp, barGraph);
		OutlineButton(GadgetItem(statusReq->ReqGadget, ABORT_BUTTON), window, statusReq, TRUE);
	}
}

/*
 *	Set pass number in status dialog
 */

void SetStatusPassNum(WORD passNum, WORD totPasses)
{
	TextChar	strBuff[50];

	if (destination.Unlimited)		// don't put anything
		return;

	strcpy(strBuff, strPass);
	NumToString(passNum, strBuff + strlen(strBuff));
	strcat(strBuff, strOf);
	NumToString(totPasses, strBuff + strlen(strBuff));
	SetGadgetItemText(statusReq->ReqGadget, STATNUM_TEXT, window, statusReq, strBuff);
}

/*
 *	Set status requester bar graph setting
 */

void SetStatusBarGraph(ULONG blockNum)
{
	if (statusReq->ReqLayer)
		SetBarGraph(statusReq->ReqLayer->rp, barGraph, blockNum);
}

/*
 *	Remove status requester
 */

void DisposeStatusReq()
{
	if (statusReq) {
		DisposeBarGraph(barGraph);
		EndRequest(statusReq, window);
		DisposeRequest(statusReq);
		statusReq = NULL;
	}
}

/*
 *	Enable or disable abort button in status requester
 */

void StatusEnableAbort(BOOL enable)
{
	if (statusReq)
		EnableGadgetItem(statusReq->ReqGadget, ABORT_BUTTON, window, statusReq, enable);
}

/*
 *	Check for abort request from user
 */

BOOL StatusCheckAbort()
{
	return (CheckRequest(mainMsgPort, window, DialogFilter) == ABORT_BUTTON);
}

/*
 *	Filter function for ModalDialog/ModalRequest
 *	Handles window updates
 */

BOOL DialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	RequestPtr	req;

	if (intuiMsg->IDCMPWindow == window && intuiMsg->Class == REFRESHWINDOW) {
		ReplyMsg((MsgPtr) intuiMsg);
		if (statusReq)
			DrawStatusRequest();
		else if ((req = window->FirstRequest) != NULL)
			OutlineButton(GadgetItem(req->ReqGadget, OK_BUTTON), window, req, TRUE);
		RefreshWindow();
		*item = -1;
		return (TRUE);
	}
	return (FALSE);
}

/*
 *	Find entry in DOS list
 *	Note: Currently only searches for DLT_DEVICE entries
 */

static DosListPtr FindDosEntry1(DosListPtr dosList, TextPtr devName, ULONG flags)
{
	WORD		nameLen;
	DosListPtr	dosEntry;
	TextChar	name[30];

	dosEntry = dosList;
	nameLen = strlen(devName);
	while ((dosEntry = NextDosEntry1(dosEntry, flags | LDF_READ)) != NULL) {
		if (dosEntry->dol_Type == DLT_DEVICE) {
			ConvertBSTR(dosEntry->dol_Name, name);
			if (CmpString(devName, name, nameLen, strlen(name), FALSE) == 0)
				return (dosEntry);
		}
	}
	return (NULL);
}

/*
 * 	Build list of DOS trackdisk devices and place in popup
 */

void GetDiskList()
{
	WORD		i;
	DosListPtr	dosEntry;

/*
	Add available devices
*/
	dosEntry = LockDosList1(LDF_DEVICES | LDF_READ);
	numDevices = 0;
	for (i = 0; i < 4; i++) {
		if (FindDosEntry1(dosEntry, devListNames[i], LDF_DEVICES) != NULL)
			diskList[numDevices++] = diskListNames[i];
	}
	diskList[numDevices] = NULL;
	UnLockDosList1(LDF_DEVICES | LDF_READ);
}

/*
 *	Check to see if diskName is in disk list
 */

BOOL InDiskList(TextPtr diskName)
{
	WORD i;

	for (i = 0; i < numDevices; i++) {		// Don't need to check file and empty string
		if (!strcmp(diskName, diskList[i]))
			return (TRUE);
	}
	return (FALSE);
}

/*
 *	Enable/disable buttons
 */

void SetButtons()
{
	GadgetPtr	gadgList = window->FirstGadget;
	TextChar	strBuff[MAX_FILENAME_LEN];
	BOOL		unlimited = destination.Unlimited;

	OutlineButton(GadgetItem(gadgList, defaultButton), window, NULL, TRUE);

	GetGadgetItemText(gadgList, IMAGE_TEXT, strBuff);

	EnableGadgetItem(gadgList, START_BUTTON, window, NULL,
						(DestinationSelected() && strBuff[0] != '\0'));
	EnableGadgetItem(gadgList, SAVEIMGE_BUTTON,	window, NULL, strBuff[0] != '\0');

	SetGadgetItemValue(gadgList, DF0_BOX, window, NULL, destination.FloppyDrives[0]);
	SetGadgetItemValue(gadgList, DF1_BOX, window, NULL, destination.FloppyDrives[1]);
	SetGadgetItemValue(gadgList, DF2_BOX, window, NULL, destination.FloppyDrives[2]);
	SetGadgetItemValue(gadgList, DF3_BOX, window, NULL, destination.FloppyDrives[3]);

	SetGadgetItemValue(gadgList, NUMBER_RADBTN , window, NULL, !unlimited);
	SetGadgetItemValue(gadgList, UNLTD_RADBTN , window, NULL, unlimited);

	SetDiskNums();

	EnableGadgetItem(gadgList, PASSDOWN_ARROW, window, NULL, !unlimited && destination.NumPasses > MIN_PASSES);
	EnableGadgetItem(gadgList, PASSUP_ARROW, window, NULL, !unlimited && destination.NumPasses < MAX_PASSES);
}

/*
 *	Display number of passes and disks required
 */

void SetDiskNums()
{
	WORD		i, numSelected;
	TextChar	numBuff[10], strBuff[20];

/*
	Find how many drives are selected
*/
	numSelected = 0;
	for (i = 0; i < 4; i++) {
		if (destination.FloppyDrives[i])
			numSelected++;
	}
/*
	Build text string
*/
	NumToString(destination.NumPasses, strBuff);
	strcat(strBuff, " (");
	NumToString(destination.NumPasses*numSelected, numBuff);
	strcat(strBuff, numBuff);
	strcat(strBuff, " ");
	strcat(strBuff, strDisks);
	strcat(strBuff, ")");
	SetGadgetItemText(window->FirstGadget, PASS_TEXT, window, NULL, strBuff);
}

/*
 *	Display prompt for user to fix error
 */

WORD FixError(ULONG errNum, WORD driveNum)
{
	TextChar	error[256];
	DialogPtr	dlg;
	WORD		item;

	switch (errNum) {
		case TDERR_WriteProt:
			strcpy(error, strWriteProtect);
			break;
		case TDERR_DiskChanged:
			strcpy(error, strNoDisk);
			break;
		default:
			return (COPY_FAIL);
	}
	strcat(error,devListNames[driveNum]);

	dlgList[DLG_DISKERROR]->Gadgets[DISKERROR_TEXT].Info = error;

	if ((dlg = GetDialog(dlgList[DLG_DISKERROR], window->WScreen, mainMsgPort)) == NULL)
		Error(ERR_NO_MEM,NULL);
	else {
		SysBeep(5);
		OutlineButton(GadgetItem(dlg->FirstGadget, OK_BUTTON), dlg, NULL, TRUE);
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
		DisposeDialog(dlg);
	}

	return ((item == OK_BUTTON) ? COPY_SUCCESS : COPY_ABORT);
}

/*
 *	Display prompt for read/write error
 */

BOOL ReadError(TextPtr drive)
{
	WORD 		item;
	DialogPtr	dlg;
	TextChar	error[256];

	strcpy(error, strReadError);
	strcat(error, drive);
	dlgList[DLG_READERROR]->Gadgets[FIRST_LINE].Info = error;

	if ((dlg = GetDialog(dlgList[DLG_READERROR], window->WScreen, mainMsgPort)) == NULL)
		Error(ERR_NO_MEM,NULL);
	else {
		SysBeep(5);
		OutlineButton(GadgetItem(dlg->FirstGadget, OK_BUTTON), dlg, NULL, TRUE);
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
		DisposeDialog(dlg);
	}

	return (item == OK_BUTTON);
}