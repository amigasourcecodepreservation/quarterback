/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Floppy Copy
 *
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Window Definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Language.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Request.h>

/*
 *	Cursor key equivalents
 */

#define CURSORKEY_UP	0x1C
#define CURSORKEY_DOWN	0x1D
#define CURSORKEY_RIGHT	0x1E
#define CURSORKEY_LEFT	0x1F

/*
 *	Standard gadget definitions
 */

#define GT_ADJUST_UPARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,													\
		left, (top)+5-ARROW_HEIGHT, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		CURSORKEY_UP, 0, (Ptr) IMAGE_ARROW_UP }

#define GT_ADJUST_DOWNARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,										\
		left, (top)+5, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		CURSORKEY_DOWN, 0, (Ptr) IMAGE_ARROW_DOWN }

#define GT_ADJUST_TEXT(left, top)	\
	{ GADG_STAT_TEXT, (left)+10+ARROW_WIDTH, top, 0, 0, 0, 0, 0, 0, 0, 0, NULL }

/*
 *  Main dialog
 */

static GadgetTemplate diskGadgets[] = {
	{ GADG_PUSH_BUTTON,  20, 10, 0, 0, 100, 20, 0, 0, 'L', 0, "Load Image" },
	{ GADG_PUSH_BUTTON, 130, 10, 0, 0, 100, 20, 0, 0, 'S', 0, "Save Image" },
	{ GADG_PUSH_BUTTON, 240, 10, 0, 0,  70, 20, 0, 0, 't', 0, "Start" },
	{ GADG_PUSH_BUTTON, 320, 10, 0, 0,  70, 20, 0, 0, 'O', 0, "Options" },

	{ GADG_RADIO_BUTTON,  90,  95, 0, 0, 0, 0, 0, 0, 'N', 0, "Number" },
	{ GADG_RADIO_BUTTON,  90, 115, 0, 0, 0, 0, 0, 0, 'U', 0, "Unlimited" },

	{ GADG_CHECK_BOX, 130, 70, 0, 0, 0, 0, 0, 0, '0', 0, "DF0:"},
	{ GADG_CHECK_BOX, 190, 70, 0, 0, 0, 0, 0, 0, '1', 0, "DF1:"},
	{ GADG_CHECK_BOX, 250, 70, 0, 0, 0, 0, 0, 0, '2', 0, "DF2:"},
	{ GADG_CHECK_BOX, 310, 70, 0, 0, 0, 0, 0, 0, '3', 0, "DF3:"},

	GT_ADJUST_UPARROW(160, 95),		// Number of passes
	GT_ADJUST_DOWNARROW(160,95),
	GT_ADJUST_TEXT(160, 95),

	{ GADG_STAT_TEXT,  85,  45, 0, 0, 200, 11,  0, 0, 0, 0, NULL },		// Image name

	{ GADG_STAT_TEXT,  20,  70, 0, 0, 0, 0,  0, 0, 0, 0, "Destination:" },
	{ GADG_STAT_TEXT,  20,  45, 0, 0, 0, 0,  0, 0, 0, 0, "Image:" },
	{ GADG_STAT_TEXT,  20,  95, 0, 0, 0, 0,  0, 0, 0, 0, "Passes:" },

	{ GADG_ITEM_NONE },
};

static DialogTemplate mainDialog = {
	DLG_TYPE_WINDOW, DLG_FLAG_CLOSE | DLG_FLAG_DEPTH | DLG_FLAG_ZOOM,
	-1, -1, 410, 140, diskGadgets, "Replicator"
};

/*
 *	Load image request
 */

static GadgetTemplate loadGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, 'L', 0, "Load" },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, 'C', 0, "Cancel" },

	{ GADG_RADIO_BUTTON, 30, 50, 0, 0, 0, 0,  0, 0, 'D', 0, "Disk" },
	{ GADG_RADIO_BUTTON, 30, 70, 0, 0, 0, 0,  0, 0, 'F', 0, "File" },

	{ GADG_POPUP,	90, 50, 0, 0, 60, 11, 0, 0, 0, 0, NULL },

	{ GADG_EDIT_TEXT,  115, 70, 0, 0, 180, 11, 0, 0, 0, 0, NULL },

	{ GADG_PUSH_BUTTON, 90, 70, 0, -3, 15, 11, 0, 6, '?', 0, "?" },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 320 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0,  0, 0, 0, 0, "Load Image" },
	{ GADG_STAT_TEXT, 20, 35, 0, 0, 0, 0,  0, 0, 0, 0, "Source:" },

	{ GADG_ITEM_NONE },
};

static RequestTemplate loadRequest = {
	-1, -1, 320, 95, loadGadgets
};

/*
 *  Error dialog
 */

static GadgetTemplate errorGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT, 55, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },

	{ GADG_ITEM_NONE }
};

static DialogTemplate errorDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 330, 100, errorGadgets, NULL
};

/*
 *  Error dialog
 */

static GadgetTemplate diskErrorGadgets[] = {
	{ GADG_PUSH_BUTTON,  -80, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },
	{ GADG_PUSH_BUTTON, -160, -30, 0, 0, 60, 20, 0, 0, 'C', 0, "Cancel" },

	{ GADG_STAT_TEXT, 55, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },

	{ GADG_ITEM_NONE }
};

static DialogTemplate diskErrorDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 330, 100, diskErrorGadgets, NULL
};

/*
 *  Read/write Error dialog
 */

static GadgetTemplate readErrorGadgets[] = {
	{ GADG_PUSH_BUTTON,  -90, -30, 0, 0, 70, 20, 0, 0, 'R', 0, "Restart" },
	{ GADG_PUSH_BUTTON, -180, -30, 0, 0, 70, 20, 0, 0, 'C', 0, "Cancel" },

	{ GADG_STAT_TEXT, 55, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 55, 30, 0, 0, 0, 0, 0, 0, 0, 0,
		"Insert new disk and click\n\"Restart\" to continue." },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },

	{ GADG_ITEM_NONE }
};

static DialogTemplate readErrorDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 330, 100, readErrorGadgets, NULL
};

/*
 *  About dialog
 */

static GadgetTemplate aboutGadgets[] = {
	{ GADG_PUSH_BUTTON, 100, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT, 70, 15, 0, 0, 0, 0, 0, 0, 0, 0, "Replicator 1.0" },
	{ GADG_STAT_TEXT, 42, 40, 0, 0, 0, 0, 0, 0, 0, 0, "Designed and developed" },
	{ GADG_STAT_TEXT, 78, 55, 0, 0, 0, 0, 0, 0, 0, 0, "by Beth Henry" },
	{ GADG_STAT_TEXT, 66, 80, 0, 0, 0, 0, 0, 0, 0, 0, "Copyright \251 1993" },
	{ GADG_STAT_TEXT, 42, 95, 0, 0, 0, 0, 0, 0, 0, 0, "Central Coast Software" },
	{ GADG_STAT_TEXT, 78,110, 0, 0, 0, 0, 0, 0, 0, 0, "A division of" },
	{ GADG_STAT_TEXT, 22,125, 0, 0, 0, 0, 0, 0, 0, 0, "New Horizons Software, Inc."},
	{ GADG_STAT_TEXT, 54,140, 0, 0, 0, 0, 0, 0, 0, 0, "All Rights Reserved" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboutDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 195, aboutGadgets, NULL
};

/*
 * 	Prompt for new disk
 */

static GadgetTemplate promptGadgets[] = {
	{ GADG_PUSH_BUTTON,  -80, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },
	{ GADG_PUSH_BUTTON, -160, -30, 0, 0, 60, 20, 0, 0, 'C', 0, "Cancel" },

	{ GADG_STAT_TEXT,  55, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },

	{ GADG_ITEM_NONE }
};

static DialogTemplate promptDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 330, 100, promptGadgets, NULL
};

/*
 * 	Prompt to remove all disks
 */

static GadgetTemplate removePromptGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT,  55, 10, 0, 0, 0, 0, 0, 0, 0, 0,
		"Please remove disks from the\ndestination drives." },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },

	{ GADG_ITEM_NONE }
};

static DialogTemplate removePromptDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 330, 90, removePromptGadgets, NULL
};

/*
 *	Reading status requester
 */

static GadgetTemplate statusReadGadgets[] = {
	{ GADG_PUSH_BUTTON, 120, -30, 0, 0,  60, 20, 0, 0, 'S', 0, "Stop" },

	{ GADG_STAT_BORDER,  20, -60, 0, 0, -40, 20, 0, 0,   0, 0, NULL },
	{ GADG_STAT_TEXT,	100,  10, 0, 0,   0,  0, 0, 0,   0, 0, NULL },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 300 - 40, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,	   20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Reading:" },

	{ GADG_ITEM_NONE }
};

static RequestTemplate statusReadRequest = {
	-1, -1, 300, 110, statusReadGadgets
};

/*
 *	Writing status requester
 */

static GadgetTemplate statusWriteGadgets[] = {
	{ GADG_PUSH_BUTTON, 120, -30, 0, 0,  60, 20, 0, 0, 'S', 0, "Stop" },

	{ GADG_STAT_BORDER,  20,  60, 0, 0, -40, 20, 0, 0,   0, 0, NULL },
	{ GADG_STAT_TEXT,	100,  10, 0, 0,   0,  0, 0, 0,   0, 0, NULL },
	{ GADG_STAT_TEXT,	 20,  40, 0, 0,   0,  0, 0, 0,   0, 0, NULL },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 300 - 40, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,	   20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Writing:" },

	{ GADG_ITEM_NONE }
};

static RequestTemplate statusWriteRequest = {
	-1, -1, 300, 125, statusWriteGadgets
};

/*
 *	Options requester
 */

static GadgetTemplate optionsGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, 'C', 0, "Cancel" },

	{ GADG_CHECK_BOX, 	20, 40, 0, 0, 0, 0, 0, 0, 'V', 0, "Verify After Write" },
	{ GADG_CHECK_BOX, 	20, 60, 0, 0, 0, 0, 0, 0, 'M', 0, "Make Each Copy Unique" },

	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE,  160, 95, 0, 0, 32, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE,  240, 95, 0, 0, 32, 11, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON,  30, 95, 0, 0, 0, 0,  0, 0, 'A', 0, "All" },
	{ GADG_RADIO_BUTTON,  90, 95, 0, 0, 0, 0,  0, 0, 'F', 0, "From" },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 300 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0,  0, 0, 0, 0, "Write Options" },

	{ GADG_STAT_TEXT,  20, 80, 0, 0, 0, 0,  0, 0, 0, 0, "Write tracks:" },
	{ GADG_STAT_TEXT, 210, 95, 0, 0, 0, 0,  0, 0, 0, 0, "to" },
	{ GADG_ITEM_NONE }

};

static RequestTemplate optionsRequest = {
	-1, -1, 300, 120, optionsGadgets
};

/*
 *	Dialog list
 */

DlgTemplPtr dlgList[] = {
	&mainDialog,
	&errorDialog,
	&diskErrorDialog,
	&readErrorDialog,
	&aboutDialog,
	&promptDialog,
	&removePromptDialog
};

/*
 *	RequestList
 */

ReqTemplPtr reqList[] = {
	&statusReadRequest,
	&statusWriteRequest,
	&loadRequest,
	&optionsRequest
};
