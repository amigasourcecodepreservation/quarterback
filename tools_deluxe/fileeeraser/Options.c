/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Wipe File
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Options routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>
#include <stdio.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>

#include "WipeFile.h"
#include "Proto.h"

/*
 *  External variables
 */

extern WindowPtr		window;
extern MsgPortPtr		mainMsgPort;
extern ScrollListPtr	scrollList;

extern Options	options;

extern ReqTemplPtr	reqList[];

/*
 *	Local variables and definitions
 */

static RequestPtr	optionsReq;

enum {
	VALUEUP_ARROW = 2,
	VALUEDOWN_ARROW,
	VALUE_TEXT,
	REPEATUP_ARROW,
	REPEATDOWN_ARROW,
	REPEAT_TEXT,
	GOVT_RADBTN,
	STD_RADBTN,
	CONFIRM_BOX,
	SUBDRW_BOX,
	WIPEICON_BOX
};

#define MIN_WIPEVALUE 	0x00
#define MIN_REPEATCOUNT	1

#define MAX_WIPEVALUE 	0xFF
#define MAX_REPEATCOUNT	100

/*
 *	Local prototypes
 */

static void	ByteToHexString(UBYTE, TextPtr);
static BOOL MatchOptions(TextPtr, TextPtr, TextPtr);
static void ChangeValue(RequestPtr, UBYTE *, WORD);
static void ChangeCount(RequestPtr, WORD *, WORD);
static void SetOptionButtons(Options, GadgetPtr);

/*
 * Convert an unsigned longword into a hex string eight(+1) characters long.
 * Terminate with a space, but no null-terminator needed.
 */

static UBYTE hexChars[] = {
	'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'
};

static void ByteToHexString(register UBYTE val, register TextPtr buff)
{
	buff[0] = '$';
	buff[2] = hexChars[val & 0xF];
	val >>=	4;
	buff[1] = hexChars[val & 0xF];
	buff[3] = '\0';
}

/*
 *	Match screen options
 */

static BOOL MatchOptions(register TextPtr text, TextPtr opt1, TextPtr opt2)
{
	register WORD	len;

/*
	Get command length
*/
	for (len = 0; text[len] && text[len] != ':' && text[len] != '='; len++) ;
	if (text[len])
		len++;
/*
	Check for match
*/
	return ((BOOL) (CmpString(text, opt1, len, strlen(opt1), FALSE) == 0 ||
					CmpString(text, opt2, len, strlen(opt2), FALSE) == 0));
}


/*
	set appropriate option from string
 */

void SetOption(TextPtr text)
{
	register TextPtr	param;
	LONG				num;
/*
	Get pointer to command param (if any)
*/
	for (param = text; *param && *param != ':' && *param != '='; param++) ;
	if (*param)
		param++;

	if (MatchOptions(text, "value:", "v:") ||
		MatchOptions(text, "value=", "v=") ) {
		num = StringToNum(param);
		if (num < 0 || num > 256)
			return;
		options.StdWipeValue = options.GovtWipeValue = num;
		return;
	}
	if (MatchOptions(text, "repeat:", "r:") ||
		MatchOptions(text, "repeat=", "r=")) {
		num = StringToNum(param);
		if (num <= 0 || num > 100)
			return;
		options.StdRepeatCount = options.GovtRepeatCount = num;
		return;
	}
	if (MatchOptions(text, "government", "g")) {
		options.GovtWipe = TRUE;
		return;
	}
	if (MatchOptions(text, "subdrawers", "s")) {
		options.WipeSubdrawers = TRUE;
		return;
	}
	if (MatchOptions(text, "noicons", "ni")) {
		options.WipeIcons = FALSE;
		return;
	}
	if (MatchOptions(text, "nowarn", "nw")) {
		options.Confirm = FALSE;
		return;
	}
}

/*
 *	Init option structure
 */

void InitOptions()
{
	BlockClear(&options, sizeof(Options));
	options.StdWipeValue	= 0x00;
	options.GovtWipeValue	= 0xF6;
	options.StdRepeatCount	= 1;
	options.GovtRepeatCount	= 3;
	options.GovtWipe		= FALSE;
	options.WipeSubdrawers	= FALSE;
	options.WipeIcons		= TRUE;
	options.Confirm			= TRUE;
}

/*
 *	Change wipe value
 */

static void ChangeValue(RequestPtr req, UBYTE *value, WORD inc)
{
	GadgetPtr	gadgetList = req->ReqGadget;
	TextChar	strBuff[10];

	*value += inc;
	if (*value >= MAX_WIPEVALUE)
		*value = MAX_WIPEVALUE;
	if (*value <= MIN_WIPEVALUE)
		*value = MIN_WIPEVALUE;
	ByteToHexString(*value, strBuff);
	SetGadgetItemText(gadgetList, VALUE_TEXT, window, req, strBuff);
/*
	Do enable/disable after displaying new value, since these will be blocked
		if gadgets are selected
*/
	EnableGadgetItem(gadgetList, VALUEUP_ARROW, window, req, *value < MAX_WIPEVALUE);
	EnableGadgetItem(gadgetList, VALUEDOWN_ARROW, window, req, *value > MIN_WIPEVALUE);
}

/*
 *	Change repeat count
 */

static void ChangeCount(RequestPtr req, WORD *count, WORD inc)
{
	GadgetPtr	gadgetList = req->ReqGadget;
	TextChar	strBuff[10];

	*count += inc;
	if (*count >= MAX_REPEATCOUNT)
		*count = MAX_REPEATCOUNT;
	if (*count <= MIN_REPEATCOUNT)
		*count = MIN_REPEATCOUNT;
	NumToString(*count, strBuff);
	SetGadgetItemText(gadgetList, REPEAT_TEXT, window, req, strBuff);
/*
	Do enable/disable after displaying new value, since these will be blocked
		if gadgets are selected
*/
	EnableGadgetItem(gadgetList, REPEATUP_ARROW, window, req, *count < MAX_REPEATCOUNT);
	EnableGadgetItem(gadgetList, REPEATDOWN_ARROW, window, req, *count > MIN_REPEATCOUNT);
}


/*
 *	Options dialog filter
 */

static BOOL OptionDialogFilter(register IntuiMsgPtr intuiMsg, register WORD *item)
{
	register ULONG	class = intuiMsg->Class;
	register UWORD	code = intuiMsg->Code;
	UWORD			modifier = intuiMsg->Qualifier;
	register WORD	gadgNum;
	GadgetPtr		gadgetList = optionsReq->ReqGadget;

	if (intuiMsg->IDCMPWindow == window) {
		switch (class) {
		case GADGETDOWN:
		case GADGETUP:
			gadgNum = GadgetNumber(intuiMsg->IAddress);
			if (gadgNum == VALUEUP_ARROW || gadgNum == VALUEDOWN_ARROW ||
				gadgNum == REPEATUP_ARROW || gadgNum == REPEATDOWN_ARROW) {
				ReplyMsg((MsgPtr) intuiMsg);
				*item = (class == GADGETDOWN) ? gadgNum : -1;
				return (TRUE);
			}
			break;
		case RAWKEY:
			if (code == CURSORUP || code == CURSORDOWN) {
				ReplyMsg((MsgPtr) intuiMsg);
				if (modifier & SHIFTKEYS)
					gadgNum = (code == CURSORUP) ? REPEATUP_ARROW : REPEATDOWN_ARROW;
				else
					gadgNum = (code == CURSORUP) ? VALUEUP_ARROW : VALUEDOWN_ARROW;
				if (DepressGadget(GadgetItem(gadgetList, gadgNum), window, optionsReq))
					*item = gadgNum;
				return (TRUE);
			}
			break;
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Set all items in options requester to correct values
 */

static void SetOptionsDisplay(RequestPtr req, Options *opts)
{
	UBYTE		*wipeValuePtr;
	WORD		*repeatCountPtr;
	GadgetPtr	gadgList = req->ReqGadget;
	TextChar	strBuff[10];

	SetGadgetItemValue(gadgList, STD_RADBTN, window, req, !opts->GovtWipe);
	SetGadgetItemValue(gadgList, GOVT_RADBTN, window, req, opts->GovtWipe);
	SetGadgetItemValue(gadgList, CONFIRM_BOX, window, optionsReq, opts->Confirm);
	SetGadgetItemValue(gadgList, SUBDRW_BOX, window, optionsReq, opts->WipeSubdrawers);
	SetGadgetItemValue(gadgList, WIPEICON_BOX, window, optionsReq, opts->WipeIcons);

	wipeValuePtr = (opts->GovtWipe) ? &opts->GovtWipeValue : &opts->StdWipeValue;
	ByteToHexString(*wipeValuePtr, strBuff);
	SetGadgetItemText(gadgList, VALUE_TEXT, window, req, strBuff);
	repeatCountPtr = (opts->GovtWipe) ? &opts->GovtRepeatCount : &opts->StdRepeatCount;
	NumToString(*repeatCountPtr, strBuff);
	SetGadgetItemText(gadgList, REPEAT_TEXT, window, req, strBuff);

	EnableGadgetItem(gadgList, VALUEDOWN_ARROW, window, req, *wipeValuePtr > MIN_WIPEVALUE);
	EnableGadgetItem(gadgList, VALUEUP_ARROW, window, req, *wipeValuePtr < MAX_WIPEVALUE);
	EnableGadgetItem(gadgList, REPEATDOWN_ARROW, window, req, *repeatCountPtr > MIN_REPEATCOUNT);
	EnableGadgetItem(gadgList, REPEATUP_ARROW, window, req, *repeatCountPtr < MAX_REPEATCOUNT);
}

/*
 *	Do options requester
 */

void DoOptions()
{
	WORD		item, delay, inc;
	BOOL		done, redoList;
	GadgetPtr	gadgetList;
	UBYTE		*wipeValuePtr;
	WORD		*repeatCountPtr;
	Options		opts;

/*
	Display requester
*/
	if ((optionsReq = GetRequest(reqList[REQ_OPTIONS], window, TRUE)) == NULL) {
		Error(ERR_NO_MEM);
		return;
	}
	opts = options;
	gadgetList = optionsReq->ReqGadget;
	OutlineButton(GadgetItem(gadgetList, OK_BUTTON), window, optionsReq, TRUE);
	SetOptionsDisplay(optionsReq, &opts);
/*
	Handle requester
*/
	delay = 0;
	done = FALSE;
	do {
		WaitPort(mainMsgPort);
		item = CheckRequest(mainMsgPort, window, OptionDialogFilter);
		switch (item) {
			case OK_BUTTON:
			case CANCEL_BUTTON:
				done = TRUE;
				break;
			case GOVT_RADBTN:
			case STD_RADBTN:
				opts.GovtWipe = (item == GOVT_RADBTN);
				SetOptionsDisplay(optionsReq, &opts);
				break;
/*
	Handle INTUITICKS messages
*/
			case -1:
				if (delay > 0)
					delay--;
				else {
					if (delay > -9) {
						delay--;
						inc = 1;
					}
					else
						inc = 5;
					wipeValuePtr = (opts.GovtWipe) ? &opts.GovtWipeValue : &opts.StdWipeValue;
					repeatCountPtr = (opts.GovtWipe) ? &opts.GovtRepeatCount : &opts.StdRepeatCount;
					if (GadgetSelected(gadgetList, VALUEUP_ARROW))
						ChangeValue(optionsReq, wipeValuePtr, inc);
					else if (GadgetSelected(gadgetList, VALUEDOWN_ARROW))
						ChangeValue(optionsReq, wipeValuePtr, (WORD) -inc);
					else if (GadgetSelected(gadgetList, REPEATUP_ARROW))
						ChangeCount(optionsReq, repeatCountPtr, inc);
					else if (GadgetSelected(gadgetList, REPEATDOWN_ARROW))
						ChangeCount(optionsReq, repeatCountPtr, (WORD) -inc);
				}
				break;
/*
	Handle value up and down arrows
*/
			case VALUEUP_ARROW:
			case VALUEDOWN_ARROW:
				wipeValuePtr = (opts.GovtWipe) ? &opts.GovtWipeValue : &opts.StdWipeValue;
				ChangeValue(optionsReq, wipeValuePtr, (item == VALUEUP_ARROW) ? +1 : -1);
				delay = 5;
				break;
/*
	Handle repeat count up and down arrows
*/
			case REPEATUP_ARROW:
			case REPEATDOWN_ARROW:
				repeatCountPtr = (opts.GovtWipe) ? &opts.GovtRepeatCount : &opts.StdRepeatCount;
				ChangeCount(optionsReq, repeatCountPtr, (item == REPEATUP_ARROW) ? +1 : -1);
				delay = 5;
				break;
			case CONFIRM_BOX:
				opts.Confirm = !opts.Confirm;
				SetOptionsDisplay(optionsReq, &opts);
				break;
			case SUBDRW_BOX:
				opts.WipeSubdrawers = !opts.WipeSubdrawers;
				SetOptionsDisplay(optionsReq, &opts);
				break;
			case WIPEICON_BOX:
				opts.WipeIcons = !opts.WipeIcons;
				SetOptionsDisplay(optionsReq, &opts);
				break;
		}
	} while (!done);
	EndRequest(optionsReq, window);
	DisposeRequest(optionsReq);
	if (item == CANCEL_BUTTON)
		return;
/*
	Save new values
*/
	redoList = (opts.WipeIcons != options.WipeIcons);
	options = opts;
	Delay(5L);			// allow for options to be set

	if (redoList) {
		ClearFileList();
		GetFileList(NULL, 0, NULL);
		SetButtons();
	}
}
