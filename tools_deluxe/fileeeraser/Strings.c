/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Wipe File
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Text strings
 */

#include <exec/types.h>

#include <Typedefs.h>

#include <Toolbox/Language.h>

TextChar screenTitle[] = " File Eraser 1.0.1 - \251 1993 Central Coast Software";

TextChar version[] = "$VER: File Eraser 1.0";

UBYTE	*initError[] = {
	" Can't open window.",
	" Can't Create Message Port.",
};

UBYTE *errMessage[] = {
	"Not enough memory.",
	"Can't access directory ",
	"No file to wipe.",
	"Object in use.",
	"DOS Error: Number ",
	"Unknown internal error."
};

TextChar	strSystemText[]	= "Mounted disks";
TextChar	strWipeError[] = "Unable to wipe ";
TextChar	strFile[] = "file:";
TextChar	strDrawer[] = "drawer:";

TextChar	strErrorCLIWipe[] = "Unable to wipe file ";