/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Wipe File/Disk Definitions
 */


typedef struct FileInfoBlock	FIB, *FIBPtr;

/*
	dialog definitions
 */

enum {
	DLG_MAIN, 	DLG_ERROR,	DLG_ABOUT
};

enum {
	REQ_OPTIONS,	REQ_VERIFY
};

/*
	error definitions
 */

enum {
	INIT_ERR_WINDOW, 	INIT_ERR_PORT
};

enum {
	ERR_NO_MEM, 	ERR_DIR_ACC, 	ERR_NOFILENAME, 	ERR_INUSE, 	ERR_DOSERR,
	ERR_UNKNOWN, 	ERR_MAX_ERROR
};

/*
	window definitions
 */

/*	file dialog */

enum {				// Main dialog
	WIPE_BUTTON,
	OPTIONS_BUTTON,
	ENTER_BUTTON,
	BACK_BUTTON,
	DISK_BUTTON,
	FILE_LIST,
	FILE_UP,
	FILE_DOWN,
	FILE_SCROLL,
	DISK_NAME,
	DISK_ICON
};

enum {				// Confirm requester
	NO_BUTTON,
	ABORT_BUTTON,
	YES_BUTTON,
	CONFIRMNAME_TEXT
};

#define MAX_FILENAME_LEN 512

/*
	type definitions
 */

enum {
	TYPE_FILE,	TYPE_DRAWER,	TYPE_DISK
};

#define TYPE_ERROR	-1

/*
	macros
 */
#define MAX(a,b)	((a)>(b)?(a):(b))
#define MIN(a,b)	((a)<(b)?(a):(b))

#define ALTKEYS		(IEQUALIFIER_LALT | IEQUALIFIER_RALT)
#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)

/*
	options
 */

typedef struct {
	BOOL	GovtWipe;
	UBYTE	StdWipeValue,GovtWipeValue;
	WORD 	StdRepeatCount,GovtRepeatCount;
	BOOL	WipeSubdrawers;
	BOOL	WipeIcons;
	BOOL	Confirm;
} Options;
