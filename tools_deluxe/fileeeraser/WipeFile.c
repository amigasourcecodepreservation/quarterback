/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Wipe File
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Wipe procedure
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <dos/dos.h>
#include <dos/dosextens.h>

#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/intuition.h>

#include <Typedefs.h>

#include <Toolbox/Memory.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/DOS.h>

#include <stdio.h>
#include <string.h>

#include "WipeFile.h"
#include "Proto.h"

/*
	external variables
 */

extern WindowPtr		window;
extern MsgPortPtr		mainMsgPort;
extern ScrollListPtr	scrollList;

extern Options	options;

extern TextChar	strErrorCLIWipe[];

/*
 *	Local variables and definitions
 */

#define BUFFER_SIZE	512

#define WIPE_SUCCESS	1			// Return values from WipeFile() and WipeDrawer()
#define WIPE_FAIL		0
#define WIPE_ABORT		-1

/*
 *	Local prototypes
 */

static WORD		GetEntryType(TextPtr);
static BOOL 	FillFile(File, ULONG, UBYTE);
static WORD		WipeFile(TextPtr, BOOL, BOOL);
static WORD 	WipeDrawer(TextPtr, BOOL, BOOL);

/*
 *	Determine type of entry this is (file, drawer, or disk)
 */

static WORD GetEntryType(TextPtr name)
{
	WORD	type;
	LONG	lock;
	FIBPtr	fib;
	Dir 	parentDir;

	type = TYPE_ERROR;
	if ((lock = Lock(name, ACCESS_READ)) != NULL) {
		if ((fib = MemAlloc(sizeof(FIB), MEMF_CLEAR)) != NULL) {
			if (Examine(lock, fib)) {
				if (fib->fib_DirEntryType <= 0)
					type = TYPE_FILE;
				else if ((parentDir = ParentDir(lock)) == NULL)
					type = TYPE_DISK;
				else {
					UnLock(parentDir);
					type = TYPE_DRAWER;
				}
			}
			MemFree(fib, sizeof(FIB));
		}
		UnLock(lock);
	}
	return (type);
}

/*
 *	Wipe open file with specified value
 */

static BOOL FillFile(File file, ULONG size, UBYTE wipeVal)
{
	register ULONG 	i, amount;
	UBYTE			buffer[BUFFER_SIZE];

/*
	Clear buffer to wipe value
*/
	for (i = 0; i < BUFFER_SIZE; i++)
		buffer[i] = wipeVal;
/*
	Wipe file contents
*/
	do {
		amount = MIN(BUFFER_SIZE, size);
		if (Write(file, buffer, amount) < amount)
			return (FALSE);
		size -= amount;
	} while (size > 0);
/*
	All done
*/
	return (TRUE);
}

/*
 * 	Wipe file then delete
 *	Note:  Must only be called with file names!
 */

static WORD WipeFile(TextPtr fileName, BOOL confirm, BOOL fromCLI)
{
	UBYTE	wipeValue;
	BOOL	success = FALSE;
	WORD	i, numWipes, totWipes;
	ULONG 	size;
	LONG	lock;
	File	file;
	FIBPtr	fib;

	fib = NULL;
/*
	Confirm wipe
*/
	if (confirm) {
		i = ConfirmWipe(fileName);
		if (i == ABORT_BUTTON)
			return (WIPE_ABORT);
		if (i == NO_BUTTON)
			return (WIPE_FAIL);
	}
/*
	Get file size and do wipe
*/
	if ((lock = Lock(fileName, ACCESS_READ)) == NULL ||
		(fib = MemAlloc(sizeof(FIB), 0)) == NULL || !Examine(lock, fib))
		goto Error;
	UnLock(lock);
	lock = NULL;
	size = fib->fib_Size;
	if ((file = Open(fileName, MODE_OLDFILE)) == NULL)
		goto Error;
	success = TRUE;
	if (options.GovtWipe) {
		totWipes = 2*options.GovtRepeatCount;
		for (numWipes = 0; success && numWipes < totWipes; numWipes++) {
			wipeValue = (numWipes & 1) ? 0xFF : 0x00;
			success = FillFile(file, size, wipeValue);
		}
		if (success)
			success = FillFile(file, size, options.GovtWipeValue);
	}
	else {
		for (numWipes = 0; success && numWipes < options.StdRepeatCount; numWipes++)
			success = FillFile(file, size, options.StdWipeValue);
	}
	Close(file);
/*
	Delete file
*/
	if (success && !DeleteFile(fileName))
		success = FALSE;				// Wiped OK, but couldn't delete
/*
	All done
*/
Error:
	if (!success && !fromCLI)
		WipeError(fileName, TYPE_FILE);
Exit:
	if (lock)
		UnLock(lock);
	if (fib)
		MemFree(fib, sizeof(FIB));
	return ((success) ? WIPE_SUCCESS : WIPE_FAIL);
}

/*
 *	Wipe contents of drawer, then delete drawer
 *	Note:  Must only be called with disk or drawer names!
 */

static BOOL WipeDrawer(TextPtr drawerName, BOOL confirm, BOOL doSubDrawers)
{
	WORD		i, type, errNum, result;
	ListHeadPtr	fileList;
	TextPtr		name;
	FIBPtr		fib;
	Dir			dir, origDir;

	dir = NULL;
	fib = NULL;
	fileList = NULL;
	result = WIPE_FAIL;
/*
	Get list of files/drawers in this drawer
*/
	if ((fib = MemAlloc(sizeof(FIB), 0)) == NULL || (fileList = CreateList()) == NULL) {
		errNum = ERR_NO_MEM;
		goto Exit;
	}
	if ((dir = Lock(drawerName, ACCESS_READ)) == NULL || !Examine(dir, fib)) {
		errNum = ERR_DIR_ACC;
		goto Exit;
	}
	while (ExNext(dir, fib)) {
		if (AddListItem(fileList, fib->fib_FileName, 0, TRUE) == -1)
			break;
	}
/*
	Wipe list of files/drawers
*/
	origDir = CurrentDir(dir);
	result = WIPE_SUCCESS;
	for (i = 0; i < NumListItems(fileList); i++) {
		name = GetListItem(fileList, i);
		type = GetEntryType(name);
		if (type == TYPE_FILE)
			result = WipeFile(name, confirm, FALSE);
		else if (doSubDrawers)
			result = WipeDrawer(name, confirm, doSubDrawers);
		else
			result = WIPE_FAIL;
		if (result == WIPE_ABORT)
			break;
	}
	CurrentDir(origDir);
	UnLock(dir);
	dir = NULL;
	if (result == WIPE_SUCCESS && GetEntryType(drawerName) == TYPE_DRAWER &&
		!DeleteFile(drawerName)) {
		WipeError(drawerName, TYPE_DRAWER);
		result = WIPE_FAIL;
	}
/*
	All done
*/
Exit:
	if (fileList)
		DisposeList(fileList);
	if (fib)
		MemFree(fib, sizeof(FIB));
	if (dir)
		UnLock(dir);
	return (result);
}

/*
 * 	main wipe procedure
 */

void DoWipeFile()
{
	WORD		i, type, result;
	LONG		lock;
	ListHeadPtr	fileList;
	TextChar	name[MAX_FILENAME_LEN + 5];

	fileList = NULL;
	SetStdPointer(window, POINTER_WAITDELAY);
/*
	Build list of selected files/drawers
*/
	if ((fileList = CreateList()) == NULL) {
		Error(ERR_NO_MEM);
		goto Exit;
	}
	i = -1;
	while ((i = SLNextSelect(scrollList, i)) != -1) {
		SLGetItem(scrollList, i, name);
		if (DiskList())
			strcat(name, ":");
		if (AddListItem(fileList, name + 1, 0, TRUE) == -1)	// Skip over first byte
			break;
	}
/*
	Wipe files/drawers
*/
	for (i = 0; i < NumListItems(fileList); i++) {
		result = WIPE_FAIL;
		strcpy(name, GetListItem(fileList, i));
		type = GetEntryType(name);
		if (type == TYPE_FILE)
			result = WipeFile(name, options.Confirm, FALSE);
		else if (type == TYPE_DRAWER || type == TYPE_DISK)
			result = WipeDrawer(name, options.Confirm, options.WipeSubdrawers);
		if (result == WIPE_ABORT)
			break;
		if (result == WIPE_SUCCESS && type != TYPE_DISK && options.WipeIcons) {
			strcat(name, ".info");
			if ((lock = Lock(name, ACCESS_READ)) != NULL) {
				UnLock(lock);
				(void) WipeFile(name, FALSE, FALSE);
			}
		}
	}
/*
	All done
*/
Exit:
	if (fileList)
		DisposeList(fileList);
	SetStdPointer(window, POINTER_ARROW);
}

/*
 *	Wipe files listed from the CLI
 */

BOOL DoCLIWipeFile(int argc, char *argv[])
{
	WORD		i;
	TextChar	error[80];
	TextPtr		text;

	for (i = 1; i < argc; i++) {
		text = argv[i];
		if (*text++ == '-')
			return (FALSE);
	}
	for (i=1; i < argc; i++) {
		if (WipeFile(argv[i],FALSE, TRUE) == WIPE_FAIL) {
			strcpy(error,strErrorCLIWipe);
			strcat(error,argv[i]);
			printf("%s\n",error);
		}
	}
	return (TRUE);
}
