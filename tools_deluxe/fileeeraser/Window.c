/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Wipe File/Disk
 *
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Window routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <dos/dos.h>
#include <dos/dosextens.h>

#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/layers.h>
#include <proto/dos.h>
#include <proto/graphics.h>

#include <string.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Image.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "WipeFile.h"
#include "Proto.h"

/*
 *  External variables
 */

extern TextChar	screenTitle[], strFile[], strDrawer[], strWipeError[];

extern TextPtr	errMessage[];

extern ScrollListPtr	scrollList;

extern DlgTemplPtr	dlgList[];
extern ReqTemplPtr	reqList[];

extern WindowPtr	window;

extern MsgPortPtr mainMsgPort;

extern BOOL	titleChanged;

/*
 * 	Local variables and definitions
 */

#define ERROR_TEXT 	1

/*
 *	Local prototypes
 */

static void	DrawWindow(void);

/*
 *  Create and draw main window
 */

void CreateWindow()
{
/*
	Initialize
*/
	SetDefaultButtons(OK_BUTTON, CANCEL_BUTTON);

	if ((scrollList = NewScrollList(SL_MULTISELECT)) == NULL) {
		ShutDown();
		return;
	}
/*
	Bring up dialog
*/
	AutoActivateEnable(FALSE);
	window = GetDialog(dlgList[DLG_MAIN], NULL, mainMsgPort);
	AutoActivateEnable(TRUE);
	if (window == NULL)
		return;
	SetStdPointer(window, POINTER_WAIT);
    SetWindowTitles(window, (UBYTE *) -1, screenTitle);
	ModifyIDCMP(window, window->IDCMPFlags | (DISKINSERTED | DISKREMOVED));
	SetWindowGlobals();
	InitScrollList(scrollList, GadgetItem(window->FirstGadget, FILE_LIST), window, NULL);
	DrawWindow();
/*
	Read file names, and activate appropriate gadgets
*/
	OffButtons();
	if (!ShowDirName()) {
		ShutDown();
		return;
	}
	SetBackButton();
	GetFileList(NULL, 0, NULL);
	SetButtons();
	SetStdPointer(window, POINTER_ARROW);
}

/*
 *	Remove main window
 */

void RemoveWindow()
{
	register RegionPtr	clipRegion;
	ImagePtr			iconImage;

	if (window) {
		clipRegion = (RegionPtr)InstallClipRegion(window->WLayer, NULL);
		iconImage = GadgetItem(window->FirstGadget, DISK_ICON)->GadgetRender;
		DisposeDialog(window);
		FreeImage(iconImage);
		DisposeScrollList(scrollList);
		window = NULL;
		if (clipRegion) DisposeRegion(clipRegion);
	}
}

/*
 *	Draw window contents
 */

static void DrawWindow()
{
	SLDrawBorder(scrollList);
	SLDrawList(scrollList);
	OutlineButton(GadgetItem(window->FirstGadget, OK_BUTTON), window, NULL, TRUE);
}

/*
 *	Refresh window
 */

void RefreshWindow()
{
	RastPtr		rPort;
	Rectangle	rect;

	GetWindowRect(window, &rect);
	if (!EmptyRect(&rect)) {
/*
	If old color scheme, fill with _tbPenLight
	Note: Cannot call RefreshGadgets() within Begin/EndRefresh()
*/
		if (_tbPenLight != 0) {
			rPort = window->RPort;
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &rect);
			RefreshGadgets(GadgetItem(window->FirstGadget, 0), window, NULL);
		}
		BeginRefresh(window);
		DrawWindow();
		EndRefresh(window, TRUE);
	}
}

/*
 *	Return screen titles to default setting
 */

void FixTitle()
{
	SetWindowTitles(window, (TextPtr) -1, screenTitle);
	titleChanged = FALSE;
}

/*
 *  Error report routine
 *  Put error message on screen title bar
 */

void Error(register WORD errNum)
{
	TextChar	num[4];
	WORD		error;
	WORD		len = strlen(errMessage[errNum]);

	if (errNum > ERR_MAX_ERROR)
		errNum = ERR_UNKNOWN;

	dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = errMessage[errNum];
	if (errNum == ERR_DOSERR) {
		error = IoErr();
		if (error != 202) {
			NumToString(error,num);
			strcat(dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info,num);
		}
		else
			dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = errMessage[ERR_INUSE];
	}

	if (StdAlert(dlgList[DLG_ERROR], window->WScreen, mainMsgPort, DialogFilter) == -1) {
		SetWindowTitles(window, (UBYTE *) -1, errMessage[errNum]);
   		DisplayBeep(window->WScreen);
    	SysBeep(5);
		titleChanged = TRUE;
	}
	if (errNum == ERR_DOSERR)
		errMessage[errNum][len] = '\0';
}

/*
 *  Error report routine
 *  Put error message on screen title bar
 */

void WipeError(TextPtr name, register WORD type)
{
	TextChar	errText[200];

	if (type == TYPE_DISK)
		return;
	strcpy(errText, strWipeError);
	strcat(errText, (type == TYPE_FILE) ? strFile : strDrawer);
	strcat(errText,"\n  \"");
	strcat(errText, name);
	strcat(errText, "\"");

	dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = errText;
	if (StdAlert(dlgList[DLG_ERROR], window->WScreen, mainMsgPort, DialogFilter) == -1) {
		SetWindowTitles(window, (UBYTE *) -1, errText);
   		DisplayBeep(window->WScreen);
    	SysBeep(5);
		titleChanged = TRUE;
	}
}

/*
 *	Display help requester
 */

void DoHelp()
{
	DialogPtr	dlg;

	if ((dlg = GetDialog(dlgList[DLG_ABOUT], window->WScreen, mainMsgPort)) == NULL)
		Error(ERR_NO_MEM);
	else {
		OutlineButton(GadgetItem(dlg->FirstGadget, OK_BUTTON),dlg,NULL,TRUE);
		(void) ModalDialog(mainMsgPort, dlg, DialogFilter);
		DisposeDialog(dlg);
	}
}

/*
 *	Confirm that user wants to wipe wipename
 *	Return YES, NO, or ABORT button number
 */

WORD ConfirmWipe(TextPtr wipeName)
{
	WORD		item;
	RequestPtr 	req;

	SetDefaultButtons(NO_BUTTON, ABORT_BUTTON);
	reqList[REQ_VERIFY]->Gadgets[CONFIRMNAME_TEXT].Info = wipeName;
	if ((req = GetRequest(reqList[REQ_VERIFY], window, TRUE)) == NULL) {
		Error(ERR_NO_MEM);
		item = ABORT_BUTTON;
	}
	else {
		SetStdPointer(window, POINTER_ARROW);
		OutlineButton(GadgetItem(req->ReqGadget, NO_BUTTON), window, req, TRUE);
		SysBeep(5);
		item = ModalRequest(mainMsgPort, window, DialogFilter);
		SetStdPointer(window, POINTER_WAITDELAY);
		EndRequest(req, window);
		DisposeRequest(req);
	}
	SetDefaultButtons(OK_BUTTON, CANCEL_BUTTON);
	return (item);
}

/*
 *	Filter function for ModalDialog/ModalRequest
 *	Handles window updates
 */

BOOL DialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	RequestPtr	req;

	if (intuiMsg->IDCMPWindow == window && intuiMsg->Class == REFRESHWINDOW) {
		ReplyMsg((MsgPtr) intuiMsg);
		if ((req = window->FirstRequest) != NULL)
			OutlineButton(GadgetItem(req->ReqGadget, OK_BUTTON), window, req, TRUE);
		RefreshWindow();
		*item = -1;
		return (TRUE);
	}
	return (FALSE);
}
