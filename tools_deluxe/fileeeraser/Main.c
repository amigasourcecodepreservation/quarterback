/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Wipe File
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Main procedure
 */

#include <proto/exec.h>
#include <proto/intuition.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/ScrollList.h>

#include "WipeFile.h"
#include "Proto.h"

/*
	external variables
 */

extern WindowPtr 		window;
extern MsgPortPtr		mainMsgPort;
extern BOOL 			titleChanged;
extern ScrollListPtr	scrollList;

/*
 *	Local variables and definitions
 */

static BOOL	quitFlag;

#define RAWKEY_HELP	0x5F

/*
 *	Prototypes
 */

static WORD	MainDialogFilter(IntuiMsgPtr, WORD *);
static void DoIntuiMessage(IntuiMsgPtr);
static void DoMainDialogItem(WORD);

/*
 *	Main routine
 */

void main(int argc, char *argv[])
{
	IntuiMsgPtr	intuiMsg;

	if (argc >= 2 && DoCLIWipeFile(argc, argv))
		return;
/*
	Set up
*/
	Init(argc,argv);

/*
	Process messages
*/
	quitFlag = FALSE;
	while (!quitFlag) {
		while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort))
			DoIntuiMessage(intuiMsg);
		if (!quitFlag)
			Wait(1L << mainMsgPort->mp_SigBit);
	}
/*
	All done
*/
	ShutDown();
}

/*
 *	Handle GetFile and PutFile dialog intuiMsg
 */

static BOOL MainDialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	ULONG		class = intuiMsg->Class;
	UWORD		code = intuiMsg->Code;
	UWORD		modifier = intuiMsg->Qualifier;
	WORD		gadgNum;
	GadgetPtr	gadgList = window->FirstGadget;

	if (intuiMsg->IDCMPWindow == window) {
		switch (class) {
/*
	Handle gadget up/down in scroll list gadgets
		and ALT key down for "Back" button
*/
		case GADGETDOWN:
		case GADGETUP:
			gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
			if (gadgNum == FILE_LIST || gadgNum == FILE_SCROLL ||
				gadgNum == FILE_UP || gadgNum == FILE_DOWN) {
				SLGadgetMessage(scrollList, mainMsgPort, intuiMsg);
				*item = gadgNum;
				return (TRUE);
			}
			break;
		case RAWKEY:
			if (code == CURSORLEFT || code == CURSORRIGHT) {
				if (code == CURSORLEFT)
					gadgNum = (modifier & ALTKEYS) ? DISK_BUTTON : BACK_BUTTON;
				else
					gadgNum = ENTER_BUTTON;
				if (DepressGadget(GadgetItem(gadgList, gadgNum), window, NULL))
					*item = gadgNum;
				return (TRUE);
			}
			break;
/*
	Handle disk inserted or removed
*/
		case DISKINSERTED:
		case DISKREMOVED:
			ReplyMsg((MsgPtr) intuiMsg);
			*item = SFPMSG_DISK;
			return (TRUE);
		}
	}
	return (FALSE);
}

static void DoIntuiMessage(register IntuiMsgPtr intuiMsg)
{
	register ULONG 	class;
	register UWORD 	code;
	WORD 		   	itemHit;
	UWORD			modifier;

	class		= intuiMsg->Class;
	code		= intuiMsg->Code;
	modifier	= intuiMsg->Qualifier;
/*
	Process the message
*/
	if (MainDialogFilter(intuiMsg, &itemHit))
		DoMainDialogItem(itemHit);
	else if (DialogSelect(intuiMsg, &window, &itemHit)) {
		ReplyMsg((MsgPtr) intuiMsg);
		DoMainDialogItem(itemHit);
	}
	else {
		ReplyMsg((MsgPtr) intuiMsg);
		switch (class) {
		case REFRESHWINDOW:
			RefreshWindow();
			break;
		case RAWKEY:
			switch (code) {
			case RAWKEY_HELP:
				DoHelp();
				break;
			case CURSORUP:
			case CURSORDOWN:
				SLCursorKey(scrollList, code);
				break;
			}
		}
	}
}

/*
 *	Handle dialog item
 */

static void DoMainDialogItem(WORD item)
{
	switch (item) {
		case DLG_CLOSE_BOX:
			quitFlag = TRUE;
			break;
		case -1:				/* INTUITICKS message */
			SetButtons();
			break;
		case FILE_LIST:
			item = DoFileList();
			if (item == ENTER_BUTTON)
				goto DoNewDir;
			if (item != WIPE_BUTTON)
				break;				/* Else fall through */
		case WIPE_BUTTON:
			DoWipeFile();
			DoDirSwitch(SFPMSG_RELIST, NULL, 0, NULL);
			break;
		case OPTIONS_BUTTON:
			DoOptions();
			break;
		case ENTER_BUTTON:
		case BACK_BUTTON:
		case DISK_BUTTON:
		case SFPMSG_DIRTOP:
		case SFPMSG_DISK:
		case SFPMSG_RELIST:
DoNewDir:
			DoDirSwitch(item, NULL, 0, NULL);
			break;
	}
}