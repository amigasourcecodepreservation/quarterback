/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	File Encrypt
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Routine to get file name
 */

#include <clib/macros.h>
#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <workbench/workbench.h>

#include <dos/dos.h>
#include <dos/dosextens.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>
#include <proto/icon.h>

#include <Typedefs.h>

#include <Toolbox/Memory.h>
#include <Toolbox/List.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/IntuiText.h>
#include <Toolbox/Request.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>
#include <Toolbox/DOS.h>

#include "Encrypt.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr		window;
extern MsgPortPtr		mainMsgPort;

extern struct Library	*IconBase;

extern DlgTemplPtr		dlgList[];

extern TextChar	strSystemText[];

extern UBYTE	_tbPenLight;

extern ScrollListPtr	scrollList;

/*
 *	Local variables and definitions
 */

typedef struct DiskObject		DiskObject, *DiskObjPtr;

#define MAXFILELEN	(30-5)			/* 5 chars for ".info" */

#define GET_DIALOG		0

static DialogPtr	sfpDialog;
static MsgPortPtr	sfpMsgPort;

static BOOL			atTop;

/*
 *	Disk and drawer icons
 */

static UWORD iconColors[] = {
	0x00F, 0xFFF, 0x000, 0xF60
};

static ULONG __chip systemIconData[] = {
	0xFC000000, 0x7E00FDFF, 0xFFFF7E00, 0xFD800003, 0x7E00FD80, 0x00037E00,
	0xFD800003, 0x7E00FD80, 0x00037E00, 0xFD800003, 0x7E00FD80, 0x00037E00,
	0xFD800003, 0x7E00FD80, 0x00037E00, 0xFD800003, 0x7E00FDFF, 0xFFFF7E00,
	0xFC000000, 0x7E00F000, 0x00001E00, 0xEFFFFFFF, 0xEE00D924, 0x92493600,
	0xB24C9264, 0x9A006499, 0x93324C00, 0x7FFFFFFF, 0xFC008000, 0x00000200,

	0x03FFFFFF, 0x80000200, 0x00008000, 0x02000000, 0x80000200, 0x00008000,
	0x02000000, 0x80000200, 0x00008000, 0x02000000, 0x80000200, 0x00008000,
	0x02000000, 0x80000200, 0x00008000, 0x02000000, 0x80000200, 0x00008000,
	0x03FFFFFF, 0x80000FFF, 0xFFFFE000, 0x10000000, 0x100026DB, 0x6DB6C800,
	0x4DB36D9B, 0x64009B66, 0x6CCDB200, 0x80000000, 0x02007FFF, 0xFFFFFC00,
};

static ULONG systemIconMask[] = {
	0x03FFFFFF, 0x800003FF, 0xFFFF8000, 0x03FFFFFF, 0x800003FF, 0xFFFF8000,
	0x03FFFFFF, 0x800003FF, 0xFFFF8000, 0x03FFFFFF, 0x800003FF, 0xFFFF8000,
	0x03FFFFFF, 0x800003FF, 0xFFFF8000, 0x03FFFFFF, 0x800003FF, 0xFFFF8000,
	0x03FFFFFF, 0x80000FFF, 0xFFFFE000, 0x1FFFFFFF, 0xF0003FFF, 0xFFFFF800,
	0x7FFFFFFF, 0xFC00FFFF, 0xFFFFFE00, 0xFFFFFFFF, 0xFFE07FFF, 0xFFFFFC00,
};

static ULONG __chip diskIconData[] = {
	0x00030FF1, 0x0F900F90, 0x0F900FF0, 0x00000000,
	0x00003FFC, 0x3FFC3FFC, 0x3FFC3FFC, 0x3FFC0000,

	0xFFFCFFFE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFC003, 0xC003C003, 0xC003C003, 0xC003FFFF,
};

static ULONG diskIconMask[] = {
	0xFFFCFFFE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF
};

static ULONG __chip drawerIconData[] = {
    0x00000000, 0x7FFFFFFE, 0x60000006, 0x6FFFFFF6,
    0x6FFFFFF6, 0x6FF7EFF6, 0x6FF00FF6, 0x6FFFFFF6,
    0x6FFFFFF6, 0x60000006, 0x7FFFFFFE, 0x00000000,

    0xFFFFFFFF, 0x80000001, 0x9FFFFFF9, 0x90000009,
    0x90000009, 0x90081009, 0x900FF009, 0x90000009,
    0x90000009, 0x9FFFFFF9, 0x80000001, 0xFFFFFFFF,
};

static Icon systemIcon = {
	39, 20, 2, &iconColors[0], (PLANEPTR) &systemIconData[0], (PLANEPTR) &systemIconMask[0]
};

static Icon diskIcon = {
	16, 16, 2, &iconColors[0], (PLANEPTR) &diskIconData[0], (PLANEPTR) &diskIconMask[0]
};

static Icon drawerIcon = {
	32, 12, 2, &iconColors[0], (PLANEPTR) &drawerIconData[0], NULL
};

/*
 *	Local prototypes
 */

static BOOL	ValidFile(FIBPtr);
static void	ShowFileList(ListHeadPtr, ListHeadPtr);
static BOOL	IsDirItem(WORD);
static WORD	CheckButtons(void);
static WORD	EnterDir(void);
static WORD	ExitDir(BOOL);

/*
 *	Display the new directory/volume name and icon in dialog
 *	Return success status
 */

BOOL ShowDirName()
{
	WORD				position;
	BOOL				success;
	Dir					currentDir, parentDir;
	FIBPtr				dirFIB;
	register GadgetPtr	nameGadg, iconGadg;
	GadgetPtr			gadgList = sfpDialog->FirstGadget;
	RastPtr				rPort = sfpDialog->RPort;
	IconPtr				icon;
	ImagePtr			iconImage;
	TextPtr				dirName;
	Rectangle			rect;

	if ((dirFIB = MemAlloc(sizeof(FIB), MEMF_CLEAR)) == NULL)
		return (FALSE);
	nameGadg = GadgetItem(gadgList, DISK_NAME);
	iconGadg = GadgetItem(gadgList, DISK_ICON);
	success = FALSE;
	currentDir = GetCurrentDir();
/*
	Display the dir/volume name
*/
	if (atTop)
		dirName = strSystemText;
	else if (Examine(currentDir, dirFIB))
		dirName = dirFIB->fib_FileName;
	else
		goto Exit;
	SetGadgetText(nameGadg, sfpDialog, NULL, dirName);
/*
	Display the icon (erasing old one first)
*/
	if ((iconImage = iconGadg->GadgetRender) != NULL) {
		GetGadgetRect(iconGadg, sfpDialog, NULL, &rect);
		rect.MinX += iconImage->LeftEdge;
		rect.MinY += iconImage->TopEdge;
		rect.MaxX = rect.MinX + iconImage->Width;
		rect.MaxY = rect.MinY + iconImage->Height;
		SetAPen(rPort, _tbPenLight);
		RectFill(rPort, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
	}
	position = RemoveGadget(sfpDialog, iconGadg);
	FreeImage(iconImage);
	if (atTop)
		icon = &systemIcon;
	else if ((parentDir = ParentDir(currentDir)) != NULL) {
		UnLock(parentDir);
		icon = &drawerIcon;
	}
	else
		icon = &diskIcon;
	iconGadg->GadgetRender = iconImage = MakeIconImage(icon, 0, 0, ICON_BOX_NONE);
	iconImage->LeftEdge = 0;
	iconImage->TopEdge = -iconImage->Height/2;
	AddGList(sfpDialog, iconGadg, position, 1, NULL);
	RefreshGList(iconGadg, sfpDialog, NULL, 1);
	success = TRUE;
/*
	All done
*/
Exit:
	MemFree(dirFIB, sizeof(FIB));
	return (success);
}

/*
 *	Return TRUE if fib entry is valid for listing
 */

static BOOL ValidFile(FIBPtr fib)
{
	WORD	len;
	TextPtr	fileName;

	fileName = fib->fib_FileName;
	len = strlen(fileName);
	if (fileName[0] == '.' ||
		(len > 4 && CmpString(fileName + len - 5, ".info", 5, 5, FALSE) == 0) ||
		fib->fib_Size == 0 ||
		(len == 1 && fileName[0] == '*'))
		return (FALSE);
	return (TRUE);
}

/*
 *	Get directory/file list for current directory and display
 *	If at top of tree, save list of mounted volumes in file list
 *		and list of path assignments in directory list
 */

void GetFileList(BOOL (*fileFilter)(TextPtr), WORD numTypes, TextPtr typeList[])
{
	register TextPtr	toolType, fileName;
	register WORD		i, len;
	register BOOL		addFile;
	DosListPtr			dosEntry;
	Dir					currentDir;
	DiskObjPtr			diskObj;
	ListHeadPtr			dirList, fileList, list;
	FIBPtr				fib;
	TextChar			nameBuff[50];

/*
	Allocate needed items
*/
	if ((fib = MemAlloc(sizeof(FIB), MEMF_CLEAR)) == NULL)
		goto Exit4;
	if ((dirList = CreateList()) == NULL)
		goto Exit3;
	if ((fileList = CreateList()) == NULL)
		goto Exit2;
/*
	If at top, get list of mounted volumes
*/
	if (atTop) {
		dosEntry = LockDosList1(LDF_VOLUMES | LDF_READ);
		while ((dosEntry = NextDosEntry1(dosEntry, LDF_VOLUMES | LDF_READ)) != NULL) {
			if ((dosEntry->dol_Type == DLT_VOLUME && dosEntry->dol_Task != NULL) ||
				dosEntry->dol_Type == DLT_DIRECTORY) {
				fileName = (TextPtr) BADDR(dosEntry->dol_Name);
				len = *fileName;
				BlockMove(fileName + 1, nameBuff, len);
				nameBuff[len] = '\0';
				list = (dosEntry->dol_Type == DLT_VOLUME) ? fileList : dirList;
				if (nameBuff[0]) {
					if (AddListItem(list, nameBuff, len, TRUE) == -1)
						break;
				}
			}
		}
		UnLockDosList1(LDF_VOLUMES | LDF_READ);
	}
/*
	Otherwise, get directory list
*/
	else {
		currentDir = GetCurrentDir();
		if (!Examine(currentDir, fib) || fib->fib_DirEntryType <= 0)
			goto Exit1;
		fileName = fib->fib_FileName;
		while (ExNext(currentDir, fib)) {
			if (CheckButtons())
				goto Exit1;
/*
	If directory entry, add entry to directory list
*/
			if (fib->fib_DirEntryType > 0) {
				if (AddListItem(dirList, fileName, 0, TRUE) == -1)
					break;
			}
/*
	Else if file, then add to list of files
*/
			else if (fib->fib_DirEntryType < 0) {
				if (!ValidFile(fib))
					continue;
/*
	Check to see if this file should be included in list
	(avoid reading icon if fileFilter says to list it)
*/
				addFile = FALSE;
				if (fileFilter)
					addFile = (*fileFilter)(fileName);
				else if (numTypes == 0)
					addFile = TRUE;
				if (!addFile && numTypes && IconBase &&
					(diskObj = GetDiskObject(fileName)) != NULL) {
					if ((toolType = FindToolType(diskObj->do_ToolTypes, "FILETYPE"))
						!= NULL) {
						for (i = 0; !addFile && i < numTypes; i++)
							addFile = MatchToolValue(toolType, typeList[i]);
					}
					FreeDiskObject(diskObj);
				}
/*
	Add file to list
*/
				if (addFile && AddListItem(fileList, fileName, 0, TRUE) == -1)
					break;
			}
		}
	}
/*
	Show the file lists and dispose of items
*/
Exit0:
	ShowFileList(dirList, fileList);
Exit1:
	MemFree(fib, sizeof(FIB));
Exit2:
	DisposeList(dirList);
Exit3:
	DisposeList(fileList);
Exit4:
	return;			/* Keeps compiler happy */
}

/*
 *	Display the new list of directories/files in dialog
 *	Mark directory names with a non-space character before name
 */

static void ShowFileList(ListHeadPtr dirList, ListHeadPtr fileList)
{
	WORD		i, j, len, numItems, numScroll;
	TextPtr		text;
	TextChar	buff[50];

	SLDoDraw(scrollList, FALSE);
/*
	Add file names
*/
	numItems = NumListItems(fileList);
	buff[0] = ' ';
	for (i = 0; i < numItems; i++) {
		text = GetListItem(fileList, i);
		strcpy(buff + 1, text);
		SLAddItem(scrollList, buff, 0, i);
	}
/*
	If at top, put separator between volume and path lists
*/
	if (atTop) {
		numScroll = SLNumItems(scrollList);
		SLEnableItem(scrollList, numScroll, FALSE);
	}
/*
	Sort directory names into file list
	Don't put flag in front of path assignments
*/
	numItems = NumListItems(dirList);
	for (i = 0; i < numItems; i++) {
		text = GetListItem(dirList, i);
		len = strlen(text);
		numScroll = SLNumItems(scrollList);
		if (atTop)
			j = numScroll;
		else {
			for (j = 0; j < numScroll; j++) {
				SLGetItem(scrollList, j, buff);
				if (CmpString(text, buff + 1, len, strlen(buff + 1), FALSE) < 0)
					break;
			}
		}
		buff[0] = (atTop) ? ' ' : '-';
		strcpy(buff + 1, text);
		SLAddItem(scrollList, buff, 0, j);
		SLSetItemStyle(scrollList, j, FSF_ITALIC);
	}
	SLDoDraw(scrollList, TRUE);
}

/*
 *	Clear the file list in dialog
 */

void ClearFileList()
{
	SLRemoveAll(scrollList);
}

/*
 *	Return TRUE if specified list item is a directory (not a file name)
 */

static BOOL IsDirItem(WORD item)
{
	TextChar	buff[50];

	SLGetItem(scrollList, item, buff);
	return ((BOOL) (atTop || buff[0] != ' '));
}

/*
 *	Disable all but "Disk" and "Cancel" buttons
 */

void OffButtons()
{
	GadgetPtr	gadgList = sfpDialog->FirstGadget;

	EnableGadgetItem(gadgList, ENCRYPT_BUTTON, sfpDialog, NULL, FALSE);
	EnableGadgetItem(gadgList, DECRYPT_BUTTON, sfpDialog, NULL, FALSE);
	EnableGadgetItem(gadgList, ENTER_BUTTON, sfpDialog, NULL, FALSE);
	EnableGadgetItem(gadgList, BACK_BUTTON, sfpDialog, NULL, FALSE);
}

/*
 *	Set "Back" button on or off
 */

void SetBackButton()
{
	EnableGadgetItem(sfpDialog->FirstGadget, BACK_BUTTON, sfpDialog, NULL, !atTop);
}

/*
 *	Set dialog gadgets to enabled or disabled as appropriate
 */

void SetButtons()
{
	WORD		selItem,nextSelItem;
	BOOL		enable;
	GadgetPtr	gadgList = sfpDialog->FirstGadget;

	SetBackButton();
/*
	Enable "Enter" button if a directory is selected
*/
	selItem = SLNextSelect(scrollList, -1);
	nextSelItem = SLNextSelect(scrollList,selItem);
	enable = (selItem >= 0 && nextSelItem == -1 && IsDirItem(selItem));
	EnableGadgetItem(gadgList, ENTER_BUTTON, sfpDialog, NULL, enable);
/*
	Enable "Encrypt" and "Decrypt" button if there is a file name
*/
	enable = selItem >= 0;
	if (enable) {
		for (selItem; selItem != -1 && !IsDirItem(selItem);
					selItem = SLNextSelect(scrollList,selItem));
		enable = selItem == -1;
	}
	EnableGadgetItem(gadgList, ENCRYPT_BUTTON, sfpDialog, NULL, enable);
	EnableGadgetItem(gadgList, DECRYPT_BUTTON, sfpDialog, NULL, enable);
}

/*
 *	Check to see if "Disk", "Back", or "Cancel" button is pressed
 *	Return TRUE if yes, FALSE if not
 */

static BOOL CheckButtons()
{
	register WORD			gadgNum;
	register BOOL			found;
	register IntuiMsgPtr	msg, nextMsg;
	GadgetPtr				gadget;

	found = FALSE;
	Forbid();
	msg = (IntuiMsgPtr) sfpMsgPort->mp_MsgList.lh_Head;
	while (nextMsg = (IntuiMsgPtr) msg->ExecMessage.mn_Node.ln_Succ) {
		if (msg->Class == GADGETUP && msg->IDCMPWindow == sfpDialog) {
			gadget = (GadgetPtr) msg->IAddress;
			if (gadget &&
				((gadgNum = GadgetNumber(gadget)) == DISK_BUTTON ||
					gadgNum == BACK_BUTTON)) {
				found = TRUE;
				break;
			}
		}
		msg = nextMsg;
	}
	Permit();
	return (found);
}

/*
 *	Enter the currently selected subdirectory
 *	Return TRUE if entered subdirectory, FALSE if not
 */

static BOOL EnterDir()
{
	WORD			selItem;
	register Dir	dir;
	TextChar		dirName[50];

	selItem = SLNextSelect(scrollList, -1);
	if (selItem < 0 || !IsDirItem(selItem))
		return (FALSE);
	SLGetItem(scrollList, selItem, dirName);
	if (atTop)
		strcat(dirName, ":");
	if ((dir = Lock(dirName + 1, ACCESS_READ)) == NULL)
		return (FALSE);
	SetCurrentDir(dir);
	UnLock(dir);
	atTop = FALSE;
	return (TRUE);
}

/*
 *	Back up one or all directory level(s)
 *	Return TRUE if backed up, FALSE if not
 */

static BOOL ExitDir(BOOL toTop)
{
	register Dir	dir, prevDir, currentDir;

	if (atTop)
		return (FALSE);
	currentDir = GetCurrentDir();
	if (toTop) {
		prevDir = NULL;
		for (dir = DupLock(currentDir); dir; dir = ParentDir(dir)) {
			if (prevDir)
				UnLock(prevDir);
			prevDir = dir;
		}
		SetCurrentDir(prevDir);
		UnLock(prevDir);
		atTop = TRUE;
	}
	else if ((dir = ParentDir(currentDir)) == NULL)
		atTop = TRUE;
	else {
		SetCurrentDir(dir);
		UnLock(dir);
	}
	return (TRUE);
}

/*
 *	Handle directory switch operations
 */

void DoDirSwitch(WORD item, BOOL (*fileFilter)(TextPtr), WORD numTypes,
						TextPtr typeList[])
{
	BOOL	success;

	OffButtons();
	SetStdPointer(sfpDialog, POINTER_WAIT);
	if (item == OK_BUTTON) {
		atTop = FALSE;
		success = TRUE;		/* Have already switched to directory */
	}
	else if (item == ENTER_BUTTON)
		success = EnterDir();
	else if (item == BACK_BUTTON || item == SFPMSG_DIRTOP)
		success = ExitDir((item == SFPMSG_DIRTOP));
	else if (item == DISK_BUTTON)
		success = ExitDir(TRUE);
	else if ((item == SFPMSG_DISK && atTop) || item == SFPMSG_RELIST)
		success = TRUE;
	else
		success = FALSE;
	if (success) {
		ClearFileList();
		ShowDirName();
		SetBackButton();
		GetFileList(fileFilter, numTypes, typeList);
	}
	SetButtons();
	SetStdPointer(sfpDialog, POINTER_ARROW);
}

/*
 *	Handle click in file list
 *	Return FILE_LIST if single-click, OK_BUTTON if double-click on file,
 *		ENTER_BUTTON if double-click on directory
 */

WORD DoFileList()
{
	WORD	selItem;

	selItem = SLNextSelect(scrollList, -1);
	if (selItem == -1)
		return (FILE_LIST);
	SetButtons();
	if (SLIsDoubleClick(scrollList))
		return ((IsDirItem(selItem)) ? ENTER_BUTTON : OK_BUTTON);
	return (FILE_LIST);
}

void SetWindowGlobals()
{
	sfpDialog = window;
	sfpMsgPort = mainMsgPort;
	atTop = FALSE;
}

/*
	return TRUE if current list is of mounted disks
 */

BOOL DiskList()
{
	return (atTop);
}
