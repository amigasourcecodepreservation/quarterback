/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *  Encrypt/Decrypt
 *
 *  Global function Prototypes
 */

#include <Toolbox/StdFile.h>

/*
 *  Main.c
 */

void 	main(int,char **);

/*
 *  Init.c
 */

void 	Init(int, char **);
void 	ShutDown(void);

/*
 *  Window.c
 */

void 	CreateWindow(void);
void	RemoveWindow(void);
void 	FixTitle(void);
void	Error(WORD);
void	TextError(WORD,TextPtr);
void	DoHelp(void);
void	GetFileToEncrypt(void);
void	RefreshWindow(void);
BOOL	DialogFilter(IntuiMsgPtr, WORD *);

/*
 *	GetFile.c
 */

WORD	ShowDirName(void);
void	OffButtons(void);
void	SetBackButton(void);
void	SetButtons(void);
void	ClearFileList(void);
void	GetFileList(BOOL (*)(TextPtr), WORD, TextPtr *);
void	DoDirSwitch(WORD, BOOL (*)(TextPtr), WORD, TextPtr *);
WORD	DoFileList(void);
void	SetWindowGlobals(void);
BOOL	DiskList(void);

/*
 *	Encrypt.c
 */

BOOL	EncryptFile(TextPtr);
BOOL	DecryptFile(TextPtr);