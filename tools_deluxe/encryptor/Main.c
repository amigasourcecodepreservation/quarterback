/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Encrypt
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Main procedure
 */

#include <proto/exec.h>
#include <proto/intuition.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/ScrollList.h>

#include <string.h>

#include "Encrypt.h"
#include "Proto.h"

/*
	external variables
 */

extern WindowPtr 		window;
extern MsgPortPtr		mainMsgPort;
extern BOOL 			titleChanged;
extern ScrollListPtr	scrollList;

/*
 *	Local variables and definitions
 */

static BOOL 			quitFlag = FALSE;

#define RAWKEY_HELP	0x5F

/*
 *	Prototypes
 */

static WORD	MainDialogFilter(IntuiMsgPtr, WORD *);
static void DoIntuiMessage(IntuiMsgPtr);
static void DoCursorKey(UWORD,UWORD);
static void DoMainDialogItem(WORD);

/*
 *	Main routine
 */

void main(int argc,char *argv[])
{
	register IntuiMsgPtr intuiMsg;

	Init(argc,argv);

	while (!quitFlag) {
		while (intuiMsg = (IntuiMsgPtr)GetMsg(mainMsgPort)) {
			if (intuiMsg->Class == CLOSEWINDOW) quitFlag = TRUE;
			else {
				DoIntuiMessage(intuiMsg);
			}
		}
		if (!quitFlag) Wait(1L<<mainMsgPort->mp_SigBit);
	}

	ShutDown();
}

/*
 *	Handle GetFile and PutFile dialog intuiMsg
 */

static BOOL MainDialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	ULONG		class = intuiMsg->Class;
	UWORD		code = intuiMsg->Code;
	UWORD		modifier = intuiMsg->Qualifier;
	WORD		gadgNum;
	GadgetPtr	gadgList = window->FirstGadget;

	if (intuiMsg->IDCMPWindow == window) {
		switch (class) {
/*
	Handle gadget up/down in scroll list gadgets
		and ALT key down for "Back" button
*/
		case GADGETDOWN:
		case GADGETUP:
			gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
			if (gadgNum == FILE_LIST || gadgNum == FILE_SCROLL ||
				gadgNum == FILE_UP || gadgNum == FILE_DOWN) {
				SLGadgetMessage(scrollList, mainMsgPort, intuiMsg);
				*item = gadgNum;
				return (TRUE);
			}
			break;
		case RAWKEY:
			if (code == CURSORLEFT || code == CURSORRIGHT) {
				if (code == CURSORLEFT)
					gadgNum = (modifier & ALTKEYS) ? DISK_BUTTON : BACK_BUTTON;
				else
					gadgNum = ENTER_BUTTON;
				if (DepressGadget(GadgetItem(gadgList, gadgNum), window, NULL))
					*item = gadgNum;
				return (TRUE);
			}
			break;
		case MOUSEBUTTONS:
			SetButtons();
			break;
/*
	Handle disk inserted or removed
*/
		case DISKINSERTED:
		case DISKREMOVED:
			ReplyMsg((MsgPtr) intuiMsg);
			*item = SFPMSG_DISK;
			return (TRUE);
		}
	}
	return (FALSE);
}

static void DoIntuiMessage(register IntuiMsgPtr intuiMsg)
{
	register ULONG 	class;
	register UWORD 	code;
	WORD 		   	itemHit;
	UWORD			modifier;

	class = intuiMsg->Class;
	code = intuiMsg->Code;
	modifier = intuiMsg->Qualifier;

	if (class == MENUVERIFY)	{
		if (code == MENUHOT)	{
			if (titleChanged)
				FixTitle();
			intuiMsg->Code = MENUCANCEL;
		}
	}
/*
	Process the message
*/
	if (MainDialogFilter(intuiMsg,&itemHit))
		DoMainDialogItem(itemHit);
	else if (DialogSelect(intuiMsg, &window, &itemHit)) {
		ReplyMsg((MsgPtr)intuiMsg);
		DoMainDialogItem(itemHit);
	}
	else {
		ReplyMsg((MsgPtr)intuiMsg);
		switch (class) {
			case REFRESHWINDOW:
				RefreshWindow();
				break;
			case RAWKEY:
				switch (code) {
					case RAWKEY_HELP:
						DoHelp();
						break;
					case CURSORUP:
					case CURSORDOWN:
					case CURSORLEFT:
					case CURSORRIGHT:
						DoCursorKey(code,modifier);
						break;
				}
		}
	}
}

static void DoCursorKey(UWORD code,UWORD modifier)
{
	register ScrollListPtr scrollListReg = scrollList;
	register WORD items;
	register WORD listItem;
	register WORD offItem;
	register WORD direction;
	BOOL select;
	BOOL bumpSel;

	if (code != CURSORUP && code != CURSORDOWN)
		return;

	listItem = SLNextSelect(scrollListReg, -1);
	items = SLNumItems(scrollList);
	bumpSel = SLNextSelect(scrollListReg, listItem) == -1;
	if( items && (listItem != -1) ) {
		if( code == CURSORUP ) {
			direction = -1;
			select = listItem != 0;
		} else {
			direction = 1;
			listItem--;
			do {
				offItem = SLNextSelect(scrollList, ++listItem);
			} while( (offItem - listItem) == 1 );
			select = (listItem+1) != SLNumItems(scrollList);
		}
		if( modifier & SHIFTKEYS ) {
			if( !select ) {
				SysBeep(5);
			}
			select = TRUE;
			bumpSel = TRUE;
		} else if( select || !bumpSel ) {
			while( (offItem = SLNextSelect(scrollList, -1)) != -1 ) {
				SLSelectItem(scrollList, offItem, FALSE);
			}
			if( !select ) {
				select = TRUE;
				direction = 0;
			}
		}
	}
	if( select ) {
		if( bumpSel ) {
			listItem += direction;
		}
		SLSelectItem(scrollList, listItem, TRUE);
		SLAutoScroll(scrollList, listItem);
		SetButtons();
	} else {
		SysBeep(5);
	}
}

static void DoMainDialogItem(WORD item)
{
	WORD  		fileNum = -1;
	TextChar	fileName[MAX_FILENAME_LEN],
				strBuff[MAX_FILENAME_LEN];


	switch (item) {
		case DLG_CLOSE_BOX:
			quitFlag = TRUE;
			break;
		case -1:				/* INTUITICKS message */
			SetButtons();
			break;
		case FILE_LIST:
			item = DoFileList();
			if (item == ENTER_BUTTON)
				goto DoNewDir;
			if (item != ENCRYPT_BUTTON)
				break;					// Else fall through
		case ENCRYPT_BUTTON:
			while ((fileNum = SLNextSelect(scrollList, fileNum)) != -1) {
				SLGetItem(scrollList, fileNum, strBuff);
				strcpy(fileName, strBuff + 1);		// Skip initial byte
				EncryptFile(fileName);
			}
			break;
		case DECRYPT_BUTTON:
			while ((fileNum = SLNextSelect(scrollList, fileNum)) != -1) {
				SLGetItem(scrollList, fileNum, strBuff);
				strcpy(fileName, strBuff + 1);		// Skip initial byte
				DecryptFile(fileName);
			}
			break;
		case ENTER_BUTTON:
		case BACK_BUTTON:
		case DISK_BUTTON:
		case SFPMSG_DIRTOP:
		case SFPMSG_DISK:
		case SFPMSG_RELIST:
DoNewDir:
			DoDirSwitch(item, NULL, 0, NULL);
			break;
	}
}