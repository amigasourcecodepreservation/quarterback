/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	File Encrypt
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Encrypt and Decrypt routines
 */

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Typedefs.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Request.h>
#include <Toolbox/ScrollList.h>

#include <stdio.h>
#include <string.h>

#include "Encrypt.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr	window;
extern MsgPortPtr	mainMsgPort;

extern ReqTemplPtr	reqList[];

extern ScrollListPtr	scrollList;

extern TextChar		strCheckPassword[],strFirstPassword[],strSecondPassword[];

/*
 *	Local variables and definitions
 */

#define PWLENGTH	20
#define BUFFER_LEN	512
#define PROMPT_LEN	100

enum {
	PWTEXT	= 2,
	PROMPTTEXT
};

static UBYTE	strBuff[BUFFER_LEN];

static TextChar	tempFileName[] = ".EncryptTempFile";

/*
 *	Array of conversion from and to encrypted data
 */

static TextChar encode[] = {
	0x00, 0x50, 0xD4, 0x59, 0xD9, 0xDD, 0x72, 0xD0, 0xC8, 0xB5, 0xCF, 0x21, 0x93, 0x58, 0x89, 0x1C,
	0xB4, 0xF2, 0x40, 0x37, 0x80, 0x7A, 0x2A, 0xE7, 0x94, 0xCC, 0x2E, 0xC4, 0x1B, 0x28, 0x7F, 0x05,
	0x39, 0x3C, 0x14, 0xA4, 0x22, 0x1A, 0x76, 0x99, 0x98, 0x84, 0x33, 0x16, 0xA5, 0x51, 0x68, 0xE0,
	0x7B, 0xFC, 0x83, 0x6C, 0x15, 0x6B, 0x01, 0xC1, 0x9F, 0x75, 0x7D, 0x09, 0x8F, 0x60, 0x36, 0x6D,
	0x02, 0x7E, 0xA8, 0x06, 0xF7, 0xB6, 0xED, 0x19, 0x18, 0x31, 0x25, 0x0D, 0xD2, 0x3A, 0x95, 0x24,
	0x56, 0xCD, 0x23, 0x57, 0x82, 0xB1, 0xBB, 0x9E, 0x9D, 0x2C, 0x0B, 0xE4, 0x12, 0xAE, 0x2B, 0xE8,
	0xC3, 0xEE, 0xA9, 0xDC, 0xCB, 0x79, 0x3B, 0x5F, 0xA2, 0xDE, 0xF4, 0xF8, 0x3D, 0x41, 0x64, 0xBA,
	0x77, 0xFD, 0x8B, 0x8A, 0x27, 0x53, 0x44, 0xC9, 0x4B, 0x91, 0xB8, 0x10, 0xAB, 0xFA, 0x6E, 0xCA,
	0x5A, 0xA1, 0xAC, 0xB7, 0xC2, 0x0F, 0xF9, 0x07, 0x08, 0x04, 0x74, 0xEB, 0x4C, 0x52, 0x65, 0x9C,
	0xF0, 0x70, 0x6F, 0x9B, 0x9A, 0x0A, 0x38, 0x30, 0xFF, 0x46, 0xE5, 0xBE, 0x62, 0x63, 0x20, 0x96,
	0x48, 0xA0, 0xE9, 0x55, 0x0C, 0x4D, 0x8D, 0x90, 0xD5, 0x43, 0xF5, 0xAA, 0x35, 0x34, 0x88, 0x03,
	0xD1, 0xDF, 0xEF, 0xEA, 0x11, 0x6A, 0xF1, 0x7C, 0x3E, 0x92, 0xF3, 0xE3, 0xEC, 0x26, 0xBC, 0x86,
	0xC0, 0x42, 0xDB, 0x5B, 0x1F, 0x29, 0x13, 0xD3, 0xE6, 0x71, 0x1E, 0xD8, 0x4A, 0xC6, 0x32, 0xBD,
	0xCE, 0x8C, 0xC5, 0x2F, 0x47, 0x67, 0xE1, 0x5C, 0xD7, 0xAF, 0x5D, 0xAD, 0xDA, 0xB2, 0x5E, 0x69,
	0xA3, 0xF6, 0x73, 0xA7, 0x0E, 0x45, 0x4F, 0x17, 0x61, 0xB9, 0xBF, 0xFE, 0x97, 0x66, 0xD6, 0xB3,
	0x78, 0x3F, 0x49, 0x87, 0x4E, 0xC7, 0x1D, 0x2D, 0xE2, 0xA6, 0x8E, 0x54, 0xB0, 0xFB, 0x85, 0x81
};

static TextChar decode[] = {
	0x00, 0x36, 0x40, 0xAF, 0x89, 0x1F, 0x43, 0x87, 0x88, 0x3B, 0x95, 0x5A, 0xA4, 0x4B, 0xE4, 0x85,
	0x7B, 0xB4, 0x5C, 0xC6, 0x22, 0x34, 0x2B, 0xE7, 0x48, 0x47, 0x25, 0x1C, 0x0F, 0xF6, 0xCA, 0xC4,
	0x9E, 0x0B, 0x24, 0x52, 0x4F, 0x4A, 0xBD, 0x74, 0x1D, 0xC5, 0x16, 0x5E, 0x59, 0xF7, 0x1A, 0xD3,
	0x97, 0x49, 0xCE, 0x2A, 0xAD, 0xAC, 0x3E, 0x13, 0x96, 0x20, 0x4D, 0x66, 0x21, 0x6C, 0xB8, 0xF1,
	0x12, 0x6D, 0xC1, 0xA9, 0x76, 0xE5, 0x99, 0xD4, 0xA0, 0xF2, 0xCC, 0x78, 0x8C, 0xA5, 0xF4, 0xE6,
	0x01, 0x2D, 0x8D, 0x75, 0xFB, 0xA3, 0x50, 0x53, 0x0D, 0x03, 0x80, 0xC3, 0xD7, 0xDA, 0xDE, 0x67,
	0x3D, 0xE8, 0x9C, 0x9D, 0x6E, 0x8E, 0xED, 0xD5, 0x2E, 0xDF, 0xB5, 0x35, 0x33, 0x3F, 0x7E, 0x92,
	0x91, 0xC9, 0x06, 0xE2, 0x8A, 0x39, 0x26, 0x70, 0xF0, 0x65, 0x15, 0x30, 0xB7, 0x3A, 0x41, 0x1E,
	0x14, 0xFF, 0x54, 0x32, 0x29, 0xFE, 0xBF, 0xF3, 0xAE, 0x0E, 0x73, 0x72, 0xD1, 0xA6, 0xFA, 0x3C,
	0xA7, 0x79, 0xB9, 0x0C, 0x18, 0x4E, 0x9F, 0xEC, 0x28, 0x27, 0x94, 0x93, 0x8F, 0x58, 0x57, 0x38,
	0xA1, 0x81, 0x68, 0xE0, 0x23, 0x2C, 0xF9, 0xE3, 0x42, 0x62, 0xAB, 0x7C, 0x82, 0xDB, 0x5D, 0xD9,
	0xFC, 0x55, 0xDD, 0xEF, 0x10, 0x09, 0x45, 0x83, 0x7A, 0xE9, 0x6F, 0x56, 0xBE, 0xCF, 0x9B, 0xEA,
	0xC0, 0x37, 0x84, 0x60, 0x1B, 0xD2, 0xCD, 0xF5, 0x08, 0x77, 0x7F, 0x64, 0x19, 0x51, 0xD0, 0x0A,
	0x07, 0xB0, 0x4C, 0xC7, 0x02, 0xA8, 0xEE, 0xD8, 0xCB, 0x04, 0xDC, 0xC2, 0x63, 0x05, 0x69, 0xB1,
	0x2F, 0xD6, 0xF8, 0xBB, 0x5B, 0x9A, 0xC8, 0x17, 0x5F, 0xA2, 0xB3, 0x8B, 0xBC, 0x46, 0x61, 0xB2,
	0x90, 0xB6, 0x11, 0xBA, 0x6A, 0xAA, 0xE1, 0x44, 0x6B, 0x86, 0x7D, 0xFD, 0x31, 0x71, 0xEB, 0x98
};

/*
	local prototypes
*/

static UBYTE 	MakeKey(TextPtr);
static void		SetPasswordPrompt(TextPtr,TextPtr,TextPtr);
static TextPtr 	CheckPassword(TextPtr,TextPtr);
static TextPtr	GetPassword(TextPtr);
static BOOL 	DecodeText(UBYTE *,LONG,TextPtr);
static TextPtr 	EncodeText(UBYTE *,ULONG,TextPtr,BOOL);

/*
 *  Make key for encoding and decoding of protected files
 */

static UBYTE MakeKey(TextPtr text)
{
	register WORD	i, ch, key, len;

	len = strlen(text);
	key = 0;
	for (i = 0; i < PWLENGTH; i++) {
		ch = (i <= len) ? text[i] : 0;
		key <<= 1;
		key += ch;
		key = ((key & 0xFF) + (key << 8)) & 0xFF;
	}
	return ((UBYTE) key);
}

/*
 *	set prompt for password to appropriate
 */

static void SetPasswordPrompt(TextPtr text, TextPtr fileName, TextPtr prompt)
{
	strcpy(prompt, text);
	if (fileName) {
		strcat(prompt, "\"");
		strcat(prompt, fileName);
		strcat(prompt, "\"");
	}
	reqList[REQ_PWREQUEST]->Gadgets[PROMPTTEXT].Info = prompt;
}

/*
 * 	compare password with saved password
 */

static TextPtr CheckPassword(TextPtr text,TextPtr fileName)
{
	register 	RequestPtr 	pwReq;
	WORD		item,i;
	BOOL		success = FALSE;
	TextPtr		newText;
	TextPtr		passTxt = NULL;
	GadgetPtr	gadgList;
	TextChar	prompt[PROMPT_LEN];

	AutoActivateEnable(FALSE);
	SetPasswordPrompt(strCheckPassword,fileName,prompt);

	if ((pwReq = GetRequest(reqList[REQ_PWREQUEST], window, TRUE)) == NULL)
		return (NULL);

	gadgList = pwReq->ReqGadget;

	OutlineButton(GadgetItem(gadgList, OK_BUTTON), window, pwReq, TRUE);

	for (i = 0; i < 3; i++) {
		SetEditItemText(gadgList, PWTEXT, window, pwReq, '\0');
		ActivateGadget(GadgetItem(gadgList, PWTEXT), window, pwReq);
		for (;;) {
			item = ModalRequest(mainMsgPort, window, DialogFilter);
			if (item == OK_BUTTON || item == CANCEL_BUTTON)
				break;
		}

		if (item == OK_BUTTON) {
			GetEditItemText(gadgList, PWTEXT, strBuff);
			if (strlen(strBuff) == 0) {
				Error(ERR_BAD_PW);
				continue;
			}
			if ((newText = EncodeText(strBuff,PWLENGTH,strBuff,TRUE)) == NULL) {
				Error(ERR_NO_MEM);
				goto Exit;
			}
			if (CmpString(text,newText,PWLENGTH,PWLENGTH,TRUE) == 0)
				success = TRUE;
			else
				Error(ERR_BAD_PW);
			MemFree(newText,PWLENGTH);
		}

		if (success || item == CANCEL_BUTTON)
			break;
	}

Exit:

	EndRequest(pwReq,window);
	DisposeRequest(pwReq);

	if (success) {
		if ((passTxt = MemAlloc(strlen(strBuff), MEMF_CLEAR)) == NULL)
			return (NULL);
		strcpy(passTxt, strBuff);
	}
	AutoActivateEnable(TRUE);
	return (passTxt);
}

/*
 * 	compare password with saved password
 */

static TextPtr	GetPassword(TextPtr fileName)
{
	register 	RequestPtr 	pwReq;
	WORD		item,firstLen,secondLen;
	TextChar	firstText[PWLENGTH],secondText[PWLENGTH];
	TextPtr		passTxt = NULL;
	GadgetPtr	gadgList;
	TextChar	prompt[PROMPT_LEN];

	SetPasswordPrompt(strFirstPassword,fileName,prompt);

	AutoActivateEnable(FALSE);
	if ((pwReq = GetRequest(reqList[REQ_PWREQUEST], window, TRUE)) == NULL)
		return (NULL);

	gadgList = pwReq->ReqGadget;

	OutlineButton(GadgetItem(gadgList, OK_BUTTON), window, pwReq, TRUE);

	for (;;) {
		SetEditItemText(gadgList,PWTEXT,window,pwReq,'\0');
		ActivateGadget(GadgetItem(gadgList,PWTEXT),window,pwReq);
		for (;;) {
			item = ModalRequest(mainMsgPort, window, DialogFilter);
			if (item == OK_BUTTON || item == CANCEL_BUTTON)
				break;
		}
		if (item == CANCEL_BUTTON)
			goto Exit;

		GetEditItemText(gadgList,PWTEXT,firstText);
		firstLen = strlen(firstText);
		if (firstLen == 0 || firstLen > PWLENGTH) {
			Error(ERR_BAD_PW);
			continue;
		}
		EndRequest(pwReq,window);
		DisposeRequest(pwReq);
/*
	get second password
 */
		AutoActivateEnable(FALSE);
		SetPasswordPrompt(strSecondPassword, NULL, prompt);

		if ((pwReq = GetRequest(reqList[REQ_PWREQUEST], window, TRUE)) == NULL)
			return (NULL);

		gadgList = pwReq->ReqGadget;

		OutlineButton(GadgetItem(gadgList, OK_BUTTON), window, pwReq, TRUE);
		for (;;) {
			SetEditItemText(gadgList, PWTEXT, window, pwReq, '\0');
			ActivateGadget(GadgetItem(gadgList, PWTEXT), window, pwReq);
			for (;;) {
				item = ModalRequest(mainMsgPort, window, DialogFilter);
				if (item == OK_BUTTON || item == CANCEL_BUTTON)
					break;
			}
			if (item == CANCEL_BUTTON)
				goto Exit;

			GetEditItemText(gadgList,PWTEXT,secondText);
			secondLen = strlen(secondText);
			if (CmpString(firstText,secondText,firstLen,secondLen,TRUE) == 0)
				goto Exit;
			Error(ERR_DIFF_PW);
		}
	}

Exit:

	EndRequest(pwReq,window);
	DisposeRequest(pwReq);
	AutoActivateEnable(TRUE);
	if (item == OK_BUTTON) {
		if ((passTxt = MemAlloc(firstLen + 1, MEMF_CLEAR)) == NULL)
			return (NULL);
		strcpy(passTxt, firstText);
	}
	return (passTxt);
}

/*
 *	decode text
 */

static BOOL DecodeText(UBYTE *text, LONG len, TextPtr password)
{
	TextPtr	newText;
	WORD	i;
	UBYTE	code;
	LONG	ch;

	if (len == 0)
		return (TRUE);

	if ((newText = MemAlloc(len,MEMF_CLEAR)) == NULL) {
		Error(ERR_NO_MEM);
		return (FALSE);
	}

	code = MakeKey(password);

	for (i=0; i < len; i++) {
		ch = (LONG) decode[text[i]];
		ch = ch - (LONG)(code + i);
		newText[i] = (UBYTE)ch;
	}
	BlockMove(newText, text, len);
	MemFree(newText, len);

	return (TRUE);
}

static TextPtr EncodeText(UBYTE *text,ULONG len,TextPtr password,BOOL isPassword)
{
	TextPtr		newText;
	ULONG		i;
	UWORD		total;
	UBYTE		code;
	UBYTE		ch;

	if ((newText = MemAlloc(len,MEMF_CLEAR)) == NULL) {
		Error(ERR_NO_MEM);
		return (NULL);
	}

	code = MakeKey(password);

	for (i=0; i < len; i++) {
		if (isPassword && i >= strlen(text))
			ch = 0;
		else
			ch = text[i];
		total = ((UWORD)ch + (UWORD)code + i) % 256;
		/* straight look up in table */
		newText[i] = encode[total];
	}
	return (newText);
}

/*
	encrypt file designated by fileNum
 */

BOOL EncryptFile(TextPtr fileName)
{
	File					oldFile,newFile;
	TextPtr					password,newText;
	BOOL					success;
	ULONG					amount;
/*
	init values
*/
	oldFile = newFile = NULL;
	newText = NULL;
	success = FALSE;

	if ((password = GetPassword(fileName)) == NULL)
		return (FALSE);

	SetStdPointer(window, POINTER_WAITDELAY);
	if (((oldFile = Open(fileName, MODE_OLDFILE)) == NULL) ||
		((newFile = Open(tempFileName, MODE_NEWFILE)) == NULL)) {
		Error(ERR_DOSERR);
		goto Exit;
	}
/*
	write password
 */
	amount = PWLENGTH;
	if ((newText = EncodeText(password, amount, password, TRUE)) == NULL)
		goto Exit;
	if (Write(newFile, newText, amount) != amount) {
		Error(ERR_DOSERR);
		goto Exit;
	}
	MemFree(newText, amount);

	BlockClear(strBuff, BUFFER_LEN);
	while ((amount = Read(oldFile, strBuff, BUFFER_LEN)) > 0) {
		if ((newText = EncodeText(strBuff, amount, password, FALSE)) == NULL)
			goto Exit;
		if (Write(newFile, newText, amount) != amount) {
			Error(ERR_DOSERR);
			goto Exit;
		}
		MemFree(newText, amount);
		BlockClear(strBuff, BUFFER_LEN);
	}

	success = TRUE;

Exit:
	if (newFile)
		Close(newFile);
	if (oldFile)
		Close(oldFile);
	if (password)
		MemFree(password,strlen(password));
	if (newText)
		MemFree(newText,amount);
/*
	clean up
*/
	if (success) {
		DeleteFile(fileName);
		Rename(tempFileName, fileName);
	}
	else {
		if (newFile)
			DeleteFile(tempFileName);
		TextError(ERR_ENCRYPT, fileName);
	}
	SetStdPointer(window, POINTER_ARROW);

	return (success);
}

/*
	decrypt file designated by fileNum
 */

BOOL DecryptFile(TextPtr fileName)
{
	File					oldFile,newFile;
	TextPtr					password;
	TextChar				savedPassword[PWLENGTH];
	BOOL					success;
	ULONG					amount;
/*
	 init values
*/
	newFile = NULL;
	password = NULL;
	success = FALSE;

	if ((oldFile = Open(fileName, MODE_OLDFILE)) == NULL) {
		Error(ERR_DOSERR);
		return (FALSE);
	}

	if (Read(oldFile, savedPassword, PWLENGTH) != PWLENGTH) {
		Error(ERR_DOSERR);
		goto Exit;
	}

	if ((password = CheckPassword(savedPassword, fileName)) == NULL)
		goto Exit;

	SetStdPointer(window, POINTER_WAITDELAY);
	if ((newFile = Open(tempFileName, MODE_NEWFILE)) == NULL) {
		Error(ERR_DOSERR);
		goto Exit;
	}
/*
	decode text
 */
	while ((amount = Read(oldFile, strBuff, BUFFER_LEN)) > 0) {
		if (!DecodeText(strBuff, amount, password))
			goto Exit;
		if (Write(newFile,strBuff,amount) != amount) {
			Error(ERR_DOSERR);
			goto Exit;
		}
	}

	success = TRUE;
Exit:
	if (newFile)
		Close(newFile);
	if (oldFile)
		Close(oldFile);
	if (password)
		MemFree(password, strlen(password));
/*
	clean up
*/
	if (success) {
		DeleteFile(fileName);
		Rename(tempFileName, fileName);
	}
	else {
		if (newFile)
			DeleteFile(tempFileName);
		TextError(ERR_DECRYPT, fileName);
	}
	SetStdPointer(window, POINTER_ARROW);

	return (success);
}
