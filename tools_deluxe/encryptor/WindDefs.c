/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Encrypt/Decrypt
 *
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Window Definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Language.h>
#include <Toolbox/Image.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Request.h>
#include <Toolbox/Border.h>

/*
 *	External variables
 */

extern TextChar strGetFile[], strFirstPassword[];

/*
 *	Standard gadget definitions
 */

#define GT_ADJUST_UPARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,													\
		left, (top)+5-ARROW_HEIGHT, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		CURSORKEY_UP, 0, (Ptr) IMAGE_ARROW_UP }

#define GT_ADJUST_DOWNARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,										\
		left, (top)+5, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		CURSORKEY_DOWN, 0, (Ptr) IMAGE_ARROW_DOWN }

#define GT_ADJUST_TEXT(left, top)	\
	{ GADG_STAT_TEXT, (left)+10+ARROW_WIDTH, top, 0, 0, 0, 0, 0, 0, 0, 0, NULL }

/*
	cursur key equivalents
*/

#define CURSORKEY_UP	0x1C
#define CURSORKEY_DOWN	0x1D
#define CURSORKEY_RIGHT	0x1E
#define CURSORKEY_LEFT	0x1F

/*
 *	dialog for wipe File
 */

#define LIST_WIDTH	(24*8)

#define OPEN_NUM	10

static GadgetTemplate fileGadgets[] = {
	{ GADG_PUSH_BUTTON,    55, -35, 0, 0, 70, 20, 0, 0, 'E', 0, "Encrypt" },
	{ GADG_PUSH_BUTTON,  -125, -35, 0, 0, 70, 20, 0, 0, 'D', 0, "Decrypt" },
#if (AMERICAN | BRITISH)
	{ GADG_PUSH_BUTTON,  -80,  50, 0, 0, 60, 20, 0, 0, 'E', 0, "Enter" },
	{ GADG_PUSH_BUTTON,  -80,  80, 0, 0, 60, 20, 0, 0, 'B', 0, "Back" },
	{ GADG_PUSH_BUTTON,  -80,  110, 0, 0, 60, 20, 0, 0, 'i', 0, "Disks" },
#elif GERMAN
	{ GADG_PUSH_BUTTON, -100,  50, 0, 0, 80, 20, 0, 0, 'U', 0, "Unterv." },
	{ GADG_PUSH_BUTTON, -100,  80, 0, 0, 80, 20, 0, 0, 'M', 0, "Mutterv." },
	{ GADG_PUSH_BUTTON, -100,  110, 0, 0, 80, 20, 0, 0, 'f', 0, "Laufw." },
#elif FRENCH
	{ GADG_PUSH_BUTTON,  -90,  50, 0, 0, 70, 20, 0, 0, 'E', 0, "Entre" },
	{ GADG_PUSH_BUTTON,  -90,  80, 0, 0, 70, 20, 0, 0, 'R', 0, "Retour" },
	{ GADG_PUSH_BUTTON,  -90,  110, 0, 0, 70, 20, 0, 0, 'V', 0, "Volumes" },
#elif SPANISH
	{ GADG_PUSH_BUTTON,  -90,  50, 0, 0, 70, 20, 0, 0, 'E', 0, "Entrar" },
	{ GADG_PUSH_BUTTON,  -90,  80, 0, 0, 70, 20, 0, 0, 'R', 0, "Regresar" },
	{ GADG_PUSH_BUTTON,  -90,  110, 0, 0, 70, 20, 0, 0, 'D', 0, "Discos" },
#elif SWEDISH
	{ GADG_PUSH_BUTTON,  -90,  50, 0, 0, 70, 20, 0, 0, 'V', 0, "Visa" },
	{ GADG_PUSH_BUTTON,  -90,  80, 0, 0, 70, 20, 0, 0, 'M', 0, "Moder" },
	{ GADG_PUSH_BUTTON,  -90,  110, 0, 0, 70, 20, 0, 0, 'l', 0, "Volymer" },
#endif

	SL_GADG_BOX(20, 50, LIST_WIDTH, OPEN_NUM),
	SL_GADG_UPARROW(20, 50, LIST_WIDTH, OPEN_NUM),
	SL_GADG_DOWNARROW(20, 50, LIST_WIDTH, OPEN_NUM),
	SL_GADG_SLIDER(20, 50, LIST_WIDTH, OPEN_NUM),

	{ GADG_STAT_TEXT,  65, 30, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_IMAGE, 20, 35, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  10, 10, 0, 0, 0, 0, 0, 0, 0, 0, strGetFile },

	{ GADG_ITEM_NONE }
};

/*
 *	Dialog template for file window
 */

static DialogTemplate mainDialog = {
	DLG_TYPE_WINDOW, DLG_FLAG_CLOSE | DLG_FLAG_DEPTH | DLG_FLAG_ZOOM,
	-1, -1, 310, 50 + OPEN_NUM*12 + 45, fileGadgets, "Encryptor"
};


/*
 *  Error dialog
 */

static GadgetTemplate errorGadgets[] = {
	{ GADG_PUSH_BUTTON, 220, 50, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT, 55, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 55, 30, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },

	{ GADG_ITEM_NONE }
};

static DialogTemplate errorDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 80, errorGadgets, NULL
};

/*
 *  About dialog
 */

static GadgetTemplate aboutGadgets[] = {
	{ GADG_PUSH_BUTTON, 100, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT, 78, 15, 0, 0, 0, 0, 0, 0, 0, 0, "Encryptor 1.0" },
	{ GADG_STAT_TEXT, 42, 40, 0, 0, 0, 0, 0, 0, 0, 0, "Designed and developed" },
	{ GADG_STAT_TEXT, 78, 55, 0, 0, 0, 0, 0, 0, 0, 0, "by Beth Henry" },
	{ GADG_STAT_TEXT, 66, 80, 0, 0, 0, 0, 0, 0, 0, 0, "Copyright \251 1993" },
	{ GADG_STAT_TEXT, 42, 95, 0, 0, 0, 0, 0, 0, 0, 0, "Central Coast Software" },
	{ GADG_STAT_TEXT, 78,110, 0, 0, 0, 0, 0, 0, 0, 0, "A division of" },
	{ GADG_STAT_TEXT, 22,125, 0, 0, 0, 0, 0, 0, 0, 0, "New Horizons Software, Inc."},
	{ GADG_STAT_TEXT, 54,140, 0, 0, 0, 0, 0, 0, 0, 0, "All Rights Reserved" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboutDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 195, aboutGadgets, NULL
};

/*
 *	Password
 */

static GadgetTemplate passwordReqGadgets[] = {
	{ GADG_PUSH_BUTTON,  55, -30, 0, 0,  70, 20, 0, 0, 'O', 0, "OK" },
	{ GADG_PUSH_BUTTON, 175, -30, 0, 0,  70, 20, 0, 0, 'C', 0, "Cancel" },

	{ GADG_EDIT_TEXT, 22, 45, 0, 0, 256, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static RequestTemplate passwordRequest = {
	-1, -1, 300, 100, passwordReqGadgets
};

/*
 *	Dialog list
 */

DlgTemplPtr dlgList[] = {
	&mainDialog,
	&errorDialog,
	&aboutDialog
};

/*
 *	RequestList
 */

ReqTemplPtr reqList[] = {
	&passwordRequest
};
