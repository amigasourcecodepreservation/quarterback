/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	All Protos
 */

#include <Toolbox/List.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Dialog.h>

/*
 *	Main.c prototypes
 */

void	DoIntuiMessage(IntuiMsgPtr);
void	CheckMainPort(void);
BOOL	DoMenu(WindowPtr, UWORD, UWORD, BOOL);

/*
 *	Init.c prototypes
 */

void	Init(int, char **);
void	SetUp(int, char **);
void	ShutDown(void);

/*
 *	Misc.c prototypes
 */

void	ErrBeep(void);
void	StdBeep(void);

void	SetBusyPointer(void);
void	NextBusyPointer(void);
void	SetWaitPointer(void);
void	SetArrowPointer(void);

void	NumTo3DigitHexString(ULONG, TextPtr);
void	LongToHexString(ULONG, TextPtr);
void	OutputHexOrDecString(ULONG, TextPtr, BOOL, BOOL, BOOL);
void	OutputEditHexOrDecString(ULONG, TextPtr, BOOL);
void	OutputStatHexOrDecString(ULONG, TextPtr, BOOL);

void	BeginWait(void);
void	EndWait(void);
void	FlushMainPort(void);

BOOL	CheckAbort(void);
BOOL	dopkt(struct MsgPort *, struct StandardPacket *);
WORD	StdDialog(WORD);
WORD	StdRequest(WORD);
void	DestroyRequest(RequestPtr);
BOOL	DoToolsPacket(MsgPortPtr, LONG, LONG[], WORD);
/*BOOL	NHIsFileSystem(TextPtr);*/
BOOL	BuildPath(TextPtr, TextPtr, Dir, WORD);
void	NumToCommaString(LONG, TextPtr);
BOOL	DialogFilter(IntuiMsgPtr, WORD *);
void	OutlineOKButton(DialogPtr);
void	PrintRightNum(RastPtr, RectPtr, ULONG);
void	PrintRightStr(RastPtr, RectPtr, TextPtr);
void	ConvertBCPLToCString(TextPtr, TextPtr);
void	ToggleCheckbox(BOOL *, WORD, DialogPtr);
BOOL	CheckNumber(TextPtr);
void	OutlineOKButton(DialogPtr);
void	GadgetOnOff(DialogPtr, WORD, BOOL);
void	GadgetOn(DialogPtr, WORD);
void	GadgetOff(DialogPtr, WORD);

BOOL	IsTopaz8ScreenFont(ScreenPtr);

void	NHInitList(struct List *);

BOOL	IsChecksumableBlock(ULONG *);
void	DoTextInWidth(TextPtr, WORD, BOOL);
BOOL	GetAltKeyStatus(WORD);

#ifdef TOOLBOX_REQUEST_H
RequestPtr DoGetRequest(WORD);
#endif

#ifdef DOS_DOSEXTENS_H
struct DevInfo *NHFindDosEntry(struct DevInfo *, TextPtr, ULONG);
LONG	DosFlagType(LONG);
struct DevInfo *NHLockDosList(LONG);
struct DevInfo *NHNextDosEntry(struct DevInfo *, LONG);
void	NHUnlockDosList(struct DevInfo *,LONG);
#endif

#ifdef TOOLBOX_BARGRAPH_H
void	DoDrawBarGraph(RequestPtr, BarGraphPtr);
void	DoSetBarGraph(RequestPtr, BarGraphPtr, ULONG);
void	DoSetBarGraphMax(RequestPtr, BarGraphPtr, ULONG);
#endif

/*
 *	Window.c prototypes
 */

WindowPtr OpenBackWindow(void);
WindowPtr OpenModeWindow(void);
WindowPtr OpenRepairWindow(void);
WindowPtr OpenRecoverWindow(void);
WindowPtr OpenReorgWindow(void);
WindowPtr OpenEditWindow(void);
void	RemoveWindow(WindowPtr);
BOOL	RemoveToolWindow(void);
BOOL	RemoveCurrentWindow(WindowPtr);
void	ConstructWindowTitle(TextPtr, WORD);
void	BuildSigBits(void);
void	DoNewSize(void);
void	DoRefresh(WindowPtr);

/*
 *	Project.c prototypes
 */

BOOL	DoProjectMenu(WindowPtr, UWORD, UWORD, UWORD);
BOOL	DoAboutBox(void);
BOOL	DoSaveAs(void);
BOOL	SaveFileAs(TextPtr);
BOOL	DoPageSetup(void);
BOOL	DoPrint(WindowPtr, UWORD, BOOL);
BOOL	DoPreferences(void);
LONG	SetPrefsOption(TextPtr);

BOOL	LoadFileOrDrawer(TextPtr, Dir);
BOOL	DoOpenFile(TextPtr, Dir);

BOOL	IsReportMode(void);

#ifdef TOOLBOX_STDFILE_H
BOOL	OpenSaveAsDialog(SFReplyPtr, TextPtr);
#endif

/*
 *	Device.c prototypes
 */

BOOL	PatchGiveUnit(void);
void	UnPatchGiveUnit(void);
BOOL	DoGetDevice(BOOL);
BOOL	GetDevice(BOOL);
void	PutDevice(void);
void	GetDiskType(void);
BOOL	UpdateVolumeOrDeviceList(ScrollListPtr, BOOL);
BOOL	StuffVolumeOrDeviceList(void);
BOOL	GetVolumeOrDeviceList(ScrollListPtr);
BYTE	ReadMulti(ULONG, ULONG, Ptr, BOOL);
BYTE	DoReadBlock(ULONG, Ptr, ULONG, BOOL);
BYTE	ReadBlock(ULONG, Ptr, BOOL);
BYTE	ReadBoot(Ptr);
BYTE	WriteBlock(ULONG, Ptr, BOOL);
BYTE	WriteMulti(ULONG, ULONG, Ptr, BOOL);
BYTE	DoWriteBlock(ULONG, Ptr, ULONG, BOOL);
BYTE	WriteBoot(Ptr);
BYTE	WaitBlock(void);
void	MotorOff(void);
void	CmdClear(void);
BOOL	IsWriteProtected(void);
BOOL	IsDiskPresent(void);
BOOL	IsDiskChanged(void);

/*
 *	Gadget.c prototypes
 */

void	MakeModeImages(void);
void	FreeModeImages(void);
BOOL	ProcessVolDevGadget(WORD, TextPtr);
BOOL	DoModeDialogMsg(IntuiMsgPtr);
void	DoRawKey(WindowPtr, UWORD, UWORD);

/*
 *	Tools.c prototypes
 */

BOOL	ChangeToolMode(WORD);
void	RefreshToolsWindow(BOOL);
void	ReturnToMain(void);
BOOL	DisplayVolInfoDialog(BOOL);
BOOL	CheckForDiskChange(void);
BOOL	DoToolsMenu(WindowPtr, UWORD, UWORD);

/*
 *	Menu.c prototypes
 */

void	SetAllMenus(void);
void	SetProjectMenu(void);
void	SetToolsMenu(void);
void	SetEditMenu(void);
void	OnOffMenu(WindowPtr, UWORD, BOOL);

/*
 *	HScrollList.c prototypes
 */

void	RecalcMaxWidth(void);
void	ScrollRight(WindowPtr, WORD);
void	ScrollLeft(WindowPtr, WORD);
void	HandleHorizScroll(MsgPortPtr, IntuiMsgPtr, GadgetPtr);
void	UpdateHScrollBar(GadgetPtr);
void	AdjustToHorizScroll(BOOL, BOOL);

/*
 * VScrollList.c prototypes
 */
 
WORD	CalcMaxHeight(void);
void	ScrollDown(WindowPtr, WORD);
void	ScrollUp(WindowPtr, WORD);
void	HandleVertScroll(MsgPortPtr, IntuiMsgPtr, GadgetPtr);
void	UpdateVScrollBar(GadgetPtr, BOOL);

/*
 *	Prefs.c prototypes
 */

void	SetDefaultPrefs(PrefsOptsPtr);
void	SetDefaultRepairOptions(RepairOptsPtr);
void	SetDefaultRecoverOptions(RecoverOptsPtr);
void	SetDefaultReorgOptions(ReorgOptsPtr);
void	SetDefaults(OptionsPtr);
void	SetPathName(TextPtr, TextPtr, BOOL);
BOOL	IsPrefsFile(TextPtr);
BOOL	GetProgPrefs(TextPtr);
BOOL	ReadProgPrefs(TextPtr);
BOOL	NewProgPrefs(TextPtr);
void	GetStdProgPrefs(void);
BOOL	SaveProgPrefs(WindowPtr, TextPtr);

/*
 * About.c prototypes
 */
 
RastPtr BeginAboutBox(WindowPtr, RectPtr, BOOL);
void	EndAboutBox(RastPtr);
void	DrawABoxFrame(RastPtr, WORD, WORD);
void	NextABoxFrame(RastPtr, RastPtr, RectPtr);

/*
 *	Error.c prototypes
 */

void	ParseDialogString(TextPtr, TextPtr);
void	FixTitle(void);
BOOL	PromptForDisk(void);
BOOL	PromptForWriteProtected(void);
void	OutOfMemory(void);
void	Error(WORD);
void	DOSError(WORD, LONG);
TextPtr	DOSErrorText(LONG);
WORD	WarningDialog(TextPtr, WORD);
void	ExplainDialog(WORD);
WORD	DoDialog(TextPtr, WORD, TextPtr, TextPtr, TextPtr, BOOL);
WORD 	DoNonParseDialog(TextPtr, WORD, TextPtr, TextPtr, TextPtr, TextPtr, BOOL);
void	DoHelp(void);

/*
 *	Macro.c prototypes
 */

void	InitRexx(void);
void	DoMacro(TextPtr, TextPtr);
void	DoRexxMsg(struct RexxMsg *);
void	ShutDownRexx(void);
void	DoAutoExec(void);
void	LoadFKeys(void);
BOOL	DoMacroMenu(WindowPtr, UWORD, UWORD);

/*
 *	PrHandler.c prototypes
 */

void	InitPrintHandler(void);
void	PrValidate(PrintRecPtr);
void	PrintDefault(PrintRecPtr);
BOOL	PageSetupRequest(void);
BOOL	PrintRequest(void);
LONG	SetPrintOption(TextPtr);

/*
 * Print.c prototypes
 */
 
BOOL	GetSysPrefs(void);
BOOL	PrintList(void);
void	GetPrinterType(void);
BOOL	WriteHeader(File, BOOL);
WORD	WriteEntry(File, WORD *, BOOL *);
WORD	WriteDirHeading(File, TextPtr, BOOL);
void	InitPrint(void);
void	EndPrint(void);

/*
 *	Icon.c prototypes
 */

void	SaveIcon(TextPtr, WORD);
void	InitIcons(void);
#ifdef TOOLBOX_IMAGE_H
void	GetIconImages(ImagePtr *, ImagePtr *, IconPtr, IconPtr);
void	SetIconImages(GadgetPtr);
#endif
void	ShutDownIcons(void);
void	CheckAppPort(void);
void	DoAppMsg(struct AppMessage *);

/*
 * Repair.c prototypes
 */

BOOL	CheckBadBlocks(void);
BOOL	CheckBadFiles(void);
void	ForceValidate(void);
BYTE	ReadRoot(BOOL, BufCtrlPtr *);
BOOL	CheckFileName(TextPtr);
BOOL	CheckBSTRFileName(UBYTE *);
UWORD CalcHash(UBYTE *);
void	FixChecksum(void *, BufCtrlPtr);
BOOL	CheckKey(ULONG);
BOOL	ValidateFileName(UBYTE *, ULONG);
BOOL	ProcessBitMapKeys(void);
void	RepairBitMap(void);
BYTE	ReadCtrlBlock(ULONG, BufCtrlPtr *, BOOL);
BOOL	VerifyCtrlBlock(UBYTE *, ULONG *);
BOOL	CheckCtrlTypes(Ptr, ULONG *);
void	DeleteFileEntry(ULONG, ULONG, BufCtrlPtr);
BOOL	CheckBootBlock(void);
BOOL	InitBoot(void);
void	MarkBadBlocksBusy(void);

/*
 *	RepairUI.c prototypes
 */

BOOL	DoRepair(void);
BOOL	DoRepairOptions(void);
void	DisplayHelpText(TextPtr, TextPtr);
void	EraseLabelRect(void);
void	UpdateBlockStats(void);
void	RefreshBlockStats(void);
void	SetHelpText(void);
void	DisplayPath(void);
void	DisplayFile(void);
void	BumpBarGraph(void);
void	AddBarGraph(LONG);
BOOL	SetupRepairMode(void);
void	CleanupRepairMode(void);
BOOL	DoRepairDialogMsg(IntuiMsgPtr);
void	RefreshRepairWindow(BOOL);
void	RefreshFileScan(void);
void	RefreshBlockScan(void);
BOOL	CheckBootBlockReq(void);
LONG	SetRepairOption(TextPtr);

/*
 * Recover.c prototypes
 */

BOOL	DoRecover(void);
BOOL	DoRecoverOptions(void);
void	DoTag(BOOL, BOOL);
BOOL	BuildDeletedTree(void);
BOOL	CreateDCBlock(ULONG, ULONG);
BOOL	InitRecoverLists(void);
void	RedoDummyTree(void);
void	*SearchTree(ULONG, void *);
void	UnlinkDummy(void *, void *);
void	*AllocateDirBlock(void);
void	FreeDirBlocks(void);

/*
 * RecoverUI.c prototypes
 */

BOOL	SetupRecoverMode(void);
void	CleanupRecoverMode(void);
BOOL	BuildFileListDisplay(DirFibPtr);
void	BuildListEntry(DirFibPtr, TextPtr);
BOOL	DoRecoverStart(void);
BOOL	AllocMiniIcons(void);
void	FreeMiniIcons(void);
void	RefreshRecoverStats(void);
void	DisplayRecoverPath(void);
BOOL	DoRecoverDialogMsg(IntuiMsgPtr);
void	RefreshRecoverWindow(BOOL);
void	SelectDir(DirFibPtr, BOOL);
DirFibPtr NextDirItem(DirFibPtr);
void	NewRecoverDestination(void);
void	NewRecoverOptions(RecoverOptsPtr, RecoverOptsPtr);
LONG	SetRecoverOption(TextPtr);

/*
 * Reorg.c prototypes
 */
 
BOOL	DoReorganize(void);
BOOL	CountFragmentedFiles(void);
BOOL	CheckRemapSpace(void);
BOOL	InitDirLists(void);
void	LinkChild(ReorgListPtr, ReorgListPtr);
BOOL	RelocateBlock(ULONG, ULONG);
BOOL	RemapKey(ULONG *);

/*
 * ReorgUI.c prototypes
 */
 
BOOL	DoOptimize(void);
BOOL	DoReorgOptions(void);
void	GetBitMapTablePtr(void);
void	DrawKeyPixel(ULONG, ULONG);
BOOL	SetupReorgMode(void);
void	CleanupReorgMode(void);
BOOL	DoReorgDialogMsg(IntuiMsgPtr);
void	RefreshReorgWindow(BOOL);
void	RefreshTimeRemaining(void);
void	RefreshFragmentationMap(void);
void	RefreshFragmentationLabel(void);
void	DisplayReorgPath(void);
LONG	SetOptimizeOption(TextPtr);

/*
 * Edit.c prototypes
 */
 
BOOL	SetupEditMode(void);
BOOL	CleanupEditMode(void);
UWORD	PromptForChangedBlock(BOOL);
void	RefreshEditWindow(BOOL);
void	ResetCursor(void);
void	CursorOff(void);
void	CursorOn(void);
void	RefreshCursor(void);
void	GetEditBlock(WORD);
BOOL	PutEditBlock(void);
void	RefreshEditLabel(void);
void	RefreshEditBlock(BOOL);
void	WriteEditHeader(TextPtr);
void	WriteEditLine(WORD, TextPtr);
BOOL	DoEditDialogMsg(IntuiMsgPtr);
BOOL	DoEditMenu(WindowPtr, UWORD, UWORD);

/*
 * Files.c prototypes
 */
 
void	InitPath(void);
BOOL	AppendPath(TextPtr);
BOOL	AppendDirPath(TextPtr, TextPtr);
void	TruncatePath(void);
void	DoTruncatePath(TextPtr);
BOOL	LinkParentDir(ULONG, ULONG);
BOOL	UnlinkParentDir(ULONG, ULONG);
BOOL	BuildFileHdr(TextPtr, BufCtrlPtr *);
BOOL	BuildExtHdr(ULONG, ULONG *, BufCtrlPtr *, ULONG);
BOOL	MarkFileBlocks(ULONG, BOOL);
BOOL	CheckFileBlocks(ULONG, ULONG *, ULONG *);
BOOL	ReadExtHdr(ULONG, ULONG, BufCtrlPtr *);
BOOL	LoadBadBlocksFile(void);
BOOL	OpenBadBlocksFile(ULONG *, BufCtrlPtr *);
BOOL	DeleteBadBlocksFile(void);
BOOL	LoadBadBlockKeys(void);
BOOL	BuildBadBlockBitMap(void);
BOOL	AddBadKey(ULONG);
BOOL	MarkBadBlocks(void);
BOOL	RawOpenFile(UBYTE *, ULONG, ULONG *, BufCtrlPtr *);
BOOL	RawDeleteFile(ULONG, ULONG);
BOOL	RawCreateFile(TextPtr, ULONG, ULONG *, BufCtrlPtr *);
BOOL	AllocFHKey(void);
/*void	GetLinkName(TextPtr, TextPtr, BOOL);*/

/*
 * Report.c prototypes
 */

void	ReportInfo(WORD);
BOOL	ReportError(WORD, WORD);
void	CondensePath(TextPtr, TextPtr, WORD);
void	AddToScrollList(TextPtr);
void	AddBlankLineToScrollList(void);
void	BeginEventString(TextPtr);
void	EndEventString(TextPtr, BOOL); 
void	EventString(struct DateStamp *, TextPtr, TextPtr, BOOL);
void	OutputRepairStats(void);
BOOL	OutputReport(TextPtr);
BOOL	WriteStr(TextPtr, File, BOOL);

/*
 * Bitmap.c
 */

BOOL	AllocFreeKey(void);
BOOL	StoreBitMapKeys(ULONG **, ULONG *, ULONG);
void	CountFreeKeys(void);
BOOL	AllocateBitMapBuffers(void);
void	FreeAltBitMap(void);
void	FreeBitMapBuffers(void);
BOOL	AllocateBadBitMap(void);
BOOL	AllocateAltBitMap(void);
BOOL	AllocateEmptyBitMap(void);
BOOL	ReadBitMap(void);
BOOL	WriteBitMap(void);
BOOL	LoadBitMapKeys(void);
BOOL	LoadBitMapData(void);
void	InitBitMapBuffer(ULONG *);
BOOL	MarkBitMapOccupied(ULONG *, ULONG, BOOL);
BOOL	MarkAltBusy(ULONG);
BOOL	MarkBadBusy(ULONG);
BOOL	MarkKeyBusy(ULONG);
BOOL	MarkAltFree(ULONG);
BOOL	MarkBadFree(ULONG);
BOOL	MarkKeyFree(ULONG);
BOOL	GetAltStatus(ULONG);
BOOL	GetBadStatus(ULONG);
BOOL	GetKeyStatus(ULONG);
BOOL	IsBitMapKey(ULONG);
BOOL	IsBitMapExtKey(ULONG);
BOOL	AnyBadInRange(ULONG, ULONG);
BOOL	RepositionBitMap(ULONG *);
BOOL	UpdateBitMapKeys(void);

/*
 * Buffer.c
 */
 
BYTE	GetCacheBuffer(BufCtrlPtr *);
void	PutCacheBuffer(BufCtrlPtr);
BYTE	ReadSpecial(ULONG, BufCtrlPtr *);
BYTE	ReadControl(ULONG, BufCtrlPtr *);
BYTE	ReadData(ULONG, BufCtrlPtr *);
BYTE	ReadCache(ULONG, WORD, BufCtrlPtr *);
BYTE	WriteCache(void);
BYTE	WriteBlockChecksum(BufCtrlPtr);
BYTE	WriteBuffer(BufCtrlPtr);
BufCtrlPtr ScanCache(ULONG);
ULONG	CalcBlockChecksum(ULONG *);
void	FlushCache(void);
void	LinkCacheKey(ULONG, WORD, BufCtrlPtr);
void	LinkCache(BufCtrlPtr);
void	UnlinkCache(BufCtrlPtr);
void	UnlinkCacheHash(BufCtrlPtr);
BOOL	AllocateCache(BOOL);
void	FreeCache(void);
UBYTE *AllocTrack(LONG *);
UBYTE *AllocBuffer(void);
UBYTE *AllocIOBuffer(ULONG);
void	FreeBuffer(UBYTE **);
void	FreeIOBuffer(UBYTE **, ULONG);
BOOL	AllocateBlock(void);
void	FreeBlock(void);
BOOL	AllocateExtBuffer(void);
void	FreeExtBuffer(void);
BOOL	AllocateBadBlockBuffer(void);
void	FreeBadBlockBuffer(void);
BOOL	AllocRemapBuffer(ULONG);
void	FreeRemapBuffer(void);
void	ForceRead(void);

/*
 * Command.c
 */
 
LONG	ProcessCommandText(TextPtr, UWORD, BOOL, LONG *);
void	GetNextWord(TextPtr, TextPtr, UWORD *);
/*BOOL	LoadCommandFile(TextPtr); */