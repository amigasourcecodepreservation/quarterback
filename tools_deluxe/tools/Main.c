/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Main Program
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <workbench/workbench.h>
#include <dos/dos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>

#include "Tools.h"
#include "Proto.h"

#include <Toolbox/Dialog.h>

/*
 *	External variables
 */

extern BOOL			quitFlag, closeFlag, abortFlag, cancelFlag;
extern MsgPortPtr	mainMsgPort;
extern ULONG		sigBits;
extern WORD			mode, nextMode, action;
extern BOOL			inMacro, titleChanged;
extern MsgPort		rexxMsgPort;
extern struct RexxLib *RexxSysBase;

/*
 *	Main program
 */

void main(int argc, char *argv[])
{
	register IntuiMsgPtr intuiMsg;
	struct RexxMsg *rexxMsg;
/*
	Open operation window, optional screen and all the libraries needed.
*/
	Init(argc, argv);
	SetUp(argc, argv);
/*
	Do auto exec macro
*/
	DoAutoExec();

	quitFlag = FALSE;
	do {
		while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) {
			DoIntuiMessage(intuiMsg);
		}
/*		
	Process REXX messages
	Only do one Rexx message per pass through main loop, so that all windows
		are opened/closed and program is in correct state
*/
		if (RexxSysBase &&
			(rexxMsg = (struct RexxMsg *) GetMsg(&rexxMsgPort)) != NULL)
			DoRexxMsg(rexxMsg);
/*
	Handle AppIcon messages
*/	
		if( !inMacro ) {
			CheckAppPort();
		}
/*
	Wait for next message unless quitting
*/
		if( !quitFlag ) {
			Wait(sigBits);
		}
	} while( !quitFlag || inMacro );
/*
	Quitting
*/
	ShutDown();

}

/*
 *	Process intuition messages
 */

void DoIntuiMessage(IntuiMsgPtr intuiMsg)
{
	register BOOL processedMsg = FALSE;
	register ULONG class;
	register UWORD code;
	UWORD modifier;
	WindowPtr window;
	
	class = intuiMsg->Class;
	code = intuiMsg->Code;
	modifier = intuiMsg->Qualifier;
	window = intuiMsg->IDCMPWindow;
	
	if (class == IDCMP_RAWKEY) {
		ConvertKeyMsg(intuiMsg);		/* Convert some RAWKEY to VANILLAKEY */
	}
	if (window != NULL) {
		if (IsDialogMsg(intuiMsg)) {
			if( (class == IDCMP_GADGETDOWN) && titleChanged ) {
				FixTitle();
			}
			switch(mode) {
				case MODE_TOOLS:
					processedMsg = DoModeDialogMsg(intuiMsg);
					break;
				case MODE_REPAIR:
					processedMsg = DoRepairDialogMsg(intuiMsg);
					break;
				case MODE_RECOVER:
					processedMsg = DoRecoverDialogMsg(intuiMsg);
					break;
				case MODE_OPTIMIZE:
					processedMsg = DoReorgDialogMsg(intuiMsg);
					break;
				case MODE_EDIT:
					processedMsg = DoEditDialogMsg(intuiMsg);
				default:
					break;
			}
		} else {
			ReplyMsg((MsgPtr) intuiMsg);
		}
	} else {
		ReplyMsg((MsgPtr) intuiMsg);
	}
	if( (!processedMsg) && (class != RAWKEY || ((code & IECODE_UP_PREFIX) == 0)) ) {

/*
	Process the only global messages
*/
		switch (class) {
		case REFRESHWINDOW:
			DoRefresh(window);
			break;
		case NEWSIZE:
			DoNewSize();
			break;
		case MENUPICK:
			DoMenu(window, code, modifier, TRUE);
			break;
		}
	}
}

/*
 *	Handle menu events
 */

BOOL DoMenu(register WindowPtr window, UWORD menuNumber, UWORD qualifier, BOOL multiSel )
{
	register UWORD menu, item, sub;
	register BOOL success = FALSE;
	BOOL changeMode = FALSE;
	
	if( titleChanged ) {
		FixTitle();
	}
	while( menuNumber != MENUNULL ) {
		menu = MENUNUM(menuNumber);
		item = ITEMNUM(menuNumber);
		sub = SUBNUM(menuNumber);
		switch( menu ) {
		case PROJECT_MENU:
			success = DoProjectMenu(window, item, sub, qualifier);
			break;
		case TOOLS_MENU:
			success = DoToolsMenu(window, item, sub);
			changeMode |= closeFlag;
			break;
		case MACRO_MENU:
			success = DoMacroMenu(window, item, sub);
			break;
		case EDIT_MENU:
			success = DoEditMenu(window, item, sub);
			break;
		}
		menuNumber = multiSel ? ItemAddress(window->MenuStrip, menuNumber)->NextSelect : MENUNULL;
	}
	
/*
	Changes nextMode based on menu item selected, if "changeMode" set.
*/
	if( changeMode && (mode != nextMode) ) {
		ChangeToolMode(nextMode);
	}
	return(success);
}

/*
 *	Process any messages at the mainMsgPort during a repair operation.
 */

void CheckMainPort()
{
	register IntuiMsgPtr intuiMsg;

/*
	Process intuition message
*/
	do {
		while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) {
			DoIntuiMessage(intuiMsg);
		}
		CheckAppPort();
		
	} while( (action == OPER_PAUSE) && (!abortFlag) && (!cancelFlag) );
}
