/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Quarterback Tools preferences routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <workbench/workbench.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/dos.h>
#include <proto/icon.h>

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>
#include <Toolbox/Screen.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Font.h>
#include <Toolbox/LocalData.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	External variables
 */

extern BOOL			_tbOnPubScreen;

extern ScreenPtr	screen;

extern TextChar	progPathName[], strBuff[], strPrefsName[];

extern OptionsRec	options;

extern GadgetPtr	currGadgList;

extern BOOL			fromCLI, decimalLabel, bitMapFlag;
extern WindowPtr	cmdWindow;
extern WORD			mode;


typedef struct {
	ULONG				FileID;		/* Prefs file ID */
	UWORD				Version;		/* Version number */
	OptionsRec		Options;		/* Program options */
/*	WORD				NumColors;
	RGBColor			ColorTable[256];
*/
	WORD				pad[373-302];/* Let's keep it to 488 or 976 bytes if we can! */
} ProgPrefs, *ProgPrefsPtr;

static ProgPrefs	progPrefs;

/*
 * Local prototypes and definitions
 */
 
static void	ValidatePrefs(OptionsPtr);

#define PREFS_VERSION	 0			/* First version */

/*
 *	SetDefaultPrefs
 *	Set only options found in Preferences
 */

void SetDefaultPrefs(PrefsOptsPtr prefsOptions)
{
	prefsOptions->SaveIcons = TRUE;
	prefsOptions->Flash = TRUE;
	prefsOptions->Beep = TRUE;
	prefsOptions->DecimalLabel = TRUE;
	prefsOptions->MemoryUsage = 10;
	prefsOptions->BypassDOS = FALSE;
}

/*
 *	Set only options found in Repair Options
 */

void SetDefaultRepairOptions(RepairOptsPtr repairOptions)
{
	repairOptions->DoBadBlocks = TRUE;
	repairOptions->DoBadFiles = TRUE;
	repairOptions->Repair = TRUE;
	repairOptions->BadBlockMode = FALSE;
	repairOptions->CheckFFSData = FALSE;
}

/*
 * Set only options found in Recover Options
 */
 
void SetDefaultRecoverOptions(RecoverOptsPtr recoverOptions)
{
	recoverOptions->ShowDelOnly = TRUE;
	recoverOptions->ShowKeys = FALSE;
	recoverOptions->CreateDrawers = FALSE;
	recoverOptions->Destination = FALSE;
	strcpy(recoverOptions->DestName, "DF0:");
}

/*
 * Set only options found in Reorganize Options
 */
 
void SetDefaultReorgOptions(ReorgOptsPtr reorgOptions)
{
	reorgOptions->Optimize = TRUE;
	reorgOptions->OptimizeType = FALSE;
	reorgOptions->Buffer = FALSE;
}

/*
 *	Setup defaults for all program preferences
 */

void SetDefaults(OptionsPtr options)
{
	BlockClear(options, sizeof(OptionsRec));
	
	PrintDefault(&options->PrintInfo.PrintRec);
	
	options->PrintInfo.MeasureUnit	= MEASURE_DEFAULT;
	
	SetDefaultPrefs(&options->PrefsOpts);
	SetDefaultRepairOptions(&options->RepairOpts);
	SetDefaultRecoverOptions(&options->RecoverOpts);
	SetDefaultReorgOptions(&options->ReorgOpts);
	options->EditOpts.AutoChecksum = TRUE;
}

/*
 *	Create path name to given file at same directory as program
 *	If abbrev is TRUE, then it's OK to use short-cut in AmigaDOS 2.0
 */

void SetPathName(TextPtr pathNameBuff, TextPtr fileName, BOOL abbrev)
{
	register WORD i;

	if (SystemVersion() >= OSVERSION_2_0_4 && abbrev) {
		strcpy(pathNameBuff, "ProgDir:");
	} else {
		strcpy(pathNameBuff, progPathName);
		for (i = strlen(pathNameBuff); i; i--) {
			if (pathNameBuff[i - 1] == '/' || pathNameBuff[i - 1] == ':')
				break;
		}
		pathNameBuff[i] = '\0';
	}
	strcat(pathNameBuff, fileName);
}


/*
 *	Return TRUE if file is a settings file
 */

BOOL IsPrefsFile(TextPtr fileName)
{
	ULONG fileID;
	UWORD version;
	BOOL success;
	File file;

	if ((file = Open(fileName, MODE_OLDFILE)) == NULL)
		return (FALSE);
	success = FALSE;
	if (Read(file, &fileID, sizeof(ULONG)) == sizeof(ULONG) &&
		fileID == ID_PREFSFILE &&
		Read(file, &version, sizeof(UWORD)) == sizeof(UWORD) &&
		version == PREFS_VERSION)
		success = TRUE;
	Close(file);
	return (success);
}

/*
 * Load QB Tools preference settings
 */
 
BOOL GetProgPrefs(TextPtr fileName)
{
	BOOL success;
	
	SetPathName(strBuff, fileName, TRUE);
	success = NewProgPrefs(strBuff);
	return(success);
}

/*
 * Make sure there are no outrageous fields.
 */
 
static void ValidatePrefs(register OptionsPtr opts)
{
	opts->PrefsOpts.MemoryUsage = 10;
	/*
	if( (opts->PrefsOpts.MemoryUsage <= 0) || (opts->PrefsOpts.MemoryUsage > 100) ) {
		opts->PrefsOpts.MemoryUsage = 50;
	}
	*/
	opts->PrefsOpts.DecimalLabel = MIN(1, opts->PrefsOpts.DecimalLabel);
	opts->ReorgOpts.OptimizeType = MIN(1, opts->ReorgOpts.OptimizeType);

	PrValidate(&opts->PrintInfo.PrintRec);
}

/*
 * Grab a new preferences file and refresh current window parameters accordingly.
 */
 
BOOL NewProgPrefs(TextPtr fileName)
{
	BOOL success;
	WORD oldDevSetting, newDevSetting;
	RecoverOptsRec oldRecoverOpts;
	
	oldDevSetting = options.Devices;
	oldRecoverOpts = options.RecoverOpts;
	success = ReadProgPrefs(fileName);
	
	switch( mode ) {
	case MODE_TOOLS:
		newDevSetting = options.Devices;
		options.Devices = oldDevSetting;
		ProcessVolDevGadget(newDevSetting, NULL);
		break;
	case MODE_REPAIR:
		SetHelpText();
		break;
	case MODE_RECOVER:
		options.RecoverOpts.Destination |= (!bitMapFlag);
		NewRecoverDestination();
		NewRecoverOptions(&oldRecoverOpts, &options.RecoverOpts);
		break;
	case MODE_EDIT:
		/* RefreshEditLabel();*/			/* HEX/DEC may change, but editor has a local */
		break;
	}
	return(success);
}

/*
 *	Read and set defaults from specified file
 *	Return success status
 */

BOOL ReadProgPrefs(TextPtr fileName)
{
	register BOOL success;
	register File file;
	UBYTE prefsHeader[6];
	
	if( success = (file = Open(fileName, MODE_OLDFILE)) != NULL ) {
		if( success = (Read(file, &prefsHeader[0], 6) == 6) &&
			(*((ULONG *)&prefsHeader[0]) == ID_PREFSFILE) &&
			(*((UWORD *)&prefsHeader[4]) == PREFS_VERSION) ) {
			BlockMove(prefsHeader, &progPrefs, 6);
			success = Read(file, &progPrefs.Options, sizeof(OptionsRec)) == sizeof(OptionsRec);
		}
		if( success ) {
			ValidatePrefs(&progPrefs.Options);
			options = progPrefs.Options;
		}
		Close(file);
	}
	return (success);
}

/*
 *	Load preference settings
 *	If no prefs file found, set to standard defaults
 */

void GetStdProgPrefs()
{
	TextChar pathNameBuff[150];

	SetDefaults(&options);
	progPrefs.Options = options;
	SetPathName(pathNameBuff, strPrefsName, TRUE);
	(void) ReadProgPrefs(pathNameBuff);
	decimalLabel = options.PrefsOpts.DecimalLabel;
}

/*
 *	Save preferences settings from given document
 *	Return success status
 */

BOOL SaveProgPrefs(WindowPtr window, TextPtr fileName)
{
/*	register WORD i, num, numColors;*/
	BOOL success = FALSE;
	File file;
	UBYTE pathNameBuff[150];
/*	RGBColor colors[256];*/
#ifdef SAVE_RECT_POS
	register RectPtr rectPtr;

/*
	Save window position now.
*/
	switch(mode) {
	case MODE_TOOLS:
		rectPtr = &options.WindowRect;
		break;
	case MODE_REPAIR:
		rectPtr = &options.RepairOpts.WindowRect;
		break;
	case MODE_RECOVER:
		rectPtr = &options.UndelOpts.WindowRect;
		break;
	case MODE_OPTIMIZE:
		rectPtr = &options.ReorgOpts.WindowRect;
		break;
	case MODE_EDIT:
		rectPtr = &options.EditOpts.WindowRect;
		break;
	}
	SetRect(rectPtr, 0, 0, cmdWindow->Width-1, cmdWindow->Height-1);
	OffsetRect(rectPtr, cmdWindow->LeftEdge, cmdWindow->TopEdge);
#endif
	if( mode == MODE_RECOVER ) {
		GetEditItemText(currGadgList, UNDEL_DEST_TEXT, options.RecoverOpts.DestName);
	}
/*
	Set prefs data
*/
	BlockClear(&progPrefs, sizeof(ProgPrefs));
	progPrefs.FileID		= ID_PREFSFILE;
	progPrefs.Version		= PREFS_VERSION;
	progPrefs.Options		= options;

#ifdef SAVE_COLORS_IN_PREFS
/*
	Save color palette
	Save as a minimum the standard color palette
*/
	if (_tbOnPubScreen) {
		BuildDefaultColorTable(&progPrefs.ColorTable);
		progPrefs.NumColors = NUM_STDCOLORS;
	}
	else {
		numColors = GetColorTable(screen, &colors);
		if (numColors < NUM_STDCOLORS) {
			BuildDefaultColorTable(&progPrefs.ColorTable);
			progPrefs.NumColors = NUM_STDCOLORS;
		}
		else
			progPrefs.NumColors = numColors;
		num = MIN(numColors, progPrefs.NumColors);
		for (i = 0; i < num/2; i++) {
			progPrefs.ColorTable[i] = colors[i];
			progPrefs.ColorTable[progPrefs.NumColors - i - 1] = colors[numColors - i - 1];
		}
	}
#endif

/*
	Open file
*/
	if (fileName) {
		strcpy(pathNameBuff, fileName);
	} else {
	 	SetPathName(pathNameBuff, strPrefsName, TRUE);
	}
	if( (file = Open(pathNameBuff, MODE_NEWFILE)) != NULL) {
		success = Write(file, (BYTE *) &progPrefs, sizeof(ProgPrefs)) == sizeof(ProgPrefs);
		Close(file);
		if (success) {
			SaveIcon(pathNameBuff, ICON_PREFS);
		}
	}
	return (success);
}
