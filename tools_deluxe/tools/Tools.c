/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Quarterback Tools
 * Copyright (c) 1992 New Horizons Software, Inc.
 *
 * Tools menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>
#include <exec/execbase.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Image.h>
#include <Toolbox/ScrollList.h>

#include "Tools.h"
#include "Proto.h"

/*
 * External variables
 */

extern UBYTE			_tbPenLight, _tbPenBlack;
extern BOOL				_tbSmartWindows;

extern TextChar		repairTitle[], undeleteTitle[], optimizeTitle[], editorTitle[];
extern TextChar		strUnknown[], strOFS[], strFFS[], strVersSeparator[], strOrSymbol[];
extern TextChar		strPublic[], strChip[], strFast[], strLocal[], str24BitDMA[], strAny[];
extern TextChar		strTextSeparator[], strInternational[], strDirCache[];
extern TextChar		strNoVol[], strInvalidVol[], strValidVol[], strWPVol[];
extern TextChar		strBuff[], choiceName[], deviceName[], volumeName[];
extern TextChar		strQuestionMark[];
extern WindowPtr		cmdWindow, backWindow;
extern ScreenPtr		screen;
extern DlgTemplPtr		dlgList[];
extern MsgPortPtr		mainMsgPort;
extern BOOL				abortFlag, cancelFlag, skipFlag, closeFlag, quitFlag, decimalLabel;
extern WORD				mode, nextMode, action, intuiVersion;

extern VolParams		currVolParams;
extern TextPtr			iconTitle[];
extern ScrollListPtr		scrollList;
extern ListHeadPtr		deviceList;
extern UWORD			charBaseline, charHeight;
extern GadgetPtr		currGadgList;
extern OptionsRec		options;
extern GadgetTemplate 	volInfoGadgets[], volInfoGadgets2[];
extern BadBlockStats		badBlockStats;
extern BOOL				deviceGotten;

/*
 *	Local prototypes and definitions
 */

static WindowPtr OpenToolWindow(WORD);
static ULONG ToK(ULONG);
static void UpdateVolStatGadgets(DialogPtr, BOOL, BOOL);
static void AddStrToMemStr(TextPtr, TextPtr);
static void StuffNumStringIfDOS(DialogPtr, ULONG, WORD);
static void StuffNumString(DialogPtr, ULONG, WORD);
static void StuffString(DialogPtr, TextPtr, WORD);
static void DoStuffString(DialogPtr, TextPtr, GadgetPtr, BOOL);
static void AppendLongToOctalStr(ULONG, TextPtr);
static TextPtr ConvertCharToOctalStr(UBYTE, TextPtr);

#define LEFT_ARROW_BUTTON	2
#define RIGHT_ARROW_BUTTON	3
#define PAGE_FLIP_BUTTON	RIGHT_ARROW_BUTTON

enum {
	FILESYSTEM_TEXT = PAGE_FLIP_BUTTON+1,
	VOL_STATUS_TEXT,
	VOLUME_TEXT,
	PHYS_SIZE_K_TEXT,
	PHYS_SIZE_BLOCKS_TEXT,
	LOG_SIZE_K_TEXT,
	LOG_SIZE_BLOCKS_TEXT,
	FREE_K_TEXT,
	FREE_BLOCKS_TEXT,
	BAD_K_TEXT,
	BAD_BLOCKS_TEXT,
	DLG_LABEL = BAD_BLOCKS_TEXT+16
};

enum {
	DEVICE_TEXT = PAGE_FLIP_BUTTON+1,
	VERSION_TEXT,
	DRIVER_TEXT,
	BLOCKSIZE_TEXT,
	BLOCKSPERTRACK_TEXT,
	CYLINDERS_TEXT,
	LOWCYL_TEXT,
	HIGHCYL_TEXT,
	SURFACES_TEXT,
	MAXTRANSFER_TEXT,
	RESERVED_TEXT,
	PREALLOC_TEXT,
	MEMTYPE_TEXT,
};

/*
 *	Change to the mode chosen in the Tools menu and stored in 'nextMode'.
 * Returns TRUE if mode is changed (cache successfully allocated).
 */

BOOL ChangeToolMode(WORD nextMode)
{
	register BOOL success;
	register BOOL toMain;
	
	abortFlag = skipFlag = cancelFlag = closeFlag = FALSE;
	SetStdPointer(backWindow, POINTER_WAIT);
	
	if( nextMode == MODE_TOOLS ) {
		FreeCache();
		toMain = RemoveCurrentWindow(cmdWindow);
	} else {
		SetWaitPointer();
		toMain = !(success = (mode != MODE_TOOLS) || DoGetDevice(TRUE));
		if( success ) {
/*
	In edit mode, we still need to read the root block, so don't turn the
	motor off yet.
*/
			if( nextMode != MODE_EDIT ) {
				MotorOff();
			}
			if( success = RemoveToolWindow() ) {
				SetWaitPointer();					/* Confirmation may have set it back. */
				mode = MODE_TOOLS;
				toMain = !(success = OpenToolWindow(nextMode) != NULL);
			}
		}
		if( toMain ) {
			FreeCache();
			PutDevice();
			SetArrowPointer();
			SetToolsMenu();
		} else {
			FlushCache();
		}
	}
	if( toMain ) {
		if( cmdWindow == NULL ) {
			if( !(success = (cmdWindow = OpenModeWindow()) != NULL) ) {
				quitFlag = TRUE;
			}
		}
	}
	SetStdPointer(backWindow, POINTER_ARROW);
	return(success);
}

/*
 * Open a tool window
 */
 
static WindowPtr OpenToolWindow(WORD newMode)
{
	register WindowPtr window = NULL;
	
	switch(newMode) {
	case MODE_REPAIR:
		window = OpenRepairWindow();
		break;
	case MODE_RECOVER:
		window = OpenRecoverWindow();
		break;
	case MODE_OPTIMIZE:
		window = OpenReorgWindow();
		break;
	case MODE_EDIT:
		window = OpenEditWindow();
	default:
		break;
	}
	SetStdPointer(backWindow, POINTER_ARROW);
	return(window);
}

/*
 * Process project menu selection
 */

BOOL DoToolsMenu(WindowPtr window, register UWORD item, UWORD sub)
{
	BOOL success = TRUE;
	register WORD newMode;
	
	if( (item == VOL_STAT_ITEM) || (item == VOL_STAT_ITEM2) ) {
		success = DisplayVolInfoDialog( (item == VOL_STAT_ITEM) );
	} else {
		if( action != OPER_IN_PROG ) {
			closeFlag = TRUE;
			switch (item) {
			case TOOLS_SELECT_ITEM:
			default:
				newMode = MODE_TOOLS;
				break;
				case VOL_REPAIR_ITEM:
				newMode = MODE_REPAIR;
				break;
			case VOL_RECOVER_ITEM:
				newMode = MODE_RECOVER;
				break;
			case VOL_OPTIMIZE_ITEM:
				newMode = MODE_OPTIMIZE;
				break;
			case VOL_EDITOR_ITEM:
				newMode = MODE_EDIT;
				break;
			}
			nextMode = newMode;
		}
	}
	return(success);
}

/*
 * Refresh for Tools mode
 */
 
void RefreshToolsWindow(register BOOL adjust)
{
	register WindowPtr window = cmdWindow;
	GadgetPtr gadget = GadgetItem(window->FirstGadget, 0);
	register WORD x, y;
	register WORD i;
	Rectangle winRect;
	register RastPtr rPort = window->RPort;
	BOOL inval = FALSE;
		
	SetDrMd(rPort, JAM1);
/*
	Clear the background of the window if first time, or pre DOS2.0 and simple refresh.
*/
	GetWindowRect(window, &winRect);
	if( adjust || (!_tbSmartWindows) ) {
		if( intuiVersion < OSVERSION_2_0 ) {
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &winRect);
			inval = TRUE;
		}
	}
	if( adjust ) {
/*
	If we haven't added the gadgets yet, we can do it now (since background done)
*/
		if( gadget == NULL ) {
			AddGList(window, currGadgList, -1, -1, 0);
		}
		inval = TRUE;
	}
	if( inval ) {
		RefreshGadgets(currGadgList, window, NULL);
		InvalRect(window, &winRect);
	}
	BeginRefresh(window);
/*
	If new window size or not a smart refresh window, we have to draw ourselves.
*/
	if( adjust || (!_tbSmartWindows) ) {

		SetAPen(rPort, _tbPenBlack);
		gadget = GadgetItem(currGadgList, BUTTON_REPAIR);
		x = gadget->LeftEdge + gadget->Width + horizSpacing;
		y = ((gadget->Height - charHeight) >> 1) + charBaseline;

		for( i = 0 ; i < numModes; i++, gadget = gadget->NextGadget ) {
#ifdef OLD_WAY
			if (adjust) {
				gadget->LeftEdge = x;
				gadget->TopEdge = y + (resize ? 0 : (charHeight - gadget->Height) >> 1);
				Move(rPort, (x+gadget->Width+horizSpacing), y + charBaseline + (resize ? ((gadget->Height-charHeight) >> 1) : 0));
				Text(rPort, iconTitle[i], strlen(iconTitle[i]));

				y += vertSpacing + (resize ? gadget->Height : charHeight);
			}
#else
			Move(rPort, x, y + gadget->TopEdge);
			TextInWidth(rPort, iconTitle[i], strlen(iconTitle[i]), (window->Width - window->BorderRight) - rPort->cp_x, FALSE);
#endif
		}
		if( scrollList != NULL ) {
			SLDrawBorder(scrollList);
			UpdateVolumeOrDeviceList(scrollList, TRUE);
		}
	}
	EndRefresh(window, TRUE);
}

/*
 * Call to unconditionally return to the main tool selector window.
 */
 
void ReturnToMain()
{
	closeFlag = TRUE;
	ChangeToolMode(MODE_TOOLS);
}

/*
 * Display the volume info of current volume.
 * Returns TRUE unless dialog was unable to be opened.
 */
 
BOOL DisplayVolInfoDialog(register BOOL firstPage)
{
	BOOL saveDecimalLabel;
	register BOOL success;
	register DialogPtr dlg;
	register WORD item;
	GadgetPtr statGadgList;
	GadgetTemplate *gadgetTemplPtr;
	Rectangle rect, arrowRect;
	register RastPtr rPort;
	BufCtrlPtr buf;
	ULONG count;
	BOOL diskPresent;
	WORD top;
		
	saveDecimalLabel = decimalLabel;
	decimalLabel = options.PrefsOpts.DecimalLabel;

	BeginWait();
/*
	If in a tool already (not in the main window), then no need to GetDevice() again.
*/
	if( (success = deviceGotten) || ((mode == MODE_TOOLS) ? DoGetDevice(FALSE) : GetDevice(FALSE)) ) {
/*
	If we did a GetDevice(), then release it immediately (we've copied our info).
	If we don't do a PutDevice(), we should at least turn the disk motor off, unless
	of course, there is no disk present (in which case it would already be off).
*/
		if( diskPresent = IsDiskPresent() ) {
			(void) OpenBadBlocksFile(&count, &buf);	/* Get bad block count */
			if( success ) {
				MotorOff();										/* Turn right back off */
			}
		}
		if( !success ) {
			PutDevice();
		}
		dlgList[DLG_VOLINFO]->Gadgets = firstPage ? &volInfoGadgets[0] : &volInfoGadgets2[0];
		if( success = (dlg = GetDialog(dlgList[DLG_VOLINFO], screen, mainMsgPort)) != NULL ) {
			statGadgList = GadgetItem(dlg->FirstGadget, 0);
			rPort = dlg->RPort;
			SetFont(rPort, screen->RastPort.Font);
			GetGadgetRect(GadgetItem(dlg->FirstGadget, LEFT_ARROW_BUTTON), dlg, NULL, &arrowRect);
			InsetRect(&arrowRect, -1, -1);
			GetGadgetRect(GadgetItem(dlg->FirstGadget, DLG_LABEL), dlg, NULL, &rect);
			top = rect.MinY;
			OutlineOKButton(dlg);
			do {
				UpdateVolStatGadgets(dlg, firstPage, diskPresent);
				item = ModalDialog(mainMsgPort, dlg, DialogFilter);
				if( (item == LEFT_ARROW_BUTTON) || (item == RIGHT_ARROW_BUTTON) ) {
					if( firstPage ) {
						gadgetTemplPtr = &volInfoGadgets2[0];
					} else {
						gadgetTemplPtr = &volInfoGadgets[0];
					}
					RemoveGList(dlg, dlg->FirstGadget, -1);
					DisposeGadgets(statGadgList);
					statGadgList = GetGadgets(gadgetTemplPtr);
					if( statGadgList == NULL ) {
						item = OK_BUTTON;		/* Exit routine if cannot swap gadgets */
					} else {
						SetAPen(rPort, _tbPenLight);
						GetWindowRect(dlg, &rect);
						InsetRect(&rect, 2, 2);
						rect.MinY = arrowRect.MaxY;
						FillRect(rPort, &rect);
						rect.MaxY = rect.MinY;
						rect.MinY = arrowRect.MinY;
						rect.MaxX = arrowRect.MinX;
						FillRect(rPort, &rect);
						rect.MinY = top;
						rect.MaxY = rect.MinY + charHeight;
						rect.MaxX -= 12;
						FillRect(rPort, &rect);
						AddGList(dlg, statGadgList, 0, -1, NULL);
						RefreshGList(GadgetItem(dlg->FirstGadget, RIGHT_ARROW_BUTTON+1), dlg, NULL, -1);
					}
					firstPage = !firstPage;
				}
			} while( item != OK_BUTTON );
			DisposeDialog(dlg);
		} else {
			Error(ERR_NO_MEM);
		}
	}
	EndWait();
	
	decimalLabel = saveDecimalLabel;
	return(success);
}

/*
 * Return massaged number of kilobytes.
 */
 
static ULONG ToK(ULONG n)
{
	return( (n + 1023) >> 10 );
}
	
/*
 * Update the gadgets of a given page
 */
 
static void UpdateVolStatGadgets(register DialogPtr dlg, register BOOL firstPage, register BOOL diskPresent)
{
	NodePtr nodePtr;
	TextChar buff[10];
	struct ExecBase *execBase;
	register UWORD temp;
	register TextPtr statusStr;
	
	OnOffGList(GadgetItem(dlg->FirstGadget, LEFT_ARROW_BUTTON), dlg, NULL, 1, !firstPage);
	OnOffGList(GadgetItem(dlg->FirstGadget, RIGHT_ARROW_BUTTON), dlg, NULL, 1, firstPage);
	SetAPen(dlg->RPort, _tbPenBlack);
	if( firstPage ) {
		if( !diskPresent ) {
			strBuff[0] = '\0';
			statusStr = strNoVol;
		} else {
			if( currVolParams.DosDisk ) {
				if( !currVolParams.VolValid ) {
					statusStr = strInvalidVol;
				} else if( currVolParams.Info.id_DiskState == ID_WRITE_PROTECTED ) {
					statusStr = strWPVol;
				} else {
					statusStr = strValidVol;
				}
				strcpy(strBuff, currVolParams.FFSFlag ? strFFS : strOFS);
				if( currVolParams.International ) {
					strcat(strBuff, strTextSeparator);
					strcat(strBuff, strInternational);
				}
				if( currVolParams.DirCache ) {
					strcat(strBuff, strTextSeparator);
					strcat(strBuff, strDirCache);
				}
			} else {
				strcpy(strBuff, strUnknown);
				statusStr = strInvalidVol;
			}
		}
		if( strBuff[0] ) {
			AppendLongToOctalStr(/*currVolParams.VolValid ? 
				currVolParams.Info.id_DiskType : */currVolParams.DiskType, &strBuff[strlen(strBuff)] );
		}
		if( diskPresent ) {
			StuffString(dlg, strBuff, FILESYSTEM_TEXT);
		}
		StuffString(dlg, statusStr, VOL_STATUS_TEXT);
		if( diskPresent ) {
			StuffString(dlg, diskPresent ? volumeName : NULL, VOLUME_TEXT);
		
			if( currVolParams.VolValid && currVolParams.BlockSize ) {
				StuffNumString(dlg, ToK(currVolParams.MaxBytes), PHYS_SIZE_K_TEXT);
				StuffNumString(dlg, currVolParams.MaxBlocks, PHYS_SIZE_BLOCKS_TEXT);
				temp = currVolParams.Envec.de_Reserved /*+ currVolParams.Envec.de_PreAlloc*/;
				StuffNumStringIfDOS(dlg, ((currVolParams.MaxBlocks - temp) * currVolParams.Info.id_BytesPerBlock) >> 10, LOG_SIZE_K_TEXT);
				StuffNumString(dlg, currVolParams.MaxBlocks - temp, LOG_SIZE_BLOCKS_TEXT);
				StuffNumStringIfDOS(dlg, ToK(currVolParams.FreeBytes), FREE_K_TEXT);
				StuffNumStringIfDOS(dlg, currVolParams.FreeBlocks, FREE_BLOCKS_TEXT);
/*
				if( temp = badBlockStats.OldBadCount ) {
					temp += temp / currVolParams.HashTableSize;
					temp++;
				}
*/
				StuffNumStringIfDOS(dlg, ToK(badBlockStats.OldBadCount * currVolParams.Info.id_BytesPerBlock), BAD_K_TEXT);
				StuffNumStringIfDOS(dlg, badBlockStats.OldBadCount, BAD_BLOCKS_TEXT);
			} /* else {
				for( temp = 0 ; temp < 6 ; temp++ ) {
					StuffString(dlg, strUnknown, SIZE_K_TEXT + temp);
				}
			} */
		}
	} else {
		StuffString(dlg, deviceName, DEVICE_TEXT);
		execBase = *((struct ExecBase **)4L);
		nodePtr = FindName(&execBase->DeviceList, currVolParams.ExecDevName);
		if( nodePtr == NULL ) {
			strcpy(strBuff, strUnknown);
		} else {
			NumToString(((struct Library *)nodePtr)->lib_Version, strBuff);
			strcat(strBuff, strVersSeparator);
			NumToString(((struct Library *)nodePtr)->lib_Revision, buff);
			strcat(strBuff, buff);
		}
		StuffString(dlg, strBuff, VERSION_TEXT);
		StuffString(dlg, currVolParams.ExecDevName, DRIVER_TEXT);
		StuffNumString(dlg, currVolParams.BlockSize, BLOCKSIZE_TEXT);
		StuffNumString(dlg, currVolParams.Envec.de_BlocksPerTrack, BLOCKSPERTRACK_TEXT);
		StuffNumString(dlg, currVolParams.MaxCylinders, CYLINDERS_TEXT);
		StuffNumString(dlg, currVolParams.Envec.de_LowCyl, LOWCYL_TEXT);
		StuffNumString(dlg, currVolParams.Envec.de_HighCyl, HIGHCYL_TEXT);
		StuffNumString(dlg, currVolParams.Envec.de_Surfaces, SURFACES_TEXT);
		StuffNumString(dlg, ToK(currVolParams.Envec.de_MaxTransfer), MAXTRANSFER_TEXT);
		StuffNumString(dlg, currVolParams.Envec.de_Reserved, RESERVED_TEXT);
		StuffNumString(dlg, currVolParams.Envec.de_PreAlloc, PREALLOC_TEXT);
		strBuff[0] = '\0';
		temp = currVolParams.Envec.de_BufMemType & 0xFFFF;
		if( (currVolParams.Envec.de_Mask & 0xFF000000) == 0 ) {
			if( (temp & MEMF_CHIP) == 0 ) {
				temp |= MEMF_24BITDMA;
			}
		}
		if( temp & MEMF_PUBLIC ) {
			AddStrToMemStr(strPublic, strBuff);
		}
		if( temp & MEMF_CHIP ) {
			AddStrToMemStr(strChip, strBuff);
		}
		if( temp & MEMF_FAST ) {
			AddStrToMemStr(strFast, strBuff);
		}
		if( temp & MEMF_LOCAL ) {
			AddStrToMemStr(strLocal, strBuff);
		}
		if( temp & MEMF_24BITDMA ) {
			AddStrToMemStr(str24BitDMA, strBuff);
		}
		if( strBuff[0] == '\0' ) {
			strcpy(strBuff, strAny);
		}
		StuffString(dlg, strBuff, MEMTYPE_TEXT);
	}
}

/*
 * Append a long field which contains probable ASCII characters into an octal string.
 */
 
static void AppendLongToOctalStr(ULONG num, TextPtr str)
{
	register WORD i;
	register UBYTE ch;
	
	if( num != 0xFFFFFFFF ) {
		*str++ = ' ';
		*str++ = '(';
		for( i = 0 ; i <= 3 ; i++ ) {
			ch = ((UBYTE *)&num)[i];
			if( (ch < ' ') || (ch > 127) ) {
				str = ConvertCharToOctalStr(ch, str);
			} else {
				*str++ = ch;
			}
		}
		*str++ = ')';
		*str = '\0';
	}
}
	
/*
 * Convert an octal byte to an octal string.
 */
 
static TextPtr ConvertCharToOctalStr(register UBYTE ch, register TextPtr str)
{
	ULONG num = 0L;
	register TextPtr ptr = &(((UBYTE *)&num)[2]);
	
	*str++ = '\\';
	*ptr-- = (ch & 7) | '0';
	*ptr-- = ((ch & 0x38) >> 3) | '0';
	if( (*ptr = (ch >> 6) | '0') == '0' ) {
		ptr++;
		if( *ptr == '0' ) {
			ptr++;
		}
	}
	do {
		*str++ = *ptr++;
	} while(*ptr);
	return(str);
}

/*
 * Add to the memory string, catenating "|" if dividing words.
 */
 
static void AddStrToMemStr(TextPtr str, register TextPtr destStr)	
{
	if( *destStr ) {
		strcat(destStr, strOrSymbol);
	}
	strcat(destStr, str);
}

/*
 * Fill in a numeric field, inserting thousand separators only if under a million
 */
 
static void StuffNumString(DialogPtr dlg, ULONG num, WORD item)
{
	if( (num < 1000000) || !decimalLabel ) {
		OutputStatHexOrDecString(num, strBuff, decimalLabel);
	} else {
		NumToString(num, strBuff);
	}
	DoStuffString(dlg, strBuff, GadgetItem(dlg->FirstGadget, item), TRUE);
}

/*
 * Stuff the numeric field only if packets allowed us to get DOS info.
 */
 
static void StuffNumStringIfDOS(DialogPtr dlg, ULONG  num, WORD item)
{
	if( options.PrefsOpts.BypassDOS ) {
		DoStuffString(dlg, strQuestionMark, GadgetItem(dlg->FirstGadget, item), TRUE );
	} else {
		StuffNumString(dlg, num, item);
	}
}

/*
 * Fill in a textual field.
 */
 
static void StuffString(DialogPtr dlg, TextPtr str, WORD item)
{
	DoStuffString(dlg, str, GadgetItem(dlg->FirstGadget, item), FALSE);
}

/*
 * Draw the field specified, drawing an ellipsis if necessary.
 */
 
static void DoStuffString(DialogPtr dlg, TextPtr str, GadgetPtr gadget, BOOL right)
{
	Rectangle rect;
	WORD len;
	WORD width;

	if( str != NULL ) {	
		GetGadgetRect(gadget, dlg, NULL, &rect);
		InsetRect(&rect, 1, 3);
		width = rect.MaxX - rect.MinX;
		rect.MinX++;
		if( right ) {
			len = MIN(width, TextLength(dlg->RPort, str, strlen(str)));
		}
		Move(dlg->RPort, right ? rect.MaxX - len : rect.MinX, rect.MinY + charBaseline);
		TextInWidth(dlg->RPort, str, strlen(str), width, FALSE);
	}
}

/*
 * Check for a disk remove/insert on the current device, and if there is one,
 * change the title bar and current vol parameters accordingly.
 */
 
BOOL CheckForDiskChange()
{
	if( IsDiskChanged() ) {
		if( IsDiskPresent() ) {
			PutDevice();
			if( !GetDevice(TRUE) ) {
				return(FALSE);
			}
			if( !options.Devices ) {
				strcpy(choiceName, volumeName);
			}
		} else {
			volumeName[0] = '\0';
		}
		IsDiskChanged();

		ConstructWindowTitle(strBuff, mode);
		SetWTitle(cmdWindow, strBuff);
	}
	return(TRUE);
}
