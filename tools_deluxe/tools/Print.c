/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Quarterback Tools
 * Copyright (c) 1992 New Horizons Software
 *
 * Print routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <graphics/gfxbase.h>
#include <devices/printer.h>
#include <devices/prtbase.h>
#include <dos/dos.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <clib/alib_protos.h>

#include <string.h>

#include <Toolbox/DateTime.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/ScrollList.h>

#include "Tools.h"
#include "Proto.h"

/*
 * External variables
 */

extern MsgPortPtr	mainMsgPort;
extern BOOL 		pagePrinter;
extern UBYTE		printerName[];
extern ScreenPtr 	screen;
extern TextPtr		strPath, strSavePath;
extern TextChar 	strBuff[], choiceName[], strNewLine[], strAt[], strPage[];
extern TextChar	strToolsLog[], strCatalogOfDel[], strCatalogOfAll[];
extern TextChar	strDirOf[];
extern TextChar	strBlockLabel[], strBlockOn[], strEndBlockLabel[];
extern WORD			cpi, lpi;
extern UWORD		tabChars[];
extern WORD			mode;
extern ScrollListPtr	scrollList;
extern RequestPtr	requester;
extern WindowPtr	cmdWindow;
extern VolParams	currVolParams;
extern OptionsRec	options;
extern LONG			dialogVarNum;
extern RecoverStats	recoverStats;

/*
 * Local prototypes
 */
 
static PrintIOPtr	OpenPrinter(void);
static void			ClosePrinter(PrintIOPtr);
static void			SetCancelButton(BOOL);
static WORD			PrintPage(File, WORD, BOOL *);
static BOOL			NextPage(void);
static BOOL			InitCharPrint(File);
static BOOL			AdvanceLines(File, WORD);
static BOOL			PrintSpaces(File, WORD);

/*
 * Local variables
 */

static struct Preferences  origPrefs;  /* For OpenPrinter() call */
static struct Preferences  lastPrefs;	/* For GetSysPrefs() call */
  
static BOOL havePrinterType;	  		/* To avoid excessive calls to GetPrinterType() */

#define PRINT_OK		0
#define PRINT_ABORT		1
#define PRINT_DONE		2
#define PRINT_ERROR		-1

/*
#define PITCH_PICA		100
#define PITCH_PICAWIDE	50
#define PITCH_ELITE		120
#define PITCH_ELITEWIDE	60
#define PITCH_COND		150
#define PITCH_CONDWIDE	75
#define PITCH_ULTRA		172
#define PITCH_ULTRAWIDE	86
*/
#define PITCH_TO_DECIPOINTS(pitch)	((7200 + (pitch - 1))/(pitch))	/* Must round up */

/* TextChar styleCommUnderline[]	= "\033[4m"; */
TextChar styleCommItalic[]		= "\033[3m";
TextChar styleCommBold[]		= "\033[1m";
TextChar styleCommNormal[]		= "\033[0m";

/*
 *	Get new preferences values
 *	Return TRUE if preferences changed since last call
 */

BOOL GetSysPrefs()
{
	register WORD i, len;
	struct Preferences newPrefs;

/*
	Check for changed preferences
*/
	GetPrefs(&newPrefs, sizeof(struct Preferences));
	if (memcmp((BYTE *) &newPrefs, (BYTE *) &lastPrefs, sizeof(struct Preferences)) == 0)
		return (FALSE);
	BlockMove(&newPrefs, &lastPrefs, sizeof(struct Preferences));
/*
	Get printer name here
	All other printer defaults are caught by PrintDefault()
*/
	*printerName = '\"';
	len = strlen(lastPrefs.PrinterFilename);
	for (i = len; i; i--) {
		if (lastPrefs.PrinterFilename[i - 1] == ':' ||
			lastPrefs.PrinterFilename[i - 1] == '/')
			break;
	}
	len -= i;
	memcpy(printerName + 1, &lastPrefs.PrinterFilename[i], len);
	printerName[++len] = '\"';
	printerName[len + 1] = '\0';
	for (i = 1; i < len; i++) {
		if (printerName[i] == '_')
			printerName[i] = ' ';
	}
	if (printerName[1] >= 'a' && printerName[1] <= 'z')
		printerName[1] -= 'a' - 'A';
	havePrinterType = FALSE;
	return (TRUE);
}

/*
 * Request the next sheet of paper
 * Return success status
 */

static BOOL NextPage()
{
	WORD item;
	
	SetCancelButton(FALSE);
	item = StdRequest(REQ_NEXTPAGE);
	SetCancelButton(TRUE);
	return( (BOOL) (item == REQ_OK_BUTTON) ) ;
}

/*
 * Advance the specified number of lines
 * Return success status
 */

static BOOL AdvanceLines(File printer, WORD num)
{
	if (num > 0) {
		while (num--) {
			if (Write(printer, strNewLine, 1) != 1)
				return (FALSE);
		}
	}
	return (TRUE);
}

/*
 * Print the specified number of spaces
 * Return success status
 */

static BOOL PrintSpaces(File printer, WORD num)
{
	if (num > 0) {
		while (num--) {
			if (Write(printer, " ", 1) != 1)
				return (FALSE);
		}
	}
	return (TRUE);
}

/*
 * Write the header found on the beginning of file or every page.
 */
 
BOOL WriteHeader(File printer, register BOOL printing)
{
	register WORD len;
	struct DateStamp dateStamp;
	TextChar tempBuff[40];
	register BOOL success;
	register TextPtr ptr;
	BOOL advance = FALSE;
	
/*
	Construct first and second line of header (second is just a blank line).
*/	
	ptr = strToolsLog;
	strcpy(strBuff, ptr);
	DateStamp(&dateStamp);
	DateString(&dateStamp, (WORD) DATE_ABBRDAY, tempBuff);
	strcat(strBuff, tempBuff);
	strcat(strBuff, strAt);
	TimeString(&dateStamp, FALSE, TRUE, tempBuff);
	strcat(strBuff, tempBuff);
	len = strlen(strBuff);
	if( !printing ) {
		strBuff[len++] = LF;
		strBuff[len++] = LF;
		strBuff[len] = 0;
	}
	success = Write(printer, strBuff, len) == len;
	if( printing && success ) {
		success = AdvanceLines(printer, 2);
	}
/*
	Construct third and last line of header.
*/
	if( success ) {
		ptr = strBuff;
		if( mode == MODE_EDIT ) {
			strcpy(ptr, strBlockLabel);
			len = strlen(ptr);
			ptr[len++] = ' ';
			WriteEditHeader(&ptr[len]);
			strcat(ptr, strBlockOn);
			strcat(ptr, choiceName);
			strcat(ptr, strEndBlockLabel);
		} else if( mode == MODE_RECOVER ) {
			strcpy(ptr, options.RecoverOpts.ShowDelOnly ? strCatalogOfDel : strCatalogOfAll);
			len = strlen(ptr);
			CondensePath(strPath, &ptr[len], options.PrintInfo.PageWidth-len);
			advance = TRUE;
		} else {
			*ptr = 0;
		}
		success = WriteStr(ptr, printer, printing);
	}
	if( success && advance && printing ) {
		success = AdvanceLines(printer, 1);
	}
	return(success);
}

/*
 * Print the heading of a directory.
 */
 
WORD WriteDirHeading(File printer, register TextPtr buff, BOOL printing)
{
	WORD status = PRINT_OK;
	register WORD len;
	register WORD pageWidth;
	TextPtr ptr;
	register WORD fill;
	
	if( mode == MODE_RECOVER ) {
		pageWidth = options.PrintInfo.PageWidth;
		ptr = buff;
	
		*buff = '\0';
		if( !printing ) {
			*ptr++ = LF;
		}
		strcpy(ptr, strDirOf);
	
		len = strlen(buff);
		fill = pageWidth - len;
		
		if( fill > 0 ) {
			CondensePath(strPath, &buff[len], fill);
		}
		len = strlen(buff);
		buff[len++] = LF;
		buff[len] = '\0';
		if( Write(printer, buff, len) != len ) {
			status = PRINT_ERROR;
		}
	}
	return(status);
}

/*
 * Assemble a line of text for adding to a list item
 */

static void BuildListEntryWithSpaces(DirFibPtr dirFib, register TextPtr destPtr, BOOL printing)
{
	TextChar tempBuff[160];
	register TextPtr srcPtr = &tempBuff[0];
	register WORD tabNum = 0;
	register TextChar ch;
	register WORD fieldLen = 0;
	register WORD i;
	BOOL change = FALSE;
	
	BuildListEntry(dirFib, tempBuff);
	
	if( printing && (dirFib->df_Flags & FLAG_NOT_BUSY_MASK) ) {
		strcpy(destPtr, styleCommItalic);
		destPtr += 4;
		change = TRUE;
	}
	while( ch = *srcPtr++ ) {
		if( (ch == TAB) || (ch == BS) ) {
/*
	If filename has just been printed (always first field), return to normal.
*/
			if( change && (tabNum == 0) ) {
				for( i = 0 ; i < 4 ; i++ ) {
					*destPtr++ = styleCommNormal[i];
				}
				change = FALSE;
			}
			if( ch == TAB ) {
/*
	Pad rest of field with spaces, and then add a few more (left justify).
*/
				for( i = 0 ; i < (tabChars[tabNum] + 1) - fieldLen; i++ ) {
					*destPtr++ = ' ';
				}
			} else {
/*
	Right justify
*/
				destPtr -= fieldLen;
				i = tabChars[tabNum];
				BlockMove(destPtr, destPtr + (i - fieldLen), fieldLen);
				BlockSet(destPtr, i - fieldLen, ' ');
				destPtr += i;
				*destPtr++ = ' ';
			}
			fieldLen = 0;
			tabNum++;
		} else {
			fieldLen++;
			*destPtr++ = ch;
		}
	}
	*destPtr++ = LF;
	*destPtr = 0;
}

/*
 * Write a catalog or session log entry to a file or printer
 */
 
WORD WriteEntry(File printer, WORD *line, BOOL *printing)
{
	TextChar buff[PATHSIZE];
	register TextPtr text = &buff[0];
	register WORD status = PRINT_OK;
	register WORD num;
	register DirFibPtr dirFib;
	DirFibPtr tempFib;
	BOOL heading = FALSE;
		
	if( mode == MODE_RECOVER ) {
		if( (dirFib = recoverStats.CurFib) == NULL ) {
			if( (dirFib = recoverStats.ParentFib->df_CurFib) != NULL ) {
				while( (dirFib = NextDirItem(dirFib)) && (!IS_DIR(dirFib->df_Type)) );
			}
			while( dirFib == NULL ) {
				if( recoverStats.ParentFib == recoverStats.DirTree ) {
					return(PRINT_DONE);
				}
				TruncatePath();
				recoverStats.ParentFib = recoverStats.ParentFib->df_Parent;
				dirFib = recoverStats.ParentFib->df_CurFib;
				while( (dirFib = NextDirItem(dirFib)) && (!IS_DIR(dirFib->df_Type)) );
			}
			AppendPath(&dirFib->df_Name);
/*
	If not enough room to fit a header line set (1 line + blank), 
	then skip to next page now, else write it now.
*/
			if( *printing ) {
				num = (options.PrintInfo.PageHeight - 5) - *line;
				if( (options.PrintInfo.PrintRec.Flags & PRT_NOGAPS) || (num > 2) ) {
					if( !AdvanceLines(printer, 1) ) {
							status = PRINT_ERROR;
					} else {
						*line += 2;
						status = WriteDirHeading(printer, text, TRUE);
					}
				} else {
					*line += num;
					if( !AdvanceLines(printer, num) ) {
						status = PRINT_ERROR;
					}
					heading = TRUE;
				}
			} else {
				status = WriteDirHeading(printer, buff, FALSE);
			}
			recoverStats.ParentFib->df_CurFib = dirFib;
			if( (dirFib = NextDirItem(tempFib = (DirFibPtr)&dirFib->df_Child)) != NULL ) {
				recoverStats.ParentFib = recoverStats.ParentFib->df_CurFib;
				recoverStats.ParentFib->df_CurFib = tempFib;
			} else {
				TruncatePath();
			}
		}
		recoverStats.CurFib = NextDirItem(dirFib);

		if( dirFib != NULL ) {
			BuildListEntryWithSpaces(dirFib, text, *printing);
			num = strlen(text);
			if( Write(printer, text, num) != num ) {
				status = PRINT_ERROR;
			}
		}
		if( printing != NULL ) {
			*printing = heading;
		}
	} else {		
		if( scrollList != NULL ) {
			SLGetItem(scrollList, *line, buff);
			num = SLNumItems(scrollList);
		} else if( mode == MODE_EDIT ) {
			WriteEditLine(*line, buff);
			num = currVolParams.BlockSize >> 4;
		}
		if( !WriteStr(buff, printer, *printing) ) {
			status = PRINT_ERROR;
		} else if( *line >= num-1 ) {
			status = PRINT_DONE;
		}
	}
	return(status);
}

/*
 * Print one page of text, unless NOGAPS on, in which case doesn't
 * return until entire print job is finished.
 * Assume we start one line down from the top
 * Return -1(PRINT_ERROR) if error, 1(PRINT_ABORT) if cancelled, 0 if OK
 */

static WORD PrintPage(File printer, WORD page, BOOL *heading)
{
	register WORD len, numLines;
	TextChar buff[256];
	register WORD status = PRINT_ERROR;
	BOOL noGaps;
	WORD line;
	
/*
	Print header
*/
	numLines = options.PrintInfo.PageHeight - 5;
	noGaps = options.PrintInfo.PrintRec.Flags & PRT_NOGAPS;
	line = 0;
	if( WriteHeader(printer, TRUE) ) {
/*
	Print body, beginning with possibly a directory header line.
*/
		if( *heading ) {
			*heading = FALSE;
			if( (status = WriteDirHeading(printer, buff, TRUE)) == PRINT_ERROR ) {
				return(status);
			}
			line++;
		}
		status = PRINT_OK;
/*
	Main print loop
*/
		while( (noGaps || (line < numLines)) && (status == PRINT_OK) ) {

			if (CheckRequest(mainMsgPort, cmdWindow, DialogFilter) == REQ_CANCEL_BUTTON)
				return(PRINT_ABORT);
			*heading = TRUE;
			status = WriteEntry(printer, &line, heading);
			line++;
		}
/*
 	Print footer
*/
		if( !noGaps ) {
			if( AdvanceLines(printer, numLines - line) ) {
				dialogVarNum = page;
				ParseDialogString(strPage, buff);
				len = strlen(buff);
				buff[len++] = '\f';
				if( (!PrintSpaces(printer, options.PrintInfo.PageWidth - (len-1))) ||
					(Write(printer, buff, len) != len) ) {
					status = PRINT_ERROR;
				}
			} else {
				status = PRINT_ERROR;
			}
		} else {
			if( !AdvanceLines(printer, 2) ) {
				status = PRINT_ERROR;
			}
		}
	}
	return(status);
}

/*
 * Print catalog
 */

BOOL PrintList()
{
	register WORD page;
	register BOOL success;
	register File printer;
	register PrintRecPtr printRec = &options.PrintInfo.PrintRec;
	register WORD status = PRINT_OK;
	register WORD copies = printRec->Copies;
	BOOL heading;

	success = FALSE;	
	BeginWait();
/*
	Open the printer device for output
	Set for no margins and proper setup
*/
/*
	Put up cancel requester
*/
	if( (requester = DoGetRequest(REQ_CANCELPRINT)) != NULL ) {

		SetCancelButton(FALSE);
		if( (printer = Open(printRec->FileName, MODE_NEWFILE)) != NULL ) {
/*
	Print the pages
*/
			if( InitCharPrint(printer) ) {
				success = TRUE;
				SetCancelButton(TRUE);
				InitPrint();
				while( copies ) {
					page = 1;
					copies--;
					heading = TRUE;
					do {
						status = PrintPage(printer, page, &heading);
						if( status == PRINT_OK ) {
							if( (!(printRec->Flags & PRT_TOFILE)) 
								&& printRec->PaperFeed == PRT_CUTSHEET && !NextPage()) {
								status = PRINT_ERROR;
							}
						}
						page++;
					} while( status == PRINT_OK );
					/*
					if( status == PRINT_ERROR || Write(printer, "\f", 1) != 1) {
						success = FALSE;
					}
					*/
				}
				EndPrint();
			}
			Close(printer);
		}
		DestroyRequest(requester);
		if( status == PRINT_ERROR ) {
			Error(ERR_PRINT);
		}
	}
	EndWait();
	return (success);
}

/*
 * Initialize printer for character-based operation
 */
 
static BOOL InitCharPrint(register File printer )
{
	register BOOL success = FALSE;
	PrintRecPtr printRec = &options.PrintInfo.PrintRec;
	register ULONG height;
	TextChar command[10];
	register WORD len;
			
	if (Write(printer, "\033#3", 3) == 3) { 
		if( !((printRec->Quality == PRT_NLQ &&
			Write(printer, "\033[2\"z", 5) != 5) ||
							(printRec->Quality == PRT_DRAFT &&
							Write(printer, "\033[1\"z", 5) != 5) ) ) {
			if(!((cpi == 10 && Write(printer, "\033[0w", 4) != 4) ||
					 (cpi == 12 && Write(printer, "\033[2w", 4) != 4) ||
					 (cpi == 15 && Write(printer, "\033[4w", 4) != 4))){
				if(!((lpi == 6 && Write(printer, "\033[1z", 4) != 4) ||
					  (lpi == 8 && Write(printer, "\033[0z", 4) != 4))){

/*
	Set form height and turn off perf skip
*/
					success = TRUE;
					if( !pagePrinter ) {
						height = ((LONG) printRec->PaperHeight*6L)/720L;
						if( height > 255 )
							height = 255;
						strcpy(command, "\033[");
						NumToString(height, strBuff);
						strcat(command, strBuff);
						strcat(command, "t");
						len = strlen(command);
/*
	Issue "no perf skip" and "normal character set" commands.
*/
						if(   ( Write(printer, command, len) != len ) ||
								( Write( printer, "\033[0q", 4 ) != 4 ) || 
								( Write( printer, styleCommNormal, 4 ) != 4 ) ) {
							success = FALSE;
						}
					}
				}
			}
		}
	}
	return(success);
}
								 
/*
 *	Enable/disable cancel button and set pointer
 */

static void SetCancelButton(BOOL on)
{
	EnableGadgetItem(requester->ReqGadget, REQ_CANCEL_BUTTON, cmdWindow, requester, on);
	if( on ) {
		SetArrowPointer();
	} else {
		SetWaitPointer();
	}
}

/*
 *	Get the printer type
 */

void GetPrinterType()
{
	register WORD i;
	register PrintIOPtr printIO;
	register struct PrinterData *pData;
	register struct PrinterExtendedData *ped;

	if (!havePrinterType) {
/*
	Set default values
*/
		pagePrinter = FALSE;
/*
	Get printer values
*/
		if ((printIO = OpenPrinter()) == NULL) {
			BeginWait();
			for (i = 0; i < 10; i++) {				/* Retry counter */
				if ((printIO = OpenPrinter()) != NULL)
					break;
				Delay(10L);
			}
			EndWait();
		}
		if (printIO != NULL) {
			pData = (struct PrinterData *) printIO->Std.io_Device;
			if (pData->pd_SegmentData) {
				ped = &pData->pd_SegmentData->ps_PED;
				pagePrinter = (ped->ped_MaxYDots != 0);
			}
			ClosePrinter(printIO);
			havePrinterType = TRUE;
		}
	}
}

/*
 *	Open the printer for direct output
 */

static PrintIOPtr OpenPrinter()
{
	register PrintIOPtr printIO = NULL;
	MsgPortPtr replyPort;
	register struct PrinterData *pData;

	if ((replyPort = CreatePort(NULL, 0)) != NULL) {
		if ((printIO = (PrintIOPtr) CreateExtIO(replyPort, sizeof(PrintIO))) == NULL) {
			DeletePort(replyPort);
		} else {
			if (OpenDevice("printer.device", 0, (IOReqPtr) printIO, 0)) {
				DeleteExtIO((IOReqPtr) printIO);
				DeletePort(replyPort);
				return (NULL);
			}
			pData = (struct PrinterData *) printIO->Std.io_Device;	

			BlockMove(&pData->pd_Preferences, &origPrefs, sizeof(struct Preferences));

			pData->pd_Preferences.PrintImage = IMAGE_POSITIVE;
			if (pData->pd_Preferences.PrintShade == SHADE_BW)
				pData->pd_Preferences.PrintShade = SHADE_GREYSCALE;
			pData->pd_Preferences.PrintLeftMargin = 1;
			pData->pd_Preferences.PrintRightMargin = 255;
			pData->pd_Preferences.PaperLength = 999;
			pData->pd_Preferences.PrintXOffset = 0;
			pData->pd_Preferences.PrintFlags &= ~(CORRECT_RGB_MASK | CENTER_IMAGE |
											  INTEGER_SCALING | DIMENSIONS_MASK |
											  DITHERING_MASK);
			pData->pd_Preferences.PrintFlags |= IGNORE_DIMENSIONS | ORDERED_DITHERING;
		}
	}
	return (printIO);
}

/*
 * Set up global variables to do a print of the catalog
 */
 
void InitPrint()
{
	if( mode == MODE_RECOVER ) {
		strcpy(strSavePath, strPath);
		InitPath();
		recoverStats.ParentFib = recoverStats.DirTree;
		recoverStats.SaveCurFib = recoverStats.CurFib;
		recoverStats.CurFib = NextDirItem((DirFibPtr)&recoverStats.DirTree->df_Child);
		recoverStats.ParentFib->df_CurFib = (DirFibPtr) &recoverStats.DirTree->df_Child;
	}
}

/*
 * Call this when finished with catalog, match with InitPrint()
 */
 
void EndPrint()
{
	if( mode == MODE_RECOVER ) {
		recoverStats.CurFib = recoverStats.SaveCurFib;
		strcpy(strPath, strSavePath);
	}
}

/*
 *	Close the printer device previously openned by OpenPrinter
 */

static void ClosePrinter(register PrintIOPtr printIO)
{
	MsgPortPtr replyPort;
	register struct PrinterData *pData;

	pData = (struct PrinterData *) printIO->Std.io_Device;

	BlockMove(&origPrefs, &pData->pd_Preferences, sizeof(struct Preferences));

	replyPort = ((IOReqPtr) printIO)->io_Message.mn_ReplyPort;
	CloseDevice((IOReqPtr) printIO);
	DeleteExtIO((IOReqPtr) printIO);
	DeletePort(replyPort);
}
