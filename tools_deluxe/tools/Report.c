/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Report generator for user logs
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <dos/dos.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/icon.h>

#include <string.h>

#include <Rexx/errors.h>

#include <Toolbox/DateTime.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>
#include <Toolbox/LocalData.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr	cmdWindow;
extern MsgPortPtr	mainMsgPort;

extern TextPtr		strPath;
extern TextChar	strBuff[], currFileName[];

extern TextChar	strCompleted[], strAborted[], strStarted[], strAt[];
extern TextChar	strCommentBegin[], strEllipsis[];
extern TextChar	strDrawer[], strFile[], strLink[], strEntry[];
extern TextChar	strAbort[], strIgnore[], strRepair[], strProceed[], strSkipFile[];
extern TextChar	strDrawerLabel[], strEntryLabel[], strErrorLabel[], strActionLabel[];
extern TextChar	strRepairFound[], strRepairDeleted[];

extern TextChar	strMajor[], strMinor[], strPeriod[];
extern TextPtr		strsErrors[], repairActionsVerbs[];
extern TextPtr		repairErrors[], repairActions[], strsResults[], strsResStatus[];
extern TextPtr		dialogVarStr;
extern LONG			dialogVarNum;
extern GadgetPtr	currGadgList;
extern ScrollListPtr	scrollList;
extern ULONG		topListNum;
extern OptionsRec	options;
extern RepairStats	repairStats;
extern BOOL			skipFlag, cancelFlag;
extern WORD			mode, action, stage, scriptError;
extern LONG			maxWidth;

enum {
	ACTION_WILL_BE_TAKEN, ACTION_TAKEN, ACTION_NOT_TAKEN
};

/*
 * Local prototypes
 */

static TextPtr LoadStatusMsg(TextPtr);

/*
 * If an error occurs that we can't do anything about, call this
 * (it has just the "OK" button).
 */
 
void ReportInfo(WORD errNum)
{
	if( scrollList != NULL ) {
		ParseDialogString(strsErrors[errNum], strBuff);
		AddToScrollList(strBuff);
	}
	MotorOff();
	Error(errNum);
	/*
	if( action == OPER_IN_PROG ) {
		scriptError = MAX(scriptError, RC_WARN);
	}
	*/
}

/*
 * Just as in the assembly version (1.x), construct a string based on
 * error and action indices. If error negative, it is a major error, else
 * it is minor. A major error is defined as some problem which almost
 * guarantees data loss.
 * Returns TRUE if user hits the default button (OK).
 */
 
BOOL ReportError(WORD errorCode, WORD actionNum)
{
	register WORD errNum = errorCode;
	register TextPtr buff;
	register TextPtr buff2;
	TextPtr buff3;
	TextPtr buff4;
	register WORD temp = OK_BUTTON;
	register WORD button;
	WORD severity;
	TextChar lastLineBuff[80];
	
	if( errNum < 0 ) {
		errNum = -errNum;
		repairStats.MajorErrCount++;
		severity = 2;
	} else {
		repairStats.MinorErrCount++;
		severity = 2;
	}
	if( (mode == MODE_REPAIR) && stage ) {
/*
	Make sure we have report capabilities.
*/
		buff = &strBuff[0];
		button = SKIPFILE_BUTTON;
		dialogVarStr = strEntry;
		switch( repairStats.CurrentBlockType ) {
		case ST_FILE:
			dialogVarStr = strFile;
			break;
		case ST_SOFTLINK:
		case ST_LINKDIR:
		case ST_LINKFILE:
			dialogVarStr = strLink;
			break;
		case ST_ROOT:
		case ST_USERDIR:
			dialogVarStr = strDrawer;
			button = SKIP_BUTTON;
			break;
		case 0:								/* Used by RepairBitMap(). */
			button = CANCEL_BUTTON;
			break;
		default:
			dialogVarStr = currFileName;	/* Unknown secondary type! Print its name. */
			break;
		}
		SLDoDraw(scrollList, FALSE);
		AddBlankLineToScrollList();
		strcpy(buff, strDrawerLabel);
		temp = strlen(strDrawerLabel);
		CondensePath(strPath, &buff[temp], 100 - temp);
		AddToScrollList(buff);
		CondensePath(strPath, &buff[temp], MAX_FILENAME_LEN);
		buff2 = &buff[strlen(buff)+1];
		*(buff2-1) = '\n';
		strcpy(buff2, strEntryLabel);
		strcat(buff2, currFileName);
		AddToScrollList(buff2);
		buff3 = &buff2[strlen(buff2)+1];
		*(buff3-1) = '\n';
		strcpy(buff3, strErrorLabel);
		ParseDialogString(repairErrors[errNum], &buff3[strlen(buff3)]);
		/*strcat(buff3, strPeriod);*/
		AddToScrollList(buff3);
		buff4 = &buff3[strlen(buff3)+1];
		*(buff4-1) = '\n';
		strcpy(buff4, strActionLabel);
		strcat(buff4, repairActions[options.RepairOpts.Repair ? actionNum : ANACT_NONE]);
		strcpy(lastLineBuff, buff4);
		dialogVarStr = repairActionsVerbs[ACTION_WILL_BE_TAKEN];
		ParseDialogString(lastLineBuff, buff4);
		/*strcat(buff4, strPeriod);*/
		if( options.PrefsOpts.Interactive ) {
			temp = OK_BUTTON;
		} else {
			if( options.RepairOpts.Repair ) {
				temp = DoNonParseDialog(buff, button, strRepair, strAbort, strIgnore, strSkipFile, TRUE);
			} else {
				if( button == SKIPFILE_BUTTON ) {
					button--;
				}
				temp = DoNonParseDialog(buff, button, strProceed, strAbort, strSkipFile, NULL, TRUE);
				if( temp == SKIP_BUTTON ) {
					temp++;		/* No Ignore button, actually a SKIPFILE_BUTTON! */
				}
			}
		}
		switch( temp ) {
		case CANCEL_BUTTON:
			cancelFlag = TRUE;
			dialogVarStr = repairActionsVerbs[ACTION_NOT_TAKEN];
			break;
		case SKIPFILE_BUTTON:
			repairStats.FileSkipped = skipFlag = TRUE;
		case SKIP_BUTTON:
			dialogVarStr = repairActionsVerbs[ACTION_NOT_TAKEN];
			break;
		case OK_BUTTON:
		default:
			dialogVarStr = repairActionsVerbs[ACTION_TAKEN];
			break;
		}
		ParseDialogString(lastLineBuff, strBuff);
		AddToScrollList(strBuff);
		SLDoDraw(scrollList, TRUE);
		
		/*if( temp != SKIPFILE_BUTTON ) {*/
			scriptError /*= MAX(scriptError, severity);*/ |= severity;
		/*}*/
/*
	Record the fact that an error was skipped, and whether it was major or not,
	for the final status report.
*/
		if( (temp != OK_BUTTON) || (!options.RepairOpts.Repair) ) {
			if( repairStats.ExistingErr >= 0 ) {
				repairStats.ExistingErr = errorCode;
			}
		}
/*	} else {*/
/*
	If not in repair mode, this routine should return FALSE, not TRUE!
*/
/*
		strcpy(strBuff, repairErrors[errNum]);
		strcat(strBuff, strPeriod);
		temp = !WarningDialog(strBuff, OK_BUTTON);
*/
	}
	return(temp == OK_BUTTON);
}

/*
 * Since you might not be able to display the whole pathname, condense it
 * into a displayable form, by putting an ellipsis in front if it can't
 * show the whole name. The small path buffer should be able to hold "maxChars"
 * worth of characters.
 */
 
void CondensePath(TextPtr path, TextPtr smallPath, WORD maxChars)
{
	register TextPtr ptr = path;
	
	smallPath[0] = '\0';
	
	if( strlen(ptr) > maxChars ) {
		if( (ptr = strrchr(ptr, '/')) == NULL ) {
			ptr = strrchr(path, ':');
		}
		strcpy(smallPath, strEllipsis);
	}
	if( ptr != NULL ) {
		strncat(smallPath, ptr, maxChars - strlen(smallPath));
	}
}

/*
 * Given an input string, catenate volume status to it to produce a status string.
 */
 
static TextPtr LoadStatusMsg(TextPtr str)
{
	register WORD index;
	register WORD len;
	
	len = stccpy(strBuff, str, PATHSIZE) - 1;

	if( repairStats.VolBadFlag ) {
		index = VOL_BAD;
	} else {
		if( repairStats.ExistingErr == 0 ) {
			index = VOL_FIXED;
			if( repairStats.MajorErrCount ) {
				dialogVarStr = strMajor;
			} else if( repairStats.MinorErrCount ) {
				dialogVarStr = strMinor;
			} else {
				index = VOL_GOOD;
			}
		} else {
			index = VOL_PROBS;
			dialogVarStr = (repairStats.ExistingErr > 0) ? strMinor : strMajor;
		}
	}
	ParseDialogString(strsResStatus[index], &strBuff[len]);
	return(strBuff);
}
	
/*
 * Output final analyze/repair statistics for file block check.
 */
 
void OutputRepairStats()
{
	register WORD i;
	register ULONG *ptr = (ULONG *) &repairStats;

/*
	Output a blank line for aesthetics.
*/
	SLDoDraw(scrollList, FALSE);
	AddBlankLineToScrollList();
	
	dialogVarStr = options.RepairOpts.Repair ? strRepairDeleted : strRepairFound;
	for( i = 0 ; i < 9 ; i++ ) {
		dialogVarNum = *ptr++;
		ParseDialogString(strsResults[i], strBuff);
		AddToScrollList(strBuff);
	}
	AddToScrollList(LoadStatusMsg(strsResults[i]));
	AddBlankLineToScrollList();
	SLDoDraw(scrollList, TRUE);
}

/*
 * Add a blank line to the scroll list.
 */
 
void AddBlankLineToScrollList()
{
	UBYTE zero = 0;
	
	AddToScrollList(&zero);
}

/*
 * Add the text passed to the scroll list.
 */
 
void AddToScrollList(TextPtr str)
{
	WORD len = strlen(str);
	LONG width;
	
	topListNum++;
	SLAddItem(scrollList, str, len, topListNum);
	if( (width = TextLength(cmdWindow->RPort, str, len)) > maxWidth ) {
		/*RecalcMaxWidth();*/
		maxWidth = width;
		UpdateHScrollBar(GadgetItem(currGadgList, LEFT_ARROW));
	}
	SLAutoScroll(scrollList, topListNum);
}

/*
 * Output a string describing an event was completed.
 * Append whether operation was successful to string passed.
 */

void EndEventString(TextPtr str, BOOL success)
{
	struct DateStamp dateStamp;
	TextChar wordBuff[100];
	
	strcat(strcpy(wordBuff, str), success ? strCompleted : strAborted);
	DateStamp(&dateStamp);
	EventString(&dateStamp, wordBuff, strBuff, TRUE);
	AddToScrollList(strBuff);
}

/*
 * Begin the event
 */
 
void BeginEventString(TextPtr str)
{
	TextChar wordBuff[80];
	struct DateStamp dateStamp;

	/*AddBlankLineToScrollList();*/
	strcat(strcpy(wordBuff, str), strStarted);
	DateStamp(&dateStamp);
	EventString(&dateStamp, wordBuff, strBuff, TRUE);
	AddToScrollList(strBuff);
}	

/*
 * Create a string in "buffStr", constructing a sentence for output based on time.
 */

void EventString(struct DateStamp *dateStamp, TextPtr str, register TextPtr buffStr, BOOL secs)
{
	TextChar buff[50];

	strcat(strcpy(buffStr, strCommentBegin), str);
	DateString(dateStamp, DATE_ABBR, buff);
	strcat(strcat(buffStr, buff), strAt);
	TimeString(dateStamp, TRUE, TIME_12HOUR, buff);
	strcat(buffStr, buff);
	/*strcat(buffStr, strCommentEnd);*/
}

/*
 * Outputs the current info to save to a file, in a non-printing mode.
 */

BOOL OutputReport(TextPtr fileName)
{
	BOOL success = FALSE;
	register File file;
	WORD line = 0;
	register WORD status;
	BOOL temp;
	
	SetBusyPointer();
	if( (file = Open(fileName, MODE_NEWFILE)) != NULL ) {
		InitPrint();
		if( WriteHeader(file, FALSE) ) {
			if( WriteDirHeading(file, strBuff, FALSE) == PRINT_OK ) {
				SetBusyPointer();
				do {
					temp = FALSE;
					status = WriteEntry(file, &line, &temp);
					NextBusyPointer();
					line++;
				} while( (status != PRINT_DONE) && (status != PRINT_ERROR) );
				success = status == PRINT_DONE;
			}
		}
		Close(file);
		if( success ) {
			SaveIcon(fileName, ICON_TEXT);
		}
		EndPrint();
	} else {
		DOSError(ERR_SAVE, IoErr());
	}
	SetArrowPointer();
	return(success);
}

/*
 *	Write a string to the file lock specified
 */

BOOL WriteStr(TextPtr str, File file, BOOL printing)
{
	register UWORD len;
	BOOL success;
	
	len = strlen(str);
	str[len++] = printing ? '\n' : 0x0A;
	success = (Write(file, str, len) == len);
	str[--len] = 0x00;
	return(success);
}


