/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Disk editor user-interface/function code.
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <devices/trackdisk.h>
#include <graphics/gfxmacros.h>

#include <string.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Border.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Request.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	Externals
 */

extern UBYTE			_tbPenLight, _tbPenDark, _tbPenBlack, _tbPenWhite;
extern BOOL				_tbSmartWindows, altKey;
extern DialogTemplate	_sfpPutFileDlgTempl;
extern UBYTE			_strYes[], _strNo[];
extern WORD				_tbXSize, _tbYSize;

extern TextPtr			destFileName, dialogVarStr;
extern TextChar		toUpper[];
extern TextChar		strDec[], strHex[];
extern TextChar		strSaveCapture[], strDefCapture[];

extern WindowPtr		cmdWindow;
extern ScreenPtr		screen;
extern MsgPortPtr		mainMsgPort;
extern TextPtr			strsErrors[];
extern TextChar		strBuff[], choiceName[], deviceName[];
extern BOOL				closeFlag, quitFlag, abortFlag, bitMapFlag, decimalLabel;
extern GadgetPtr		currGadgList;
extern ReqTemplPtr		reqList[];
extern RequestPtr		requester;
extern Rectangle		statusRect, labelRect;
extern VolParams		currVolParams;
extern UWORD			charBaseline;
extern WORD				intuiVersion, action, mode;
extern LONG				xoffset, yoffset, maxWidth;
extern UBYTE			*blockBuffer;
extern TextFontPtr		smallFont;
extern GadgetTemplate	editGadgets[];
extern MenuTemplate		diskEditorMenu[];
extern OptionsRec		options;
extern EditStats		editStats;
extern TextChar		strFileHdr[], strFileExt[], strCacheType[], strData[];
extern TextChar		strBoot[], strRoot[], strBitMap[], strBitMapExt[];
extern TextChar		strDrawerType[], strSoftLink[], strHardLink[];
extern TextPtr			strRootBlockTypes[], strDirAndLinkBlockTypes[], strOFSDataBlockTypes[];
extern TextPtr			strFileBlockTypes[], strExtBlockTypes[], strCacheBlockTypes[];
extern TextChar		strReserved[], strBitMapData[], strBitMapKey[], strChecksum[];
extern TextChar		strHashTable[], strExtKey[], strBitMapValid[], strBitMapExtKey[];
extern TextChar		strVolumeName[], strDirName[], strLinkName[], strFileName[];
extern TextChar		strHashChain[], strParentKey[], strSecType[];
extern TextChar		strModDate[], strVolModDate[], strVolCrDate[], strRootModDate[];
extern TextChar		strPrevLink[], strLinkToObject[], strProt[], strComment[];
extern TextChar		strFSID[], strFileSize[], strFileData[], strUnknown[];
extern TextChar		strCacheKey[], strSoftLinkObj[], strOwnerXID[];
extern TextChar		strKeyTable[], strDirCacheInfo[];
extern TextChar		strMoving[], strClosing[];
#ifdef DRAW_TYPE_ICONS
extern ImagePtr		iconList[];
#endif

UWORD		*areaBuff;
PLANEPTR	planePtr;
WORD		xRad;
WORD		yRad;
UWORD 	rasSize;

/*
 * Local prototypes and definitions
 */

static void		ReadError(ULONG); 
static BOOL		WriteEditBlock(void);
static BOOL		EditListFilter(IntuiMsgPtr, WORD *);
static UWORD	NumEditLines(RectPtr);
static BOOL		ClickInEdit(PointPtr, ULONG, ULONG);
static void		AdjustCursorPos(UWORD);
static void		RecalcCursor(ULONG);
static void		InvertCursor(RectPtr);
static void		SetTypeLabel(void);
static void		SetFieldLabel(void);
static void		RefreshCurrentLine(void);
static void		RefreshMiniIcon(void);
static void		FillBusyLightCircle(RastPtr, WORD, WORD);
static void		WriteASCIIGroup16(UBYTE *, UBYTE *);
static BOOL		DoCalcChecksum(void);
static BOOL		CreateCaptureFile(void);
static BOOL		WriteToCaptureFile(void);
static BOOL		CloseCaptureFile(void);
static ULONG	EditStringToNum(TextPtr, BOOL);
static ULONG	HexStringToNum(TextPtr);
static UWORD	ConvertHexToBinStr(TextPtr, TextPtr);
static BOOL 	StuffFindTextBuffer(WORD, TextPtr);
static void		UpdateFindBlock(ULONG *);
static WORD		FastCmpString(TextPtr, TextPtr, WORD);
static BOOL		FindRequestFilter(IntuiMsgPtr, WORD *);
static void		BumpFindCounters(void);
static BOOL		DoFind(void);
static BOOL 	DoFindNext(void);
static void		GoToNewBlock(BOOL);
static BOOL		DoCalcHash(void);
static BOOL 	DoGoTo(void);

enum {
	FIND_TEXT = 2,
	FIND_POPUP,
	FIND_FORWARD_RADBTN,
	FIND_REVERSE_RADBTN,
	FIND_ALL_RADBTN,
	FIND_FROM_RADBTN,
	FIND_START_TEXT,
	FIND_END_TEXT,
	FIND_WARN_BOX,
};

enum {
	ASCII_ITEM,
	HEX_ITEM,
	CURR_KEY_ITEM = 3,
	PARENT_ITEM,
	HEADER_ITEM
};

enum {
	GOTO_TEXT = 2,
	GOTO_POPUP,
};

enum {
	HASH_TEXT = 2,
};

#define LINE_X_OFFSET		291
#define BUSY_LIGHT_RADIUS	3

/*
 * Deletes the "Edit" menu at the end of the editor.
 * NOT A GENERIC ROUTINE BY ANY STRETCH!
 * It is hoped that this function will be supplied in the toolbox someday.
 */
 
static void DeleteMenu(WindowPtr, UWORD);
static void DeleteMenu(WindowPtr window, UWORD menuNum)
{
	MenuPtr menuStrip = window->MenuStrip;
	register MenuPtr menu = menuStrip;

	ClearMenuStrip(cmdWindow);
	while( --menuNum ) {
		menu = menu->NextMenu;
	}
	if( menu->NextMenu != NULL ) {
		DisposeMenu(menu->NextMenu);
		menu->NextMenu = NULL;
	}
	SetMenuStrip(cmdWindow, menuStrip);
}

/*
 * Initialize edit parameters upon opening the disk editor window.
 */
 
BOOL SetupEditMode()
{
	register BOOL success;
	MenuPtr menu;
/*	register ULONG temp;*/
	
	BlockClear(&editStats, sizeof(EditStats));
#ifdef DRAW_TYPE_ICONS
	if( success = PromptForDisk() && AllocMiniIcons() ) {
#else
	if( success = PromptForDisk() ) {
#endif
		GetDiskType();
		if( !(currVolParams.NonFSDisk || currVolParams.DosDisk) ) {
			success = TRUE;
			if( !(currVolParams.NonFSDisk |= DoDialog(strsErrors[ERR_UNKNOWN_DISK], CANCEL_BUTTON, _strYes, _strNo, NULL, FALSE) == CANCEL_BUTTON) ) {
				success = CheckBootBlockReq();
			}
		}
		if( success &= ((menu = GetMenu(&diskEditorMenu[0])) != NULL) ) {
			CheckMenu(cmdWindow, MENUITEM(EDIT_MENU, AUTOCALC_CKSUM_ITEM, 0), options.EditOpts.AutoChecksum);
			
			if( success = (currVolParams.BitMapBlocks == 0) || AllocateBitMapBuffers() ) {
				if( (!currVolParams.NonFSDisk) && currVolParams.BitMapBlocks ) {
					if( LoadBitMapKeys() ) {
						LoadBitMapData();
					}
				}
				InsertMenu(cmdWindow, menu, 0xFFFF);
				mode = MODE_EDIT;
				action = OPER_READY;
		
				decimalLabel = options.PrefsOpts.DecimalLabel;
		
				editStats.FindWarn = TRUE;
				xoffset = currVolParams.NonFSDisk ? 0 : currVolParams.RootBlock;
				editStats.FindEndBlock = currVolParams.MaxBlocks-1;
				yoffset = 0;
				editStats.LastGoToBlock--;				/* Force setting to current */
				editStats.HistoryBlock--;
				editStats.BitMapKey = 0xFFFFFFFF;
				RecalcMaxWidth();
	
				AddGList(cmdWindow, currGadgList, -1, -1, 0);
				
				xRad = (MAX(6,_tbXSize) * 3) >> 3;
				yRad = xRad;
				rasSize = RASSIZE(xRad+17+2, yRad+1+2);
				areaBuff = MemAlloc((2*5)+20, MEMF_CLEAR);
				planePtr = AllocRaster(xRad+17+2+9, yRad+1+2+9);
				
				GetGadgetRect(GadgetItem(currGadgList, EDIT_TYPE_BORDER), cmdWindow, NULL, &statusRect);
				statusRect.MinX = statusRect.MaxX + 4;
#ifdef DRAW_TYPE_ICONS
				statusRect.MaxX = statusRect.MinX + iconList[DRAWER_ICON]->Width;
				temp = iconList[DOC_ICON]->Height;	/* Tallest icon */
				statusRect.MinY = (statusRect.MinY + statusRect.MaxY - temp) >> 1;
				statusRect.MaxY = statusRect.MinY + temp;
#else
				statusRect.MaxX = statusRect.MinX + (xRad + xRad);
				statusRect.MinY = ((statusRect.MinY + statusRect.MaxY - yRad) >> 1)-1;
				statusRect.MaxY = statusRect.MinY + (yRad + yRad);
#endif				
				(void) IsDiskChanged();					/* Initialize counter */
				
				GetEditBlock(1);							/* Get root (or boot) block */
				MotorOff();
				if( editStats.BlockBufferValid && (!currVolParams.NonFSDisk) ) {
					editStats.BitMapKey = ((RootBlockEndPtr)(((UBYTE *)blockBuffer) + currVolParams.BlockSize - sizeof(RootBlockEnd)))->BMPtrs[0];
					/*
					if( temp = ((RootBlockEndPtr)(((UBYTE *)blockBuffer) + currVolParams.BlockSize - sizeof(RootBlockEnd)))->BMExt ) {
						editStats.BitMapExtKey = temp;
					}
					*/
				}
				RefreshEditWindow(TRUE);
	
				SetAllMenus();
			}
		}
	}
	SetArrowPointer();
	(void) IsDiskChanged();
	return(success);
}

/*
 * Get rid of the disk editor menu.
 */
 
BOOL CleanupEditMode()
{
	BOOL success;
	
	if( success = PromptForChangedBlock(TRUE) != CANCEL_BUTTON ) {

		if( areaBuff != NULL ) {
			MemFree(areaBuff, (2*5)+20);
			areaBuff = NULL;
		}
		if( planePtr != NULL ) {
			FreeRaster(planePtr, xRad+17+2+9, yRad+1+2+9);
			planePtr = NULL;
		}
#ifdef DRAW_TYPE_ICONS
		FreeMiniIcons();
#endif
		ForceRead();
		FreeBitMapBuffers();
		if( editStats.CaptureFH != NULL ) {
			CloseCaptureFile();
		}
		DeleteMenu(cmdWindow, EDIT_MENU);	/* Not in the Toolbox (yet) */
		decimalLabel = options.PrefsOpts.DecimalLabel;
		RemoveWindow(cmdWindow);
	}
	return(success);
}

/*
 * Prompts user to save changed data in block.
 */
 
UWORD PromptForChangedBlock(BOOL closing)
{
	register UWORD response = OK_BUTTON;
	register RequestPtr req;
	
	if( editStats.BlockBufferChanged ) {
		BeginWait();
		if( (req = DoGetRequest(REQ_SAVEBLOCKWARN)) == NULL ) {
			Error(ERR_NO_MEM);
		} else {
			requester = req;
			dialogVarStr = closing ? strClosing : strMoving;
			ParseDialogString(strsErrors[ERR_SAVE_BLOCK_WARN], strBuff);
			SetGadgetItemText(req->ReqGadget, SKIP_BUTTON+1, cmdWindow, req, strBuff);
			OutlineOKButton(cmdWindow);
			ErrBeep();
			FlushMainPort();
			while( (response = ModalRequest(mainMsgPort, cmdWindow, DialogFilter)) == -1 );
			DestroyRequest(req);
			if( response == OK_BUTTON ) {
				if( !PutEditBlock() ) {
					response = CANCEL_BUTTON;
				}
			}
		}
		EndWait();
		editStats.BlockBufferChanged = response == CANCEL_BUTTON;
	}
	return(response);
}

/*
 *	Refresh the window's contents
 */

void RefreshEditWindow(register BOOL adjust)
{
	Rectangle winRect, rect, myRect, editRect;
	register WindowPtr window = cmdWindow;
	register RastPtr rPort = window->RPort;
	register GadgetPtr gadgList = currGadgList;
	register BOOL inval = FALSE;
	register WORD temp;
	GadgetPtr gadget;
	BOOL newOS = intuiVersion >= OSVERSION_2_0;
	
	if( !WINDOW_ZOOMED(window) ) {
		SetDrMd(rPort, JAM1);
		GetWindowRect(window, &winRect);
/*
	Clear the background of the window if first time, or pre DOS2.0 and simple refresh.
*/
		if( adjust || (!_tbSmartWindows) ) {
			if( !newOS ) {
				SetAPen(rPort, _tbPenLight);
				FillRect(rPort, &winRect);
				inval = TRUE;
			}
		}
		GetGadgetRect(GadgetItem(currGadgList, EDIT_SCROLL_LIST), window, NULL, &editRect);
		editRect.MaxY = (window->Height - window->BorderBottom) - 1;
		if( adjust ) {
			GetGadgetRect(GadgetItem(currGadgList, EDIT_BLOCK_LABEL), cmdWindow, NULL, &labelRect);
			UpdateHScrollBar(GadgetItem(gadgList, EDIT_LEFT_ARROW));
			temp = (HEIGHT(&editRect) & ~7) >> 3;
			CursorOff();
			yoffset = MIN(CalcMaxHeight(), yoffset + temp) - temp;
			UpdateVScrollBar(GadgetItem(gadgList, EDIT_UP_ARROW), FALSE);
			inval = TRUE;
		}
		if( inval ) {
			RefreshGadgets(gadgList, window, NULL);
			InvalRect(window, &winRect);
		}
		BeginRefresh(window);
/*
	If new window size or not a smart refresh window, we have to draw ourselves.
*/
		if( adjust || (!_tbSmartWindows) ) {
			SetAPen(rPort, _tbPenLight);
			gadget = GadgetItem(gadgList, EDIT_VERT_SCROLL);
			GetGadgetRect(gadget, window, NULL, &rect);
			myRect = rect;
			InsetRect(&myRect, -1, -1);
			FillRect(rPort, &myRect);
			RefreshGList(gadget, window, NULL, 1);
		
			rect.MaxY++;
			DrawShadowBox(rPort, &rect, newOS ? -2 : -1, TRUE);
			GetGadgetRect(GadgetItem(gadgList, EDIT_HORIZ_SCROLL), window, NULL, &rect);
			DrawShadowBox(rPort, &rect, newOS ? -2 : -1, TRUE);
			rect = editRect;
			editRect.MinY += 130;
			editRect.MinX = window->BorderLeft;
			FillRect(rPort, &editRect);
			DrawShadowBox(rPort, &rect, 0, TRUE);
			SetAPen(rPort, _tbPenBlack);
			temp = rect.MinY - 2;
			Move(rPort, window->BorderLeft, temp);
			Draw(rPort, window->Width - window->BorderRight, temp);
			RefreshEditLabel();
			SetTypeLabel();
			RefreshEditBlock(TRUE);
			RefreshCursor();
		}
	} else {
		BeginRefresh(window);
	}
	EndRefresh(window, TRUE);
}

/*
 * Gets the block data of current block "xoffset", refreshes screen accordingly.
 * Note: You should call MotorOff() sometime after this routine!
 * It takes a word parameter, "skipRefresh", which should normally be FALSE (0).
 * If non-zero, it will not refresh the window contents or label of the block.
 * If negative, it will not refresh the menus/buttons either, but if -1 is passed and
 * block is read in successfully, it will act normally, as though zero was passed.
 */

void GetEditBlock(register WORD skipRefresh)
{
	if( currVolParams.IsTD525 || IsDiskChanged() ) {
		CmdClear();
	}
	if( editStats.BlockBufferValid = (ReadBlock(xoffset, blockBuffer, FALSE) == 0) ) {
		if( skipRefresh == -1 ) {
			skipRefresh = 0;
		}
	}
	if( skipRefresh >= 0 ) {
		EnableGadgetItem(currGadgList, EDIT_BUTTON_WRITE, cmdWindow, NULL, editStats.BlockBufferValid);
		EnableGadgetItem(currGadgList, EDIT_BUTTON_JUMP, cmdWindow, NULL, editStats.BlockBufferValid && (editStats.CursorMode != 0) );
	}
	if( skipRefresh == 0 ) {
		if( !editStats.BlockBufferValid ) {
			CursorOff();
			editStats.CursorMode = 0;
		}
		RefreshEditLabel();
		SetTypeLabel();
		RefreshEditBlock(FALSE);
		RefreshCursor();
	}
	if( !editStats.BlockBufferValid ) {
/*
	If not a valid key, we've already put up an error message, so don't bother again.
*/
		if( xoffset <= currVolParams.HighestKey ) {
			ReadError(xoffset);
		}
	} else {
		editStats.BlockBufferChanged = FALSE;
	}
}		

/*
 * Display Read Error requester
 */
 
static void ReadError(ULONG block)
{
	TextChar numBuff[15];

	MotorOff();
	OutputStatHexOrDecString(block, numBuff, decimalLabel);
	dialogVarStr = numBuff;
	(void) WarningDialog(strsErrors[ERR_READ_BLOCK], OK_BUTTON);
}

/*
 * Write block back to disk.
 */
 
BOOL PutEditBlock()
{
	BOOL success = FALSE;
	
	if( editStats.BlockBufferValid ) {
		if( !IsChecksumableBlock((ULONG *)blockBuffer) ) {
			success = WriteEditBlock();
		} else {
			if( (CalcBlockChecksum((ULONG *)blockBuffer) == 0L) || options.EditOpts.AutoChecksum || (DoDialog(strsErrors[ERR_FIX_CHECKSUM], CANCEL_BUTTON, _strYes, _strNo, NULL, FALSE) == OK_BUTTON) ) {
				DoCalcChecksum();
			}
			success = WriteEditBlock();
		}
	}
	return(success);
}
	
/*
 * Output the current block back to disk.
 */
 
static BOOL WriteEditBlock()
{
	BOOL success = FALSE;
	register BYTE err = 0;
	TextChar numBuff[15];
	
/*
	Check for no disk or write-protection first.
*/
	if( (!currVolParams.IsTD525) && IsDiskChanged() ) {
		err = WarningDialog(strsErrors[ERR_WRITE_DIFF_DISK], CANCEL_BUTTON);
	}
	if( !err && PromptForWriteProtected() ) {
		err = WriteBlock(xoffset, blockBuffer, FALSE);
		MotorOff();
		if( err ) {
			OutputStatHexOrDecString(xoffset, numBuff, decimalLabel);
			dialogVarStr = numBuff;
			(void) WarningDialog(strsErrors[ERR_WRITE_BLOCK], OK_BUTTON);
		}
		if( success = err == 0 ) {
			editStats.BlockBufferChanged = FALSE;
			SetTypeLabel();		/* In case user changed block type on us */
		}
	}
	abortFlag = FALSE;			/* If PromptForWriteProtect() fails, we CAN retry */
	return(success);
}
		
/*
 * Refresh the disk block label field.
 */
 
void RefreshEditLabel()
{
	TextChar numBuff[16];
	register TextPtr bufPtr = numBuff;
	
	WriteEditHeader(bufPtr);
	bufPtr[9] = 0;				/* Just in case garbage */
	PrintRightStr(cmdWindow->RPort, &labelRect, bufPtr);
}

/*
 * Refresh the disk block window data.
 * If "refreshOffset" TRUE, also refresh the label at left of the data.
 */
 
void RefreshEditBlock(BOOL refreshOffset)
{
	register RastPtr rPort = cmdWindow->RPort;
	register ULONG *ptr;
	register WORD offset;
	register WORD i;
	register WORD x,y;
	UWORD numLines;
	Rectangle rect, eraseRect;
	TextChar lineBuff[40];
	WORD startLine;
	
	if( !WINDOW_ZOOMED(cmdWindow) ) {
		refreshOffset |= !editStats.BlockBufferValid;
		numLines = NumEditLines(&rect);
		y = rect.MinY + 1;
		x = rect.MinX + 288;
		if( !editStats.BlockBufferValid ) {
			eraseRect = rect;
			InsetRect(&eraseRect, 1, 1);
			eraseRect.MaxX = x-1;
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &eraseRect);
			eraseRect.MinX = x+1;
			eraseRect.MaxX = rect.MaxX - 1;
			FillRect(rPort, &eraseRect);
		}
		SetAPen(rPort, _tbPenBlack);
		Move(rPort, x, y + 2);
		Draw(rPort, x, y + (rect.MaxY - rect.MinY)-6);
		x = rect.MinX - 28;
		SetFont(rPort, smallFont);
		SetBPen(rPort, _tbPenLight);
		SetDrMd(rPort, JAM2);
		startLine = yoffset;
		offset = yoffset << 4;
		ptr = (ULONG *) &blockBuffer[offset];
		for( i = startLine ; i < startLine+numLines ; i++ ) {
			y += smallFont->tf_YSize;
			if( refreshOffset ) {
				Move(rPort, x, y);
				NumTo3DigitHexString(offset, lineBuff);
				Text(rPort, lineBuff, 3);
				}
			if( editStats.BlockBufferValid ) {
				Move(rPort, rect.MinX+4, y);
				LongToHexString( *ptr++, &lineBuff[0]);
				LongToHexString( *ptr++, &lineBuff[9]);
				LongToHexString( *ptr++, &lineBuff[18]);
				LongToHexString( *ptr++, &lineBuff[27]);
					Text(rPort, lineBuff, 35);
				Move(rPort, rect.MinX + 2+LINE_X_OFFSET, y);
				WriteASCIIGroup16((UBYTE *)(ptr-4), lineBuff);
				Text(rPort, lineBuff, 16);
			}
			offset += 16;
		}
		SetFont(rPort, screen->RastPort.Font);
		SetDrMd(rPort, JAM1);
	}
}	

/*
 * Redraw the mini-icon that tells us whether this is a deleted or active file/dir.
 */
 
static void RefreshMiniIcon()
{
	register RastPtr rPort = cmdWindow->RPort;
	register BOOL isFree;
#ifdef DRAW_TYPE_ICONS
	register WORD width;
	WORD height, depth;
	register ImagePtr icon;
	APTR mask;
	BitMap bitMap;
	register WORD index;
	register ULONG blockType;
#endif
	BOOL valid;

/*
	Minimize the time between FillRect() and the re-drawing.
*/	
	if( valid = (!currVolParams.NonFSDisk) && bitMapFlag ) {
		isFree = GetKeyStatus(xoffset);
	} else {
		SetAPen(rPort, _tbPenLight);
		FillRect(rPort, &statusRect);
	}
	
	if( valid ) {

#ifdef DRAW_TYPE_ICONS
		width = iconList[DRAWER_ICON]->Width;
		blockType = *(((ULONG *)blockBuffer) + currVolParams.BlockSizeL - 1);
		if( ( *((ULONG *)blockBuffer) == T_SHORT ) &&
			( (blockType == ST_USERDIR) || (blockType == ST_FILE) ) ) {
			if( blockType == ST_USERDIR ) {
				index = isFree ? DRAWER_ICON_GRAY : DRAWER_ICON;
				mask = iconList[DRAWER_ICON_MASK]->ImageData;
			} else {
				index = isFree ? DOC_ICON_GRAY : DOC_ICON;
				mask = iconList[DOC_ICON_MASK]->ImageData;
			}
			icon = iconList[index];
			
			height = icon->Height;	
			depth = screen->RastPort.BitMap->Depth;
	
			InitBitMap(&bitMap, depth, width, height);
			for( index = 0; index < depth; index++) {
				bitMap.Planes[index] = (PLANEPTR) icon->ImageData + index*RASSIZE(width, height);
			}
			BltMaskBitMapRastPort(&bitMap, 0, 0, rPort, statusRect.MinX, statusRect.MinY,
				width, height, 0xE0, mask);
		} else {
#endif
			if( intuiVersion < OSVERSION_2_0 ) {
				SetAPen(rPort, _tbPenBlack);
				FillBusyLightCircle(rPort, statusRect.MinX, statusRect.MinY);
				xRad-=1;
				yRad-=1;
				SetAPen(rPort, isFree ? _tbPenLight : _tbPenDark);
				FillBusyLightCircle(rPort, statusRect.MinX+1, statusRect.MinY+1);
				xRad+=1;
				yRad+=1;
			} else {
				SetAPen(rPort, isFree ? _tbPenLight : _tbPenDark);
				SetOPen(rPort, _tbPenBlack);
				FillBusyLightCircle(rPort, statusRect.MinX, statusRect.MinY);
				BNDRYOFF(rPort);
			}
#ifdef DRAW_TYPE_ICONS
		}
#endif
	}
	SetAPen(rPort, _tbPenBlack);
}

/*
 * Fill the busy light ellipse, and since xRad and yRad are the same, it's a circle.
 */

static void FillBusyLightCircle(register RastPtr rPort, WORD x, WORD y)
{
	struct AreaInfo areaInfo;
	struct TmpRas tmpRas;
	
	if( areaBuff != NULL ) {
		if( planePtr != NULL ) {
			InitArea(&areaInfo, areaBuff, 2+3);
			InitTmpRas(&tmpRas, planePtr, rasSize);
			rPort->AreaInfo = &areaInfo;
			rPort->TmpRas = &tmpRas;
			AreaEllipse(rPort, x + xRad, y + yRad, xRad, yRad);
			AreaEnd(rPort);
			WaitBlit();
			rPort->AreaInfo = NULL;
			rPort->TmpRas = NULL;
		}
	}
}

/*
 * Refresh only the current cursor line.
 * Call this if changing the block contents in a small way.
 */
 
static void RefreshCurrentLine()
{
	register RastPtr rPort = cmdWindow->RPort;
	register ULONG *ptr;
	register UWORD x,y;
	Rectangle rect;
	TextChar lineBuff[40];
	register UWORD temp;
	
	if( (editStats.BlockBufferValid) && (!WINDOW_ZOOMED(cmdWindow)) ) {
		(void) NumEditLines(&rect);
		x = rect.MinX+4;
		y = rect.MinY + 1;
		SetAPen(rPort, _tbPenBlack);
		SetFont(rPort, smallFont);
		SetBPen(rPort, _tbPenLight);
		SetDrMd(rPort, JAM2);
		temp = editStats.CursorOffset >> 5;
		y += smallFont->tf_YSize * (1+(temp - yoffset));
		ptr = (ULONG *) (&blockBuffer[temp << 4]);
		Move(rPort, x, y);
		LongToHexString( *ptr++, &lineBuff[0]);
		LongToHexString( *ptr++, &lineBuff[9]);
		LongToHexString( *ptr++, &lineBuff[18]);
		LongToHexString( *ptr++, &lineBuff[27]);
		Text(rPort, lineBuff, 35);
		Move(rPort, x + (LINE_X_OFFSET-2), y);
		WriteASCIIGroup16((UBYTE *)(ptr-4), lineBuff);
		Text(rPort, lineBuff, 16);
		SetFont(rPort, screen->RastPort.Font);
		SetDrMd(rPort, JAM1);
	}
}

/*
 * For equivalent functionality that uses only text, call this.
 * (Used for saving/printing block).
 */
 
void WriteEditHeader(TextPtr destPtr)
{
	OutputHexOrDecString(xoffset, destPtr, decimalLabel, TRUE, TRUE);
}

/*
 * For equivalent functionality that uses only text, call this.
 * (Used for saving/printing block).
 */
 
void WriteEditLine(WORD line, register TextPtr destPtr)
{
	register ULONG *bufPtr = (ULONG *)(blockBuffer + (line << 4));

	NumTo3DigitHexString(line << 4, destPtr);
	destPtr += 3;
	*destPtr++ = ':';
	LongToHexString(*bufPtr++, destPtr);
	destPtr += 9;
	LongToHexString(*bufPtr++, destPtr);
	destPtr += 9;
	LongToHexString(*bufPtr++, destPtr);
	destPtr += 9;
	LongToHexString(*bufPtr++, destPtr);
	destPtr += 9;
	WriteASCIIGroup16((UBYTE *)(bufPtr - 4), destPtr);
	destPtr[16] = '\0';
}
	
/*
 * Write a group of 16 bytes as ASCII characters, substituting the
 * standard period character for control characters.
 */

static void WriteASCIIGroup16(register UBYTE *ptr, register UBYTE *destPtr)
{
	register WORD n;
	
	for( n = 16; n != 0; n-- ) {
		*destPtr++ = ((*ptr & 127) < 32) ? '.' : *ptr;
		ptr++;
	}
}

/*
 * Return the "scroll-list" rectangle and number of lines you can make w/ topaz/8.
 */
 
static UWORD NumEditLines(register RectPtr rect)
{
	register WindowPtr window = cmdWindow;
	
	GetGadgetRect(GadgetItem(currGadgList, EDIT_SCROLL_LIST), window, NULL, rect);
	if( WINDOW_ZOOMED(window) ) {
		rect->MaxY = rect->MinY;
	} else {
		rect->MaxY = (window->Height - window->BorderBottom) - 1;
	}
	return( (HEIGHT(rect) & ~7) >> 3 );
}

/*
 * Recalculate the X,Y for the cursor
 */
 
static void RecalcCursor(ULONG newOffset)
{
	CursorOff();
	editStats.CursorOffset = newOffset;
	editStats.CursorLX = editStats.CursorOffset & 0x1F;
	editStats.CursorLY = editStats.CursorOffset >> 5;
}

#define TOPAZ_CHARWIDTH		8
#define TOPAZ_CHARHEIGHT	8

/*
 * Internal routine which inverts the cursor rectangle
 */

static void InvertCursor(register RectPtr rect)
{
	register RastPtr rPort = cmdWindow->RPort;
	register WORD cursX = editStats.CursorLX;
	
	if( editStats.CursorMode == CMODE_HEX ) {
		rect->MinX += 2+((cursX + (cursX >> 3)) << 3);
		rect->MaxY--;
	} else {
		cursX >>= 1;
		rect->MinX += LINE_X_OFFSET + (cursX << 3);
	}
	SetDrMd(rPort, COMPLEMENT);
	SetWrMsk(rPort, _tbPenWhite ^ _tbPenBlack);
	RectFill(rPort, rect->MinX, rect->MinY+1, rect->MinX+(TOPAZ_CHARWIDTH-1), rect->MaxY);
	WaitBlit();
	SetWrMsk(rPort, 0xFF);
	SetDrMd(rPort, JAM1);
}
	
/*
 * Refresh the cursor if it's on
 */
 
void RefreshCursor()
{
	Rectangle rect, boxRect;
	register UWORD numLines;
	register WindowPtr window = cmdWindow;
	
	if( editStats.BlockBufferValid && (editStats.CursorMode != 0) && (!WINDOW_ZOOMED(window)) ) {
		
		SetFieldLabel();
		
		numLines = NumEditLines(&rect);
		
		if( (editStats.CursorLY >= yoffset) && (editStats.CursorLY < (yoffset+numLines)) ) {
			EnableGadgetItem(currGadgList, EDIT_BUTTON_JUMP, window, NULL, TRUE);
			
			InsetRect(&rect, 2, 2);
			boxRect = rect;
		
			rect.MinX += 1+((editStats.CursorLX >> 3) * (9*TOPAZ_CHARWIDTH));
			rect.MinY += (editStats.CursorLY-yoffset) << 3;
			rect.MaxX = rect.MinX + (1+(TOPAZ_CHARWIDTH*8));
			rect.MaxY = rect.MinY + TOPAZ_CHARHEIGHT;
			SetAPen(window->RPort, _tbPenBlack);
			FrameRect(window->RPort, &rect);
			if( !editStats.CursorOn ) {
				rect.MinX = boxRect.MinX;
				InvertCursor(&rect);
				editStats.CursorOn = TRUE;
			}
		}
	}
}

/*
 * Hilights the cursor and its block box.
 */
 
void CursorOn()
{
	if( !editStats.CursorOn ) {
		RefreshCursor();
	}
}

/*
 * Sets the block type for this block
 */
 
static void SetTypeLabel()
{
	register TextPtr textPtr = NULL;
	register ULONG id;
	register ULONG secType;
/*	register UWORD len;*/
	register UWORD strLen;
	register RastPtr rPort;
	Rectangle rect;
	
	if( editStats.BlockBufferValid ) {
		if( xoffset < currVolParams.LowestKey ) {
			textPtr = strBoot;
		} else if( xoffset > currVolParams.HighestKey ) {
			textPtr = strReserved;
		} else if( xoffset == currVolParams.RootBlock ) {
			textPtr = strRoot;
		} else if( IsBitMapExtKey(xoffset) ) {
			textPtr = strBitMapExt;
		} else if( IsBitMapKey(xoffset) ) {
			textPtr = strBitMap;
		} else {
			id = *((ULONG *)blockBuffer);
			if( id == T_LIST ) {
				textPtr = strFileExt;
			} else if( id == T_SHORT ) {
				secType = *((ULONG *)blockBuffer + currVolParams.BlockSizeL - 1);
				if( secType == ST_FILE ) {
					textPtr = strFileHdr;
				} else if( secType == ST_USERDIR ) {
					textPtr = strDrawerType;
				} else if( (secType == ST_LINKFILE) || (secType == ST_LINKDIR) ) {
					textPtr = strHardLink;
				} else if( secType == ST_SOFTLINK ) {
					textPtr = strSoftLink;
				} else {
					textPtr = currVolParams.FFSFlag ? strData : strUnknown;
				}
			} else if( id == T_DATA ) {
				textPtr = strData;
			} else if( currVolParams.DirCache && ((id < 256) && (id & T_DIRCACHE)) ) {
				textPtr = strCacheType;
			} else {
				textPtr = currVolParams.FFSFlag ? strData : strUnknown;
			}
		}
	}
#ifdef RIGHT_JUSTIFY
	rPort = cmdWindow->RPort;
	GetGadgetRect(GadgetItem(currGadgList, EDIT_TYPE_TEXT+2), cmdWindow, NULL, &rect);
	len = TextLength(rPort, textPtr, MIN(9, strLen = strlen(textPtr)));
	InsetRect(&rect, 2, 1);
	SetAPen(rPort, _tbPenLight);
	FillRect(rPort, &rect);
	SetAPen(rPort, _tbPenBlack);
	SetDrMd(rPort, JAM1);
	Move(rPort, rect.MaxX - len, rect.MinY + charHeight);
	Text(rPort, textPtr, strLen);
#else
	/*
	GetGadgetItemText(currGadgList, EDIT_TYPE_TEXT, strBuff);
	if( (textPtr == NULL) || strcmp(strBuff, textPtr) ) {
		SetGadgetItemText(currGadgList, EDIT_TYPE_TEXT, cmdWindow, NULL, textPtr);
	}
	*/
	rPort = cmdWindow->RPort;
	GetGadgetRect(GadgetItem(currGadgList, EDIT_TYPE_TEXT+2), cmdWindow, NULL, &rect);
	InsetRect(&rect, 2, 1);
	SetAPen(rPort, _tbPenLight);
	SetDrMd(rPort, JAM1);
	Move(rPort, rect.MinX, rect.MinY + 2 + charBaseline);/* Do now to reduce flicker */
	FillRect(rPort, &rect);
	if( textPtr != NULL ) {
		strLen = strlen(textPtr);
		SetAPen(rPort, _tbPenBlack);
		TextInWidth(rPort, textPtr, strLen, rect.MaxX - rect.MinX, FALSE);
	}
#endif
	RefreshMiniIcon();
}

/*
 * Sets the field label for the cursor
 */
 
static void SetFieldLabel()
{
	register TextPtr textPtr = strReserved; 
	register ULONG field = editStats.CursorOffset >> 3;	/* nth longword in block */
	register ULONG currBlock = xoffset;
	register ULONG endIndex = currVolParams.HashTableSize+6;
	register ULONG secType;
	ULONG id;
	
	if( !(currVolParams.NonFSDisk || currVolParams.DosDisk) ) {
		textPtr = strUnknown;
	} else if( IsBitMapExtKey(xoffset) ) {
		textPtr = field == (currVolParams.BlockSizeL - 1) ? strBitMapExtKey : strBitMapKey;
	} else if( currBlock == currVolParams.RootBlock ) {
		if( field < 6 ) {
			textPtr = strRootBlockTypes[field];
		} else if( field < endIndex ) {
			textPtr = strHashTable;
		} else if( field == endIndex ) {
			textPtr = strBitMapValid;
		} else if( field < (endIndex + 26) ) {
			textPtr = strBitMapKey;
		} else switch( field - (endIndex+26) ) {
			case 0:
				textPtr = strBitMapExtKey;
				break;
			case 1:
			case 2:
			case 3:
				textPtr = strRootModDate;
			case 12:
			case 13:
				break;
			case 14:
			case 15:
			case 16:
				textPtr = strVolModDate;
				break;
			case 17:
			case 18:
			case 19:
				textPtr = strVolCrDate;
			case 20:
			case 21:
				break;
			case 22:
				if( currVolParams.DirCache ) {
					textPtr = strCacheKey;
				}
				break;
			case 23:
				textPtr = strSecType;
				break;
			default:
				textPtr = strVolumeName;
				break;
		}
	} else if( (currBlock < currVolParams.LowestKey) || (currBlock > currVolParams.HighestKey) ) {
		if( (currBlock == 0) && (field == 0) ) {
			textPtr = strFSID;
		}
	} else if( IsBitMapKey(currBlock) ) {
		textPtr = field ? strBitMapData : strChecksum;
	} else {
		id = *((ULONG *)blockBuffer);
		if( currVolParams.FFSFlag && 
			((id != T_SHORT) && (id != T_LIST) && ((!currVolParams.DirCache) || (id > 255) || ((id & T_DIRCACHE) == 0))) ) {
			textPtr = strFileData;
		} else {
			if( id == T_SHORT ) {
				secType = *((ULONG *)blockBuffer+currVolParams.BlockSizeL-1);
				if( field < 6 ) {
					if( secType == ST_FILE ) {
						textPtr = strFileBlockTypes[field];
					} else {
						textPtr = strDirAndLinkBlockTypes[field];
					}
				} else if( field < endIndex ) {
					if( secType == ST_SOFTLINK ) {
						textPtr = strSoftLinkObj;
					} else if( (secType != ST_LINKDIR) && (secType != ST_LINKFILE) ) {
						textPtr = (secType == ST_USERDIR) ? strHashTable : strKeyTable;
					}
				} else {
					if( field < (endIndex + 27) ) {
						if( secType == ST_SOFTLINK ) {
							textPtr = strSoftLinkObj;
						} else {
							if( field == (endIndex+1) ) {
								textPtr = strOwnerXID;
							} else if( field == (endIndex+2) ) {
								textPtr = strProt;
							} else if( (field == (endIndex+3)) && (secType == ST_FILE) ) {
								textPtr = strFileSize;
							} else if( field > (endIndex+3) ) {
								textPtr = strComment;
							}
						}
					} else {
						switch( field - (endIndex + 27) ) {
						case 0:
						case 1:
						case 2:
							textPtr = strModDate;
							break;
						case 11:
							break;
						case 12:
							if( (secType == ST_LINKDIR) || (secType == ST_LINKFILE) ) {
								textPtr = strLinkToObject;
							}
							break;
						case 13:
							textPtr = strPrevLink;
							break;
						case 14:
						case 15:
						case 16:
						case 17:
						case 18:
							break;
						case 19:
							textPtr = strHashChain;
							break;
						case 20:
							textPtr = strParentKey;
							break;
						case 21:
							if( secType == ST_USERDIR ) {
								if( currVolParams.DirCache ) {
									textPtr = strCacheKey;
								}
							} else {
								textPtr = strExtKey;
							}
							break;
						case 22:
							textPtr = strSecType;
							break;
						default:
							if( secType == ST_FILE ) {
								textPtr = strFileName;
							} else if( secType == ST_USERDIR ) {
								textPtr = strDirName;
							} else {
								textPtr = strLinkName;
							}
							break;
						}
					}
				}
			} else if( id == T_LIST ) {
				if( field < 6 ) {
					textPtr = strExtBlockTypes[field];
				} else if( field < endIndex ) {
					textPtr = strKeyTable;
				} else if( field == (endIndex + 47) ) {
					textPtr = strParentKey;
				} else if( field == (endIndex + 48) ) {
					textPtr = strExtKey;
				} else if( field == (endIndex + 49) ) {
					textPtr = strSecType;
				}	/* Otherwise, it's one of the reserved fields */
			} else if( id == T_DATA ) {
				if( field < 6 ) {
					textPtr = strOFSDataBlockTypes[field];
				} else {
					textPtr = strFileData;
				}
			} else if( (id < 256) && (id & 0x20) ) {
				if( field < 6 ) {
					textPtr = strCacheBlockTypes[field];
				} else {
					textPtr = strDirCacheInfo;
				}
			} else {
				textPtr = strUnknown;
			}
		}	
	}

/*
	This extra code just eliminates all the unnecessary flicker.
*/
	GetGadgetItemText(currGadgList, EDIT_FIELD_TEXT, strBuff);
	if( strcmp(strBuff, textPtr) ) {
		SetGadgetItemText(currGadgList, EDIT_FIELD_TEXT, cmdWindow, NULL, textPtr);
	}
}


/*
 * Reset cursor to default condition (off, at top left).
 */
 
void ResetCursor()
{
	CursorOff();
	EnableGadgetItem(currGadgList, EDIT_BUTTON_JUMP, cmdWindow, NULL, FALSE);
	editStats.CursorOffset = 0;
	editStats.CursorLX = editStats.CursorLY = 0;
	editStats.CursorMode = 0;
	/*
	if( yoffset ) {
		yoffset = 0;
		UpdateVScrollBar(GadgetItem(currGadgList, EDIT_UP_ARROW), TRUE);
	}
	*/
	SetGadgetItemText(currGadgList, EDIT_FIELD_TEXT, cmdWindow, NULL, NULL);
}

/*
 * Erases the cursor and its block box.
 */
 
void CursorOff()
{
	RastPtr rPort = cmdWindow->RPort;
	Rectangle rect, boxRect;
	
	if( editStats.CursorOn ) {
		editStats.CursorOn = FALSE;
		SetAPen(rPort, _tbPenLight);
		GetGadgetRect(GadgetItem(currGadgList, EDIT_SCROLL_LIST), cmdWindow, NULL, &rect);
		InsetRect(&rect, 2, 2);
		boxRect = rect;
		
		rect.MinX += 1+((editStats.CursorLX >> 3) * (9*TOPAZ_CHARWIDTH));
		rect.MinY += (editStats.CursorLY-yoffset) << 3;
		rect.MaxX = rect.MinX + (1+(TOPAZ_CHARWIDTH*8));
		rect.MaxY = rect.MinY + TOPAZ_CHARHEIGHT;
		FrameRect(rPort, &rect);
		rect.MinX = boxRect.MinX;
		InvertCursor(&rect);

		SetAPen(rPort, _tbPenBlack);
	}
}

/*
 * Handle mouse click in scroll list, return TRUE if in list, FALSE if outside.
 */
 
static BOOL ClickInEdit(PointPtr pt, ULONG seconds, ULONG micros)
{
	Rectangle rect;
	register UWORD numLines;
	register UWORD temp;
	BOOL inRect;
	static ULONG prevSecs, prevMicros;
/*
	Constrain Y-coordinates to actual bounding box.
*/
	if( inRect = editStats.BlockBufferValid ) {
		numLines = NumEditLines(&rect);
		if( inRect = PtInRect(pt, &rect) ) {
			temp = pt->y - rect.MinY;
			if( temp < 2 ) {
				temp = 0;
			} else {
				temp = (temp - 2) >> 3;
				if( temp >= numLines ) {
					temp = numLines-1;
				}
			}
			CursorOff();
			editStats.CursorLY = temp + yoffset;
			temp = pt->x - rect.MinX;
			if( temp < (LINE_X_OFFSET-2) ) {
				if( temp < 8 ) {
					temp = 0;
				} else {
					temp -= 5;				/* Subtract eight but add three for correction */
					temp = MIN(31, (temp >> 3) - (temp / (9*TOPAZ_CHARWIDTH)));
				}
				editStats.CursorMode = CMODE_HEX;
				editStats.CursorLX = temp;
			} else {
				editStats.CursorMode = CMODE_ASCII;
				if( temp < (LINE_X_OFFSET+2) ) {
					temp = 0;
				} else {
					temp -= (LINE_X_OFFSET+2);
					temp = MIN(30<<2, temp);
				}
				editStats.CursorLX = temp >> 2;
			}
			temp = editStats.CursorOffset;
			editStats.CursorOffset = (editStats.CursorLY << 5) + editStats.CursorLX;
			
			if( (editStats.CursorMode == CMODE_HEX) && 
				DoubleClick(prevSecs, prevMicros, seconds, micros) &&
				( (temp/* & ~15*/) == (editStats.CursorOffset/*& ~15*/) ) ) {
				prevSecs = 0;
				DepressGadget(GadgetItem(currGadgList, EDIT_BUTTON_JUMP), cmdWindow, NULL);
				GoToNewBlock(FALSE);
				SetFieldLabel();
			} else {
				prevSecs = seconds;
				prevMicros = micros;
			}
			CursorOn();
		}
	}
	return(inRect);
}

/*
 * Utility routine to bump the cursor position
 */
 
static void AdjustCursorPos(register UWORD adjust)
{
	register UWORD numLines;
	Rectangle rect;
	register UWORD temp = adjust >> 5;
	
	CursorOff();
	if( temp < yoffset ) {
		yoffset += (temp - yoffset);
		UpdateVScrollBar(GadgetItem(currGadgList, EDIT_UP_ARROW), TRUE);
	} else {
		numLines = NumEditLines(&rect);
		if( temp >= (yoffset+numLines) ) {
			yoffset += (temp - (yoffset+(numLines-1)));
			UpdateVScrollBar(GadgetItem(currGadgList, EDIT_UP_ARROW), TRUE);
		}
	}
	RefreshCurrentLine();
	RecalcCursor(adjust);
	CursorOn();
}

/*
 *	Handle scrollList's functions
 */

static BOOL EditListFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	ULONG class = intuiMsg->Class;
	WORD gadgNum;
	register UWORD code;
	register UWORD temp;
	GadgetPtr gadget;
	UWORD modifier, oldMode;
	register WindowPtr window = cmdWindow;
	Point pt;
	register BOOL valid;
	BOOL isNotRepeat;
	register BOOL zoomed = WINDOW_ZOOMED(window);
	BOOL ignoreCmdKeys;
	BOOL dialogAppeared;
	
	if( (requester == NULL) && (intuiMsg->IDCMPWindow == window) ) {
		code = intuiMsg->Code;
		modifier = intuiMsg->Qualifier;
		isNotRepeat = (modifier & IEQUALIFIER_REPEAT) == 0;
		oldMode = editStats.CursorMode;
		switch (class) {
		case INTUITICKS:
			if( !CheckForDiskChange() ) {
				ReplyMsg((MsgPtr)intuiMsg);
				ReturnToMain();
				return(TRUE);
			}
			break;
		case GADGETDOWN:
		case GADGETUP:
			switch( gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress) ) {
			case EDIT_VERT_SCROLL:
			case EDIT_UP_ARROW:
			case EDIT_DOWN_ARROW:
				HandleVertScroll(mainMsgPort, intuiMsg, GadgetItem(currGadgList, EDIT_UP_ARROW));
				*item = gadgNum;
				return(TRUE);
			case EDIT_HORIZ_SCROLL:
			case EDIT_LEFT_ARROW:
			case EDIT_RIGHT_ARROW:
				dialogAppeared = editStats.BlockBufferChanged;
				temp = PromptForChangedBlock(FALSE);
				if( temp != CANCEL_BUTTON ) {
					if( dialogAppeared ) {
						ReplyMsg((MsgPtr)intuiMsg);
						switch(gadgNum) {
						case EDIT_HORIZ_SCROLL:
							AdjustToHorizScroll(TRUE, TRUE);
							break;
						case EDIT_LEFT_ARROW:
							ScrollLeft(window, 0);
							break;
						case EDIT_RIGHT_ARROW:
							ScrollRight(window, 0);
							break;
						}
					} else {
						HandleHorizScroll(mainMsgPort, intuiMsg, GadgetItem(currGadgList, EDIT_LEFT_ARROW));
					}
				}
				*item = gadgNum;
				return(TRUE);
			case EDIT_SCROLL_LIST:
				if( class == GADGETDOWN ) {
					pt.x = intuiMsg->MouseX;
					pt.y = intuiMsg->MouseY;
					ClickInEdit(&pt, intuiMsg->Seconds, intuiMsg->Micros);
				}
			}
			break;
/*
	Handle cursor keys
*/
		case MOUSEBUTTONS:
			if( code == SELECTDOWN ) {
				pt.x = intuiMsg->MouseX;
				pt.y = intuiMsg->MouseY;
				if( !ClickInEdit(&pt, intuiMsg->Seconds, intuiMsg->Micros) ) {
					ResetCursor();
				}
			}
			break;
		case RAWKEY:
			ReplyMsg((MsgPtr) intuiMsg);
			switch( code ) {
			case CURSORUP:
			case CURSORDOWN:
				if( !zoomed ) {
					if( editStats.CursorMode ) {
						temp = editStats.CursorOffset;
						if( code == CURSORUP ) {
							if( valid = temp >= 32 ) {
								temp -= 32;
							}
						} else {
							if( valid = (temp >> 1) < (currVolParams.BlockSize - 16) ) {
								temp += 32;
							}
						} 
						if( valid ) {
							AdjustCursorPos(temp);
						} /*else {
							if( isNotRepeat ) {	
								ErrBeep();
							}
						} */
					} else {
						if( code == CURSORUP ) {
							DepressGadget(GadgetItem(currGadgList, EDIT_UP_ARROW), window, NULL);
							ScrollUp(window, 0);
						} else {
							DepressGadget(GadgetItem(currGadgList, EDIT_DOWN_ARROW), window, NULL);
							ScrollDown(window, 0);
						}
					}
				}
				break;
			case CURSORLEFT:
			case CURSORRIGHT:
				if( !zoomed ) {
					if( editStats.CursorMode ) {
						temp = editStats.CursorMode == CMODE_HEX ? 1 : 2;
						if( code == CURSORLEFT ) {
							if( valid = editStats.CursorOffset >= temp ) {
								temp = editStats.CursorOffset - temp;
							}
						} else {
							if( valid = ((editStats.CursorOffset+temp) >> 1) < currVolParams.BlockSize ) {
								temp += editStats.CursorOffset;
							}
						}
						if( valid ) {
							AdjustCursorPos(temp);
						}/* else {
							if( isNotRepeat ) {
								ErrBeep();
							}
						} */ 	
					} else {
						if( code == CURSORLEFT ) {
							if( xoffset ) {
								DepressGadget( GadgetItem(currGadgList, EDIT_LEFT_ARROW), window, NULL);
								if( PromptForChangedBlock(FALSE) != CANCEL_BUTTON ) {
									ScrollLeft(window, 0);
								}
							}
						} else {
							if( xoffset < (maxWidth-1) ) {
								DepressGadget( GadgetItem(currGadgList, EDIT_RIGHT_ARROW), window, NULL);
								if( PromptForChangedBlock(FALSE) != CANCEL_BUTTON ) {
									ScrollRight(window, 0);
								}
							}
						}
					}
				}
				break;
			case REV_TAB_RAW:
				if( !zoomed ) {
					switch(editStats.CursorMode) {
					case CMODE_HEX:
						if( editStats.CursorOffset > 7 ) {
							temp = (editStats.CursorOffset | 7) - 15;
							AdjustCursorPos(temp);
						}
						break;
					case CMODE_NONE:
						if( editStats.BlockBufferValid ) {
							editStats.CursorMode = CMODE_ASCII;
							CursorOn();
						}
						break;
					default:
						break;
					}
				}
				break;
			default:
				DoRawKey(window, code, modifier);
				break;
			}
			return(TRUE);
			break;
		case VANILLAKEY:
			if( !(ignoreCmdKeys = zoomed) ) {
				temp = toUpper[code];
				if( !editStats.CursorMode ) {
					switch(temp) {
					case CMD_KEY_DEC:
					case CMD_KEY_HEX:
						temp = temp == strDec[0];
						gadget = GadgetItem(currGadgList, EDIT_POPUP_BASE);
						if( GetGadgetValue(gadget) != temp ) {
							SetGadgetValue(gadget, window, NULL, temp);
							decimalLabel = temp;
							RefreshEditLabel();
						}
						break;
					case TAB:
					case CR:
						if( editStats.BlockBufferValid ) {
							editStats.CursorMode++;
							if( modifier & SHIFTKEYS ) {
								editStats.CursorMode++;
							}
							AdjustCursorPos(editStats.CursorOffset);
						}
						break;
					default:
						break;
					}
				} else if( editStats.CursorMode == CMODE_HEX ) {
					if( ((temp >= '0') && (temp <= '9')) || ((temp >= 'A') && (temp <= 'F')) ) {
						if( temp < 'A') {
							temp -= '0';
						} else {
							temp -= ('A'- 10);
						}
						if( editStats.CursorOffset & 1 ) {
							blockBuffer[editStats.CursorOffset >> 1] &= 0xF0;
						} else {
							temp <<= 4;
							blockBuffer[editStats.CursorOffset >> 1] &= 0x0F;
						}
						editStats.BlockBufferChanged = TRUE;
						blockBuffer[editStats.CursorOffset >> 1] |= temp;
						if( valid = ((editStats.CursorOffset+1) >> 1) < currVolParams.BlockSize ) {
							AdjustCursorPos(editStats.CursorOffset+1);
						} else {
							RefreshCurrentLine();
							editStats.CursorOn = FALSE;
							CursorOn();
						}
					} else if( code == CR ) {
						if( editStats.BlockBufferValid ) {
							CursorOff();
							if( modifier & SHIFTKEYS ) {
								editStats.CursorMode = 0;
							} else {
								editStats.CursorMode++;
								CursorOn();
							}
						}
					} else if( code == TAB ) {
						if( (editStats.CursorOffset >> 1) >= (currVolParams.BlockSize - 4) ) {
							/*
							if( isNotRepeat ) {
								ErrBeep();
							}*/
						} else {
							temp = (editStats.CursorOffset | 7) + 1;
							AdjustCursorPos(temp);
						}
					} else {
						if( (temp != CMD_KEY_GO) && (temp != ESC) /*&& (temp != CMD_KEY_REREAD) && (temp != CMD_KEY_WRITE)*/ ) {
							if( isNotRepeat ) {
								ErrBeep();
							}
						}
					}
				} else {
					if( (code == CR) && (modifier & IEQUALIFIER_NUMERICPAD) ) {
						if( modifier & SHIFTKEYS ) {
							editStats.CursorMode--;
							AdjustCursorPos(editStats.CursorOffset);
						} else {
							CursorOff();
							editStats.CursorMode = 0;
							SetGadgetItemText(currGadgList, EDIT_FIELD_TEXT, window, NULL, NULL);
						}
					} else {
						editStats.BlockBufferChanged = TRUE;
						blockBuffer[editStats.CursorOffset >> 1] = code;
						if( valid = ((editStats.CursorOffset+2) >> 1) < currVolParams.BlockSize ) {
							AdjustCursorPos(editStats.CursorOffset+2);
						} else {
							RefreshCurrentLine();
							editStats.CursorOn = FALSE;
							CursorOn();
						}
					}
					ignoreCmdKeys = (temp == ESC) || (temp == CMD_KEY_GO) /*|| (temp == CMD_KEY_REREAD) || (temp == CMD_KEY_WRITE)*/;
				}
			}
			if( ignoreCmdKeys ) {
				ReplyMsg((MsgPtr) intuiMsg);
				return(TRUE);
			}
			break;
		}
		if( oldMode != editStats.CursorMode ) {
			EnableGadgetItem(currGadgList, EDIT_BUTTON_JUMP, window, NULL, editStats.BlockBufferValid && (editStats.CursorMode != 0) );
		}
	}
	return (FALSE);
}

/*
 *	Handle editor window button hits
 */

BOOL DoEditDialogMsg(register IntuiMsgPtr intuiMsg)
{
	WORD item;
	DialogPtr dlg;
	register WORD temp;
	
	if (EditListFilter(intuiMsg, &item)) {
		return TRUE;
	}
	if (DialogSelect(intuiMsg, &dlg, &item)) {
		ReplyMsg((MsgPtr)intuiMsg);
		switch(item) {
		case DLG_CLOSE_BOX:
			closeFlag = TRUE;
			ChangeToolMode(MODE_TOOLS);
			break;
		case EDIT_POPUP_BASE:
			temp = GetGadgetValue(GadgetItem(currGadgList, item));
			if( temp != decimalLabel ) {
				decimalLabel = temp;
				RefreshEditLabel();
			}
			break;
		case EDIT_BUTTON_REREAD:
			ResetCursor();
			GetEditBlock(FALSE);
			MotorOff();
			break;
		case EDIT_BUTTON_WRITE:
			(void) PutEditBlock();
			break;
		case EDIT_BUTTON_JUMP:
			if( editStats.BlockBufferValid ) {
				GoToNewBlock(TRUE);
			}
			break;
		}
		return TRUE;
	}

	ReplyMsg((MsgPtr)intuiMsg);
	return FALSE;
}

/*
 * Given a new block at the cursor offset, go to it.
 */
 
static void GoToNewBlock(BOOL resetCursor)
{
	ULONG oldXOffset;
	BOOL wasGoodKey;
	BOOL oldValid;
	
	if( PromptForChangedBlock(FALSE) != CANCEL_BUTTON ) {
		oldXOffset = xoffset;
		oldValid = editStats.BlockBufferValid;
		xoffset = *((ULONG *)(blockBuffer + ((editStats.CursorOffset & ~7) >> 1)));
		if( resetCursor ) {
			ResetCursor();
		} else {
			CursorOff();
		}
		GetEditBlock(-1);
		if( !editStats.BlockBufferValid ) {
			wasGoodKey = xoffset <= currVolParams.HighestKey;
			xoffset = oldXOffset;
			if( (editStats.BlockBufferValid = oldValid) && wasGoodKey ) {
				GetEditBlock(FALSE);
			}
		} else {
			editStats.HistoryBlock = oldXOffset;
			UpdateHScrollBar(GadgetItem(currGadgList, EDIT_LEFT_ARROW));
		}
		SetEditMenu();
		MotorOff();
	}
}
			
/*
 * Get the hash value of a file/dir/link/whatever name.
 */
 
static BOOL DoCalcHash()
{
	register WORD item;
	register BOOL success;
	register RequestPtr req;
	register WindowPtr window;
	GadgetPtr gadgList;
	register BOOL done;
	
	if( editStats.BlockBufferValid && (!currVolParams.NonFSDisk) && !IsBitMapKey(xoffset) && (*((ULONG *)blockBuffer) == T_SHORT) ) {
		BeginWait();
		if( success = ((req = DoGetRequest(REQ_HASH)) != NULL) ) {
			requester = req;
			window = cmdWindow;
			gadgList = req->ReqGadget;
			OutlineOKButton(window);
			done = FALSE;
			do {
				WaitPort(mainMsgPort);
				item = CheckRequest(mainMsgPort, window, DialogFilter);
				GetEditItemText(gadgList, HASH_TEXT, &strBuff[1]);
				switch(item) {
				case -1:						/* INTUITICKS message */
					EnableGadgetItem(gadgList, OK_BUTTON, window, req, strBuff[1] != 0);
					break;
				case CANCEL_BUTTON:
					success = FALSE;
				case OK_BUTTON:
					done = TRUE;
					break;
				default:
					break;
				}
			} while( !done );
			DestroyRequest(req);
			if( success ) {
				strBuff[0] = strlen(&strBuff[1]);
				CursorOff();
				editStats.CursorMode = CMODE_HEX;
				RecalcCursor((CalcHash(strBuff) + sizeof(RootBlock)) << 1);
				CursorOn();
			}
		}
		EndWait();
	}
	return(success);
}

/*
 * Calculate the checksum and stick it in the appropriate spot.
 * Returns FALSE if block not in memory, or is a bitmap extension block.
 */
 
static BOOL DoCalcChecksum()
{
	register BOOL success;
	register ULONG checksum;
	register BOOL isBitMapKey = IsBitMapKey(xoffset);
	
	if( success = editStats.BlockBufferValid && 
		(!currVolParams.NonFSDisk) && IsChecksumableBlock((ULONG *)blockBuffer) ) {
		if( checksum = CalcBlockChecksum((ULONG *)blockBuffer) ) {
			if( isBitMapKey ) {
				*((ULONG *)blockBuffer) -= checksum;
			} else {
				((FileHdrPtr)blockBuffer)->CkSum -= checksum;
			}
			CursorOff();
			RefreshEditBlock(FALSE);
			CursorOn();
		}
	}
	return(success);
}

/*
 * Create the capture file.
 */
 
static BOOL CreateCaptureFile()
{
	BOOL success = FALSE;
	SFReply sfReply;
	
	SFPPutFile(screen, mainMsgPort, strSaveCapture, strDefCapture,
		DialogFilter, NULL, &_sfpPutFileDlgTempl, &sfReply);
	if( sfReply.Result == SFP_OK && (destFileName = AllocMem(PATHSIZE, MEMF_ANY)) ) {
		BuildPath(sfReply.Name, destFileName, sfReply.DirLock, PATHSIZE);
		UnLock(sfReply.DirLock);
		if( (editStats.CaptureFH = Open(sfReply.Name, MODE_NEWFILE)) != NULL ) {
			SetEditMenu();
			success = TRUE;
		} else {
			DOSError(ERR_SAVE, IoErr());
		}
	}
	return(success);
}

/*
 * Write to the capture file.
 */
 
static BOOL WriteToCaptureFile()
{
	register ULONG len;
	register BOOL success = FALSE;
	
	if( editStats.BlockBufferValid ) {
		if( currVolParams.VolValid ) {
			len = currVolParams.Info.id_BytesPerBlock;
		} else {	
			len = (currVolParams.FFSFlag || (!currVolParams.DosDisk)) ? currVolParams.BlockSize : currVolParams.BlockSize - sizeof(OldDataBlk);
		}
		if( !(success = Write(editStats.CaptureFH, blockBuffer+currVolParams.BlockSize-len, len) == len) ) {
			DOSError(ERR_CAPTURE_WRITE, IoErr());
		}
	}
	return(success);
}

/*
 * Close the capture file.
 */
 
static BOOL CloseCaptureFile()
{
	if( editStats.CaptureFH ) {
		Close(editStats.CaptureFH);
		editStats.CaptureFH = NULL;
		SaveIcon(destFileName, ICON_CAPTURE);
		FreeMem(destFileName, PATHSIZE);
		destFileName = NULL;
	}
	SetEditMenu();
	return(TRUE);
}


/*
 *	Find request filter
 */

static BOOL FindRequestFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;
		
	class = intuiMsg->Class;
	if( intuiMsg->IDCMPWindow == cmdWindow && (class == GADGETDOWN || class == GADGETUP ) ) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if( class == GADGETDOWN ) {
			if( itemHit >= FIND_START_TEXT && itemHit <= FIND_END_TEXT ) {
				ReplyMsg((MsgPtr) intuiMsg);
				*item = FIND_FROM_RADBTN;
				return (TRUE);
			}
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 * Invoke the "Find" dialog, with all of its options.
 */
 
static BOOL DoFind()
{
	register WORD item;
	register BOOL success;
	register BOOL done;
	register RequestPtr req;
	register WindowPtr window;
	GadgetPtr gadget, gadgList;
	WORD findType;
	BOOL findDir, findRange, findWarn;
	register ULONG num1;
	register ULONG num2;
	TextChar oldASCIIBuf[100];
	TextChar oldHexBuf[100];
	
	BeginWait();
	reqList[REQ_FIND]->Gadgets[FIND_POPUP].Value = findType = editStats.FindType;
	AutoActivateEnable(FALSE);
	req = DoGetRequest(REQ_FIND);
	AutoActivateEnable(TRUE);
	if( success = (req != NULL) ) {
		requester = req;
		window = cmdWindow;
		gadgList = req->ReqGadget;
		OutlineOKButton(window);
		gadget = GadgetItem(gadgList, FIND_START_TEXT);
		num1 = RemoveGList(window, gadget, 2);
		if( decimalLabel ) {
			gadget->Activation |= GACT_LONGINT;
			gadget->NextGadget->Activation |= GACT_LONGINT;
		}
		AddGList(window, gadget, num1, 2, req);
		strcpy(oldASCIIBuf, editStats.ASCIIString);
		strcpy(oldHexBuf, editStats.HexString);
		
		StuffFindTextBuffer(editStats.FindType, strBuff);
		SetEditItemText(gadgList, FIND_TEXT, window, req, strBuff);
		EnableGadgetItem(gadgList, FIND_TEXT, window, req, findType < CURR_KEY_ITEM);
		if( findType < CURR_KEY_ITEM ) {
			ActivateGadget(GadgetItem(gadgList, FIND_TEXT), window, req);
		}
		
		if( editStats.FindDirection ) {
			num2 = editStats.FindStartBlock;
			num1 = editStats.FindEndBlock;
		} else {
			num1 = editStats.FindStartBlock;
			num2 = editStats.FindEndBlock;
		}
		OutputEditHexOrDecString(num1, strBuff, decimalLabel);
		SetEditItemText(gadgList, FIND_START_TEXT, window, req, strBuff);
		OutputEditHexOrDecString(num2, strBuff, decimalLabel);
		SetEditItemText(gadgList, FIND_END_TEXT, window, req, strBuff);
		findDir = editStats.FindDirection;
		findRange = editStats.FindRange;
		GadgetOn(window, FIND_FORWARD_RADBTN + findDir);
		GadgetOn(window, FIND_ALL_RADBTN + findRange);
		if( findWarn = editStats.FindWarn ) {
			GadgetOn(window, FIND_WARN_BOX);
		}
		done = FALSE;
		do {
			WaitPort(mainMsgPort);
			item = CheckRequest(mainMsgPort, window, FindRequestFilter);
			GetEditItemText(gadgList, FIND_TEXT, strBuff);
			switch(item) {
			case -1:						/* INTUITICKS message */
				EnableGadgetItem(gadgList, OK_BUTTON, window, req, strBuff[0] != 0);
				break;
			case CANCEL_BUTTON:
				success = FALSE;
				done = TRUE;
				strcpy(editStats.ASCIIString, oldASCIIBuf);
				strcpy(editStats.HexString, oldHexBuf);
				break;
			case OK_BUTTON:
				done = TRUE;
				editStats.FindDirection = findDir;
				if( findType == ASCII_ITEM ) {
					strcpy(editStats.FindString, strBuff);
					strcpy(editStats.ASCIIString, strBuff);
					editStats.FindLen = strlen(editStats.FindString);
				} else {
					if( findType == HEX_ITEM ) {
						strcpy(editStats.HexString, strBuff);
					}
					editStats.FindLen = ConvertHexToBinStr(strBuff, editStats.FindString);
				}
				num1 = 0;
				num2 = currVolParams.MaxBlocks-1;
				if( editStats.FindRange = findRange ) {
					GetEditItemText(gadgList, FIND_START_TEXT, strBuff);
					num1 = EditStringToNum(strBuff, decimalLabel);
					GetEditItemText(gadgList, FIND_END_TEXT, strBuff);
					num2 = EditStringToNum(strBuff, decimalLabel);
					if( (num1 >= currVolParams.MaxBlocks) || (num2 >= currVolParams.MaxBlocks) ) {
						Error(ERR_OUT_OF_RANGE);
						done = FALSE;
					}
				}
				if( findDir ) {
					editStats.FindStartBlock = num2;
					editStats.FindEndBlock = num1;
					editStats.FindEndOffset = 0;
				} else {
					editStats.FindStartBlock = num1;
					editStats.FindEndBlock = num2;
					editStats.FindEndOffset = currVolParams.BlockSize-1;
				}
				break;
			case FIND_POPUP:
				findType = GetGadgetValue(GadgetItem(gadgList, item));
				if( !StuffFindTextBuffer(findType, strBuff) ) {
					ErrBeep();
				} else {
					SetEditItemText(gadgList, FIND_TEXT, window, req, strBuff);
					EnableGadgetItem(gadgList, FIND_TEXT, window, req, findType < CURR_KEY_ITEM);
					if( findType < CURR_KEY_ITEM ) {
						ActivateGadget(GadgetItem(gadgList, FIND_TEXT), window, req);
					}
				}
				break;
			case FIND_FORWARD_RADBTN:
			case FIND_REVERSE_RADBTN:
				GadgetOff(window, findDir + FIND_FORWARD_RADBTN);
				GadgetOn(window, item);
				findDir = item - FIND_FORWARD_RADBTN;
				break;
			case FIND_ALL_RADBTN:
			case FIND_FROM_RADBTN:
				GadgetOff(window, findRange + FIND_ALL_RADBTN);
				GadgetOn(window, item);
				findRange = item - FIND_ALL_RADBTN;
				break;
			case FIND_WARN_BOX:
				ToggleCheckbox(&findWarn, item, window);
				break;
			default:
				break;
			}
		} while( !done );
		DestroyRequest(req);
		if( success ) {
			editStats.FindType = findType;
			editStats.FindWarn = findWarn;
			editStats.FindCurrBlock = editStats.FindStartBlock;
			editStats.FindStartOffset = editStats.CursorMode ? editStats.CursorOffset >> 1 : 0;
			editStats.FindCurrOffset = editStats.FindStartOffset;
			BumpFindCounters();
			success = DoFindNext();
		}
	}
	EndWait();
	return(success);
}

/*
 * Given a hexadecimal string, convert to binary and return the length of the
 * new binary string stuffed in "binStr".
 */
 
static UWORD ConvertHexToBinStr(TextPtr hexStr, register TextPtr binStr)
{
	register TextChar ch;
	register WORD len = 0;
	register TextPtr ptr;
	register WORD upperNibble;
	register BOOL secondNibble = FALSE;
	
	ptr = hexStr;
	while( ch = toUpper[*ptr++] ) {
		if( (ch >= '0') && (ch <= '9') ) {
			len++;
		} else if( (ch >= 'A') && (ch <= 'F') ) {
			len++;
		}
	}
	if( len & 1 ) {
		upperNibble = 0;
		secondNibble = TRUE;
		len++;
	}
	ptr = hexStr;
	while( ch = toUpper[*ptr++] ) {
		if( (ch >= '0') && (ch <= '9') ) {
			ch -= '0';
			if( secondNibble ) {
				*binStr++ = (upperNibble << 4) + ch;
			} else {
				upperNibble = ch;
			}
			secondNibble = !secondNibble;
		} else if( (ch >= 'A') && (ch <= 'F') ) {
			ch -= ('A' - 10);
			if( secondNibble ) {
				*binStr++ = (upperNibble << 4) + ch;
			} else {
				upperNibble = ch;
			}
			secondNibble = !secondNibble;
		}
	}
	return(len >> 1);
}

/*
 * Bump the current counter past the cursor
 */
 
static void BumpFindCounters()
{
	if( editStats.FindDirection ) {
		if( editStats.FindCurrOffset == 0 ) {
			editStats.FindCurrOffset = currVolParams.BlockSize;
			if( editStats.FindCurrBlock == 0 ) {
				editStats.FindCurrBlock = currVolParams.MaxBlocks;
			}
			editStats.FindCurrBlock--;
		}
		editStats.FindCurrOffset--;
	} else {
		editStats.FindCurrOffset++;
		if( editStats.FindCurrOffset == currVolParams.BlockSize ) {
			editStats.FindCurrOffset = 0;
			editStats.FindCurrBlock++;
			if( editStats.FindCurrBlock == currVolParams.MaxBlocks ) {
				editStats.FindCurrBlock = 0;
			}
		}
	}
}

/*
 * Depending on the type of item, stuff the find buffer with appropriate contents.
 */
 
static BOOL StuffFindTextBuffer(register WORD item, register TextPtr buff)
{
	register BOOL success = TRUE;
	register ULONG num;
	
	if( item == ASCII_ITEM ) {
		strcpy(buff, editStats.ASCIIString);
	} else if( item == HEX_ITEM ) {
		strcpy(buff, editStats.HexString);
	} else {
		switch(item) {
		case CURR_KEY_ITEM:
			num = xoffset;
			break;
		case PARENT_ITEM:
			num = ((RootBlockEndPtr)(blockBuffer + currVolParams.BlockSize - sizeof(RootBlockEnd)))->Parent;
			success = editStats.BlockBufferValid;
			break;
		case HEADER_ITEM:
			num = ((OldDataBlkPtr)blockBuffer)->Hdrkey;
			success = editStats.BlockBufferValid;
			break;
		}
		if( success ) {
			OutputHexOrDecString(num, buff, FALSE, TRUE, TRUE);
		} else {
			*buff = '\0';
		}
	}
	return(success);
}

/*
 * Update the find block counter.
 */
 
static void UpdateFindBlock(ULONG *lastTick)
{
	struct DateStamp currDateStamp;
	register ULONG ticks;
	
	DateStamp(&currDateStamp);
	ticks = (currDateStamp.ds_Minute * 3000) + currDateStamp.ds_Tick;
	if( (ticks - (*lastTick)) > 1 ) {
		*lastTick = ticks;
		RefreshEditLabel();
		UpdateHScrollBar(GadgetItem(currGadgList, EDIT_LEFT_ARROW));
	}
}

/*
 * Find the next occurrence of the current string. If none found, return FALSE.
 */
 
static BOOL DoFindNext()
{
	RequestPtr req;
	BOOL done = FALSE;
	register BOOL found = FALSE;
	register WORD item = -1;
	register TextPtr ptr, endPtr;
	register WORD len = editStats.FindLen;
	register WORD incr;
	TextPtr startPtr;
	WORD startOffset;
	WORD endOffset;
	BOOL altRead = FALSE;
	register ULONG temp;
	UBYTE *altBuf = AllocBuffer();
	ULONG saveXOffset = xoffset;
	BOOL displayed;
	ULONG lastTick;
	
	if( (len != 0) && (altBuf != NULL) && (PromptForChangedBlock(FALSE) != CANCEL_BUTTON) ) {
		editStats.HistoryBlock = xoffset;
		lastTick = 0;
		BeginWait();
		req = GetRequest(reqList[REQ_FINDING], cmdWindow, FALSE);
		displayed = FALSE;
		found = FALSE;
		ptr = &blockBuffer[editStats.FindCurrOffset];
		if( editStats.FindDirection ) {
			incr = -1;
			startOffset = currVolParams.BlockSize-1;
			endOffset = -1;
		} else {
			incr = 1;
			startOffset = 0;
			endOffset = currVolParams.BlockSize;
		}
		startPtr = &blockBuffer[startOffset];
		endPtr = &blockBuffer[endOffset];
		do {
			if( req != NULL ) {
				item = CheckRequest(mainMsgPort, cmdWindow, DialogFilter);
			}
			switch(item) {
			case -1:
				xoffset = editStats.FindCurrBlock;
/*
	If we've already pre-read the next buffer, use it. Otherwise read it here.
*/
				if( altRead ) {
					CopyMemQuick(altBuf, blockBuffer, currVolParams.BlockSize);
					altRead = FALSE;
					UpdateFindBlock(&lastTick);
				} else {
					ReadBlock(xoffset, blockBuffer, TRUE);
					UpdateFindBlock(&lastTick);
					editStats.BlockBufferValid = WaitBlock() == 0;
					if( editStats.FindWarn ) {
						if( !editStats.BlockBufferValid ) {
							ReadError(xoffset);
						}
					}
				}
				if( editStats.BlockBufferValid ) {
					while( ptr != endPtr ) {
						if( incr > 0 ) {
							len = MIN(editStats.FindLen, endOffset - editStats.FindCurrOffset);
							found = FastCmpString(editStats.FindString, ptr, len) == 0;
						} else {
							len = MIN(editStats.FindLen, editStats.FindCurrOffset - endOffset);
							found = FastCmpString(&editStats.FindString[editStats.FindLen-len], ptr-(len-1), len) == 0;
						}
						if( found ) {
/*
	Did we find the search string entirely in this block?
*/
							if( len == editStats.FindLen ) {	/* Yes, done. */
								break;
							} else {
								temp = xoffset + incr;		/* Nope, get next */
								if( temp == currVolParams.MaxBlocks ) {
									temp = 0;
								} else if( temp == 0xFFFFFFFF ) {
									temp = currVolParams.MaxBlocks-1;
								}
/*
	Try to read the next block to continue searching for the other portion of the
	word in the adjacent block. In any event, don't re-read this block anymore.
*/
								if( (!altRead) && ReadBlock(temp, altBuf, FALSE) != 0 ) {
									ReadError(temp);
									editStats.FindCurrBlock = temp;	/* Skip past block */
									found = FALSE;
									ptr = endPtr - incr;		/* To end of block */
								} else {
									altRead = TRUE;			/* Don't re-read */
									temp = len;
									len = editStats.FindLen - len;
									if( incr > 0 ) {
										if( found = FastCmpString(&editStats.FindString[temp], altBuf, len) == 0 ) {
											break;
										}
									} else {
										if( found = FastCmpString(editStats.FindString, &altBuf[currVolParams.BlockSize-len], len) == 0 ) {
											xoffset += incr;
											ptr = startPtr;
											CopyMemQuick(altBuf, blockBuffer, currVolParams.BlockSize);
											break;
										}
									}
								}
							}
						}
						ptr += incr;
						editStats.FindCurrOffset += incr;
						if( editStats.FindCurrOffset == editStats.FindEndOffset ) {
							if( editStats.FindCurrBlock == editStats.FindEndBlock ) {
								done = TRUE;
								break;
							}
						}
					}
				} else {
					done = editStats.FindCurrBlock == editStats.FindEndBlock;
				}
				if( found ) {
					RecalcCursor( ( ((incr > 0 ) ? ((ULONG)ptr) : (((ULONG)ptr)-(len-1))) - ((ULONG)blockBuffer) ) << 1 );
					editStats.CursorMode = CMODE_ASCII;
				}
				if( (!found) || altRead ) {
					editStats.FindCurrOffset = startOffset;
					editStats.FindCurrBlock += incr;
					ptr = startPtr;
					if( (!displayed) && (!done) ) {
						displayed = TRUE;
						ShowRequest(req, cmdWindow);
					}
				}
				if( editStats.FindCurrBlock == currVolParams.MaxBlocks ) {
					editStats.FindCurrBlock = 0;
				} else if( editStats.FindCurrBlock == 0xFFFFFFFF ) {
					editStats.FindCurrBlock = currVolParams.MaxBlocks-1;
				}
				break;
			default:
			case CANCEL_BUTTON:
				done = TRUE;
				break;
			}
		} while( (!found) && (!done) );
		DestroyRequest(req);					/* Can handle NULL */
		EndWait();
		FreeBuffer(&altBuf);

		MotorOff();
		
		RefreshEditLabel();
		SetTypeLabel();
		SetFieldLabel();
		UpdateHScrollBar(GadgetItem(currGadgList, EDIT_LEFT_ARROW));
		if( saveXOffset != xoffset ) {
			if( !found ) {
				ResetCursor();
			}
			RefreshEditBlock(FALSE);		/* Redraw if moved to a different block. */
		}
		if( !found ) {
			if( item != CANCEL_BUTTON ) {
				Error(ERR_FIND_FAIL);		/* If "Cancel" hit, we know already! */
			}
			editStats.FindCurrOffset = editStats.FindEndOffset;
			editStats.FindCurrBlock = editStats.FindEndBlock;
		}
		RefreshCursor();
		SetEditMenu();
	}
	return(found);
}

/*
 * Same as CmpString(), except takes only one length, and assumes case-insensitive.
 * Also, to shave a few more cycles, the result is merely 0 for same, 1 for not-same.
 */
 
static BOOL FastCmpString(register TextPtr s1, register TextPtr s2, register WORD len)
{
#if SWEDISH
	register TextChar ch1, ch2;
	
	while( len-- ) {
		ch1 = *s1++;
		ch2 = *s2++;
		if (ch1 == (UBYTE) '�')
			ch1 = (UBYTE) '�';
		else if (ch1 == (UBYTE) '�')
			ch1 = (UBYTE) '�';
		if (ch2 == (UBYTE) '�')
			ch2 = (UBYTE) '�';
		else if (ch2 == (UBYTE) '�')
			ch2 = (UBYTE) '�';
		if( ch1 != ch2 ) {
			return(TRUE);
		}
	}
#else
	while( len-- ) {
		if( *s1++ != *s2++ ) {
			return(TRUE);
		}
	}
#endif
	return(FALSE);
}
	
/*
 * Handle the "Go To..." command, which queries for a block to go to from user.
 */
 
static BOOL DoGoTo()
{
	register WORD item = 0;
	register BOOL success;
	BOOL done;
	register RequestPtr req;
	register WindowPtr window;
	GadgetPtr gadgList, gadget;
	register BOOL localDecLabel;
	ULONG num;
	
	BeginWait();
	reqList[REQ_GOTO]->Gadgets[GOTO_POPUP].Value = localDecLabel = decimalLabel;
	AutoActivateEnable(FALSE);
	req = DoGetRequest(REQ_GOTO);
	AutoActivateEnable(TRUE);
	if( success = (req != NULL) ) {
		requester = req;
		window = cmdWindow;
		gadgList = req->ReqGadget;
		OutlineOKButton(window);
		num = editStats.LastGoToBlock;
		if( num >= currVolParams.MaxBlocks ) {
			num = xoffset;
		}
		OutputEditHexOrDecString(num, strBuff, localDecLabel);
		SetEditItemText(gadgList, GOTO_TEXT, window, req, strBuff);
		done = FALSE;
		do {
			if( item != -1 ) {
				gadget = GadgetItem(gadgList, GOTO_TEXT);
				num = RemoveGadget(window, gadget);
				if( localDecLabel ) {
					gadget->Activation |= GACT_LONGINT;
				} else {
					gadget->Activation &= ~GACT_LONGINT;
				}
				AddGList(window, gadget, num, 1, req);
				ActivateGadget(gadget, window, req);
			}
			WaitPort(mainMsgPort);
			item = CheckRequest(mainMsgPort, window, DialogFilter);
			GetEditItemText(gadgList, GOTO_TEXT, strBuff);
			switch(item) {
			case -1:						/* INTUITICKS message */
				EnableGadgetItem(gadgList, OK_BUTTON, window, req, strBuff[0] != 0);
				break;
			case CANCEL_BUTTON:
				success = FALSE;
				done = TRUE;
				break;
			case OK_BUTTON:
				num = localDecLabel ? StringToNum(strBuff) : HexStringToNum(strBuff);
				if( num >= currVolParams.MaxBlocks ) {
					Error(ERR_OUT_OF_RANGE);
				} else {
					done = TRUE;
					if( success = PromptForChangedBlock(FALSE) != CANCEL_BUTTON ) {
						editStats.HistoryBlock = xoffset;
						xoffset = editStats.LastGoToBlock = num;
					}
				}
				break;
			case GOTO_POPUP:
				num = EditStringToNum(strBuff, localDecLabel);
				if( num >= currVolParams.MaxBlocks ) {
					num = 0;
				}
				localDecLabel = GetGadgetValue(GadgetItem(gadgList, item));
				OutputEditHexOrDecString(num, strBuff, localDecLabel);
				SetEditItemText(gadgList, GOTO_TEXT, window, req, strBuff);
				break;
			default:
				break;
			}
		} while( !done );
		DestroyRequest(req);
	}
	EndWait();
	return(success);
}

/*
 * Convert an EDIT_TEXT string into a long, using base 10 if "decimal" TRUE.
 */
 
static ULONG EditStringToNum(TextPtr str, BOOL decimal)
{
	return( decimal ? StringToNum(str) : HexStringToNum(str) );
}

/*
 * Like StringToNum(), except for hex numbers.
 */
 
static ULONG HexStringToNum(TextPtr str)
{
	register ULONG num = 0;
	register WORD i;
	register WORD len = strlen(str);
	register UBYTE *ptr = &str[len];
	register TextChar ch;
	register ULONG base = 1;
	
	for( i = 0 ; i < len ; i++ ) {
		ch = toUpper[*--ptr];
		if( (ch >= '0') && (ch <= '9') ) {
			num += (ch - '0') * base;
			base <<= 4;					/* Good digit, next nibble, please */
		} else if( (ch >= 'A') && (ch <= 'F') ) {
			num += (ch - ('A'- 10)) * base;
			base <<= 4;					/* Good digit, next nibble, please */
		} else if( ch != '$' ) {	/* Ignore leading (or any for that matter) '$' */
			num = 0xFFFFFFFF;
			break;					/* Invalid hex chars break out */
		}
	}
	return(num);
}

/*
 *	Handle macro menu
 */

BOOL DoEditMenu(WindowPtr window, UWORD item, UWORD sub)
{
	register BOOL success = FALSE;
	ULONG oldOffset;
	BOOL oldValid;
	register BOOL isBitMapKey;
	register ULONG temp;
	register BOOL validADOS;
	
	switch(item) {
	case FIND_ITEM:
		success = DoFind();
		break;
	case FIND_NEXT_ITEM:
		if( success = editStats.FindLen != 0 ) {
			BumpFindCounters();
			success = DoFindNext();
		}
		break;
	case CALC_HASH_ITEM:
		success = DoCalcHash();
		break;
	case CALC_CKSUM_ITEM:
		success = DoCalcChecksum();
		break;
	case AUTOCALC_CKSUM_ITEM:
		if( success = (!currVolParams.NonFSDisk) ) {
			options.EditOpts.AutoChecksum = !options.EditOpts.AutoChecksum;
		}
		break;
	case CAPTURE_CREATE_ITEM:
		success = CreateCaptureFile();	
		break;
	case CAPTURE_WRITE_ITEM:
		success = WriteToCaptureFile();
		break;
	case CAPTURE_CLOSE_ITEM:
		success = CloseCaptureFile();
		break;
	case GOTO_ITEM:
	case GOTO_PREV_ITEM:
	case GOTO_FIELD_ITEM:
		oldOffset = xoffset;
		oldValid = editStats.BlockBufferValid;
		isBitMapKey = IsBitMapKey(xoffset);
		validADOS = (!currVolParams.NonFSDisk) && editStats.BlockBufferValid;
		if( item == GOTO_ITEM ) {
			success = DoGoTo();
		} else if( item == GOTO_PREV_ITEM ) {
			if( success = editStats.HistoryBlock != 0xFFFFFFFF ) {
				temp = editStats.HistoryBlock;
			}
		} else {
			switch(sub) {
			case GOTO_ROOT_SUBITEM:
				if( success = !currVolParams.NonFSDisk ) {
					temp = currVolParams.RootBlock;
				}
				break;
			case GOTO_BITMAP_SUBITEM:
				if( success = ((editStats.BitMapKey != 0xFFFFFFFF) && CheckKey(editStats.BitMapKey)) ) {
					temp = editStats.BitMapKey;
				}
				success = CheckKey(temp);			/* Just in case...*/
				break;
			case GOTO_HASH_SUBITEM:
				if( success = validADOS && (!isBitMapKey) ) {
					temp = ((RootBlockEndPtr)(blockBuffer + currVolParams.BlockSize - sizeof(RootBlockEnd)))->HashChain;
					success = (temp != 0L) && CheckKey(temp);
				}
				break;
			case GOTO_PARENT_SUBITEM:
				if( success = (editStats.BlockBufferValid) && (!isBitMapKey) ) {
					if( (*((ULONG *)blockBuffer) > 32) && (*((ULONG *)blockBuffer) < 63) ) {
						temp = ((CacheHdrPtr)blockBuffer)->Parent;
					} else {
						temp = ((RootBlockEndPtr)(blockBuffer + currVolParams.BlockSize - sizeof(RootBlockEnd)))->Parent;
					}
					success = (temp != 0L) && CheckKey(temp);
				}
				break;
			case GOTO_EXT_SUBITEM:
				if( success = validADOS ) {
					if( IsBitMapKey(xoffset) ) {
						temp = IsBitMapExtKey(xoffset) ? *(((ULONG *)blockBuffer) + currVolParams.BlockSizeL - 1) : 0;
					} else {
						temp = ((RootBlockEndPtr)(blockBuffer + currVolParams.BlockSize - sizeof(RootBlockEnd)))->Ext;
					}
					success = (temp != 0L) && (CheckKey(temp));
				}
				break;
			case GOTO_HEADER_SUBITEM:
				if( success = validADOS && (!isBitMapKey) && !currVolParams.FFSFlag ) {
					temp = ((OldDataBlkPtr)blockBuffer)->Hdrkey;
					success = (temp != 0L) && CheckKey(temp);
				}
				break;
			case GOTO_DATA_SUBITEM:
				if( success = validADOS && (!isBitMapKey) &&
					( (((OldDataBlkPtr)blockBuffer)->Type == T_SHORT) ||
					  (((OldDataBlkPtr)blockBuffer)->Type == T_DATA) ) ) {
					temp = ((OldDataBlkPtr)blockBuffer)->NextData;
					success = (temp != 0L) && CheckKey(temp);
				}
				break;
			}
		}
		if( success ) {
			if( item != GOTO_ITEM ) {
				if( PromptForChangedBlock(FALSE) == CANCEL_BUTTON ) {
					break;
				}
				xoffset = temp;
			}
			ResetCursor();
			GetEditBlock(-1);			/* No updating whatsoever unless successful */
		}
		if( !editStats.BlockBufferValid ) {
			xoffset = oldOffset;
			if( editStats.BlockBufferValid = oldValid ) {
				GetEditBlock(FALSE);
			}
		} else {
			if( success ) {
				editStats.HistoryBlock = oldOffset;
				ResetCursor();
				UpdateHScrollBar(GadgetItem(currGadgList, EDIT_LEFT_ARROW));
			}
		}
		MotorOff();
		SetEditMenu();
		break;
	default:
		break;
	}
	return(success);
}	

