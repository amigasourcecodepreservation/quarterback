/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	REXX interface routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <clib/alib_protos.h>

#include <string.h>

#include <rexx/storage.h>
#include <rexx/rxslib.h>
#include <rexx/errors.h>

//#include <IFF/WORD.h>

#include <Toolbox/Menu.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/AREXX.h>

#include "Tools.h"
#include "Proto.h"
#include "Version.h"

/*
 *	AREXX definitions and prototypes
 */

typedef struct RexxMsg	RexxMsg, *RexxMsgPtr;

ULONG			InitPort(MsgPortPtr, TextPtr);
void			FreePort(MsgPortPtr);
BOOL			IsRexxMsg(RexxMsgPtr);
RexxMsgPtr	CreateRexxMsg(MsgPortPtr, TextPtr, TextPtr);
BOOL			FillRexxMsg(RexxMsgPtr, ULONG, ULONG);
TextPtr		CreateArgstring(TextPtr, ULONG);
void			ClearRexxMsg(RexxMsgPtr, ULONG);
void			DeleteRexxMsg(RexxMsgPtr);

/*
 *	External variables
 */

extern struct RexxLib	*RexxSysBase;

extern MsgPort		rexxMsgPort;
extern MsgPortPtr	mainMsgPort;

extern ScreenPtr	screen;
extern WindowPtr	backWindow;

extern WindowPtr	cmdWindow;

extern BOOL			inMacro, fromCLI;

extern TextChar	strAREXXPortName[];
extern TextChar	strFKeysName[];
extern TextChar	strAutoExecName[];
extern TextChar	macroNames1[10][32], macroNames2[10][32];

extern FKey			fKeyTable1[], fKeyTable2[];

extern TextChar	strBuff[], macroDrawerName[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */


#define MACRONAME_TEXT	2
#define MACROARG_TEXT	3

#define SAVE_BUTTON	2
#define NAME1_TEXT	3

static TextChar	progPortName[15];

static UWORD		rexxModifiers;		/* Inited to 0 */

static RexxMsgPtr	macroMsg;		/* For macro commands (Inited to NULL) */

/*
 *	Local prototypes
 */

static BOOL	DoOtherMacro(void);
static void SaveFKeys(void);
static BOOL	DoCustomizeMacro(WindowPtr);


ULONG InitPort(MsgPortPtr port, TextPtr name)
{
	ULONG success = FALSE;
	int signal_bit;

	if(port != NULL)
	{
		memset(port,0,sizeof(*port));

		port->mp_Node.ln_Name	= name;
		port->mp_Node.ln_Type	= NT_MSGPORT;
		port->mp_Flags		= PA_IGNORE;

		NewList(&port->mp_MsgList);

		signal_bit = AllocSignal(-1);
		if(signal_bit > 0)
		{
			port->mp_SigBit		= signal_bit;
			port->mp_SigTask	= FindTask(NULL);

			success = TRUE;
		}
	}

	return(success);
}

void FreePort(MsgPortPtr port)
{
	if(port != NULL)
	{
		if(port->mp_SigBit > 0)
			FreeSignal(port->mp_SigBit);
	}
}


/*
 *	Open REXX library and init REXX port
 *	If REXX port already exists, then do not init REXX interface
 */

void InitRexx()
{
	WORD portNum;

/*
	Open REXX
*/
	RexxSysBase = (struct RexxLib *) OpenLibrary("rexxsyslib.library", 0);
	if (RexxSysBase == NULL)
		return;
/*
	Find unused port name and set up port
*/
	portNum = -1;
	do {
		strcpy(progPortName, strAREXXPortName);
		portNum++;				/* Compiler bug, this must go after strcpy() */
		if (portNum > 0) {
			NumToString(portNum, strBuff);
			strcat(progPortName, ".");
			strcat(progPortName, strBuff);
		}
	} while (FindPort(progPortName));
	InitPort(&rexxMsgPort, progPortName);
	AddPort(&rexxMsgPort);
}

/*
 *	Shut down REXX port and close library
 */

void ShutDownRexx()
{
	if (RexxSysBase) {
		RemPort(&rexxMsgPort);
		FreePort(&rexxMsgPort);
		CloseLibrary((struct Library *)RexxSysBase);
	}
}

/*
 * Handle REXX messages
 */
 
void DoRexxMsg(RexxMsgPtr rexxMsg)
{
	if (rexxMsg == macroMsg) {		/* Message reply? */
		if (macroMsg->rm_Result1 != RC_OK)
			Error(ERR_MACRO_FAIL);
		ClearRexxMsg(macroMsg, (macroMsg->rm_Action & 0xFF) + 1);
		DeleteRexxMsg(macroMsg);
		macroMsg = NULL;
		rexxModifiers = 0;
		inMacro = FALSE;
/*		SetAllMenus();*/
		return;
	} else {
/*
	Handle other messages
*/
		if( IsRexxMsg(rexxMsg) ) {
			rexxMsg->rm_Result1 = ProcessCommandText(rexxMsg->rm_Args[0], rexxModifiers, (rexxMsg->rm_Action & RXFF_RESULT) != 0, &rexxMsg->rm_Result2);
			if( rexxMsg->rm_Result2 ) {
				rexxMsg->rm_Result2 = (LONG) CreateArgstring((TextPtr)rexxMsg->rm_Result2, (ULONG)strlen((TextPtr)rexxMsg->rm_Result2) );
			}
		}
		ReplyMsg((MsgPtr) rexxMsg);
	}
}

/*
 *	Initiate AREXX macro
 *	If macroMsg is not NULL then we are already executing a macro, so ignore
 *	This routine will use a copy of macroName
 */

void DoMacro(TextPtr macroName, TextPtr arg)
{
	register UWORD i;
	register UWORD len;
	WORD argCount;
	BOOL success, hasPath;
	MsgPortPtr rexxPort;

	if (RexxSysBase == NULL || macroMsg)
		return;
/*
	Add path to program if no path specified
*/
	hasPath = FALSE;
	len = strlen(macroName);
	for (i = 0; i < len; i++) {
		if (macroName[i] == ':') {
			hasPath = TRUE;
			break;
		}
	}
	if (hasPath) {
		strcpy(strBuff, macroName);
	} else {
		SetPathName(strBuff, macroDrawerName, FALSE);
		AppendDirPath(strBuff, macroName);
	}
/*
	Find REXX port and start macro
*/
	argCount = (arg && strlen(arg)) ? 2 : 1;
	success = FALSE;
	if ((macroMsg = (RexxMsgPtr) CreateRexxMsg(&rexxMsgPort, NULL, progPortName)) == NULL)
		goto Error;
	macroMsg->rm_Action = RXFUNC + argCount - 1;
	if (!fromCLI)
		macroMsg->rm_Action |= (1 << RXFB_NOIO);
	macroMsg->rm_Args[0] = (STRPTR) strBuff;
	macroMsg->rm_Args[1] = (STRPTR) arg;
	if (!FillRexxMsg(macroMsg, argCount, 0))
		goto Error;
	Forbid();
	rexxPort = FindPort("REXX");
	if (rexxPort)
		PutMsg(rexxPort, (MsgPtr) macroMsg);
	Permit();
	if (rexxPort) {
		inMacro = success = TRUE;
	} else
		ClearRexxMsg(macroMsg, argCount);
Error:
	if (!success) {
		if (macroMsg) {
			DeleteRexxMsg(macroMsg);
			macroMsg = NULL;
		}
		Error((rexxPort) ? ERR_NO_MEM : ERR_NO_REXX);
	}
}

/*
 *	Do auto exec macro
 *	If auto exec macro does not exist, don't report an error
 */

void DoAutoExec()
{
	LONG lock;

	if( RexxSysBase != NULL ) {
		SetPathName(strBuff, macroDrawerName, TRUE);
		AppendDirPath(strBuff, strAutoExecName);
		if ((lock = Lock(strBuff, ACCESS_READ)) != NULL) {
			UnLock(lock);
			DoMacro(strAutoExecName, NULL);
		}
	}
}

/*
 *	Get macro name from user and execute
 */

static BOOL DoOtherMacro()
{
	BOOL success;
	TextChar macroName[GADG_MAX_STRING], arg[GADG_MAX_STRING];

/*
	Get macro name from user
*/
	BeginWait();
	success = GetMacroName(screen, mainMsgPort, DialogFilter, macroName, arg);
	EndWait();
	if( success ) {
		DoMacro(macroName, arg);
	}
	return(success);
}

/*
 *	Save FKeys file
 */

static void SaveFKeys()
{
	WORD i, version;
	ULONG fileID;
	BOOL success;
	File file;
	TextChar pathName[200];

	success = FALSE;
	SetPathName(pathName, strFKeysName, TRUE);
	if ((file = Open(pathName, MODE_NEWFILE)) == NULL)
		goto Exit;
/*
	Write fileID and version
*/
	fileID = ID_FKEYSFILE;
	version = PROGRAM_VERSION;
	if (Write(file, &fileID, sizeof(ULONG)) != sizeof(ULONG) ||
		Write(file, &version, sizeof(WORD)) != sizeof(WORD))
		goto Exit;
/*
	Write FKey tables and data
*/
	for (i = 0; i < 10; i++) {
		if (Write(file, &fKeyTable1[i], sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKeyTable1[i].Type == FKEY_MACRO &&
			Write(file, fKeyTable1[i].Data, 32) != 32)
			goto Exit;
	}
	for (i = 0; i < 10; i++) {
		if (Write(file, &fKeyTable2[i], sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKeyTable2[i].Type == FKEY_MACRO &&
			Write(file, fKeyTable2[i].Data, 32) != 32)
			goto Exit;
	}
	success = TRUE;
/*
	All done
*/
Exit:
	if (file)
		Close(file);
	if (success)
		SaveIcon(pathName, ICON_FKEYS);
}

/*
 *	Load FKeys file
 */

void LoadFKeys()
{
	WORD i, version;
	ULONG fileID;
	File file;
	FKey fKey;
	TextChar macroName[32], pathName[200];

	SetPathName(pathName, strFKeysName, TRUE);
	if ((file = Open(pathName, MODE_OLDFILE)) == NULL)
		goto Exit;
/*
	Read fileID and version
*/
	if (Read(file, &fileID, sizeof(ULONG)) != sizeof(ULONG) ||
		Read(file, &version, sizeof(WORD)) != sizeof(WORD) ||
		fileID != ID_FKEYSFILE ||
		!(version == PROGRAM_VERSION))
		goto Exit;
/*
	Write FKey tables and data
*/
	if (version == PROGRAM_VERSION) {
		for (i = 0; i < 10; i++) {
			if (Read(file, &fKey, sizeof(FKey)) != sizeof(FKey))
				goto Exit;
			if (fKey.Type == FKEY_MACRO &&
				Read(file, macroName, 32) != 32)
				goto Exit;
			fKeyTable1[i] = fKey;
			if (fKey.Type == FKEY_MACRO) {
				strncpy(macroNames1[i], macroName, 32);
				fKeyTable1[i].Data = macroNames1[i];
			}
		}
	}
	else {
		if (Seek(file,10*sizeof(FKey),OFFSET_CURRENT) == -1)
			goto Exit;
	}
	for (i = 0; i < 10; i++) {
		if (Read(file, &fKey, sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKey.Type == FKEY_MACRO &&
			Read(file, macroName, 32) != 32)
			goto Exit;
		fKeyTable2[i] = fKey;
		if (fKey.Type == FKEY_MACRO) {
			strncpy(macroNames2[i], macroName, 32);
			fKeyTable2[i].Data = macroNames2[i];
		}
	}
/*
	Set macro menu
*/
	for (i = 0; i < 10; i++)
		SetMenuItem(backWindow, MENUITEM(MACRO_MENU, i, NOSUB), macroNames2[i]);
/*
	All done
*/
Exit:
	if (file)
		Close(file);
}

/*
 *	Get custom macro names and insert in menu
 */

static BOOL DoCustomizeMacro(WindowPtr window)
{
	WORD i, item;
	DialogPtr dlg;
	TextChar newNames[10][32];

/*
	Get macro name from user
*/
	if ((dlg = GetDialog(dlgList[DLG_CUSTOMMACRO], screen, mainMsgPort)) == NULL) {
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(dlg);
	do {
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
	} while (item != OK_BUTTON && item != CANCEL_BUTTON && item != SAVE_BUTTON);
	for (i = 0; i < 10; i++) {
		GetEditItemText(dlg->FirstGadget, NAME1_TEXT + i, strBuff);
		strncpy(newNames[i], strBuff, 32);
		newNames[i][31] = '\0';
	}
	DisposeDialog(dlg);
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Insert macro names into menu
*/
	for (i = 0; i < 10; i++) {
		strncpy(macroNames2[i], newNames[i], 32);
		SetMenuItem(backWindow, MENUITEM(MACRO_MENU, i, NOSUB), macroNames2[i]);
	}
	if (item == SAVE_BUTTON) {
		SetWaitPointer();
		SaveFKeys();
		SetArrowPointer();
	}
	return (TRUE);
}

/*
 *	Handle macro menu
 */

BOOL DoMacroMenu(WindowPtr window, UWORD item, UWORD sub)
{
	BOOL success = FALSE;

	if (RexxSysBase != NULL && !inMacro) {
		if (item >= 0 && item <= 9) {
			DoMacro(macroNames2[item], NULL);
			success = TRUE;
		} else if (item == OTHERMACRO_ITEM) {
			success = DoOtherMacro();
		} else if (item == CUSTOMMACRO_ITEM) {
			success = DoCustomizeMacro(window);
		}
		/* else failure */
	}
	return (success);
}