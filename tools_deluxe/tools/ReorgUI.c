/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Disk optimize/reorganize code.
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <dos/dos.h>
#include <graphics/gfx.h>
#include <graphics/gfxmacros.h>

#include <rexx/errors.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/Border.h>
#include <Toolbox/BarGraph.h>
#include <Toolbox/DateTime.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Image.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	Externals
 */

extern UBYTE			_tbPenLight, _tbPenDark, _tbPenBlack;
extern BOOL				_tbSmartWindows;
extern TextChar		_strYes[], _strNo[];

extern WindowPtr		cmdWindow;
extern ScreenPtr		screen;
extern MsgPortPtr		mainMsgPort;
extern OptionsRec		options;
extern BOOL				closeFlag, quitFlag;
extern GadgetPtr		currGadgList;
extern TextPtr			strsErrors[], bigStrsErrors[], strPath;
extern TextChar		strBuff[], choiceNameColon[];
extern TextChar		strStart[], strPause[], strResume[], strAbort[];
extern TextChar		strUsed[], strUnused[], strFragments[]/*, strNoFragments[]*/;
extern TextChar		strProcessing[], strCountingFrags[], strBuildingRemap[];
extern TextChar		strResolvingFiles[];
extern WORD				action, mode, stage, intuiVersion;
extern UWORD			charWidth, charHeight, charBaseline;
extern ReqTemplPtr		reqList[];
extern RequestPtr		requester;
extern VolParams		currVolParams;
extern BOOL				abortFlag;
extern ReorgStats		reorgStats;
extern ULONG			*bitMapDataPtr;
extern Rectangle		labelRect, statusRect, statusRect2;
extern BarGraphPtr		barGraph;
extern WORD				processingWidth;
extern ULONG			prevSecs, startSecs, extentBufferSize;
extern UBYTE			*blockCacheBuffer;

/*
 *	Local definitions
 */

enum {
	OPTS_RESET_BUTTON = 2,
	COUNT_RADBTN,
	OPTIMIZE_RADBTN,
	OPTIMIZE_POPUP,
#ifdef OPTION_REMAP
	BUFFER_BOX,
#endif
};

#define USED_LABEL_PAD			16
#define UPDATE_REORG_BUTTONS	99

#define FLOPPY_SIZE				64*32			/*  0 -   1M */
#define MODE1_SIZE				128*64		/*  1 -   4M */
#define MODE2_SIZE				256*128		/*  4 -  16M */
#define MODE3_SIZE				512*128		/* 16 -  32M */
#define MODE4_SIZE				512*128*2	/* 32 -  64M */
#define MODE5_SIZE				512*128*4	/* 64 - 128M */
#define MODE6_SIZE				512*128*8	/*128 - 256M */
#define MODE7_SIZE				512*128*16	/*256 - 512M */
#define MODE8_SIZE				512*128*32	/*512 - 1.0G */
#define MODE9_SIZE				0xFFFFFFFF	/* >1.0G, if we really work with those */

#define NUM_REORG_OPTIONS		(sizeof(reorgOptNames)/sizeof(TextPtr))

static TextPtr reorgOptNames[] = {
	"CheckOnly",	"CheckAndOptimize",
	"FileAccess",	"DirAccess"
};

enum {
	OPT_CHECKONLY,	OPT_CHECKANDOPT,
	OPT_WB,			OPT_CLI,
};

/*
 * Local prototypes
 */

static void	CountFragments(void);
static void BuildFragmentationMap(void);
static ULONG Get1Key(void);
static ULONG Get2Key(void);
static ULONG Get4Key(void);
static ULONG Get8Key(void);
static ULONG Get16Key(void);
static ULONG Get32Key(void);
static ULONG Get64Key(void);
static void PixelBlk(UWORD, ULONG);
static void PutPixel(UWORD, ULONG);
/*static void HandleAbortButton(void);*/
static void HandleLeftButton(void);
static void HandleCloseBox(void);
static BOOL ReorgListFilter(IntuiMsgPtr, WORD *);

/*
 * Local data
 */

BitMapParams	bitMapParamsTable[] = {
	{ FLOPPY_SIZE,	8, 4, 0, Get1Key, PixelBlk },
	{ MODE1_SIZE,	4, 2, 0, Get1Key, PixelBlk },
	{ MODE2_SIZE,	2, 1, 0, Get1Key, PixelBlk },
	{ MODE3_SIZE,	1, 1, 0, Get1Key, PutPixel },
	{ MODE4_SIZE,	1, 1, 1, Get2Key, PutPixel },
	{ MODE5_SIZE,	1, 1, 2, Get4Key, PutPixel },
	{ MODE6_SIZE,	1, 1, 3, Get8Key, PutPixel },
	{ MODE7_SIZE,	1, 1, 4, Get16Key,PutPixel },
	{ MODE8_SIZE,	1, 1, 5, Get32Key,PutPixel },
	{ MODE9_SIZE,	1, 1, 6, Get64Key,PutPixel },
};

/*
 * If reorganization window opens successfully, perform various initializations to it.
 */

BOOL SetupReorgMode()
{
	register WindowPtr window = cmdWindow;
	register GadgetPtr gadgList = currGadgList;
	BOOL success = FALSE;
	register WORD width;
	register WORD height;
	WORD slot;
	Rectangle rect;
	
	AddGList(window, gadgList, -1, -1, 0);
	GetGadgetRect(GadgetItem(gadgList, BORDER_FRAGMENTATION), window, NULL, &statusRect);
	GetGadgetRect(GadgetItem(gadgList, OPTIMIZE_BAR_GRAPH), window, NULL, &rect);
	GetGadgetRect(GadgetItem(gadgList, FRAG_LABEL_TEXT), window, NULL, &labelRect);
	GetGadgetRect(GadgetItem(gadgList, ETA_BORDER), window, NULL, &statusRect2);
	InsetRect(&statusRect2, 2, 1);

	if( (barGraph = NewBarGraph(&rect, currVolParams.MaxKeys, 8, BG_HORIZONTAL | BG_SHOWPERCENT)) != NULL) {
		width = (statusRect.MaxX - statusRect.MinX)-1;
		height = (statusRect.MaxY - statusRect.MinY)-1;
		if( ((reorgStats.Image = NewImage(width, height, screen->BitMap.Depth)) != NULL) ) {
			slot = RemoveGList(window, gadgList, 3);	/* Keep user from seeing */

			InitPath();
			processingWidth = TextLength(window->RPort, strProcessing, (const) strlen(strProcessing));
			labelRect.MaxX = cmdWindow->Width - 15 /* - cmdWindow->BorderRight*/;
			
			mode = MODE_OPTIMIZE;
			action = OPER_READY;
			SetAllMenus();
			
			reorgStats.ImageSize = RASSIZE(width, height);
			BuildFragmentationMap();						/* This takes quite a while...*/
			CountFragments();									/* This takes some too...*/
			
			success = TRUE;
			
			AddGList(window, gadgList, slot, 3, NULL);	/* Okay to hit buttons now. */
			
	/*		EnableGadgetItem(gadgList, BUTTON_CANCEL, window, NULL, FALSE);*/
			RefreshReorgWindow(TRUE);
			
		}
	}
	SetArrowPointer();
	return(success);
}

/* 
 * Free up resources allocated by the reorganization window.
 */
 
void CleanupReorgMode()
{
	if( blockCacheBuffer != NULL ) {
		FreeIOBuffer(&blockCacheBuffer, extentBufferSize);
	}
	if( barGraph != NULL ) {
		DisposeBarGraph(barGraph);
		barGraph = NULL;
	}
	if( reorgStats.Image != NULL ) {
		FreeImage(reorgStats.Image);
	}
	FreeBitMapBuffers();
	RemoveWindow(cmdWindow);
}

/*
 *	Refresh the window's contents
 */

void RefreshReorgWindow(BOOL adjust)
{
	Rectangle winRect;
	register WindowPtr window = cmdWindow;
	register RastPtr rPort = window->RPort;
	BOOL inval = FALSE;
	
	SetDrMd(rPort, JAM1);
	GetWindowRect(window, &winRect);
/*
	Clear the background of the window if first time, or pre DOS2.0 and simple refresh.
*/
	if( adjust || (!_tbSmartWindows) ) {
		if( intuiVersion < OSVERSION_2_0 ) {
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &winRect);
			inval = TRUE;
		}
	}
	if( adjust ) {
		inval = TRUE;
	}
	if( inval ) {
		RefreshGadgets(currGadgList, window, NULL);
		InvalRect(window, &winRect);
	}
	BeginRefresh(window);
/*
	If new window size or not a smart refresh window, we have to draw ourselves.
*/
	if( adjust || (!_tbSmartWindows) ) {
		OutlineOKButton(window);
		DrawBarGraph(rPort, barGraph);
		DrawShadowBox(rPort, &statusRect, 0, FALSE);
		RefreshTimeRemaining();
		RefreshFragmentationLabel();
		RefreshFragmentationMap();
	}
	EndRefresh(window, TRUE);
}

/*
 * Refresh the volume fragmentation label text.
 */
 
void RefreshFragmentationLabel()
{
	register RastPtr rPort;
	register WORD len;
	register WORD strLen;
	register WORD x;
	register WORD y;
	WORD padBox;
	TextChar numBuff[16];
	Rectangle rect;
	register TextPtr textPtr;
	BOOL readyMode;
	
	if( cmdWindow->Height > labelRect.MinY ) {
		rPort = cmdWindow->RPort;
		padBox = charWidth + 2;
		SetDrMd(rPort, JAM1);
		len = TextLength(rPort, strUnused, strLen = strlen(strUnused)) + padBox;
		x = ((cmdWindow->Width - 15) - (len<<1)) - USED_LABEL_PAD;
		y = labelRect.MinY;
		rect = labelRect;
		rect.MaxX = x-1;
		
		readyMode = (action != OPER_IN_PROG) && (action != OPER_PAUSE);
		if( readyMode || (stage == STAGE_COUNT) ) {
			SetOPen(rPort, _tbPenBlack);
			SetAPen(rPort, _tbPenDark);
			RectFill(rPort, x, y, x+charWidth, y+charBaseline);
			WaitBlit();
			SetAPen(rPort, _tbPenLight);
			x += len + USED_LABEL_PAD;
			RectFill(rPort, x, y, x+charWidth, y+charBaseline);
			WaitBlit();
			BNDRYOFF(rPort);
			SetAPen(rPort, _tbPenBlack);
			x -= ((len + USED_LABEL_PAD) - padBox);
			Move(rPort, x, y + charBaseline);
			Text(rPort, strUsed, strlen(strUsed));
			Move(rPort, x + len + USED_LABEL_PAD, y + charBaseline);
			Text(rPort, strUnused, strLen);
		}
		if( readyMode ) {
			SetAPen(rPort, _tbPenBlack);
			strcpy(strBuff, strFragments);
			NumToCommaString(reorgStats.NumFragments, numBuff);
			strcat(strBuff, numBuff);
			textPtr = strBuff;
		} else {
			switch(stage) {
			case STAGE_COUNT:
				textPtr = strCountingFrags;
				break;
			case STAGE_RESOLVING:
				textPtr = strResolvingFiles;
				break;
			case STAGE_REMAP:
				textPtr = strBuildingRemap;
				break;
			case STAGE_REORG:
			default:
				strcpy(strBuff, strProcessing);
				strcat(strBuff, choiceNameColon);
				textPtr = strBuff;
				break;
			}
		}
		SetAPen(rPort, _tbPenLight);
		FillRect(rPort, &rect);
		rect.MaxX--;
		SetAPen(rPort, _tbPenBlack);
		Move(rPort, labelRect.MinX, y + charBaseline);
		TextInWidth(rPort, textPtr, strlen(textPtr), WIDTH(&rect), TRUE);
		if( textPtr == strProcessing ) {
			DisplayReorgPath();
		}
	}
}

/*
 * Refresh the time remaining display
 */
 
void RefreshTimeRemaining()
{
	ULONG secs;
	ULONG micros;
	register RastPtr rPort;
	register ULONG num;
	register ULONG fin;
	register ULONG max;
	register UWORD mins;
	WORD hrs;
	TextChar buff[15];
	TextPtr ptr;
	
	if( cmdWindow->Height > statusRect2.MinY ) { 
		rPort = cmdWindow->RPort;
		num = currVolParams.DoneBlocks;
		max = (currVolParams.MaxKeys - currVolParams.FreeKeys) - (currVolParams.BitMapBlocks + currVolParams.BitMapExtBlks);
		if( !reorgStats.MemKeysFlag ) {
			max -= reorgStats.RemapBlocks;
		}
		ptr = &buff[0];
		
		CurrentTime(&secs, &micros);
		if( (action != OPER_IN_PROG) || (stage != STAGE_REORG) || (num == 0) ||
			( (num < (max >> 4)) && (secs <= (startSecs+9)) ) ) {
#ifdef INCLUDE_SECS
			strcpy(ptr, "--:--:--");
#else
			strcpy(ptr, "--:--");
#endif
		} else {
			while( max > 32767 ) {	/* Could make 65535, but this increases secs precision */
				max >>= 1;
				num >>= 1;
			}
			if( (num == 0) || (secs == prevSecs) ) {
				return;
			}
			prevSecs = secs;
			secs -= startSecs;
			
			num = (((UWORD)secs) * ((UWORD)max) / ((UWORD)num)) - secs;
			
			max = reorgStats.ActiveFiles + reorgStats.ActiveDirs + reorgStats.ActiveLinks;
			fin = reorgStats.FinishedEntries;
			while( max > 32767 ) {
				max >>= 1;
				fin >>= 1;
			}
			if( fin == 0 ) {
				return;
				}
		
			fin = (((UWORD)secs) * ((UWORD)max) / ((UWORD)fin)) - secs;
/*
	Make it a weighted average of 3/4 bytes processed, 1/4 files processed
*/		
			fin = (num + fin) >> 1;
			num = (num + fin) >> 1;	
#ifdef INCLUDE_SECS
			secs = num % 60;
#else
			num += 59;
#endif
			num /= 60;
			mins = num % 60;
			hrs = num / 60;
			if( hrs < 10 ) {
				*ptr++ = '0';
			}
			NumToString(hrs, ptr);
			ptr = &buff[2];
			*ptr++ = ':';
			if( mins < 10 ) {
				*ptr++ = '0';
			}
			NumToString(mins, ptr);
#ifdef INCLUDE_SECS
			ptr = &buff[5];
			*ptr++ = ':';
			if( secs < 10 ) {
				*ptr++ = '0';
			}
			NumToString(secs, ptr);
#endif
		}
		SetAPen(rPort, _tbPenLight);
		SetDrMd(rPort, JAM1);
		Move(rPort, statusRect2.MinX, statusRect2.MinY + charHeight);
		FillRect(rPort, &statusRect2);
		SetAPen(rPort, _tbPenBlack);
#ifdef INCLUDE_SECS
		TextInWidth(rPort, buff, 8, statusRect2.MaxX - statusRect2.MinX, FALSE);
#else
		TextInWidth(rPort, buff, 5, statusRect2.MaxX - statusRect2.MinX, FALSE);
#endif
	}
}

/*
 * Build the graphic bitmap image from the volume bitmap
 */
 
static void BuildFragmentationMap()
{
	register WORD keysPerRow;
	register UWORD counter;
	register UWORD currPixel = 0;
	BitMapParamsPtr bitMapTablePtr = reorgStats.BitMapTablePtr;
	register ULONG (*getFunc)(void) = bitMapTablePtr->GetKeyStatusFunc;
	register void (*drawFunc)(UWORD, ULONG) = bitMapTablePtr->DrawKeyStatusFunc;
	
	reorgStats.CurrBit = 0L;
	reorgStats.CurrMask = 0L;
	reorgStats.CurrPtr = bitMapDataPtr-1;

	counter = currVolParams.MaxKeys /*/ (1 <<*/>> bitMapTablePtr->Shift;
	while( counter-- ) {
		(*drawFunc)(currPixel, (*getFunc)());
		currPixel++;
	}

	keysPerRow = REORG_BORDER_WIDTH / bitMapTablePtr->XSize;
	counter = keysPerRow - (currPixel % keysPerRow);
	while( counter-- ) {
		(*drawFunc)(currPixel, reorgStats.BusyFragFlag);
		currPixel++;
	}
}

/*
 * Called during the reorganization process to update only a single pixel of the
 * bitmap image. Key of pixel in "key", marked free if "free" TRUE (else busy).
 */
 
void DrawKeyPixel(ULONG key, ULONG free)
{
	(*reorgStats.BitMapTablePtr->DrawKeyStatusFunc)((key - currVolParams.LowestKey) >> reorgStats.BitMapTablePtr->Shift, free);
}

/*
 * Determine the correct bitmap table entry to use based on the number of keys.
 */
 
void GetBitMapTablePtr()
{
	register ULONG keys = currVolParams.MaxKeys;
	register BitMapParamsPtr bitMapTablePtr = &bitMapParamsTable[0];
	
	while( keys > bitMapTablePtr->KeyLimit ) {
		bitMapTablePtr++;
	}
	reorgStats.BitMapTablePtr = bitMapTablePtr;
}

/*
 * Draw the volume fragmentation map
 */
 
void RefreshFragmentationMap()
{
	Rectangle rect;

	if( cmdWindow->Height > statusRect.MinY ) {
		rect = statusRect;
		InsetRect(&rect, 1, 1);
		DrawImage(cmdWindow->RPort, reorgStats.Image, rect.MinX, rect.MinY);
	}
}

/*
 * One of the following routines is called from BuildFragmentationMap().
 * Returns a boolean as to how it should be drawn.
 */
 
static ULONG Get1Key()
{
	if( reorgStats.CurrBit == 0 ) {
		reorgStats.CurrPtr++;
		reorgStats.CurrBit = 31;
		reorgStats.CurrMask = 1L;
	} else {
		reorgStats.CurrBit--;
		reorgStats.CurrMask <<= 1;
	}
	return(*reorgStats.CurrPtr & reorgStats.CurrMask);
}

static ULONG Get2Key()
{
	if( reorgStats.CurrBit == 0 ) {
		reorgStats.CurrPtr++;
		reorgStats.CurrBit = 30;
		reorgStats.CurrMask = 3L;
	} else {
		reorgStats.CurrBit -= 2;
		reorgStats.CurrMask <<= 2;
	}
	return(*reorgStats.CurrPtr & reorgStats.CurrMask);
}

static ULONG Get4Key()
{
	if( reorgStats.CurrBit == 0 ) {
		reorgStats.CurrPtr++;
		reorgStats.CurrBit = 28;
		reorgStats.CurrMask = 15L;
	} else {
		reorgStats.CurrBit -= 4;
		reorgStats.CurrMask <<= 4;
	}
	return(*reorgStats.CurrPtr & reorgStats.CurrMask);
}

static ULONG Get8Key()
{
	if( reorgStats.CurrBit == 0 ) {
		reorgStats.CurrPtr++;
		reorgStats.CurrBit = 24;
		reorgStats.CurrMask = 255L;
	} else {
		reorgStats.CurrBit -= 8;
		reorgStats.CurrMask <<= 8;
	}
	return(*reorgStats.CurrPtr & reorgStats.CurrMask);
}

static ULONG Get16Key()
{	
	if( reorgStats.CurrBit = !reorgStats.CurrBit ) {
		reorgStats.CurrPtr++;
		return( (ULONG) *(((UWORD *)reorgStats.CurrPtr)+1) );
	} else {
		return( (ULONG) *((UWORD *)reorgStats.CurrPtr) );
	}
}

static ULONG Get32Key()
{
	return(*++reorgStats.CurrPtr);
}

static ULONG Get64Key()
{
	return(*++reorgStats.CurrPtr | *++reorgStats.CurrPtr);
}

/*
 * Stores key status as a block of 2x1, 4x2, or 8x4 pixels.
 * The variable "status" determines pixel color.
 */
 
static void PixelBlk(UWORD pixel, ULONG status)
{
	register UBYTE *ptr;
	register BitMapParamsPtr bitMapTablePtr = reorgStats.BitMapTablePtr;
	register UWORD i;
	register ULONG mask;
	register UBYTE bitMask;
	register UBYTE color;
	UWORD byteOffset;
	UWORD y;
	UWORD xPos;
	UWORD depth = screen->BitMap.Depth;
	
	if( reorgStats.BusyFragFlag = status != 0 ) {
		color = _tbPenLight;
	} else {
		color = _tbPenDark;
	}
	i = REORG_BORDER_WIDTH / bitMapTablePtr->XSize;
	
	xPos = pixel % i;
	byteOffset = ((xPos * bitMapTablePtr->XSize) >> 3) + ((pixel / i) * bitMapTablePtr->YSize * (REORG_BORDER_WIDTH >> 3));
	bitMask = 1 << (((7 - (xPos & 7)) & ((8 / bitMapTablePtr->XSize)-1)) * bitMapTablePtr->XSize);
	mask = bitMask;
	
	for( i = 0; i < bitMapTablePtr->XSize ; i++ ) {
		mask += mask;
		bitMask |= mask;
	}
	for( y = 0 ; y < bitMapTablePtr->YSize ; y++ ) {
		
		ptr = ((UBYTE *)reorgStats.Image->ImageData) + byteOffset;
		
		for( i = 0, mask = 1 ; i < depth ; i++, mask <<= 1 ) {
			if( color & mask ) {
				*ptr |= bitMask;
			} else {
				*ptr &= ~bitMask;
			}
			ptr += reorgStats.ImageSize;
		}
		byteOffset += REORG_BORDER_WIDTH >> 3;
	}
}

/*
 * Put a single pixel in the correct color (determined by "status").
 */
 
static void PutPixel(UWORD pixel, ULONG status)
{
	register UBYTE *ptr = ((UBYTE *)reorgStats.Image->ImageData) + (pixel >> 3);
	register UBYTE color;
	register UWORD i;
	register ULONG mask;					/* Compiler loves 32-bit ints... */
	register UBYTE bitMask;
	UWORD depth = screen->BitMap.Depth;
	
	bitMask = 1 << (7 - (pixel & 7));
	if( reorgStats.BusyFragFlag = status != 0) {
		color = _tbPenLight;
	} else {
		color = _tbPenDark;
	}
	for( i = 0, mask = 1 ; i < depth; i++, mask <<= 1 ) {
		if( color & mask ) {
			*ptr |= bitMask;
		} else {
			*ptr &= ~bitMask;
		}
		ptr += reorgStats.ImageSize;
	}
}

/*
 * Determine number of fragments and set the label accordingly.
 */
 
static void CountFragments()
{
	register ULONG *ptr = bitMapDataPtr;
	register ULONG keys = currVolParams.MaxKeys;
	register ULONG mask = 1;
	register BOOL state;
		
#ifdef DO_UNUSED
	state = FALSE;
#else
	state = TRUE;
#endif

	reorgStats.NumFragments = 0;
	do {
#ifdef DO_UNUSED
		if( *ptr & mask ) {
			if( !state ) {
				++reorgStats.NumFragments;
				state = TRUE;
			}
		} else {
			state = FALSE;
		}
#else
		if( *ptr & mask ) {
			state = TRUE;
		} else {
			if( state ) {
				++reorgStats.NumFragments;
				state = FALSE;
			}
		}
#endif		
		if( --keys ) {
			if( ((LONG)mask) < 0 ) {
				ptr++;
				mask = 1;
			} else {
				mask <<= 1;
			}
		}
	} while( keys );
}

/*
 * Display the full pathname above the reorg bitmap while reorganizing.
 * NOTE: The volume/dev should already be there, so don't draw it!
 */
 
void DisplayReorgPath()
{
	register RastPtr rPort;
	Rectangle rect;
	register TextPtr ptr;
	
	if( cmdWindow->Height > labelRect.MinY ) {
		rPort = cmdWindow->RPort;
		ptr = strPath;
		rect = labelRect;
		rect.MinX += processingWidth;
		
		while( *ptr && (*ptr != ':') ) {
			ptr++;
		}
		if( *ptr++ ) {
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &rect);
			SetAPen(rPort, _tbPenBlack);
			Move(rPort, rect.MinX, rect.MinY + charBaseline);
			DoTextInWidth(ptr, WIDTH(&rect), TRUE);
		}
	}
}

/*
 * Proceed with whatever optimization the user has specified (if any).
 * This routine sets up all the user-interface parameters, while the routines it
 * calls deal with the actual low-level disk munging.
 */
 
BOOL DoOptimize(void)
{
	register BOOL success = FALSE;
	register BOOL reorganize = options.ReorgOpts.Optimize;
	register WindowPtr window = cmdWindow;
	register GadgetPtr gadgList = currGadgList;
	UWORD size;
	BOOL reorgStarted = FALSE;
	WORD stageFinished;
	
	SetBarGraphMax(window->RPort, barGraph, currVolParams.MaxKeys);
	abortFlag = FALSE;
	if( AllocateExtBuffer() && AllocateAltBitMap() ) {
		if( (!reorganize) || (options.PrefsOpts.Interactive != 0) || (DoDialog(bigStrsErrors[BIG_ERR_REORG_WARN], CANCEL_BUTTON, _strYes, _strNo, NULL, TRUE) == OK_BUTTON) ) {
			if( reorganize ? PromptForWriteProtected() : PromptForDisk() ) {

				InitPath();
				strcpy(strBuff, strProcessing);
				strcat(strBuff, strPath);
				
				processingWidth = TextLength(window->RPort, strBuff, strlen(strBuff));
				action = OPER_IN_PROG;
				stage = STAGE_COUNT;
				currVolParams.DoneBlocks = 0;
				RefreshFragmentationLabel();
				SetAllMenus();
				SetButtonItem(gadgList, BUTTON_OK, window, NULL, &strPause[0], CMD_KEY_PAUSE);
/*				EnableGadgetItem(gadgList, BUTTON_CANCEL, window, NULL, TRUE);*/
				EnableGadgetItem(gadgList, BUTTON_OPTIONS, window, NULL, FALSE);
				
				size = (((ULONG)&reorgStats.BitMapTablePtr) - ((ULONG)&reorgStats)) + sizeof(BitMapParamsPtr);
				BlockClear( ((UBYTE *)&reorgStats)+size, sizeof(ReorgStats) - size);
				
				CountFreeKeys();
				if( CheckRemapSpace() ) {	/* Verify that enough free blocks exist */
					if( InitDirLists() ) {	/* Build initial entries in dir tree */
						if( success = CountFragmentedFiles() ) {
							if( reorganize ) {
								currVolParams.DoneBlocks = 0;
								reorgStarted = TRUE;
								blockCacheBuffer = AllocIOBuffer(extentBufferSize);
								success = DoReorganize();
								stageFinished = stage;
								stage = STAGE_DONE;
								if( success ) {
/*
	Once WriteBitMap() is done, the bitmap has checksums in it. Therefore, we
	need to re-read it in order to continue to display it.
*/
									if( ReadBitMap() ) {
										BuildFragmentationMap();
										RefreshFragmentationMap();
									}
								}
								InitPath();					/* Zap pathname */
								DisplayReorgPath();		/* Erase last pathname */
								RefreshTimeRemaining();
								CountFragments();
							}
						}
						SetBarGraph(window->RPort, barGraph, 0);
						stage = STAGE_COUNT;
						FreeDirBlocks();
					}
				}
				FreeBadBlockBuffer();			/* In case "bad.blocks" file loaded */
				action = OPER_READY;
				RefreshFragmentationLabel();
				SetAllMenus();
				SetButtonItem(gadgList, BUTTON_OK, window, NULL, &strStart[0], CMD_KEY_START);
/*				EnableGadgetItem(gadgList, BUTTON_CANCEL, window, NULL, FALSE);*/
				EnableGadgetItem(gadgList, BUTTON_OPTIONS, window, NULL, TRUE);
				if( reorgStarted ) {
					if( success ) {
						if( options.PrefsOpts.Interactive == 0 ) {
							if( currVolParams.ActiveLocks ) {
								ExplainDialog(BIG_ERR_REORG_REBOOT_NOW);
							} else {
								Error(ERR_REORG_OK);
							}
						}
					} else if( (!abortFlag) && (stageFinished > STAGE_RESOLVING) ) {
						ExplainDialog(BIG_ERR_REORG_FAILED);
					}
				}
			}
		}
	} else {
		OutOfMemory();
	}
	FreeAltBitMap();
	FreeExtBuffer();
	return(success);
}

/*
 *	Handles what to do with the close box in
 *	the Repair window based on current 'action' mode.
 */

static void HandleCloseBox()
{
	switch(action) {
	case OPER_READY:
		ReturnToMain();
		break;
	case OPER_IN_PROG:
	case OPER_PAUSE:
		if( closeFlag = CheckAbort() ) {
			abortFlag = TRUE;
			action = OPER_READY;
			SetAllMenus();
		}
		break;
	}
}

/*
 *	Handles what to do with the left button in
 *	the Repair window based on current 'action' mode.
 */

static void HandleLeftButton()
{
	switch(action) {
	case OPER_READY:
		DoOptimize();
		if( closeFlag && (!quitFlag) ) {
			ReturnToMain();
		}
		break;
	case OPER_IN_PROG:
		action = OPER_PAUSE;
		SetButtonItem(currGadgList, BUTTON_OK, cmdWindow, NULL, &strResume[0], CMD_KEY_RESUME);
/*		RefreshRepairStats();*/
		MotorOff();
		break;
	case OPER_PAUSE:
		action = OPER_IN_PROG;
		SetButtonItem(currGadgList, BUTTON_OK, cmdWindow, NULL, &strPause[0], CMD_KEY_PAUSE);
		break;
	default:
		break;
	}
}

/*
 *	Handles what to do with the right button in
 *	the Repair window based on current 'action' mode.
 */
/*
static void HandleAbortButton()
{
	switch(action) {
	case OPER_IN_PROG:
	case OPER_PAUSE:
		if( abortFlag = CheckAbort() ) {
			action = OPER_READY;
			SetAllMenus();
		}
	default:
		break;
	}
}
*/
/*
 *	Allow user to adjust repair options
 */

BOOL DoReorgOptions()
{
	register WindowPtr window;
	register RequestPtr req;
	BOOL success = FALSE;
	ReorgOptsRec reorgOpts;
	register WORD item;
	GadgetPtr gadgList;
	
	reqList[REQ_REORGOPTS]->Gadgets[OPTIMIZE_POPUP].Value = options.ReorgOpts.OptimizeType != 0;
	if( action == OPER_READY ) {
		req = DoGetRequest(REQ_REORGOPTS);
		if( req == NULL ) {
			Error(ERR_NO_MEM);
		} else {
			requester = req;
			window = cmdWindow;
			reorgOpts = options.ReorgOpts;
			OutlineOKButton(window);
			gadgList = req->ReqGadget;
			item = UPDATE_REORG_BUTTONS;
			do {
				switch( item ) {
				case OPTS_RESET_BUTTON:
					SetDefaultReorgOptions(&reorgOpts);
					SetGadgetValue(GadgetItem(gadgList, OPTIMIZE_POPUP), window, req, reorgOpts.OptimizeType != 0);
				case UPDATE_REORG_BUTTONS:
					GadgetOnOff(window, COUNT_RADBTN, !reorgOpts.Optimize);
					GadgetOnOff(window, OPTIMIZE_RADBTN, reorgOpts.Optimize);
#ifdef OPTION_REMAP
					GadgetOnOff(window, BUFFER_BOX, reorgOpts.Buffer);
#endif
					break;
				case COUNT_RADBTN:
				case OPTIMIZE_RADBTN:
					GadgetOff(window, reorgOpts.Optimize + COUNT_RADBTN);
					GadgetOn(window, item);
					reorgOpts.Optimize = item - COUNT_RADBTN;
					break;
				case OPTIMIZE_POPUP:
					reorgOpts.OptimizeType = GetGadgetValue(GadgetItem(gadgList, item)) != 0;
					break;
#ifdef OPTION_REMAP
				case BUFFER_BOX:
					ToggleCheckbox(&reorgOpts.Buffer, item, window);
					break;
#endif
				default:
					break;
				}
				item = ModalRequest(mainMsgPort, window, DialogFilter);
			} while( (item != OK_BUTTON) && (item != CANCEL_BUTTON) );
			DestroyRequest(req);
			if( success = (item != CANCEL_BUTTON) ) {
/*
	Set new values
*/
				options.ReorgOpts = reorgOpts;
			}
		}
	}
	return(success);
}

/*
 *	Handle special keys.
 */

static BOOL ReorgListFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	ULONG class = intuiMsg->Class;
	register WORD code;
	register WindowPtr window = cmdWindow;
	WORD modifier;
	
	if (intuiMsg->IDCMPWindow == window) {
		switch (class) {
/*
	Handle cursor keys
*/
		case RAWKEY:
			code = intuiMsg->Code;
			modifier = intuiMsg->Qualifier;
			ReplyMsg((MsgPtr)intuiMsg);
			DoRawKey(window, code, modifier);
			return(TRUE);
		}
	}
	return (FALSE);
}

/*
 *	Handle repair window button hits
 */

BOOL DoReorgDialogMsg(IntuiMsgPtr intuiMsg)
{
	WORD item;
	DialogPtr dlg;

	if (ReorgListFilter(intuiMsg, &item)) {
		return TRUE;
	}
	if (DialogSelect(intuiMsg, &dlg, &item)) {
		ReplyMsg((MsgPtr)intuiMsg);
		switch(item) {
		case DLG_CLOSE_BOX:
			HandleCloseBox();
			break;
		case BUTTON_OK:
			HandleLeftButton();
			break;
		/*
		case BUTTON_CANCEL:
			HandleAbortButton();
			break;
		*/
		case BUTTON_OPTIONS:
			(void) DoReorgOptions();
			break;
		}
		return TRUE;
	}

	ReplyMsg((MsgPtr)intuiMsg);
	return FALSE;
}

/*
 *	Set reorg options of given option name
 *	Used for AREXX macros
 *	Return FALSE if not a valid option name
 */

LONG SetOptimizeOption(TextPtr optName)
{
	register WORD i;
	TextChar buff[256];
	UWORD len;
	LONG result = RC_OK;

	do {	
		GetNextWord(optName, buff, &len);
		optName += len;
		for (i = 0; i < NUM_REORG_OPTIONS; i++) {
			if (CmpString(buff, reorgOptNames[i], strlen(buff), (WORD) strlen(reorgOptNames[i]), FALSE) == 0) {
				break;
			}
		}
		if( i >= NUM_REORG_OPTIONS ) {
			result = RC_WARN;
		} else {
			switch (i) {
			case OPT_CHECKONLY:
			case OPT_CHECKANDOPT:
				options.ReorgOpts.Optimize = i == OPT_CHECKANDOPT;
				break;
			case OPT_WB:
			case OPT_CLI:
				options.ReorgOpts.OptimizeType = i - OPT_WB;
				break;
			}
		}
	} while(*optName);
	return(result);
}
