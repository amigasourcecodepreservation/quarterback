/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Initialization
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <graphics/gfxbase.h>
#include <graphics/displayinfo.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>
#include <workbench/startup.h>
#include <workbench/workbench.h>
#include <libraries/dos.h>
#include <libraries/dosextens.h>
#include <resources/disk.h>
#include <exec/memory.h>
#include <exec/tasks.h>
#include <dos/dostags.h>
#include <dos/dosextens.h>
 
#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>
#include <proto/icon.h>

#include <clib/alib_protos.h>

#include <string.h>
#include <stdlib.h>					/* For exit() prototype */

#include <Toolbox/ColorPick.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/DOS.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Image.h>
#include <Toolbox/List.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Screen.h>		/* For DisposeScreen() prototype */
#include <Toolbox/SerialInfo.h>
#include <Toolbox/StdFile.h>		/* For ConvertFileName() prototype */
#include <Toolbox/StdInit.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>		/* For CloseWindowSafely() prototype */

#include "Tools.h"
#include "Version.h"
#include "Proto.h"

/*
 *	External variables
 */

extern UBYTE			_tbPenBlack, _tbPenDark, _tbPenLight, _tbPenWhite;
extern TextPtr			initErrors[];
extern TextPtr			strPath, strSavePath;
extern TextChar		strCancel[], strProgPath[], progPathName[], strProgName[];
extern TextChar		strPortName[], strBuff[], strSerializedFor[], strSerialNumber[];

extern struct WBStartup	*WBenchMsg;
extern WORD				intuiVersion;
extern ScreenPtr		screen;
extern struct NewScreen	newScreen;
extern MenuPtr			menuStrip;

extern WORD				numColors;
extern WindowPtr		cmdWindow, backWindow;

extern ProcessPtr 		process;

extern MsgPortPtr		keyPort;
extern struct IOStdReq *keyRequest;

extern MenuTemplate		docWindMenus[];

extern BOOL				_tbOnPubScreen, _tbSmartWindows;
extern BOOL				fromCLI;

extern MsgPortPtr		mainMsgPort;

extern UWORD			charHeight, charBaseline, charWidth;
extern OptionsRec		options;

extern GadgetTemplate	repairGadgets[], recoverGadgets[], editGadgets[];

extern DlgTemplPtr		dlgList[];

extern Ptr				fib;
extern ListHeadPtr		deviceList, volumeList;

extern UWORD			arrowWidth, arrowHeight, leftEdge;
extern TextFontPtr		smallFont;

extern BOOL				blockAccess;
extern UWORD			iconColors[];

/*
 *	Local variavles and definitions
 */

static TextAttr smallAttr = {
	"topaz.font", 8, FS_NORMAL, FPF_ROMFONT
};

#define INIT_DLG_WAIT	200		/* Four seconds, as per the boss' request */

/*
 *	Local prototypes
 */

static void WaitVisitor(void);
static void	DoInitError(WORD);
static void TweakWindowGadgets(GadgetTemplate *);
static void TweakEditGadgets(void);
static void TweakWindowArrow(GadgetTemplate *);
static void	ReadQBOptions(int, char **);
static void	DoQBOption(TextPtr);
static BOOL	MatchOptions(TextPtr, TextPtr, TextPtr);	

/*
 * Call Toolbox InitError() and abort mission
 */
 
static void DoInitError(WORD errNum)
{
	InitError(initErrors[errNum]);
	ShutDown();
	_exit(RETURN_FAIL);
}

/*
 *	Initialization routine
 *	Open necessary libraries and devices, and open background window
 */

void Init(register int argc, char *argv[])
{
	TextPtr progName;
	register Dir dirLock;
	struct DateStamp initDateStamp, endDateStamp;
	register DialogPtr initDlg;
	Rectangle rect;
	RastPtr rastPtr = NULL;
	struct TagItem tag;
	ColorMapPtr colorMap;
	TextPtr userName, companyName, serialNum;
	
	fromCLI = (argc > 0);
	process = (struct Process *)FindTask(NULL);

	if( !StdInit(OSVERSION_1_2) ) {
		ShutDown();
		_exit(RETURN_FAIL);
	} else {
		

/*
	Open basic libraries
*/
		intuiVersion = LibraryVersion((struct Library *) IntuitionBase);

/*
	For pre-2.0 system, need to get "ALT" keys through keyboard device.
*/	
		if( intuiVersion < OSVERSION_2_0 ) {
			if( (keyPort = CreatePort(NULL, NULL)) == NULL )
				DoInitError(INIT_NOMEM);
			if( (keyRequest = (struct IOStdReq *) CreateExtIO(keyPort, sizeof(struct IOStdReq)) ) == NULL )
				DoInitError(INIT_NOMEM);
			if( OpenDevice("keyboard.device", NULL, (IOReqPtr)keyRequest, NULL) ) {
				DeleteExtIO((IOReqPtr) keyRequest);
				keyRequest = NULL;
				DoInitError(INIT_NOMEM);
			}
		}		
		mainMsgPort = (MsgPortPtr) CreatePort(strPortName, NULL);
		if( intuiVersion < OSVERSION_2_0 ) {
			fib = AllocMem(sizeof(struct FileInfoBlock), MEMF_CLEAR);
		} else {
			tag.ti_Tag = TAG_END;
			fib = AllocDosObject(DOS_FIB, &tag);
		}
		strPath = AllocMem(PATHSIZE, MEMF_CLEAR);
		strSavePath = AllocMem(PATHSIZE, MEMF_CLEAR);
		deviceList = CreateList();
		volumeList = CreateList();
		if( (fib || mainMsgPort || strPath ||
				strSavePath || deviceList || volumeList) == NULL ) {
			DoInitError(INIT_NOMEM);
		}
		
		if (argc)								/* Running under CLI */
			progName = argv[0];
		else										/* Running under Workbench */
			progName = WBenchMsg->sm_ArgList->wa_Name;
		dirLock = ConvertFileName(progName);
		BuildPath(progName, progPathName, dirLock, GADG_MAX_STRING-2);
		UnLock(dirLock);
		
		GetStdProgPrefs();
		
		ReadQBOptions(argc, argv);
/*
	Initialize smart windows default before getting screen options
*/
		_tbSmartWindows = TRUE;

/*
	Open screen (or get pointer to workbench screen)
*/
		if ((screen = GetScreen(argc, argv, &newScreen, NULL, NUM_STDCOLORS, strProgName )) == NULL) {
			DoInitError(INIT_NOSCREEN);
		}
/*
	Init the toolbox
*/
		InitToolbox(screen);

/*
	Get 8 pixel high font (used in disk editor)
*/
	smallFont = GetFont(&smallAttr);
/*
	A few font defs
*/
		charHeight = screen->RastPort.Font->tf_YSize;
		charBaseline = screen->RastPort.Font->tf_Baseline;
		charWidth = screen->RastPort.Font->tf_XSize;
		arrowWidth  = ARROW_WIDTH;
		arrowHeight = ARROW_HEIGHT;
		leftEdge	= 2;
		if (intuiVersion < OSVERSION_2_0) {
			arrowWidth  -= 2;
			arrowHeight -= 1;
			leftEdge--;
		}
		TweakWindowGadgets(repairGadgets);
		TweakWindowGadgets(recoverGadgets);
		TweakEditGadgets();
/*
	Open background window and display init dialog
*/
		numColors = 1 << screen->BitMap.Depth;
		if ((backWindow = OpenBackWindow()) == NULL)
			DoInitError(INIT_NOMEM);

/*
	Have system requesters appear in this screen
*/
		SetSysRequestWindow(backWindow);

/*
	Set up menus
*/
		if ((menuStrip = GetMenuStrip(docWindMenus)) == NULL)
			DoInitError(INIT_NOMEM);

		InsertMenuStrip(backWindow, menuStrip);

		if( _tbOnPubScreen && intuiVersion >= OSVERSION_2_0)
			UnlockPubScreen(NULL, screen);				/* Recommended procedure */

/*
	Initialize icon colors for main gadgets and about box.
*/		
		colorMap = screen->ViewPort.ColorMap;
		iconColors[0] = GetRGB4(colorMap, _tbPenLight);
		iconColors[1] = GetRGB4(colorMap, _tbPenBlack);
		iconColors[2] = GetRGB4(colorMap, _tbPenWhite);
		iconColors[3] = GetRGB4(colorMap, _tbPenDark);
/*
	Get serial info
*/
#if 0
		if( !GetSerialInfo(&userName, &companyName, &serialNum, screen, mainMsgPort,
			strProgName, PROGRAM_VERSION, progPathName) ) {
			ShutDown();
			_exit(RETURN_FAIL);
		}
#else
		userName = companyName = serialNum = "-";
#endif
/*
	Open the about box for a little while
*/
		dlgList[DLG_ABOUT]->Gadgets++;					/* Past OK button */
		initDlg = GetDialog(dlgList[DLG_ABOUT], screen, mainMsgPort);
		dlgList[DLG_ABOUT]->Gadgets--;					/* Restore... */
		DateStamp(&initDateStamp);

		if( initDlg ) {
			SetStdPointer(initDlg, POINTER_WAIT);
			GetGadgetRect(GadgetItem(initDlg->FirstGadget, ABOX_BORDER-1), initDlg, NULL, &rect);
			rastPtr = BeginAboutBox(initDlg, &rect, TRUE);
			rect.MinX += 4;
			rect.MinY += 3 + charBaseline;
			Move(initDlg->RPort, rect.MinX /*+ 
				((WIDTH(&rect) - TextLength(initDlg->RPort, strSerializedFor, (const)strlen(strSerializedFor))) >> 1)*/, rect.MinY);
			Text(initDlg->RPort, strSerializedFor, (const)strlen(strSerializedFor));
			rect.MinX += (charWidth << 1);
			rect.MinY += (charHeight + 15);
			Move(initDlg->RPort, rect.MinX, rect.MinY);
			TextInWidth(initDlg->RPort, userName, strlen(userName), WIDTH(&rect), FALSE);
			Move(initDlg->RPort, rect.MinX, rect.MinY + charHeight + 4);
			TextInWidth(initDlg->RPort, companyName, strlen(companyName), WIDTH(&rect), FALSE);
			strcpy(strBuff, strSerialNumber);
			strcat(strBuff, serialNum);
			Move(initDlg->RPort, rect.MinX, rect.MinY + (charHeight << 1) + 8);
			Text(initDlg->RPort, strBuff, strlen(strBuff));
		}
/*
	Begin time-consuming stuff here.
*/
		PatchGiveUnit();
		BeginWait();
		GetSysPrefs();
		InitIcons();
		InitPrintHandler();
		InitRexx();
		MakeModeImages();
		LoadFKeys();
/*
	Done with time-consuming stuff, time to close the about box.
*/
		DateStamp(&endDateStamp);
/*
	Assume initialization won't take more than one minute (won't hurt if it does though)
	The idea here is to always present the Init window the same amount of time,
	regardless of CPU speed, so that the user isn't left wondering "what was that?"
*/
		if( initDateStamp.ds_Minute != endDateStamp.ds_Minute ) {
			endDateStamp.ds_Tick += TICKS_PER_SECOND * 60;
		}
		if( (endDateStamp.ds_Tick - initDateStamp.ds_Tick) < INIT_DLG_WAIT) {
			Delay(INIT_DLG_WAIT - (endDateStamp.ds_Tick - initDateStamp.ds_Tick));
		}
		EndAboutBox(rastPtr);
		DisposeDialog(initDlg);
		EndWait();					/* Don't add the operation window before this! */
		if( (cmdWindow = OpenModeWindow()) == NULL ) {
			DoInitError(INIT_NOWIN);
		}
	}
}

/*
 *	Wait until all visitor windows are closed on screen
 */

static void WaitVisitor()
{
	register WindowPtr window;

	if( screen != NULL && backWindow != NULL && (!_tbOnPubScreen) ) {
		ModifyIDCMP(backWindow, REFRESHWINDOW );
		for (;;) {
			FlushMainPort();
			for (window = screen->FirstWindow; window; window = window->NextWindow) {
				if (window != backWindow)
					break;
			}
			if( window == NULL ) {		/* No visitor windows found */
				if( intuiVersion < OSVERSION_2_0 ||
					!IsPubScreen(screen) ||
					(PubScreenStatus(screen, PSNF_PRIVATE) & PSNF_PRIVATE))
					break;
			}
			Delay(10L);					 /* Wait 1/5 second before checking again */
		}
	}
}

/*
 * Tweak the gadgets in a gadget template to be aesthetically pleasing for
 * both V2.0 and pre-V2.0 AmigaDOS.
 */
 
static void TweakWindowGadgets(GadgetTemplate *gadgets)
{
	register WORD width = arrowWidth;
	register WORD height = arrowHeight;
	
	TweakWindowArrow(&gadgets[LEFT_ARROW]);
	TweakWindowArrow(&gadgets[RIGHT_ARROW]);
	TweakWindowArrow(&gadgets[UP_ARROW]);
	TweakWindowArrow(&gadgets[DOWN_ARROW]);
	
	gadgets[UP_ARROW].TopOffset -= 2*height;
	gadgets[DOWN_ARROW].TopOffset -= height;
	gadgets[LEFT_ARROW].LeftOffset -= 2*width;
	gadgets[RIGHT_ARROW].LeftOffset -= width;
	
	gadgets[VERT_SCROLL].LeftOffset = 1 - width;
	gadgets[VERT_SCROLL].WidthOffset = width;
	gadgets[VERT_SCROLL].HeightOffset -= (3*height);

	gadgets[HORIZ_SCROLL].LeftOffset = leftEdge;
	gadgets[HORIZ_SCROLL].WidthOffset = -leftEdge - (3*width);	
	gadgets[HORIZ_SCROLL].TopOffset = 1 - height;
	gadgets[HORIZ_SCROLL].HeightOffset = height;

	gadgets[SCROLL_LIST].WidthOffset -= width;
	gadgets[SCROLL_LIST].HeightOffset -= ARROW_HEIGHT;	/* Seems to work better */
		
	if( intuiVersion >= OSVERSION_2_0 ) {
		gadgets[SCROLL_LIST].WidthOffset ++;
		gadgets[VERT_SCROLL].TopOffset	+= 1;
		gadgets[VERT_SCROLL].HeightOffset-= 2;
		gadgets[VERT_SCROLL].LeftOffset	+= 4;
		gadgets[VERT_SCROLL].WidthOffset -= 8;
		gadgets[HORIZ_SCROLL].LeftOffset += 2;
		gadgets[HORIZ_SCROLL].TopOffset	+= 2;
		gadgets[HORIZ_SCROLL].WidthOffset-= 4;
		gadgets[HORIZ_SCROLL].HeightOffset -= 4;
	}
}

/*
 * Adjust arrow & prop gadgets of edit window for 1.3/2.0 differences.
 */
 
static void TweakEditGadgets()
{
	register WORD width = arrowWidth;
	register WORD height = arrowHeight;
	register GadgetTemplate *gadgets = editGadgets;
	register GadgetTemplate *gadgTempl;
	register BOOL isNewLook = intuiVersion >= OSVERSION_2_0;
	
	gadgTempl = &gadgets[EDIT_UP_ARROW];
	gadgTempl->LeftOffset	= -(width+1+2);
	gadgTempl->TopOffset		= -(1+(2*height));
	gadgTempl->WidthOffset	= width;
	gadgTempl->HeightOffset	= height;
	
	gadgTempl = &gadgets[EDIT_DOWN_ARROW];
	gadgTempl->LeftOffset	= -(width+1+2);
	gadgTempl->TopOffset		= -(1+height);
	gadgTempl->WidthOffset	= width;
	gadgTempl->HeightOffset	= height;

	gadgTempl = &gadgets[EDIT_VERT_SCROLL];
	gadgTempl->LeftOffset	= -(width+3);
	gadgTempl->WidthOffset	= width;
	gadgTempl->HeightOffset	= -(5+(2*height));
	if( isNewLook ) {
/*		gadgTempl->TopOffset		-= 1;*/
		gadgTempl->LeftOffset	+= 1;
		gadgTempl->WidthOffset	-= 2;
		gadgTempl->HeightOffset	-= 1;
	}
	
	gadgTempl = &gadgets[EDIT_LEFT_ARROW];
	TweakWindowArrow(gadgTempl);
	gadgTempl->LeftOffset = 1-(2*width);
	gadgTempl->TopOffset = 0;
	
	gadgTempl = &gadgets[EDIT_RIGHT_ARROW];
	TweakWindowArrow(gadgTempl);
	gadgTempl->TopOffset = 0;
	
	gadgTempl = &gadgets[EDIT_HORIZ_SCROLL];
	gadgTempl->WidthOffset = -(1+(2*width));
	gadgTempl->HeightOffset = height - 2;
	
	gadgTempl = &gadgets[EDIT_SCROLL_LIST];
	gadgTempl->WidthOffset -= width;
}	

/*
 * Tweak the pixels of an arrow gadget
 */
 
static void TweakWindowArrow(register GadgetTemplate *arrowGadget)
{
	arrowGadget->LeftOffset = 1 - arrowWidth;
	arrowGadget->TopOffset = 1 - arrowHeight;
	arrowGadget->WidthOffset = arrowWidth;
	arrowGadget->HeightOffset = arrowHeight;
}

/*
 *	Open initial files, or open new window if none given
 */

void SetUp(int argc, char *argv[])
{
	register WORD i, numFiles;
	struct WBArg *wbArgList;
	register TextPtr fileName;
	Dir dir, startupDir;
	
/*
	Get number of files to open
*/
	if (argc) {					/* Running under CLI */
		numFiles = argc;
		wbArgList = NULL;
	} else {						/* Running under Workbench */
		numFiles = WBenchMsg->sm_NumArgs;
		wbArgList = WBenchMsg->sm_ArgList;
	}
	startupDir = DupLock(GetCurrentDir());
/*
	Open files
	First check to see if this is a prefs file, if so then load prefs
*/
	for(i = 1; i < numFiles; i++) {
		if( wbArgList == NULL ) {
			fileName = argv[i];
			if( *fileName == '-' ) {	/* Ignore program options */
				continue;
			} else {
				SetCurrentDir(startupDir);	/* Path is relative to startupDir */
				dir = ConvertFileName(fileName);
			}
		} else {							/* Running under Workbench */
			fileName = wbArgList[i].wa_Name;
			dir = DupLock(wbArgList[i].wa_Lock);
		}
		LoadFileOrDrawer(fileName, dir);
	}
	UnLock(startupDir);
}

/*
 *	Shut down program
 *	Close backWindow, close libraries, and quit
 */

void ShutDown()
{
	register struct Message *msg;

	if( backWindow != NULL ) {
		SetStdPointer(backWindow, POINTER_WAIT);
	}
	if( cmdWindow != NULL ) {
		RemoveCurrentWindow(cmdWindow);
	}
	if( smallFont != NULL ) {
		CloseFont(smallFont);
	}
	ShutDownIcons();
	ShutDownRexx();
	
	FreeModeImages();
	UnPatchGiveUnit();
	
	if (backWindow) {
		ClearSysRequestWindow();
		ClearMenuStrip(backWindow);
		WaitVisitor();
		CloseWindowSafely(backWindow, mainMsgPort);
	}

	if( menuStrip )
		DisposeMenuStrip(menuStrip);
	if (screen)
		DisposeScreen(screen);

	if( strPath ) {
		FreeMem(strPath, PATHSIZE);
	}
	if( strSavePath ) {
		FreeMem(strSavePath, PATHSIZE);
	}
/*
	Now free my temporary FileInfoBlock
*/
	if (fib != NULL) {
		if( intuiVersion >= OSVERSION_2_0 ) {
			FreeDosObject(DOS_FIB, fib);
		} else {
			FreeMem(fib, sizeof(struct FileInfoBlock));
		}
	}
	if( deviceList != NULL ) {
		DisposeList(deviceList);
	}
	if( volumeList != NULL ) {
		DisposeList(volumeList);
	}
/*
 * Perform pre-2.0 keyboard deallocation...
 */
	if( keyRequest != NULL ) {
		CloseDevice((IOReqPtr) keyRequest);
		DeleteExtIO((IOReqPtr) keyRequest);
	}
	if( keyPort != NULL ) {
		DeletePort(keyPort);
	}
/*
	Delete remaining ports...
*/
	if (mainMsgPort) {
		while( (msg = GetMsg(mainMsgPort)) != NULL)
			ReplyMsg(msg);
		DeletePort(mainMsgPort);
	}
	StdShutDown();
}

/*
 * Read all QB-specific tooltype parameters
 */
 
static void ReadQBOptions(int argc, char *argv[])
{
	register WORD i;
	register TextPtr text;
	struct DiskObject *icon;
	struct WBStartup *wbMsg;
	
	if (argc) {					/* Running under CLI */
		for (i = 1; i < argc; i++) {
			text = argv[i];
			if (*text++ == '-')
				DoQBOption(text);
		}
	} else {						/* Running under Workbench */
		wbMsg = (struct WBStartup *) argv;
		if ((icon = GetDiskObject(wbMsg->sm_ArgList->wa_Name)) != NULL) {
			if (icon->do_ToolTypes) {
				for (i = 0; (text = icon->do_ToolTypes[i]) != NULL; i++)
					DoQBOption(text);
			}
			FreeDiskObject(icon);
		}
	}
}

/*
 * Initialize QB-specific tooltype parameters.
 */
 
static void DoQBOption(TextPtr text)
{
	if( MatchOptions(text, "BlockAccess", "ba")) {
		blockAccess = TRUE;
	}
}

/*
 *	Match screen options
 */

static BOOL MatchOptions(register TextPtr text, TextPtr opt1, TextPtr opt2)
{
	register WORD len;
/*
	Get command length
*/
	for (len = 0; text[len] && text[len] != ':' && text[len] != '='; len++) ;
	if (text[len])
		len++;
/*
	Check for match
*/
	return ((BOOL) (CmpString(text, opt1, len, strlen(opt1), FALSE) == 0 ||
					CmpString(text, opt2, len, strlen(opt2), FALSE) == 0));
}