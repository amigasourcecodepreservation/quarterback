/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Horizontal scroll operations
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	Local prototypes
 */

static WORD CalcListWidth(void);
static void AdjustHScrollBar(void);
static void DoAdjustHScrollBar(void);
static void TrackHSlider(WindowPtr, WORD);

/*
 *	External variables
 */

extern WORD				mode;
extern UWORD			charWidth;
extern LONG				xoffset, maxWidth;
extern VolParams 		currVolParams;
extern ScrollListPtr		scrollList;
extern WindowPtr		cmdWindow;
extern BOOL				altKey;
extern GadgetPtr		currGadgList;
extern EditStats		editStats;
extern RecoverStats		recoverStats;
extern UWORD			tabbing[];
extern ImagePtr		iconList[];

/*
 *	Local variables
 */

static GadgetPtr		trackHProp;

/*
 *	Returns the width in pixels of the widest item in the scroll list
 */

void RecalcMaxWidth()
{
	register WORD len;
	register LONG numItems;
	register LONG count;
	register ULONG max = 0;
	TextPtr str;
	
	switch(mode) {
	case MODE_REPAIR:
		numItems = SLNumItems(scrollList);
		for( count = 0; count < numItems; count++) {
			str = GetListItem(scrollList->List, count) + 1;
			len = TextLength(cmdWindow->RPort, str, strlen(str));
			max = MAX(max, len);
		}
		break;
	case MODE_RECOVER:
		max = NextDirItem((DirFibPtr)&recoverStats.CurFib->df_Child) ? 
			tabbing[0] + tabbing[1] + tabbing[2] + tabbing[3] + tabbing[4] + tabbing[5] + charWidth +
			iconList[DRAWER_ICON]->Width + (iconList[CHECK_ICON_MASK]->Width >> 2) : 0;
		break;
	case MODE_EDIT:
		max = currVolParams.MaxBlocks;
		break;
	}
	maxWidth = max;
}

/*
 * Calculate the scroll list domain.
 */
 
static WORD CalcListWidth()
{
	register WORD listWidth;
	
	switch(mode) {
	case MODE_REPAIR:
	default:
		listWidth = cmdWindow->Width + scrollList->ListBox->Width-2;
		break;
	case MODE_EDIT:
		listWidth = 1;
		break;
	}
	return(listWidth);
}

/*
 *	Scroll left
 *	Called by TrackGadget()
 */

void ScrollLeft(WindowPtr window, WORD gadgNum)
{
	if( xoffset ) {
		switch(mode) {
		case MODE_REPAIR:
		default:
			if (xoffset >= charWidth) {
				SLHorizScroll(scrollList, -charWidth);
				xoffset -= charWidth;
			} else {
				SLHorizScroll(scrollList, -xoffset);
				xoffset = 0;
			}
			break;
		case MODE_EDIT:
			ResetCursor();
			xoffset--;
			GetEditBlock(FALSE);
			MotorOff();
			break;
		}
		DoAdjustHScrollBar();
	}
}

/*
 *	Scroll right
 *	Called by TrackGadget()
 */

void ScrollRight(WindowPtr window, WORD gadgNum)
{
	register WORD newOffset;
	register WORD listWidth = CalcListWidth();
	register LONG width;

	switch(mode) {
	case MODE_REPAIR:
	default:
		width = maxWidth - listWidth;
		if( width - xoffset ) {
			newOffset = xoffset + charWidth;
			if (maxWidth > listWidth) {
				if (newOffset < width) {
					SLHorizScroll(scrollList, charWidth);
					xoffset += charWidth;
				} else {
					newOffset = width-xoffset;
					SLHorizScroll(scrollList, newOffset);
					xoffset += newOffset;
				}
				AdjustHScrollBar();
			}
		}
		break;
	case MODE_EDIT:
		if( xoffset < (maxWidth-1) ) {
			ResetCursor();
			xoffset++;
			GetEditBlock(FALSE);
			MotorOff();
			DoAdjustHScrollBar();
		}
		break;
	}
}

/*
 *	Track horizontal slider
 *	Called by TrackGadget()
 */

static void TrackHSlider(WindowPtr window, WORD gadgNum)
{
	AdjustToHorizScroll(altKey, FALSE);
	
}

/*
 *	Adjust horizontal scroll bar setting
 */

static void AdjustHScrollBar()
{
	ULONG totAmount = maxWidth;
	UWORD listWidth = CalcListWidth();
	PropInfoPtr propInfo;
	register ULONG newPot;
	ULONG newBody;
	register ULONG temp;
	ULONG temp2;
	register UWORD extra;
	
	if( !(trackHProp->Flags & SELECTED) ) { 
		if( totAmount < listWidth + xoffset ) {
			totAmount = listWidth + xoffset;
		}
		if( totAmount == listWidth ) {
			newPot = 0xFFFF;
		} else {
			temp = xoffset;
			newPot = 0xFFFFL * (temp & 0xFFFFL);
			temp2 = 0xFFFFL * (temp >> 16);
			extra = temp2 >> 16;
			if( (temp2 & 0xFFFF) + (newPot >> 16) > 0xFFFFL ) {
				++extra;
			}
			newPot += (temp2 & 0xFFFFL) << 16;
			temp = totAmount - listWidth;
			while( extra ) {
				newPot >>= 1;
				if( extra & 1 ) {
					newPot |= 0x80000000;
				}
				extra >>= 1;
				temp >>= 1;
			}
			newPot /= temp;
		}
		if( newPot & 0xFFFF0000 ) {
			newPot = 0xFFFF;
		}
		if( !(newBody = (0xFFFFL*listWidth)/((UWORD)totAmount)) ) {
			newBody++;
		}
		propInfo = (PropInfoPtr) trackHProp->SpecialInfo;
		if( (propInfo->HorizPot != newPot) || (propInfo->HorizBody != newBody) ) {
			NewModifyProp(trackHProp, cmdWindow, NULL, propInfo->Flags, newPot, 0, newBody, 0xFFFF, 1);
		}
	}
}

/*
 * Adjust the horizontal scroll bar and then, if in edit mode,
 * enable/disable the scroll arrows for it.
 */
 
static void DoAdjustHScrollBar()
{
	GadgetPtr firstGadget;
	
	AdjustHScrollBar();
	if( mode == MODE_EDIT ) {
		firstGadget = GadgetItem(currGadgList, EDIT_LEFT_ARROW);
		EnableGadgetItem(currGadgList, GadgetNumber(firstGadget), cmdWindow, NULL, xoffset > 0 );
		EnableGadgetItem(currGadgList, GadgetNumber(firstGadget->NextGadget), cmdWindow, NULL, xoffset < (maxWidth-CalcListWidth()) );
	}
}

/*
 *	Adjust offset to current scroll bar setting
 */

void AdjustToHorizScroll(BOOL updateBlock, BOOL alwaysUpdate)
{
	LONG totAmount;
	register WORD listWidth;
	PropInfoPtr propInfo = (PropInfoPtr) trackHProp->SpecialInfo;
	register BOOL retry = FALSE;
	register LONG currPos;
	register ULONG offset;
	ULONG temp, temp2;
	UWORD extra, shift;
	
	do {
		totAmount = maxWidth;
		listWidth = CalcListWidth();
		currPos = xoffset;
		if( totAmount < listWidth + currPos ) {
			totAmount = listWidth + currPos;
		}
		temp = totAmount - listWidth;
		offset = propInfo->HorizPot * (temp & 0xFFFFL);
		temp2 = propInfo->HorizPot * (temp >> 16);
		extra = temp2 >> 16;
		if( (temp2 & 0xFFFF) + (offset >> 16) > 0xFFFFL ) {
			++extra;
		}
		offset += (temp2 & 0xFFFFL) << 16;
		if( offset > 0xFFFF8000 ) {
			extra += 1;
		}
		offset += 0x7FFF;
		shift = 0;
		while( extra ) {
			offset >>= 1;
			if( extra & 1 ) {
				offset |= 0x80000000;
			}
			extra >>= 1;
			shift++;
		}
		offset /= 0xFFFFL;
		offset <<= shift;
		if( propInfo->HorizPot == MAXPOT ) {
			offset = maxWidth-listWidth;
		}
/*
	The variable "offset" now contains the longword offset, but there may be
	additional bits of information in "extra".
*/ 		
		if( (currPos != offset) || alwaysUpdate ) {
			alwaysUpdate = FALSE;		/* Subsequent retries must earn it! */
			offset -= currPos;
			xoffset += offset;
			
			switch( mode ) {
			case MODE_EDIT:
				if( updateBlock ) {
					ResetCursor();
					GetEditBlock(FALSE);
/*
	Turn the motor off if this is not our first time through and we're successful.
*/
					if( retry ) {
						updateBlock = FALSE;
						if( retry == editStats.BlockBufferValid ) {
							MotorOff();
						}
					}
					retry = !editStats.BlockBufferValid;
				} else {
					RefreshEditLabel();
					retry = FALSE;
				}
				break;
			case MODE_REPAIR:
			case MODE_RECOVER:
			default:
				SLHorizScroll(scrollList, offset);
				break;
			}
		} else {
			retry = FALSE;
		}
	} while( retry );
}

/*
 *	Handle gadget down/up on horizontal scroll list gadgets
 *	This routine will reply to the message
 *	firstGadget - the left arrow of the HScroll bar
 *		Gadgets are assumed to be in position of LEFT_ARROW, RIGHT_ARROW, SCROLL_BAR
 */

void HandleHorizScroll(MsgPortPtr msgPort, IntuiMsgPtr intuiMsg, GadgetPtr firstGadget)
{
	register GadgetPtr gadget = (GadgetPtr) intuiMsg->IAddress;
	WindowPtr window = intuiMsg->IDCMPWindow;
	ULONG class = intuiMsg->Class;

	altKey = (intuiMsg->Qualifier & ALTKEYS) != 0;

	ReplyMsg((MsgPtr) intuiMsg);
	if( window == cmdWindow ) {
		switch (class) {
		case GADGETDOWN:
			trackHProp = firstGadget->NextGadget->NextGadget;
			if( gadget == firstGadget ) {
				TrackGadget(msgPort, window, gadget, ScrollLeft);
			} else if( gadget == firstGadget->NextGadget)
				TrackGadget(msgPort, window, gadget, ScrollRight);
			else if( gadget == trackHProp )
				TrackGadget(msgPort, window, gadget, TrackHSlider);
			break;
		case GADGETUP:
			if( gadget == trackHProp ) {
				altKey = FALSE;				/* Cause SetEditMenu() to be called */
				AdjustToHorizScroll(TRUE, mode == MODE_EDIT);
			} else {
				AdjustHScrollBar();
			}
			if( mode == MODE_EDIT ) {
				MotorOff();
			}
/*
	We COULD just bypass this testing for the edit window, but disabling the
	horizontal scroll gadgets on a report scroll list is not aesthetic or as necessary.
*/
			if( mode == MODE_EDIT ) {
				EnableGadgetItem(currGadgList, GadgetNumber(firstGadget), window, NULL, xoffset > 0 );
				EnableGadgetItem(currGadgList, GadgetNumber(firstGadget->NextGadget), window, NULL, xoffset < (maxWidth-CalcListWidth()) );
				SetEditMenu();
			}
			break;
		}
	}
}

/*
 *	Make the HScroll bar represent the scroll list
 *	firstGadget - the left arrow of the HScroll bar
 * ASSUMES PROP IS THE SECOND GADGET AFTER THE LEFT ARROW!
 */

void UpdateHScrollBar(GadgetPtr firstGadget)
{
	trackHProp = firstGadget->NextGadget->NextGadget;
	DoAdjustHScrollBar();
}
