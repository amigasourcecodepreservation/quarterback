/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	ProWrite print record
 *	Copyright (c) 1990 New Horizons Software, Inc.
 */

#define PRINTHANDLER_VERSION	1

#ifndef GRAPHICS_GFX_H
#include <graphics/gfx.h>
#endif

#ifndef EXEC_IO_H
#include <exec/io.h>
#endif

enum {								/* Page Size */
	PRT_CUSTOM,
	PRT_USLETTER,	PRT_USLEGAL,
	PRT_A4LETTER,	PRT_WIDECARRIAGE
};

enum {								/* Orientation */
	PRT_PORTRAIT,	PRT_LANDSCAPE
};

enum {								/* Quality */
	PRT_DRAFT,		PRT_NLQ,	PRT_GRAPHIC,	PRT_POSTSCRIPT
};

enum {								/* Paper Feed */
	PRT_CONTINUOUS,	PRT_CUTSHEET
};

/*
 *	Flags
 */

#define PRT_ASPECTADJ		0x0001
#define PRT_NOGAPS			0x0002	/* No gaps between pages */
#define PRT_SMOOTH			0x0004	/* Smoothing on */
#define PRT_PICTURES		   0x0008	/* Pictures with character print */
#define PRT_BACKTOFRONT		0x0010	/* Print back to front */
#define PRT_COLLATE			0x0020	/* Print in collated order */
#define PRT_ODDEVEN			0x0040	/* Print separate odd and even pages */
#define PRT_OPTSPACE		   0x0080	/* Optimal spacing */
#define PRT_FONTS			0x0100	/* Bit-map fonts with character print */
#define PRT_TOFILE			0x0200	/* Destination is file */

enum {								/* Print Pitch */
	PRT_PICA,	PRT_ELITE,	PRT_CONDENSED
};

enum {								/* Print Spacing */
	PRT_SIXLPI,	PRT_EIGHTLPI
};

typedef union  {
	struct IOStdReq		Std;
	struct IODRPReq		DRP;
	struct IOPrtCmdReq	Cmd;
} PrintIO, *PrintIOPtr;

typedef struct PrintRecord {
	WORD		Version;				/* Version number of record */
	Rectangle	PaperRect, PageRect;	/* Upper left of PaperRect is (0,0) */
	WORD		xDPI, yDPI;

	UWORD		PaperWidth, PaperHeight;	/* In decipoints */

	WORD		Copies, FirstPage, LastPage;

	UBYTE		PageSize;				/* From defines above */
	UBYTE		Orientation;
	UWORD		XScale, YScale;		/* Enlargement/reduction * 100 */
	UBYTE		PrintDensity;
	UBYTE		Quality;
	UBYTE		PaperFeed;
	UBYTE		FontNum;					/* Printer font number */
	UWORD		Flags;
	UBYTE		PrintPitch;				/* Not used by ProWrite */
	UBYTE		PrintSpacing;			/* Not used by ProWrite */
	UBYTE		Depth;					/* Not used by Flow */
	UBYTE		pad[120-103];			/* Pad to 120 bytes */
	TextChar FileName[32];			/* Print to file capability */
	Dir		FileDir;					/* Print to file capability */
	RastPtr		RPort;
	PrintIOPtr	PrintIO;
	ColorMapPtr	ColorMap;
	WORD		Top, Left, Width, Height;
} *PrintRecPtr;

#ifndef c_plusplus
typedef struct PrintRecord PrintRecord;
#endif
