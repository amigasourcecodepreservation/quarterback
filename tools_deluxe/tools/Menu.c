/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Menu operations
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/intuition.h>

#include <Toolbox/Menu.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr		backWindow, cmdWindow;
extern struct RexxLib  *RexxSysBase;
extern WORD				mode, action;
extern OptionsRec		options;
extern VolParams		currVolParams;
extern EditStats		editStats;
extern UBYTE			*blockBuffer;
extern ULONG			xoffset;
extern ScrollListPtr		scrollList;

/*
 *	A function for turning menus on or off
 */

void OnOffMenu(WindowPtr window, UWORD menuNum, BOOL menuOn)
{
	if (menuOn) {
		OnMenu(window, menuNum);
	} else {
		OffMenu(window, menuNum);
	}
}

/*
 * Set all of the menu items
 */
 
void SetAllMenus()
{
	SetProjectMenu();
	SetToolsMenu();
	if( mode == MODE_EDIT ) {
		SetEditMenu();
	}
	OnOffMenu(backWindow, MENUITEM(MACRO_MENU, NOITEM, NOSUB), (BOOL) RexxSysBase);
}

/*
 * Set project menu
 */
 
void SetProjectMenu()
{
	register BOOL inReportTool = IsReportMode();
	register WindowPtr window = cmdWindow;
	
	OnOffMenu(window, MENUITEM(PROJECT_MENU, SAVEAS_ITEM, 0), inReportTool);
	OnOffMenu(window, MENUITEM(PROJECT_MENU, PAGESETUP_ITEM, 0), inReportTool);
	OnOffMenu(window, MENUITEM(PROJECT_MENU, PRINT_ITEM, 0), inReportTool);
	OnOffMenu(window, MENUITEM(PROJECT_MENU, SETTINGS_ITEM, LOADSETTINGS_SUBITEM), (action == OPER_READY) || (action == OPER_DONE));
}

/*
 *	Set Tools Menu
 */

void SetToolsMenu()
{
	register BOOL menuFlag;
	register WindowPtr window = cmdWindow;
	register WORD currMode = mode;
	
	if( window != NULL && ((window = backWindow) != NULL) ) {
		if( currMode == MODE_TOOLS ) {
			OnOffMenu( window, MENUITEM(TOOLS_MENU, VOL_STAT_ITEM, 0), 
				menuFlag = (scrollList != NULL) && (SLNumItems(scrollList) != 0) );
		} else {
			menuFlag = (action == OPER_DONE) || (action == OPER_READY);
		}
		OnOffMenu(window, MENUITEM(TOOLS_MENU, TOOLS_SELECT_ITEM, 0), menuFlag);
		CheckMenu(window, MENUITEM(TOOLS_MENU, TOOLS_SELECT_ITEM, 0), currMode == MODE_TOOLS);
		OnOffMenu(window, MENUITEM(TOOLS_MENU, VOL_REPAIR_ITEM, 0), menuFlag);
		CheckMenu(window, MENUITEM(TOOLS_MENU, VOL_REPAIR_ITEM, 0), currMode == MODE_REPAIR);
		OnOffMenu(window, MENUITEM(TOOLS_MENU, VOL_RECOVER_ITEM, 0), menuFlag);
		CheckMenu(window, MENUITEM(TOOLS_MENU, VOL_RECOVER_ITEM, 0), currMode == MODE_RECOVER);
		OnOffMenu(window, MENUITEM(TOOLS_MENU, VOL_OPTIMIZE_ITEM, 0), menuFlag);
		CheckMenu(window, MENUITEM(TOOLS_MENU, VOL_OPTIMIZE_ITEM, 0), currMode == MODE_OPTIMIZE);
		OnOffMenu(window, MENUITEM(TOOLS_MENU, VOL_EDITOR_ITEM, 0), menuFlag);
		CheckMenu(window, MENUITEM(TOOLS_MENU, VOL_EDITOR_ITEM, 0), currMode == MODE_EDIT);
	}
}

/*
 * Set Edit Menu
 */

void SetEditMenu()
{
	register BOOL notBitMapKey = !IsBitMapKey(xoffset);
	register BOOL validADOS = (!currVolParams.NonFSDisk) && editStats.BlockBufferValid;
	register ULONG key;
	register RootBlockEndPtr rootBlockEndPtr = (RootBlockEndPtr)(blockBuffer + currVolParams.BlockSize - sizeof(RootBlockEnd));
	register WindowPtr window = cmdWindow;
	
	OnOffMenu(window, MENUITEM(EDIT_MENU, FIND_NEXT_ITEM, 0), (editStats.FindLen != 0) && ((editStats.FindCurrBlock != editStats.FindEndBlock) || (editStats.FindCurrOffset != editStats.FindEndOffset)) );

	OnOffMenu(window, MENUITEM(EDIT_MENU, CALC_HASH_ITEM, 0), validADOS &&
		notBitMapKey &&
		(*((ULONG *)blockBuffer) == T_SHORT) );
	OnOffMenu(window, MENUITEM(EDIT_MENU, CALC_CKSUM_ITEM, 0), validADOS &&
		IsChecksumableBlock((ULONG *)blockBuffer) );
	OnOffMenu(window, MENUITEM(EDIT_MENU, AUTOCALC_CKSUM_ITEM, 0), !currVolParams.NonFSDisk);
	CheckMenu(window, MENUITEM(EDIT_MENU, AUTOCALC_CKSUM_ITEM, 0), options.EditOpts.AutoChecksum);
	
	OnOffMenu(window, MENUITEM(EDIT_MENU, CAPTURE_CREATE_ITEM, 0), editStats.CaptureFH == NULL );
	OnOffMenu(window, MENUITEM(EDIT_MENU, CAPTURE_WRITE_ITEM, 0), editStats.BlockBufferValid && (editStats.CaptureFH != NULL) );
	OnOffMenu(window, MENUITEM(EDIT_MENU, CAPTURE_CLOSE_ITEM, 0), editStats.CaptureFH != NULL);
	
	OnOffMenu(window, MENUITEM(EDIT_MENU, GOTO_PREV_ITEM, 0), editStats.HistoryBlock != -1);
	OnOffMenu(window, MENUITEM(EDIT_MENU, GOTO_FIELD_ITEM, GOTO_ROOT_SUBITEM), !currVolParams.NonFSDisk);
	OnOffMenu(window, MENUITEM(EDIT_MENU, GOTO_FIELD_ITEM, GOTO_BITMAP_SUBITEM), (editStats.BitMapKey != 0xFFFFFFFF) && (CheckKey(editStats.BitMapKey)) );

	OnOffMenu(window, MENUITEM(EDIT_MENU, GOTO_FIELD_ITEM, GOTO_HASH_SUBITEM), validADOS &&
		notBitMapKey &&
		(rootBlockEndPtr->HashChain != 0L) &&
		CheckKey(rootBlockEndPtr->HashChain) );	
	OnOffMenu(window, MENUITEM(EDIT_MENU, GOTO_FIELD_ITEM, GOTO_PARENT_SUBITEM), validADOS &&
		notBitMapKey &&
		(rootBlockEndPtr->Parent != 0L) &&
		CheckKey(rootBlockEndPtr->Parent) );
	
	OnOffMenu(window, MENUITEM(EDIT_MENU, GOTO_FIELD_ITEM, GOTO_EXT_SUBITEM), validADOS &&
		((IsBitMapExtKey(xoffset) && (rootBlockEndPtr->SecType != 0L)) ||
		((rootBlockEndPtr->Ext != 0L) && CheckKey(rootBlockEndPtr->Ext))) );
	
	key = ((OldDataBlkPtr)blockBuffer)->Hdrkey;
	OnOffMenu(window, MENUITEM(EDIT_MENU, GOTO_FIELD_ITEM, GOTO_HEADER_SUBITEM), validADOS &&
		notBitMapKey &&
		(!currVolParams.FFSFlag) &&
		(key != 0L) && (key != xoffset) && CheckKey(key) );
	key = ((OldDataBlkPtr)blockBuffer)->NextData;
	OnOffMenu(window, MENUITEM(EDIT_MENU, GOTO_FIELD_ITEM, GOTO_DATA_SUBITEM), validADOS &&
		notBitMapKey &&
		( (((OldDataBlkPtr)blockBuffer)->Type == T_SHORT) || (((OldDataBlkPtr)blockBuffer)->Type == T_DATA) ) &&
		(key != 0L) && CheckKey(key) );
}
