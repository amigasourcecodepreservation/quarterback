/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Quarterback Tools
 * Copyright (c) 1992 New Horizons Software, Inc.
 *
 * Project menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <rexx/errors.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>


#include <Toolbox/Dialog.h>
#include <Toolbox/DOS.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Image.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>

#include "Tools.h"
#include "Proto.h"

/*
 * External variables
 */

extern DialogTemplate _sfpPutFileDlgTempl;
extern TextChar	_strYes[], _strNo[];

extern BOOL			quitFlag, abortFlag, closeFlag, decimalLabel;
extern WORD			mode, action, stage;
extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;
extern TextPtr		strsErrors[];
extern TextChar	strPrefsName[], strSaveAsDefaults[], strLoadDefaults[];
extern TextChar	strBuff[], strSaveCatalog[], strSaveLog[], strReportName[];
extern DlgTemplPtr	dlgList[];
extern WindowPtr	cmdWindow;
extern OptionsRec	options;
extern GadgetPtr	currGadgList;

/*
 *	Local Variables and definitions
 */

enum {
	PREFSRESET_BUTTON = 2,
	HEX_RADBTN,
	DEC_RADBTN,
	INTERACTIVE_RADBTN,
	QUIET_RADBTN,
	SAVEICONS_BOX,
	FLASH_BOX,
	BEEP_BOX,
	BYPASS_BOX
};

#define UPDATE_PREFS_BUTTONS	99

#define NUM_PREFS_OPTIONS		(sizeof(prefsOptNames)/sizeof(TextPtr))

static TextPtr prefsOptNames[] = {
	"Decimal",		"Hex",
	"Interactive",	"Quiet",
	"FlashOn",		"FlashOff",
	"BeepOn",		"BeepOff",
	"SaveIconsOn",	"SaveIconsOff",
	"BypassDOSOn",	"BypassDOSOff"
};

enum {
	OPT_DECIMAL,	OPT_HEX,
	OPT_INTERACTIVE,	OPT_QUIET,
	OPT_FLASHON,	OPT_FLASHOFF,
	OPT_BEEPON,		OPT_BEEPOFF,
	OPT_SAVEICONSON,	OPT_SAVEICONSOFF,
	OPT_BYPASSDOSON,	OPT_BYPASSDOSOFF
};

/*
 *	Local prototypes
 */

static BOOL DoLoadDefaults(void);

/*
 * Open the about box, wait for "OK" to be hit, close it, return TRUE if opened OK.
 */

BOOL DoAboutBox()
{
	register DialogPtr dlg;
	register WORD item;
	register RastPtr rastPtr;
	Rectangle rect;
	
	BeginWait();
	if( (dlg = GetDialog(dlgList[DLG_ABOUT], screen, mainMsgPort)) != NULL) {
		OutlineOKButton(dlg);

		GetGadgetRect(GadgetItem(dlg->FirstGadget, ABOX_BORDER), dlg, NULL, &rect);
		rastPtr = BeginAboutBox(dlg, &rect, FALSE);
		do {
			item = CheckDialog(mainMsgPort, dlg, DialogFilter);
			if( (item == -1) && rastPtr != NULL ) {
				NextABoxFrame(rastPtr, dlg->RPort, &rect);
			}
		} while( item != OK_BUTTON );
		EndAboutBox(rastPtr);
		DisposeDialog(dlg);
	}
	EndWait();
	return(dlg != NULL);
}

/*
 * Save selected from menu.
 */
 
BOOL DoSaveAs()
{
	register BOOL success;
	SFReply sfReply;
	
	if( success = IsReportMode() && OpenSaveAsDialog(&sfReply, strReportName) ) {
		UnLock(sfReply.DirLock);
		success = OutputReport(sfReply.Name);
	}
	return(success);
}

/*
 * Given a filename, output the current info to a report, but only if in proper mode.
 */
 
BOOL SaveFileAs(TextPtr fileName)
{
	register BOOL success;
	
	if( success = IsReportMode() ) {
		success = OutputReport(fileName);
	}
	return(success);
}

/*
 * Open the standard file requester for saving either catalog or session log
 */

BOOL OpenSaveAsDialog(SFReplyPtr sfReply, TextPtr defText)
{
	SFPPutFile(screen, mainMsgPort, (mode == MODE_RECOVER) ? strSaveCatalog : strSaveLog, 
		defText, DialogFilter, NULL, /*dlgList[DLG_DESTFILE]*/&_sfpPutFileDlgTempl, sfReply);
	return(sfReply->Result == SFP_OK );
}

/*
 * Page setup
 */

BOOL DoPageSetup()
{
	register BOOL success;

	if( success = IsReportMode() ) {
		success = PageSetupRequest();
	}
	return(success);
}

/*
 * Print project
 */

BOOL DoPrint(WindowPtr window, UWORD modifier, register BOOL printOne)
{
	register PrintRecPtr printRec = &options.PrintInfo.PrintRec;
	register BOOL success = FALSE;
	
	if( IsReportMode() ) {
		if( modifier & ALTKEYS )
			printOne = TRUE;
		printRec->Copies = 1;
		if( printOne ) {
			(void) PrValidate(printRec);
		}
		if( printOne || PrintRequest() ) {
			success = PrintList();						/* Print catalog or logfile */
		}
	}
	return(success);
}

/*
 *	Adjust user preferences
 */

BOOL DoPreferences()
{
	register WORD item;
	register WindowPtr window;
	register BOOL success = FALSE;
	PrefsOptsRec prefsOpts = options.PrefsOpts;
	BOOL sameBase;
/*
	Get dialog
*/
	if( (window = GetDialog(dlgList[DLG_PREFS], screen, mainMsgPort)) != NULL ) {
		OutlineOKButton(window);
/*
	Handle dialog
*/
		item = UPDATE_PREFS_BUTTONS;
		do {
			switch (item) {
			case PREFSRESET_BUTTON:
				SetDefaultPrefs(&prefsOpts);
			case UPDATE_PREFS_BUTTONS:
				GadgetOnOff(window, HEX_RADBTN, !prefsOpts.DecimalLabel);
				GadgetOnOff(window, DEC_RADBTN, prefsOpts.DecimalLabel);
				GadgetOnOff(window, INTERACTIVE_RADBTN, prefsOpts.Interactive == 0);
				GadgetOnOff(window, QUIET_RADBTN, prefsOpts.Interactive != 0);
				GadgetOnOff(window, SAVEICONS_BOX, prefsOpts.SaveIcons);
				GadgetOnOff(window, FLASH_BOX, prefsOpts.Flash);
				GadgetOnOff(window, BEEP_BOX, prefsOpts.Beep);
				GadgetOnOff(window, BYPASS_BOX, prefsOpts.BypassDOS);
				break;
			case HEX_RADBTN:
			case DEC_RADBTN:
				GadgetOff(window, (prefsOpts.DecimalLabel != 0) + HEX_RADBTN);
				GadgetOn(window, item);
				prefsOpts.DecimalLabel = item - HEX_RADBTN;
				break;
			case INTERACTIVE_RADBTN:
			case QUIET_RADBTN:
				GadgetOff(window, (prefsOpts.Interactive != 0) + INTERACTIVE_RADBTN);
				GadgetOn(window, item);
				prefsOpts.Interactive = item - INTERACTIVE_RADBTN;
				break;
			case SAVEICONS_BOX:
				ToggleCheckbox(&prefsOpts.SaveIcons, item, window);
				break;
			case FLASH_BOX:
				ToggleCheckbox(&prefsOpts.Flash, item, window);
				break;
			case BEEP_BOX:
				ToggleCheckbox(&prefsOpts.Beep, item, window);
				break;
			case BYPASS_BOX:	
				ToggleCheckbox(&prefsOpts.BypassDOS, item, window);
				break;
			default:
				break;
			}
			item = ModalDialog(mainMsgPort, window, DialogFilter);
		} while( (item != OK_BUTTON) && (item != CANCEL_BUTTON) );
		success = item == OK_BUTTON;
		DisposeDialog(window);
		if( success ) {
			sameBase = options.PrefsOpts.DecimalLabel == prefsOpts.DecimalLabel;
			options.PrefsOpts = prefsOpts;
			if( mode != MODE_EDIT ) {
				decimalLabel = options.PrefsOpts.DecimalLabel;
				if( (mode == MODE_RECOVER) && stage && (!sameBase) ) {
					BuildFileListDisplay(NULL);
				}
			}
		}
	} else {
		Error(ERR_NO_MEM);
	}
	return(success);
}

/*
 *	Set prefs options of given option name
 *	Used for AREXX macros
 *	Return FALSE if not a valid option name
 */

LONG SetPrefsOption(TextPtr optName)
{
	register WORD i;
	TextChar buff[256];
	UWORD len;
	LONG result = RC_OK;

	do {	
		GetNextWord(optName, buff, &len);
		optName += len;
		for (i = 0; i < NUM_PREFS_OPTIONS; i++) {
			if (CmpString(buff, prefsOptNames[i], strlen(buff), (WORD) strlen(prefsOptNames[i]), FALSE) == 0) {
				break;
			}
		}
		if( i >= NUM_PREFS_OPTIONS ) {
			result = RC_WARN;
		} else {
			switch (i) {
			case OPT_DECIMAL:
			case OPT_HEX:
				options.PrefsOpts.DecimalLabel = i == OPT_DECIMAL;
				break;
			case OPT_INTERACTIVE:
			case OPT_QUIET:
				options.PrefsOpts.Interactive = i == OPT_QUIET;
				break;
			case OPT_FLASHON:
			case OPT_FLASHOFF:
				options.PrefsOpts.Flash = i == OPT_FLASHON;
				break;
			case OPT_BEEPON:
			case OPT_BEEPOFF:
				options.PrefsOpts.Beep = i == OPT_BEEPON;
				break;
			case OPT_SAVEICONSON:
			case OPT_SAVEICONSOFF:
				options.PrefsOpts.SaveIcons = i == OPT_SAVEICONSON;
				break;
			case OPT_BYPASSDOSON:
			case OPT_BYPASSDOSOFF:
				options.PrefsOpts.BypassDOS = i == OPT_BYPASSDOSON;
				break;
			}
		}
	} while(*optName);
	return(result);
}

/*
 *	Save program defaults
 */

static BOOL DoSaveDefaults(WindowPtr window)
{
	BOOL success;
	
	if( success = DoDialog(strsErrors[ERR_SAVE_PREFS], CANCEL_BUTTON, _strYes, _strNo, NULL, FALSE) == OK_BUTTON ) {
/*
	Save settings
*/
		SetWaitPointer();
		success &= SaveProgPrefs(window, NULL);
		SetArrowPointer();
		if( !success ) {
			DOSError(ERR_SAVE, IoErr());
		}
	}
	return(success);
}

/*
 *	Save program defaults with specified file name
 */

static BOOL DoSaveAsDefaults(WindowPtr window)
{
	BOOL success;
	SFReply sfReply;

/*
	Get file name to save
*/
	SFPPutFile(screen, mainMsgPort, strSaveAsDefaults, strPrefsName,
			   DialogFilter, NULL, NULL, &sfReply);
	if (sfReply.Result == SFP_CANCEL)
		return (FALSE);
	UnLock(sfReply.DirLock);
/*
	Save settings
*/
	SetWaitPointer();
	success = SaveProgPrefs(window, sfReply.Name);
	SetArrowPointer();
	if (!success)
		DOSError(ERR_SAVE, IoErr());
	return (success);
}

/*
 *	Load defaults
 */

static BOOL DoLoadDefaults()
{
	SFReply sfReply;
	register BOOL success;
	
	if( success = (action == OPER_READY) || (action == OPER_DONE) ) {
/*
	Get file name of defaults file
*/
		SFPGetFile(screen, mainMsgPort, strLoadDefaults, DialogFilter, NULL,
			   NULL, 0, NULL, IsPrefsFile, &sfReply);
		if (sfReply.Result != SFP_OK) {
			if (sfReply.Result == SFP_NOMEM)
				Error(ERR_NO_MEM);
			return (FALSE);
		}
		UnLock(sfReply.DirLock);
/*
	Load defaults
*/
		success = NewProgPrefs(sfReply.Name);
	}
	return(success);
}

/*
 * Load a file or drawer from the initial invocation or during an AppMsg.
 * Assumes that zero length of fileName indicates directory.
 * Unlocks the directory lock in "dir" at subroutine completion.
 */
 
BOOL LoadFileOrDrawer(TextPtr fileName, Dir dir)
{
	register WORD len;
	BOOL success;
	
	len = strlen(fileName);
	if( success = len <= MAX_FILENAME_LEN ) {
		if( len == 0 ) {
			/*Open a dir/volume here */
		} else {
			DoOpenFile(fileName, dir);
		}
	}
	UnLock(dir);
	return(success);
}

/*
 * At setup time, or when AppIcon/Menu file to be opened, call this.
 */
 
BOOL DoOpenFile(TextPtr fileName, Dir dir)
{
	SetCurrentDir(dir);
	if( !NewProgPrefs(fileName) ) {
		/* try it as another kind of file */
	}
	return(TRUE);
}

/*
 * Return whether the SaveAs and Print menu items are valid in this mode.
 */
 
BOOL IsReportMode()
{
	return( (mode != MODE_TOOLS) && (mode != MODE_OPTIMIZE) && ((mode != MODE_RECOVER) || (stage && (action != OPER_IN_PROG))) );
}

/*
 *	Handle defaults submenu
 */

static BOOL DoDefaultsMenu(WindowPtr window, UWORD item, UWORD modifier)
{
	BOOL success;

	success = FALSE;
	switch (item) {
	case SAVESETTINGS_SUBITEM:
		success = DoSaveDefaults(window);
		break;
	case SAVEASSETTINGS_SUBITEM:
		success = DoSaveAsDefaults(window);
		break;
	case LOADSETTINGS_SUBITEM:
		success = DoLoadDefaults();
		break;
	}
	return (success);
}

/*
 * Process project menu selection
 */

BOOL DoProjectMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifier)
{
	register BOOL success = FALSE;

	switch (item) {
	case ABOUT_ITEM:
		success = DoAboutBox();
		break;
	case SAVEAS_ITEM:
		success = DoSaveAs();
		break;
	case PAGESETUP_ITEM:
		success = DoPageSetup();
		break;
	case PRINT_ITEM:
		success = DoPrint(window, modifier, FALSE);
		break;
	case PREFS_ITEM:
		success = DoPreferences();
		break;
	case SETTINGS_ITEM:
		success = DoDefaultsMenu(window, sub, modifier);
		break;
	case QUIT_ITEM:
		closeFlag = TRUE;
		
		switch(mode) {
		case MODE_REPAIR:
			action = OPER_DONE;
			break;
		case MODE_RECOVER:
			if( action == OPER_IN_PROG ) {
				abortFlag = CheckAbort();
			}
			break;
		case MODE_OPTIMIZE:
			if( action != OPER_READY ) {
				abortFlag = CheckAbort();
			}
			break;
		case MODE_EDIT:
			if( PromptForChangedBlock(TRUE) == CANCEL_BUTTON ) {
				closeFlag = FALSE;
			}
		}
		success = quitFlag = closeFlag;
		break;
	}
	return(success);
}
