/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 * Bitmap manipulation routines.
 */

#include <exec/types.h>

#include "Tools.h"
#include "Proto.h"

/*
 * Extern variables
 */

extern ULONG		*bitMapBadPtr, *bitMapAltPtr, *bitMapDataPtr;
extern BOOL			bitMapFlag, bitMapDirtyFlag;
extern VolParams	currVolParams;
extern WORD			mode, stage;
extern BadBlockStats	badBlockStats;

/*
 * Local prototypes
 */

static BOOL VerifyBitMap(void);
static BOOL	ReadBitMapBlock(ULONG *, ULONG);
static BOOL	GetBitMapStatus(ULONG *, ULONG);
static BOOL	AssignBitMapKeys(void);

/*
 * Allocates a single free key from bitmap.
 * Returns TRUE if successful, and key in LastUsedKey.
 * The code here really is the only code which limits the maximum volume size.  The
 * number of bitmap extensions is the limiting factor (and, possibly, memory limits).
 * Currently, the code supports volumes up to 1.1 GB.
 */
 
BOOL AllocFreeKey()
{
	BOOL success;
	const /*register*/ ULONG numBits = 31;
	register ULONG max;
	register ULONG temp;
	register ULONG bit;
	register ULONG *valPtr;
	register ULONG lastKey;
	BOOL status;
	BOOL cont;
		
	if( success = currVolParams.FreeKeys != 0) {
		lastKey = currVolParams.LastUsedKey;
		max = currVolParams.HighestKey;
		do {
			cont = FALSE;
			temp = lastKey - currVolParams.LowestKey;
			bit = numBits & temp;
			valPtr = bitMapDataPtr + (temp >> 5);
			do {
				temp = *valPtr & (~(1 << bit));		/* Clear bit for this key */
				if( status = (*valPtr != temp) ) {	/* Was the bit set? */
					*valPtr = temp;						/* Yes, so store new result. */
					currVolParams.LastUsedKey = lastKey;
					bitMapDirtyFlag = TRUE;
					--currVolParams.FreeKeys;			/* We can exit now. */
				} else {
					if( ++lastKey > max ) {				/* Bit was already clear */
						lastKey = currVolParams.LowestKey;
						cont = status = TRUE;
					} else {
						if( ++bit > numBits ) {		/* Advance to next bit */
							bit = 0;						/* End of longword */
							valPtr++;					/* Advance to next long */
						}
					}
				}
			} while( !status );
		} while( cont );
	}
	return(success);
}

/*
 * Allocate a set of "*keyCount" bitmap keys, up to "max", beginning at "*bitMapBuf",
 * Returns updated buffer pointer and count, and TRUE if allocated OK.
 */
 
BOOL StoreBitMapKeys(ULONG **bitMapBuf, ULONG *keyCount, ULONG max)
{
	register ULONG limit = *keyCount;
	register BOOL success = TRUE;
	register ULONG *ptr = *bitMapBuf;
	
	max--;
	limit = MIN(limit, max);
	*keyCount -= limit;
	
	while( limit-- && (success = AllocFreeKey()) ) {
		*ptr++ = currVolParams.LastUsedKey;
	}
	*bitMapBuf = ptr;
	return(success);
}

/*
 * Counts actual free keys in bitmap.
 */
 
void CountFreeKeys()
{
	register ULONG counter;
	register ULONG free;
	register ULONG mask;
	register ULONG val;
	register ULONG *ptr;
	
	if( bitMapDataPtr != NULL ) {
		counter = currVolParams.MaxKeys >> 5;
		free = 0;
		ptr = bitMapDataPtr;
/*
	Treat as many as you can as longwords; for efficiency we test for
	0L and -1L rather than just loop through all 32 bits all of the time.
*/
		while( counter-- ) {
			if( val = *ptr++ ) {
				if( val == 0xFFFFFFFF ) {
					free += 32;
				} else {
					for( mask = 0x80000000; mask != 0 ; mask >>= 1 ) {
						if( val & mask ) {
							free++;
						}
					}
				}
			}
		}
/*
	Process last longword now
*/
		counter = currVolParams.MaxKeys & 31;
		mask = 1;
		val = *ptr;
		while( counter-- ) {
			if( val & mask ) {
				++free;
			}
			mask += mask;
		}
		currVolParams.FreeKeys = free;
	}
}
	

/*
 * Allocates bitmap buffer to hold number of bitmap blocks for volume based
 * on size of volume in blocks. Bitmap buffer is always a multiple of block size.
 */
 
BOOL AllocateBitMapBuffers()
{
	register ULONG size;
	register BOOL success;
	
	currVolParams.BitMapBufSize = size = currVolParams.BitMapBlocks << currVolParams.BlockSizeShift;
	if( success = (currVolParams.BitMapBuf = (ULONG *)AllocIOBuffer(size)) != NULL ) {
		currVolParams.BitMapKeyBufSize = size = (currVolParams.BitMapBlocks + currVolParams.BitMapExtBlks) << 2;
		success = (currVolParams.BitMapKeyBuf = (ULONG *)AllocIOBuffer(size)) != NULL;
	}
	return(success);
}

/*
 * Free the alternate bitmap (see Reorganize code).
 */
 
void FreeAltBitMap()
{
	FreeIOBuffer((UBYTE **)&currVolParams.BitMapAltBuf, currVolParams.BitMapBufSize);
}

/*
 * Frees any allocated bitmap buffers.
 */
 
void FreeBitMapBuffers()
{
	FreeIOBuffer((UBYTE **)&currVolParams.BitMapBuf, currVolParams.BitMapBufSize);
	FreeIOBuffer((UBYTE **)&currVolParams.BitMapKeyBuf, currVolParams.BitMapKeyBufSize);
	FreeIOBuffer((UBYTE **)&currVolParams.BitMapBadBuf, currVolParams.BitMapBufSize);
	FreeAltBitMap();
	bitMapFlag = FALSE;
}

BOOL AllocateBadBitMap()
{
	BOOL success;
	
	if( success = (bitMapBadPtr = currVolParams.BitMapBadBuf = (ULONG *)AllocIOBuffer(currVolParams.BitMapBufSize)) != NULL ) {
		InitBitMapBuffer(++bitMapBadPtr);
	}
	return(success);
}

BOOL AllocateAltBitMap()
{
	BOOL success;
	
	if( success = (bitMapAltPtr = currVolParams.BitMapAltBuf = (ULONG *)AllocIOBuffer(currVolParams.BitMapBufSize)) != NULL ) {
		InitBitMapBuffer(++bitMapAltPtr);
	}
	return(success);
}

/*
 * Allocate an empty bitmap. If successful (TRUE), it initializes
 * the bitmap (usually as precursor to repair operation).
 */
 
BOOL AllocateEmptyBitMap()
{
	BOOL success;
	
	FreeBitMapBuffers();
	if( success = AllocateBitMapBuffers() ) {
		currVolParams.FreeKeys = currVolParams.MaxKeys;
		InitBitMapBuffer(bitMapDataPtr = currVolParams.BitMapBuf+1);
	}
	return(success);
}

/*
 *	Reads root block, figures out how big the bitmap will be, allocates
 *	a buffer of the required size, and reads the bitmap into the buffer.  
 *	NOTE: this routine (and the WriteBitmap routine) may have a problem
 *	with the OFS.  The documentation is not clear on whether there are 25 or
 *	26 entries in the bitmap table for the OFS.  The 26th entry for the FFS
 * is the bitmap extension field.  That is how it is treated here.
 * ALSO NOTE: Error requesters are displayed for all error conditions here.
 */
 
BOOL ReadBitMap()
{
	BOOL success = FALSE;

	if( !bitMapFlag ) {
		FreeBitMapBuffers();
		if( currVolParams.BitMapBlocks ) {
			if( AllocateBitMapBuffers() ) {
				if( LoadBitMapKeys() ) {
					if( LoadBitMapData() ) {
						if( !(success = VerifyBitMap()) ) {
							if( mode == MODE_REPAIR ) {
								ReportInfo(ERR_BITMAP_INVALID);
							}
						}
					}
				}
			} else {
				OutOfMemory();
			}
		}
	}
	return(success);
}

/*
 * Writes existing bitmap buffers back to volume. Note: bitmap blocks are
 * rewritten into their previous keys. They are NOT moved to different keys,
 * as old ADOS does.
 */
 
BOOL WriteBitMap()
{
	register UWORD num = currVolParams.BitMapBlocks;
	register ULONG *bitMapPtr = bitMapDataPtr;
	register ULONG *bitMapBuf = currVolParams.BitMapKeyBuf;
	register const ULONG size = currVolParams.BlockSize;
	register BYTE err = 0;
	register LONG key;
	
	while( (num--) && (err == 0) ) {
		if( key = *bitMapBuf++ ) {					/* Get bitmap key */
			if( key > 0 ) {							/* Ignore extension key */
				bitMapPtr--;							/* Backup for checksum field */
				*bitMapPtr -= CalcBlockChecksum(bitMapPtr);
				if( (err = WriteBlock(key, bitMapPtr, FALSE)) == CERR_NOERR ) {
					bitMapPtr = (ULONG *) (((UBYTE *)bitMapPtr) + size);
				} else {
					err = ERR_WRITE_BITMAP;
				}
			}
		} else {
			err = ERR_BAD_BITMAP;					/* Something clobbered key list */
		}
	}
	bitMapFlag = FALSE;								/* Bitmap no longer valid */
	if( err ) {
		ReportInfo(err);
	} else {
		bitMapDirtyFlag = FALSE;
	}
	return(err == 0);
}

/*
 * Builds list of bitmap keys from root and any bitmap extension blocks.  NOTE:
 * this code uses a dirty little stunt to mark BitmapExtBlk keys, which are stored
 * in the BitmapKeyBuffer along with bitmap block keys.  The high bit it SET to
 * mark extension block keys.  This way they are allocated and stored along with
 * the bitmap block keys, providing an essentially unlimited volume size capability.
 * Using the high bit this way limits us to about 2 billion bitmap block keys...(sigh).
 * From Glen: I think George means 1 billion.
 */
 
BOOL LoadBitMapKeys()
{
	register BOOL success;
	RootBlockEndPtr rootBlockEnd;
	register ULONG *bitMapKeyPtr;
	register ULONG *keyBufPtr;
	register ULONG numKeys;
	register UWORD blocks;
	register ULONG key;
	UWORD saveBlocks;
	BufCtrlPtr buffer;
	
	if( success = (ReadRoot(FALSE, &buffer) == CERR_NOERR) ) {
		rootBlockEnd = (RootBlockEndPtr) (((UBYTE *)buffer) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(RootBlockEnd)));
		if( success = (rootBlockEnd->BMFlg) != 0 ) {
			keyBufPtr = currVolParams.BitMapKeyBuf;
			saveBlocks = blocks = currVolParams.BitMapBlocks + currVolParams.BitMapExtBlks;
			bitMapKeyPtr = &rootBlockEnd->BMPtrs[0];			
			numKeys = 26;					/* Root has 25 keys + extension key max */
			do {
				while( numKeys-- ) {
					if( key = *bitMapKeyPtr++ ) {
						*keyBufPtr++ = key;
						if( --blocks == 0 ) {
							success = FALSE;
							break;
						}
					} else {
						success = FALSE;
						break;
					}
				}
				if( success ) {
					*((UBYTE *)(keyBufPtr-1)) |= 0x80;
					if( !(success = ReadData(key, &buffer) == CERR_NOERR) ) {
						ReportInfo(ERR_BITMAP_READ);
					}
					bitMapKeyPtr = (ULONG *)(buffer+1);
					numKeys = currVolParams.BlockSizeL;
				}
			} while( success );
			success = (( ((ULONG)keyBufPtr) - ((ULONG)currVolParams.BitMapKeyBuf) ) >> 2) == saveBlocks;
		}
	}	
	return(success);
}

/*
 * Verify validity of bitmap
 */
 
static BOOL VerifyBitMap()
{
	register UWORD blocks;
	register ULONG *ptr;
	register ULONG key;
	register BOOL success;
	
	if( success = MarkKeyBusy(currVolParams.RootBlock) == FALSE ) {
		blocks = currVolParams.BitMapBlocks;
		ptr = currVolParams.BitMapKeyBuf;
		while( blocks-- ) {
			if( key = *ptr++ ) {
				if( MarkKeyBusy(key & ~(1 << 31)) ) {
					success = FALSE;				/* Marked as FREE! Bad, bad... */
					break;
				}
			} else {
				success = FALSE;					/* Zero key?! Something clobbered key list. */
				break;
			}
		}
	}
	return(success);	
}
					
/*
 * Reads bitmap data into contigous buffer, checks and removes checksums.
 * Result is one big contigous bitmap. Great for fast access. Loads in
 * reverse order to allow overwriting checksum in first longword.
 */
 
BOOL LoadBitMapData()
{
	register ULONG *bitMapBufPtr;
	register LONG *bitMapKeyPtr;
	register UWORD blocks = currVolParams.BitMapBlocks;
	register ULONG size = currVolParams.BlockSize;
	register LONG key;
	register BOOL success = TRUE;
	
	bitMapBufPtr = (ULONG *) (((ULONG)currVolParams.BitMapBuf) + currVolParams.BitMapBufSize);
	bitMapKeyPtr = (LONG *) (((ULONG)currVolParams.BitMapKeyBuf) + currVolParams.BitMapKeyBufSize);
	
	while( blocks-- ) {
		while( (key = *--bitMapKeyPtr) < 0 );
		if( key == 0 ) {
			break;					/* Zero key?! Something clobbered key list... */
		} else {
			bitMapBufPtr = (LONG *) (((ULONG)bitMapBufPtr) - size);
			if( success = ReadBitMapBlock(bitMapBufPtr, key) ) {
				++bitMapBufPtr;
			} else {
				ReportInfo(ERR_BITMAP_READ);
				break;
			}
		}
	}
	if( success ) {
		bitMapDataPtr = bitMapBufPtr;
		bitMapFlag = TRUE;
		bitMapDirtyFlag = FALSE;
	}
	return(success);
}

/*
 * Read a bitmap block from the disk
 */
 
static BOOL ReadBitMapBlock(register ULONG *buf, ULONG key)
{
	BOOL success;
	
	if( success = ReadBlock(key, (UBYTE *)buf, FALSE) == 0 ) {
		success = CalcBlockChecksum(buf) == 0;
	}
	return(success);
}
	

/*
 * Initialize the bitmap buffer to all bits on (free)
 */
 
void InitBitMapBuffer(register ULONG *dataPtr)
{
	register ULONG keys = currVolParams.MaxKeys;
	register ULONG bitNum = 32;
	register ULONG val = -1;
	
	/* keys -= currVolParams.Envec.de_PreAlloc - POSSIBLY NEEDED? */

	do {
		if( keys < bitNum ) {
			val = *dataPtr;				/* Fewer than 32 keys left */
			bitNum = 0;						/* Start with bit zero */
			do {
				val |= 1 << bitNum;		/* Mark one key free */
				++bitNum;
			} while( --keys );
			*dataPtr = val;				/* Store final longword and exit routine. */
		} else {
			*dataPtr++ = val;				/* Mark 32 keys free */
			keys -= bitNum;
		}
		
	} while( keys );
}


/*
 * These two trios call MarkBitMapOccupied(), which resets/sets the correct
 * bit in the bitmap of the key passed in the specified data area.
 * In addition, the previous state is returned, for convenience.
 * NOTE: TRUE means free, FALSE means busy, just like in AmigaDOS.
 */
 
BOOL MarkAltBusy(ULONG key)
{
	return( MarkBitMapOccupied(bitMapAltPtr, key, FALSE) );
}

BOOL MarkBadBusy(ULONG key)
{
	return( MarkBitMapOccupied(bitMapBadPtr, key, FALSE) );
}

BOOL MarkKeyBusy(ULONG key)
{
	bitMapDirtyFlag = TRUE;
	return( MarkBitMapOccupied(bitMapDataPtr, key, FALSE) );
}

BOOL MarkAltFree(ULONG key)
{
	return( MarkBitMapOccupied(bitMapAltPtr, key, TRUE) );
}

BOOL MarkBadFree(ULONG key)
{
	return( MarkBitMapOccupied(bitMapBadPtr, key, TRUE) );
}

BOOL MarkKeyFree(ULONG key)
{
	bitMapDirtyFlag = TRUE;
	return( MarkBitMapOccupied(bitMapDataPtr, key, TRUE) );
}

/*
 * This trio of routines, all call GetBitMapStatus() to determine
 * key busy status, TRUE if free, FALSE if invalid or busy.
 */
 
BOOL GetAltStatus(ULONG key)
{
	return( GetBitMapStatus(bitMapAltPtr, key) );
}

BOOL GetBadStatus(ULONG key)
{
	return( GetBitMapStatus(bitMapBadPtr, key) );
}

BOOL GetKeyStatus(ULONG key)
{
	return( GetBitMapStatus(bitMapDataPtr, key) );
}

/*
 * This routine is called by the higher level routines named
 * GetAltStatus(), GetBadStatus(), and GetBusyStatus() to 
 * determine the busy state of a bitmap.
 * Returns TRUE if free, FALSE if busy or invalid.
 */
 
static BOOL GetBitMapStatus(ULONG *ptr, register ULONG key)
{
	register BOOL status;
	register ULONG mask;
	
	if( status = (key <= currVolParams.HighestKey) && (key >= currVolParams.LowestKey) ) {
		key -= currVolParams.LowestKey;
		mask = 1 << (key & 31);
		key >>= 5;		/* Divide by 32 to get longword index */
		status = (ptr[key] & mask) != 0;
	}
	return(status);
}

/*
 * This routine is called whenever a key, alt, or bad bitmap is
 * to have an entry marked free or busy. 
 * Returns the previous state, just like calling GetBitMapStatus().
 * NOTE: This routine need only be called by the MarkxxxxBusy/Free() routines,
 * which take only one parameter, rather than the three here.
 */
 
BOOL MarkBitMapOccupied(ULONG *ptr, register ULONG key, BOOL free)
{
	register BOOL status;
	register ULONG mask;
	
	if( status = (key <= currVolParams.HighestKey) && (key >= currVolParams.LowestKey) ) {
		key -= currVolParams.LowestKey;
		mask = 1 << (key & 31);
		key >>= 5;
		status = ((ptr[key] & mask) != 0);
		if( free ) {
			ptr[key] |= mask;
		} else {
			ptr[key] &= ~mask;
		}
	}
	return(status);
}

/*
 * Determine whether the key passed is a bitmap key (or bitmap extension key)
 */
 
BOOL IsBitMapKey(register ULONG matchKey)
{
	register ULONG *bitMapKeyPtr;
	register ULONG key;
	register UWORD blocks = currVolParams.BitMapBlocks + currVolParams.BitMapExtBlks;

	if( currVolParams.BitMapKeyBuf ) {
		bitMapKeyPtr = (LONG *) (((ULONG)currVolParams.BitMapKeyBuf) + currVolParams.BitMapKeyBufSize);
	
		while( blocks-- ) {
			key = *--bitMapKeyPtr;
			if( IS_EXT_KEY(key) ) {
				key = key & 0x7FFFFFFF;
			}
			if( key == matchKey ) {
				return(TRUE);
			}
		}
	}
	return(FALSE);
}

/*
 * Determine whether the key passed is a bitmap extension key.
 */
 
BOOL IsBitMapExtKey(ULONG matchKey)
{
	register ULONG *bitMapKeyPtr;
	register ULONG key;
	register UWORD blocks = currVolParams.BitMapBlocks + currVolParams.BitMapExtBlks;

	if( currVolParams.BitMapKeyBuf ) {
		bitMapKeyPtr = (LONG *) (((ULONG)currVolParams.BitMapKeyBuf) + currVolParams.BitMapKeyBufSize);
	
		while( blocks-- ) {
			key = *--bitMapKeyPtr;
			if( IS_EXT_KEY(key) ) {
				key = key & 0x7FFFFFFF;
				if( key == matchKey ) {
					return(TRUE);
				}
			}
		}
	}
	return(FALSE);
}

/*
 * Quick and dirty for Reorg.c
 * Returns TRUE if any key from "block" to "block+num" is bad.
 */

BOOL AnyBadInRange(ULONG block, register ULONG num)
{
	register ULONG *ptr = bitMapBadPtr;
	register ULONG i;
	register ULONG mask;
	register ULONG key;
	
	if( badBlockStats.BadBlockFlag ) {
		key = block - currVolParams.LowestKey;
		for( i = 0 ; i < num ; i++, key++ ) {
			mask = 1 << (key & 31);
			if( (ptr[key >> 5] & mask) == 0 ) {
				return(TRUE);
			}
		}
	}
	return(FALSE);
}	

/*
 * Relocates bitmap blocks to the control area immediately following the root.
 * Marks old bitmap keys free, unless corresponding new key is busy, in which case
 * contents of new key is relocated to old key.
 */

BOOL RepositionBitMap(ULONG *newPos)
{
	register BOOL success = TRUE;
	register ULONG block;
	register ULONG key;
	register ULONG *ptr;
	register UWORD num;
	ULONG temp;
	
	block = currVolParams.RootBlock;
	ptr = currVolParams.BitMapKeyBuf;
	num = currVolParams.BitMapBlocks;
	
	while( num-- ) {
		block++;
		if( key = *ptr++ ) {
			temp = key & 0x7FFFFFFF;
			if( RemapKey(&temp) ) {
				key = temp;
				MarkKeyFree(key);
				if( key != block ) {
					if( !GetKeyStatus(block) ) {
						if( RelocateBlock(block, key) ) {
							MarkKeyBusy(key);
						} else {
							success = FALSE;
							break;
						}
					}
				}
			} else {
				success = FALSE;
				break;
			}
		}
	}
	if( success ) {
		*newPos = block;
	}
	return(success);
}

/*
 * Resets bitmap block keys to area immediately following root node. The bitmap
 * isn't actually written. That comes at the end of the operation.
 */
 
BOOL UpdateBitMapKeys()
{
	BOOL success = TRUE;
	register UWORD num;
	register ULONG block;
	
	num = currVolParams.BitMapBlocks + currVolParams.BitMapExtBlks;
	block = currVolParams.RootBlock;
	
	while( num-- ) {
		++block;
		currVolParams.FreeKeys++;
		MarkKeyFree(block);
	}
	currVolParams.LastUsedKey = currVolParams.RootBlock;
	AssignBitMapKeys();

	return(success);
}

/*
 * Assigns the bitmap block keys
 */
 
static BOOL AssignBitMapKeys()
{
	register UWORD blocks;
	register ULONG *ptr;
	register ULONG *destPtr;
	register ULONG max = 26;
	register BOOL cont;
	ULONG ext;
	BufCtrlPtr buf;
	ULONG *blockPtr = NULL;			/* NULL is only done to squelch compiler */
	ULONG *tempPtr = currVolParams.BitMapKeyBuf;
	ULONG numBlocks = currVolParams.BitMapBlocks;
	BOOL success;
	
	do {
		(void) StoreBitMapKeys(&tempPtr, &numBlocks, max);
		if( numBlocks ) {
			(void) AllocFreeKey();
			*tempPtr++ = currVolParams.LastUsedKey | 0x80000000;
			max = currVolParams.BlockSizeL;
		}
	} while( numBlocks );
	
	if( success = ReadControl(currVolParams.RootBlock, &buf) == CERR_NOERR ) {
		buf->DirtyFlag = TRUE;
		destPtr = &((RootBlockEndPtr)(((UBYTE *)buf) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(RootBlockEnd))))->BMPtrs[0];
		max = 25;
		ext = 0;
		blocks = currVolParams.BitMapBlocks;
		ptr = currVolParams.BitMapKeyBuf;
		do {
			while( max-- ) {
				*destPtr++ = *ptr++;
				if( --blocks == 0 ) {
					break;
				}
			}
			if( blocks ) {
				*destPtr = (*ptr++) & 0x7FFFFFFF;
					max = *destPtr;
			}
			if( ext ) {
				WriteBlock(ext, (UBYTE *)blockPtr, FALSE);
			}
			if( cont = (blocks != 0) ) {
				ext = max;
				if( GetCacheBuffer(&buf) == CERR_NOERR ) {
					buf->DirtyFlag = TRUE;
					blockPtr = destPtr = (ULONG *)(buf+1);
					BlockClear(destPtr, currVolParams.BlockSize);
					max = currVolParams.BlockSizeL - 1;
				} else {
					success = cont = FALSE;
				}
			}
		} while( cont );
	}
	return(success);
}
