/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Repair user interface code.
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <devices/trackdisk.h>
#include <dos/dos.h>

#include <rexx/errors.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/layers.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Border.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>
#include <Toolbox/DateTime.h>
#include <Toolbox/BarGraph.h>
#include <Toolbox/Graphics.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	Externals
 */

extern UBYTE			_tbPenLight, _tbPenBlack;
extern BOOL				_tbSmartWindows;

extern WindowPtr		cmdWindow;
extern ScreenPtr		screen;
extern MsgPortPtr		mainMsgPort;
extern OptionsRec		options;
extern BOOL				closeFlag, quitFlag;
extern GadgetPtr		currGadgList;
extern TextPtr			strsErrors[], strPath;
extern TextChar		strBuff[], choiceName[], currFileName[];
extern TextChar		strStart[], strPause[], strResume[];
extern TextChar		strFile[], strCommentBegin[], strDrive[], strQuote[];
extern TextChar		strReadyToAnalyze[], strAndRepair[];
extern TextChar		strScan[], strStarted[], strCompleted[], strAborted[];
extern TextChar		strAt[]/*, strOn[]*/;
extern TextChar		strChkBoth[], strChkBadBlocks[], strChkBadFiles[];
extern TextChar		strAnalyzeOnly[], strRepairToo[];
extern TextChar		strBlockLabel[], strFileLabel[], strDrawerLabel[];
extern WORD				action, mode, stage, intuiVersion;
extern ReqTemplPtr		reqList[];
extern RequestPtr		requester;
extern ScrollListPtr		scrollList;
extern VolParams		currVolParams;
extern BOOL				abortFlag;
extern Rectangle		labelRect, statusRect, statusRect2;
extern BarGraphPtr		barGraph;
extern UWORD			charHeight, charBaseline;
extern RepairStats		repairStats;
extern ULONG			lastTick;
extern LONG				xoffset, maxWidth;
extern ULONG			topListNum;

/*
 *	Local prototypes
 */

static void RefreshRepairStats(void);
static void HandleAbortButton(void);
static void HandleLeftButton(void);
static void HandleCloseBox(void);
static void SetListTitle(void);
static void	SetRepairStartState(void);
static void DrawItem(RastPtr, TextPtr, RectPtr);

enum {
	OPTSRESET_BUTTON = 2,
	BLOCKS_BOX,
	FILES_BOX,
	CHECK_FFS_BOX,
	REPAIR_BOX,
	BLOCKMODE_POPUP
};

enum {
	FS_POPUP = 2
	/*
	FFS_BOX = 2,
	INTL_BOX,
	CACHE_BOX,
	*/
};

#define UPDATE_REPAIR_BUTTONS	99

#define NUM_REPAIR_OPTIONS		(sizeof(repairOptNames)/sizeof(TextPtr))

static TextPtr repairOptNames[] = {
	"BadBlocksOn",	"BadBlocksOff",
	"BadFilesOn",	"BadFilesOff",
	"ReadsOnly",	"ReadsAndWrites",
	"CheckFFSDataOn",	"CheckFFSDataOff",
	"RepairOn",		"RepairOff"
};

enum {
	OPT_BADBLOCKSON,	OPT_BADBLOCKSOFF,
	OPT_BADFILESON,	OPT_BADFILESOFF,
	OPT_READSONLY,	OPT_READSANDWRITES,
	OPT_CHECKFFSDATAON,OPT_CHECKFFSDATAOFF,
	OPT_REPAIRON,	OPT_REPAIROFF
};

/*
 *	The master routine for analyze and repair!
 * Call from script or when user hits the buttons.
 * Does either a block scan or file scan or both.
 * Returns TRUE if the entire process proceeds without a hitch.
 */

BOOL DoRepair()
{
	register BOOL success = FALSE;
	register WindowPtr window = cmdWindow;
	register GadgetPtr gadgList = currGadgList;
	register BOOL started = FALSE;

	if( action == OPER_DONE ) {
		AddBlankLineToScrollList();
		AddBlankLineToScrollList();
	}
	SetListTitle();

	action = OPER_IN_PROG;
	stage = 0;
	abortFlag = FALSE;
	
	SetAllMenus();							/* Set Project and Tools menus */
	SetButtonItem(gadgList, BUTTON_OK, window, NULL, &strPause[0], CMD_KEY_PAUSE);
	EnableGadgetItem(gadgList, BUTTON_CANCEL, window, NULL, TRUE);
	EnableGadgetItem(gadgList, BUTTON_OPTIONS, window, NULL, FALSE);
	
	if( PromptForDisk() ) {
		GetDiskType();
		if( (!currVolParams.NonFSDisk) || (WarningDialog(strsErrors[ERR_NO_FS_PROMPT], CANCEL_BUTTON) == OK_BUTTON) ) {
			if( (!options.RepairOpts.Repair) || PromptForWriteProtected() ) {
				/*closeFlag = FALSE;*/
				if (options.RepairOpts.DoBadBlocks) {
					success = CheckBadBlocks();
				}
				if( options.RepairOpts.DoBadFiles && (action != OPER_DONE) ) {
					stage++;
					success = CheckBadFiles();
				}
				started = TRUE;
			}
		}
	}
	action = started ? OPER_DONE : OPER_READY;
	SetAllMenus();
	
	if( !quitFlag && closeFlag ) {
		ChangeToolMode(MODE_TOOLS);
	} else {
		SetRepairStartState();
	}
	return(success);
}

/*
 * If repair window opens successfully, perform various initializations to it.
 */

BOOL SetupRepairMode()
{
	Rectangle rect;
	register WindowPtr window = cmdWindow;
	
	AddGList(window, currGadgList, -1, -1, 0);
	GetGadgetRect(GadgetItem(currGadgList, PROGRESS_BAR), window, NULL, &rect);
	if ((barGraph = NewBarGraph(&rect, currVolParams.MaxKeys, 8, BG_HORIZONTAL | BG_SHOWPERCENT)) == NULL) {
		CleanupRepairMode();
		return FALSE;
	}
	DrawBarGraph(window->RPort, barGraph);

	InitScrollList(scrollList, GadgetItem(currGadgList, SCROLL_LIST), window, NULL);
	xoffset = 0;
	maxWidth = 0;
	topListNum = 0;

	mode = MODE_REPAIR;
	action = OPER_READY;
	
	RefreshRepairWindow(TRUE);
			
	SLSetDrawProc(scrollList, DrawItem);
	
	SetAllMenus();

	SetRepairStartState();

	(void) IsDiskChanged();
	return TRUE;
}

/*
 * Initialize user-interface to start state on repair.
 */
 
static void SetRepairStartState()
{
	register WindowPtr window = cmdWindow;
	register GadgetPtr gadgList = currGadgList;
	
	SetButtonItem(gadgList, BUTTON_OK, window, NULL, &strStart[0], CMD_KEY_START);

	if (!options.RepairOpts.DoBadBlocks && !options.RepairOpts.DoBadFiles) {
		EnableGadgetItem(gadgList, BUTTON_OK, window, NULL, FALSE);
	}
	EnableGadgetItem(gadgList, BUTTON_CANCEL, window, NULL, FALSE);
	EnableGadgetItem(gadgList, BUTTON_OPTIONS, window, NULL, TRUE);
}

/* 
 * Free up resources allocated by the repair window.
 */
 
void CleanupRepairMode()
{
	if( barGraph != NULL ) {
		DisposeBarGraph(barGraph);
		barGraph = NULL;
	}
	RemoveWindow(cmdWindow);
}

/*
 *	Refresh the window's contents
 */

void RefreshRepairWindow(BOOL adjust)
{
	Rectangle winRect;
	register WindowPtr window = cmdWindow;
	register RastPtr rPort = window->RPort;
	BOOL inval = FALSE;
	
	SetDrMd(rPort, JAM1);
	GetWindowRect(window, &winRect);
/*
	Clear the background of the window if first time, or pre DOS2.0 and simple refresh.
*/
	if( adjust || (!_tbSmartWindows) ) {
		if( intuiVersion < OSVERSION_2_0 ) {
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &winRect);
			inval = TRUE;
		}
	}
	if( adjust ) {
		UpdateHScrollBar(GadgetItem(currGadgList, LEFT_ARROW));
		inval = TRUE;
	}
	if( inval ) {
		RefreshGadgets(currGadgList, window, NULL);
		InvalRect(window, &winRect);
	}
	BeginRefresh(window);
/*
	If new window size or not a smart refresh window, we have to draw ourselves.
*/
	if( adjust || (!_tbSmartWindows) ) {
		GetGadgetRect(GadgetItem(currGadgList, LABEL_TEXT), window, NULL, &labelRect);
		GetGadgetRect(GadgetItem(currGadgList, STATUS_TEXT), window, NULL, &statusRect);
		statusRect2 = statusRect;
		statusRect.MaxX = (cmdWindow->Width - cmdWindow->BorderRight) - 2;
		statusRect2.MinY += charHeight+2;
		OutlineOKButton(window);
		SetHelpText();
		DrawBarGraph(rPort, barGraph);
		SLDrawBorder(scrollList);
		SLDrawList(scrollList);
		if( (action == OPER_IN_PROG) || (action == OPER_PAUSE) ) {
			RefreshRepairStats();
		}
	}
	EndRefresh(window, TRUE);
}

/*
 *	Minimal text draw routine
 */

static void DrawItem(RastPtr rPort, TextPtr text, RectPtr rect)
{
	Text(rPort, text, strlen(text));
}

/*
 * Display two help strings in the help box. If NULL, draw nothing.
 */
 
void DisplayHelpText(register TextPtr text1, TextPtr text2)
{
	register RastPtr rPort = cmdWindow->RPort;
	Rectangle rect;
	register WORD y;
	register WORD temp;
	WORD width;
	
	GetGadgetRect(GadgetItem(currGadgList, HELP_TEXT), cmdWindow, NULL, &rect);

	InsetRect(&rect, 1, 1);
	SetDrMd(rPort, JAM1);
	SetAPen(rPort, _tbPenLight);
	FillRect(rPort, &rect);
	
	SetAPen(rPort, _tbPenBlack);
	y = (rect.MaxY - rect.MinY) >> 1;
	temp = rect.MinY + ((y + charBaseline) >> 1);
	rect.MinX++;
	width = rect.MaxX - rect.MinX;		/* Don't add one, because of TextInWidth() */
	if( text1 != NULL ) {
		Move(rPort, rect.MinX, temp);
		TextInWidth(rPort, text1, strlen(text1), width, FALSE);
	}
	if( (text1 = text2) != NULL ) {
		Move(rPort, rect.MinX, y + temp);
		TextInWidth(rPort, text1, strlen(text1), width, FALSE);
	}
}

/*
 * Display the path in global "strPath" in the bar graph label.
 */
 
void DisplayPath()
{
	register RastPtr rPort = cmdWindow->RPort;
	register TextPtr str;
	
	SetAPen(rPort, _tbPenLight);
	FillRect(rPort, &statusRect);
	SetAPen(rPort, _tbPenBlack);
	if( str = strchr(strPath, ':') ) {
		str++;
		Move(rPort, statusRect.MinX, statusRect.MinY + charBaseline);
		/*CondensePath(str, strBuff);*/
		DoTextInWidth(str, WIDTH(&statusRect), TRUE);
	}
}

/*
 * Display the file being processed in the bar graph label.
 * Italicize for links.
 * Note: Currently does not check for "bad.blocks" file, like V1.x does.
 * Maybe it is more correct not to display it, but due to the fact that
 * this is called in a different place, we'd need two compares instead
 * of just one.
 */
 
void DisplayFile()
{
	register RastPtr rPort = cmdWindow->RPort;
	register BOOL italicize;
	register LONG temp;
	Rectangle rect = statusRect2;
	
	rect.MaxX  = statusRect.MaxX;
	SetAPen(rPort, _tbPenLight);
	FillRect(rPort, &rect);
	SetAPen(rPort, _tbPenBlack);
	if( currFileName[0] ) {
		temp = repairStats.CurrentBlockType;
		italicize = (temp == ST_LINKFILE) || (temp == ST_LINKDIR) || (temp == ST_SOFTLINK);
		Move(rPort, rect.MinX, rect.MinY + charBaseline);
		if( italicize ) {
			SetSoftStyle(rPort, FSF_ITALIC, 0xFF);
		}
		DoTextInWidth(currFileName, WIDTH(&rect), FALSE);
		if( italicize ) {
			SetSoftStyle(rPort, FS_NORMAL, 0xFF);
		}
	}
}

/*
 * Refresh the label and associated stat(s) with finding bad blocks OR files.
 */
 
static void RefreshRepairStats()
{
	if( stage ) {
		RefreshFileScan();
		DisplayPath();
		DisplayFile();
	} else {
		RefreshBlockScan();
		RefreshBlockStats();
	}
}

/*
 * Update block statistics
 */
 
void UpdateBlockStats()
{
	register ULONG ticks;
	struct DateStamp currDateStamp;
	register ULONG maxBlocks = currVolParams.MaxBlocks >> 13;
	register ULONG treshhold = 4 + MIN(4, maxBlocks);
	
	DateStamp(&currDateStamp);
	ticks = (currDateStamp.ds_Minute * 3000) + currDateStamp.ds_Tick;
	if( (ticks - lastTick) > treshhold ) {
		lastTick = ticks;
		RefreshBlockStats();
	}
}

/*
 * Refresh the block number.
 */
 
void RefreshBlockStats()
{
	RastPtr rPort = cmdWindow->RPort;
	Rectangle rect = statusRect2;
	
	rect.MaxX += 2;
	rect.MinY -= 3;
	SetBarGraph(rPort, barGraph, currVolParams.DoneBlocks+1);
	PrintRightNum(rPort, &rect, currVolParams.DoneBlocks);
}


/*
 * Erase the status rectangle, and return its pen color to black.
 */
 
void EraseLabelRect()
{
	register RastPtr rPort = cmdWindow->RPort;
	
	SetAPen(rPort, _tbPenLight);
	FillRect(rPort, &labelRect);
	SetAPen(rPort, _tbPenBlack);
}

/*
 * Refresh the "Block:" label.
 */

void RefreshBlockScan()
{
	Rectangle rect;
	RastPtr rPort = cmdWindow->RPort;
		
	rect = labelRect;
	rect.MinY += charHeight + 2;
	EraseLabelRect();
	Move(rPort, labelRect.MinX, rect.MinY + charBaseline);
	Text(rPort, strBlockLabel, strlen(strBlockLabel));
}
 
/*
 * Refresh the "Drawer:" and "File:" labels.
 */
 
void RefreshFileScan()
{
	register RastPtr rPort = cmdWindow->RPort;
	
	EraseLabelRect();
	Move(rPort, labelRect.MinX, statusRect.MinY + charBaseline);
	Text(rPort, strDrawerLabel, strlen(strDrawerLabel));
	Move(rPort, labelRect.MinX, statusRect2.MinY + charBaseline);
	Text(rPort, strFileLabel, strlen(strFileLabel));
}



/*
 *	Adjusts the scrollList's first line based on Action radio buttons
 */

static void SetListTitle()
{
	register TextPtr str = &strBuff[0];
	
	strcpy(str, strCommentBegin);
	strcat(str, strReadyToAnalyze);
	if (options.RepairOpts.Repair) {
		strcat(str, strAndRepair);
	}
	strcat(str, strDrive);
	strcat(str, choiceName);
	strcat(str, strQuote);

	SLChangeItem(scrollList, topListNum, str, strlen(str));
	RecalcMaxWidth();
	UpdateHScrollBar(GadgetItem(currGadgList, LEFT_ARROW));
}

/*
 *	Adjusts the command rectangle below buttons
 *	to show whether bad blocks, and bad files and drawers are selected
 */

void SetHelpText()
{
	register TextPtr textPtr;

	if( options.RepairOpts.DoBadFiles ) {
		textPtr = options.RepairOpts.DoBadBlocks ? strChkBoth : strChkBadFiles;
	} else {
		textPtr = options.RepairOpts.DoBadBlocks ? strChkBadBlocks : NULL;
	}
	DisplayHelpText(options.RepairOpts.Repair ? strRepairToo : strAnalyzeOnly, textPtr);
}

/*
 * Increment the file repair bar graph by one, as one block was read, and redraw.
 * We can rely on SetBarGraph() to confine to the maximum reading.
 */
 
void BumpBarGraph()
{
	if( currVolParams.VolValid ) {
		AddBarGraph(1);
	}
}

/*
 * Add to bar graph value.
 */
 
void AddBarGraph(LONG add)
{
	SetBarGraph(cmdWindow->RPort, barGraph, (currVolParams.DoneBlocks += add));
}

/*
 *	Handles what to do with the close nbox in
 *	the Repair window based on current 'action'
 *	mode.
 */

static void HandleCloseBox()
{
	switch(action) {
	case OPER_READY:
	case OPER_DONE:
		ReturnToMain();
		break;
	case OPER_IN_PROG:
	case OPER_PAUSE:
		if( closeFlag = CheckAbort() ) {
			abortFlag = TRUE;
			action = OPER_DONE;
			SetAllMenus();
		}
		break;
	}
}

/*
 *	Handles what to do with the left button in
 *	the Repair window based on current 'action'
 *	mode.
 */

static void HandleLeftButton()
{
	switch(action) {
	case OPER_READY:
		DoRepair();
		break;
	case OPER_IN_PROG:
		action = OPER_PAUSE;
		SetButtonItem(currGadgList, BUTTON_OK, cmdWindow, NULL, &strResume[0], CMD_KEY_RESUME);
		RefreshRepairStats();
		MotorOff();
		break;
	case OPER_PAUSE:
		action = OPER_IN_PROG;
		SetButtonItem(currGadgList, BUTTON_OK, cmdWindow, NULL, &strPause[0], CMD_KEY_PAUSE);
		break;
	case OPER_DONE:
		DoRepair();
		/*
		ReturnToMain();
		*/
		break;
	}
}

/*
 *	Handles what to do with the right button in
 *	the Repair window based on current 'action'
 *	mode.
 */

static void HandleAbortButton()
{
	switch(action) {
	case OPER_IN_PROG:
	case OPER_PAUSE:
		if( abortFlag = CheckAbort() ) {
			action = OPER_DONE;
			SetAllMenus();
		}
		break;
	default:
		break;
	}
}

/*
 *	Allow user to adjust repair options
 */

BOOL DoRepairOptions()
{
	register WindowPtr window;
	register RequestPtr req;
	GadgetPtr gadgList;
	BOOL success = FALSE;
	RepairOptsRec repairOpts;
	register WORD item;
	
	if( (action == OPER_READY) || (action == OPER_DONE) ) {
		reqList[REQ_REPAIROPTS]->Gadgets[BLOCKMODE_POPUP].Value = options.RepairOpts.BadBlockMode != 0;
		req = DoGetRequest(REQ_REPAIROPTS);
		if( req == NULL ) {
			Error(ERR_NO_MEM);
		} else {
			requester = req;
			window = cmdWindow;
			gadgList = req->ReqGadget;
			repairOpts = options.RepairOpts;
			OutlineOKButton(window);
			item = UPDATE_REPAIR_BUTTONS;
			do {
				switch( item ) {
				case OPTSRESET_BUTTON:
					SetDefaultRepairOptions(&repairOpts);
					SetGadgetValue(GadgetItem(gadgList, BLOCKMODE_POPUP), window, req, repairOpts.BadBlockMode != 0);
				case UPDATE_REPAIR_BUTTONS:
					GadgetOnOff(window, BLOCKS_BOX, repairOpts.DoBadBlocks);
					GadgetOnOff(window, FILES_BOX, repairOpts.DoBadFiles);
					GadgetOnOff(window, CHECK_FFS_BOX, repairOpts.CheckFFSData);
					EnableGadgetItem(gadgList, CHECK_FFS_BOX, window, req, repairOpts.DoBadFiles);
					EnableGadgetItem(gadgList, BLOCKMODE_POPUP, window, req, repairOpts.DoBadBlocks);
					GadgetOnOff(window, REPAIR_BOX, repairOpts.Repair);
					break;
				case FILES_BOX:
					ToggleCheckbox(&repairOpts.DoBadFiles, FILES_BOX, window);
					EnableGadgetItem(gadgList, BUTTON_OK, window, req, repairOpts.DoBadFiles | repairOpts.DoBadBlocks);
					EnableGadgetItem(gadgList, CHECK_FFS_BOX, window, req, repairOpts.DoBadFiles);
					break;
				case CHECK_FFS_BOX:
					ToggleCheckbox(&repairOpts.CheckFFSData, CHECK_FFS_BOX, window);
					break;
				case REPAIR_BOX:
					ToggleCheckbox(&repairOpts.Repair, REPAIR_BOX, window);
					break;
				case BLOCKMODE_POPUP:
					repairOpts.BadBlockMode = GetGadgetValue(GadgetItem(gadgList, item)) != 0;
					if( repairOpts.DoBadBlocks ) {
						break;
					}
				case BLOCKS_BOX:
					ToggleCheckbox(&repairOpts.DoBadBlocks, BLOCKS_BOX, window);
					EnableGadgetItem(gadgList, BUTTON_OK, window, req, repairOpts.DoBadFiles | repairOpts.DoBadBlocks);
					EnableGadgetItem(gadgList, BLOCKMODE_POPUP, window, req, repairOpts.DoBadBlocks);
					break;
				default:
					break;
				}
				item = ModalRequest(mainMsgPort, window, DialogFilter);
			} while( (!((item == OK_BUTTON) && (repairOpts.DoBadFiles | repairOpts.DoBadBlocks))) && (item != CANCEL_BUTTON) );
			DestroyRequest(req);
			if( success = item != CANCEL_BUTTON ) {
/*
	Set new values
*/
				options.RepairOpts = repairOpts;
				SetHelpText();
			}
		}
	}
	return(success);
}

/*
 *	Handle scrollList's functions
 */

static BOOL RepairListFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	ULONG class = intuiMsg->Class;
	WORD gadgNum;
	register WORD code;
	register WindowPtr window = cmdWindow;
	GadgetPtr gadget;
	WORD modifier;
	
	if (intuiMsg->IDCMPWindow == window) {
		switch (class) {
		case INTUITICKS:
			if( !CheckForDiskChange() ) {
				ReplyMsg((MsgPtr)intuiMsg);
				ReturnToMain();
				return(TRUE);
			}
			break;
/*
	Handle gadget up/down in scroll list gadgets
		and ALT key down for "Back" button
*/
		case GADGETDOWN:
		case GADGETUP:
			switch( gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress) ) {
			case VERT_SCROLL:
			case UP_ARROW:
			case DOWN_ARROW:
				SLGadgetMessage(scrollList, mainMsgPort, intuiMsg);
				*item = gadgNum;
				return(TRUE);
			case HORIZ_SCROLL:
			case LEFT_ARROW:
			case RIGHT_ARROW:
				HandleHorizScroll(mainMsgPort, intuiMsg, GadgetItem(currGadgList, LEFT_ARROW));
				*item = gadgNum;
				return (TRUE);
			}
			break;
/*
	Handle cursor keys
*/
		case RAWKEY:
			code = intuiMsg->Code;
			modifier = intuiMsg->Qualifier;
			ReplyMsg((MsgPtr)intuiMsg);
			switch( code ) {
			case CURSORUP:
			case CURSORDOWN:
				SLCursorKey(scrollList, code);
				*item = SCROLL_LIST;
				break;
			case CURSORLEFT:
			case CURSORRIGHT:
				if( code == CURSORLEFT ) {
					gadget = GadgetItem(currGadgList, LEFT_ARROW);
					HiliteGadget(gadget, window, NULL, TRUE);
					ScrollLeft(window, 0);
				} else {
					gadget = GadgetItem(currGadgList, RIGHT_ARROW);
					HiliteGadget(gadget, window, NULL, TRUE);
					ScrollRight(window, 0);
				}
				HiliteGadget(gadget, window, NULL, FALSE);
				break;
			default:
				DoRawKey(window, code, modifier);
				break;
			}
			return(TRUE);
		}
	}
	return (FALSE);
}

/*
 *	DoRepairDialogMsg
 *	Handle repair window button hits
 */

BOOL DoRepairDialogMsg(IntuiMsgPtr intuiMsg)
{
	WORD item;
	DialogPtr dlg;

	if (RepairListFilter(intuiMsg, &item)) {
		return TRUE;
	}
	if (DialogSelect(intuiMsg, &dlg, &item)) {
		ReplyMsg((MsgPtr)intuiMsg);
		switch(item) {
		case DLG_CLOSE_BOX:
			HandleCloseBox();
			break;
		case BUTTON_OK:
			HandleLeftButton();
			break;
		case BUTTON_CANCEL:
			HandleAbortButton();
			break;
		case BUTTON_OPTIONS:
			(void) DoRepairOptions();
			break;
		}
		return TRUE;
	}

	ReplyMsg((MsgPtr)intuiMsg);
	return FALSE;
}

/*
 * User interface for deciding which boot block this will be.
 */
 
BOOL CheckBootBlockReq()
{
	BOOL success;
	register RequestPtr req;
	register WindowPtr window;
	register BOOL done;
	register WORD item;
	register WORD val;
		
	if( success = (req = DoGetRequest(REQ_FILESYSTEM)) != NULL ) {
		window = cmdWindow;
		requester = req;
		OutlineOKButton(window);
		done = FALSE;
		do {
			switch( item = ModalRequest(mainMsgPort, window, DialogFilter) ) {
			case CANCEL_BUTTON:
				success = FALSE;
			case OK_BUTTON:
				done = TRUE;
				break;
			default:
				break;
			}
		} while( !done );
		val = GetGadgetValue(GadgetItem(req->ReqGadget, FS_POPUP));
		DestroyRequest(req);
		if( success ) {
			currVolParams.FFSFlag = val & 1;
			currVolParams.International = val > 1;
			currVolParams.DirCache = val > 3;
		}
	}
	return(success);
}

/*
 *	Set repair options of given option name
 *	Used for AREXX macros
 *	Return FALSE if not a valid option name
 */

LONG SetRepairOption(register TextPtr optName)
{
	register WORD i;
	TextChar buff[256];
	UWORD len;
	register LONG result = RC_OK;
	BOOL wordFound = FALSE;
	
	do {	
		GetNextWord(optName, buff, &len);
		optName += len;
		for (i = 0; i < NUM_REPAIR_OPTIONS; i++) {
			if (CmpString(buff, repairOptNames[i], strlen(buff), (WORD) strlen(repairOptNames[i]), FALSE) == 0) {
				break;
			}
		}
		if( i >= NUM_REPAIR_OPTIONS ) {
			result = RC_WARN;
		} else {
			wordFound = TRUE;
			switch (i) {
			case OPT_BADBLOCKSON:
			case OPT_BADBLOCKSOFF:
				options.RepairOpts.DoBadBlocks = i == OPT_BADBLOCKSON;
				break;
			case OPT_BADFILESON:
			case OPT_BADFILESOFF:
				options.RepairOpts.DoBadFiles = i == OPT_BADFILESON;
				break;
			case OPT_READSONLY:
			case OPT_READSANDWRITES:
				options.RepairOpts.BadBlockMode = i == OPT_READSANDWRITES;
				break;
			case OPT_CHECKFFSDATAON:
			case OPT_CHECKFFSDATAOFF:
				options.RepairOpts.CheckFFSData = i == OPT_CHECKFFSDATAON;
				break;
			case OPT_REPAIRON:
			case OPT_REPAIROFF:
				options.RepairOpts.Repair = i == OPT_REPAIRON;
				break;
			}
		}
	} while(*optName);
	if( mode == MODE_REPAIR ) {
		if( !wordFound ) {
			if( !DoRepairOptions() ) {
				result = RC_ERROR;
			}
		}
		SetHelpText();
	}
	return(result);
}
