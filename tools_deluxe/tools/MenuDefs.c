/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Menu definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Menu.h>

#include "Tools.h"

extern TextChar	macroNames2[10][MACRONAME_LEN];
extern TextChar	strQuit[], strPrint[];
extern TextChar	repairTitle[], recoverTitle[], optimizeTitle[], editTitle[];

/*
 *	Project menu items
 */

static MenuItemTemplate defaultsItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Save", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Save As...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Load...", NULL },
	{ MENU_NO_ITEM }
};

static MenuItemTemplate projectItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "About Quarterback Tools...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Save As...", NULL },
	{ MENU_TEXT_ITEM,	0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Page Setup...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Print...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Preferences...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0, 0, 0, "Settings", &defaultsItems[0] },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'Q', 0, 0, &strQuit[0], NULL },
	{ MENU_NO_ITEM }
};

/*
 * Utilities menu
 */

static MenuItemTemplate utilitiesItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED|MENU_CHECKED,   '0', 0,  ~1L&~128L, "Tool Selection", NULL},
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED|MENU_CHECKABLE, '1', 0,  ~4L&~128L, repairTitle, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED|MENU_CHECKABLE, '2', 0,  ~8L&~128L, recoverTitle, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED|MENU_CHECKABLE, '3', 0, ~16L&~128L, optimizeTitle, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED|MENU_CHECKABLE, '4', 0, ~32L&~128L, editTitle, NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'I', 0, 0, "Get Volume Information...", NULL },
	{ MENU_NO_ITEM }
};

/*
 *	Macro menu
 */

static MenuItemTemplate macroItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[0][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[1][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[2][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[3][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[4][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[5][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[6][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[7][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[8][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, &macroNames2[9][0], NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'M', 0, 0, "Other...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Customize...", NULL },
	{ MENU_NO_ITEM }
};

/*
 *	Menu strip templates for main window
 */

MenuTemplate docWindMenus[] = {
	{ " Project ", &projectItems[0] },
	{ " Tools ", &utilitiesItems[0] },
	{ " Macro ", &macroItems[0] },
	{ NULL, NULL }
};

/*
 * Submenu items for "Go To"
 */
 
static MenuItemTemplate gotoSubItems[] = { 
	{ MENU_TEXT_ITEM, MENU_ENABLED,'R', 0, 0, "Root", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'B', 0, 0, "Bitmap", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'C', 0, 0, "Hash Chain", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'P', 0, 0, "Parent", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'E',	0, 0, "Extension", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'H', 0, 0, "Header", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'D', 0, 0, "Next Data", NULL },
	{ MENU_NO_ITEM }
};

/*
 * Menu items for disk editor menu
 */
 
static MenuItemTemplate diskEditorItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,'F', 0, 0, "Find...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'N', 0, 0, "Find Next", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'L', 0, 0, "Calculate Hash...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'A', 0, 0, "Adjust Checksum", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED | MENU_TOGGLE, 0,  0, 0, "Auto-Adjust Checksum", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0,  0, 0, "Create Capture...", NULL },
	{ MENU_TEXT_ITEM, 0, 'W', 0, 0, "Write To Capture", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, "Close Capture", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'G', 0, 0, "Go To Block...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,'V', 0, 0, "Go To Previous", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0,  0, 0, "Go To", &gotoSubItems[0] },
	{ MENU_NO_ITEM },
};

/*
 * Menu for disk editor
 */
 
MenuTemplate diskEditorMenu[] = {
	{ " Edit ", &diskEditorItems[0] },
	{ NULL, NULL }
};
