/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Print handler routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <rexx/errors.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>
#include <Toolbox/FixMath.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/LocalData.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;

extern BOOL			graphicPrinter, pagePrinter;

extern TextChar	printerName[], strBuff[];
extern TextChar	strReportName[], strPrint[], strSave[], strSaveAs[], strPRT[];

extern ReqTemplPtr	reqList[];
extern RequestPtr	requester;

extern BOOL			destinationFile;

extern WindowPtr	cmdWindow;

extern UWORD		defaultLeftMargin, defaultRightMargin;

extern OptionsRec	options;
extern WORD			cpi, lpi;

extern Ptr			fib;

/*
 *	Page setup requester
 */

enum {
	PAPERSIZE_POPUP = 2,
	WIDTH_TEXT,
	HEIGHT_TEXT,
	PICA_RADBTN,
	ELITE_RADBTN,
	CONDENSED_RADBTN,
	SIXLPI_RADBTN,
	EIGHTLPI_RADBTN,
	NOGAPS_BOX,
	PRINTERNAME1_TEXT,
	INCH_RADBTN,
	CM_RADBTN,
};

/*
 * Print requester
 */
 
enum {
	NLQ_RADBTN = 2,
	DRAFT_RADBTN,
	AUTOMATIC_RADBTN,
	HANDFEED_RADBTN,
	COPIES_TEXT,
	FONTUP_ARROW,
	FONTDOWN_ARROW,
	FONTNUM_TEXT,
	TOPRINTER_RADBTN,
	TOFILE_RADBTN,
	PRINTERNAME2_TEXT
};


#define DECIPOINTS_TO_UNITS(a)	(((LONG)(a)<<4)/720)
#define UNITS_TO_DECIPOINTS(a)	(((LONG)(a)*720)>>4)

#define DOTS_TO_UNITS(a,dpi)		(((LONG)(a)<<4)/(dpi))
#define UNITS_TO_DOTS(a,dpi)		(((LONG)(a)*(dpi))>>4)

#define DOTS_TO_DECIPOINTS(a,dpi) (((LONG)(a)*720)/(dpi))
#define DECIPOINTS_TO_DOTS(a,dpi) (((LONG)(a)*(dpi))/720)

#define MIN_PAPERWIDTH	1440		/* 2 inch min width */
#define MIN_PAPERHEIGHT	720		/* 1 inch min height (plus top/bottom margins) */

#define MIN_SCALE	25
#define MAX_SCALE	400

#define MIN_FONTNUM	0
#define MAX_FONTNUM	10

enum {
	USLETTER_ITEM,
	USLEGAL_ITEM,
	A4LETTER_ITEM,
	WIDECARRIAGE_ITEM,
	CUSTOM_ITEM
};

/*
 *	Print options for SetPrintOption()
 */

#define NUM_OPTIONS	(sizeof(printOptNames)/sizeof(TextPtr))

static TextPtr printOptNames[] = {
	"NLQ",			"Draft",
	"AutoFeed",		"HandFeed",
};

enum {
	OPT_NLQ,				OPT_DRAFT,
	OPT_AUTOFEED,		OPT_HANDFEED,
};

/*
 *	Local prototypes
 */

static WORD	NormalizeDPI(WORD);
static void	SetDPI(PrintRecPtr);
static void	SetPrRects(PrintRecPtr);
static void	SetPrintFlag(PrintRecPtr, UWORD, BOOL);
static void	CalcWidthHeight(WORD, WORD *, WORD *);
static void	ShowWidthHeight(PrintRecPtr, WORD, WORD);
static void	SetDensityText(WORD, TextPtr);
static BOOL	PageSetupRequestFilter(IntuiMsgPtr, WORD *);
static BOOL	PrintRequestFilter(IntuiMsgPtr, WORD *);
static void  UpdatePrintButton(UWORD);

static WORD	GetFracValue(GadgetPtr, WORD);
static void	DecipointToText(WORD, TextPtr, WORD);
static WORD	TextToDecipoint(TextPtr);

static WORD	ConvertPitch(UBYTE);
static WORD	ConvertSpacing(UBYTE);

/*
 *	Get fractional inch value from gadget buffer and return value in decipoints
 */

static WORD GetFracValue(GadgetPtr gadgList, WORD item)
{
	GadgetPtr gadget;

	gadget = GadgetItem(gadgList, item);
	return (TextToDecipoint(((StrInfoPtr) gadget->SpecialInfo)->Buffer));
}

/*
 *	Convert decipoint value into decimal string in current measurement units
 */

static void DecipointToText(WORD num, TextPtr text, WORD places)
{
	Fixed inches;

	inches = FixRatio(num, 720);
	if (options.PrintInfo.MeasureUnit == MEASURE_CM)
		Fix2Ascii(FixMul(inches, FixRatio(254, 100)), text, places);
	else
		Fix2Ascii(inches, text, places);
}

/*
 *	Convert decimal string into decipoint value using current measurement units
 *	Clip to maximum value of 44 inches (less than 32,000 decipoints)
 */

static WORD TextToDecipoint(TextPtr text)
{
	Fixed num;

	num = Ascii2Fix(text);
	if (options.PrintInfo.MeasureUnit == MEASURE_CM)
		num = FixDiv(num, FixRatio(254, 100));	/* Convert to inches */
	if (num < 0)
		num = 0;
	else if (num > Long2Fix(44L))
		num = Long2Fix(44L);
	return (FixRound(FixMul(num, Long2Fix(720L))));
}

/*
 *	Initialize the print handler
 *	Checks for the presence of ProScript
 */

void InitPrintHandler()
{
#ifdef POSTSCRIPT
	LONG lock;
	struct FileInfoBlock *fibPtr = (struct FileInfoBlock *)fib;

	postScript = FALSE;
	SetPathName(strBuff, proScriptName, TRUE);
	if ((lock = Lock(strBuff, ACCESS_READ)) != NULL) {
		if (Examine(lock, fibPtr) && fibPtr->fib_DirEntryType < 0)
			postScript = TRUE;
		}
	}
	UnLock(lock);
#endif
}

/*
 *	Set x and y DPI values
 */

static void SetDPI(printRec)
register PrintRecPtr printRec;
{
	if (printRec->PrintPitch == PRT_PICA)
		printRec->xDPI = 80;
	else if (printRec->PrintPitch == PRT_ELITE)
		printRec->xDPI = 96;
	else
		printRec->xDPI = 137;
	printRec->yDPI = (printRec->PrintSpacing == PRT_SIXLPI) ? 6*8 : 8*8;
}

/*
 *	Set paper and page rectangles according to print record settings
 */

static void SetPrRects(printRec)
register PrintRecPtr printRec;
{
	register WORD xDPI, yDPI, width, height;
	WORD topMarg, botMarg, leftMarg, rightMarg;
	BOOL noGaps;

	xDPI = printRec->xDPI;
	yDPI = printRec->yDPI;
	noGaps = printRec->Flags & PRT_NOGAPS;
	switch (printRec->PageSize) {
	case PRT_USLETTER:
		printRec->PaperWidth = 6120;			/* 8.5 in */
		printRec->PaperHeight = 7920;			/* 11 in */
		break;
	case PRT_USLEGAL:
		printRec->PaperWidth = 6120;			/* 8.5 in */
		printRec->PaperHeight = 10080;		/* 14 in */
		break;
	case PRT_A4LETTER:
		printRec->PaperWidth = 5953;			/* 210 mm */
		printRec->PaperHeight = 8419;			/* 297 mm */
		break;
	case PRT_WIDECARRIAGE:
		printRec->PaperWidth = 10080;			/* 14 in */
		printRec->PaperHeight = 7920;			/* 11 in */
		break;
	}
	if (printRec->PaperWidth < MIN_PAPERWIDTH)
		printRec->PaperWidth = MIN_PAPERWIDTH;
	if (noGaps) {
		if (printRec->PaperHeight < MIN_PAPERHEIGHT)
			printRec->PaperHeight = MIN_PAPERHEIGHT;
	}
	else {
		if (printRec->PaperHeight < MIN_PAPERHEIGHT + 720)
			printRec->PaperHeight = MIN_PAPERHEIGHT + 720;
	}
	/*
	if (printRec->Orientation == PRT_PORTRAIT) {
		width = DECIPOINTS_TO_DOTS(printRec->PaperWidth, xDPI);
		height = DECIPOINTS_TO_DOTS(printRec->PaperHeight, yDPI);
		leftMarg = rightMarg = DECIPOINTS_TO_DOTS(180, xDPI);
		topMarg = botMarg = (noGaps) ? 0 : DECIPOINTS_TO_DOTS(360, yDPI);
	} else {
	*/
		width = DECIPOINTS_TO_DOTS(printRec->PaperHeight, xDPI);
		height = DECIPOINTS_TO_DOTS(printRec->PaperWidth, yDPI);
		leftMarg = rightMarg = (noGaps) ? 0 : DECIPOINTS_TO_DOTS(360, xDPI);
		topMarg = botMarg = DECIPOINTS_TO_DOTS(180, yDPI);
	/*}*/
	SetRect(&printRec->PaperRect, 0, 0, width, height);
	SetRect(&printRec->PageRect, leftMarg, topMarg, (WORD) (width - rightMarg),
			(WORD) (height - botMarg));
/*
	Quarterback really wants just character dimensions, so stuff it in prefs now!
*/
	options.PrintInfo.PageWidth = ((printRec->PaperWidth - 360) / 720) * cpi;
	options.PrintInfo.PageHeight = ((printRec->PaperHeight - 720) / 720) * lpi;
}

/*
 *	Validate the print record
 */

void PrValidate(printRec)
register PrintRecPtr printRec;
{
	GetPrinterType();
	if (printRec->Quality > PRT_NLQ)
		printRec->Quality = PRT_NLQ;
	SetDPI(printRec);
	printRec->Orientation = PRT_PORTRAIT;
	printRec->Flags |= PRT_ASPECTADJ;
	printRec->Flags &= ~(PRT_OPTSPACE | PRT_PICTURES | PRT_FONTS | PRT_SMOOTH);
	if (pagePrinter)
		printRec->Flags &= ~PRT_NOGAPS;
	if (printRec->FontNum < MIN_FONTNUM)
		printRec->FontNum = MIN_FONTNUM;
	else if (printRec->FontNum > MAX_FONTNUM)
		printRec->FontNum = MAX_FONTNUM;
	SetPrRects(printRec);
	printRec->Copies = 1;
	if (printRec->FirstPage < 1)
		printRec->FirstPage = 1;
	if (printRec->LastPage > 9999)
		printRec->LastPage = 9999;
	SetPrintFlag( printRec, PRT_TOFILE, 0);
	strcpy( printRec->FileName, strPRT );
}

/*
 *	Set print record to default values
 */

void PrintDefault(printRec)
register PrintRecPtr printRec;
{
	WORD density;
	struct Preferences prefs;
/*	register WORD width, left, right;*/
	
	GetPrinterType();
	printRec->Version = PRINTHANDLER_VERSION;
	printRec->Flags = 0;
/*
	Set preferences values
*/
	GetPrefs(&prefs, sizeof(struct Preferences));
	printRec->PageSize = (prefs.PaperSize == W_TRACTOR) ?
						 PRT_WIDECARRIAGE : PRT_USLETTER;
	printRec->Orientation = (prefs.PrintAspect == ASPECT_HORIZ) ?
							PRT_PORTRAIT : PRT_LANDSCAPE;
	printRec->PaperFeed = (prefs.PaperType == FANFOLD) ?
						  PRT_CONTINUOUS : PRT_CUTSHEET;
	if (prefs.PrintPitch == PICA)
		printRec->PrintPitch = PRT_PICA;
	else if (prefs.PrintPitch == ELITE)
		printRec->PrintPitch = PRT_ELITE;
	else
		printRec->PrintPitch = PRT_CONDENSED;
	printRec->PrintSpacing = (prefs.PrintSpacing == SIX_LPI) ?
							 PRT_SIXLPI : PRT_EIGHTLPI;
	cpi = ConvertPitch(printRec->PrintPitch);
	lpi = ConvertSpacing(printRec->PrintSpacing);
	
	printRec->Quality = (prefs.PrintQuality == LETTER) ? PRT_NLQ : PRT_DRAFT;

	density = prefs.PrintDensity;
	if (density < 1)
		density = 1;
	else if (density > 7)
		density = 7;
	printRec->PrintDensity = density;
	if (prefs.PrintFlags & ANTI_ALIAS)
		printRec->Flags |= PRT_SMOOTH;
/*
	Set other defaults
*/
	printRec->XScale = printRec->YScale = 100;
	printRec->Flags |= PRT_ASPECTADJ;
	printRec->FontNum = MIN_FONTNUM;
	printRec->Copies = 1;
	printRec->FirstPage = 1;
	printRec->LastPage = 9999;
/*	
	width = (prefs.PaperSize == W_TRACTOR) ? 13*cpi : 8*cpi;

	left = prefs.PrintLeftMargin - 1;
	right = width - prefs.PrintRightMargin;
	if( right < 0 )
		right = 0;
	if( left + right >= width )
		left = right = 0;

	defaultLeftMargin = UNITS_TO_DECIPOINTS(left);
	defaultRightMargin = UNITS_TO_DECIPOINTS(right);
*/

/*
	Validate the print record
*/
	PrValidate(printRec);
}

/*
 *	Set or clear flag in printRec
 */

static void SetPrintFlag(PrintRecPtr printRec, UWORD flagBit, BOOL setIt)
{
	if (setIt)
		printRec->Flags |= flagBit;
	else
		printRec->Flags &= ~flagBit;
}

/*
 *	Calculate new width and height for given page size
 */

static void CalcWidthHeight(pageSize, width, height)
register WORD pageSize, *width, *height;
{
	switch (pageSize) {
	case USLETTER_ITEM:
		*width = 6120;			/* 8.5 in */
		*height = 7920;		/* 11 in */
		break;
	case USLEGAL_ITEM:
		*width = 6120;			/* 8.5 in */
		*height = 10080;		/* 14 in */
		break;
	case A4LETTER_ITEM:
		*width = 5953;			/* 210 mm */
		*height = 8419;		/* 297 mm */
		break;
	case WIDECARRIAGE_ITEM:
		*width = 10080;		/* 14 in */
		*height = 7920;		/* 11 in */
		break;
	}
}

/*
 *	Show new width and height in page setup requester.
 */

static void ShowWidthHeight(PrintRecPtr printRec, WORD width, WORD height)
{
	register RequestPtr req = requester;

#ifdef CHARACTER_UNITS
	NumToString( ( ((long) width - 360) * cpi ) / 720, strBuff );
	SetEditItemText( req->ReqGadget, WIDTH_TEXT, cmdWindow, req, strBuff);
	NumToString( ( ((long) height) * lpi ) / 720, strBuff );
#else
	DecipointToText(width, strBuff, 2);
	SetEditItemText( req->ReqGadget, WIDTH_TEXT, cmdWindow, req, strBuff);
	DecipointToText(height, strBuff, 2);
#endif
	SetEditItemText( req->ReqGadget, HEIGHT_TEXT, cmdWindow, req, strBuff);
}

/*
 *	Page setup request filter
 */

static BOOL PageSetupRequestFilter(intuiMsg, item)
register IntuiMsgPtr intuiMsg;
WORD *item;
{
	register WORD itemHit;
	register ULONG class;
	/* UWORD code = intuiMsg->Code; */
		
	class = intuiMsg->Class;
	if( intuiMsg->IDCMPWindow == cmdWindow && (class == GADGETDOWN || class == GADGETUP ) ) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if( class == GADGETDOWN ) {
			if( itemHit >= WIDTH_TEXT && itemHit <= HEIGHT_TEXT ) {
				ReplyMsg((MsgPtr) intuiMsg);
				SetGadgetItemValue(requester->ReqGadget, PAPERSIZE_POPUP, cmdWindow, requester, CUSTOM_ITEM);
				*item = -1;
				return (TRUE);
			}
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Display and handle page setup dialog in given window
 *	Return success status
 */

BOOL PageSetupRequest()
{
	register PrintRecPtr printRec = &options.PrintInfo.PrintRec;
	register WORD item, offItem, onItem;
	WORD pageSize, width, height, pitch, spacing, charsWidth, charsHeight;
	WORD oldMeasure;
	BOOL success, noGaps;
	WORD strItem;
	register GadgetPtr gadgList;
	register BOOL recalc = FALSE;
	register WindowPtr window = cmdWindow;
	RequestPtr req;

	PrValidate(printRec);		/* Calls GetPrinterType() */
/*
	Get initial settings
*/
	switch(printRec->PageSize) {
	case PRT_USLETTER:
		pageSize = USLETTER_ITEM;
		break;
	case PRT_USLEGAL:
		pageSize = USLEGAL_ITEM;
		break;
	case PRT_A4LETTER:
		pageSize = A4LETTER_ITEM;
		break;
	case PRT_WIDECARRIAGE:
		pageSize = WIDECARRIAGE_ITEM;
		break;
	default:
		pageSize = CUSTOM_ITEM;
		break;
	}
	width = printRec->PaperWidth;
	height = printRec->PaperHeight;

	pitch = printRec->PrintPitch + PICA_RADBTN;
	spacing = printRec->PrintSpacing + SIXLPI_RADBTN;
	
	noGaps = (printRec->Flags & PRT_NOGAPS);
	oldMeasure = options.PrintInfo.MeasureUnit;
	
	reqList[REQ_PAGESETUP]->Gadgets[PRINTERNAME1_TEXT].Info = printerName;
	reqList[REQ_PAGESETUP]->Gadgets[PAPERSIZE_POPUP].Value = pageSize;
	
	success = FALSE;
	BeginWait();
	AutoActivateEnable(FALSE);
	req = DoGetRequest(REQ_PAGESETUP);
	AutoActivateEnable(TRUE);
	if( req == NULL ) {
		EndWait();
		Error(ERR_NO_MEM);
	} else {
		requester = req;
		gadgList = req->ReqGadget;
		OutlineOKButton(window);
		
		/*SetGadgetItemValue(gadgList, PAPERSIZE_POPUP, window, req, pageSize);*/
		EnableGadgetItem(gadgList, NOGAPS_BOX, window, req, !pagePrinter);
		if( noGaps && !pagePrinter )
			GadgetOn(window, NOGAPS_BOX);
		
		GadgetOn(window, pitch);
		GadgetOn(window, spacing);
		GadgetOn(window, oldMeasure + INCH_RADBTN);

		ShowWidthHeight(printRec, width, height);
/*
	Handle dialog
*/
		item = pageSize;
		
		do {
			offItem = onItem = strItem = -1;
						
			switch (item) {
			/*
			case USLETTER_RADBTN:
			case USLEGAL_RADBTN:
			case A4LETTER_RADBTN:
			case WIDECARRIAGE_RADBTN:
				offItem = pageSize;
				pageSize = onItem = item;
				recalc = TRUE;
				break;
			case CUSTOM_RADBTN:
				offItem = pageSize;
				pageSize = onItem = item;
				strItem = WIDTH_TEXT;
				break;
			*/
			case PAPERSIZE_POPUP:
				pageSize = GetGadgetValue(GadgetItem(gadgList, item));
				if( pageSize == CUSTOM_ITEM ) {
					strItem = WIDTH_TEXT;
				} else {
					recalc = TRUE;
				}
				break;				
			case NOGAPS_BOX:
				if(!pagePrinter) {
					ToggleCheckbox(&noGaps, item, window);
				}
				break;
			case PICA_RADBTN:
			case ELITE_RADBTN:
			case CONDENSED_RADBTN:
				offItem = pitch;
				pitch = onItem = item;
				recalc = TRUE;
				break;
			case SIXLPI_RADBTN:
			case EIGHTLPI_RADBTN:
				offItem = spacing;
				spacing = onItem = item;
				recalc = TRUE;
				break;
			case INCH_RADBTN:
			case CM_RADBTN:
/*
 * We directly manipulate the prefs structure so ShowWidthHeight() will get it.
 */
				offItem = options.PrintInfo.MeasureUnit + INCH_RADBTN;
				options.PrintInfo.MeasureUnit = item - INCH_RADBTN;
				onItem = item;
				recalc = TRUE;
				break;
			}	
			if( recalc && ( pageSize != CUSTOM_ITEM ) ) {
				recalc = FALSE;
				CalcWidthHeight( pageSize, &width, &height ) ;
				ShowWidthHeight( printRec, width, height ) ;
			}
			if( item != -1 ) {
				if (offItem != -1)
					GadgetOff(window, offItem);
				if (onItem != -1)
					GadgetOn(window, onItem);
				if( strItem > 0 ) {
					ActivateGadget(GadgetItem(gadgList, strItem), window, req);
				}
			}
			item = ModalRequest(mainMsgPort, window, PageSetupRequestFilter);
		} while( (item != OK_BUTTON) && (item != CANCEL_BUTTON) ); 

#ifdef CHARACTER_UNITS
		GetEditItemText(gadgList, WIDTH_TEXT, strBuff);
		charsWidth = StringToNum(strBuff);
		GetEditItemText(gadgList, HEIGHT_TEXT, strBuff);
		charsHeight = StringToNum(strBuff);
#else
		width = GetFracValue(gadgList, WIDTH_TEXT);
		height = GetFracValue(gadgList, HEIGHT_TEXT);
#endif
		DestroyRequest(req);
		EndWait();
		if(item != CANCEL_BUTTON) {
/*
	Save options
*/
			if (pitch == PICA_RADBTN)
				printRec->PrintPitch = PRT_PICA;
			else if (pitch == ELITE_RADBTN)
				printRec->PrintPitch = PRT_ELITE;
			else
				printRec->PrintPitch = PRT_CONDENSED;
			printRec->PrintSpacing = (spacing == SIXLPI_RADBTN) ? PRT_SIXLPI : PRT_EIGHTLPI;
			cpi = ConvertPitch(printRec->PrintPitch);
			lpi = ConvertSpacing(printRec->PrintSpacing);

#ifdef CHARACTER_UNITS
			width = (((long)charsWidth) * 720 / cpi) + 360;
			height = ((long)charsHeight) * 720 / lpi;	  
#else
			charsWidth = ((width - 360) * cpi) / 720 ;
			charsHeight = (height * lpi) / 720 ;
#endif
			switch(pageSize) {
			case USLETTER_ITEM:
				pageSize = PRT_USLETTER;
				break;
			case USLEGAL_ITEM:
				pageSize = PRT_USLEGAL;
				break;
			case A4LETTER_ITEM:
				pageSize = PRT_A4LETTER;
				break;
			case WIDECARRIAGE_ITEM:
				pageSize = PRT_WIDECARRIAGE;
				break;
			default:
				pageSize = PRT_CUSTOM;
				break;
			}
			printRec->PageSize = pageSize;

			if (width < MIN_PAPERWIDTH || height < MIN_PAPERHEIGHT ||
				(!noGaps && height < MIN_PAPERHEIGHT + 720)) {
				Error(ERR_PAGE_SIZE);		/* Will be fixed by PrValidate */
			} else {
				options.PrintInfo.PageWidth = charsWidth;
				options.PrintInfo.PageHeight = charsHeight;
				printRec->PaperWidth = width;
				printRec->PaperHeight = height;
			}

			SetPrintFlag(printRec, PRT_NOGAPS, noGaps);			
/*
	Set paper and page rectangles
*/
			PrValidate(printRec);
			success = TRUE;
		} else {
			options.PrintInfo.MeasureUnit = oldMeasure;
		}
	}
	return(success);
}

/* 
 * Return the numeric value of characters per inch corresponding to the pitch.
 */
 
static WORD ConvertPitch(UBYTE pitch)
{
	register WORD val;
	
	if( pitch == PRT_CONDENSED ) {
		val = 17;
	} else if( pitch == PRT_ELITE ) {
		val = 12;
	} else {
		val = 10;
	}
	return(val);
}

/*
 * Return the numeric value of lines per inch corresponding to the spacing value.
 */
 
static WORD ConvertSpacing(UBYTE spacing)
{
	if( spacing == PRT_SIXLPI ) {
		return( 6 );
	} else {
		return( 8 );
	}
}

/*
 *	Print dialog filter
 */

static BOOL PrintRequestFilter(intuiMsg, item)
register IntuiMsgPtr intuiMsg;
WORD *item;
{
	register WORD itemHit;
	register ULONG class = intuiMsg->Class;
	register UWORD code = intuiMsg->Code;

	if( intuiMsg->IDCMPWindow == cmdWindow ) {
		switch(class) {
		case GADGETDOWN:
		case GADGETUP:	
			itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
			if( itemHit == FONTUP_ARROW || itemHit == FONTDOWN_ARROW ) {
				ReplyMsg((MsgPtr) intuiMsg);
				*item = (class == GADGETDOWN) ? itemHit : -1;
				return (TRUE);
			}
		case RAWKEY:
			if( code == CURSORUP || code == CURSORDOWN /*&&
				 (intuiMsg->Qualifier & IEQUALIFIER_REPEAT) == 0*/ ) {
				ReplyMsg((MsgPtr) intuiMsg);
				itemHit = (code == CURSORUP) ? FONTUP_ARROW : FONTDOWN_ARROW;
				if( DepressGadget(GadgetItem(requester->ReqGadget, itemHit), cmdWindow, requester))
					*item = itemHit;
				return(TRUE);
			}
			break;
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 * Update the text of the print request's default button, to read "Print" or "Save".
 */
 
static void UpdatePrintButton( UWORD dest )
{
	TextPtr textPtr;
	TextChar cmdKey;
	
	if( dest == TOFILE_RADBTN ) {
		textPtr = strSave;
		cmdKey = CMD_KEY_SAVE;
	} else {
		textPtr = strPrint;
		cmdKey = CMD_KEY_PRINT;
	}
	SetButtonItem( requester->ReqGadget, OK_BUTTON, cmdWindow, requester, textPtr, cmdKey);
}		

/*
 *	Display and handle print dialog
 *	Return success status
 */

BOOL PrintRequest()
{
	register WORD item, copies;
	register WORD offItem, onItem;
	WORD quality, feedItem, fontNum;
	BOOL done;
	UWORD destination;
	PrintRecPtr printRec = &options.PrintInfo.PrintRec;
	register GadgetPtr gadget, gadgList;
	SFReply sfReply;
	WindowPtr window = cmdWindow;
	register RequestPtr req;
	register BOOL success = FALSE;
	
	PrValidate(printRec);		/* Calls GetPrinterType() */
/*
	Get initial settings
*/
	copies = printRec->Copies;
	fontNum = printRec->FontNum;

	quality = (printRec->Quality == PRT_NLQ) ? NLQ_RADBTN : DRAFT_RADBTN;

	feedItem = (printRec->PaperFeed == PRT_CONTINUOUS) ?
				AUTOMATIC_RADBTN : HANDFEED_RADBTN;
	
	if( printRec->Flags & PRT_TOFILE ) {
		destination = TOFILE_RADBTN;
		reqList[REQ_PRINT]->Gadgets[OK_BUTTON].Info = &strSave[0];
	} else {
		destination = TOPRINTER_RADBTN;
		reqList[REQ_PRINT]->Gadgets[OK_BUTTON].Info = &strPrint[0];
	}
/*
	Get dialog and set initial values
*/
	BeginWait();
	AutoActivateEnable(FALSE);
	req = DoGetRequest(REQ_PRINT);
	AutoActivateEnable(TRUE);
	if( req != NULL ) {
		requester = req;
		OutlineOKButton(window);
		gadgList = req->ReqGadget;
		NumToString(fontNum, strBuff);
		SetGadgetItemText(gadgList, FONTNUM_TEXT, window, req, strBuff);
		if (fontNum == MIN_FONTNUM) {
			gadget = GadgetItem(gadgList, FONTDOWN_ARROW);
			OffGList(gadget, window, req, 1);
		}
		if (fontNum == MAX_FONTNUM) {
			gadget = GadgetItem(gadgList, FONTUP_ARROW);
			OffGList(gadget, window, req, 1);
		}
		SetGadgetItemText(gadgList, PRINTERNAME2_TEXT, window, req, printerName);
		GadgetOn(window, quality);
		GadgetOn(window, feedItem);
		GadgetOn(window, destination);
		NumToString(copies, strBuff);
		SetEditItemText(gadgList, COPIES_TEXT, window, req, strBuff);
/*
	Handle dialog
*/
		done = FALSE;
		do {
			offItem = onItem = -1;
			item = ModalRequest(mainMsgPort, window, PrintRequestFilter);
			switch (item) {
			case OK_BUTTON:
				sfReply.Result = SFP_OK;
				if( destination == TOFILE_RADBTN ) {
					OpenSaveAsDialog(&sfReply, strReportName);
					printRec->FileDir = sfReply.DirLock;
					strcpy(printRec->FileName, sfReply.Name);
				} else {
					strcpy(printRec->FileName, strPRT);
				}
				if( sfReply.Result == SFP_OK ) {
					GetEditItemText(gadgList, COPIES_TEXT, strBuff);
					copies = CheckNumber(strBuff) ? StringToNum(strBuff) : -1;
					if (copies <= 0) {
						Error(ERR_BAD_COPIES);
						break;
					}
				} else {
					break;
				}
				success = TRUE;
			case CANCEL_BUTTON:
				done = TRUE;
				break;
			case NLQ_RADBTN:
			case DRAFT_RADBTN:
				offItem = quality;
				quality = onItem = item;
				break;
			case AUTOMATIC_RADBTN:
			case HANDFEED_RADBTN:
				offItem = feedItem;
				feedItem = onItem = item;
				break;
			case TOFILE_RADBTN:
			case TOPRINTER_RADBTN:
				offItem = destination;
				destination = onItem = item;
				UpdatePrintButton(item);
				break;
			case FONTUP_ARROW:
			case FONTDOWN_ARROW:
				if (item == FONTUP_ARROW) {
					if (fontNum < MAX_FONTNUM)
						fontNum++;
				}
				else {
					if (fontNum > MIN_FONTNUM)
						fontNum--;
				}
				NumToString(fontNum, strBuff);
				SetGadgetItemText(gadgList, FONTNUM_TEXT, window, req, strBuff);
				gadget = GadgetItem(gadgList, FONTDOWN_ARROW);
				if (fontNum == MIN_FONTNUM)
					OffGList(gadget, window, req, 1);
				else
					OnGList(gadget, window, req, 1);
				gadget = GadgetItem(gadgList, FONTUP_ARROW);
				if (fontNum == MAX_FONTNUM)
					OffGList(gadget, window, req, 1);
				else
					OnGList(gadget, window, req, 1);
				break;
			}
			if (!done && item != -1) {
				if (offItem != -1)
					GadgetOff(window, offItem);
				if (onItem != -1)
					GadgetOn(window, onItem);
			}
		} while (!done);
		DestroyRequest(req);
		if( success ) {
			printRec->Quality = (quality == NLQ_RADBTN) ? PRT_NLQ : PRT_DRAFT;
			printRec->PaperFeed = (feedItem == AUTOMATIC_RADBTN) ? PRT_CONTINUOUS : PRT_CUTSHEET;
			SetPrintFlag(printRec, PRT_TOFILE, destination == TOFILE_RADBTN);
			printRec->FontNum = fontNum;
			printRec->Copies = copies;			
		}
	} else {
		Error(ERR_NO_MEM);
	}
	EndWait();
	return(success);
}

/*
 *	Set print option of given option name
 *	If printRec is NULL, set default print record
 *	Used for AREXX macros
 *	Return FALSE if not a valid option name
 */

LONG SetPrintOption(TextPtr optName)
{
	register WORD i;
	PrintRecPtr printRec = &options.PrintInfo.PrintRec;
	TextChar buff[256];
	UWORD len;
	LONG result = RC_OK;

	do {	
		GetNextWord(optName, buff, &len);
		optName += len;
		for (i = 0; i < NUM_OPTIONS; i++) {
			if (CmpString(buff, printOptNames[i], strlen(buff), (WORD) strlen(printOptNames[i]), FALSE) == 0) {
				break;
			}
		}
		if( i >= NUM_OPTIONS ) {
			result = RC_WARN;
		} else {
			switch (i) {
			case OPT_NLQ:
				printRec->Quality = PRT_NLQ;
				break;
			case OPT_DRAFT:
				printRec->Quality = PRT_DRAFT;
				break;
			case OPT_AUTOFEED:
				printRec->PaperFeed = PRT_CONTINUOUS;
				break;
			case OPT_HANDFEED:
				printRec->PaperFeed = PRT_CUTSHEET;
				break;
			}
		}
	} while(*optName);
	PrValidate(printRec);
	return(result);
}
