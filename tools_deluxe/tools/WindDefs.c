/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Window, screen, and window gadget definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Image.h>
#include <Toolbox/Border.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/ScrollList.h>

#include "Tools.h"

/*#define BEST_POSSIBLE_ARROWS*/
/*
 *	External variables
 */

extern TextChar		screenTitle[];
extern TextChar		strStart[], strStop[], strAbort[], strVolumes[], strDevices[];
extern TextChar		strSave[], strBlockLabel[], strHex[], strDec[];
extern TextChar		strTag[], strUntag[], strBack[], strEnter[];
extern TextChar		strOptions[];

#define TOOL_BUTTON_WIDTH	(STD_BUTTON_WIDTH+10)
#define EDIT_BUTTON_WIDTH	TOOL_BUTTON_WIDTH
#define UNDEL_BUTTON_WIDTH	(TOOL_BUTTON_WIDTH+8)

/*
 *	Screen definition
 */

struct NewScreen newScreen = {
	0, 0, 640, STDSCREENHEIGHT, 2, 0, 1,
	HIRES, WBENCHSCREEN,
	NULL, &screenTitle[0], NULL, NULL
};

/*
 *	Window definitions
 */

struct NewWindow newBackWindow = {
	0, 0, 640, 200, -1, -1,
	MOUSEBUTTONS | MENUPICK | REFRESHWINDOW | ACTIVEWINDOW | INACTIVEWINDOW |
		RAWKEY | NEWPREFS,
	SIMPLE_REFRESH | BACKDROP | BORDERLESS | ACTIVATE,
	NULL, NULL, NULL, NULL, NULL,
	0, 0, 0, 0,
	CUSTOMSCREEN
};

struct NewWindow newMainWindow = {
	0, 0, 327, 250, -1, -1,
	0, /* IDCMP  flags set in Window.c */
	0, /* Window flags set in Window.c */
	NULL, NULL, NULL, NULL, NULL,
	220, 200, 0xFFFF, 0xFFFF,
	CUSTOMSCREEN
};

struct NewWindow newToolWindow = {
	0, 0, 640, 250, -1, -1,
	0, /* IDCMP  flags set in Window.c */
	0,	/* Window flags set in Window.c */
	NULL, NULL, NULL, NULL, NULL,
	220, 200, 0xFFFF, 0xFFFF,
	CUSTOMSCREEN
};

TextPtr		baseList[] = {
	strHex, strDec, NULL
};

#define MAIN_NUM	4
#define MAIN_X		15
#define MAIN_Y		197
#define MAIN_WIDTH	296

GadgetTemplate modeGadgets[] = {
	{ GADG_STAT_TEXT, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,MAIN_X,MAIN_Y-17, 0, 0, 0, 0, 0, 0, 0, 0, "Available:" },
		
	{ GADG_ACTIVE_IMAGE, MAIN_X,	 36,10, 0, TOOLS_ICON_WIDTH, TOOLS_ICON_HEIGHT,  0,  0, 0, 0, NULL },
	{ GADG_ACTIVE_IMAGE, MAIN_X,	 68,10, 0, TOOLS_ICON_WIDTH, TOOLS_ICON_HEIGHT,  0,  0, 0, 0, NULL },
	{ GADG_ACTIVE_IMAGE, MAIN_X,	100,10, 0, TOOLS_ICON_WIDTH, TOOLS_ICON_HEIGHT,  0,  0, 0, 0, NULL },
	{ GADG_ACTIVE_IMAGE,	MAIN_X,	132,10, 0, TOOLS_ICON_WIDTH, TOOLS_ICON_HEIGHT,  0,  0, 0, 0, NULL },
	
#ifdef BEST_POSSIBLE_ARROWS
#define ARRWD	(ARROW_WIDTH)
#define ARRHT	(ARROW_HEIGHT)
	{ GADG_ACTIVE_BORDER,   0,-4*11,21,-10, 0,4*11,(-42)-ARRWD, 0, 0, 0, NULL },
	{ GADG_ACTIVE_STDIMAGE, 0,  0,-20-ARRWD,-8-2*ARRHT,0,  0, ARRWD, ARRHT, 28, 0,(Ptr)IMAGE_ARROW_UP },
	{ GADG_ACTIVE_STDIMAGE, 0,  0,-20-ARRWD,-8-ARRHT,  0,  0, ARRWD, ARRHT, 29, 0,(Ptr)IMAGE_ARROW_DOWN },
	{ GADG_PROP_VERT | GADG_PROP_NEWLOOK, 0,-4*11, -19-ARRWD,-10, 0,4*11, ARRWD-2, (-2*ARRHT), 0, 0, NULL },
#else
	SL_GADG_BOX(MAIN_X, MAIN_Y, MAIN_WIDTH, MAIN_NUM),
	SL_GADG_UPARROW(MAIN_X, MAIN_Y, MAIN_WIDTH, MAIN_NUM),
	SL_GADG_DOWNARROW(MAIN_X, MAIN_Y, MAIN_WIDTH, MAIN_NUM),
	SL_GADG_SLIDER(MAIN_X, MAIN_Y, MAIN_WIDTH, MAIN_NUM),
#endif
#ifdef POPUP_VOLDEV
	{ GADG_POPUP, MAIN_X+80, MAIN_Y-17, 0, 0, 80, 11, 0, 0, 0, 0, &getDevList },
#else
	{ GADG_RADIO_BUTTON, MAIN_X+88,MAIN_Y-17, 0, 0, 0, 0, 0, 0, 'V', 0, strVolumes },
	{ GADG_RADIO_BUTTON, MAIN_X+176,MAIN_Y-17, 0, 0, 0, 0, 0, 0, 'D', 0, strDevices },
#endif
	{ GADG_STAT_STDBORDER, MAIN_X, 25, 0, 0, MAIN_WIDTH, 143, 0, 0, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_ITEM_NONE }
};

/*
 * Repair tool gadgets
 */
 
GadgetTemplate repairGadgets[] = {
	{ GADG_PUSH_BUTTON,	  15, 24, 0, 0, TOOL_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_START, 0, strStart },
	{ GADG_PUSH_BUTTON,	  15+(1*(TOOL_BUTTON_WIDTH+12)), 24, 0, 0, TOOL_BUTTON_WIDTH, 20, 0, 0, 'A', 0, strAbort },
	{ GADG_PUSH_BUTTON,	  15+(2*(TOOL_BUTTON_WIDTH+12)), 24, 0, 0, TOOL_BUTTON_WIDTH, 20, 0, 0, 'O', 0, strOptions },

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1-ARROW_WIDTH, 1-ARROW_HEIGHT,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_LEFT },
	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1-ARROW_WIDTH, 1-ARROW_HEIGHT,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_RIGHT },
	{ GADG_PROP_HORIZ | GADG_PROP_NEWLOOK,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_ACTIVE_BORDER,
		0,147, 5, -1,
		0,-147, -7, 0, 0, 0, NULL },
		
	{ GADG_ACTIVE_STDIMAGE,
		0, 0, -19, -30,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_UP },
	{ GADG_ACTIVE_STDIMAGE,
		0, 0, -19, -20,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_DOWN },
	{ GADG_PROP_VERT | GADG_PROP_NEWLOOK,
		0,147, 1 - ARROW_WIDTH + 4, -2,
		0,-147, ARROW_WIDTH - 8, 1, 0, 0, NULL },
		
	{ GADG_STAT_STDBORDER,	15, 53, 0, 0, 234, 26, 0, 2, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_BORDER,		15, 90, 0, 0, 234, 22, 0, 2, 0, 0, NULL },
	{ GADG_STAT_BORDER,		74, 90, 0, 0, 175, 22, 0, 2, 0, 0, NULL },
	{ GADG_STAT_STDBORDER,	15,117, 0, 0, 234, 18, 0, 0, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_ITEM_NONE }
};

/*
 * Recover tool gadgets
 */
 
TextPtr		destList[] = {
	"Same volume", "Different volume:", NULL
};

GadgetTemplate recoverGadgets[] = {
	{ GADG_PUSH_BUTTON,	  32+170, 20, 0, 0, UNDEL_BUTTON_WIDTH, 20, 0, 0, 'S', 0, "Scan" },
	{ GADG_PUSH_BUTTON,	  32+170+(1*(UNDEL_BUTTON_WIDTH+12)), 20, 0, 0, UNDEL_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_ENTER, 0, strEnter },
	{ GADG_PUSH_BUTTON,	  32+170+(1*(UNDEL_BUTTON_WIDTH+12)), 47, 0, 0, UNDEL_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_BACK, 0, strBack },
	
	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1-ARROW_WIDTH, 1-ARROW_HEIGHT,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_LEFT },
	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1-ARROW_WIDTH, 1-ARROW_HEIGHT,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_RIGHT },
	{ GADG_PROP_HORIZ | GADG_PROP_NEWLOOK,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_ACTIVE_BORDER,
		0,(91+34), 5, -1,
		0,-(91+34), -7, 0, 0, 0, NULL },
		
	{ GADG_ACTIVE_STDIMAGE,
		0, 0, -19, -30,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_UP },
	{ GADG_ACTIVE_STDIMAGE,
		0, 0, -19, -20,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_DOWN },
	{ GADG_PROP_VERT | GADG_PROP_NEWLOOK,
		0,91+34, 1 - ARROW_WIDTH + 4, -2,
		0,-(91+34), ARROW_WIDTH - 8, 1, 0, 0, NULL },

	{ GADG_PUSH_BUTTON,	  32+170, 47, 0,  0, UNDEL_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_OPTIONS, 0, strOptions },
	{ GADG_PUSH_BUTTON,	  32+170+(2*(UNDEL_BUTTON_WIDTH+12)), 20, 0, 0, UNDEL_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_TAG, 0, strTag },
	{ GADG_PUSH_BUTTON,	  32+170+(2*(UNDEL_BUTTON_WIDTH+12)), 47, 0, 0, UNDEL_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_UNTAG, 0, strUntag },

	{ GADG_STAT_TEXT,			12, 81, 0, 0,  0, 0, 0, 0, 0, 0, "Recover to:" },
	{ GADG_POPUP, 		  119, 81, 2, 0,159,11, 0, 0, 0, 0, &destList },
	{ GADG_EDIT_TEXT,		  32+170+(1*(UNDEL_BUTTON_WIDTH+12)), 81, 2, 0,162,11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_BORDER,		12, 72+33, 0,  3, 0,11,0, 0, 0, 0, NULL },
	{ GADG_STAT_STDBORDER, 119, 22, 0, 0, 69,11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER, 119, 47, 0, 0, 69,11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },

	{ GADG_STAT_TEXT,			12, 22, 0, 3,  0, 0, 0, 0, 0, 0, "Tagged files:" },
	{ GADG_STAT_TEXT,			12, 47, 0, 3,  0, 0, 0, 0, 0, 0, "Total files:" },
	{ GADG_ITEM_NONE }
};

/*
 * Reorg tool gadgets
 */
 
#define BAR_WIDTH	0 /*((2*(TOOL_BUTTON_WIDTH+12))+TOOL_BUTTON_WIDTH)*/

GadgetTemplate reorgGadgets[] = {
	{ GADG_PUSH_BUTTON,	   0, 24, 15, 0, TOOL_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_START, 0, strStart },

#ifdef INCLUDE_SECS
	{ GADG_STAT_STDBORDER,  (2*(TOOL_BUTTON_WIDTH+12)),24, 16, 1, TOOL_BUTTON_WIDTH-1, 18, 0, 0, 0, 0, (Ptr) BORDER_SHADOWBOX },
#else
	{ GADG_STAT_STDBORDER,  (2*(TOOL_BUTTON_WIDTH+12)),24, 16, 1, TOOL_BUTTON_WIDTH-25, 18, 0, 0, 0, 0, (Ptr) BORDER_SHADOWBOX },
#endif
/*	{ GADG_PUSH_BUTTON,	   (1*(TOOL_BUTTON_WIDTH+12)),24, 15, 0, TOOL_BUTTON_WIDTH, 20, 0, 0, 'A', 0, &strAbort[0] },*/

	{ GADG_PUSH_BUTTON,	   (1*(TOOL_BUTTON_WIDTH+12)),24, 15, 0, TOOL_BUTTON_WIDTH, 20, 0, 0, 'O', 0, strOptions },
	{ GADG_STAT_BORDER, 	   0, 55+13, 15, 0, 0,  0,514, 0, 0, 0, NULL },
	{ GADG_STAT_BORDER,		0, 55,    15, 0, 0, 11,  0, 0, 0, 0, NULL },
	{ GADG_STAT_STDBORDER,	-BAR_WIDTH, 24, 0/*527*/, 1, BAR_WIDTH, 18, 0, 0, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_ITEM_NONE }
};

/*
 * Disk editor tool gadgets
 */
 
GadgetTemplate editGadgets[] = {
	{ GADG_STAT_STDBORDER, 71, 44, 0, 0, 77, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },

	{ GADG_PUSH_BUTTON,	-(10+(3*(EDIT_BUTTON_WIDTH+12))), 42, 0, 0, EDIT_BUTTON_WIDTH, 20, 0, 0, 0, 0, "Re-Read" },
	{ GADG_PUSH_BUTTON,	-(10+(2*(EDIT_BUTTON_WIDTH+12))), 42, 0, 0, EDIT_BUTTON_WIDTH, 20, 0, 0, 0, 0, "Write" },
	{ GADG_PUSH_BUTTON,	-(10+(1*(EDIT_BUTTON_WIDTH+12))), 42, 0, 0, EDIT_BUTTON_WIDTH, 20, 0, 0, 'G', 0, "Go" },

	{ GADG_ACTIVE_STDIMAGE,
		471, 22, 1-(2*ARROW_WIDTH), 0,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_LEFT },
	{ GADG_ACTIVE_STDIMAGE,
		471, 22, 1-ARROW_WIDTH, 0,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_RIGHT },
	{ GADG_PROP_HORIZ | GADG_PROP_NOBORDER,
		20, 22, 1, 1,
		451, 0, -(2*ARROW_WIDTH), ARROW_HEIGHT, 0, 0, NULL },
	{ GADG_ACTIVE_BORDER,  0, 73+22, -445, 0, 0, 0, 442, 135, 0, 0, NULL },

	{ GADG_ACTIVE_STDIMAGE, 
		0, 0, -18, -29,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_UP },
	{ GADG_ACTIVE_STDIMAGE,
		0, 0, -18, -19,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0, (Ptr) IMAGE_ARROW_DOWN },
	{ GADG_PROP_VERT | GADG_PROP_NOBORDER,
		0, 73+22, 0, 1,
		0,-(72+22), 0, 0, 0, 0, NULL },
	
	{ GADG_POPUP, 153, 47, 0, 1, 46, 11, 0, 0, 0, 0, &baseList },

	{ GADG_STAT_TEXT, 74, 44+25, 0, 3, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,241, 44+25, 0, 3, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDBORDER, 71, 44+25, 0, 0, 77, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,238, 44+25, 0, 0,234, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX }, 
	{ GADG_STAT_TEXT, 20, 44+25, 0, 3, 0, 0, 0, 0, 0, 0, "Type:" },
	{ GADG_STAT_TEXT,186, 44+25, 0, 3, 0, 0, 0, 0, 0, 0, "Field:" },

	{ GADG_STAT_TEXT, 20, 44, 0, 3, 0, 0, 0, 0, 0, 0, strBlockLabel },
	{ GADG_ITEM_NONE }
};

