/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Dialog/requester definitions
 */

#include <exec/types.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/Request.h>
#include <Toolbox/Image.h>
#include <Toolbox/Border.h>
#include <Toolbox/Language.h>

#include "Tools.h"

/*
 *	External references
 */

extern TextChar macroNames2[10][MACRONAME_LEN];
extern TextChar strPrint[], strFind[];
extern TextChar strEnter[], strBack[], strDisks[], strK[], strStop[], strSkip[];
extern TextChar _strOK[], _strCancel[], _strYes[], _strNo[];

extern TextPtr  baseList[];

/*
 *	Statics
 */

/*
static TextChar strAvailMem[]	= "Available memory:";
static TextChar strExpansion[]= "Expansion";
static TextChar strGraphics[]	= "Graphics";
*/
static TextChar strCustom[]	= "Custom";
static TextChar strUse[]		= "Use";
static TextChar strReset[]		= "Reset";
/*
static TextChar strNext[]		= "Next";
static TextChar strPrev[]		= "Prev";
*/
static TextChar strBlocks[]	= "blocks";

#define DLIST_WIDTH		(28*8)
#define OPEN_NUM		10

#define SL_GADG_BOX(left, top, width, num)	\
	{ GADG_ACTIVE_BORDER, left, top, 0, 0, width-ARROW_WIDTH, (num)*11, -1, num, NULL }

#define SL_GADG_UPARROW(left, top, width, num)	\
	{ GADG_ACTIVE_STDIMAGE,														\
		(left)+(width)-ARROW_WIDTH, (top)+(num)*11-2*ARROW_HEIGHT, 0, (num)+1,	\
		ARROW_WIDTH, ARROW_HEIGHT, 0, 0, 0, 0, (Ptr) IMAGE_ARROW_UP }

#define SL_GADG_DOWNARROW(left, top, width, num)	\
	{ GADG_ACTIVE_STDIMAGE,														\
		(left)+(width)-ARROW_WIDTH, (top)+(num)*11-ARROW_HEIGHT, 0, (num)+1,	\
		ARROW_WIDTH, ARROW_HEIGHT, 0, 0, 0, 0, (Ptr) IMAGE_ARROW_DOWN }

#define SL_GADG_SLIDER(left, top, width, num)	\
	{ GADG_PROP_VERT | GADG_PROP_NOBORDER,		\
		(left)+(width)-ARROW_WIDTH, top, 1, 0,	\
		ARROW_WIDTH, (num)*11-2*ARROW_HEIGHT, -2, num, NULL }
 
#define MY_ARROW_WIDTH	(ARROW_WIDTH-6)
#define MY_ARROW_HEIGHT	(ARROW_HEIGHT-2)

#define GT_UPARROW(left, top, key)	\
	{ GADG_ACTIVE_STDIMAGE,										\
		left, 1 + (top) + (MY_ARROW_HEIGHT / 2) - MY_ARROW_HEIGHT, 0, 0, MY_ARROW_WIDTH, MY_ARROW_HEIGHT, 0, 0,\
		key, 0, (Ptr) IMAGE_ARROW_UP }

#define GT_DNARROW(left, top, key)	\
	{ GADG_ACTIVE_STDIMAGE,										\
		left, 2 + (top) + (MY_ARROW_HEIGHT / 2), 0, 0, MY_ARROW_WIDTH, MY_ARROW_HEIGHT, 0, 0,	\
		key, 0, (Ptr) IMAGE_ARROW_DOWN }

#define GT_TEXT(left, top)	\
	{ GADG_STAT_TEXT, (left)+10+ARROW_WIDTH, top, 0, 0, 0, 0, 0, 0, 0, 0, NULL }


/*
	 Page Setup requester
*/

/*
static PopUpInfo	paperList = 
#if (AMERICAN | BRITISH | GERMAN)
	{ 5,"US Letter" };
#elif FRENCH
	{ 5 "Lettre US" };
#endif
*/

static TextPtr		paperList[] =
#if (AMERICAN | BRITISH)
	{ "US Letter", "US Legal", "A4 Letter",	"Wide Carriage", strCustom, NULL };
#elif GERMAN
	{ "US Letter", "US Legal", "DIN A4", "Breitformat", strCustom, NULL };
#elif FRENCH
	{ "Lettre US", "US L�gal", "Lettre A4", "Tracteur large" strCustom, NULL };
#endif

#if (AMERICAN | BRITISH)
static GadgetTemplate pageSetupGadgets[] = {
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20), 10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_OK,0, &_strOK[0] },
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20), 40, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_CANCEL,0, &_strCancel[0] },

	{ GADG_POPUP,		116, 40, 0, 0,130, 11, 0, 0, 0, 0, &paperList },
		
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 171+10, 70-8, 0, 0, 48, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 292-111, 70+14, 0, 0, 48, 11, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON,	 94,104+9, 0, 0,  0,  0, 0, 0,'P',0, "Pica" },
	{ GADG_RADIO_BUTTON,	168,104+9, 0, 0,  0,  0, 0, 0,'E',0, "Elite" },
	{ GADG_RADIO_BUTTON,	242,104+9, 0, 0,  0,  0, 0, 0,'d',0, "Condensed" },
		
	{ GADG_RADIO_BUTTON,	 94,124+9, 0, 0,  0,  0, 0, 0,'6',0, "6 LPI" },
	{ GADG_RADIO_BUTTON,	168,124+9, 0, 0,  0,  0, 0, 0,'8',0, "8 LPI" },
		
	{ GADG_CHECK_BOX,		 94,164+9, 0, 0,  0,  0, 0, 0,'N',0, "No Gaps Between Pages" },
	
	{ GADG_STAT_TEXT,		134, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	
	{ GADG_RADIO_BUTTON,	 94,144+9, 0, 0,  0,  0, 0, 0,'I',0, "Inches" },
	{ GADG_RADIO_BUTTON, 168,144+9, 0, 0,  0,  0, 0, 0,'m',0, "Centimeters" },
		
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Page Setup" },
	{ GADG_STAT_TEXT,		 20, 40, 0, 0,  0,  0, 0, 0, 0, 0, "Paper size:" },
	{ GADG_STAT_TEXT,		116, 62, 0, 0,  0,  0, 0, 0, 0, 0, "width:" },
	{ GADG_STAT_TEXT,		232-116, 84, 0, 0,  0,  0, 0, 0, 0, 0, "height:" },
	{ GADG_STAT_TEXT,		 20,104+9, 0, 0,  0,  0, 0, 0, 0, 0, "Pitch:" },
	{ GADG_STAT_TEXT,		 20,124+9, 0, 0,  0,  0, 0, 0, 0, 0, "Spacing:" },
	{ GADG_STAT_TEXT,		 20,164+9, 0, 0,  0,  0, 0, 0, 0, 0, "Options:" },
	{ GADG_STAT_TEXT,		 20,144+9, 0, 0,  0,  0, 0, 0, 0, 0, "Units:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,278,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate pageSetupReqT = {
	-1, -1, 398, 184+9, &pageSetupGadgets[0]
};

#elif GERMAN

static GadgetTemplate pageSetupGadgets[] = {
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20), 10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_OK,0, &_strOK[0] },
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20), 40, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_CANCEL,0, &_strCancel[0] },

	{ GADG_POPUP,		116, 40, 0, 0,130, 11, 0, 0, 0, 0, &paperList },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 291, 74, 0, 0, 48, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 399, 74, 0, 0, 48, 11, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON,	150,104, 0, 0,	 0,  0, 0, 0,'P',0, "Pica (10 Zeichen/Inch)" },
	{ GADG_RADIO_BUTTON,	150,119, 0, 0,	 0,  0, 0, 0,'E',0, "Elite (12 Zeichen/Inch)" },
	{ GADG_RADIO_BUTTON,	150,134, 0, 0,	 0,  0, 0, 0,'K',0, "Komrimiert (17 Zeichen/Inch)" },

	{ GADG_RADIO_BUTTON,	150,154, 0, 0,	 0,  0, 0, 0,'6',0, "6 Zeilen/Inch" },
	{ GADG_RADIO_BUTTON,	284,154, 0, 0,	 0,  0, 0, 0,'8',0, "8 Zeilen/Inch" },

	{ GADG_CHECK_BOX,		150,209, 0, 0,	 0,  0, 0, 0,'L',0, "Keine L�cken zwischen den Seiten" },

	{ GADG_STAT_TEXT,		230, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON,	150,174, 0, 0,	 0,  0, 0, 0,'I',0, "Inch" },
	{ GADG_RADIO_BUTTON, 284,174, 0, 0,	 0,  0, 0, 0,'Z',0, "Zentimeter" },

	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Druckseiteneinstellungen" },
	{ GADG_STAT_TEXT,		 20, 38, 0, 0,  0,  0, 0, 0, 0, 0, "Papier format" },
	{ GADG_STAT_TEXT,		230, 74, 0, 0,  0,  0, 0, 0, 0, 0, "Breite:" },
	{ GADG_STAT_TEXT,		354, 74, 0, 0,  0,  0, 0, 0, 0, 0, "H�he:" },
	{ GADG_STAT_TEXT,		 20,104, 0, 0,  0,  0, 0, 0, 0, 0, "Zeichenabstand:" },
	{ GADG_STAT_TEXT,		 20,154, 0, 0,  0,  0, 0, 0, 0, 0, "Zeilenabstand:" },
	{ GADG_STAT_TEXT,		 20,194, 0, 0,  0,  0, 0, 0, 0, 0, "Zus�tzliche Einstellungen:" },
	{ GADG_STAT_TEXT,		 20,174, 0, 0,  0,  0, 0, 0, 0, 0, "Ma�einheiten:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,350,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static DialogTemplate pageSetupReqT = {
	-1, -1, 470, 224+8, &pageSetupGadgets[0]
};

#elif FRENCH

static GadgetTemplate pageSetupGadgets[] = {
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20), 10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_OK,0, &_strOK[0] },
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20), 40, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_CANCEL,0, &_strCancel[0] },
	
	{ GADG_POPUP,		116, 40, 0, 0,130, 11, 0, 0, 0, 0, &paperList },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 216, 74, 0, 0, 48, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 338, 74, 0, 0, 48, 11, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON,	 90,104, 0, 0,  0,  0, 0, 0,'P',0, "Pica" },
	{ GADG_RADIO_BUTTON,	164,104, 0, 0,  0,  0, 0, 0,'i',0, "Elite" },
	{ GADG_RADIO_BUTTON,	238,104, 0, 0,  0,  0, 0, 0,'C',0, "Condens�e" },

	{ GADG_RADIO_BUTTON,	 90,124, 0, 0,  0,  0, 0, 0,'6',0, "6 LPI" },
	{ GADG_RADIO_BUTTON,	164,124, 0, 0,  0,  0, 0, 0,'8',0, "8 LPI" },

	{ GADG_CHECK_BOX,		 90,164, 0, 0,  0,  0, 0, 0,'d',0, "Pas d'espace entre les pages" },

	{ GADG_STAT_TEXT,		150, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON,	 90,144, 0, 0,  0,  0, 0, 0,'e',0, "Pouces" },
	{ GADG_RADIO_BUTTON, 164,144, 0, 0,  0,  0, 0, 0,'n',0, "Centim�tres" },

	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Format de page" },
	{ GADG_STAT_TEXT,		 20, 38, 0, 0,  0,  0, 0, 0, 0, 0, "Taille" },
	{ GADG_STAT_TEXT,		 28, 56, 0, 0,  0,  0, 0, 0, 0, 0, "page:" },
	{ GADG_STAT_TEXT,		164, 74, 0, 0,  0,  0, 0, 0, 0, 0, "larg.:" },
	{ GADG_STAT_TEXT,		278, 74, 0, 0,  0,  0, 0, 0, 0, 0, "haut.:" },
	{ GADG_STAT_TEXT,		 20,104, 0, 0,  0,  0, 0, 0, 0, 0, "Police:" },
	{ GADG_STAT_TEXT,		 20,124, 0, 0,  0,  0, 0, 0, 0, 0, "Espacmt:" },
	{ GADG_STAT_TEXT,		 20,164, 0, 0,  0,  0, 0, 0, 0, 0, "Options:" },
	{ GADG_STAT_TEXT,		 20,144, 0, 0,  0,  0, 0, 0, 0, 0, "Unit�s:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,278,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate pageSetupReqT = {
	-1, -1, 398, 184, &pageSetupGadgets[0],
};

#elif SWEDISH
#endif

/*
	 Print requester
*/

#if (AMERICAN | BRITISH)

static GadgetTemplate printGadgets[] = {
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20), 10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,'P',0, &strPrint[0] },
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20), 40, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,'C',0, &_strCancel[0] },

	{ GADG_RADIO_BUTTON,	122, 40, 0, 0,  0,  0, 0, 0,'N',0, "NLQ" },
	{ GADG_RADIO_BUTTON,	222, 40, 0, 0,  0,  0, 0, 0,'D',0, "Draft" },		

	{ GADG_RADIO_BUTTON,	122, 80, 0, 0,  0,  0, 0, 0,'A',0, "Automatic" },
	{ GADG_RADIO_BUTTON,	222, 80, 0, 0,  0,  0, 0, 0,'H',0, "Hand Feed" },

	{ GADG_EDIT_TEXT,		122, 60, 0, 0, 40, 11, 0, 0, 0, 0, NULL },

	GT_UPARROW(202,122, 0x1C),
	GT_DNARROW(202,122, 0x1D),
	GT_TEXT(202-8,122),

	{ GADG_RADIO_BUTTON,	122,100, 0, 0,  0,  0, 0, 0,'r',0, "Printer" },
	{ GADG_RADIO_BUTTON,	222,100, 0, 0,  0,  0, 0, 0,'F',0, "File" },
		
	{ GADG_STAT_TEXT,		120, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Printer:" },
	{ GADG_STAT_TEXT,		 20, 40, 0, 0,  0,  0, 0, 0, 0, 0, "Quality:" },
	{ GADG_STAT_TEXT,		 20, 60, 0, 0,  0,  0, 0, 0, 0, 0, "Copies:" },
	{ GADG_STAT_TEXT,		 20, 80, 0, 0,  0,  0, 0, 0, 0, 0, "Paper feed:" },
	{ GADG_STAT_TEXT,		 20,122, 0, 0,  0,  0, 0, 0, 0, 0, "Printer font number:" },
	{ GADG_STAT_TEXT,		 20,100, 0, 0,  0,  0, 0, 0, 0, 0, "Destination:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,278,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate printReqT = {
	-1, -1, 398, 144, &printGadgets[0]
};

#elif GERMAN

static GadgetTemplate printGadgets[] = {
	{ GADG_PUSH_BUTTON, -(STD_BUTTON_WIDTH+20), 10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,'D',0, &strPrint[0] },
	{ GADG_PUSH_BUTTON, -(STD_BUTTON_WIDTH+20), 40, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,'A',0, &_strCancel[0] },

	{ GADG_RADIO_BUTTON,	142, 40, 0, 0,  0,  0, 0, 0,'N',0, "NLQ" },
	{ GADG_RADIO_BUTTON, 196, 40, 0, 0,  0,  0, 0, 0,'S',0, "Schnellschrift" },

	{ GADG_RADIO_BUTTON, 157, 80, 0, 0,  0,  0, 0, 0,'u',0, "Automatisch" },
	{ GADG_RADIO_BUTTON, 282, 80, 0, 0,  0,  0, 0, 0,'H',0, "von Hand" },

	{ GADG_EDIT_TEXT,		172, 60, 0, 0, 40, 11, 0, 0, 0, 0, NULL },

	GT_UPARROW(237,122),
	GT_DOWNARROW(237,122),
	GT_TEXT(229,122),

	{ GADG_RADIO_BUTTON, 157,100, 0, 0,  0,  0, 0, 0,'r',0, "Drucker" },
	{ GADG_RADIO_BUTTON, 282,100, 0, 0,  0,  0, 0, 0,'t',0, "Datei" },

	{ GADG_STAT_TEXT,		120, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Drucker:" },
	{ GADG_STAT_TEXT,		 20, 40, 0, 0,  0,  0, 0, 0, 0, 0, "Druckqualit�t:" },
	{ GADG_STAT_TEXT,		 20, 60, 0, 0,  0,  0, 0, 0, 0, 0, "Anzahl der Kopien:" },
	{ GADG_STAT_TEXT,		 20, 80, 0, 0,  0,  0, 0, 0, 0, 0, "Papierzuf�hrung:" },
	{ GADG_STAT_TEXT,		 20,122, 0, 0,  0,  0, 0, 0, 0, 0, "Druckerzeichensatznummer:" },
	{ GADG_STAT_TEXT,		 20,100, 0, 0,  0,  0, 0, 0, 0, 0, "Ausgabe auf:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,310,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate printReqT = {
	-1, -1, 430, 144, &printGadgets[0]
};

#elif FRENCH

static GadgetTemplate printGadgets[] = {
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20), 10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,'I',0, &strPrint[0] },
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20), 40, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,'A',0, &_strCancel[0] },

	{ GADG_RADIO_BUTTON,	122, 40, 0, 0,  0,  0, 0, 0,'N',0, "NLQ" },
	{ GADG_RADIO_BUTTON,	222, 40, 0, 0,  0,  0, 0, 0,'B',0, "Brouillon" },

	{ GADG_RADIO_BUTTON,	132, 80, 0, 0,  0,  0, 0, 0,'u',0, "Automatique" },
	{ GADG_RADIO_BUTTON,	242, 80, 0, 0,  0,  0, 0, 0,'M',0, "Manuelle" },

	{ GADG_EDIT_TEXT,		122, 60, 0, 0, 40, 11, 0, 0, 0, 0, NULL },

	GT_UPARROW(182,122),
	GT_DOWNARROW(182,122),
	GT_TEXT(174,122),

	{ GADG_RADIO_BUTTON,	132,100, 0, 0,  0,  0, 0, 0,'p',0, "Imprimante" },
	{ GADG_RADIO_BUTTON,	242,100, 0, 0,  0,  0, 0, 0,'F',0, "Fichier" },

	{ GADG_STAT_TEXT,		120, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 20, 10, 0, 0,  0,  0, 0, 0, 0, 0, "Imprimante:" },
	{ GADG_STAT_TEXT,		 20, 40, 0, 0,  0,  0, 0, 0, 0, 0, "Qualit�:" },
	{ GADG_STAT_TEXT,		 20, 60, 0, 0,  0,  0, 0, 0, 0, 0, "Exemplaires:" },
	{ GADG_STAT_TEXT,		 20, 80, 0, 0,  0,  0, 0, 0, 0, 0, "Alimentation:" },
	{ GADG_STAT_TEXT,		 20,122, 0, 0,  0,  0, 0, 0, 0, 0, "Police imprimante:" },
	{ GADG_STAT_TEXT,		 20,100, 0, 0,  0,  0, 0, 0, 0, 0, "Destination:" },
	{ GADG_STAT_STDBORDER,20, 25, 0, 0,278,  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate printReqT = {
	-1, -1, 398, 144, &printGadgets[0]
};

#elif SWEDISH
#endif

/*
 *	Repair options dialog
 */

static TextPtr		blockModeList[] =
#if (AMERICAN | BRITISH)
	{ "reads only", "reads and writes", NULL };
#endif

static GadgetTemplate repairOptionsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20),  10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'O', 0, _strOK },
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20),  40, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'C', 0, _strCancel },
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20),  70, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'R', 0, strReset },

	{ GADG_CHECK_BOX,		 30,  50, 0, 0,  0,  0, 0, 0, 'b', 0, "Bad blocks on:" },
	{ GADG_CHECK_BOX,		 30,  70, 0, 0,  0,  0, 0, 0, 'f', 0, "Bad files and drawers" },
	{ GADG_CHECK_BOX,		 50,  85, 0, 0,  0,  0, 0, 0, 'd', 0, "Check data blocks on FFS disks" },
	
	{ GADG_CHECK_BOX,		 30, 125, 0, 0,  0,  0, 0, 0, 'e', 0, "Repair disk" },

	{ GADG_POPUP,		161,  50, 0, 0,150, 11, 0, 0, 0, 0, &blockModeList },
		
	/*{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY,	130, 140, 0, 0,32,11, 0, 0, 0, 0, NULL },*/

	{ GADG_STAT_STDBORDER,	 20,  25, 0, 0, (330+80)-120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,		 20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Repair Options" },
	{ GADG_STAT_TEXT,		 20,  35, 0, 0, 0, 0, 0, 0, 0, 0, "Check for:" },
	{ GADG_STAT_TEXT,		 20, 110, 0, 0, 0, 0, 0, 0, 0, 0, "Action:" },
	/*
	{ GADG_STAT_TEXT,		 20, 140, 0, 0, 0, 0, 0, 0, 0, 0, "Memory usage:" },
	{ GADG_STAT_TEXT,		168, 140, 0, 0, 0, 0, 0, 0, 0, 0, "%" },
	*/
	{ GADG_ITEM_NONE }
};

static RequestTemplate repairOptionsReqT = {
	-1, -1, 330+80, 146, repairOptionsGadgets
};

/*
 * File system prompt requester
 */


static TextPtr		fileSysList[] =
	{ "Old", "Fast", "Old, Int'l",	"Fast, Int'l", "Old, DirCache", "Fast, DirCache", NULL };
 
static GadgetTemplate fileSysGadgets[] = {
	{ GADG_PUSH_BUTTON, -(STD_BUTTON_WIDTH+16),-32, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_OK,0, &_strOK[0] },
	{ GADG_PUSH_BUTTON, 2*(-(STD_BUTTON_WIDTH+16)),-32, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_CANCEL,0, &_strCancel[0] },

	{ GADG_POPUP,		160, 68, 0, 0,118, 11, 0, 0, 0, 0, &fileSysList },		
/*
	{ GADG_CHECK_BOX,	 	 20, 68, 0, 0,  0,  0, 0, 0, 'F',0, "Fast File System" },
	{ GADG_CHECK_BOX,		 20, 87, 0, 0,  0,  0, 0, 0, 'I',0, "International" },
	{ GADG_CHECK_BOX,		 20,106, 0, 0,  0,  0, 0, 0, 'D',0, "Directory Cache" },
*/	
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_STAT_TEXT,		 54, 10, 0, 0,  0,  0, 0, 0,  0, 0,
		 "The boot block is corrupt." },
	{ GADG_STAT_TEXT,     54, 30, 0, 0,  0,  0, 0, 0,  0, 0,
		 "Please indicate which file system was" },
	{ GADG_STAT_TEXT,		 54, 44, 0, 0,  0,  0, 0, 0,  0, 0,
		 "chosen when this volume was formatted:" },
		 
	{ GADG_STAT_TEXT,		 54, 68, 0, 0,  0,  0, 0, 0, 0, 0, "File system:" },
	{ GADG_ITEM_NONE },
};

static RequestTemplate fsReqT = {
	-1, -1, 370, 124, &fileSysGadgets[0]
};

/*
	 Cancel print dialog
*/

static GadgetTemplate cancelPrGadgets[] = {
	
#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT,		 60, 15, 0, 0,  0,  0, 0, 0, 0, 0, "Printing in progress." },
#elif GERMAN
	{ GADG_STAT_TEXT,		 60, 15, 0, 0,  0,  0, 0, 0, 0, 0, "Druckvorgang l�uft." },
#elif FRENCH
	{ GADG_STAT_TEXT,		 60, 15, 0, 0,  0,  0, 0, 0, 0, 0, "Impression en cours." },
#elif SWEDISH
#endif

	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20), 50, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_CANCEL,0, &_strCancel[0] },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_NOTE },
	{ GADG_ITEM_NONE }
};

RequestTemplate cancelPrReqT = {
	-1, -1, 300, 80, &cancelPrGadgets[0]
};


/*
	 Next Page requester
*/

static GadgetTemplate nextPageGadgets[] = {
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20), 10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_OK,0, &_strOK[0] },
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20), 40, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_CANCEL,0, &_strCancel[0] },

#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT,		 60, 15, 0, 0,  0,  0, 0, 0, 0, 0, "Please insert the next\nsheet of paper" },
#elif GERMAN
	{ GADG_STAT_TEXT,		 60, 15, 0, 0,  0,  0, 0, 0, 0, 0, "Bitte das n�chste Blatt\nPapier einlegen" },
#elif FRENCH
	{ GADG_STAT_TEXT,		 60, 15, 0, 0,  0,  0, 0, 0, 0, 0, "Ins�rez une autre feuille\nde papier, SVP." },
#elif SWEDISH
#endif

	{ GADG_STAT_STDIMAGE, 10, 15, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

RequestTemplate nextPageReqT = {
	-1, 30, 370, 70, &nextPageGadgets[0]
};

/*
	 Error requester, has one button, that's it.
*/

GadgetTemplate errorGadgets[] = {
	{ GADG_PUSH_BUTTON, 	-(ERR_BUTTON_WIDTH+16), -32, 0, 0, ERR_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_OK,0, &_strOK[0] },
	{ GADG_STAT_TEXT,		 51, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

/*
	Warning dialog, has two buttons instead of one.
*/

GadgetTemplate warnGadgets[] = {
	{ GADG_PUSH_BUTTON,  -(ERR_BUTTON_WIDTH+16),   -32, 0, 0, ERR_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_OK,0, &_strOK[0] },
	{ GADG_PUSH_BUTTON, 2*(-(ERR_BUTTON_WIDTH+16)),-32, 0, 0, ERR_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_CANCEL,0, &_strCancel[0] },
	{ GADG_STAT_TEXT,		 51, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

/*
	Skip dialog, has three buttons.
*/

GadgetTemplate skipGadgets[] = {
	{ GADG_PUSH_BUTTON,  -(REPAIR_BUTTON_WIDTH+16),   -32, 0, 0, REPAIR_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_OK,0, &_strOK[0] },
	{ GADG_PUSH_BUTTON, 3*(-(REPAIR_BUTTON_WIDTH+16)),-32, 0, 0, REPAIR_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_CANCEL,0, &_strCancel[0] },
	{ GADG_PUSH_BUTTON, 2*(-(REPAIR_BUTTON_WIDTH+16)),-32, 0, 0, REPAIR_BUTTON_WIDTH, 20, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 51, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

/*
 	Four button gadget template
*/

GadgetTemplate ignoreGadgets[] = {
	{ GADG_PUSH_BUTTON,  -(REPAIR_BUTTON_WIDTH+16),   -32, 0, 0, REPAIR_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_OK,0, &_strOK[0] },
	{ GADG_PUSH_BUTTON, 4*(-(REPAIR_BUTTON_WIDTH+16)),-32, 0, 0, REPAIR_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_CANCEL,0, &_strCancel[0] },
	{ GADG_PUSH_BUTTON, 2*(-(REPAIR_BUTTON_WIDTH+16)),-32, 0, 0, REPAIR_BUTTON_WIDTH, 20, 0, 0, 0, 0, NULL },
	{ GADG_PUSH_BUTTON, 3*(-(REPAIR_BUTTON_WIDTH+16)),-32, 0, 0, REPAIR_BUTTON_WIDTH, 20, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 51, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

static RequestTemplate errorReqT = {
#if (AMERICAN | BRITISH)
	-1, -1, 390, 80, errorGadgets
#elif GERMAN
	-1, -1, 410, 95, errorGadgets
#elif FRENCH
	-1, -1, 410, 95, errorGadgets
#elif SWEDISH
#endif
};

static RequestTemplate bigErrorReqT = {
#if (AMERICAN | BRITISH)
	-1, -1, 398, MAX_ERR_HEIGHT, errorGadgets
#elif GERMAN
	-1, -1, 418  MAX_ERR_HEIGHT, errorGadgets
#elif FRENCH
	-1, -1, 418, MAX_ERR_HEIGHT, errorGadgets
#elif SWEDISH
#endif
};

TextChar emptyStr = '\0';

static TextPtr		findList[] =
#if (AMERICAN | BRITISH)
	{ "ASCII", "Hex", &emptyStr, "Current Key", "Parent Key", "Header Key", NULL };
#endif

/*
 * Disk editor "Find..." requester
 */
 
static GadgetTemplate findGadgets[] = {
	{ GADG_PUSH_BUTTON,		-(STD_BUTTON_WIDTH+20),  10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,'F',0, strFind },
	{ GADG_PUSH_BUTTON,		-(STD_BUTTON_WIDTH+20),  40, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_CANCEL, 0, _strCancel },
	{ GADG_EDIT_TEXT,			175,  40, 0, 0,208,  0, 0, 0, 0, 0, NULL },
	{ GADG_POPUP,			 65,  40, 0, 0,101,  0, 0, 0, 0, 0, &findList },
	{ GADG_RADIO_BUTTON,		122,  92, 0, 0,  0,  0, 0, 0,'o',0, "Forward" },
	{ GADG_RADIO_BUTTON,		210,  92, 0, 0,  0,  0, 0, 0,'R',0, "Reverse" },
	{ GADG_RADIO_BUTTON,		122, 112, 0, 0,  0,  0, 0, 0,'A',0, "All" },
	{ GADG_RADIO_BUTTON,		210, 112, 0, 0,  0,  0, 0, 0,'m',0, "From:" },
	{ GADG_EDIT_TEXT,			279, 112, 0, 0, 64,  0, 0, 0, 0 ,0, NULL },
	{ GADG_EDIT_TEXT,			377, 112, 0, 0, 64,  0, 0, 0, 0, 0, NULL },
	{ GADG_CHECK_BOX,			 20,  70, 0, 0,  0,  0, 0, 0,'W',0, "Warn on each bad block" },
	{ GADG_STAT_TEXT,			 20,  10, 0, 0,  0,  0, 0, 0, 0, 0, strFind },
	{ GADG_STAT_TEXT,			 20,  40, 0, 0,  0,  0, 0, 0, 0, 0, "Find:" },
	{ GADG_STAT_TEXT,			 20,  92, 0, 0,  0,  0, 0, 0, 0, 0, "Direction:" },
	{ GADG_STAT_TEXT,			 20, 112, 0, 0,  0,  0, 0, 0, 0, 0, "Block range:" },
	{ GADG_STAT_TEXT,			352, 112, 0, 0,  0,  0, 0, 0, 0, 0, "to" },
	{ GADG_STAT_STDBORDER,	 20, 	25, 0, 0, 366, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate findReqT = {
	-1, -1, 486, 140, findGadgets
};

/*
 * Disk editor "Go to block" requester
 */
 
static GadgetTemplate gotoGadgets[] = {
	{ GADG_PUSH_BUTTON, 40, 43, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'G', 0, "Go" },
	{ GADG_PUSH_BUTTON, -(40+STD_BUTTON_WIDTH),43, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_CANCEL, 0, _strCancel },
	{ GADG_EDIT_TEXT,			118,  15, 0, 0, 64,  0, 0, 0, 0, 0, NULL },
	{ GADG_POPUP, 			190,  15, 0, 0, 46, 11, 0, 0, 0, 0, &baseList },	
	{ GADG_STAT_TEXT,			 15,  15, 0, 0,  0,  0, 0, 0, 0, 0, "Go to block:" },
	{ GADG_ITEM_NONE }
};

static RequestTemplate gotoReqT = {
	-1, -1, 254, 80, gotoGadgets
};

/*
 * Disk editor "Calculate Hash (and hilight it) requester
 */
 
static GadgetTemplate hashGadgets[] = {
	{ GADG_PUSH_BUTTON,		-(STD_BUTTON_WIDTH+20),  10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_OK, 0, _strOK },
	{ GADG_PUSH_BUTTON,		-(STD_BUTTON_WIDTH+20),  40, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_CANCEL, 0, _strCancel },

	{ GADG_EDIT_TEXT,			 20,  73, 3, 0, 264, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,			 20,  40, 0, 0,  0,  0, 0, 0, 0, 0, "Enter the name of the file, drawer\nor link that you wish to locate." },
	{ GADG_STAT_TEXT,			 20, 100, 0, 0,  0,  0, 0, 0, 0, 0, "The hash table entry will be hilighted." },

	{ GADG_STAT_STDBORDER,	 20,  25, 0, 0, 390-120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,			 20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Calculate Hash" },
	{ GADG_ITEM_NONE }
};

static RequestTemplate hashReqT = {
	-1, -1, 390, 125, hashGadgets
};

/*
 * Disk editor "Finding..." requester
 */
 
static GadgetTemplate findingGadgets[] = {
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_NOTE },
	{ GADG_PUSH_BUTTON,(-(STD_BUTTON_WIDTH+16)), 22, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_STOP, 0, strStop },
	{ GADG_STAT_TEXT,		 50, 19, 0, 0,  0,  0, 0, 0, 0, 0, "Searching disk..." },
	{ GADG_ITEM_NONE }
};

static RequestTemplate findingReqT = {
	-1, -1, 300, 53, findingGadgets
};

static TextPtr reorgTypes[] = {
	"file access", "directory access", NULL
};

/*
 * Reorganize options
 */
 
static GadgetTemplate reorgOptsGadgets[] = {
	{ GADG_PUSH_BUTTON,		-(STD_BUTTON_WIDTH+20),  15, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_OK, 0, _strOK },
	{ GADG_PUSH_BUTTON,		-(STD_BUTTON_WIDTH+20),  45, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_CANCEL, 0, _strCancel },
	{ GADG_PUSH_BUTTON,		-(STD_BUTTON_WIDTH+20),  75, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'R', 0, strReset },
	{ GADG_RADIO_BUTTON,		 30,  60, 0, 0,  0,  0, 0, 0, 'F', 0, "Count fragmented files only" },
	{ GADG_RADIO_BUTTON,		 30,  80, 0, 0,  0,  0, 0, 0, 'p', 0, "Optimize disk for:" },
	{ GADG_POPUP,			195,  80, 0, 0,152,  0, 0, 0,  0,  0, &reorgTypes },
#ifdef OPTION_REMAP
	{ GADG_CHECK_BOX,			 30, 108, 0, 0,  0,  0, 0, 0, 'B', 0, "Buffer remapping table" },
#endif
	{ GADG_STAT_STDBORDER,	 20,  25, 0, 0, 446-120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,		    20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Optimize Options" },
	{ GADG_STAT_TEXT,		    20,  40, 0, 0, 0, 0, 0, 0, 0, 0, "Action:" },
	{ GADG_ITEM_NONE }
};

static RequestTemplate reorgOptsReqT = {
#ifdef OPTION_REMAP
	-1, -1, 446, 130, reorgOptsGadgets
#else
	-1, -1, 446, 110, reorgOptsGadgets
#endif
};

/*
 * Recover "in-progress" requester.
 */
 
static GadgetTemplate recoverGadgets[] = {
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_NOTE },
	{ GADG_PUSH_BUTTON,(-(STD_BUTTON_WIDTH+16)), 35, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'C', 0, _strCancel },
	{ GADG_STAT_BORDER,	 51, 29, 0, 0,236-40, 18, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 50, 14, 0, 0,  0,  0, 0, 0, 0, 0, "Building list of files..." },
		
	{ GADG_ITEM_NONE }
};

static RequestTemplate recoverReqT = {
	-1, -1, 380-40, 66, recoverGadgets
};

/*
 * Recover options
 */
 
static GadgetTemplate recoverOptsGadgets[] = {
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20),  10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'O', 0, _strOK },
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20),  40, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'C', 0, _strCancel },
	{ GADG_PUSH_BUTTON,	-(STD_BUTTON_WIDTH+20),  70, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'R', 0, strReset },

	{ GADG_CHECK_BOX,		 30,  54, 0, 0,  0,  0, 0, 0, 'D', 0, "Show only deleted files" },
	{ GADG_CHECK_BOX,		 30,  71, 0, 0,  0,  0, 0, 0, 'B', 0, "Show block keys" },
	{ GADG_CHECK_BOX,		 30, 110, 0, 0,  0,  0, 0, 0, 'e', 0, "Create replacement drawers" },

	{ GADG_STAT_TEXT,		 20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Recover Options" },
	{ GADG_STAT_TEXT,		 20,  37, 0, 0, 0, 0, 0, 0, 0, 0, "Catalog display:" },
	{ GADG_STAT_TEXT,		 20,  93, 0, 0, 0, 0, 0, 0, 0, 0, "Lost drawers:" },

	{ GADG_STAT_STDBORDER,20,  25, 0, 0, 352-120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_ITEM_NONE }
};

static RequestTemplate recoverOptsReqT = {
	-1, -1, 352, 134, recoverOptsGadgets
};

/*
	Prompt to rename the file. 
*/

#if (AMERICAN | BRITISH)

static GadgetTemplate renameGadgets[] = {
	{ GADG_PUSH_BUTTON,  -80,-34, 0, 0, 66, 20, 0, 0,'R',0, "Rename" },
	{ GADG_PUSH_BUTTON, -320,-34, 0, 0, 66, 20, 0, 0,CMD_KEY_STOP2,0, strStop },
	{ GADG_PUSH_BUTTON, -240,-34, 0, 0, 66, 20, 0, 0,CMD_KEY_SKIP,0, strSkip },
	{ GADG_PUSH_BUTTON, -160,-34, 0, 0, 66, 20, 0, 0,'p',0, "Replace" },
	
	{ GADG_EDIT_TEXT,		134, 88, 0, 0,240, 12, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 54, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
		
	{ GADG_STAT_STDIMAGE, 12, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_STAT_TEXT,		 54, 28, 0, 0,  0,  0, 0, 0, 0, 0, "Press \"Replace\" to replace the existing\nfile, \"Rename\" to create a new file name,\nor \"Skip\" to skip recovery of this file." },
	{ GADG_STAT_TEXT,		 54, 88, 0, 0,  0,  0, 0, 0, 0, 0, "New name:" },
	{ GADG_ITEM_NONE }	
};

static RequestTemplate renameReqT = {
	-1, -1, 400, 152, &renameGadgets[0]
};

#elif GERMAN

static GadgetTemplate renameGadgets[] = {
	{ GADG_PUSH_BUTTON, -100,-32, 0, 0, 90, 20, 0, 0,'U',0, "Umbenennen" },
	{ GADG_PUSH_BUTTON, -355,-32, 0, 0, 80, 20, 0, 0,CMD_KEY_STOP2,0, strStop },
	{ GADG_PUSH_BUTTON, -270,-32, 0, 0, 80, 20, 0, 0,CMD_KEY_SKIP,0, strSkip },
	{ GADG_PUSH_BUTTON, -185,-32, 0, 0, 80, 20, 0, 0,'E',0, "Ersetzen" },

	{ GADG_EDIT_TEXT,		 85, 40, 0, 0,224, 12, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 54, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDIMAGE, 12, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_STAT_TEXT,		 54, 25, 0, 0,  0,  0, 0, 0, 0, 0, "Neuen Dateinamen eingeben:" },
	{ GADG_ITEM_NONE }
};

static RequestTemplate renameReqT = {
	-1, -1, 440, 90, &renameGadgets[0]
};

#elif FRENCH

static GadgetTemplate renameGadgets[] = {
	{ GADG_PUSH_BUTTON,  -90,-32, 0, 0, 80, 20, 0, 0,'R',0, "Renomme" },
	{ GADG_PUSH_BUTTON, -360,-32, 0, 0, 80, 20, 0, 0,CMD_KEY_STOP2,0, strStop },
	{ GADG_PUSH_BUTTON, -270,-32, 0, 0, 80, 20, 0, 0,CMD_KEY_SKIP,0, strSkip },
	{ GADG_PUSH_BUTTON, -180,-32, 0, 0, 80, 20, 0, 0,'p',0, "Remplace" },

	{ GADG_EDIT_TEXT,		238, 28, 0, 0,184, 12, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,		 54, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDIMAGE, 12, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_STAT_TEXT,		 54, 28, 0, 0,  0,  0, 0, 0, 0, 0, "Entrez un nouveau nom:" },
	{ GADG_ITEM_NONE }
};

static RequestTemplate renameReqT = {
	-1, -1, 440, 84, &renameGadgets[0]
};

#elif SWEDISH
#endif


/*
	Edit's save confirmation requester gadget template
*/

GadgetTemplate editSaveGadgets[] = {
	{ GADG_PUSH_BUTTON,  -(ERR_BUTTON_WIDTH+16),   -32, 0, 0, ERR_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_YES, 0, _strYes },
	{ GADG_PUSH_BUTTON, 51, -32, 0, 0, ERR_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_CANCEL, 0, _strCancel },
	{ GADG_PUSH_BUTTON, 2*(-(ERR_BUTTON_WIDTH+16)),-32, 0, 0, ERR_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_NO, 0, _strNo },
	{ GADG_STAT_TEXT,		 51, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

static RequestTemplate editSaveReqT = {
	-1, -1, 390, 80, editSaveGadgets
};

/******************** END OF REQUESTERS, BEGINNING OF DIALOGS **********************/
/*
 * About box
 */

static GadgetTemplate aboxGadgets[] = {
	{ GADG_PUSH_BUTTON, -75, -30,0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_OK, 0, &_strOK[0] },
	{ GADG_STAT_STDBORDER,15,56, 0, 0,357-31,85,0, 0, 0, 0, (Ptr) BORDER_SHADOWBOX },
#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT,		15, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Quarterback Tools 2.0\nCentral Coast Software\nA division of New Horizons Software, Inc." },
	{ GADG_STAT_TEXT,		15,152, 0, 0, 0, 0, 0, 0, 0, 0, "Copyright \251 1993\nAll Rights Reserved" },
#elif GERMAN
	{ GADG_STAT_TEXT,		15, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Quarterback Tools 2.0\nCentral Coast Software\neine Abteilung von New Horizons Software, Inc." },
	{ GADG_STAT_TEXT,		15,152, 0, 0, 0, 0, 0, 0, 0, 0, "Copyright \251 1993\nAlle Rechte vorbehalten" },
#elif FRENCH
	{ GADG_STAT_TEXT,		15, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Quarterback Tools 2.0\nCentral Coast Software\ndivision de New Horizons Software, Inc." },
	{ GADG_STAT_TEXT,		15,152, 0, 0, 0, 0, 0, 0, 0, 0, "Copyright \251 1993\nTous droits r�serv�s" },
#elif SWEDISH
#endif

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboxDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 357, 188, &aboxGadgets[0], NULL
};

#ifdef BLAH
/*
 *	About (and memory) dialog
 */

#if (AMERICAN | BRITISH)

static GadgetTemplate aboutGadgets[] = {
	{ GADG_PUSH_BUTTON, -(STD_BUTTON_WIDTH+20), 10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'O', 0, _strOK },

	{ GADG_STAT_TEXT, 150, 150, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 150, 165, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 150, 185, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20,  16, 0, 0, 0, 0, 0, 0, 0, 0, "Quarterback Tools 2.0" },
/*	{ GADG_STAT_TEXT,  158,  10, 0, 0, 0, 0, 0, 0, 0, 0, "�" },*/
	{ GADG_STAT_TEXT,  20,  60, 0, 0, 0, 0, 0, 0, 0, 0,
		"Copyright \251 1993\nCentral Coast Software\nA division of\nNew Horizons Software, Inc.\nAll Rights Reserved" },
	{ GADG_STAT_TEXT,  20, 135, 0, 0, 0, 0, 0, 0, 0, 0, strAvailMem },
	{ GADG_STAT_TEXT,  40, 150, 0, 0, 0, 0, 0, 0, 0, 0, strGraphics },
	{ GADG_STAT_TEXT,  40, 165, 0, 0, 0, 0, 0, 0, 0, 0, strExpansion },
	{ GADG_STAT_TEXT,  20, 185, 0, 0, 0, 0, 0, 0, 0, 0, "AREXX port:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboutDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 320, 200, aboutGadgets, NULL
};

#elif GERMAN

#elif FRENCH

static GadgetTemplate aboutGadgets[] = {
	{ GADG_PUSH_BUTTON, -(STD_BUTTON_WIDTH+20), 10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'O', 0, _strOK },

	{ GADG_STAT_TEXT, 150, 170, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 150, 185, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 150, 205, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20,  16, 0, 0, 0, 0, 0, 0, 0, 0, "Quarterback Tools 2.0" },
	{ GADG_STAT_TEXT,  84,  10, 0, 0, 0, 0, 0, 0, 0, 0, "�" },
	{ GADG_STAT_TEXT,  20,  85, 0, 0, 0, 0, 0, 0, 0, 0,
		"Copyright \251 1993\nCentral Coast Software\nNew Horizons Software, Inc.\nTous droits r�serv�s" },
	{ GADG_STAT_TEXT,  20, 155, 0, 0, 0, 0, 0, 0, 0, 0, strAvailMem },
	{ GADG_STAT_TEXT,  40, 170, 0, 0, 0, 0, 0, 0, 0, 0, strGraphics },
	{ GADG_STAT_TEXT,  40, 185, 0, 0, 0, 0, 0, 0, 0, 0, strExpansion },
	{ GADG_STAT_TEXT,  20, 205, 0, 0, 0, 0, 0, 0, 0, 0, "Port AREXX:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboutDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 225, aboutGadgets, NULL
};


#elif SWEDISH

static GadgetTemplate aboutGadgets[] = {
	{ GADG_PUSH_BUTTON, -(STD_BUTTON_WIDTH+20), 10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'O', 0, _strOK },

	{ GADG_STAT_TEXT, 150, 150, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 150, 165, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 150, 185, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20,  16, 0, 0, 0, 0, 0, 0, 0, 0, "Quarterback Tools 2.0" },
	{ GADG_STAT_TEXT,  84,  10, 0, 0, 0, 0, 0, 0, 0, 0, "�" },
	{ GADG_STAT_TEXT,  20,  80, 0, 0, 0, 0, 0, 0, 0, 0,
		"Copyright \251 1993\nCentral Coast Software\nNew Horizons Software, Inc.\nAlla r�ttigheter f�rbeh�llna" },
	{ GADG_STAT_TEXT,  20, 135, 0, 0, 0, 0, 0, 0, 0, 0, strAvailMem },
	{ GADG_STAT_TEXT,  40, 150, 0, 0, 0, 0, 0, 0, 0, 0, strGraphics },
	{ GADG_STAT_TEXT,  40, 165, 0, 0, 0, 0, 0, 0, 0, 0, strExpansion },
	{ GADG_STAT_TEXT,  20, 185, 0, 0, 0, 0, 0, 0, 0, 0, "AREXX port:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboutDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 205, aboutGadgets, NULL
};

#endif

static GadgetTemplate memoryGadgets[] = {
	{ GADG_PUSH_BUTTON, -(STD_BUTTON_WIDTH+20), 10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'O', 0, _strOK },

	{ GADG_STAT_TEXT, 150, 25, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 150, 40, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, strAvailMem },
	{ GADG_STAT_TEXT,  40, 25, 0, 0, 0, 0, 0, 0, 0, 0, strGraphics },
	{ GADG_STAT_TEXT,  40, 40, 0, 0, 0, 0, 0, 0, 0, 0, strExpansion },

	{ GADG_ITEM_NONE }
};

static DialogTemplate memoryDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 280, 65, memoryGadgets, NULL
};
#endif

/*
 *	Customize macro menu dialog
 */

#if (AMERICAN | BRITISH)

static GadgetTemplate customMacroGadgets[] = {
	{ GADG_PUSH_BUTTON, 103, -30, 0, 0,  STD_BUTTON_WIDTH, 20, 0, 0, 'U', 0, strUse },
	{ GADG_PUSH_BUTTON, 185, -30, 0, 0,  STD_BUTTON_WIDTH, 20, 0, 0, 'C', 0, _strCancel },
	{ GADG_PUSH_BUTTON,  21, -30, 0, 0,  STD_BUTTON_WIDTH, 20, 0, 0, 'S', 0, "Save" },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  30, 0, 0, 200, 11, 0, 0, 0, 0, &macroNames2[0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  50, 0, 0, 200, 11, 0, 0, 0, 0, &macroNames2[1] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  70, 0, 0, 200, 11, 0, 0, 0, 0, &macroNames2[2] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  90, 0, 0, 200, 11, 0, 0, 0, 0, &macroNames2[3] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 110, 0, 0, 200, 11, 0, 0, 0, 0, &macroNames2[4] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 130, 0, 0, 200, 11, 0, 0, 0, 0, &macroNames2[5] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 150, 0, 0, 200, 11, 0, 0, 0, 0, &macroNames2[6] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 170, 0, 0, 200, 11, 0, 0, 0, 0, &macroNames2[7] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 190, 0, 0, 200, 11, 0, 0, 0, 0, &macroNames2[8] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 210, 0, 0, 200, 11, 0, 0, 0, 0, &macroNames2[9] },

	{ GADG_STAT_TEXT, 20,  30, 0, 0, 0, 0, 0, 0, 0, 0, "F1" },
	{ GADG_STAT_TEXT, 20,  50, 0, 0, 0, 0, 0, 0, 0, 0, "F2" },
	{ GADG_STAT_TEXT, 20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "F3" },
	{ GADG_STAT_TEXT, 20,  90, 0, 0, 0, 0, 0, 0, 0, 0, "F4" },
	{ GADG_STAT_TEXT, 20, 110, 0, 0, 0, 0, 0, 0, 0, 0, "F5" },
	{ GADG_STAT_TEXT, 20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "F6" },
	{ GADG_STAT_TEXT, 20, 150, 0, 0, 0, 0, 0, 0, 0, 0, "F7" },
	{ GADG_STAT_TEXT, 20, 170, 0, 0, 0, 0, 0, 0, 0, 0, "F8" },
	{ GADG_STAT_TEXT, 20, 190, 0, 0, 0, 0, 0, 0, 0, 0, "F9" },
	{ GADG_STAT_TEXT, 20, 210, 0, 0, 0, 0, 0, 0, 0, 0, "F10" },
	{ GADG_STAT_TEXT, 20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Macro names:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate customMacroDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 272, 260, customMacroGadgets, NULL
};

#elif GERMAN

#elif FRENCH

static GadgetTemplate customMacroGadgets[] = {
	{ GADG_PUSH_BUTTON, 115, -30, 0, 0,  70, 20, 0, 0, 'O', 0, strUse },
	{ GADG_PUSH_BUTTON, 205, -30, 0, 0,  70, 20, 0, 0, 'A', 0, _strCancel },
	{ GADG_PUSH_BUTTON,  25, -30, 0, 0,  70, 20, 0, 0, 'S', 0, "Sauve" },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65,  30, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65,  50, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[1] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65,  70, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[2] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65,  90, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[3] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65, 110, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[4] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65, 130, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[5] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65, 150, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[6] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65, 170, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[7] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65, 190, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[8] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65, 210, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[9] },

	{ GADG_STAT_TEXT, 35,  30, 0, 0, 0, 0, 0, 0, 0, 0, "F1" },
	{ GADG_STAT_TEXT, 35,  50, 0, 0, 0, 0, 0, 0, 0, 0, "F2" },
	{ GADG_STAT_TEXT, 35,  70, 0, 0, 0, 0, 0, 0, 0, 0, "F3" },
	{ GADG_STAT_TEXT, 35,  90, 0, 0, 0, 0, 0, 0, 0, 0, "F4" },
	{ GADG_STAT_TEXT, 35, 110, 0, 0, 0, 0, 0, 0, 0, 0, "F5" },
	{ GADG_STAT_TEXT, 35, 130, 0, 0, 0, 0, 0, 0, 0, 0, "F6" },
	{ GADG_STAT_TEXT, 35, 150, 0, 0, 0, 0, 0, 0, 0, 0, "F7" },
	{ GADG_STAT_TEXT, 35, 170, 0, 0, 0, 0, 0, 0, 0, 0, "F8" },
	{ GADG_STAT_TEXT, 35, 190, 0, 0, 0, 0, 0, 0, 0, 0, "F9" },
	{ GADG_STAT_TEXT, 35, 210, 0, 0, 0, 0, 0, 0, 0, 0, "F10" },
	{ GADG_STAT_TEXT, 35,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Noms des macros:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate customMacroDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 302, 260, customMacroGadgets, NULL
};

#elif SWEDISH

static GadgetTemplate customMacroGadgets[] = {
	{ GADG_PUSH_BUTTON, 115, -30, 0, 0,  70, 20, 0, 0, 'n', 0, strUse },
	{ GADG_PUSH_BUTTON, 205, -30, 0, 0,  70, 20, 0, 0, 'A', 0, _strCancel },
	{ GADG_PUSH_BUTTON,  25, -30, 0, 0,  70, 20, 0, 0, 'S', 0, "Spara" },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65,  30, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65,  50, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[1] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65,  70, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[2] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65,  90, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[3] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65, 110, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[4] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65, 130, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[5] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65, 150, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[6] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65, 170, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[7] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65, 190, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[8] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 65, 210, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[9] },

	{ GADG_STAT_TEXT, 35,  30, 0, 0, 0, 0, 0, 0, 0, 0, "F1" },
	{ GADG_STAT_TEXT, 35,  50, 0, 0, 0, 0, 0, 0, 0, 0, "F2" },
	{ GADG_STAT_TEXT, 35,  70, 0, 0, 0, 0, 0, 0, 0, 0, "F3" },
	{ GADG_STAT_TEXT, 35,  90, 0, 0, 0, 0, 0, 0, 0, 0, "F4" },
	{ GADG_STAT_TEXT, 35, 110, 0, 0, 0, 0, 0, 0, 0, 0, "F5" },
	{ GADG_STAT_TEXT, 35, 130, 0, 0, 0, 0, 0, 0, 0, 0, "F6" },
	{ GADG_STAT_TEXT, 35, 150, 0, 0, 0, 0, 0, 0, 0, 0, "F7" },
	{ GADG_STAT_TEXT, 35, 170, 0, 0, 0, 0, 0, 0, 0, 0, "F8" },
	{ GADG_STAT_TEXT, 35, 190, 0, 0, 0, 0, 0, 0, 0, 0, "F9" },
	{ GADG_STAT_TEXT, 35, 210, 0, 0, 0, 0, 0, 0, 0, 0, "F10" },
	{ GADG_STAT_TEXT, 35,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Makronamn:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate customMacroDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 302, 260, customMacroGadgets, NULL
};

#endif

/*
	User request dialogs
*/

static GadgetTemplate userReq1Gadgets[] = {
	{ GADG_PUSH_BUTTON, -(STD_BUTTON_WIDTH+20), -30, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'O', 0, &_strOK[0] },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReq1DialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 85, &userReq1Gadgets[0], NULL
};


static GadgetTemplate userReq2Gadgets[] = {
	{ GADG_PUSH_BUTTON,  STD_BUTTON_WIDTH-20, -30, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_OK, 0, &_strOK[0] },
	{ GADG_PUSH_BUTTON, (2*STD_BUTTON_WIDTH)+20, -30, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_CANCEL, 0, &_strCancel[0] },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReq2DialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 85, &userReq2Gadgets[0], NULL
};


static GadgetTemplate userReq3Gadgets[] = {
	{ GADG_PUSH_BUTTON,  30, -30, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_YES, 0, &_strYes[0] },
	{ GADG_PUSH_BUTTON, 210, -30, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_CANCEL, 0, &_strCancel[0] },
	{ GADG_PUSH_BUTTON, 120, -30, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_NO, 0, &_strNo[0] },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReq3DialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 90, &userReq3Gadgets[0], NULL
};


static GadgetTemplate userReqTextGadgets[] = {
	{ GADG_PUSH_BUTTON,  STD_BUTTON_WIDTH-20, -30, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_OK, 0, &_strOK[0] },
	{ GADG_PUSH_BUTTON, (2*STD_BUTTON_WIDTH)+20, -30, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, CMD_KEY_CANCEL, 0, &_strCancel[0] },

	{ GADG_EDIT_TEXT, 22, 55, 0, 0, 256, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReqTextDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 110, &userReqTextGadgets[0], NULL
};

/*
	Volume information dialog
*/

#define VOL_INFO_WD	425

GadgetTemplate volInfoGadgets[] = {
	{ GADG_PUSH_BUTTON,-(STD_BUTTON_WIDTH+20), 12, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_OK,0, _strOK },
	{ GADG_STAT_STDBORDER,20,27, 0, 0,VOL_INFO_WD-(STD_BUTTON_WIDTH+58),  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
/*	{ GADG_PUSH_BUTTON,-(STD_BUTTON_WIDTH+20), 42, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_NEXT, 0, strNext },*/
	
	{ GADG_ACTIVE_STDIMAGE,
		-(STD_BUTTON_WIDTH+20-6), 42, 0, 0,
		25-5, 20-4, 0, 0, 0x1F, 0, (Ptr) IMAGE_ARROW_LEFT },
	{ GADG_ACTIVE_STDIMAGE,
		-(STD_BUTTON_WIDTH+20-(6+28)), 42, 0, 0,
		25-5, 20-4, 0, 0, 0x1E, 0, (Ptr) IMAGE_ARROW_RIGHT },

	{ GADG_STAT_STDBORDER,120,45,0, 0,(VOL_INFO_WD-(STD_BUTTON_WIDTH+40))-118, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,120,66,0, 0,(VOL_INFO_WD-(STD_BUTTON_WIDTH+40))-118, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,120,87,0, 0,(VOL_INFO_WD-(STD_BUTTON_WIDTH+40))-118, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,136,108,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,217,108,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,136,129,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,217,129,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,136,150,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,217,150,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,136,171,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,217,171,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
		
#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT,	  20,  45, 0, 3,  0,  0, 0, 0, 0, 0, "File system:" },
	{ GADG_STAT_TEXT,   20,  66, 0, 3,  0,  0, 0, 0, 0, 0, "Status:" },
	{ GADG_STAT_TEXT,	  20,  87, 0, 3,  0,  0, 0, 0, 0, 0, "Volume name:" },
	{ GADG_STAT_TEXT,	  20, 108, 0, 3,  0,  0, 0, 0, 0, 0, "Physical size:" },
	{ GADG_STAT_TEXT,  198, 108, 0, 3,  0,  0, 0, 0, 0, 0, strK },
	{ GADG_STAT_TEXT,	 279, 108, 0, 3,  0,  0, 0, 0, 0, 0, strBlocks },
	{ GADG_STAT_TEXT,	  20, 129, 0, 3,  0,  0, 0, 0, 0, 0, "Logical size:" },
	{ GADG_STAT_TEXT,  198, 129, 0, 3,  0,  0, 0, 0, 0, 0, strK },
	{ GADG_STAT_TEXT,	 279, 129, 0, 3,  0,  0, 0, 0, 0, 0, strBlocks },
	{ GADG_STAT_TEXT,	  20, 150, 0, 3,  0,  0, 0, 0, 0, 0, "Amount free:" },
	{ GADG_STAT_TEXT,  198, 150, 0, 3,  0,  0, 0, 0, 0, 0, strK },
	{ GADG_STAT_TEXT,	 279, 150, 0, 3,  0,  0, 0, 0, 0, 0, strBlocks },
	{ GADG_STAT_TEXT,	  20, 171, 0, 3,  0,  0, 0, 0, 0, 0, "Known bad:" },
	{ GADG_STAT_TEXT,  198, 171, 0, 3,  0,  0, 0, 0, 0, 0, strK },
	{ GADG_STAT_TEXT,	 279, 171, 0, 3,  0,  0, 0, 0, 0, 0, strBlocks },

	{ GADG_STAT_TEXT,   20,  12, 0, 0,  0,  0, 0, 0, 0, 0, "Volume Information" },
#endif	
	{ GADG_ITEM_NONE }
};

GadgetTemplate volInfoGadgets2[] = {
	{ GADG_PUSH_BUTTON,-(STD_BUTTON_WIDTH+20), 12, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_OK,0, _strOK },
	{ GADG_STAT_STDBORDER,20,27, 0, 0,VOL_INFO_WD-(STD_BUTTON_WIDTH+58),  0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
/*	{ GADG_PUSH_BUTTON,-(STD_BUTTON_WIDTH+20), 42, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0,CMD_KEY_PREV, 0, strPrev },*/

	{ GADG_ACTIVE_STDIMAGE,
		-(STD_BUTTON_WIDTH+20-6), 42, 0, 0,
		25-5, 20-4, 0, 0, 0x1F, 0, (Ptr) IMAGE_ARROW_LEFT },
	{ GADG_ACTIVE_STDIMAGE,
		-(STD_BUTTON_WIDTH+20-(6+28)), 42, 0, 0,
		25-5, 20-4, 0, 0, 0x1E, 0, (Ptr) IMAGE_ARROW_RIGHT },

	{ GADG_STAT_STDBORDER,120, 45,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,265, 45,0,0, 62, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,120, 66,0,0,(VOL_INFO_WD-(STD_BUTTON_WIDTH+40))-118, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,120, 87,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,345, 87,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,120,108,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,234,108,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,345,108,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,120,129,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,345,129,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,153,150,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,345,150,0,0, 60, 11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
	{ GADG_STAT_STDBORDER,120,171,0,0, (VOL_INFO_WD-(20+120)),11, 0, 5, 0, 0, (Ptr) BORDER_SHADOWBOX },
		
#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT,   20,  45, 0, 3,  0,  0, 0, 0, 0, 0, "Device name:" },
	{ GADG_STAT_TEXT,  197,  45, 0, 3,  0,  0, 0, 0, 0, 0, "Version:" },
	{ GADG_STAT_TEXT,	  20,  66, 0, 3,  0,  0, 0, 0, 0, 0, "Driver name:" },
	{ GADG_STAT_TEXT,	  20,  87, 0, 3,  0,  0, 0, 0, 0, 0, "Block size:" },
	{ GADG_STAT_TEXT,	 197,  87, 0, 3,  0,  0, 0, 0, 0, 0, "Blocks per track:" },
	{ GADG_STAT_TEXT,	  20, 108, 0, 3,  0,  0, 0, 0, 0, 0, "Cylinders:" },
	{ GADG_STAT_TEXT,	 197, 108, 0, 3,  0,  0, 0, 0, 0, 0, "Low:" },
	{ GADG_STAT_TEXT,	 301, 108, 0, 3,  0,  0, 0, 0, 0, 0, "High:" },
	{ GADG_STAT_TEXT,	  20, 129, 0, 3,  0,  0, 0, 0, 0, 0, "Surfaces:" },
	{ GADG_STAT_TEXT,	 197, 129, 0, 3,  0,  0, 0, 0, 0, 0, "Max.transfer (KB):" },
	{ GADG_STAT_TEXT,	  20, 150, 0, 3,  0,  0, 0, 0, 0, 0, "Blocks reserved:" },
	{ GADG_STAT_TEXT,	 229, 150, 0, 3,  0,  0, 0, 0, 0, 0, "Pre-allocated:" },
	{ GADG_STAT_TEXT,	  20, 171, 0, 3,  0,  0, 0, 0, 0, 0, "Memory type:" },

	{ GADG_STAT_TEXT,   20,  12, 0, 0,  0,  0, 0, 0, 0, 0, "Device Information" },
#endif	
	{ GADG_ITEM_NONE }
};

static DialogTemplate volInfoDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, VOL_INFO_WD, 202, &volInfoGadgets[0], NULL
};

/*
 *	Preferences dialog
 */

static GadgetTemplate prefsGadgets[] = {
	{ GADG_PUSH_BUTTON,		-(STD_BUTTON_WIDTH+20),  10, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'O', 0, _strOK },
	{ GADG_PUSH_BUTTON,		-(STD_BUTTON_WIDTH+20),  40, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'C', 0, _strCancel },
	{ GADG_PUSH_BUTTON,		-(STD_BUTTON_WIDTH+20),  70, 0, 0, STD_BUTTON_WIDTH, 20, 0, 0, 'R', 0, strReset },
	
	{ GADG_RADIO_BUTTON,	 30,  50, 0, 0,  0, 0, 0, 0, 'H', 0, "Hexadecimal" },
	{ GADG_RADIO_BUTTON, 152,  50, 0, 0,  0, 0, 0, 0, 'D', 0, "Decimal" },
		
	{ GADG_RADIO_BUTTON,	 30,  90, 0, 0,  0, 0, 0, 0, 'I', 0, "Interactive" },
	{ GADG_RADIO_BUTTON,	152,  90, 0, 0,  0, 0, 0, 0, 'Q', 0, "Quiet" },
		
	{ GADG_CHECK_BOX,		 30, 186, 0, 0,  0, 0, 0, 0, 'S', 0, "Save Icons With QB Tools Files" },
	{ GADG_CHECK_BOX,		 30, 130, 0, 0,  0, 0, 0, 0, 'F', 0, "Flash Screen" },
	{ GADG_CHECK_BOX,		 30, 146, 0, 0,  0, 0, 0, 0, 'B', 0, "Beep Sound" },
	{ GADG_CHECK_BOX,		 30, 202, 0, 0,  0, 0, 0, 0, 'y', 0, "Bypass AmigaDOS" },
	
	{ GADG_STAT_STDBORDER,20,  25, 0, 0, 342-120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,		 20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Preferences" },
	{ GADG_STAT_TEXT,		 20,  35, 0, 0, 0, 0, 0, 0, 0, 0, "Display block numbers in:" },
	{ GADG_STAT_TEXT,		 20,  75, 0, 0, 0, 0, 0, 0, 0, 0, "User interaction mode:" },
	{ GADG_STAT_TEXT,		 20, 115, 0, 0, 0, 0, 0, 0, 0, 0, "Notification method:" },
	{ GADG_STAT_TEXT,		 20, 171, 0, 0, 0, 0, 0, 0, 0, 0, "Other:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate prefsDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 322+20, 228, prefsGadgets, NULL
};

/*
	 Error dialog, used whenever requester won't do.
*/

static DialogTemplate errorDialogT = {
#if (AMERICAN | BRITISH)
	DLG_TYPE_ALERT, 0, -1, -1, 390, 80, errorGadgets, NULL
#elif GERMAN
	DLG_TYPE_ALERT, 0, -1, -1, 410, 95, errorGadgets, NULL
#elif FRENCH
	DLG_TYPE_ALERT, 0, -1, -1, 410, 95, errorGadgets, NULL
#elif SWEDISH
#endif
};

static DialogTemplate bigErrorDialogT = {
#if (AMERICAN | BRITISH)
	DLG_TYPE_ALERT, 0, -1, -1, 398, 115, errorGadgets, NULL
#elif GERMAN
	DLG_TYPE_ALERT, 0, -1, -1, 418, 130, errorGadgets, NULL
#elif FRENCH
	DLG_TYPE_ALERT, 0, -1, -1, 418, 130, errorGadgets, NULL
#elif SWEDISH
#endif
};

/*
 *	Dialog list
 */

DlgTemplPtr dlgList[] = {
	&aboxDialogT,	&customMacroDialogT,
	&userReq1DialogT,	&userReq2DialogT,		&userReq3DialogT, &userReqTextDialogT,
	&volInfoDialogT,	&errorDialogT,			&bigErrorDialogT, &prefsDialogT,
};

/*
 * Requester list
 */
 
ReqTemplPtr reqList[] = {
	&pageSetupReqT,	&printReqT,		&repairOptionsReqT,
	&fsReqT,			&cancelPrReqT,	&nextPageReqT,	&errorReqT,
	&bigErrorReqT,	&findReqT,		&gotoReqT,		&hashReqT,	
	&findingReqT,	&reorgOptsReqT,&recoverReqT,	&recoverOptsReqT,
	&renameReqT,	&editSaveReqT
};