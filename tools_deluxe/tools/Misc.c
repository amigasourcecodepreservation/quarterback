/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Misc routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <dos/dos.h>
#include <dos/dosextens.h>
#include <intuition/intuition.h>
#include <devices/keyboard.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/DOS.h>
#include <Toolbox/List.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Request.h>
#include <Toolbox/LocalData.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/BarGraph.h>
#include <Toolbox/Packet.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	External variables
 */

extern TextChar				_strYes[], _strNo[], _strCancel[];

extern struct DosLibrary		*DOSBase;
extern TextPtr					strsErrors[], bigStrsErrors[];
extern TextChar				strEllipsis[];
extern WORD						intuiVersion;
extern MsgPortPtr				mainMsgPort;
extern DialogTemplate			alertDlgTempl;
extern DlgTemplPtr				dlgList[];
extern ReqTemplPtr				reqList[];
extern GadgetTemplate			alertGadgets[];
extern ScreenPtr				screen;
extern UBYTE					_tbPenLight, _tbPenBlack;
extern WindowPtr				backWindow, cmdWindow;
extern WORD						waitCount;
extern RequestPtr				requester;
extern ScrollListPtr				scrollList;
extern UWORD					charBaseline;
extern OptionsRec				options;
extern BOOL						decimalLabel;
extern VolParams				currVolParams;
extern WORD						mode, stage;
extern ULONG					xoffset;
extern EditStats				editStats;
extern struct IOStdReq 			*keyRequest;
extern GadgetPtr				currGadgList;

/*
 *	Local variables and definitions
 */

#define DOCWIND_MSGS		(GADGETDOWN|GADGETUP|NEWSIZE|REFRESHWINDOW|ACTIVEWINDOW|INACTIVEWINDOW|RAWKEY|INTUITICKS)
#define DLGWIND_MSGS		(NEWSIZE|REFRESHWINDOW|ACTIVEWINDOW|INACTIVEWINDOW)
#define BACKWIND_MSGS	(REFRESHWINDOW|NEWPREFS)

static ULONG oldOperIDCMPFlags, oldBackIDCMPFlags;
 
static void SetBusyData(void);

/*
 * Hex character array.
 */
 
static UBYTE _hexChars[] = {
	'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'
};

/*
 *	Busy pointer images
 */

static UWORD __chip busyPointerData[] = {
	0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000
};

static Pointer busyPointer = {
	14, 14, -7, -7, &busyPointerData[0]
};

static UWORD busyData1[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1E60, 0x1FE0, 0x3E10, 0x3FF0, 0x7E08,
	0x3FF0, 0x7E08, 0x7FF8, 0xFE04, 0x7FF8, 0xFE04, 0x7FF8, 0x81FC,
	0x7FF8, 0x81FC, 0x3FF0, 0x41F8, 0x3FF0, 0x41F8, 0x1FE0, 0x21F0,
	0x0780, 0x19E0, 0x0000, 0x0780,
	0x0000, 0x0000
};

static UWORD busyData2[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1FE0, 0x1FE0, 0x3FF0, 0x3FF0, 0x5FC8,
	0x3FF0, 0x4F88, 0x7FF8, 0x8704, 0x7FF8, 0x8204, 0x7FF8, 0x8104,
	0x7FF8, 0x8384, 0x3FF0, 0x47C8, 0x3FF0, 0x4FE8, 0x1FE0, 0x3FF0,
	0x0780, 0x1FE0, 0x0000, 0x0780,
	0x0000, 0x0000
};

static UWORD busyData3[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x19E0, 0x1FE0, 0x21F0, 0x3FF0, 0x41F8,
	0x3FF0, 0x41F8, 0x7FF8, 0x81FC, 0x7FF8, 0x81FC, 0x7FF8, 0xFE04,
	0x7FF8, 0xFE04, 0x3FF0, 0x7E08, 0x3FF0, 0x7E08, 0x1FE0, 0x3E10,
	0x0780, 0x1E60, 0x0000, 0x0780,
	0x0000, 0x0000
};

static UWORD busyData4[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1860, 0x1FE0, 0x2010, 0x3FF0, 0x6038,
	0x3FF0, 0x7078, 0x7FF8, 0xF8FC, 0x7FF8, 0xFDFC, 0x7FF8, 0xFEFC,
	0x7FF8, 0xFC7C, 0x3FF0, 0x7838, 0x3FF0, 0x7018, 0x1FE0, 0x2010,
	0x0780, 0x1860, 0x0000, 0x0780,
	0x0000, 0x0000
};

static WORD busyNum;
static WindowPtr busyWindow;
static UWORD *busyData[] = {
	&busyData1[0], &busyData2[0], &busyData3[0], &busyData4[0]
};

/*
 *	Set up system for lengthy operation
 *	If window is not NULL then window should be set up for requester
 */

void BeginWait()
{
	SetArrowPointer();

	if (waitCount++)
		return;
	
	if( cmdWindow != NULL ) {
		oldOperIDCMPFlags = cmdWindow->IDCMPFlags;
		ModifyIDCMP(cmdWindow, DOCWIND_MSGS);
	}
	oldBackIDCMPFlags = backWindow->IDCMPFlags;
	ModifyIDCMP(backWindow, BACKWIND_MSGS);
	
	FlushMainPort();
}

/*
 * Flush the message port without regard as to what it contains.
 */
 
void FlushMainPort()
{
	register MsgPtr msg;
	register MsgPortPtr msgPort = mainMsgPort;
	
	if( msgPort != NULL ) {	
		while( msg = GetMsg(msgPort)) {
			ReplyMsg(msg);
		}
	}
}

/*
 *	Reset system to normal state
 */

void EndWait()
{
	Delay(5L);

/*
	Handle BeginWait/EndWait nesting
*/
	if( --waitCount )
		return;
/*
	First enable messages, so we don't miss any menu messages
*/
	if( cmdWindow != NULL ) {
		ModifyIDCMP(cmdWindow, oldOperIDCMPFlags);
	}
	ModifyIDCMP(backWindow, oldBackIDCMPFlags);
}

/*
 * Convert a long (which should really only go to 4K max, otherwise truncated)
 * into a three-digit hexadecimal string Not null-terminated.
 */
 
void NumTo3DigitHexString(register ULONG offset, register TextPtr buff)
{
	*buff++ = _hexChars[(offset >> 8) & 0xF];
	*buff++ = _hexChars[(offset & 0xF0) >> 4];
	*buff = _hexChars[offset & 0xF];
}

/*
 * Convert an unsigned longword into a hex string eight(+1) characters long.
 * Terminate with a space, but no null-terminator needed.
 */
 
void LongToHexString(register ULONG val, register TextPtr buff)
{
	register WORD i;
	
	buff += 8;
	*buff = ' ';
	for( i = 0 ; i < 8 ; i++ ) {
		*--buff = _hexChars[val & 0xF];
		val >>= 4;
	}
}

/*
 * Output a number into a string, respecting the Dec/Hex setting.
 * Puts leading zeros in the hex string, and a "$" as well.
 * Commas are present in the dec string.
 * However, if "punc" is FALSE, the punctuation is not put in, and if
 * "leading" is FALSE, the leading zeros are not put in the hex string.
 */
 
void OutputHexOrDecString(ULONG num, TextPtr buff, BOOL decimal, BOOL punc, BOOL leading)
{
	register WORD i;
	register TextPtr ptr = buff;
	register WORD len;
	register WORD strLen;
	
	if( decimal ) {
		if( punc ) {
			NumToCommaString(num, ptr);
		} else {
			NumToString(num, ptr);
		}
	} else {
		if( punc ) {
			*ptr++ = '$';
		}
		LongToHexString(num, ptr);
		ptr[len = 8] = '\0';
		if( !leading ) {
			/*len = strlen(ptr);*/
			strLen = len-1;
			for( i = 0 ; i < strLen; i++, len-- ) {
				if( *ptr == '0' ) {
					BlockMove( ptr+1, ptr, len);
				}
			}
		}
	}
}

/*
 * Outputs hex or dec number, wiping out all unsavory characters for edit texts.
 * This is what you want for all EDIT_TEXT strings.
 */
 
void OutputEditHexOrDecString(ULONG num, TextPtr buff, BOOL decimal)
{
	OutputHexOrDecString(num, buff, decimal, !decimal, FALSE);
}

/*
 * Outputs hex or dec number, with all the punctuation to make it pretty, but
 * no leading zeros. This is normally what you want to print out for STAT_TEXTs.
 */

void OutputStatHexOrDecString(ULONG num, TextPtr buff, BOOL decimal)
{
	OutputHexOrDecString(num, buff, decimal, TRUE, FALSE);
}

/*
 *	Set pointer for window to busy pointer
 */

void SetBusyPointer()
{
	busyNum = 0;
	SetBusyData();
	busyWindow = cmdWindow;
	busyNum -= 2;
}

/*
 * Blasts the pointer data for its cursor
 */
 
static void SetBusyData()
{
	register WORD i;
	
	for (i = 2; i < 2*busyPointer.Height + 2; i++)
		busyPointerData[i] = busyData[busyNum][i];
}

/*
 *	Set busy pointer to next in sequence
 */

void NextBusyPointer()
{
	ULONG seconds, micros ;
	static ULONG lastMicro = 0;
	
	CurrentTime( &seconds, &micros );	
	
	if( ( micros - lastMicro ) > 100000 ) {
		lastMicro = micros;
		switch(++busyNum){
		case -2:
		case -1:
			break;
		case 0:
			SetPointer(busyWindow, busyPointer.PointerData, busyPointer.Height,
				busyPointer.Width, busyPointer.XOffset, busyPointer.YOffset);
			break;
		case 4:
			busyNum = 0;
		default:
			SetBusyData();
			break;
		}
	}
}

/*
 * Verify that the user really wants to abort the operation.
 */
 
BOOL CheckAbort()
{
	register BOOL result;
	
	if( (mode == MODE_OPTIMIZE) && (stage > STAGE_RESOLVING) ) {
		result = DoDialog(bigStrsErrors[BIG_ERR_REORG_ABORT], CANCEL_BUTTON, _strYes, _strNo, NULL, TRUE) == OK_BUTTON;
	} else {
		result = WarningDialog(strsErrors[ERR_ABORT], CANCEL_BUTTON) == OK_BUTTON;
	}
	return(result);
}

/*
 * For convenience
 */
 
void SetWaitPointer()
{
	if( cmdWindow != NULL )
		SetStdPointer(cmdWindow, POINTER_WAIT);
}

/*
 * For convenience
 */
 
void SetArrowPointer()
{
	if( cmdWindow != NULL )
		SetStdPointer(cmdWindow, POINTER_ARROW);
}

/*
 *	Flash the screen and/or beep the speaker
 */

void ErrBeep()
{
	if( options.PrefsOpts.Flash )
		DisplayBeep(screen);
	if( options.PrefsOpts.Beep )
		StdBeep();
}

/*
 * Sound a single-beep on speaker
 */

void StdBeep()
{
	SysBeep(5);
}

/*
 *	Check string for positive numeric value
 */

BOOL CheckNumber(TextPtr s)
{
	if (strlen(s) == 0)
		return (FALSE);
	while (*s) {
		if (*s < '0' || *s > '9')
			return (FALSE);
		s++;
	}
	return (TRUE);
}

/*
 *	Outline first button in requester
 */

void OutlineOKButton(DialogPtr dlg)
{
	if( requester != NULL ) {
		OutlineButton( requester->ReqGadget, dlg, requester, TRUE);
	} else {
		OutlineButton( GadgetItem(dlg->FirstGadget, 0), dlg, NULL, TRUE);
	}
} 

/*
 * Toggle checkbox boolean value and change its appearance
 */
 
void ToggleCheckbox( BOOL *value, WORD item, DialogPtr dlg)
{
	*value = !*value;
	if( requester != NULL ) {
		SetGadgetItemValue(requester->ReqGadget, item, dlg, requester, *value);
	} else {
		SetGadgetItemValue(dlg->FirstGadget, item, dlg, NULL, *value);
	}
	return;
}

/*
 * Turn dialog/requester gadget on or off
 */
 
void GadgetOnOff(DialogPtr dlg, WORD item, BOOL onOff)
{
	if( requester != NULL ) {
		SetGadgetItemValue(requester->ReqGadget, item, dlg, requester, onOff);
	} else {
		SetGadgetItemValue(dlg->FirstGadget, item, dlg, NULL, onOff);
	}
}

void GadgetOn(DialogPtr dlg, WORD item)
{
	GadgetOnOff(dlg, item, 1);
}

/*
 * Turn dialog/requester gadget off
 */
 
void GadgetOff(DialogPtr dlg, WORD item)
{
	GadgetOnOff(dlg, item, 0);
}

/*
 * Process a DOS packet through toolbox, but ONLY if packets don't lock us up!
 */
 
BOOL DoToolsPacket(MsgPortPtr procID, LONG action, LONG args[], WORD numArgs)
{
	BOOL success = FALSE;
	
	if( !options.PrefsOpts.BypassDOS ) {
		success = DoPacket(procID, action, args, numArgs);
	}
	return(success);
}

/*
 *	DosFlagType
 *	Returns correct LDF_--- value for DLT_--- value.
 */

LONG DosFlagType(LONG listType)
{
	LONG flags;

	switch(listType) {
	case DLT_DEVICE:
		flags = LDF_DEVICES | LDF_READ;
		break;
	case DLT_VOLUME:
		flags = LDF_VOLUMES | LDF_READ;
		break;
	case DLT_DIRECTORY:
		flags = LDF_ASSIGNS | LDF_READ;
		break;
	}
	return(flags);
}

/*
 *	NHLockDosList
 *	Locks dos list and return first node of type listType
 *	1.3 and 2.0 compatible.
 */

struct DevInfo *NHLockDosList(LONG listType)
{
	register struct DosList *dList = NULL;
	struct DosInfo *dosInfo;
		
	if( (LibraryVersion((struct Library *) DOSBase) >= OSVERSION_2_0_4) &&
		(!options.PrefsOpts.BypassDOS) ) {
		dList = LockDosList(DosFlagType(listType));
	} else {
		Forbid();
	}
	if( dList == NULL ) {
		dosInfo = (struct DosInfo *) BADDR( ((struct RootNode *)DOSBase->dl_Root)->rn_Info );
		dList = (struct DosList *) BADDR(dosInfo->di_DevInfo);
		if( dList->dol_Type == listType ) {
			return( (struct DevInfo *)dList );
		}
	}
	return( NHNextDosEntry((struct DevInfo *)dList, listType) );
}

/*
 *	NHNextDosEntry
 *	Returns next entry of type "listType".
 *	1.3 and 2.0 compatible.
 */

struct DevInfo *NHNextDosEntry(register struct DevInfo *dList, LONG listType)
{
	struct DevInfo *dev = NULL;

	if( (dList != 0L) && (dList != (struct DevInfo *)1L) ) {
		if( (LibraryVersion((struct Library *) DOSBase) >= OSVERSION_2_0_4) &&
			 (!options.PrefsOpts.BypassDOS) ) {
			dev = (struct DevInfo *) NextDosEntry((struct DosList *)dList, DosFlagType(listType));
		} else {
			dev = dList;
			do {
				dev = BADDR(dev->dvi_Next);
			} while( dev != NULL && (dev->dvi_Type != listType));
		}
	}
	return(dev);
}

/*
 *	NHUnlockDosList
 *	Unlocks DOS list.
 *	1.3 and 2.0 compatible.
 */

void NHUnlockDosList(struct DevInfo *dList, LONG listType)
{
	if( (dList != 0L) && (dList != (struct DevInfo *)1L) ) {
		if( options.PrefsOpts.BypassDOS) {
			Permit();
		} else {
			UnLockDosList1(DosFlagType(listType));
		}
	}
}

#ifdef NO_MARK_CODE
/*
 *	NHIsFileSystem
 *	Determine if volume has a file system
 *	volume is name of volume with trailing ":"
 *	1.3 and 2.0 compatible.
 */

BOOL NHIsFileSystem(TextPtr volume)
{
	BOOL isFileSys;
	BPTR lock;

	if (intuiVersion >= OSVERSION_2_0_4 ) {
		isFileSys = IsFileSystem(volume);
	} else {
		if (lock = Lock(volume, ACCESS_READ)) {
			isFileSys = TRUE;
			UnLock(lock);
		} else {
			isFileSys = FALSE;
		}
	}
	return isFileSys;
}
#endif

/*
 *	NHFindDosEntry
 *	Find the node of the device passed in dname and with the flags.
 *	Must be called with dos list locked just as the 2.0 function.
 *	1.3 and 2.0 compatible.
 */

struct DevInfo *NHFindDosEntry(register struct DevInfo *devList, TextPtr dname, ULONG flags)
{
	register TextPtr nodeName;
	TextChar buff[MAX_FILENAME_LEN+1];
	register WORD len;
	
/*
	If you pass an entry with a trailing colon, we'll ignore it for 'ya.
*/
	len = strlen(dname);
	strcpy(buff, dname);
	if( len-- && (buff[len] == ':') ) {
		buff[len] = '\0';
	}
	if( LibraryVersion((struct Library *) DOSBase) >= OSVERSION_2_0_4 ) {

		devList = (struct DevInfo*) FindDosEntry((struct DosList *)devList, buff, DosFlagType(flags));
	} else {
		while( devList != NULL ) {
			nodeName = (char *)((ULONG) BADDR(devList->dvi_Name));
			if( CmpString(&nodeName[1], buff, *nodeName, len, FALSE) ==0 ) {
				break;
			}
			devList = NHNextDosEntry(devList, flags);
		}
	}
	return(devList);
}

#ifdef OBSOLETE_PACKET_CALL
/*
 *	DoPkt() by A.Finkel, P.Lindsay, C.Scheppner
 *				modified by M.Thomas & GM.
 *
 *	Uses the global port pktPort for replyport.
 */

BOOL dopkt(struct MsgPort *procID, register struct StandardPacket *packet)
{
	BOOL success;
	
	if( success = (pktPort != NULL) && (packet != NULL) ) {

		packet->sp_Msg.mn_Node.ln_Name = (char *)&(packet->sp_Pkt);
		packet->sp_Pkt.dp_Link = &(packet->sp_Msg);
		packet->sp_Pkt.dp_Port = pktPort;

		PutMsg(procID, (MsgPtr)packet); /* send packet */

		WaitPort(pktPort);
		GetMsg(pktPort);
		success = packet->sp_Pkt.dp_Res1 == DOSTRUE;
	}
	return(success);
}
#endif

/*
 * Converts and copies BCPL string passed to a C string as destination
 */

void ConvertBCPLToCString(TextPtr bstr, TextPtr cstr)
{
	register WORD len;
	register TextPtr srcStr = (TextPtr) bstr;
	
	len = (WORD) *srcStr++;
	BlockMove(srcStr, cstr, len);
	cstr[len] = 0;
}

#ifdef I_LIKE_SWILL
/*
 *	ShowStdAlert - present a standard alert with the text that is passed
 */

void ShowStdAlert(TextPtr str)
{
	alertGadgets[1].Info = (Ptr)str;
	StdAlert(&alertDlgTempl, screen, alertPort, NULL);
	alertGadgets[1].Info = NULL;
}
#endif

#ifdef NEED_STDDIALOG
/*
 *	Handle simple dialog and return result
 *	Return -1 if not enough memory for dialog
 */

WORD StdDialog(WORD dlgNum)
{
	WORD item;
	DialogPtr dlg;
	
	BeginWait();
	if( (dlg = GetDialog(dlgList[dlgNum], screen, mainMsgPort)) != NULL ) {
		OutlineOKButton(dlg);
		do {
			item = ModalDialog(mainMsgPort, dlg, DialogFilter);
		} while( item == -1 );
		DisposeDialog(dlg);
	} else {
		Error(ERR_NO_MEM);
		item = -1;
	}
	EndWait();
	return(item);
}
#endif

/*
 * Standard no-frills alert-type dialog call
 */
 
WORD StdRequest(WORD dlgNum)
{
	WORD item;
	RequestPtr req;
	
	BeginWait();
	if( (req = DoGetRequest(dlgNum)) != NULL ) {
		requester = req;
		OutlineOKButton(cmdWindow);
		ErrBeep();
		do {
			item = ModalRequest(mainMsgPort, cmdWindow, DialogFilter);
		} while(item == -1);
		DestroyRequest(req);
	} else {
		Error(ERR_NO_MEM);
		item = -1;
	}
	EndWait();
	return(item);
}

/*
 * Builds a path from a filename and lock.
 */

BOOL BuildPath(TextPtr fileName, TextPtr destPtr, Dir dirLock, WORD maxLen)
{
	struct FileInfoBlock fib;
	BOOL success = TRUE;
	register TextPtr newName;
	register WORD nameLen, newLen;
	register Dir dir, parentDir;

	strcpy(destPtr, fileName);
	newName = fib.fib_FileName;
	dir = DupLock(dirLock);
	while( dir ) {
		if( !Examine(dir, &fib))
			break;
		nameLen = strlen(destPtr);
		newLen = strlen(newName);
		if( nameLen + newLen > maxLen )
			success = FALSE;
		BlockMove( destPtr, destPtr + newLen + 1, nameLen + 1);
		BlockMove( newName, destPtr, newLen);
		parentDir = ParentDir(dir);
		destPtr[newLen] = (parentDir) ? '/' : ':';
		UnLock(dir);
		dir = parentDir;
	}
	if( newLen = strlen(destPtr) ) {
		if( destPtr[--newLen] == '/' ) {
			destPtr[newLen] = 0;
		}
	}
	if( dir ) {
		UnLock(dir);
	}
	return(success);
}

/*
 * Insert thousand separators into a numeric string, output into buff.
 */

void NumToCommaString(LONG num, TextPtr str)
{
	TextChar buff[20];
	register TextPtr destPtr;
	register TextPtr srcPtr;
	register WORD i;

	NumToString(num, buff);

	i = strlen(buff);
	srcPtr = &buff[i];
	destPtr = str + i + ((i-1) / 3);

	i = 0;
	while( srcPtr >= &buff[0] ) {
		if( (i++ == 4) && (destPtr != str) ) {
			*destPtr-- = THOUSAND_SEP;
			i = 2;
		}
		*destPtr-- = *srcPtr--;
	}
}

/*
 *	Filter function for ModalDialog/ModalRequest
 *	Handles ALL window re-sizes and updates
 *	For single window applications only.
 *	cmdWindow is global pointer to the current application window.
 */

BOOL DialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	register ULONG class;
	BOOL success;
	/* register WindowPtr window = intuiMsg->IDCMPWindow; */
	
	class = intuiMsg->Class;
	*item = -1;
	if( success = (class == REFRESHWINDOW || class == NEWSIZE || class == NEWPREFS ||
		(requester == NULL && (class & DLGWIND_MSGS))) ) {
		DoIntuiMessage(intuiMsg);
	}
	return(success);
}

/*
 *	Clears given rectangle, and then prints number right justified in rectangle.
 */

void PrintRightNum(register RastPtr rPort, Rectangle *rectPtr, ULONG num)
{
	TextChar numBuff[16];
	
	OutputStatHexOrDecString(num, numBuff, decimalLabel);
	PrintRightStr(rPort, rectPtr, numBuff);
}

/*
 * Clears given rectangle, and then prints string right-justified in it.
 */
  
void PrintRightStr(register RastPtr rPort, Rectangle *rectPtr, register TextPtr str)
{
	register WORD len;
	register WORD textLen;
	Rectangle rect = *rectPtr;
	register WORD width = WIDTH(&rect);
	
	InsetRect(&rect, 1, 1);
	rect.MaxX--;
	SetAPen(rPort, _tbPenLight);
	len = TextLength(rPort, str, textLen = strlen(str));
	Move(rPort, (rect.MaxX-MIN(width,len)), rect.MinY + 2 + charBaseline);
	FillRect(rPort, &rect);
	SetAPen(rPort, _tbPenBlack);
	TextInWidth(rPort, str, textLen, width, FALSE);
}
	
/*
 *	Return TRUE if screen font is topaz-8 or equivalent
 */

BOOL IsTopaz8ScreenFont(ScreenPtr screen)
{
	TextFontPtr font = screen->RastPort.Font;

	return (font->tf_YSize == 8 && font->tf_XSize == 8 &&
			(font->tf_Flags & FPF_PROPORTIONAL) == 0);
}

/*
 * Initialize a minimum list structure to empty.
 */
 
void NHInitList(register struct List *list)
{
	BlockClear(list, sizeof(struct List));
	list->lh_Head = (NodePtr) &list->lh_Tail;
	list->lh_TailPred = (NodePtr) list;
}

/*
 * Calls GetRequest() after refreshing window
 */

RequestPtr DoGetRequest(WORD id)
{
	if( (cmdWindow->Height - screen->BarHeight) < 3 ) {
		if( intuiVersion >= OSVERSION_2_0 ) {
			ZipWindow(cmdWindow);
			Delay(5);
			DoNewSize();
		}
	}
	if( scrollList != NULL ) {
		SLDrawBorder(scrollList);
	}
	return(GetRequest(reqList[id], cmdWindow, TRUE));
}

/*
 * End, Dispose, and reset global requester variable
 */
 
void DestroyRequest(RequestPtr req)
{
	register WindowPtr window;
	
	if( req != NULL ) {
		if( (window = req->RWindow) != NULL ) {
			EndRequest(req, req->RWindow);
			DisposeRequest(req);
			requester = window->FirstRequest;
		} else {
			DisposeRequest(req);
		}
	}
}

/*
 * Updates a bar graph whose rastport might have been obliterated
 */
 
void DoDrawBarGraph(RequestPtr req, BarGraphPtr barGraph)
{
	if( barGraph != NULL ) {
		if( req != NULL ) {
			Forbid();
			if( req->ReqLayer != NULL ) {
				LockLayer(0, req->ReqLayer);
				Permit();
				DrawBarGraph(req->ReqLayer->rp, barGraph);
				UnlockLayer(req->ReqLayer);
			} else {
				Permit();
			}
		}
	}
}

/*
 * Sets a bar graph whose rastport might have been obliterated
 */

void DoSetBarGraph(RequestPtr req, BarGraphPtr barGraph, ULONG num)
{
	if( barGraph != NULL ) {	
		if( req != NULL ) {
			Forbid();
			if( req->ReqLayer != NULL ) {
				LockLayer(0, req->ReqLayer);
				Permit();
				SetBarGraph(req->ReqLayer->rp, barGraph, num);
				UnlockLayer(req->ReqLayer);
			} else {
				Permit();
			}
		}
	}
}

/*
 * Sets a bar graph whose rastport might have been obliterated
 */

void DoSetBarGraphMax(RequestPtr req, BarGraphPtr barGraph, ULONG num)
{
	if( barGraph != NULL ) {	
		if( req != NULL ) {
			Forbid();
			if( req->ReqLayer != NULL ) {
				LockLayer(0, req->ReqLayer);
				Permit();
				SetBarGraphMax(req->ReqLayer->rp, barGraph, num);
				UnlockLayer(req->ReqLayer);
			} else {
				Permit();
			}
		}
	}
}
/*
 * Returns whether this block ID calls for a checksum to be done.
 */

BOOL IsChecksumableBlock(ULONG *blockPtr)
{
	register ULONG id = *blockPtr;
	register BOOL checksumable = FALSE;
	register ULONG currOffset = xoffset;
	
	if( !IsBitMapExtKey(currOffset) && (currOffset >= currVolParams.LowestKey) ) {
		if( !(checksumable = IsBitMapKey(currOffset)) ) {
			checksumable = ((id == T_DATA) && (!currVolParams.FFSFlag)) ||
				(id == T_SHORT) ||
				(id == T_LIST) ||
				(currVolParams.DirCache && (id < 256) && (id & T_DIRCACHE));
		}
	}
	return(checksumable);
}

/*
 * A kerning-less version of TextInWidth()
 */
 
void DoTextInWidth(register TextPtr text, register WORD width, BOOL atStart)
{
	register RastPtr rPort = cmdWindow->RPort;
	register WORD len = strlen(text);
	register WORD drawWidth, extra;

	drawWidth = TextLength(rPort, text, len);

	if (drawWidth <= width)
		Text(rPort, text, len);
	else {
		extra = TextLength(rPort, strEllipsis, 3);
		while (len && TextLength(rPort, text, len) + extra > width) {
			if (atStart)
				text++;
			len--;
		}
		if (atStart)
			Text(rPort, strEllipsis, 3);
		Text(rPort, text, len);
		if (!atStart)
			Text(rPort, strEllipsis, 3);
	}
	WaitBlit();
}

/*
 * Read the current key matrix to determine the ALT keys' status. Return TRUE if down.
 */
 
BOOL GetAltKeyStatus(WORD modifier)
{
	BOOL altStatus;
	UBYTE keyMatrix[16];	/* Should be in PUBLIC memory really, but it's only for 1.3 */

	if( intuiVersion < OSVERSION_2_0 ) {
		if( keyRequest != NULL ) {
			keyRequest->io_Command = KBD_READMATRIX;
			keyRequest->io_Data = (APTR) keyMatrix;
			keyRequest->io_Length = 13;
			DoIO((struct IORequest *) keyRequest);
			altStatus = (keyMatrix[12] & 0x30) != 0;
		}
	} else {
		altStatus = (modifier & ALTKEYS) != 0;
	}
	return(altStatus);
}
