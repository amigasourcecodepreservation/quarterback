/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Quarterback Tools
 * Copyright (c) 1992 New Horizons Software, Inc.
 *
 * Text strings
 */

#include <exec/types.h>

#include <TypeDefs.h>

#include <Toolbox/Language.h>

#include "Tools.h"

TextPtr initErrors[] = {
	"Can't open screen",
	"Not enough memory",
	"Quarterback Tools already running",
	"Can't open window",
};

TextPtr strsErrors[] = {
	"Unrecognized error type.",
	"Not enough memory.",
	"Unable to save file.\n",
	"Unable to print.",
	"Are you sure it is OK to abort?",
	"Improper number of copies.",
	"Improper page size.",
	"No disk present. Please insert a disk.",
	"Block not in valid range.",
	"Unable to get device information.",
	"Unable to read root block.",
	"Cannot mark any more bad blocks.",
	"Warning: The disk has been changed. Do\nyou wish to write this block anyway?",
	"Disk is locked. Please unlock the disk.",
	"Unable to open device.",
	"Unable to read bitmap.",
	"Bitmap is invalid.",
	"Unable to get cache buffer.",
	"Error deleting file.",
	"Error reading block % from disk.",
	"Error writing block % to disk.",
	"Error writing bitmap to disk.",
	"Repaired bitmap corrupt - not written.",
	"Error reading \"bad.blocks\" file.",
	"Error writing \"bad.blocks\" file.\nVolume is full.",
	"Unable to write to capture file.\n",
	"Unable to find match.",
	"Checksum appears to be incorrect.\nDo you want to fix it?",
	"Changes were made to this disk block.\nSave changes before %?",
	"Unknown disk type. Do you believe\nthis to be a valid AmigaDOS disk?",
	"This disk appears not to contain\nan AmigaDOS file system. Proceed anyway?",
	"Unable to proceed. This disk does\nnot appear to be an AmigaDOS volume.",
 	"You cannot rename to a different\nvolume or drawer. Please try again.",
	"ERROR: Dir already in list.\nOperation aborted.",
	"Quarterback Tools does not support\nthis file system at this time.",
	"Unable to read data block %.",
	"Unable to read control block %.",
	"Control block has checksum error.",
	"Control block has bad type.",
	"Remapping problem during optimization.",
	"Unable to recover to this volume.\nPlease choose a different volume.",
	"That is an existing filename. Please\ntype in a volume or drawer name.",
	"Unable to copy recovered file.\n",
	"Volume bitmap is invalid. Files must\nbe recovered to a different volume.",
	"Optimization was successful.",
	"All directory cache blocks must\nfirst be validated. Optimization aborted.",
	"Are you sure you want to save\ncurrent settings as default values?",
	"Volume has ~ block(s) already marked\nas bad. Do you wish to recheck them?",
	"Because files were skipped, the\nbitmap cannot be checked for errors.",
	"AREXX not present.",
	"Macro execution failed."
};

TextPtr bigStrsErrors[] = {
	"DANGER: ~ bad block(s) are part of\nexisting files. Use \"Check for bad\nfiles\" to identify these files. Next,\ncopy these files to a different volume,\ndelete them, and rerun this test.",
	"CAUTION: Repairing a bad volume\nmay cause loss of files. You should\ncopy critical files to a different\nvolume before attempting a repair. Do you\nwish to proceed with the repair now?",
	"Unable to check for bad blocks, because\nthere is not a valid volume bitmap,\nwhich is needed to identify and mark\nbad blocks.",
	"The root block is unreadable, making all\nfiles inaccessible. To recover these\nfiles, copy them to a different volume\nusing \"Recover Lost/Deleted Files\".\nThen, reformat this volume if possible.",
	"CAUTION: Volume optimization can cause\nlost or scrambled files.\nYou should proceed only if you have\na backup of all important files. Do you\nwish to proceed with the optimization?",
	"This disk does not have a valid AmigaDOS\nvolume bitmap. Optimization cannot be\nperformed without a valid bitmap.",
	"This volume is too full, and cannot\nbe optimized. This volume needs\n~ free blocks in order to be\noptimized. Please free up some disk\nspace on this volume and try again.",
	"An unreadable block has been found\non this volume. Optimization cannot\nbe performed until this volume has\nall of its bad blocks marked by the tool\n\"%.\"",
	"This volume has a critical error which\nmust be repaired before optimization\ncan proceed. To correct the problem,\ntry \"%.\"",
	"This volume has a potential problem\nwhich could affect optimization.\nTo be absolutely safe, first try\n\"%\" to repair it.",
	"Volume has passed basic integrity checks.\n\nFragmented files: ~ out of %.\nFile fragmentation: ",
	"Optimization was successful.\nBecause there are resident references\nto the old organization, the drive will\nbe inaccessible to AmigaDOS until the\nnext reboot.",
	"CAUTION: Optimization has failed!\nSome files may be lost or scrambled.\nCopy files to a different volume, then\nreformat this volume and restore from\na backup of this volume.",
	"CAUTION: Aborting optimization will\ncause lost or scrambled files.\nYou should abort only if you have\na backup of all important files.\nDo you wish to abort anyway?",
	"The recover scan has found ~ files,\nbelonging to % lost drawers. ",
	"The file \"%\"\nis damaged and cannot be fully recovered.\nDo you wish to salvage as much as possible\nof the damaged file?",
	"Unable to read block ~ from disk.\nThe file \"%\"\nis therefore not completely recoverable.\nDo you wish to attempt to salvage the\nrest of the file?",
	"The file \"%\"\ndoes not have its protection flag set\nto allow reading. Do you wish to ignore\nthe flag and copy this file anyway?",
	"Sorry, there is no safe area left on\nthe disk to create a directory cache\nblock for this drawer. To recover\nthe remainder of the data, recover to a\ndifferent volume instead and try again.",
	"Sorry, but due to bad blocks in the\narea used to keep track of bad blocks,\nthis volume cannot mark bad blocks.",
};

/*
 * Errors encountered on analyze/repair.
 * These strings should not exceed 32 characters (not counting appended period).
 */
 
TextPtr repairErrors[] = {
	"%header contains extra data",
	"Drawer entry key out of range",
	"Drawer entry checksum error",
	"Unknown secondary type",
	"Drawer entry claims to be root",
	"%is cross-linked",
	"%header own key mismatch",
	"Drawer entry unreadable",
	"Hash chain link key out of range",
	"%parent key mismatch",
	"%linked into wrong hash chain",
	"Hash chain entry improperly sorted",
	"%header invalid name",
	"%header invalid comment",
	"%header invalid date stamp",
	"%first data block mismatch",
	"%has incorrect block count",
	"%data key ~ out of range",
	"%data key ~ unreadable",
	"%data key ~ cross-linked",
	"%ext header key out of range",
	"%size error",
	"%ext header unreadable",
	"%ext header checksum error",
	"%data block header error",
	"%ext header type is bad",
	"%ext header own key mismatch",
	"%ext header parent mismatch",
	"%ext header cross-linked",
	"Root drawer unreadable",
	"Root drawer contents corrupted",
	"Bitmap key ~ is bad",
	"Bitmap unreadable",
	"Bitmap checksum error",
	"Bad bitmap extension key",
	"Bitmap extension unreadable",
	"Extraneous bitmap key ~",
	"Extraneous bitmap extension key",
	"%hash chain key cross-linked",
	"Bitmap contents incorrect",
	"%data block checksum error",
	"Root block checksum error",
	"Hard link points to invalid key",
	"Soft-linked pathname too long",
	"DC block ~ key out of range",
	"DC block ~ cross-linked",
	"DC block ~ unreadable",
	"DC block ~ own key mismatch",
	"DC block ~ checksum error",
	"DC block ~ parent mismatch",
	"DC block ~ appears garbled",
	"%header date in future",
};

/*
 * Actions taken on analyze/repair.
 */
 
TextPtr repairActions[] = {
	"Use \"Repair\" mode to fix",
	"Bad entry%deleted",
	"Checksum%recalculated",
	"Error%corrected",
	"Entry%properly relinked",
	"Hash chain%sorted",
	"Name%replaced",
	"Comment%deleted",
	"Date/time%set to current",
	"File%deleted",
	"All files are inaccessible",
	"New key%allocated",
	"Hash chain%deleted",
	"Data key%deleted",
	"Bitmap%reconstructed",
};

TextPtr repairActionsVerbs[] = {
	" will be ",
	" was ",
	" was not ",
};

/*
 * Repair result strings.
 */
 
TextPtr strsResults[] = {
	"Major problems found:    ~",
	"Minor problems found:    ~",
	"Total files processed:   ~",
	"Bad files %      ~",
	"Total links processed:   ~",
	"Bad links %      ~",
	"Total drawers processed: ~",
	"Bad drawers detected:    ~",
	"Unreadable blocks found: ~",
	"Volume status: ",
};

/* 
 * Results for volume status.
 */
 
TextPtr strsResStatus[] = {
	"All files appear to be good.",
	"% corrected. Volume usable.",
	"% exist. Use \"Repair\" to fix.",
	"Volume bad. Restore files to different volume.",
};

/*
 * For the disk-editor window, a labelling facility.
 */

TextChar strReserved[]		= "Reserved";
TextChar strBitMapData[]	= "Bitmap Data";
TextChar strBitMapKey[]		= "Bitmap Key";
TextChar strChecksum[]		= "Checksum";
TextChar strHashTable[]		= "Hash Table Key";
TextChar strKeyTable[]		= "File Key";
TextChar strBlockType[]		= "Block Type";
TextChar strOwnKey[]			= "Own Key";
TextChar strBlockCount[]	= "Block Count";
TextChar strExtKey[]			= "Extension Key";
TextChar strHeaderKey[]		= "Header Key";
TextChar strSeqNum[] 		= "Sequence Number";
TextChar strDataSize[]		= "Data Size";
TextChar strNextData[]		= "Next Data Key";
TextChar strBitMapValid[]	= "Bitmap Valid Flag";
TextChar strBitMapExtKey[]	= "Bitmap Extension Key";
TextChar strVolumeName[]	= "Volume Name";
TextChar strFileName[]		= "File Name";
TextChar strDirName[]		= "Drawer Name";
TextChar strLinkName[]		= "Link Name";
TextChar strHashChain[]		= "Hash Chain Key";
TextChar strParentKey[]		= "Parent Key";
TextChar strSecType[]		= "Secondary Type";
TextChar strProt[]			= "Protection Flags";
TextChar strComment[]		= "Comment";
TextChar strModDate[]		= "Modification Date";
TextChar strVolCrDate[]		= "Volume Creation Date";
TextChar strRootModDate[]	= "Root Modification Date";
TextChar strVolModDate[]	= "Volume Modification Date";
TextChar strPrevLink[]		= "Previous Hard Link Key";
TextChar strLinkToObject[]	= "Hard Link To Object Key";
TextChar strFileSize[]		= "File Size";
TextChar strFSID[]			= "File System ID";
TextChar strFileData[]		= "File Data";
TextChar strFirstKey[]		= "First Block Key";
TextChar strUnknown[]		= "Unknown";
TextChar strHashSize[]		= "Hash Table Size";
TextChar strCacheKey[]		= "Directory Cache Block Key";
TextChar strSoftLinkObj[]	= "Soft Link Object Name";
TextChar strOwnerXID[]		= "Owner Information";
TextChar strDirCacheInfo[]	= "Directory Cache Information";

TextPtr strRootBlockTypes[] = {
	strBlockType, strReserved, strReserved, strHashSize, strReserved, strChecksum
};

TextPtr strDirAndLinkBlockTypes[] = {
	strBlockType, strOwnKey, strReserved, strReserved, strReserved, strChecksum
};

TextPtr strFileBlockTypes[] = {
	strBlockType, strOwnKey, strBlockCount, strReserved, strFirstKey, strChecksum
};

TextPtr strExtBlockTypes[] = {
	strBlockType, strOwnKey, strBlockCount, strReserved, strReserved, strChecksum
};

TextPtr strCacheBlockTypes[] = {
	strBlockType, strOwnKey, strParentKey, "Number of Entries", "Next Block", strChecksum
};

TextPtr strOFSDataBlockTypes[] = {
	strBlockType, strHeaderKey, strSeqNum, strDataSize, strNextData, strChecksum
};

TextChar strFileHdr[]	= "File Hdr";
TextChar strDrawerType[]= "Drawer";
TextChar strSoftLink[]	= "Soft Link";
TextChar strHardLink[]	= "Hard Link";
TextChar strFileExt[]	= "File Ext";
TextChar strCacheType[]	= "Dir Cache";
TextChar strData[]		= "Data";
TextChar strBoot[]		= "Boot";
TextChar strRoot[]		= "Root";
TextChar strBitMap[]		= "Bitmap";
TextChar strBitMapExt[]	= "Bitmap Ext";

TextChar strClosing[]	= "closing";
TextChar strMoving[]		= "moving";

TextChar	strRepairFound[]	= "detected:";
TextChar strRepairDeleted[]= "deleted: ";

TextChar strOptimizeVerify[]="\nDo you wish the optimization to proceed?";

TextChar strMajor[]			= "Major problems";
TextChar strMinor[]			= "Minor problems";
TextChar strNoVol[]			= "No disk in drive";
TextChar strInvalidVol[]	= "Not an AmigaDOS volume";
TextChar strWPVol[]			= "Valid volume, read-only";
TextChar strValidVol[]		= "Valid AmigaDOS volume";

TextChar strAboutText[]		= "Quarterback Tools by Glen Merriman";
/*TextChar strLoading[]		= "Loading program data...";*/
TextChar strProgName[] 		= "Quarterback Tools";

TextChar strPrefsName[]		= "QBT Defaults";
TextChar strFKeysName[]		= "QBT FKeys";
TextChar strAutoExecName[]	= "QBT Startup";
TextChar strAppIconName[]	= "QBT Deposit";
TextChar strReportName[]	= "QBT Report";

TextChar strDefCapture[]	= "QBT Capture";
TextChar strSaveCapture[]	= "Save capture file as:";

TextChar strLoadDefaults[]	= "Select the settings to use";
TextChar strSaveAsDefaults[]= "Save settings as:";
TextChar strSaveLog[]		= "Save session log as:";
TextChar strSaveCatalog[]	= "Save catalog as:";

TextChar strDOSError[]		= "DOS error: ~";
TextChar strFileNotFound[]	= "File not found.";
TextChar strDiskLocked[]	= "Disk is locked.";
TextChar strFileNoDelete[]	= "File is delete protected.";
TextChar strFileNoRead[]	= "File is read protected.";
TextChar strFileBusy[]		= "File is in use.";

TextChar bstrRecoveredItems[] = "\17Recovered Items";

/*
 *	Some tool buttons
 */

TextChar	strStop[]			= "Stop";
TextChar strSkip[]			= "Skip";
TextChar strSkipFile[]		= "Skip File";
TextChar strIgnore[]			= "Ignore";
TextChar strProceed[]		= "Proceed";
TextChar strRepair[]			= "Repair";
TextChar strPrint[]			= "Print";
TextChar strFind[]			= "Find";
TextChar strSave[]			= "Save";
TextChar strOptions[]		= "Options";

TextChar strQuit[]			= "Quit";

TextChar strToolsLog[]		= "Quarterback Tools Report, ";
TextChar strPage[]			= "Page ~";
TextChar strNewLine[]		= "\n";
TextChar strColon[]			= ":";
TextChar strWindTitleSep[] = ": ";
TextChar strQuote[]			= "\"";
TextChar	strPeriod[]			= ".";	/* Ends error strings on ReportError() */
TextChar strEllipsis[]		= "...";
TextChar strQuestionMark[]	= "?";
/*TextChar strSpace[]			= " ";*/

/*
 * Stat strings
 */

TextChar strOFS[]			= "Old";
TextChar strFFS[]			= "Fast";
TextChar strVersSeparator[]= ".";
TextChar strTextSeparator[]= ", ";
TextChar strInternational[]= "Int'l";
TextChar strDirCache[]		= "DirCache";
TextChar strK[]			= "K";

/*
 *	Gadget Strings
 */

TextChar repairTitle[]		= "Analyze and Repair Volume";
TextChar recoverTitle[]		= "Recover Lost/Deleted Files";
TextChar	optimizeTitle[]	= "Optimize Volume";
TextChar editTitle[]			= "Edit Volume";

/*
 * The icon titles for each tool
 */
 
TextPtr iconTitle[] = {
	repairTitle, recoverTitle, optimizeTitle, editTitle
};

TextChar strVolumes[]		= "Volumes";
TextChar strDevices[]		= "Devices";

TextChar strStart[]			= "Start";
TextChar strPause[]			= "Pause";
TextChar strResume[]			= "Resume";
/*TextChar strDone[]			= "Done";*/
TextChar strAbort[]			= "Abort";

TextChar strBack[]			= "Back";
TextChar strDisks[]			= "Disks";
TextChar strEnter[]			= "Enter";

TextChar strBlockLabel[]	= "Block:";
TextChar	strFileLabel[]		= "File:";
TextChar strDrawerLabel[]	= "Drawer: ";
TextChar strEntryLabel[]	= "Entry:  ";
TextChar strErrorLabel[]	= "Error:  ";
TextChar strActionLabel[]	= "Action: ";

TextChar strBlockOn[]		= " on \"";
TextChar strEndBlockLabel[]= "\"";

TextChar strHex[]			= "Hex";
TextChar strDec[]			= "Dec";


/*
 *	Strings to display in the Repair window's scroll list
 */

TextChar strReadyToAnalyze[]	= "Starting to analyze ";
TextChar strAndRepair[]			= "and repair ";
TextChar strDrive[]				= "disk \"";
TextChar strBlockScan[]			= "Block scan ";
TextChar strFileScan[]			= "File scan ";

TextChar strStarted[]			= "started ";
TextChar strCompleted[]			= "completed ";
TextChar strAborted[]			= "aborted ";
/*TextChar strOn[]				= "on ";*/
TextChar	strAt[]				= " at ";
TextChar strErrorReadBlock[]	= "Error reading block %";
TextChar strErrorWriteBlock[]	= "Error writing block %";
/*TextChar strErrorCmpBlock[]	= "Error verifying block %";*/
TextChar strTotalBadBlocks[]	= "Total bad blocks found: ~";
TextChar strNoBadBlocks[]		= "No bad blocks were found.";
TextChar strNoNewBadBlocks[]	= "No new bad blocks were found.";
TextChar strChkBadBlocks[]		= "Check bad blocks";
TextChar strChkBadFiles[]		= "Check bad files";
TextChar strChkBoth[]			= "Check bad blocks and files";
TextChar strAnalyzeOnly[]		= "Analyze only";
TextChar strRepairToo[]			= "Analyze and repair";

TextChar strFile[]				= "File ";
TextChar strDrawer[]				= "Drawer ";
TextChar strLink[]				= "Link ";
TextChar strEntry[]				= "Entry ";

TextChar strFragments[]			= "Fragments of used space: ";
/*TextChar strNoFragments[]		= "Used space not fragmented";*/
TextChar strCountingFrags[]	= "Checking integrity of volume...";
TextChar strResolvingFiles[]	= "Resolving file conflicts...";
TextChar strBuildingRemap[]	= "Building remap table on volume...";
TextChar strProcessing[]		= "Processing: ";
TextChar strUsed[]				= "-Used";
TextChar strUnused[]				= "-Unused";

/*
 * Some recover strings
 */
 
TextChar strCatalogOf[]			= "Catalog of ";
TextChar strCatalogOfAll[]		= "Catalog of all files on ";
TextChar strCatalogOfDel[]		= "Catalog of deleted files on ";
TextChar strDirOf[]				= "Directory of ";
TextChar strTag[]				= "Tag";
TextChar strTagAll[]				= "Tag All";
TextChar strUntag[]				= "Untag";
TextChar strUntagAll[]			= "Untag All";
TextChar strDir[]				= "Dir";
TextChar strRenameFile[]		= "File \"%\" already exists.";
TextChar strDelCheck[]			= "Calculating file recoverability...";
TextChar	strCopy[]				= "_copy";
TextChar strIgnoreKeys[]		= "These files\nwill be placed together in a drawer on\nthe root called \"Recovered Items\".";
TextChar strEnoughKeys[]		= "Replacement\ndrawers will be created in a drawer on\nthe root called \"Recovered Items\".";
TextChar strNotEnoughKeys[]	= "Due to insufficient\ndisk space, these files will be placed\ninside the drawer \"Recovered Items\", but\nwill not have replacement drawers created.";
TextChar strNoKeys[]				= "Due to the lack of\nany disk space on this volume, all of\nthese files will be placed in the root.";

/*
 *	Macro strings
 */

/*TextChar	strMacroNames1[10][32];*/		/* Not assigned */

#if (GERMAN | SWEDISH)
TextChar	strMacroNames2[10][32] = {
	"Makro_1", "Makro_2", "Makro_3", "Makro_4", "Makro_5",
	"Makro_6", "Makro_7", "Makro_8", "Makro_9", "Makro_10"
};
#endif

TextChar macroDrawerName[]	= "Macros";

/*
 * Strings NOT to be translated!!
 */
 
TextChar strAny[]			= "ANY";
TextChar strPublic[]			= "PUBLIC";
TextChar strChip[]			= "CHIP";
TextChar strFast[]			= "FAST";
TextChar strLocal[]			= "LOCAL";
TextChar str24BitDMA[]		= "24BITDMA";
TextChar strOrSymbol[]		= " | ";

TextChar strPRT[]			= "PRT:";

TextChar A2090DevSuffix[]	= "ddisk.device";
TextChar strCommentBegin[]	= "* ";
TextChar strAREXXPortName[]= "QB_Tools";
TextChar strPortName[]		= "Quarterback Tools Port";

TextChar strSerializedFor[]= "This program is licensed to:";
TextChar strSerialNumber[]	= "Serial no.: ";