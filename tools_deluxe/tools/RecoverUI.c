/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	File Recovery code.
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <dos/dos.h>
#include <graphics/gfxmacros.h>

#include <rexx/errors.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/Border.h>
#include <Toolbox/DateTime.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Image.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	Externals
 */

extern UBYTE			_tbPenLight, _tbPenBlack, _tbPenWhite;
extern BOOL				_tbSmartWindows;

extern WindowPtr		cmdWindow;
extern ScreenPtr		screen;
extern MsgPortPtr		mainMsgPort;
extern OptionsRec		options;
extern UWORD			grayPat[];
extern BOOL				closeFlag, quitFlag, decimalLabel;
extern GadgetPtr		currGadgList;
extern TextPtr			strsErrors[], strPath;
extern TextChar		strBuff[], currFileName[], choiceNameColon[];
extern TextChar		strStart[], strPause[], strStop[], strResume[];
extern TextChar		strProcessing[], strCatalogOf[];
extern TextChar		strTag[], strTagAll[], strUntag[], strUntagAll[];
extern TextChar		strBack[], strRoot[];
extern TextChar		strDir[];/*, strHardLink[], strSoftLink[];*/
extern WORD				action, mode, stage, intuiVersion;
extern ReqTemplPtr		reqList[];
extern RequestPtr		requester;
extern ScrollListPtr		scrollList;
extern VolParams		currVolParams;
extern BOOL				bitMapFlag, abortFlag, inMacro;
extern RecoverStats		recoverStats;
extern LONG				xoffset, maxWidth;
extern ULONG			topListNum;
extern UWORD			charBaseline, charWidth, charHeight;
extern Rectangle		labelRect, statusRect, statusRect2;
extern ImagePtr		iconList[];
extern UWORD			tabbing[], tabChars[], tabTable[];
extern WORD				processingWidth;

/*
 *	Local prototypes
 */

static void		DisposeRecoverInfo(void);
static void		RefreshRecoverPath(void);
static BOOL		HandleLeftButton(void);
static void		HandleCloseBox(void);
static BOOL		InitRecover(void);
static void		SetAltButtons(BOOL);
static void		DoCursorUpDown(UWORD, UWORD);
static void		RecoverDrawProc(RastPtr, TextPtr, RectPtr);
static BOOL		RecoverListFilter(IntuiMsgPtr, WORD *);
static void		AdjustWidth(void);
static ImagePtr ConstructIconImage(WORD);
static void		UpdateProceedButton(void);
static void		UpdateEnterButton(void);
static void 	DoDoubleClick(WORD);
static BOOL		EnterDirectory(DirFibPtr);
static BOOL		DoEnterDirectory(void);
static BOOL		DoParentDirectory(BOOL);
static WORD		NextFileListItem(BOOL, WORD);
static void 	OnOffFileItem(DirFibPtr, WORD, BOOL);
static BOOL		SetResetTag(BOOL, DirFibPtr);
static void		UpdateAllDirStatus(void);
static void		UpdatePartBusyStatus(DirFibPtr);
static void		UpdateDirStatus(DirFibPtr, BOOL);

#define UPDATE_RECOVER_BUTTONS	99
#define MAX_LIST_ITEMS				32767

enum {
	UNDEL_OPTS_RESET_BUTTON = 2,
	SHOW_DEL_BOX,
	SHOW_KEY_BOX,
	CREATE_DIRS_BOX,
};

#define NUM_RECOVER_OPTIONS	(sizeof(recoverOptNames)/sizeof(TextPtr))

static TextPtr recoverOptNames[] = {
	"ShowDeleted",	"ShowAll",
	"ShowKeysOn",	"ShowKeysOff",
	"CreateDrawersOn","CreateDrawersOff",
	"ToSameVolume",	"ToDifferentVolume",
};

enum {
	OPT_SHOWDELON,	OPT_SHOWDELOFF,
	OPT_SHOWKEYSON,	OPT_SHOWKEYSOFF,
	OPT_CREATEDIRSON,	OPT_CREATEDIRSOFF,
	OPT_SAMEVOL,	OPT_DIFFVOL,
};

/*
 *	Icon data
 */

static WORD iconNumPts[] = {
	 5, 8, 4, 5, 5
};

static WORD iconX[] = {
	1, 15, 15, 1, 1,
	9, 9, 12, 12, 4, 4, 9, 12,
	1, 15, 15, 1,
	9, 12, 12, 4, 4,
	0, 1, 2, 6, 2		/* This is the checkmark mask AND image (one plane) */	
};

static WORD iconY[] = {
	4, 4, 8, 8, 4,
	0, 3, 3, 9, 9, 0, 0, 3,
	4, 4, 8, 8,
	0, 3, 9, 9, 0,
	5, 6, 9, 0, 9
};

TextChar customDateFormat[] = "\x09-\x06-\x08";

static ULONG lastSel, lastTotal;

#define SCALE(x, srcWidth, dstWidth)	\
	(((LONG) (x)*(dstWidth - 1) + (srcWidth - 1)/2)/(srcWidth - 1))


/*
 * If recover window opens successfully, perform various initializations to it.
 */

BOOL SetupRecoverMode()
{
	register WindowPtr window = cmdWindow;
	register GadgetPtr gadgList = currGadgList;
	
	BlockClear(&recoverStats, sizeof(RecoverStats));	
	InitPath();	
	
	AddGList(window, gadgList, -1, -1, 0);
	InitScrollList(scrollList, GadgetItem(gadgList, SCROLL_LIST), window, NULL);
	
	xoffset = 0;
	maxWidth = 0;
	topListNum = 0;
	stage = 0;
	
	mode = MODE_RECOVER;
	action = OPER_READY;
	abortFlag = FALSE;
	
	SetAllMenus();
	
	RefreshRecoverWindow(TRUE);
	
	NewRecoverDestination();
	/*
	SetEditItemText(gadgList, UNDEL_DEST_TEXT, window, NULL, options.RecoverOpts.DestName);
	EnableGadgetItem(gadgList, UNDEL_DEST_TEXT, window, NULL, options.RecoverOpts.Destination != 0);
	*/
	
	OffGList(GadgetItem(gadgList, UNDEL_BUTTON_ENTER), window, NULL, 2);
	OffGList(GadgetItem(gadgList, UNDEL_BUTTON_TAG), window, NULL, 2);

	if( inMacro ) {
		(void) HandleLeftButton();
	}
	return(!abortFlag);
}

/* 
 * Free up resources allocated by the recover window.
 */
 
void CleanupRecoverMode()
{
	if( stage ) {
		DisposeRecoverInfo();
	}
	GetEditItemText(currGadgList, UNDEL_DEST_TEXT, options.RecoverOpts.DestName);
	options.RecoverOpts.Destination = GetGadgetValue( GadgetItem(currGadgList, UNDEL_DEST_POPUP) ) != 0;
	RemoveWindow(cmdWindow);
}

/*
 * Deallocate what hitting "Start" might have allocated.
 */
 
static void DisposeRecoverInfo()
{
	FreeMiniIcons();
	FreeBitMapBuffers();
	FreeDirBlocks();
}

/*
 *	Refresh the window's contents
 */

void RefreshRecoverWindow(BOOL adjust)
{
	Rectangle winRect;
	register WindowPtr window = cmdWindow;
	register RastPtr rPort = window->RPort;
	BOOL inval = FALSE;
	Rectangle rect;
	
	SetDrMd(rPort, JAM1);
	GetWindowRect(window, &winRect);
/*
	Clear the background of the window if first time, or pre DOS2.0 and simple refresh.
*/
	if( adjust || (!_tbSmartWindows) ) {
		if( intuiVersion < OSVERSION_2_0 ) {
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &winRect);
			inval = TRUE;
		}
	}
	if( adjust ) {
		UpdateHScrollBar(GadgetItem(currGadgList, LEFT_ARROW));
		inval = TRUE;
	}
	if( inval ) {
		RefreshGadgets(currGadgList, window, NULL);
		InvalRect(window, &winRect);
	}
	BeginRefresh(window);
/*
	If new window size or not a smart refresh window, we have to draw ourselves.
*/
	if( adjust || (!_tbSmartWindows) ) {
		GetGadgetRect(GadgetItem(currGadgList, UNDEL_PATHNAME_BORDER), window, NULL, &labelRect);
		labelRect.MaxX = (window->Width - window->BorderRight) - 1;
		rect.MinX = window->BorderLeft;
		rect.MaxX = labelRect.MaxX;
		/*
		rect.MinY = rect.MaxY = labelRect.MinY - (HEIGHT(&labelRect) + 18);
		DrawStdBorder(rPort, BORDER_SHADOWLINE, &rect);
		*/
		rect.MinY = rect.MaxY = labelRect.MinY - 4;
		DrawStdBorder(rPort, BORDER_SHADOWLINE, &rect);
		GetGadgetRect(GadgetItem(currGadgList, UNDEL_FILES_BORDER), window, NULL, &statusRect);
		GetGadgetRect(GadgetItem(currGadgList, UNDEL_BYTES_BORDER), window, NULL, &statusRect2);
		OutlineOKButton(window);
		SLDrawBorder(scrollList);
		SLDrawList(scrollList);
		lastSel = lastTotal = -1;
		RefreshRecoverStats();
		RefreshRecoverPath();
	}
	EndRefresh(window, TRUE);
}

/*
 * Refresh the labels for the files/bytes tagged.
 */
 
void RefreshRecoverStats()
{
	RastPtr rPort = cmdWindow->RPort;
	TextChar numBuff[16];
	
	if( lastSel != recoverStats.SelFiles ) {
		NumToCommaString(recoverStats.SelFiles, numBuff);
		PrintRightStr(rPort, &statusRect, numBuff);
		lastSel = recoverStats.SelFiles;
	}
	if( lastTotal != recoverStats.TotalFiles ) {
		NumToCommaString(recoverStats.TotalFiles, numBuff);
		PrintRightStr(rPort, &statusRect2, numBuff);
		lastTotal = recoverStats.TotalFiles;
	}
}

/*
 * Update the enable status of the proceed button.
 */
 
static void UpdateProceedButton()
{
	EnableGadgetItem(currGadgList, UNDEL_BUTTON_OK, cmdWindow, NULL, recoverStats.SelFiles + recoverStats.SelDirs);
}

/*
 * Update the enable status of the enter button.
 */

static void UpdateEnterButton()
{
	register WORD item;
	register WORD prevItem;
	register DirFibPtr nextFib;
	register BOOL enterOn = FALSE;
	
	prevItem = item = -1;
	nextFib = (DirFibPtr) &recoverStats.CurFib->df_Child;
	while( ((item = SLNextSelect(scrollList, item)) != -1) && !enterOn ) {
		do {
			nextFib = NextDirItem(nextFib);
		} while( ++prevItem < item );
		
		enterOn = IS_DIR(nextFib->df_Type);
	}
	EnableGadgetItem(currGadgList, UNDEL_BUTTON_ENTER, cmdWindow, NULL, enterOn);
}

/*
 * Perform a refresh of the recover path label (call when need to draw entire label).
 */
 
static void RefreshRecoverPath()
{
	register RastPtr rPort = cmdWindow->RPort;
	
	if( action == OPER_IN_PROG ) {
		strcpy(strBuff, strProcessing);
	} else {
		strcpy(strBuff, strCatalogOf);
	}
	strcat(strBuff, choiceNameColon);
	
	SetAPen(rPort, _tbPenLight);
	SetDrMd(rPort, JAM1);
	Move(rPort, labelRect.MinX, labelRect.MinY + charBaseline);
	FillRect(rPort, &labelRect);
	SetAPen(rPort, _tbPenBlack);
	if( stage ) {
		DoTextInWidth(strBuff, WIDTH(&labelRect), TRUE);
		processingWidth = TextLength(rPort, strBuff, strlen(strBuff));
		DisplayRecoverPath();
	}
}

/*
 * Display the current pathname in the label field (call when pathname changes).
 */
 
void DisplayRecoverPath()
{
	register RastPtr rPort = cmdWindow->RPort;
	Rectangle rect = labelRect;
	TextPtr ptr;
	
	rect.MinX += processingWidth;
	SetAPen(rPort, _tbPenLight);
	SetDrMd(rPort, JAM1);
	FillRect(rPort, &rect);
	SetAPen(rPort, _tbPenBlack);
	Move(rPort, rect.MinX, rect.MinY + charBaseline);
	ptr = &strPath[strlen(choiceNameColon)];
	DoTextInWidth(ptr, WIDTH(&rect), TRUE);
}

/*
 * Given a dirFib entry, return the next one which would appear in the
 * scrollable list (since if only deleted are shown, non-deleted need to be skipped.
 */
 
DirFibPtr NextDirItem(register DirFibPtr dirFib)
{
	if( dirFib != NULL ) {
		if( options.RecoverOpts.ShowDelOnly ) {
			do {
				dirFib = dirFib->df_Next;
			} while( (dirFib != NULL) && (dirFib->df_Flags & (FLAG_NOT_BUSY_MASK | FLAG_PART_BUSY_MASK)) == 0 );
		} else {
			dirFib = dirFib->df_Next;
		}
	}
	return(dirFib);
}

/*
 * Process double-click on file or directory.
 */
 
static void DoDoubleClick(register WORD item)
{
	register DirFibPtr dirFib = (DirFibPtr) &recoverStats.CurFib->df_Child;
	register WORD i = 0;
	
	do {
		dirFib = NextDirItem(dirFib);
	} while( i++ < item );
	
	if( IS_DIR(dirFib->df_Type) ) {
		(void) EnterDirectory(dirFib);
	} else {
		OnOffFileItem(dirFib, item, (dirFib->df_Flags & FLAG_SEL_MASK) == 0);
		UpdateProceedButton();
		RefreshRecoverStats();
	}
}

/*
 * Save the originating dirFib and call BuildFileListDisplay().
 */
 
static BOOL EnterDirectory(DirFibPtr dirFib)
{
	BOOL success = FALSE;
	
	if( dirFib != NULL && (IS_DIR(dirFib->df_Type)) ) {
		AppendPath(&dirFib->df_Name);
		DisplayRecoverPath();
		recoverStats.CurFib = dirFib;
		success = BuildFileListDisplay(NULL);
	}
	return(success);
}

/*
 * Enter the directory via "Enter" gadget hit.
 */
 
static BOOL DoEnterDirectory()
{
	register WORD prevItem;
	register WORD listItem;
	register DirFibPtr dirFib;
	register BOOL success = FALSE;
	
	dirFib = NextDirItem( (DirFibPtr) &recoverStats.CurFib->df_Child );
	listItem = -1;
	prevItem = 0;
	while( (listItem = SLNextSelect(scrollList, listItem)) != -1 ) {
		if( options.RecoverOpts.ShowDelOnly ) {
			while( prevItem != listItem ) {
				dirFib = dirFib->df_Next;
				if( dirFib->df_Flags & (FLAG_NOT_BUSY_MASK | FLAG_PART_BUSY_MASK) ) {
					prevItem++;
				}
			}
		} else {
			while( prevItem++ < listItem ) {
				dirFib = dirFib->df_Next;
			}
		} 
		if( IS_DIR(dirFib->df_Type) ) {
			success = EnterDirectory(dirFib);
			break;
		}
	}
	return(success);
}

/*
 * Go to parent of current directory, via left arrow or "Parent" gadget hit.
 * If "root" TRUE, go all the way to root.
 */
  
static BOOL DoParentDirectory(BOOL root)
{
	DirFibPtr parentDirFib;
	BOOL success;
	
	if( success = recoverStats.CurFib->df_Parent != NULL ) {
		SetWaitPointer();
		UpdateDirStatus(parentDirFib = recoverStats.CurFib, root);
		if( root ) {
			recoverStats.CurFib = recoverStats.DirTree;
			InitPath();
		} else {
			recoverStats.CurFib = recoverStats.CurFib->df_Parent;
			TruncatePath();
		}
		DisplayRecoverPath();
		success = BuildFileListDisplay(root ? NULL : parentDirFib);
	}
	return(success);
}

/*
 * Turn off or on one dir item
 */
 
static void OnOffFileItem(register DirFibPtr dirFib, WORD listItem, BOOL onOff)
{
	register UBYTE *text;
	
	text = GetListItem(scrollList->List, listItem) + 1;
	
	if( onOff ) {
		*text = (*text & ~FLAG_PART_MASK) | FLAG_SEL_MASK;
		dirFib->df_Flags = (dirFib->df_Flags & ~FLAG_PART_MASK);
	} else {
		*text &= ~(FLAG_SEL_MASK | FLAG_PART_MASK);
		dirFib->df_Flags &= ~(FLAG_PART_MASK);
	}
	
	if( onOff != ((dirFib->df_Flags & FLAG_SEL_MASK) != 0) ) {
		SetResetTag(onOff, dirFib);
	}

	SLDrawItem(scrollList, listItem);
}

/*
 * Set or reset the flag field of the dirFib passed if necessary.
 */
 
static BOOL SetResetTag(BOOL include, DirFibPtr dirFib)
{
	register BOOL change = FALSE;
/*	register BOOL isLink = (dirFib->df_Type & (TYPE_SOFT_LINK_MASK | TYPE_HARD_LINK_MASK)) != 0;*/
	register UBYTE *flagPtr = &dirFib->df_Flags;
	
	if( include ) {
		if( (*flagPtr & FLAG_SEL_MASK) == 0 ) {
			if( dirFib->df_Type & TYPE_DIR_MASK ) {
				if( dirFib->df_Child == NULL ) {
					recoverStats.SelDirs++;
				}
			} else {
			/*if( !isLink ) {*/
				recoverStats.SelFiles++;
				if( *flagPtr & FLAG_NOT_BUSY_MASK ) {
					recoverStats.SelDelFiles++;
				} else {
					recoverStats.SelActFiles++;
				}
			/*}*/
			}
			*flagPtr |= FLAG_SEL_MASK;
			change = TRUE;
		}
	} else {
		if( *flagPtr & FLAG_SEL_MASK ) {
			if( dirFib->df_Type & TYPE_DIR_MASK ) {
				if( dirFib->df_Child == NULL ) {
					recoverStats.SelDirs--;
				}
			} else {
			/*if( !isLink ) {*/
				recoverStats.SelFiles--;
				if( *flagPtr & FLAG_NOT_BUSY_MASK ) {
					recoverStats.SelDelFiles--;
				} else {
					recoverStats.SelActFiles--;
				}
			/*}*/
			}
			*flagPtr &= ~FLAG_SEL_MASK;
			change = TRUE;
		}
	}
	return(change);
}

/*
 * Select/Deselect all files/dirs in a directory.
 */

void SelectDir(register DirFibPtr dirFib, register BOOL onOff)
{
	register BOOL isDir;
	DirFibPtr rootFib = dirFib;
	register BOOL cont = TRUE;
	DirFibPtr nextFib;
	DirFibPtr child;
	
	if( (dirFib != NULL) && IS_DIR(dirFib->df_Type) ) {
		if( nextFib = NextDirItem((DirFibPtr) &dirFib->df_Child) ) {
			do {
				if( nextFib == NULL ) {
					dirFib = dirFib->df_Parent;
					cont = dirFib != rootFib;
				} else {
					dirFib = nextFib;
					while( (isDir = IS_DIR(dirFib->df_Type)) && ((child = NextDirItem((DirFibPtr)&dirFib->df_Child)) != NULL) ) {
						if( onOff ) {
							dirFib->df_Flags &= ~FLAG_PART_MASK;
							dirFib->df_Flags |= FLAG_SEL_MASK;
						} else {
							dirFib->df_Flags &= ~(FLAG_PART_MASK | FLAG_SEL_MASK);
						}
						dirFib = child;
					}
					if( onOff != ((dirFib->df_Flags & FLAG_SEL_MASK) != 0) ) {
						SetResetTag(onOff, dirFib);
					}
				}
				nextFib = NextDirItem(dirFib);
			} while( cont );
		}
	}
}

/*
 * Update the partial selection flag status for all directories in the tree.
 */
 
static void UpdateAllDirStatus()
{
	register BOOL cont = TRUE;
	register BOOL isDir;
	DirFibPtr rootFib = recoverStats.DirTree;
	register DirFibPtr dirFib = (DirFibPtr) &rootFib->df_Child;
	
	SetWaitPointer();
	if( dirFib->df_Next != NULL ) {
		do {
			if( dirFib->df_Next == NULL ) {
				dirFib = dirFib->df_Parent;
				UpdatePartBusyStatus(dirFib);
				UpdateDirStatus(dirFib, FALSE);
				cont = dirFib != rootFib;
			} else {
				dirFib = dirFib->df_Next;
				while( (isDir = IS_DIR(dirFib->df_Type)) && (dirFib->df_Child != NULL) ) {
					dirFib = dirFib->df_Child;
				}
				/*
				if( isDir ) {
					dirFib->df_Flags &= ~(FLAG_PART_MASK | FLAG_SEL_MASK);
				}
				*/
			}
		} while( cont );
	}
	if( options.RecoverOpts.ShowDelOnly ) {
		recoverStats.TotalFiles = recoverStats.DelFiles;
		recoverStats.SelFiles = recoverStats.SelDelFiles;
	} else {
		recoverStats.TotalFiles = recoverStats.MaxFiles;
		recoverStats.SelFiles = recoverStats.SelDelFiles + recoverStats.SelActFiles;
	}
	RefreshRecoverStats();
}

/* 
 * Calculate the FLAG_SEL status for the current level.
 * Go all the way to the root if "toRoot" is TRUE.
 */
 
static void UpdateDirStatus(register DirFibPtr dirFib, BOOL toRoot)
{
	register BOOL fullySel;
	register BOOL anySel;
	register DirFibPtr nextFib;
	register UBYTE flags;
	register BOOL cont = TRUE;
	
	if( dirFib != NULL ) {
		while( dirFib->df_Parent != NULL && cont ) {
			nextFib = NextDirItem((DirFibPtr)&dirFib->df_Child);
			fullySel = TRUE;
			flags = dirFib->df_Flags;
			anySel = (nextFib == NULL) && (flags & FLAG_SEL_MASK);
			while( nextFib != NULL ) {
				flags = nextFib->df_Flags;
				if( flags & FLAG_SEL_MASK ) {
					anySel = TRUE;
					if( flags & FLAG_PART_MASK ) {
						fullySel = FALSE;
					}
				} else {
					fullySel = FALSE;
				}
				nextFib = NextDirItem(nextFib);
			}
			nextFib = dirFib;
			dirFib = dirFib->df_Parent;
			if( nextFib != NULL ) {
				if( anySel ) {
					if( !fullySel ) {
						nextFib->df_Flags |= FLAG_PART_MASK;
					} else {
						nextFib->df_Flags &= ~FLAG_PART_MASK;
					}
					nextFib->df_Flags |= FLAG_SEL_MASK;
				} else {
					nextFib->df_Flags &= ~FLAG_SEL_MASK;
				}
				/*
				if( anySel != ((dirFib->df_Flags & FLAG_SEL_MASK) != 0) ) {
					SetResetTag(anySel, nextFib);
				}
				*/
			}
			cont = toRoot;
		}
	}
}

/*
 * Update the PART_BUSY_MASK flag bit, to indicate whether there is a deleted
 * file within this drawer at any point.
 */
 
static void UpdatePartBusyStatus(register DirFibPtr dirFib)
{
	register DirFibPtr nextFib = dirFib->df_Child;
			
	while( nextFib != NULL ) {
		if( nextFib->df_Flags & FLAG_NOT_BUSY_MASK ) {
			do {
				dirFib->df_Flags |= FLAG_PART_BUSY_MASK;
				dirFib = dirFib->df_Parent;
			} while( dirFib != NULL );
			break;
		}
		nextFib = NextDirItem(nextFib);
	}
}

/*
 * Call this whenever a selection needs to be tagged. Inputs are TRUE/FALSE for
 * whether it should be tagged or untagged, and whether all should be that way.
 * It assumes the first item to do is the first selected item in "scrollList".
 */
 
void DoTag(BOOL tagOffOn, BOOL altOn)
{
	register WORD prevItem, listItem;
	register DirFibPtr dirFib;
	
	prevItem = listItem = -1;
	dirFib = (DirFibPtr) &recoverStats.CurFib->df_Child;
	SetWaitPointer();
	while( (listItem = NextFileListItem(altOn, listItem)) != -1 ) {
		if( options.RecoverOpts.ShowDelOnly ) {
			while( prevItem != listItem ) {
				dirFib = dirFib->df_Next;
				if( dirFib->df_Flags & (FLAG_NOT_BUSY_MASK | FLAG_PART_BUSY_MASK) ) {
					prevItem++;
				}
			}
		} else {
			do {
				dirFib = dirFib->df_Next;
			} while( ++prevItem < listItem );
		} 
		OnOffFileItem(dirFib, listItem, tagOffOn);

		if( IS_DIR(dirFib->df_Type) && (NextDirItem((DirFibPtr)&dirFib->df_Child) != NULL) ) {
			SelectDir(dirFib, tagOffOn);
		}
	}
	RefreshRecoverStats();
	UpdateProceedButton();
	SetArrowPointer();
}

/*
 * Return next item selected, or if Alt down, just the next item in the list.
 * If no more items, return -1.
 */
 
static WORD NextFileListItem(BOOL altOn, register WORD listItem)
{
	if( altOn ) {
		listItem = (++listItem == SLNumItems(scrollList)) ? -1 : listItem;
	} else {
		listItem = SLNextSelect(scrollList, listItem);
	}
	return(listItem);
}

/*
 *	Handles what to do with the close nbox in
 *	the Repair window based on current 'action'
 *	mode.
 */

static void HandleCloseBox()
{
	if( action == OPER_IN_PROG ) {
		abortFlag = closeFlag = TRUE;
	} else {
		ReturnToMain();
	}
}

/*
 *	Handles what to do with the left button in
 *	the Repair window based on current 'action'
 *	mode.
 */

static BOOL HandleLeftButton()
{
	BOOL success;
	register WindowPtr window = cmdWindow;
	GadgetPtr gadget;
	
	if( stage == 0 ) {
		success = DoRecoverStart();
	} else {
		switch(action) {
		case OPER_READY:
			SetButtonItem(currGadgList, UNDEL_BUTTON_OK, window, NULL, strStop, CMD_KEY_STOP);
			action = OPER_IN_PROG;
			RefreshRecoverPath();
			SetAllMenus();
			if( success = (recoverStats.CurFib == recoverStats.DirTree) || DoParentDirectory(TRUE) ) {
				OffGList(GadgetItem(currGadgList, UNDEL_BUTTON_ENTER), window, NULL, 2);
				gadget = GadgetItem(currGadgList, UNDEL_BUTTON_OPTIONS);
				OffGList(gadget, window, NULL, 3);
				success = DoRecover();
				action = OPER_READY;
				OnGList(gadget, window, NULL, 1);
				if( closeFlag ) {
					if( !quitFlag ) {
						ChangeToolMode(MODE_TOOLS);
					}
				} else {
					InitRecover();
				}
			}
			break;
		case OPER_IN_PROG:
			abortFlag = TRUE;
			break;
		}
	}
	return(success);
}

/*
 * Start the recovery process. Called from button hit or AREXX script.
 */
 
BOOL DoRecoverStart()
{
	register BOOL success;
	register GadgetPtr gadgList = currGadgList;
	register WindowPtr window = cmdWindow;
	
	SetWaitPointer();
	success = abortFlag = FALSE;
	if( AllocMiniIcons() && InitRecoverLists() ) {
		if( PromptForDisk() ) {
			(void) ReadBitMap();
			if( AllocateAltBitMap() ) {
				(void) IsDiskChanged();
				if( success = BuildDeletedTree() ) {
					stage++;
					SLSetDrawProc(scrollList, RecoverDrawProc);
					success = InitRecover();
					if( !bitMapFlag ) {
						OffGList(GadgetItem(gadgList, UNDEL_DEST_POPUP), window, NULL, 1);
						OnGList(GadgetItem(gadgList, UNDEL_DEST_TEXT), window, NULL, 1);
						SetGadgetValue(GadgetItem(gadgList, UNDEL_DEST_POPUP), window, NULL, options.RecoverOpts.Destination = 1);
						Error(ERR_NO_RECOVER_IN_PLACE);
					}
				}
			} else {
				OutOfMemory();
			}
		}
	} else {
		OutOfMemory();
	}
	if( !success ) {
		DisposeRecoverInfo();
		BlockClear(&recoverStats, sizeof(RecoverStats));
		RefreshRecoverStats();
	}
	SetArrowPointer();
	return(success);
}

/*
 * Initialize recover
 */
 
static BOOL InitRecover()
{	
	SetAllMenus();
	InitPath();
	RefreshRecoverPath();
	UpdateAllDirStatus();
	AdjustWidth();
	SetButtonItem(currGadgList, UNDEL_BUTTON_OK, cmdWindow, NULL, strStart, CMD_KEY_START);
	UpdateProceedButton();
	return( BuildFileListDisplay(NULL) );
}

/*
 *	Allow user to adjust repair options
 */

BOOL DoRecoverOptions()
{
	register WindowPtr window;
	register RequestPtr req;
	BOOL success = FALSE;
	RecoverOptsRec recoverOpts;
	register WORD item;
	BOOL diffSize;
	
	if( (action == OPER_READY) || (action == OPER_DONE) ) {
		req = DoGetRequest(REQ_RECOVEROPTS);
		if( req == NULL ) {
			Error(ERR_NO_MEM);
		} else {
			requester = req;
			window = cmdWindow;
			recoverOpts = options.RecoverOpts;
			OutlineOKButton(window);
			item = UPDATE_RECOVER_BUTTONS;
			do {
				switch( item ) {
				case UNDEL_OPTS_RESET_BUTTON:
					SetDefaultRecoverOptions(&recoverOpts);
				case UPDATE_RECOVER_BUTTONS:
					diffSize = recoverStats.NumAltKeys >= recoverStats.LostDirs;
					EnableGadgetItem(req->ReqGadget, CREATE_DIRS_BOX, window, req, diffSize);
					GadgetOnOff(window, SHOW_DEL_BOX, recoverOpts.ShowDelOnly);
					GadgetOnOff(window, SHOW_KEY_BOX, recoverOpts.ShowKeys);
					GadgetOnOff(window, CREATE_DIRS_BOX, diffSize && recoverOpts.CreateDrawers);
					break;
				case SHOW_DEL_BOX:
					ToggleCheckbox(&recoverOpts.ShowDelOnly, item, window);
					break;
				case SHOW_KEY_BOX:
					ToggleCheckbox(&recoverOpts.ShowKeys, item, window);
					break;
				case CREATE_DIRS_BOX:
					ToggleCheckbox(&recoverOpts.CreateDrawers, item, window);
					break;
				}
				item = ModalRequest(mainMsgPort, window, DialogFilter);
			} while( (item != OK_BUTTON) && (item != CANCEL_BUTTON) );
			DestroyRequest(req);
			if( success = item != CANCEL_BUTTON ) {
/*
	Set new values
*/
				NewRecoverOptions(&options.RecoverOpts, &recoverOpts);
			}
		}
	}
	return(success);
}

/*
 * Given that the destination may have changed, refresh the popup and edit text
 * settings for the recover destination.
 */
 
void NewRecoverDestination()
{
	register GadgetPtr gadgList = currGadgList;
	register WindowPtr window = cmdWindow;

	EnableGadgetItem(gadgList, UNDEL_DEST_TEXT, window, NULL, options.RecoverOpts.Destination != 0);
	SetEditItemText(gadgList, UNDEL_DEST_TEXT, window, NULL, options.RecoverOpts.DestName);
	SetGadgetValue(GadgetItem(gadgList, UNDEL_DEST_POPUP), window, NULL, options.RecoverOpts.Destination);
}

/*
 * Given a previos set of recover options, compare with the current setting, and
 * adjust the window display accordingly.
 */
 
void NewRecoverOptions(register RecoverOptsPtr oldRecoverOpts, register RecoverOptsPtr newRecoverOpts)
{
	register BOOL diffSize = newRecoverOpts->ShowDelOnly != oldRecoverOpts->ShowDelOnly;
	register BOOL diffStatus = newRecoverOpts->CreateDrawers != oldRecoverOpts->CreateDrawers;
	register BOOL anyChange = diffSize || diffStatus || (newRecoverOpts->ShowKeys != oldRecoverOpts->ShowKeys);
		
	options.RecoverOpts = *newRecoverOpts;
	if( anyChange && stage ) {
		if( diffSize ) {
			UpdateAllDirStatus();
			RefreshRecoverStats();
		}
		if( diffStatus ) {
			recoverStats.CurFib = recoverStats.DirTree;
			InitPath();
			RedoDummyTree();
		}
		AdjustWidth();
		BuildFileListDisplay(NULL);
	}
}
				
/*
 * Build scrolling list of files - if dirFib is non-NULL, have it pre-selected.
 */
 
BOOL BuildFileListDisplay(DirFibPtr parentDirFib)
{
	register WORD i = 0;
	TextChar text[150];
	register DirFibPtr dirFib;
	register WindowPtr window = cmdWindow;
	WORD autoScrollItem = 0;
	
	SetWaitPointer();
	EnableGadgetItem(currGadgList, UNDEL_BUTTON_BACK, window, NULL, recoverStats.CurFib->df_Parent != NULL);
	EnableGadgetItem(currGadgList, UNDEL_BUTTON_TAG, window, NULL, parentDirFib != NULL);
	EnableGadgetItem(currGadgList, UNDEL_BUTTON_UNTAG, window, NULL, parentDirFib != NULL);

	SLRemoveAll(scrollList);				/* First remove previous entries */
	SLDoDraw(scrollList, FALSE);			/* Turn drawing off while we add */
	SLHorizScroll(scrollList, -xoffset);/* Return List to origin */
	xoffset = 0;

	dirFib = NextDirItem((DirFibPtr)&recoverStats.CurFib->df_Child);
	while( ++i < MAX_LIST_ITEMS && dirFib != NULL ) {
		BuildListEntry(dirFib, &text[2]);
		text[0] = dirFib->df_Flags;
		text[1] = dirFib->df_Type;
		SLAddItem(scrollList, text, (WORD) strlen(text), i);
		if( dirFib == parentDirFib ) {
			autoScrollItem = i-1;
			SLSelectItem(scrollList, autoScrollItem, TRUE);
		}
		dirFib = NextDirItem(dirFib);
	}
	/*
	if( parentDirFib == NULL ) {
		SLSelectItem(scrollList, 0, TRUE);
	}
	*/
	UpdateEnterButton();
	SLDoDraw(scrollList, TRUE);			/* Turn back on so we can see! */
	SLAutoScroll(scrollList, autoScrollItem);
	RecalcMaxWidth();
	UpdateHScrollBar(GadgetItem(currGadgList, LEFT_ARROW));
	SetArrowPointer();
	return(TRUE);
}
 
/*
 * Adjust the width of the horizontal dimensions in pixels
 */
 
static void AdjustWidth()
{
	register WORD i;
	struct DateStamp dateStamp;
	register RastPtr rPort = cmdWindow->RPort;
	
	dateStamp.ds_Days = 10;
	dateStamp.ds_Minute = 1200;
	dateStamp.ds_Tick = 0;
	strcpy(strBuff, customDateFormat);
	DateString( &dateStamp, DATE_CUSTOM, strBuff);

	tabbing[1] = TextLength(rPort, "100% ", 5);
	tabbing[3] = TextLength(rPort, strBuff, strlen(strBuff)) + charWidth;

	TimeString( &dateStamp, FALSE, TRUE, strBuff);
	tabbing[4] = TextLength(rPort, strBuff, strlen(strBuff)) + charWidth;
	tabbing[5] = options.RecoverOpts.ShowKeys ? tabChars[4] * charWidth : 0;
	for( i = 0 ; i <= 5 ; i++ ) {
		tabTable[i] = tabbing[i];
	}
}

/*
 * Assemble a line of text for adding to a list item
 */

void BuildListEntry(DirFibPtr dirFib, register TextPtr destPtr)
{
	register WORD len;
	TextChar buff[20];
	register TextPtr bufPtr = &buff[0];
	register WORD hour;
	struct DateStamp dateStamp;
	
	len = stccpy(destPtr, &dirFib->df_Name, tabChars[0]+1);
	destPtr[len-1] = TAB;

	if( (dirFib->df_Flags & FLAG_NOT_BUSY_MASK) && ((dirFib->df_Type & TYPE_DIR_MASK) == 0) ) {
		if( ((BYTE)dirFib->df_Percent) < 0 ) {
			destPtr[len++] = '?';
		} else {
			NumToString(dirFib->df_Percent, &destPtr[len]);
			len += strlen(&destPtr[len]);
			destPtr[len++] = '%';
		}
		destPtr[len++] = ' ';
	}
	destPtr[len++] = BS;

	if( dirFib->df_Type & (/*TYPE_HARD_LINK_MASK | TYPE_SOFT_LINK_MASK |*/ TYPE_DIR_MASK) ) {
		/*
		if( dirFib->df_Type & TYPE_HARD_LINK_MASK ) {
			bufPtr = strHardLink;
		} else if( dirFib->df_Type & TYPE_SOFT_LINK_MASK ) {
			bufPtr = strSoftLink;
		} else {
		*/
			bufPtr = strDir;
		/*}*/
		len += stccpy(destPtr + len, bufPtr, tabChars[2]+1);
		bufPtr = &buff[0];
	} else {
		NumToString(dirFib->df_Size, bufPtr);
		len += stccpy(destPtr + len, bufPtr, tabChars[2]+1);
	}
	destPtr[len-1] = ' ';		/* Append space to end, for neatness */
	destPtr[len++] = BS;
	
	dateStamp.ds_Days = dirFib->df_Date;
	dateStamp.ds_Minute = (dirFib->df_Time) >> 5;
	dateStamp.ds_Tick = 0L;
	
	strcpy(bufPtr, customDateFormat);
	DateString( &dateStamp, DATE_CUSTOM, bufPtr);
	len += stccpy(destPtr + len, bufPtr, tabChars[3]+1);
	
	destPtr[len-1] = TAB;

	TimeString(&dateStamp, FALSE, TRUE, &buff[1]);
	hour = dateStamp.ds_Minute / 60;
	if( hour && ((hour < 10) || ((hour > 12) && (hour < 22))) ) {
		*bufPtr = ' ';
	} else {
		bufPtr++;
	}
	len += stccpy(destPtr + len, bufPtr, tabChars[4]+1);
	
	destPtr[len-1] = TAB;
	
	if( options.RecoverOpts.ShowKeys ) {
		bufPtr = &buff[0];
		*bufPtr = '\0';
		if( (dirFib->df_Ownkey != 0) &&
			((!options.RecoverOpts.CreateDrawers) || (dirFib->df_Parent != recoverStats.RecoverEntry)) ) {
			OutputEditHexOrDecString( dirFib->df_Ownkey, bufPtr, decimalLabel);
		}
		len += stccpy(destPtr + len, bufPtr, tabChars[5]+1);
	}
	destPtr[len-1] = BS;
	destPtr[len] = 0;
}

/*
 * Attempt to move to file-selection mode from a different mode.
 */
 
BOOL AllocMiniIcons()
{
	BOOL success = TRUE;
	register WORD i;

	for( i = 0 ; i < NUM_MINI_ICONS ; i++ ) {
		iconList[i] = ConstructIconImage(i);
		if( iconList[i] == NULL ) {
			success = FALSE;
			FreeMiniIcons();
			OutOfMemory();
			break;
		}
	}
	return(success);
}

/*
 * Exit selection mode. Frees allocated stuff above.
 */
 
void FreeMiniIcons()
{
	register WORD i;
	
	for( i = 0 ; i < NUM_MINI_ICONS ; i++ ) {
		if( iconList[i] != NULL ) {
			FreeImage(iconList[i]);
			iconList[i] = NULL;
		}
	}
}

/*
 *	Create mini-icon image.
 */

static ImagePtr ConstructIconImage(WORD origID)
{
	register WORD i, numPts, width, height;
	ImagePtr image;
	RastPort tempRastPort;
	register RastPtr rPort = &tempRastPort;
	BitMap bitMap;
	PLANEPTR imageData;
	Point ptList[16];
	Point maskPtList[16];	
	BYTE old_cp_y;
	WORD depth, offset;
	WORD iconOffsets[NUM_MINI_ICONS];
	WORD id;
	BOOL gray;

/*
	Calcualate the icon offsets on the fly, even though they are constants...
	...for maintainability's sake.
*/
	id = origID;
	if( id == 0 ) {
		numPts = 0;
		for( i = 0 ; i < NUM_MINI_ICONS ; i++ ) {
			iconOffsets[i] = numPts;
			numPts += iconNumPts[i];
		}
	}
	gray = (id == DRAWER_ICON_GRAY) || (id == DOC_ICON_GRAY);

	if( id == DRAWER_ICON_GRAY ) {
		id = DRAWER_ICON;
	} else if( id == DOC_ICON_GRAY ) {
		id = DOC_ICON;
	}
	
	height = charHeight;
	width = (((height * 3) >> 1));		/* Make width 1.5 times height */
	if( height == 8 ) { 
		width += 3;
	}
/*
	Allocate image data
*/
	depth = screen->RastPort.BitMap->Depth;
	if( id >= DRAWER_ICON_MASK ) {
		depth = 1;							/* Hey, this is just a mask, dude. */
	}
	if( (screen->ViewPort.Modes & LACE) == 0 ) {
		width += (width >> 1);				/* Increase width if non-interlaced */
	}
	if ((image = NewImage(width, height, depth)) == NULL)
		return (NULL);
/*
	Initialize bitMap and rPort for drawing
*/
	InitBitMap(&bitMap, depth, width, height);
	InitRastPort(rPort);
	rPort->BitMap = &bitMap;
	imageData = (PLANEPTR) image->ImageData;
	for (i = 0; i < depth; i++)
		bitMap.Planes[i] = imageData + i * RASSIZE(width, height);
/*
	Draw into rPort
*/
	SetRast(rPort, 0);						/* Needed for masks only */

	numPts = iconNumPts[id];
	offset = iconOffsets[id];
	
	for( i = 0; i < numPts; i++ ) {
		ptList[i].x = SCALE(iconX[i+offset], 16, width);
		ptList[i].y = SCALE(iconY[i+offset], 10, height);
	}
	if( id < DRAWER_ICON_MASK ) {
		offset = iconOffsets[id+DRAWER_ICON_MASK];
		for( i = 0 ; i < iconNumPts[id+DRAWER_ICON_MASK] ; i++ ) {
			maskPtList[i].x = SCALE(iconX[i+offset], 16, width);
			maskPtList[i].y = SCALE(iconY[i+offset], 10, height);
		}
/*
	Set fill color to white, unless it's the "empty folder" icon and not monochrome.
*/
		SetAPen(rPort, _tbPenWhite);
		if( (depth > 1) && gray ) {
			SetAPen(rPort, _tbPenLight);
		}
		FillPoly(rPort, iconNumPts[id+DRAWER_ICON_MASK], maskPtList);
	}
	SetAPen(rPort, _tbPenBlack);
	if( id == DRAWER_ICON ) {		/* Drawer requires an additional set of lines */
		Move(rPort, SCALE(5, 16, width), SCALE(5, 10, height));
		old_cp_y = rPort->cp_y;
		Draw(rPort, rPort->cp_x, SCALE(6, 10, height));
		Draw(rPort, SCALE(11, 16, width), rPort->cp_y);
		Draw(rPort, rPort->cp_x, old_cp_y);
	}
	if( gray && (depth == 1) ) {
		SetDrMd(rPort, JAM1);
		SetAfPt(rPort, &grayPat[0], 1);
		RectFill(rPort, 0, 0, width - 1, height - 1);
		SetAfPt(rPort, NULL, 0);
	}
	if( id < DRAWER_ICON_MASK ) {
		FramePoly(rPort, numPts, ptList);
	} else {
		SetAPen(rPort, 1);
		FillPoly(rPort, numPts, ptList);
	}
	return(image);
}


/*
 * Drawing procedure
 */

static void RecoverDrawProc(register RastPtr rPort, TextPtr text, RectPtr rect)
{
	register TextPtr str;
	register WORD i;
	register WORD len;
	register WORD width;
	register WORD oldX;
	BYTE flags;
	WORD oldY, textLen, height;
	register ImagePtr icon;
	APTR mask;
	BitMap bitMap;
	WORD depth;
	BYTE minterm = 0xE0;										/* Cookie-cut copy */
	WORD begin;
	BOOL newStyle = FALSE;
	
	begin = (iconList[CHECK_ICON_MASK]->Width >> 2) + 2;

	oldX = rPort->cp_x;
	oldY = rPort->cp_y;
	flags = *text++;
	
	width = iconList[DRAWER_ICON]->Width;				/* Nothing wider */
	
	if( *text & TYPE_DIR_MASK ) {
		if( flags & FLAG_NOT_BUSY_MASK ) {
			icon = iconList[DRAWER_ICON_GRAY];			/* Gray drawer icon */
		} else {
			icon = iconList[DRAWER_ICON];					/* Drawer icon */
		}
		mask = iconList[DRAWER_ICON_MASK]->ImageData;	/* Drawer mask */
	} else if( *text & TYPE_FILE_HDR_MASK ) {
		if( flags & FLAG_NOT_BUSY_MASK ) {
			icon = iconList[DOC_ICON_GRAY];
		} else {
			icon = iconList[DOC_ICON];
		}
		mask = iconList[DOC_ICON_MASK]->ImageData;
	}
	height = icon->Height;
	depth = screen->RastPort.BitMap->Depth;
	InitBitMap(&bitMap, depth, width, height);
	for (i = 0; i < depth; i++) {
		bitMap.Planes[i] = (PLANEPTR) icon->ImageData + i*RASSIZE(width, height);
	}
	if( flags & FLAG_SEL_MASK ) {
		if( flags & FLAG_PART_MASK ) {
			Move(rPort, (oldX + 2) - scrollList->XOffset, rect->MinY + (height >> 1));
			Draw(rPort, rPort->cp_x + (begin - 2), rPort->cp_y);
			Move(rPort, rPort->cp_x, rPort->cp_y + 1);
			Draw(rPort, (oldX + 2) - scrollList->XOffset, rPort->cp_y);
		} else {
			BltTemplate((PLANEPTR) iconList[CHECK_ICON_MASK]->ImageData, 0, (((width-1) >> 4)+1) << 1, rPort, (rect->MinX + 2) - scrollList->XOffset, rect->MinY, width, height);
		}
	}
	BltMaskBitMapRastPort(&bitMap, 0, 0, rPort, rect->MinX + begin + 3 - scrollList->XOffset,
		rect->MinY + 0, width, height, minterm, mask);
	
	begin += (width + 5);
	
/*
	Now draw the text portion of the line
*/							
	Move(rPort, oldX + begin, oldY);
	if( flags & FLAG_NOT_BUSY_MASK ) {
		SetSoftStyle(rPort, FSF_ITALIC, 0xFF);
		newStyle = TRUE;
	}
	text++;
	
	for( i = 0 ; *text ; i++ ) {					/* Tabbed text drawing here */
		for( str = text; (*str != TAB) && (*str != BS); str++ );
		len = str - text;
		oldX = rPort->cp_x;
		if( i ) {
			textLen = TextLength(rPort, text, len);
			if( *str == BS ) {
				Move(rPort, oldX + (tabTable[i] - textLen), rPort->cp_y);
			}
			Text(rPort, text, len);
		} else {
			textLen = tabbing[0];
			TextInWidth(rPort, text, len, textLen, FALSE);
			if( newStyle ) {
				SetSoftStyle(rPort, FS_NORMAL, 0xFF);
				newStyle = FALSE;
			}
		}
		Move(rPort, oldX + tabTable[i], rPort->cp_y);
		text = ++str;
	}

	if( newStyle ) {
		SetSoftStyle(rPort, FS_NORMAL, 0xFF);
	}
}

/*
 *	Handle scrollList's functions
 */

static BOOL RecoverListFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	ULONG class = intuiMsg->Class;
	register WORD gadgNum;
	register WORD code;
	register WindowPtr window = cmdWindow;
	register WORD modifier = intuiMsg->Qualifier;
	BOOL onOff;
	
	if (intuiMsg->IDCMPWindow == window) {
		switch (class) {
		case INTUITICKS:
			SetAltButtons( GetAltKeyStatus(modifier) );
			if( stage && IsDiskChanged() ) {
				HandleCloseBox();
			}
			break;
/*
	Handle gadget up/down in scroll list gadgets
		and ALT key down for "Back" button
*/
		case GADGETDOWN:
		case GADGETUP:
			switch( gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress) ) {
			case VERT_SCROLL:
			case UP_ARROW:
			case DOWN_ARROW:
			case SCROLL_LIST:
				if( (gadgNum != SCROLL_LIST) || (action != OPER_IN_PROG) ) {
					if( (gadgNum == SCROLL_LIST) && (SLNextSelect(scrollList, -1) == -1) && SLNumItems(scrollList) ) {
						EnableGadgetItem(currGadgList, UNDEL_BUTTON_TAG, window, NULL, TRUE);
						EnableGadgetItem(currGadgList, UNDEL_BUTTON_UNTAG, window, NULL, TRUE);
					}
					SLGadgetMessage(scrollList, mainMsgPort, intuiMsg);
					*item = gadgNum;
					if( gadgNum == SCROLL_LIST ) {
						if( SLIsDoubleClick(scrollList) && ((modifier & SHIFTKEYS) == 0) ) {
							DoDoubleClick(SLDoubleClickItem(scrollList));
						}
						UpdateEnterButton();
						onOff = SLNextSelect(scrollList, -1) != -1;
						EnableGadgetItem(currGadgList, UNDEL_BUTTON_TAG, window, NULL, onOff);
						EnableGadgetItem(currGadgList, UNDEL_BUTTON_UNTAG, window, NULL, onOff);
					}
					return(TRUE);
				}
			case HORIZ_SCROLL:
			case LEFT_ARROW:
			case RIGHT_ARROW:
				HandleHorizScroll(mainMsgPort, intuiMsg, GadgetItem(currGadgList, LEFT_ARROW));
				*item = gadgNum;
				return(TRUE);
			case UNDEL_DEST_TEXT:
				ReplyMsg((MsgPtr) intuiMsg);
				*item = gadgNum;
				return (TRUE);
			}
			break;
/*
	Handle cursor keys
*/
		case RAWKEY:
			code = intuiMsg->Code;
			ReplyMsg((MsgPtr)intuiMsg);
			switch( code ) {
			case CURSORUP:
			case CURSORDOWN:
				if( action != OPER_IN_PROG ) {
					DoCursorUpDown(code, modifier);
					UpdateEnterButton();
				}
				*item = SCROLL_LIST;
				break;
			case CURSORLEFT:
			case CURSORRIGHT:
				if( action != OPER_IN_PROG ) {
					if( code == CURSORLEFT ) {
						DepressGadget(GadgetItem(currGadgList, UNDEL_BUTTON_BACK), window, NULL);
						DoParentDirectory(modifier & ALTKEYS);
					} else {
						DepressGadget(GadgetItem(currGadgList, UNDEL_BUTTON_ENTER), window, NULL);
						DoEnterDirectory();
					}
				}
				break;
			default:
				DoRawKey(window, code, modifier);
				break;
			}
			return(TRUE);
		}
	}
	return (FALSE);
}

/*
 * Do cursor up/down for the QB-style recovery tool.
 */
 
static void DoCursorUpDown(UWORD code, UWORD modifier)
{
	register ScrollListPtr scrollListReg = scrollList;
	register WORD items;
	register WORD listItem;
	register WORD offItem;
	register WORD direction;
	BOOL select;
	BOOL bumpSel;
	
	listItem = SLNextSelect(scrollList, -1);
	items = SLNumItems(scrollList);
	bumpSel = SLNextSelect(scrollList, listItem) == -1;

	if( items ) {
		select = TRUE;
		direction = 1;
		if( listItem == -1 ) {
			OnGList(GadgetItem(currGadgList, UNDEL_BUTTON_TAG), cmdWindow, NULL, 2);
		} else {
			if( code == CURSORUP ) {
				direction = -1;
				select = listItem != 0;
			} else {
				listItem--;
				do {
					offItem = SLNextSelect(scrollListReg, ++listItem);
				} while( (offItem - listItem) == 1 );
				select = (listItem+1) != items;
			}
			if( modifier & SHIFTKEYS ) {
				if( !select ) {
					ErrBeep();
				}
				select = TRUE;
				bumpSel = TRUE;
			} else if( select || !bumpSel ) {
				while( (offItem = SLNextSelect(scrollListReg, -1)) != -1 ) {
					SLSelectItem(scrollListReg, offItem, FALSE);
				}
				if( !select ) {
					select = TRUE;
					direction = 0;
				}
			}
		}
		if( select ) {
			if( bumpSel ) {
				listItem += direction;
			}
			SLSelectItem(scrollListReg, listItem, TRUE);
			SLAutoScroll(scrollListReg, listItem);
		} else {
			ErrBeep();
		}
	}
}

/*
 * Set the button text of those buttons which can change when "ALT" is depressed.
 */

static void SetAltButtons(BOOL altOn)
{
	BOOL sel;
	static BOOL lastAltOn;
	register WindowPtr window = cmdWindow;
	register GadgetPtr gadgList = currGadgList;
	
	if( (lastAltOn != altOn) && stage ) {
		if( altOn ) {
#if !GERMAN
			SetButtonItem(gadgList, UNDEL_BUTTON_TAG, window, NULL, strTagAll, 0);
			SetButtonItem(gadgList, UNDEL_BUTTON_UNTAG, window, NULL, strUntagAll, 0);
			SetButtonItem(gadgList, UNDEL_BUTTON_BACK, window, NULL, strRoot, 0);
#endif
			OnOffGList(GadgetItem(gadgList, UNDEL_BUTTON_TAG), window, NULL, 2, NextDirItem((DirFibPtr)&recoverStats.CurFib->df_Child) != NULL);
		} else {
			SetButtonItem(gadgList, UNDEL_BUTTON_TAG, window, NULL, strTag, CMD_KEY_TAG);
			SetButtonItem(gadgList, UNDEL_BUTTON_UNTAG, window, NULL, strUntag, CMD_KEY_UNTAG);
			SetButtonItem(gadgList, UNDEL_BUTTON_BACK, window, NULL, strBack, CMD_KEY_BACK);

			sel = SLNextSelect(scrollList, -1) != -1;
			OnOffGList(GadgetItem(gadgList, UNDEL_BUTTON_TAG), window, NULL, 2, sel);
		}
		lastAltOn = altOn;
	}	
}

/*
 *	Handle repair window button hits
 */

BOOL DoRecoverDialogMsg(register IntuiMsgPtr intuiMsg)
{
	WORD item;
	DialogPtr dlg;
	register BOOL altOn;
	
	if (RecoverListFilter(intuiMsg, &item)) {
		return TRUE;
	}
	if (DialogSelect(intuiMsg, &dlg, &item)) {
		altOn = (intuiMsg->Qualifier & ALTKEYS) != 0;

		ReplyMsg((MsgPtr)intuiMsg);
		switch(item) {
		case DLG_CLOSE_BOX:
			HandleCloseBox();
			break;
		case UNDEL_DEST_POPUP:
			EnableGadgetItem(currGadgList, UNDEL_DEST_TEXT, cmdWindow, NULL, options.RecoverOpts.Destination = (GetGadgetValue( GadgetItem(currGadgList, UNDEL_DEST_POPUP) ) != 0) );
			break;
		case UNDEL_BUTTON_OK:
			(void) HandleLeftButton();
			break;
		case UNDEL_BUTTON_ENTER:
			DoEnterDirectory();
			break;
		case UNDEL_BUTTON_BACK:
			DoParentDirectory(altOn);
			break;
		case UNDEL_BUTTON_TAG:
		case UNDEL_BUTTON_UNTAG:
			DoTag(item == UNDEL_BUTTON_TAG, altOn);
			break;
		case UNDEL_BUTTON_OPTIONS:
			(void) DoRecoverOptions();
			break;
		}
		return TRUE;
	}
	ReplyMsg((MsgPtr)intuiMsg);
	return FALSE;
}

/*
 *	Set recover options of given option name
 *	Used for AREXX macros
 *	Return FALSE if not a valid option name
 */

LONG SetRecoverOption(TextPtr optName)
{
	register WORD i;
	TextChar buff[256];
	UWORD len;
	LONG result = RC_OK;
	register BOOL maybeNamed = FALSE;
	BOOL refreshDest = FALSE;
	
	do {	
		GetNextWord(optName, buff, &len);
		optName += len;
		for (i = 0; i < NUM_RECOVER_OPTIONS; i++) {
			if (CmpString(buff, recoverOptNames[i], strlen(buff), (WORD) strlen(recoverOptNames[i]), FALSE) == 0) {
				break;
			}
		}
		if( i >= NUM_RECOVER_OPTIONS ) {
			if( maybeNamed ) {
				buff[99] = '\0';			/* Just in case string exceeds 100 chars */
				strcpy(options.RecoverOpts.DestName, buff);
			} else {
				result = RC_WARN;
			}
		} else {
			maybeNamed = FALSE;
			switch (i) {
			case OPT_SHOWDELON:
			case OPT_SHOWDELOFF:
				options.RecoverOpts.ShowDelOnly = i == OPT_SHOWDELON;
				break;
			case OPT_SHOWKEYSON:
			case OPT_SHOWKEYSOFF:
				options.RecoverOpts.ShowKeys = i == OPT_SHOWKEYSON;
				break;
			case OPT_CREATEDIRSON:
			case OPT_CREATEDIRSOFF:
				options.RecoverOpts.CreateDrawers = i == OPT_CREATEDIRSON;
				break;
			case OPT_SAMEVOL:
			case OPT_DIFFVOL:
				refreshDest = TRUE;
				if( options.RecoverOpts.Destination = i == OPT_DIFFVOL ) {
					maybeNamed = TRUE;
				}
				break;
			}
		}
	} while(*optName);
	if( refreshDest ) {
		NewRecoverDestination();
	}
	return(result);
}
