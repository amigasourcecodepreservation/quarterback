/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Screen Title and version string
 */

#include <exec/types.h>
#include <TypeDefs.h>

#include <Toolbox/Language.h>

TextChar version[]		= "$VER: Quarterback_Tools 2.0";

#ifdef DEMO
TextChar screenTitle[]	= " Quarterback Tools DEMO 2.0.2 - \251 1993 Central Coast Software";
#else
#if (AMERICAN | BRITISH)
TextChar screenTitle[]	= " Quarterback Tools 2.0.2 - \251 1993 Central Coast Software";
#elif GERMAN
TextChar screenTitle[]	= " Quarterback Tools DE 2.0.2 - \251 1993 Central Coast Software";
#elif FRENCH
TextChar screenTitle[]	= " Quarterback Tools FR 2.0.2 - \251 1993 Central Coast Software";
#elif SWEDISH
TextChar screenTitle[]	= " Quarterback Tools SV 2.0.2 - \251 1993 Central Coast Software";
#endif
#endif

