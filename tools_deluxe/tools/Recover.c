/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	File Recovery code.
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <dos/dos.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>
#include <proto/icon.h>

#include <Toolbox/BarGraph.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Packet.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	Externals
 */

extern UBYTE			_tbPenLight, _tbPenBlack;
extern BOOL				_tbSmartWindows;
extern TextChar		_strOK[], _strYes[], _strNo[];

extern struct DosLibrary *DOSBase;
extern WindowPtr		cmdWindow;
extern ScreenPtr		screen;
extern MsgPortPtr		mainMsgPort;
extern OptionsRec		options;
extern GadgetPtr		currGadgList;
extern LONG				dialogVarNum;
extern TextPtr			bigStrsErrors[], strPath, strSavePath, dialogVarStr;
extern TextChar		strBuff[], choiceNameColon[], bstrRecoveredItems[];
extern TextChar		strCopy[], strDrawerType[], strRenameFile[], strDelCheck[];
extern TextChar		strIgnoreKeys[], strEnoughKeys[], strNotEnoughKeys[], strNoKeys[];
extern TextChar		strStop[];
extern WORD				action, mode;
extern ReqTemplPtr		reqList[];
extern RequestPtr		requester;
extern ScrollListPtr		scrollList;
extern VolParams		currVolParams;
extern BOOL				abortFlag, cancelFlag, skipFlag;
extern BOOL				bitMapFlag, bitMapDirtyFlag;
extern RecoverStats		recoverStats;
extern ULONG			curDBCount, extentBufferSize;
extern UBYTE			*curDBBase, *curDBPtr, *baseDirFib, *blockBuffer, *extentBuffer;
extern BYTE				cacheErr;
extern BarGraphPtr		barGraph;
extern ProcessPtr		process;
extern Ptr				fib;


#define RECOVER_CANCEL	1
#define RECOVER_BAR		2
#define RECOVER_TEXT		3

/*
 * Replace file on restore requester
 */
 
enum {
	RENAME_BUTTON,
	REPLACE_BUTTON = 3,
	RENAME_EDIT_TEXT,
	RENAME_STAT_TEXT
};

#define REQ_BUILD_TEXT	3


/*
 *	Local prototypes
 */

static void			DetermineFileIntegrities(BOOL);
static void			AddControlBlock(ULONG, FileHdrPtr);
static BOOL			AddUserDir(ULONG, DirBlockPtr);
static BOOL			AddUserFile(ULONG, FileHdrPtr);
static void			UpdateCommentAndProt(void *);
static void			MarkAllHashKeysUntouchable(ULONG *);
static void			MarkAllDCKeysUntouchable(ULONG);
static ULONG		GetNextFreeKey(void);
static ULONG		CountFreeAltKeys(void);
static void			AskRecover(WORD);
static DirFibPtr	RCreateDummyParent(ULONG, BOOL);
static DirFibPtr	BuildRFileEntry(UBYTE *, ULONG, BOOL);
static DirFibPtr	BuildRDirEntry(UBYTE *, ULONG, BOOL);
static void			UpdateDummyDir(UBYTE *, DirFibPtr, BOOL);
static void			SortInsert(DirFibPtr, DirFibPtr);
static BOOL			CheckDummyTree(void);
static void			MarkDummyDirDeleted(DirFibPtr);
static BOOL			RecoverFile(DirFibPtr);
static BOOL			RestoreDir(DirFibPtr);
static BOOL			RestoreParent(DirFibPtr);
static WORD			GetNewRecoverEntryName(UBYTE *, ULONG);
static DirFibPtr	LatestDirItem(void);
static DirFibPtr	NextFile(void);
static ULONG 		DoesFileExist(TextPtr, UBYTE *, ULONG);
static void			FormulateNewName(UBYTE *, ULONG, BOOL);
static BOOL			CopyFile(DirFibPtr);
static BOOL			CopyData(DirFibPtr, File);
static BOOL			ShortenFile(DirFibPtr, ULONG, ULONG);
static BOOL			ReadRecoverExtent(FileHdrPtr, BOOL, ULONG *, ULONG *, ULONG *);
static BOOL			TestFileExists(UBYTE *, ULONG);
static BOOL			CreateParentBlock(DirFibPtr, ULONG);
static void			LinkDummyTreeToDirTree(void);

/*
 * Do the recovery!
 */
 
BOOL DoRecover()
{
	register BOOL success;
	register DirFibPtr dirFib;
	register Dir lock = NULL;
	TextChar buff[GADG_MAX_STRING];
	TextPtr ptr;
	WindowPtr old_window;
	struct FileInfoBlock *fibPtr = (struct FileInfoBlock *) fib;
	register WORD len;
	BOOL more;
	
	abortFlag = FALSE;
	recoverStats.NextFreeKey = currVolParams.LowestKey;
	
	if( options.RecoverOpts.Destination != DEST_SAME_VOL ) {
		success = FALSE;
		if( AllocateExtBuffer() ) {
			GetEditItemText(currGadgList, UNDEL_DEST_TEXT, options.RecoverOpts.DestName);
/*
	Try to get a lock on the path as is. If we fail, don't let AmigaDOS put up
	a requester yet, because if it is an invalid volume, we're going to do Lock()
	again, and we'll give the user a chance to insert the volume at that time.
*/
			old_window = process->pr_WindowPtr;
			process->pr_WindowPtr = (APTR)-1L;
			lock = Lock(options.RecoverOpts.DestName, MODE_OLDFILE);
			process->pr_WindowPtr = old_window;
			strcpy(buff, options.RecoverOpts.DestName);
			len = strlen(buff);
			ptr = strchr(buff, ':');
			more = ptr[1];							/* Null-terminated afterward? */
			if( lock != NULL ) {					/* Make sure it's a dir! */
				if( Examine(lock, fibPtr) ) {
					if( fibPtr->fib_DirEntryType < 0 ) {
						Error(ERR_FILE_DEST);
					} else {
						success = TRUE;
					}
				}
			} else {
				if( ptr != NULL ) {
					ptr[1] = 0;
					if( lock = Lock(buff, MODE_OLDFILE) ) {
						UnLock(lock);
						strcpy(buff, options.RecoverOpts.DestName);
						if( buff[len-1] == '/' ) {	/* Zap any ending slash */
							buff[len-1] = 0;
						}
						if( (lock = Lock(buff, MODE_OLDFILE)) == NULL ) {
							lock = CreateDir(buff);
						}
					}
				}
				if( !(success = lock != NULL) ) {
					FreeExtBuffer();
					Error(ERR_BAD_DESTNAME);
				}
			}
			if( success ) {
				strcpy(strSavePath, options.RecoverOpts.DestName);
				if( ptr != NULL ) {
					if( more ) {					/* Is there a directory specification? */
						if( strSavePath[len-1] != '/' ) {
							strSavePath[len++] = '/';
							strSavePath[len] = '\0';
						}
					}
				}
				recoverStats.PathLen = len;
			}
		} else {
			OutOfMemory();
		}
	} else {
		success = PromptForWriteProtected();
	}
	if( success ) {
		if( recoverStats.DirTree->df_Child != NULL ) {
			recoverStats.CurFib = (DirFibPtr)&recoverStats.DirTree->df_Child;
			if( options.RecoverOpts.Destination == DEST_DIFF_VOL ) {
				while( (!abortFlag) && (dirFib = NextFile()) ) {
					success = CopyFile(dirFib);
					skipFlag = cancelFlag = FALSE;
					CheckMainPort();
				}
			} else {
				while( (!abortFlag) && (dirFib = LatestDirItem()) ) {
					if( dirFib->df_Flags & FLAG_NOT_BUSY_MASK ) {
						if( success = RestoreParent(dirFib->df_Parent) ) {
							success = RecoverFile(dirFib);
						}
					}
					skipFlag = cancelFlag = FALSE;
					CheckMainPort();
				}
				ForceValidate();
				WriteCache();
				if( bitMapDirtyFlag ) {
					WriteBitMap();			/* Do this now, so everything is finished. */
					(void) ReadBitMap();	/* Re-read, since writing destroys it. */
				}
			}
			recoverStats.CurFib = recoverStats.DirTree;
		}
		DetermineFileIntegrities(TRUE);
		MotorOff();
	}
	FreeExtBuffer();
	if( lock != NULL ) {
		UnLock(lock);
	}
	return(success);
}

/*
 * This routine actually restores the file (in place) passed.
 */
 
static BOOL RecoverFile(register DirFibPtr dirFib)
{
	ULONG blocksLost, blocksExpected;
	register BOOL success = FALSE;
	register DirFibPtr parentFib = dirFib->df_Parent;
	BufCtrlPtr buffer;
	UBYTE bstrName[MAX_FILENAME_LEN+2];
	register ULONG ownKey = dirFib->df_Ownkey;
	register ULONG temp;
	
	AppendPath(&dirFib->df_Name);
	DisplayRecoverPath();
	if( (!CheckFileBlocks(ownKey, &blocksLost, &blocksExpected)) || blocksLost ) {
/*
	If we haven't warned the user about this file being corrupt yet, and we 
	are not in a quiet interaction mode, then do so now, and give the user
	the chance to skip recovery of this file.
*/
		if( options.PrefsOpts.Interactive == 0 ) {
			dialogVarStr = &dirFib->df_Name;
			AskRecover(BIG_ERR_FILE_DAMAGED);
		}
	}
	if( !cancelFlag ) {
		
/*
	File recoverable. But does duplicate file already exist? If so prompt to rename.
*/
		success = TRUE;
		if( temp = DoesFileExist(&dirFib->df_Name, bstrName, parentFib->df_Ownkey) ) {
			switch( GetNewRecoverEntryName(bstrName, parentFib->df_Ownkey) ) {
			case REPLACE_BUTTON:
				success = RawDeleteFile(temp, parentFib->df_Ownkey);
				break;
			case RENAME_BUTTON:
				if( success = ReadControl(ownKey, &buffer) == CERR_NOERR ) {
					UnlinkCache(buffer);
					BlockMove(bstrName, ( (FileHdrEndPtr) (((UBYTE *)buffer) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(FileHdrEnd)) )->FileName, bstrName[0]+1);
					LinkCache(buffer);
					buffer->DirtyFlag = TRUE;
				}
				break;
			case CANCEL_BUTTON:
				abortFlag = TRUE;
			case SKIP_BUTTON:
				success = FALSE;
				/*cancelFlag = skipFlag = TRUE;*/
			}
		}
		if( success ) {
			if( success = ReadControl(ownKey, &buffer) == CERR_NOERR ) {
				if( ((FileHdrPtr)(buffer+1))->Ownkey != ownKey ) {
					buffer->DirtyFlag = TRUE;
					((FileHdrPtr)(buffer+1))->Ownkey = ownKey;
				}
				if( dirFib->df_Flags & FLAG_NEW_PARENT_MASK ) {
					( (FileHdrEndPtr) (((UBYTE *)buffer) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(FileHdrEnd)) )->Parent = parentFib->df_Ownkey;
					buffer->DirtyFlag = TRUE;
				}
				temp = blocksExpected - blocksLost;
				if( skipFlag && blocksExpected && temp ) {
					success = ShortenFile(dirFib, temp, blocksLost);
				}
			}
			if( success ) {
				currVolParams.ValidateVol = currVolParams.DirCache;
				MarkFileBlocks(ownKey, FALSE);			/* Mark blocks busy */
				if( success = LinkParentDir(ownKey, parentFib->df_Ownkey) ) {
					if( WriteCache() == 0 ) {
						--recoverStats.SelFiles;
						--recoverStats.SelDelFiles;
						--recoverStats.TotalFiles;
						--recoverStats.DelFiles;
						dirFib->df_Flags &= ~(FLAG_SEL_MASK | FLAG_NOT_BUSY_MASK);
						RefreshRecoverStats();
					}
				}
			}
		}
	}
	InitPath();
	return(success);
}

/*
 * Rewrite shortened file header/extension blocks, zapping the keys that
 * are already marked as busy.
 */
 
static BOOL ShortenFile(DirFibPtr dirFib, ULONG numBlocks, ULONG lostBlocks)
{
	ULONG *extPtr;
	ULONG *savePtr, *saveExtPtr;
	BufCtrlPtr buffer;
	FileHdrPtr fileHdrPtr;
	FileHdrEndPtr fileHdrEndPtr;
	ULONG ownKey;
	register ULONG *ptr;
	register BOOL success = FALSE;
	register ULONG *keyPtr;
	register ULONG count;
	register ULONG key;
	register ULONG fileKey = dirFib->df_Ownkey;
	FileHdrEnd saveFileHdrEnd;
	ULONG numExts = (numBlocks - 1) / currVolParams.HashTableSize;
	
	if( (ptr = (ULONG *) MemAlloc((numBlocks+1) << 2, MEMF_CLEAR)) &&
		((!numExts) || (extPtr = (ULONG *)MemAlloc(numExts << 2, MEMF_CLEAR))) ) {
		savePtr = ptr;
		saveExtPtr = extPtr;
		if( ReadCtrlBlock(fileKey, &buffer, TRUE) == CERR_NOERR ) {
			success = TRUE;
			fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)buffer) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(FileHdrEnd));
			saveFileHdrEnd = *fileHdrEndPtr;
			UnlinkCache(buffer);
			do {
				fileHdrPtr = (FileHdrPtr)(buffer+1);
				fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
				keyPtr = &fileHdrEndPtr->Reserved;
				count = fileHdrPtr->BlkCnt;				/* Get block count */
				if( count > currVolParams.HashTableSize ) {
					count = currVolParams.HashTableSize;	/* Limit # entries */
				}
				while( count-- ) {							/* Get all data keys */
					if( key = *--keyPtr ) {					/* Just in case */
						if( GetKeyStatus(key) && numBlocks--) {
							*ptr++ = key;
							/*MarkKeyBusy(key);*/
						}
					}
				}
				if( key = fileHdrEndPtr->Ext ) {			/* Get ext filehdr, if any */
					if( success = GetKeyStatus(key) ) {	/* Ext block MUST be FREE! */
						success = ReadExtHdr(key, fileKey, &buffer);
						*extPtr++ = key;
					}
				}
			} while( success && key );
			ptr = savePtr;
			extPtr = saveExtPtr;
			if( success = BuildFileHdr(saveFileHdrEnd.FileName, &buffer) ) {
				LinkCacheKey(fileKey, BLOCK_CONTROL, buffer);
				fileHdrPtr = (FileHdrPtr) (buffer+1);
				fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
				*fileHdrEndPtr = saveFileHdrEnd;
				fileHdrEndPtr->FileSize -= lostBlocks * currVolParams.BlockSize - (currVolParams.FFSFlag ? 0 : 24);
				fileHdrPtr->FirstBlk = *ptr;
				ownKey = dirFib->df_Ownkey;
				do {
					buffer->DirtyFlag = TRUE;	
					fileHdrPtr = (FileHdrPtr) (buffer+1);
					fileHdrPtr->Ownkey = ownKey;
					fileHdrPtr->BlkCnt = 0;
					fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
					keyPtr = &fileHdrEndPtr->Reserved;
					count = currVolParams.HashTableSize;
					while( count-- ) {
						if( key = *ptr ) {
							*--keyPtr = key;
							++fileHdrPtr->BlkCnt;
							ptr++;
						} else {
							break;
						}
					}
					if( *ptr ) {
						ownKey = dirFib->df_Ownkey;
						success = BuildExtHdr(fileKey, &ownKey, &buffer, *extPtr++);
						fileKey = ownKey;
					} else {
						break;
					}
				} while( success &= (ReadControl(fileKey, &buffer) == CERR_NOERR) );
			}
		}
	}
	ptr = savePtr;
	if( ptr != NULL ) {
		MemFree(ptr, (numBlocks+1) << 2);
		if( numExts && (saveExtPtr != NULL) ) {
			MemFree(saveExtPtr, numExts << 2);
		}
	}
	return(success);		
}

/*
 * See if filename already exists (bypassing AmigaDOS of course).
 * Return zero if non-existent, otherwise its key.
 */
 
static ULONG DoesFileExist(TextPtr fileName, UBYTE *bstrName, ULONG key)
{
	register WORD temp;
	ULONG result;
	ULONG resultKey;
	BufCtrlPtr buffer;
	
	temp = MIN(MAX_FILENAME_LEN, strlen(fileName));
	bstrName[0] = temp;
	BlockMove(fileName, &bstrName[1], temp+1);
	
	if( options.RecoverOpts.Destination == DEST_DIFF_VOL ) {
		if( result = (ULONG) Lock(strSavePath, MODE_OLDFILE) ) {
			UnLock((File)result);
		} 
	} else {
			
		result = RawOpenFile(bstrName, key, &resultKey, &buffer) ? resultKey : 0L;
	}
	return(result);
}
				
/*
 * Checks for existence of parent and restores it if necessary.
 * Can loop all the way back to the root.
 */
 
static BOOL RestoreParent(register DirFibPtr dirFib)
{
	BOOL success = TRUE;	
	register DirFibPtr origFib;
	
	if( dirFib->df_Flags & FLAG_NOT_BUSY_MASK ) {
		origFib = dirFib;
		do {
			dirFib->df_Parent->df_CurFib = dirFib;
			dirFib = dirFib->df_Parent;
		} while( dirFib->df_Flags & FLAG_NOT_BUSY_MASK );
		
		do {
			dirFib = dirFib->df_CurFib;
			success = RestoreDir(dirFib);
		} while( (dirFib != origFib) && success );	
	}
	return(success);
}

/*
 * Restores a directory in place.
 */
 
static BOOL RestoreDir(register DirFibPtr dirFib)
{
	register BOOL success;
	BufCtrlPtr buffer;
	DirBlockPtr dirBlockPtr;
	UBYTE bstrDir[MAX_FILENAME_LEN+2];
	register Dir lock;
	register ULONG key;
	
	if( options.RecoverOpts.Destination == DEST_DIFF_VOL ) {
		strcpy(&strSavePath[recoverStats.PathLen], &strPath[strlen(choiceNameColon)]);
		if( (lock = Lock(strSavePath, MODE_OLDFILE)) == NULL ) {
			lock = CreateDir(strSavePath);
			if( ReadCtrlBlock(dirFib->df_Ownkey, &buffer, TRUE) == CERR_NOERR ) {
				UpdateCommentAndProt((((UBYTE *)buffer) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(DirBlockEnd))));
			}
		}
		if( success = lock != NULL ) {
			UnLock(lock);
		}
	} else {
		if( success = (dirFib->df_Flags & FLAG_NEW_PARENT_MASK) &&
			((dirFib->df_Flags & FLAG_PROCESSED_MASK) == 0) ) {
			if( (dirFib->df_Ownkey != -2) && GetAltStatus(dirFib->df_Ownkey) ) {
				key = dirFib->df_Ownkey;		/* Try to use same key if possible */
			} else {
				key = GetNextFreeKey();			/* Otherwise grab first safe one */
			}
			if( success = (key != 0) ) {
				success = CreateParentBlock(dirFib, key);
			}
		} else if( ReadCtrlBlock(dirFib->df_Ownkey, &buffer, TRUE) == CERR_NOERR ) {
			success = TRUE;
			buffer->DirtyFlag = TRUE;
			dirBlockPtr = (DirBlockPtr)(buffer+1);
			BlockClear(dirBlockPtr+1, currVolParams.HashTableSize << 2);
			((DirBlockEndPtr) (((UBYTE *)dirBlockPtr) + currVolParams.BlockSize - sizeof(DirBlockEnd)))->HashChain = NULL;
		}
		if( success ) {
			currVolParams.ValidateVol = currVolParams.DirCache;
			key = dirFib->df_Ownkey;
			MarkKeyBusy(key);
			
			if( DoesFileExist(&dirFib->df_Name, bstrDir, key) ) {
				success = GetNewRecoverEntryName(bstrDir, key) < SKIP_BUTTON;
			}
			if( success ) {
				LinkParentDir(key, dirFib->df_Parent->df_Ownkey);
			}
		}
	}
	if( success ) {
		if( dirFib->df_Child == NULL ) {
			recoverStats.SelDirs--;
		}
		dirFib->df_Flags &= ~(FLAG_NOT_BUSY_MASK | FLAG_PART_BUSY_MASK);
	}
	return(success);
}

/*
 * Calculate the maximum recoverable percentage of each deleted file.
 * An additional effect is that the processed flag is reset for each entry.
 */
 
static void DetermineFileIntegrities(BOOL dontRead)
{
	register DirFibPtr dirFib = (DirFibPtr) &recoverStats.DirTree;
	register DirFibPtr rootFib = dirFib->df_Next;
	register BOOL cont = TRUE;
	register BOOL isDir;
	register ULONG filesDone = 0;
	ULONG lost, total;
	
	if( rootFib->df_Child != NULL ) {
		do {
			if( dirFib->df_Next == NULL ) {
				dirFib = dirFib->df_Parent;
				cont = dirFib != rootFib;
			} else {
				dirFib = dirFib->df_Next;
				while( (isDir = IS_DIR(dirFib->df_Type)) && (dirFib->df_Child != NULL) ) {
					dirFib = dirFib->df_Child;
					dirFib->df_Flags &= ~FLAG_PROCESSED_MASK;
				}
				dirFib->df_Flags &= ~FLAG_PROCESSED_MASK;
/*
	Empty drawers are now counted, and deselected if existing and selected.
*/
				if( isDir ) {
					if( dirFib->df_Flags & FLAG_SEL_MASK ) {
						if( dirFib->df_Flags & FLAG_NOT_BUSY_MASK ) {
							++recoverStats.SelDirs;
						} else {
							dirFib->df_Flags &= ~FLAG_SEL_MASK;
						}
					}
				} else if( (dirFib->df_Flags & FLAG_NOT_BUSY_MASK) ) {
					if( !dontRead ) {
						if( barGraph != NULL ) {
							DoSetBarGraph(requester, barGraph, ++filesDone);
							if( dontRead = CheckRequest(mainMsgPort, cmdWindow, DialogFilter) == RECOVER_CANCEL ) {
								DisposeBarGraph(barGraph);
								barGraph = NULL;
								if( requester != NULL ) {
									DestroyRequest(requester);
								}
							}
						}
						CheckFileBlocks(dirFib->df_Ownkey, &lost, &total);
					}
					if( (!dontRead) || (options.RecoverOpts.Destination == DEST_SAME_VOL) ) {
						if( dontRead ) {
							dirFib->df_Percent = -1;
						} else {
							if( total && lost ) {
								dirFib->df_Percent = MIN(99, (100 - ((lost * 100) / total)));
							} else {
								dirFib->df_Percent = 100;
							}
						}
					}
				}
			}
		} while( cont );
	}
}

/*
 * At the urging of James, we will do the arduous task of traversing
 * the tree and finding the most recent entries to undelete first,
 * so that the most recent work is always undeleted, for EACH and
 * EVERY single directory entry in the list.
 */
 
static DirFibPtr LatestDirItem()
{
	register UWORD days = 0;
	register UWORD time = 0;
	register DirFibPtr currFib = NULL;
	register DirFibPtr dirFib = NULL;	/* Squelch compiler */
	DirFibPtr rootFib = recoverStats.DirTree;
	DirFibPtr nextFib = rootFib->df_Child;
	register BOOL cont = TRUE;
	register BOOL isDir;
	
	do {
		if( nextFib == NULL ) {
			dirFib = dirFib->df_Parent;
			TruncatePath();
			cont = dirFib != rootFib;
		} else {
			dirFib = nextFib;
			while( (isDir = IS_DIR(dirFib->df_Type)) && (dirFib->df_Child != NULL) && (dirFib->df_Flags & FLAG_SEL_MASK) ) {
				AppendPath(&dirFib->df_Name);
				dirFib = dirFib->df_Child;
			}
			if( dirFib->df_Flags & FLAG_SEL_MASK ) {
				if( (dirFib->df_Flags & FLAG_PROCESSED_MASK) == 0 ) {
					if( (!options.RecoverOpts.ShowDelOnly) || (dirFib->df_Flags & FLAG_NOT_BUSY_MASK) ) {
						if( isDir ) {
							RestoreDir(dirFib);
						} else if( (dirFib->df_Date > days) ||
							(dirFib->df_Time > time) ) {
							currFib = dirFib;
							days = currFib->df_Date;
							time = currFib->df_Time;
							strcpy(strSavePath, strPath);
						}
					}
				}
			}
		}
		nextFib = dirFib->df_Next;
	} while( cont );

	if( currFib != NULL ) {
		currFib->df_Flags |= FLAG_PROCESSED_MASK;
		strcpy(strPath, strSavePath);
	}
	return( currFib );
}

/*
 * Return the next item to be restored to a different volume.
 * Creates missing subdirectories on the fly.
 */
 
static DirFibPtr NextFile()
{
	register DirFibPtr currFib = NULL;
	register DirFibPtr dirFib = recoverStats.CurFib;
	DirFibPtr nextFib = NextDirItem(dirFib);
	register BOOL cont = TRUE;
	register BOOL isDir;
	
	do {
		if( nextFib == NULL ) {
			if( cont = dirFib != recoverStats.DirTree ) {
				dirFib = dirFib->df_Parent;
				TruncatePath();
			}
		} else {
			dirFib = nextFib;
			while( (isDir = IS_DIR(dirFib->df_Type)) && ((nextFib = NextDirItem((DirFibPtr)&dirFib->df_Child)) != NULL) && (dirFib->df_Flags & FLAG_SEL_MASK) ) {
				AppendPath(&dirFib->df_Name);
				RestoreDir(dirFib);				/* Create possible new intermediate dirs */
				dirFib = nextFib;
			}
			if( dirFib->df_Flags & FLAG_SEL_MASK ) {
				if( (dirFib->df_Flags & FLAG_PROCESSED_MASK) == 0 ) {
					if( isDir ) {
						AppendPath(&dirFib->df_Name);
						RestoreDir(dirFib);	/* Create the selected empty drawer */
						TruncatePath();
					} else {
						currFib = dirFib;		/* Otherwise we have our file */
						cont = FALSE;
					}
				}
			}
		}
		nextFib = NextDirItem(dirFib);
	} while( cont );

	if( currFib != NULL ) {
		currFib->df_Flags |= FLAG_PROCESSED_MASK;
		recoverStats.CurFib = dirFib;
	}
	return( currFib );
}

/*
 * Get a new name for this entry from the user. Return TRUE if did just that.
 * Returns new filename to use, or NULL if this file is not to be restored.
 */
 
static WORD GetNewRecoverEntryName(register UBYTE *bstr, ULONG parentKey)
{
	register RequestPtr req;
	DialogPtr window = cmdWindow;
	GadgetPtr gadgList;
	TextChar buff[MAX_FILENAME_LEN + 50/*strlen(strRenameFile)*/];
	TextChar editBuff[GADG_MAX_STRING];
	register WORD item;
	
	if( options.PrefsOpts.Interactive ) {
		item = RENAME_BUTTON;
		FormulateNewName(bstr, parentKey, TRUE);
	} else {
		
		BeginWait();
		AutoActivateEnable(FALSE);
		req = DoGetRequest(REQ_RENAMEFILE);
		AutoActivateEnable(TRUE);
	
		if( req != NULL ) {
			gadgList = req->ReqGadget;
			requester = req;
			OutlineOKButton(window);
			dialogVarStr = &bstr[1];
			ParseDialogString(strRenameFile, buff);
			SetGadgetText(GadgetItem(gadgList, RENAME_STAT_TEXT), window, req, buff);
			FormulateNewName(bstr, parentKey, TRUE);
			SetEditItemText(gadgList, RENAME_EDIT_TEXT, window, req, &bstr[1]);
			ErrBeep();

			do {
				if( (item = ModalRequest(mainMsgPort, window, DialogFilter)) == RENAME_BUTTON ) {
					GetEditItemText(gadgList, RENAME_EDIT_TEXT, editBuff);
					editBuff[MAX_FILENAME_LEN] = '\0';
					if( strchr(editBuff, ':') || strchr(editBuff, '/') ) {
						Error(ERR_BAD_RENAME);
						item = -1;
					} else {
						strcpy(&bstr[1], editBuff);
						bstr[0] = strlen(editBuff);
						FormulateNewName(bstr, parentKey, FALSE);
					}
				}
			} while( item == -1 );
			DestroyRequest(req);
		}
		EndWait();
	}
	return(item);
}

/*
 * Formulate a new unique name 
 */
 
static void FormulateNewName(register UBYTE *bstr, register ULONG key, BOOL checked)
{
	register TextPtr str = &bstr[1];
	register ULONG i;
	TextPtr numPtr;
	
	if( checked || TestFileExists(bstr, key) ) {
		if( (i = bstr[0]) > (MAX_FILENAME_LEN-5) ) {
			i = MAX_FILENAME_LEN-5;
		}
		strcpy(&str[i], strCopy);
		bstr[0] = strlen(str);
		if( TestFileExists(bstr, key) ) {
			if( (i = bstr[0]) > 25 ) {
				i = 25;
			}
			/*
			strcpy(&str[i], strCopy);
			*/
			numPtr = &str[i];
			i = 1;
			do {
				NumToString(++i, numPtr);
				bstr[0] = strlen(str);
			} while( TestFileExists(bstr, key) );
		}
	}
}

/*
 * Test whether file exists from within the rename requester
 */
 
static BOOL TestFileExists(register UBYTE *bstr, ULONG key)
{
	ULONG resultKey;
	BufCtrlPtr buf;
	BOOL success;
	File lock;
	
	if( options.RecoverOpts.Destination == DEST_DIFF_VOL ) {
		DoTruncatePath(strSavePath);
		AppendDirPath(strSavePath, &bstr[1]);
		if( success = lock = Lock(strSavePath, MODE_OLDFILE) ) {
			UnLock(lock);
		}
	} else {
		success = RawOpenFile(bstr, key, &resultKey, &buf);
	}
	return(success);
}

/*
 * Copy file to differnt volume
 */

static BOOL CopyFile(DirFibPtr dirFib)
{
	ULONG blocksLost, blocksExpected;
	BOOL success = FALSE;
	UBYTE bstrName[MAX_FILENAME_LEN+2];
	File file;
	LONG dosError;
	
	AppendPath(&dirFib->df_Name);
	DisplayRecoverPath();
	dialogVarStr = &dirFib->df_Name;

/*
	If the read bit was turned off, check to see if the user wants to copy it.
*/
	if( (dirFib->df_Flags & FLAG_BAD_BLOCK_MASK) && (options.PrefsOpts.Interactive == 0) ) {
		AskRecover(BIG_ERR_NO_READ_PRIV);
		skipFlag = cancelFlag;
	}

	if( !cancelFlag ) {
		if( (dirFib->df_Flags & FLAG_NOT_BUSY_MASK) && ((!CheckFileBlocks(dirFib->df_Ownkey, &blocksLost, &blocksExpected)) || blocksLost) ) {
/*
	If we haven't warned the user about this file being corrupt yet, and we 
	are not in a quiet interaction mode, then do so now, and give the user
	the chance to skip recovery of this file.
*/
			if( !(skipFlag || options.PrefsOpts.Interactive) ) {
				AskRecover(BIG_ERR_FILE_DAMAGED);
			}
		}
		if( !cancelFlag ) {
		
/*
	File recoverable. But does duplicate file already exist? If so prompt to rename.
*/
			success = FALSE;
			strcpy(&strSavePath[recoverStats.PathLen], &strPath[strlen(choiceNameColon)]);
			if( DoesFileExist(&dirFib->df_Name, bstrName, NULL) ) {
				switch( GetNewRecoverEntryName(bstrName, NULL) ) {
				case RENAME_BUTTON:
					TruncatePath();
					AppendPath(&bstrName[1]);
				case REPLACE_BUTTON:
					strcpy(&strSavePath[recoverStats.PathLen], &strPath[strlen(choiceNameColon)]);
					success = (file = Open(strSavePath, MODE_NEWFILE)) != NULL;
					break;
				case CANCEL_BUTTON:
					abortFlag = TRUE;
				case SKIP_BUTTON:
					file = NULL;
					skipFlag = cancelFlag = TRUE;
				}
			} else {
				success = (file = Open(strSavePath, MODE_NEWFILE)) != NULL;
			}
			if( file != NULL ) {
				if( success = CopyData(dirFib, file) ) {
					dirFib->df_Flags &= ~FLAG_SEL_MASK;
					--recoverStats.SelFiles;
					if( dirFib->df_Flags & FLAG_NOT_BUSY_MASK ) {
						--recoverStats.SelDelFiles;
					} else {
						--recoverStats.SelActFiles;
					}
				}	
			} else if( !skipFlag ) {
				dosError = IoErr();
				if( dosError != ERROR_DISK_FULL ) {
					DOSError(ERR_WRITE_DEST_FILE, dosError);
				} else {
					abortFlag = TRUE;				/* User already told AmigaDOS "Cancel" */
				}
			}
		}
	}
	TruncatePath();
	return(success);
}

/*
 * Grab the data from source and write it to destination file.
 */
 
static BOOL CopyData(register DirFibPtr dirFib, File file)
{
	register BOOL success = FALSE;
	register BOOL activeFile = (dirFib->df_Flags & FLAG_NOT_BUSY_MASK) == 0;
	register ULONG size;
	BufCtrlPtr buffer;
	ULONG remainSize, actualSize, markSize;
	ULONG key;
	LONG dosError;
	FileHdrEndPtr fileHdrEndPtr;
	register BOOL cont;
	
	if( ReadCtrlBlock(dirFib->df_Ownkey, &buffer, TRUE) == CERR_NOERR ) {
		fileHdrEndPtr = ( (FileHdrEndPtr) (((UBYTE *)buffer) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(FileHdrEnd)) );
		remainSize = fileHdrEndPtr->FileSize;
		dialogVarStr = &dirFib->df_Name;	/* In case requester brought up */
		do {
			(void) ReadRecoverExtent((FileHdrPtr)(buffer+1), activeFile, &markSize, &actualSize, &remainSize);
			size = actualSize;
			cont = remainSize != 0;
			if( !(success = Write(file, extentBuffer + extentBufferSize - markSize, size) == size) ) {
				dosError = IoErr();
				if( dosError != ERROR_DISK_FULL ) {
					DOSError(ERR_WRITE_DEST_FILE, dosError);
				} else {
					abortFlag = TRUE;				/* User already told AmigaDOS "Cancel" */
				}
			} else {
				if( cont && (cont = ((key = ( (FileHdrEndPtr) (((UBYTE *)buffer) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(FileHdrEnd)) )->Ext) != NULL)) ) {
					success = ReadExtHdr(key, dirFib->df_Ownkey, &buffer);
				}
			}
		} while( success && cont && (!cancelFlag) );
	}
	Close(file);
	if( success && (ReadCtrlBlock(dirFib->df_Ownkey, &buffer, TRUE) == CERR_NOERR) ) {
		UpdateCommentAndProt((((UBYTE *)buffer) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(FileHdrEnd)) );
	}		
	return(success);
}

/*
 * Update the file/dir's protection flags and comment
 */
 
static void UpdateCommentAndProt(void *fileHdrEndPtr)
{
	register ULONG protect = ((FileHdrEndPtr)fileHdrEndPtr)->Protect;
	register TextPtr comment = ((FileHdrEndPtr)fileHdrEndPtr)->Comment;
	MsgPortPtr procID;
	register TextPtr ptr;
	register WORD size;
	ULONG args[4];
	
	if( protect || *comment ) {
		if( LibraryVersion((struct Library *) DOSBase) < OSVERSION_2_0_4 ) {
			ptr = (TextPtr) ((((ULONG)&strBuff[0]) | 3) + 1);
			procID = DeviceProc(strSavePath);
			args[1] = IoErr();
			args[2] = (ULONG) MKBADDR(ptr);
			size = strlen(strSavePath);
			size = MIN(255, size);
			BlockMove(strSavePath, &ptr[1], size+1);
			ptr[0] = size;
			if( protect ) {
				args[3] = protect;
				DoPacket(procID, ACTION_SET_PROTECT, &args[0], 4);
			}
			if( *comment ) {
				args[3] = (ULONG) MKBADDR(comment);
				DoPacket(procID, ACTION_SET_COMMENT, &args[0], 4);
			}
		} else {
			if( protect ) {
				SetProtection(strSavePath, protect);
			}
			if( *comment ) {
				BlockMove(&comment[1], strBuff, *comment);
				strBuff[*comment] = '\0';
				SetComment(strSavePath, strBuff);
			}
		}
	}
}

/*
 * Reads data blocks for exthdr passed into extent buffer. If error reading
 * block, deletes that block from the file and proceeds to next block, if any.
 * Returns FALSE if remainder of file to is be skipped. 
 *
 * Variables output: 
 * "bytesReadSize" is the number of bytes from the end of the extent buffer
 *		that the data begins.
 *	"bytesRead" is the actual number of bytes in that buffer
 * 	minus the possible zap of the remainder in the last block.
 * "remainingSize"
 * 	the number of bytes remaining the file to be processed, used to determine
 * 	whether it is time for "bytesRead" to zap its remainder. This is also
 *		an input that must be supplied!
 */
 
static BOOL ReadRecoverExtent(FileHdrPtr fileHdrPtr, BOOL activeFile, ULONG *bytesReadSize, ULONG *bytesRead, ULONG *remainingSize)
{
	register UWORD numBlocks = MIN(currVolParams.HashTableSize, fileHdrPtr->BlkCnt);
	UWORD saveBlocks = numBlocks;
	FileHdrEndPtr fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
	register ULONG *keyPtr = &fileHdrEndPtr->Reserved;
	register UBYTE *blockBuf = extentBuffer + extentBufferSize - currVolParams.BlockSize;
	UWORD offset = currVolParams.FFSFlag ? 0 : 24;
	UWORD defaultBlockSize = currVolParams.BlockSize - offset;
	register UWORD blockSize = defaultBlockSize;
	register ULONG remainder;
	
	remainder = *remainingSize;
	if( remainder < extentBufferSize ) {
		if( remainder = remainder % blockSize ) {
			blockSize = remainder;
		}
	}
	*bytesReadSize = 0;
	*bytesRead = 0;
	while( numBlocks-- && *--keyPtr );
	numBlocks = saveBlocks;
	
	while( numBlocks-- ) {
		*remainingSize -= blockSize;
		if( (activeFile || ((!bitMapFlag) || GetKeyStatus(*keyPtr))) && (ReadBlock(*keyPtr, blockBuf, FALSE) == 0) ) {
			*bytesRead += blockSize;
			blockSize = defaultBlockSize;
			blockBuf -= blockSize;
			*bytesReadSize += blockSize;
		} else {
/*
	If we haven't warned the user about this file being corrupt yet, and we 
	are not in a quiet interaction mode, then do so now, and give the user
	the chance to skip recovery of this file.
*/
			if( !(skipFlag || options.PrefsOpts.Interactive) ) {
				dialogVarNum = *keyPtr;
				AskRecover(BIG_ERR_READ_FILE_BLOCK);
			}
			blockSize = defaultBlockSize;
			/*BlockClear(&blockBuf[offset], blockSize);*/
		}
		keyPtr++;
		/*blockBuf -= blockSize;*/
	}
	return(cancelFlag);
}

/*
 * Display a dialog with three choices: Yes, No, and Stop.
 * It displays the big string error text passed in "id".
 * Returns results in the global flags "cancelFlag", "abortFlag",
 * and sets "skipFlag" to TRUE.
 */
 
static void AskRecover(WORD id)
{
	register WORD response;
	
	response = DoDialog(bigStrsErrors[id], SKIP_BUTTON, _strYes, strStop, _strNo, TRUE);
	cancelFlag = response != OK_BUTTON;
	abortFlag = response == SKIP_BUTTON;
	skipFlag = TRUE;
}

/*
 * Build the deleted file tree
 */
 
BOOL BuildDeletedTree()
{
	register ULONG block;
	register UWORD index = 0;
	register UWORD remainder;
	register UBYTE *ptr;
	register UBYTE *basePtr;
	ULONG numBufs;
	BOOL cancelFlag = FALSE;
	UBYTE *bufPtr = NULL;
	BYTE err;
	RequestPtr req;
	Rectangle rect;
	ULONG max = currVolParams.HighestKey /* - currVolParams.Envec.de_PreAlloc*/;
	
	BeginWait();
	
	barGraph = NULL;
	if( requester = req = DoGetRequest(REQ_RECOVER) ) {
		GetGadgetRect(GadgetItem(req->ReqGadget, RECOVER_BAR), cmdWindow, req, &rect);
		if( barGraph = NewBarGraph(&rect, currVolParams.MaxKeys, 8, BG_HORIZONTAL | BG_SHOWPERCENT) ) {
			DoDrawBarGraph(req, barGraph);
		}
	}
	(void) LoadBadBlocksFile();

	if( currVolParams.BlockAccess || (bufPtr = basePtr = AllocTrack(&numBufs)) == NULL ) {
		numBufs = 1;
		basePtr = blockBuffer;
	}
	remainder = numBufs - (currVolParams.LowestKey % numBufs);
	
	for( block = currVolParams.LowestKey ; (block <= max) && !(abortFlag) ; block++ ) {
		if( req != NULL ) {
			abortFlag = CheckRequest(mainMsgPort, cmdWindow, DialogFilter) == RECOVER_CANCEL;
		}
		if( !abortFlag ) {
			if( index == 0 ) {
				ptr = basePtr;
				if( AnyBadInRange(block, remainder) ) {
					if( GetBadStatus(block) ) {	/* There's bad ones ahead: is this one? */
						index = 1;					/* Nope, so allow I/O to it. */
					} else {
						RefreshRecoverStats();
						DoSetBarGraph(req, barGraph, block+remainder);
					}
					if( --remainder == 0 ) {		/* In any event, bump remainder down */
						remainder = numBufs;
					}
				} else {
					index = remainder;
					remainder = numBufs;
				}
				if( index ) {							/* Don't try to read if index is 0. */
					DoReadBlock(block, ptr, index << currVolParams.BlockSizeShift, TRUE);
					RefreshRecoverStats();
					DoSetBarGraph(req, barGraph, block+index);
					if( err = WaitBlock() ) {
						index = 0;					/* Force non-processing of block */
					}
				}
			} else {
				ptr += currVolParams.BlockSize;
			}
			if( index ) {
				index--;
/*
	Only processes blocks non-bitmap blocks if bitmap was loaded.
*/
				if( bitMapFlag && IsBitMapKey(block) ) {
					MarkAltBusy(block);
				} else {
					if( ((FileHdrPtr)ptr)->Type == T_SHORT ) {
						AddControlBlock(block, (FileHdrPtr)ptr);
					} else if( ((FileHdrPtr)ptr)->Type == T_LIST ) {
						MarkAltBusy(block);		/* File extension can't be destroyed */
						MarkAllHashKeysUntouchable((ULONG *)(ptr + sizeof(FileHdr)));
					}
				}
			}
		}
	}
	
	RefreshRecoverStats();

	FreeBadBlockBuffer();
	
	if( bufPtr != NULL ) {
		FreeIOBuffer(&bufPtr, numBufs << currVolParams.BlockSizeShift);
	}

	ForceRead();
	
	if( !( cancelFlag |= abortFlag) ) {
		(void) CheckDummyTree();
		recoverStats.CurFib = recoverStats.DirTree;
	
		if( barGraph != NULL ) {
			SetGadgetItemText(req->ReqGadget, REQ_BUILD_TEXT, cmdWindow, req, strDelCheck);
			DoSetBarGraph(req, barGraph, 0);
			DoSetBarGraphMax(req, barGraph, recoverStats.DelFiles);
		}
		SetArrowPointer();
		DetermineFileIntegrities(!bitMapFlag);
		SetWaitPointer();
	}
	if( barGraph != NULL ) {
		DisposeBarGraph(barGraph);
		barGraph = NULL;
	}
	if( requester != NULL ) {
		DestroyRequest(requester);
	}
	
	ForceRead();
	MotorOff();						/* In case aborted without dialog somehow */
	EndWait();
	
	return( !cancelFlag );
}

/*
 * Processes a file header/user dir for disk recovery.
 */
 
static void AddControlBlock(ULONG key, register FileHdrPtr fileHdrPtr)
{
	LONG secType;

	if( VerifyCtrlBlock((UBYTE *)fileHdrPtr, &secType) ) {
		if( CalcBlockChecksum((ULONG *)fileHdrPtr) == 0L ) {
			MarkAltBusy(key);
			switch( secType ) {
			case ST_ROOT:
				if( currVolParams.DirCache ) {
					MarkAllDCKeysUntouchable( *(((ULONG *)fileHdrPtr) + currVolParams.BlockSizeL - 2) );
				}
				break;
			case ST_USERDIR:
				abortFlag = !AddUserDir(key, (DirBlockPtr) fileHdrPtr);
				break;
			case ST_FILE:
				abortFlag = !AddUserFile(key, fileHdrPtr);
				break;
			case ST_SOFTLINK:
			case ST_LINKDIR:
			case ST_LINKFILE:
				break;
			default:
				break;
			}
		}
	}
}

/*
 * Mark all DC blocks as unusable for restore-in-place destruction.
 */
 
static void MarkAllDCKeysUntouchable(ULONG extKey)
{
	while( extKey != NULL ) {
		if( ReadBlock(extKey, blockBuffer, FALSE) == 0 ) {
			if( extKey = ((CacheHdrPtr)blockBuffer)->NextBlock ) {
				MarkAltBusy(extKey);
			}
		} else {
			extKey = NULL;
		}
	}
}

/*
 * Process a user directory block.
 */
 
static BOOL AddUserDir(ULONG key, DirBlockPtr dirBlockPtr)
{
	DirBlockEndPtr dirBlockEndPtr;
	register DirFibPtr dirFibPtr;
	DirFibPtr parentEntry;
	register BOOL flag = FALSE;
	register ULONG parentKey = -2;
	register BOOL free = (!bitMapFlag) || GetKeyStatus(key);
	
	dirBlockEndPtr = (DirBlockEndPtr) (((UBYTE *)dirBlockPtr) + currVolParams.BlockSize - sizeof(DirBlockEnd));
	
/*
	If DC file system, find all directory cache blocks for this directory.
*/
	if( currVolParams.DirCache ) {
		MarkAllDCKeysUntouchable(dirBlockEndPtr->Ext);
	}
	if( dirBlockEndPtr->Parent != key ) {
		parentKey = dirBlockEndPtr->Parent;
		if( (parentEntry = SearchTree(parentKey, &recoverStats.DirTree)) != NULL ) {
			if( (parentEntry->df_Type & TYPE_DIR_MASK) == 0) {
				parentKey = -2;
			} else {
				flag = TRUE;
			}
		}
	}
	if( !flag ) {				
		if( (parentEntry = SearchTree(parentKey, &recoverStats.DummyTree)) == NULL ) {
			flag = (parentEntry = RCreateDummyParent(parentKey, free)) != NULL;
		} else {
			flag = TRUE;
		}
	}
	if( flag ) {
		flag = FALSE;
		if( dirFibPtr = SearchTree(key, &recoverStats.DirTree) ) {
			Error(ERR_DIR_ALREADY_IN_LIST);			/* Should NEVER happen!!! */
		} else {
			if( (dirFibPtr = SearchTree(key, &recoverStats.DummyTree)) != NULL ) {
				flag = TRUE;
				UpdateDummyDir(dirBlockEndPtr->DirName, dirFibPtr, free);
				UnlinkDummy(dirFibPtr, recoverStats.DummyTree);
			} else {
				flag = (dirFibPtr = BuildRDirEntry(dirBlockEndPtr->DirName, key, free)) != NULL;
			}
			if( flag ) {
				if( (strcmp(&dirFibPtr->df_Name, &bstrRecoveredItems[1]) == 0) &&
					(dirBlockEndPtr->Parent == currVolParams.RootBlock) ) {

					if( (parentEntry == recoverStats.DirTree) &&
					   ((recoverStats.RecoverEntry == NULL) ||
					   ((recoverStats.RecoverEntry->df_Flags & FLAG_NOT_BUSY_MASK) && (!free)) ) ) {
						recoverStats.RecoverEntry = dirFibPtr;
					}
				}
				dirFibPtr->df_Date = dirBlockEndPtr->MDate.ds_Days;
				dirFibPtr->df_Time = ((dirBlockEndPtr->MDate.ds_Minute) << 5) + (dirBlockEndPtr->MDate.ds_Tick >> 7);
				SortInsert(dirFibPtr, parentEntry);
			}
		}
	}
	return( flag );
}

/*
 * Process a file header block.
 */

static BOOL AddUserFile(ULONG key, FileHdrPtr fileHdrPtr)
{
	FileHdrEndPtr fileHdrEndPtr;
	register BOOL free = (!bitMapFlag) || GetKeyStatus(key);
	register ULONG parentKey = -2;
	register DirFibPtr parentEntry;
	register DirFibPtr dirFibPtr;
	register BOOL flag = FALSE;
	
	++recoverStats.MaxFiles;
	MarkAllHashKeysUntouchable((ULONG *)(fileHdrPtr+1));
	
	if( free ) {
		++recoverStats.DelFiles;
		++recoverStats.SelFiles;
		++recoverStats.SelDelFiles;
		++recoverStats.TotalFiles;
	} else if( !options.RecoverOpts.ShowDelOnly ) {
		++recoverStats.TotalFiles;
	}
	fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
	if( fileHdrEndPtr->Ext != NULL ) {
		MarkAltBusy(fileHdrEndPtr->Ext);
	}
	if( fileHdrEndPtr->Parent != key ) {
		parentKey = fileHdrEndPtr->Parent;
		if( parentEntry = SearchTree(parentKey, &recoverStats.DirTree) ) {
			if( (parentEntry->df_Type & TYPE_DIR_MASK) == 0 ) {
				parentKey = -2;
			} else {
				flag = TRUE;
			}
		}
	}
/*
	If parent key was set to -2, we have a new filehdr which claims to have a parent
	dir which is already in the dirtree as a FILE.  The error is probably in the 
	new filehdr, since we only enter a file into the dirtree if it is real.
*/
	if( !flag ) {
	 	if( (parentEntry = SearchTree(parentKey, &recoverStats.DummyTree)) == NULL ) {
			parentEntry = RCreateDummyParent(parentKey, free);
		}
	}
	if( flag = parentEntry != NULL ) {
		if( dirFibPtr = SearchTree(key, &recoverStats.DummyTree) ) {
			dirFibPtr->df_Ownkey = -2;		/* Zap the dummy parent's key */
		}
		if( flag = (dirFibPtr = BuildRFileEntry(fileHdrEndPtr->FileName, key, free)) != NULL ) {
			dirFibPtr->df_Date = fileHdrEndPtr->MDate.ds_Days;
			dirFibPtr->df_Time = ((fileHdrEndPtr->MDate.ds_Minute) << 5) + (fileHdrEndPtr->MDate.ds_Tick >> 7);
			dirFibPtr->df_Size = fileHdrEndPtr->FileSize;
			if( fileHdrEndPtr->Protect & FIBF_READ ) {
				dirFibPtr->df_Flags |= FLAG_BAD_BLOCK_MASK;
			}
			SortInsert(dirFibPtr, parentEntry);
		}
	}
 	return(flag);
}

/*
 * Denote in alternate bitmap that we cannot create any new data in these
 * blocks because they are marked as having data in them by existing or
 * active files.
 */
 
static void MarkAllHashKeysUntouchable(register ULONG *ptr)
{
	register UWORD i;
	register UWORD size = currVolParams.HashTableSize;
	
	for( i = 0 ; i < size ; i++ ) {
		if( *ptr && CheckKey(*ptr) ) {
			MarkAltBusy(*ptr);
		}
		ptr++;
	}
}

/*
 * Returns the next free key that we can destroy, and marks it busy.
 * INPUT:	recoverStats.NextFreeKey must be initialized.
 * OUTPUT:	the next free key or zero if no more.
 */
 
static ULONG GetNextFreeKey()
{
	register ULONG key;
	
	if( bitMapFlag ) {
		key = recoverStats.NextFreeKey;
		while( key < currVolParams.HighestKey /*- currVolParams.Envec.de_PreAlloc*/ ) {
			if( GetAltStatus(key) ) {
				MarkAltBusy(key);
				recoverStats.NextFreeKey = key + 1;
				return(key);
			}
			key++;
		}
		recoverStats.NextFreeKey = key;
	}
	return(0);
}

/*
 * Returns the number of free keys that we can destroy.
 */
 
static ULONG CountFreeAltKeys()
{
	register ULONG num = 0;
	register ULONG max = currVolParams.HighestKey /*- currVolParams.Envec.de_PreAlloc */;
	register ULONG i;
	
	for( i = currVolParams.LowestKey ; i < max ; i++ ) {
		if( GetAltStatus(i) ) {
			num++;
		}
	}
	return(num);
}

/*
 * Inserts new FIB into chain at proper place, depending upon name. Directories
 * sort ahead of files. Replaces current level's FIB chain's head if necessary.
 * Addition in 2.0 Tools: Sorts entries by date if same name.
 */
 
#define PRESORT	1

static void SortInsert(DirFibPtr newFib, DirFibPtr newParent)
{
	register DirFibPtr *dirFib = &newParent->df_Child;
	register DirFibPtr nextFib;
	TextPtr newFileName;
	register WORD len;
#ifdef PRESORT
	register BOOL done;
	register WORD cmp;

	newFib->df_Next = NULL;
	newFib->df_Parent = newParent;
	newFileName = &newFib->df_Name;
	len = strlen(newFileName);
	if( IS_DIR(newFib->df_Type) ) {					/* Dirs sort ahead of files and others */
		do {
			nextFib = *dirFib;							/* Get first/next dir entry */
			if( nextFib != NULL ) {						/* End of chain? */
				if( IS_DIR(nextFib->df_Type) ) {
					cmp = CmpString(newFileName, &nextFib->df_Name, len, strlen(&nextFib->df_Name), FALSE);
					done = (cmp < 0) || ((cmp == 0) && ((newFib->df_Date > nextFib->df_Date) || ((newFib->df_Date == nextFib->df_Date) && (newFib->df_Time >= nextFib->df_Time))));
					if( !done ) {
						dirFib = &nextFib->df_Next;
					}
				} else {
					done = TRUE;
				}
			}
		} while( nextFib != NULL && !done );
	} else {
		do {
			nextFib = *dirFib;							/* Get first/next file entry */
			if( nextFib != NULL ) {						/* End of chain? */
				if( IS_DIR(nextFib->df_Type) ) {
					done = FALSE;							/* A dir, skip to next */
				} else {
					cmp = CmpString(newFileName, &nextFib->df_Name, len, strlen(&nextFib->df_Name), FALSE);
					done = (cmp < 0) || ((cmp == 0) && ((newFib->df_Date > nextFib->df_Date) || ((newFib->df_Date == nextFib->df_Date) && (newFib->df_Time >= nextFib->df_Time))));
				}
				if( !done ) {
					dirFib = &nextFib->df_Next;
				}
			}
		} while( nextFib != NULL && !done );
	}
#else
	nextFib = *dirFib;									/* Sorting later! */
#endif
	if( nextFib != NULL ) {
		newFib->df_Next = nextFib;
	} 
	*dirFib = newFib;
	return;
}

	
/*
 * This routine is called when a file hdr is found which references a parent
 * directory which is not yet present in the directory list.
 * Creates a dummy directory entry for parent key passed.
 * Returns it or NULL if failure.
 */
 
static DirFibPtr RCreateDummyParent(ULONG key, BOOL free)
{
	DirFibPtr dirFibPtr;
	
	if( (dirFibPtr = BuildRDirEntry(NULL, key, free)) != NULL ) {
		LinkChild((ReorgListPtr) recoverStats.DummyTree, (ReorgListPtr) dirFibPtr);
	}
	return(dirFibPtr);
}

/*
 * Called when real dir entry is found and dummy dir exists.
 */
 
static void UpdateDummyDir(register UBYTE *name, DirFibPtr dirFibPtr, BOOL free)
{
	dirFibPtr->df_Flags = free ? FLAG_NOT_BUSY_MASK | FLAG_DUMMY_MASK : FLAG_DUMMY_MASK;
	*name = MIN(MAX_FILENAME_LEN, *name);
	ConvertBCPLToCString(name, &dirFibPtr->df_Name);
}

/*
 * Attempts to build a file entry for recovering deleted files.
 * BSTR filename in "name", key in "key", busy status in "free".
 * Returns allocated block or NULL if failure.
 */
  
static DirFibPtr BuildRFileEntry(register UBYTE *name, ULONG key, BOOL free)
{
	register DirFibPtr dirFibPtr = NULL;
	register UWORD size;

	*name = MIN(MAX_FILENAME_LEN, *name);
	size = ((sizeof(DirFib) + *name) | 3)+1;
	if( (curDBCount >= size) || AllocateDirBlock() ) {
		dirFibPtr = (DirFibPtr) curDBPtr;
		dirFibPtr->df_Ownkey = key;
		dirFibPtr->df_Type = TYPE_FILE_HDR_MASK;
		dirFibPtr->df_Flags = free ? FLAG_NOT_BUSY_MASK | FLAG_SEL_MASK | FLAG_DUMMY_MASK : FLAG_DUMMY_MASK;
		ConvertBCPLToCString(name, &dirFibPtr->df_Name);
		curDBCount -= size;
		curDBPtr += size;
	}
	if( dirFibPtr == NULL ) {
		OutOfMemory();
	}
	return(dirFibPtr);
}

/*
 * Attempts to build a directory entry for recovering deleted files.
 * BSTR dirname in "name", key in "key", busy status in "free".
 * Returns allocated block or NULL if failure.
 */
  
static DirFibPtr BuildRDirEntry(register UBYTE *name, ULONG key, BOOL free)
{
	register DirFibPtr dirFibPtr = NULL;
	register UWORD size;

	if( name != NULL ) {
		*name = MIN(MAX_FILENAME_LEN, *name);
		size = ((sizeof(DirFib) + *name) | 3)+1;
	} else {
		size = ((sizeof(DirFib) + MAX_FILENAME_LEN) | 3)+1;
	}
	if( (curDBCount >= size) || AllocateDirBlock() ) {
		dirFibPtr = (DirFibPtr) curDBPtr;
		dirFibPtr->df_Ownkey = key;
		dirFibPtr->df_Type = TYPE_DIR_MASK;
		dirFibPtr->df_Flags = free ? FLAG_SEL_MASK | FLAG_NOT_BUSY_MASK | FLAG_DUMMY_MASK : FLAG_SEL_MASK | FLAG_DUMMY_MASK;
		if( name != NULL ) {
			ConvertBCPLToCString(name, &dirFibPtr->df_Name);
		} else {
			strcpy(&dirFibPtr->df_Name, strDrawerType);
			NumToString(key, (TextPtr) (((ULONG)&dirFibPtr->df_Name) + strlen(strDrawerType)));
		}
		curDBCount -= size;
		curDBPtr += size;
	} 
	if( dirFibPtr == NULL ) {
		OutOfMemory();
	}
	return(dirFibPtr);
}

/*
 * Scans dummy dir tree looking for files which can be restored, but which
 * have become "lost" - the parent dir was not found during the scan, so the
 * file was never moved into the DirTree.  Also tries to rescue files
 * in dummy tree which referenced as parent either themselves or a filehdr
 * block (parent key=-2).
 */
 
static BOOL CheckDummyTree()
{
	register ULONG lostFiles = 0;
	register ULONG lostDirs = 0;
	register DirFibPtr dirFibPtr;
	register DirFibPtr childFibPtr;
	register ULONG numKeys;
	TextChar numBuff[16];
	DirFibPtr temp;
	register BOOL foundLostFile;
	struct DateStamp dateStamp;
	TextPtr endText;

	SetWaitPointer();
/*
	Consolidate all "Recovered Items" drawers, regardless of conditions.
*/
	if( recoverStats.RecoverEntry != NULL ) {
		dirFibPtr = recoverStats.DirTree->df_Child;
		temp = NULL;
		while( dirFibPtr != NULL ) {
			if( strcmp(&dirFibPtr->df_Name, &bstrRecoveredItems[1]) == 0 ) {
				if( dirFibPtr != recoverStats.RecoverEntry ) {
					if( dirFibPtr->df_Type & TYPE_DIR_MASK ) {
						childFibPtr = dirFibPtr->df_Child;	/* Move children to new dad */
						while( childFibPtr != NULL ) {
							childFibPtr->df_Parent = recoverStats.RecoverEntry;
							childFibPtr = childFibPtr->df_Next;
						}
						if( temp == NULL ) {				/* Unlink forever from list */
							dirFibPtr->df_Parent->df_Child = dirFibPtr->df_Next;
						} else {
							temp->df_Next = dirFibPtr->df_Next;
						}
					}
				}
			}
			temp = dirFibPtr;
			dirFibPtr = dirFibPtr->df_Next;
		}
	}
/*
	Determine whether "lost" files exist (those with parent missing).
*/

	dirFibPtr = (DirFibPtr) &recoverStats.DummyTree->df_Child;
	while( dirFibPtr = dirFibPtr->df_Next ) {
		foundLostFile = FALSE;
		childFibPtr = (DirFibPtr) &dirFibPtr->df_Child;
		while( childFibPtr = childFibPtr->df_Next ) {
			if( childFibPtr->df_Type & TYPE_FILE_HDR_MASK ) {
				if( foundLostFile |= (!bitMapFlag) || GetKeyStatus(childFibPtr->df_Ownkey) ) {
					++lostFiles;		/* Test key status to avoid "fake" control blocks */
				}
			}
		}
		if( foundLostFile ) {
			++lostDirs;
		}
	}
	recoverStats.LostDirs = lostDirs;
/*
	If lost files exist, put them in the appropriate drawer - create a
	"Recovered Items" drawer if necessary - and either retain their
	hierarchy or stick them all in the same drawer, depending on preference.
*/	
	if( lostFiles ) {
		numKeys = CountFreeAltKeys();
		
		if( options.PrefsOpts.Interactive == 0 ) {
			dialogVarNum = lostFiles;
			NumToCommaString(lostDirs, dialogVarStr = numBuff);
			ParseDialogString(bigStrsErrors[BIG_ERR_RECOVER_LOST_DRAWERS], strBuff);
	
			if( options.RecoverOpts.CreateDrawers ) {
				if( numKeys ) {
					endText = (numKeys < lostDirs) ? strNotEnoughKeys : strEnoughKeys;
				} else {
					endText = strNoKeys;
				}
			} else {
				endText = numKeys ? strIgnoreKeys : strNoKeys;
			}
			strcat(strBuff, endText);
			DoDialog(strBuff, OK_BUTTON, _strOK, NULL, NULL, TRUE);
			SetWaitPointer();
		}
		if( (numKeys == 0) && ((recoverStats.RecoverEntry == NULL) || (recoverStats.RecoverEntry->df_Flags & FLAG_NOT_BUSY_MASK)) ) {
			dirFibPtr = recoverStats.DirTree;
		} else {
			if( recoverStats.RecoverEntry != NULL ) {
				dirFibPtr = recoverStats.RecoverEntry;
			} else {
				dirFibPtr = BuildRDirEntry(bstrRecoveredItems, GetNextFreeKey(), TRUE);
				numKeys--;
				DateStamp(&dateStamp);
				dirFibPtr->df_Date = dateStamp.ds_Days;
				dirFibPtr->df_Time = (dateStamp.ds_Minute << 5) + (dateStamp.ds_Tick >> 7);
				dirFibPtr->df_Flags |= FLAG_SEL_MASK | FLAG_NOT_BUSY_MASK | FLAG_NEW_PARENT_MASK;
				SortInsert(dirFibPtr, recoverStats.DirTree);
			}
		}
		recoverStats.RecoverEntry = dirFibPtr;
		recoverStats.NumAltKeys = numKeys;
/*
	Sort recovered (dummy) drawer list
*/
		if( childFibPtr = recoverStats.DummyTree->df_Child ) {
			recoverStats.DummyTree->df_Child = NULL;
			while( (dirFibPtr = childFibPtr) != NULL ) {
				childFibPtr = dirFibPtr->df_Next;
				SortInsert(dirFibPtr, recoverStats.DummyTree);
			}
		}
		LinkDummyTreeToDirTree();
	}
	return(TRUE);
}

/*
 * Attach all items in the dummy tree to the proper place in the directory tree.
 */
 
static void LinkDummyTreeToDirTree()
{
	register DirFibPtr dirFibPtr = (DirFibPtr) &recoverStats.DummyTree->df_Child;
	register DirFibPtr childFibPtr;
	DirFibPtr temp;
	struct DateStamp dateStamp;

	if( (!options.RecoverOpts.CreateDrawers) || (recoverStats.NumAltKeys < recoverStats.LostDirs) ) {
/*
	"Flatten" all files in the root/recovery drawer in this case.
*/
		while( dirFibPtr = dirFibPtr->df_Next ) {
			temp = dirFibPtr->df_Child;
			while( childFibPtr = temp ) {
				temp = childFibPtr->df_Next;
				childFibPtr->df_Flags |= FLAG_NEW_PARENT_MASK;
				SortInsert(childFibPtr, recoverStats.RecoverEntry);
			}
		}
	} else {
/*
	Keep files as children of their dummy dirs, but mark them as NEW_PARENT.
*/
		childFibPtr = dirFibPtr->df_Next;
		while( dirFibPtr = childFibPtr ) {
			childFibPtr = dirFibPtr->df_Next;
			if( !dirFibPtr->df_Date ) {
				DateStamp(&dateStamp);
				dirFibPtr->df_Date = dateStamp.ds_Days;
				dirFibPtr->df_Time = (dateStamp.ds_Minute << 5) + (dateStamp.ds_Tick >> 7);
				MarkDummyDirDeleted(dirFibPtr);
				dirFibPtr->df_Flags |= FLAG_SEL_MASK | FLAG_NOT_BUSY_MASK | FLAG_NEW_PARENT_MASK;
			}
			SortInsert(dirFibPtr, recoverStats.RecoverEntry);
		}
	}
}

/*
 * The "CreateDrawers" flag has been changed. Update the directory tree to reflect
 * the new state.
 */
 
void RedoDummyTree()
{
	register DirFibPtr nextFibPtr;	/* To link pre-existing recovered entries */
	register DirFibPtr dirFibPtr;		/* Current entry in recovered items drawer */
	DirFibPtr tempFibPtr = NULL;		/* To unlink pre-existing recovered entries */
	DirFibPtr firstFibPtr;
	DirFibPtr saveFibPtr;
	register BOOL cont;
	
	SetWaitPointer();						/* Might be a long while... */
	
	if( (recoverStats.NumAltKeys >= recoverStats.LostDirs) && (recoverStats.RecoverEntry != NULL) ) {
		/*
		dirFibPtr = recoverStats.RecoverEntry->df_Child;
		nextFibPtr = NULL;
		*/
		nextFibPtr = (DirFibPtr) &recoverStats.RecoverEntry->df_Child;
		dirFibPtr = nextFibPtr->df_Next;
		firstFibPtr = dirFibPtr;
/*
	First, we will unlink all pre-existing recovered items from their associations
	with the lost items, while relinking them to their drawer. At the end of this
	section, all of the "df_Next" pointers will reflect that there are two lists
	now, one for lost items and one for pre-existing recovered items.
*/
		while( dirFibPtr != NULL ) {
			if( dirFibPtr->df_Flags & FLAG_NEW_PARENT_MASK ) {
				/*if( nextFibPtr != NULL ) {*/
					nextFibPtr->df_Next = dirFibPtr->df_Next;
				/*}*/
				tempFibPtr = dirFibPtr;
			} else {
				if( tempFibPtr != NULL ) {
					tempFibPtr->df_Next = dirFibPtr->df_Next;
				}
				nextFibPtr = dirFibPtr;
			}
			dirFibPtr = dirFibPtr->df_Next;
		}
/*
	Re-establish "parent" fields for each of the children of former lost dirs
	(since these were set to the "Recovered Items" drawer previously).
*/
		if( options.RecoverOpts.CreateDrawers && (recoverStats.DummyTree != NULL) ) {
/*
	Here we save all df_Next pointers to df_CurFib for temporary use below.
*/
			dirFibPtr = recoverStats.DummyTree->df_Child;
			while( dirFibPtr != NULL ) {
				nextFibPtr = dirFibPtr->df_CurFib = dirFibPtr->df_Child;
				dirFibPtr->df_Child = NULL;
				while( nextFibPtr != NULL ) {
					nextFibPtr->df_CurFib = nextFibPtr->df_Next;
					nextFibPtr = nextFibPtr->df_Next;
				}
				dirFibPtr = dirFibPtr->df_Next;
			}
/*
	Traverse the deleted file list and reattach them to their original parents
	one at a time.
*/
			dirFibPtr = (DirFibPtr) &firstFibPtr;
			saveFibPtr = dirFibPtr->df_Next;
			while( (dirFibPtr = saveFibPtr) != NULL ) {
				saveFibPtr = dirFibPtr->df_Next;
				nextFibPtr = recoverStats.DummyTree->df_Child;
				do {
					tempFibPtr = nextFibPtr;
					nextFibPtr = nextFibPtr->df_CurFib;
					while( (cont = (nextFibPtr != dirFibPtr)) && (nextFibPtr->df_CurFib != NULL) ) {
						nextFibPtr = nextFibPtr->df_CurFib;
					}
					nextFibPtr = tempFibPtr;
					if( cont ) {
						nextFibPtr = nextFibPtr->df_Next;
					}
				} while( cont );
				SortInsert(dirFibPtr, nextFibPtr);
			}
		}
		LinkDummyTreeToDirTree();
	}
}
		
/*
 * Mark as deleted/selected all files/dirs in a directory.
 */

static void MarkDummyDirDeleted(register DirFibPtr dirFib)
{
	register BOOL isDir;
	register DirFibPtr rootFib = dirFib;
	register BOOL cont = TRUE;
	
	dirFib = (DirFibPtr) &dirFib->df_Child;
		
	if( dirFib->df_Next != NULL ) {
		do {
			if( dirFib->df_Next == NULL ) {
				dirFib = dirFib->df_Parent;
				cont = dirFib != rootFib;
			} else {
				dirFib = dirFib->df_Next;
				while( (isDir = IS_DIR(dirFib->df_Type)) && (dirFib->df_Child != NULL) ) {
					dirFib->df_Flags |= FLAG_SEL_MASK | FLAG_NOT_BUSY_MASK | FLAG_NEW_PARENT_MASK;
					dirFib = dirFib->df_Child;
				}
				dirFib->df_Flags |= FLAG_SEL_MASK | FLAG_NOT_BUSY_MASK | FLAG_NEW_PARENT_MASK;
				if( !isDir && (bitMapFlag && (!GetKeyStatus(dirFib->df_Ownkey))) ) {
					recoverStats.SelDelFiles++;
					if( !options.RecoverOpts.ShowDelOnly ) {
						++recoverStats.TotalFiles;
					}
				}
			}
		} while( cont );
	}
}

/*
 * For those "dummy" directories that need to be created in place, this routine
 * creates one on the root and writes it out at position "newKey".
 */
 
static BOOL CreateParentBlock(DirFibPtr dirFib, ULONG newKey)
{
	register DirBlockPtr dirBlockPtr;
	register DirBlockEndPtr dirBlockEndPtr;
	BufCtrlPtr buffer;
	BOOL success = FALSE;
	ULONG nextKey;
	
	if( ReadSpecial(newKey, &buffer) == CERR_NOERR ) {
		if( (currVolParams.DirCache) && ((nextKey = GetNextFreeKey()) == 0) ) {
			ExplainDialog(BIG_ERR_NO_SAFE_AREA);
			abortFlag = TRUE;
		} else {
			UnlinkCache(buffer);
			dirBlockPtr = (DirBlockPtr) (buffer+1);
			BlockClear(dirBlockPtr, currVolParams.BlockSize);
			dirBlockEndPtr = (DirBlockEndPtr) (((UBYTE *)dirBlockPtr) + currVolParams.BlockSize - sizeof(DirBlockEnd));
			DateStamp(&dirBlockEndPtr->MDate);
			dirBlockEndPtr->DirName[0] = strlen(&dirFib->df_Name);
			strcpy(&dirBlockEndPtr->DirName[1], &dirFib->df_Name);
			dirBlockEndPtr->SecType = ST_USERDIR;
			dirBlockPtr->Type = T_SHORT;
			dirBlockPtr->Ownkey = newKey;
			LinkCacheKey(newKey, BLOCK_CONTROL, buffer);
			if( success = LinkParentDir(newKey, dirFib->df_Parent->df_Ownkey) ) {
				dirFib->df_Ownkey = newKey;
				if( currVolParams.DirCache ) {
					dirBlockEndPtr->Ext = nextKey;
					success = CreateDCBlock(nextKey, newKey);
				} else {
					buffer->DirtyFlag = TRUE;
				}
			}	
		}
	}
	return(success);
}

/*
 * Create a Directory Cache block that will be validated by the file system.
 */
 
BOOL CreateDCBlock(ULONG newKey, ULONG parentKey)
{
	BufCtrlPtr buffer, buffer2;
	register CacheHdrPtr cacheHdrPtr;
	register BOOL success;
	
	if( success = (ReadSpecial(newKey, &buffer) == CERR_NOERR) ) {
		cacheHdrPtr = (CacheHdrPtr) (buffer+1);
		BlockClear(cacheHdrPtr, currVolParams.BlockSize);
		cacheHdrPtr->Type = T_DIRCACHE;
		cacheHdrPtr->Ownkey = newKey;
		cacheHdrPtr->Parent = parentKey;
		cacheHdrPtr->CkSum -= CalcBlockChecksum((ULONG *)cacheHdrPtr);
		buffer->DirtyFlag = TRUE;
		if( success = ReadControl(parentKey, &buffer2) == CERR_NOERR ) {
			buffer2->DirtyFlag = TRUE;
			cacheHdrPtr = (CacheHdrPtr)(buffer2 + 1);
			*(((ULONG *)cacheHdrPtr) + currVolParams.BlockSizeL - 2) = newKey;
		}
	}
	return(success);
}

/*
 * Initialize recover deleted files lists.
 */
 
BOOL InitRecoverLists()
{
	BOOL success = FALSE;
	
	curDBBase = (UBYTE *) &baseDirFib;
	curDBCount = 0L;
	if( recoverStats.DirTree = BuildRDirEntry("\x01:", currVolParams.RootBlock, FALSE) ) {
		if( recoverStats.DummyTree = BuildRDirEntry("\12Dummy-tree", -1, FALSE) ) {
			recoverStats.RecoverEntry = NULL;
			success = TRUE;
		} else {
			FreeDirBlocks();
		}
	}
	return(success);
}

/*
 * Search for the key "key" in the directory tree in "treePtr".
 * Returns pointer to matching entry, or NULL if not found.
 * Removed recursion for V2.0.
 * NOTE: "treePtr" should be the ADDRESS of the tree you want to search.
 */

void *SearchTree(register ULONG key, void *treePtr)
{
	register BOOL cont;
	register DirFibPtr curFibPtr = (DirFibPtr) treePtr;
	register BOOL nextLevel;
	
	do {
		if( curFibPtr->df_Next == NULL ) {
			curFibPtr = curFibPtr->df_Parent;
			cont = curFibPtr != NULL;
		} else {
			curFibPtr = curFibPtr->df_Next;
			nextLevel = TRUE;
			while( (cont = curFibPtr->df_Ownkey != key) && nextLevel ) {
				if( IS_DIR(curFibPtr->df_Type) ) {
					if( nextLevel = (curFibPtr->df_Child != NULL) ) {
						curFibPtr = curFibPtr->df_Child;
					}
				} else {
					nextLevel = FALSE;
				}
			}
		}
	} while( cont );
	return( curFibPtr );
}

/*
 * Called to search top level of a dummy tree and remove entry "entryPtr" from it.
 * Entry is known to exist at top level of dummy tree.
 */
 
void UnlinkDummy(register void *entryPtr, void *treePtr)
{
	register DirFibPtr dirFib = (DirFibPtr) &((DirFibPtr)treePtr)->df_Child;
	
	while( (dirFib->df_Next != NULL) && (dirFib->df_Next != entryPtr) ) {
		dirFib = dirFib->df_Next;
	}
	if( dirFib != NULL ) {
		dirFib->df_Next = ((DirFibPtr)entryPtr)->df_Next;
	}
}

/*
 * Allocate memory for a directory block and setup forward link.
 */
 
#define DIR_BLOCK_SIZE	2100

void *AllocateDirBlock()
{
	register UBYTE *newFib;
	
	newFib = AllocMem(DIR_BLOCK_SIZE, MEMF_CLEAR);
	*((UBYTE **)curDBBase) = newFib;
	if( newFib == NULL ) {
		OutOfMemory();
	} else {
		curDBBase = newFib;
		curDBPtr =  newFib + sizeof(DirFibPtr);
		curDBCount = DIR_BLOCK_SIZE-4;
	}
	return(newFib);
}

/*
 * Free all blocks allocated by AllocateDirBlock()
 */

void FreeDirBlocks()
{
	register ULONG *currFib;
	register ULONG *nextFib;
	
	currFib = (ULONG *) baseDirFib;
	while( currFib != NULL ) {
		nextFib = (ULONG *) *currFib;
		FreeMem(currFib, DIR_BLOCK_SIZE);
		currFib = nextFib;
	}
	baseDirFib = NULL;
}
