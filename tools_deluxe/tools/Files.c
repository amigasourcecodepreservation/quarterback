/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	File-oriented functions
 */

#include <exec/types.h>
#include <dos/dos.h>
#include <dos/dosextens.h>

#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Utility.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct DosLibrary *DOSBase;
extern TextChar		strBuff[], choiceNameColon[];
extern TextPtr			strPath, strSavePath;
extern VolParams		currVolParams;
extern UBYTE			badBlockBSTR[];
extern ULONG			*bitMapDataPtr;
extern ULONG			*badBlockBuffer;
extern BadBlockStats		badBlockStats;
extern OptionsRec		options;
extern BOOL				bitMapFlag, bitMapDirtyFlag;

/*
 *	Local prototypes and definitions
 */

#define LOOP_LIMIT		100			/* Arbitrary V1.x looped dir limit. */
#define MAX_BAD_BLOCKS	1000			/* Arbitrary V1.x number- Obsolete now. */

#define CHECK_BAD_BLOCKS_MAX	1

/*
 * Ensure the path ends in an appropriate vol/dir character.
 */
 
void InitPath()
{
	register UWORD len;
	register TextChar ch;
	
	strcpy(strPath, choiceNameColon);
	len = strlen(strPath);
	ch = strPath[len-1];
	if( ch != ':' && ch != '/' && len < PATHSIZE ) {
		strPath[len] = '/';
		strPath[len+1] = 0;
	}
}			

/*
 * Appends name passed to path variable. Limits path to PATHSIZE length.
 * On restore, if full path NOT being restored, does nothing. Algorithm
 * appends a "/" if another "/" is detected before the volume specifier 
 * (":") is reached.
 */

BOOL AppendPath(register TextPtr str)
{
	return(AppendDirPath(strPath, str));
}

/*
 * Unconditionally appends file/dir name "str" to vol/dir spec "strPathname"
 */
 
BOOL AppendDirPath(register TextPtr strPathname, register TextPtr str)
{
	register UWORD len;
	register TextChar ch;
	BOOL success = FALSE;
	
	len = strlen(strPathname);
	if( (len + strlen(str)) < (PATHSIZE - 2) ) {
		success = TRUE;
		if( len ) {
			ch = strPathname[len-1];
			if( ch != ':' && ch != '/' ) {
				strPathname[len] = '/';
				strPathname[len+1] = 0;
			}
		}
		strcat(strPathname, str);
	}
	return(success);
}

/*
 * Deletes last subdir name from path variable. On restore, if full path
 * NOT being restored, does nothing. Algorithm deletes the trailing "/"
 * if there is one, but not a trailing ":". If neither present, you get
 * an empty string as the result.
 */
 
void TruncatePath()
{
	DoTruncatePath(strPath);
}

/*
 * The grunt routine, in case you want to do truncating on other than
 * the global "strPath".
 */
 
void DoTruncatePath(register TextPtr pathName)
{
	register UWORD len;
	register TextPtr str;
	
	if( ( len = strlen(pathName) ) ) {
		str = &pathName[len];
		while( (str > pathName) && (*(str-1) != ':') && (*str != '/') ) {
			str--;				
		}
		*str = 0;					/* Null-terminate truncated string */
	}
}

/*
 * Links new filehdr or dir into parent dir's hash chain.
 * Updates parent dir's date of last modification, and
 * clears archive bit, unless root.
 */
 
BOOL LinkParentDir(ULONG newKey, register ULONG parentDirHashChain)
{
	register BOOL success;
	FileHdrEndPtr fileHdrEndPtr;
	register ULONG *hashEntryPtr;
	BufCtrlPtr buf1, buf2;
	register BufCtrlPtr rewriteChain;

	if( success = !ReadCtrlBlock(newKey, &buf1, TRUE) ) {
		if( success = !ReadCtrlBlock(parentDirHashChain, &buf2, TRUE) ) {
			DateStamp(&((RootBlockEndPtr)(((UBYTE *)buf2) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(RootBlockEnd))))->RMDate);
			buf2->DirtyFlag = TRUE;
			rewriteChain = NULL;
			fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)buf1) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(FileHdrEnd)));
			fileHdrEndPtr->Parent = parentDirHashChain;
			hashEntryPtr = (ULONG *) (((UBYTE *)buf2) + (sizeof(BufCtrl) + sizeof(DirBlock)) + CalcHash(fileHdrEndPtr->FileName));
			while( ((parentDirHashChain = *hashEntryPtr) != NULL) && (parentDirHashChain <= newKey) && success ) {
				if( success = !ReadCtrlBlock(parentDirHashChain, &buf2, TRUE) ) {
					rewriteChain = buf2;
					hashEntryPtr = &((FileHdrEndPtr)(((UBYTE *)(buf2)) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(FileHdrEnd))))->HashChain;
				}
			}
			if( success ) {
				*hashEntryPtr = newKey;
				fileHdrEndPtr->HashChain = parentDirHashChain;
				if( rewriteChain != NULL ) {
					rewriteChain->DirtyFlag = TRUE;
				}
			}
		}
	}
	return(success);
}

/*
 * Unlinks filehdr or dir from parent, the converse of above routine.
 * Returns TRUE if successful.
 */
 
BOOL UnlinkParentDir(ULONG oldKey, ULONG parentDirHashChain)
{
	register BOOL success;
	ULONG saveFwdLink;
	FileHdrEndPtr fileHdrEndPtr;
	register ULONG *hashEntryPtr;
	BufCtrlPtr buf1, buf2;
	register BufCtrlPtr rewriteFileHdr;

	if( success = !ReadCtrlBlock(oldKey, &buf1, TRUE) ) {
		if( success = !ReadCtrlBlock(parentDirHashChain, &buf2, TRUE) ) {
			buf2->DirtyFlag = TRUE;
			rewriteFileHdr = NULL;
			fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)buf1) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(FileHdrEnd)));
			saveFwdLink = fileHdrEndPtr->HashChain;
			hashEntryPtr = (ULONG *) (((UBYTE *)buf2) + sizeof(BufCtrl) + sizeof(FileHdr) + CalcHash(fileHdrEndPtr->FileName));
			while( (success &= (*hashEntryPtr != NULL)) && (*hashEntryPtr != oldKey) ) {
				if( success = !ReadCtrlBlock(*hashEntryPtr, &buf2, TRUE) ) {
					rewriteFileHdr = buf2;
					hashEntryPtr = &((FileHdrEndPtr)(((UBYTE *)buf2) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(FileHdrEnd))))->HashChain;
				}
			}
			if( success ) {
				*hashEntryPtr = saveFwdLink;
				if( rewriteFileHdr != NULL ) {
					rewriteFileHdr->DirtyFlag = TRUE;
				}
			} else {
				Error(ERR_DELETE_ERR);
			}
		}
	}
	return(success);
}
	
/*
 * Marks all blocks of a filehdr whose key is passed busy or free.
 * NOTE: POTENTIAL LOOP PROBLEM WITH EXTENSION FILEHDRS!!!!
 */
 
BOOL MarkFileBlocks(ULONG fileKey, register BOOL free)
{
	BufCtrlPtr buffer;
	FileHdrPtr fileHdrPtr;
	FileHdrEndPtr fileHdrEndPtr;
	register ULONG *bitMapPtr;
	register ULONG *keyPtr;
	register ULONG count;
	register ULONG key = fileKey;
	register BOOL success;
	
	if( success = !ReadCtrlBlock(key, &buffer, TRUE) ) {
		bitMapPtr = bitMapDataPtr;						/* Put into register for speed */
		do {
			fileHdrPtr = (FileHdrPtr)(buffer+1);
			fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
			keyPtr = &fileHdrEndPtr->Reserved;
			count = fileHdrPtr->BlkCnt;
			if( count > currVolParams.HashTableSize ) {
				count = currVolParams.HashTableSize;/* Limit # of entries */
			}
			MarkBitMapOccupied(bitMapPtr, key, free);
			while( count-- ) {							/* Get all data block keys */
				if( key = *--keyPtr ) {					/* Check for 0, just in case. */
					MarkBitMapOccupied(bitMapPtr, key, free);
				}
			}
			if( key = fileHdrEndPtr->Ext ) {			/* Get ext filehdr, if any */
				success = ReadExtHdr(key, fileKey, &buffer);
			}
		} while( success && key );						/* Process ext hdr if no error */
		bitMapDirtyFlag = TRUE;
	}
	return(success);
}

/*
 * Checks that all data blocks of a filehdr whose key is passed are still
 * free in bitmap. Links forward via filehdr extension, if necessary.
 * Returns FALSE if read error if extensions not free, TRUE if successful.
 * Number of lost blocks is also returned in the supplied variable.
 */
 
BOOL CheckFileBlocks(ULONG fileKey, register ULONG *lostBlocks, ULONG *totalBlocks)
{
	BufCtrlPtr buffer;
	FileHdrPtr fileHdrPtr;
	FileHdrEndPtr fileHdrEndPtr;
	register ULONG *keyPtr;
	register ULONG count;
	register ULONG key;
	register BOOL success = TRUE;
	register ULONG totalKeys = 0;
	ULONG expectedKeys;
	
	*lostBlocks = 0;
	if( bitMapFlag ) {
		if( success = !ReadCtrlBlock(fileKey, &buffer, TRUE) ) {
			fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)buffer) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(FileHdrEnd));
			expectedKeys = fileHdrEndPtr->FileSize >> currVolParams.BlockSizeShift;
			do {
				fileHdrPtr = (FileHdrPtr)(buffer+1);
				fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
				keyPtr = &fileHdrEndPtr->Reserved;
				count = fileHdrPtr->BlkCnt;				/* Get # of blocks in hdr */
				if( count > currVolParams.HashTableSize ) {
					count = currVolParams.HashTableSize;	/* Limit # of entries */
				}
				while( count-- ) {							/* Get all data block keys */
					if( key = *--keyPtr ) {					/* Check for 0 just in case. */
						totalKeys++;
						if( !GetKeyStatus(key) ) {
							++*lostBlocks;					/* Oops, lost a block. */
						}
					}
				}
				if( key = fileHdrEndPtr->Ext ) {			/* Get ext filehdr, if any */
					if( success = GetKeyStatus(key) ) {	/* Ext block MUST be FREE! */
						success = ReadExtHdr(key, fileKey, &buffer);
					}
				}
			} while( success && key );
		
			if( !success ) {
				*lostBlocks = expectedKeys - totalKeys;
				totalKeys = expectedKeys;
			}
		}
	}
	*totalBlocks = totalKeys;
	return(success);
}

/*
 * Reads "fileExtKey" and verifies that it is a file extension header, and
 * that its parent is "parentKey". If successful, returns TRUE and stuffs
 * the "buffer" variable with the control block that was read.
 */
 
BOOL ReadExtHdr(ULONG fileExtKey, ULONG parentKey, BufCtrlPtr *buffer)
{
	FileHdrPtr fileHdrPtr;
	FileHdrEndPtr fileHdrEndPtr;
	register BOOL success;
		
	if( success = (ReadControl(fileExtKey, buffer) == CERR_NOERR) ) {
		success = FALSE;
		fileHdrPtr = (FileHdrPtr)((*buffer)+1);
		if( (fileHdrPtr->Type == T_LIST) && (fileHdrPtr->Ownkey == fileExtKey) ) {
			fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
			if( fileHdrEndPtr->SecType == ST_FILE ) {
				if( !(success = fileHdrEndPtr->Parent == parentKey) ) {
					PutCacheBuffer(*buffer);
				}
			}
		}
	}
	return(success);
}

/*
 * Initializes file hdr buffer for a new file. Filename is BSTR "bstr".
 * Returns TRUE if successful, and pointer to file header.
 */
 
BOOL BuildFileHdr(register UBYTE *bstr, BufCtrlPtr *buffer)
{
	register BOOL success;
	FileHdrPtr fileHdrPtr;
	FileHdrEndPtr fileHdrEndPtr;
	register UBYTE *ptr;
	register UBYTE len;
	
	*buffer = NULL;
	if( success = CheckBSTRFileName(bstr) ) {
		if( success = GetCacheBuffer(buffer) == CERR_NOERR ) {
			fileHdrPtr = (FileHdrPtr) ((*buffer)+1);
			BlockClear(fileHdrPtr, currVolParams.BlockSize);
			fileHdrPtr->Type = T_SHORT;
			fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
			fileHdrEndPtr->SecType = ST_FILE;
			ptr = &fileHdrEndPtr->FileName[0];
			len = *bstr;
			do {
				*ptr++ = *bstr++;
			} while( len-- );
			DateStamp(&fileHdrEndPtr->MDate);
			(*buffer)->DirtyFlag = TRUE;
		}
	}
	return(success);
}

/*
 * Builds an extension header from "predKey" and adds it to existing file.
 * Pass the parent key in "extKey"!
 * Returns TRUE if successful, and "extKey" is changed.
 * to reflect the new extension allocated, and its buffer is returned.
 */
 
BOOL BuildExtHdr(ULONG predKey, ULONG *extKey, BufCtrlPtr *buffer, ULONG newKey)
{
	BOOL success = FALSE;
	register FileExtPtr fileExtPtr;
	register FileExtEndPtr fileExtEndPtr;
	BufCtrlPtr predBuf;
	register ULONG blockSize;
	
	if( GetCacheBuffer(buffer) == CERR_NOERR ) {
		(*buffer)->DirtyFlag = TRUE;
		fileExtPtr = (FileExtPtr) ((*buffer)+1);
		blockSize = currVolParams.BlockSize;
		fileExtEndPtr = (FileExtEndPtr) (((UBYTE *)fileExtPtr) + blockSize - sizeof(FileExtEnd));
		BlockClear(fileExtPtr, blockSize);
		fileExtPtr->Type = T_LIST;
		fileExtEndPtr->SecType = ST_FILE;
		fileExtEndPtr->Parent = *extKey;
		*extKey = fileExtPtr->Ownkey = newKey;
		LinkCacheKey(*extKey, BLOCK_CONTROL, *buffer);
		if( ReadControl(predKey, &predBuf) == CERR_NOERR ) {
			predBuf->DirtyFlag = TRUE;
			fileExtEndPtr = (FileExtEndPtr) (((UBYTE *)(predBuf+1)) + blockSize - sizeof(FileExtEnd));
			fileExtEndPtr->Ext = *extKey;			/* Set up link to new ext */
			success = TRUE;
		}
	}
	return(success);
}


/* 
 * This routine is called by anybody who wants the "bad.blocks" file loaded into a
 * bitmap so the bad blocks can be avoided. Returns TRUE if successful, and the
 * number of bad blocks in the repair stat variable "OldBadCount". If this is zero,
 * then no bad blocks were found and no bitmap was set up.
 */

BOOL LoadBadBlocksFile()
{
	ULONG count;
	BufCtrlPtr buf;
	BOOL success = FALSE;
	
	if( OpenBadBlocksFile(&count, &buf) ) {
		if( count ) {
			if( LoadBadBlockKeys() && BuildBadBlockBitMap() ) {
				success = TRUE;
			} else {
				badBlockStats.BadBlockFlag = FALSE;	/* It's invalid, so don't bother. */
			}
		}
	}
	return(success);
}

/*
 * Called to "OPEN" the "bad.blocks" file in the root block, if possible.
 * Returns TRUE if successful, and the following:
 * Count of bad blocks in "count", and its buffer returned in "buffer".
 */
 
BOOL OpenBadBlocksFile(ULONG *count, BufCtrlPtr *buffer)
{
	register BOOL success;
	register ULONG curKey;
	BufCtrlPtr buf;
	FileHdrPtr fileHdrPtr;
	FileHdrEndPtr fileHdrEndPtr;
	ULONG key;
	
	*count = 0;
	badBlockStats.BadBlocksKey = 0;
	badBlockStats.OldBadCount = 0;
	badBlockStats.NewBadCount = 0;
	badBlockStats.InUseCount = 0;
	badBlockStats.BadBlockFlag = FALSE;
	if( success = !currVolParams.NonFSDisk ) {
		if( success = ReadRoot(FALSE, &buf) == CERR_NOERR ) {
			if( success = RawOpenFile(badBlockBSTR, currVolParams.RootBlock, &key, &buf) ) {
				badBlockStats.BadBlocksKey = key;
				*buffer = buf;
				do {
					fileHdrPtr = (FileHdrPtr) (buf+1);
					fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
					*count += fileHdrPtr->BlkCnt;
					if( (curKey = fileHdrEndPtr->Ext) == NULL ) {
						badBlockStats.OldBadCount = *count;
						badBlockStats.BadBlockFlag = TRUE;
						break;
					} else {
						if( !ReadExtHdr(curKey, key, &buf) ) {
							Error(ERR_BB_READ);
							success = FALSE;
						}
					}
				} while( success );
			}
		}
	}
	return(success);
}

/*
 * Deletes "bad.blocks" file and resets as though no bad blocks are known.
 * Returns TRUE if file unlinked from parent dir.
 */
 
BOOL DeleteBadBlocksFile()
{
	BOOL success;

	if( success = RawDeleteFile(badBlockStats.BadBlocksKey, currVolParams.RootBlock) ) {
		badBlockStats.BadBlockFlag = FALSE;				/* Show nothing to test */
		badBlockStats.BadBlocksKey = 0;					/* No longer around */
		/*
		WriteBitMap();
		WriteCache();
		*/
	}
	return(success);
}

/*
 * Given that the "bad.blocks" file exists, this routine loads the bad block keys
 * into the BadBlockBuffer.
 */
 
BOOL LoadBadBlockKeys()
{
	register BOOL success = TRUE;
	register ULONG *keyPtr;
	register ULONG *fileKeyPtr;
	register ULONG numBlocks;
	register ULONG key;
	FileHdrPtr fileHdrPtr;
	FileHdrEndPtr fileHdrEndPtr;
	BufCtrlPtr buf;
	register WORD count = currVolParams.MaxBadBlocks;
	
	if( badBlockBuffer == NULL ) {
		success = AllocateBadBlockBuffer();
	}
	if( success ) {
		keyPtr = badBlockBuffer;
		key = badBlockStats.BadBlocksKey;
		success = ReadCtrlBlock(key, &buf, TRUE) == CERR_NOERR;
		while( success ) {
			fileHdrPtr = (FileHdrPtr)(buf+1);
			numBlocks = fileHdrPtr->BlkCnt;
			fileHdrEndPtr = (FileHdrEndPtr)(((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
			fileKeyPtr = &fileHdrEndPtr->Reserved;
			while( numBlocks-- ) {
				*keyPtr++ = *--fileKeyPtr;				/* Store a bad block key */
#ifdef CHECK_BAD_BLOCKS_MAX
				if( (--count) == 0 ) {
					Error(ERR_TOO_MANY_BAD);			/* Oops, too many bad blocks */
					success = FALSE;						/* Exit routine. */
					break;
				}
#endif
			}
			if( success )  {
				if( ((key = fileHdrEndPtr->Ext) == 0) ) {
					*keyPtr = 0;								/* Terminate list */
/*					badBlockStats.BadBlocksPtr = keyPtr;	*/
					break;
				} else {
					success = ReadControl(key, &buf) == CERR_NOERR;
				}
			}
		}
	}
	return(success);
}

/*
 * Builds a special "bad.blocks" bitmap for use by any program wishing to avoid
 * bad blocks on the disk.
 */
 
BOOL BuildBadBlockBitMap()
{
	BOOL success;
	register ULONG *keyPtr;
	register ULONG temp;
	register WORD count = currVolParams.MaxBadBlocks;	

	if( success = AllocateBadBitMap() ) {		/* Init an empty bitmap for bad keys */
		keyPtr = badBlockBuffer;

#ifdef CHECK_BAD_BLOCKS_MAX
		while( count-- ) {
			if( temp = *keyPtr++ ) {
				MarkBadBusy(temp);					/* Mark busy in bad bitmap */
			} else {
				break;									/* End of list reached */
			}
		}
#else
		while( temp = *keyPtr++ ) {
			MarkBadBusy(temp);
		}
#endif

	}
	return(success);
}

/*
 * Adds key passed to existing bad blocks list. First checks if key is already
 * in the list. If not, adds to list and counts as new.
 */
 
BOOL AddBadKey(ULONG key)
{
	register ULONG *keyPtr;
	register BOOL success = FALSE;
	register WORD count = currVolParams.MaxBadBlocks;
	
	keyPtr = badBlockBuffer;

#ifdef CHECK_BAD_BLOCKS_MAX
	while( count-- ) {
		if( !(success = *keyPtr == NULL) ) {	/* At end of list yet? */
			if( *keyPtr == key ) {					/* Found a matching entry? */
				count = 0;								/* Yes, so not a new key. Abort. */
			} else {
				++keyPtr;								/* Nope, so try next */
			}
		} else {	
			break;									/* No match in list, a success. */
		}
	}
#else
	while( !(success = *keyPtr == NULL) ) {
		if( *keyPtr == key ) {			
			break;									/* Found a matching key. Abort. */
		} else {
			++keyPtr;							
		}
	}
#endif
	if( success ) {
		*keyPtr++ = key;								/* Add new key to list */
		*keyPtr = 0;									/* Terminate list */
		++badBlockStats.NewBadCount;				/* Count this new key */
		if( !GetKeyStatus(key) ) {					/* Is this key in use? */
			++badBlockStats.InUseCount;			/* Yes, so count as "in use" */
		}
	}
	return(success);
}

/*
 * Marks blocks in "BadBlockBuffer" as busy in the bitmap, and enters them into
 * "bad.blocks" fileheader. If there is no existing "bad.blocks" file, one is
 * created at this time. Then it rewrites the bitmap and "bad.blocks" file header,
 * plus extensions, as necessary.
 */
 
BOOL MarkBadBlocks()
{
	register BOOL success = TRUE;
	register WORD count;
 	register ULONG *keyPtr;
	register ULONG *fileKeyPtr;
	FileHdrPtr fileHdrPtr;
	FileHdrEndPtr fileHdrEndPtr;
	register ULONG temp;
	register ULONG curKey;
	ULONG newKey;
	BufCtrlPtr buf;
	
	MarkBadBlocksBusy();
	keyPtr = badBlockBuffer;
	if( !badBlockStats.BadBlocksKey ) {
		success = RawCreateFile(badBlockBSTR, currVolParams.RootBlock, &badBlockStats.BadBlocksKey, &buf);
	}
	if( success ) {
		CountFreeKeys();
		curKey = badBlockStats.BadBlocksKey;
		while( success &= (ReadControl(curKey, &buf) == CERR_NOERR) ) {
			buf->DirtyFlag = TRUE;
			fileHdrPtr = (FileHdrPtr) (buf+1);
			fileHdrPtr->BlkCnt = 0;							/* Start with no bad blocks */
			fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
			fileKeyPtr = &fileHdrEndPtr->Reserved;		/* One above "BlkPtr1" */
			count = currVolParams.HashTableSize;		/* Can store this many */
			if( badBlockBuffer == keyPtr ) {
				fileHdrPtr->FirstBlk = *keyPtr;
				fileHdrEndPtr->Protect = FIBF_READ | FIBF_WRITE | FIBF_EXECUTE | FIBF_DELETE;
				fileHdrEndPtr->FileSize = (badBlockStats.NewBadCount /*- badBlockStats.InUseCount*/) * currVolParams.Info.id_BytesPerBlock;
			}
			while( count-- ) {
				if( temp = *keyPtr++ ) {
					*--fileKeyPtr = temp;
					++fileHdrPtr->BlkCnt;
				} else {
					goto MBBEx;
				}
			}
			if( *keyPtr ) {
				if( (newKey = fileHdrEndPtr->Ext) == NULL ) {
					if( success = AllocFHKey() ) {
						newKey = badBlockStats.BadBlocksKey;
						success = BuildExtHdr(curKey, &newKey, &buf, currVolParams.LastUsedKey);
					} else {
						Error(ERR_BB_WRITE);
					}
				}
				curKey = newKey;
			} else {
				break;
			}
		}
MBBEx:if( success ) {
			if( WriteCache() == CERR_NOERR ) {
				WriteBitMap();		/* Write out updated bitmap */
			} else {
				ExplainDialog(BIG_ERR_MARK_FAIL);
			}
		}
	}
	return(success);		
}

/*
 * Tries to open file name "bstr" with directory key passed.
 * Returns TRUE if successful, and key of matching file header and its pointer.
 */
 
BOOL RawOpenFile(UBYTE *bstr, ULONG key, ULONG *matchKey, BufCtrlPtr *buffer)
{
	register BOOL success;
	register WORD count;
	register BOOL found;
	register ULONG *hashEntry;
	register ULONG hash;
	register FileHdrEndPtr fileHdrEndPtr;
	BufCtrlPtr buf;
	
	if( success = !ReadCtrlBlock(key, &buf, TRUE) ) {	/* Get parent block */
		count = LOOP_LIMIT;
		hashEntry = (ULONG *) (((UBYTE *)buf) + (sizeof(BufCtrl) + sizeof(RootBlock)) + CalcHash(bstr));
		found = FALSE;
		do {
			if( success = ((hash = *hashEntry) != NULL) && !ReadCtrlBlock(hash, buffer, TRUE) ) {
				fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)((*buffer)+1)) + currVolParams.BlockSize - sizeof(FileHdrEnd));
				if( found = CmpString(bstr, fileHdrEndPtr->FileName, *bstr, fileHdrEndPtr->FileName[0], FALSE) == 0 ) {
					*matchKey = hash;
				} else {
					hashEntry = &fileHdrEndPtr->HashChain;
				}
				success &= (count != 0);
				count--;
			}
		} while( success && !found );
	}
	return(success);
}

/*
 * Adds file whose name string is "str" to parent directory "dirKey".
 * NOTE: New files are added to collision chain in sorted order, as in FFS.
 * DOES NOT write filehdr or parent dir blocks to disk.
 * Returns TRUE if successful, and key of new filehdr and its pointer.
 */

BOOL RawCreateFile(TextPtr str, register ULONG parentKey, register ULONG *newKey, BufCtrlPtr *buffer)
{
	register BOOL success = FALSE;
	BufCtrlPtr buf;
	CacheHdrPtr cacheHdrPtr;
	
	if( BuildFileHdr(str, buffer) ) {	   			/* Init the hdr buffer */
		if( AllocFHKey() ) {				   				/* Get free block for file hdr */
			*newKey = ((FileHdrPtr) ((*buffer)+1))->Ownkey = currVolParams.LastUsedKey;
			LinkCacheKey(*newKey, BLOCK_CONTROL, *buffer);
			success = LinkParentDir(*newKey, parentKey);	/* Link into parent hash tbl */
/*
	If DCFS, calibrate creation with cached entry.
*/
			if( currVolParams.DirCache && (ReadControl(parentKey, &buf) == CERR_NOERR) ) {
				parentKey = *(((ULONG *)(buf+1)) + currVolParams.BlockSizeL-2);
				while( (parentKey) && CheckKey(parentKey) && (ReadControl(parentKey, &buf) == CERR_NOERR) ) {
					buf->DirtyFlag = TRUE;
					cacheHdrPtr = (CacheHdrPtr)(buf+1);
					cacheHdrPtr->Type = T_DIRCACHE;		/* Force validation */
					parentKey = cacheHdrPtr->NextBlock;	/* Validate all blocks */
				}
				WriteCache();
				currVolParams.ValidateVol = TRUE;
			}
		} else {
			Error(ERR_BB_WRITE);
			PutCacheBuffer(*buffer);						/* Disk full (or bitmap invalid?) */
		}
	}
	return(success);
}

/*
 * Unlinks file/dir key passed from parent key passed. Frees active blocks in
 * bitmap, including the filehdr block, extension blocks, if any, and data
 * blocks. Also unlinks from parent's hash table and any collision hdr.
 * Returns TRUE if file/dir unlinked from parent successfully.
 */
 
BOOL RawDeleteFile(ULONG fileKey, register ULONG parentKey)
{
	BOOL success;
	BufCtrlPtr buf;
	CacheHdrPtr cacheHdrPtr;
	
	if( success = UnlinkParentDir(fileKey, parentKey) ) {
/*
	If DCFS, calibrate deletion with cached entry.
*/
		if( currVolParams.DirCache && (ReadControl(parentKey, &buf) == CERR_NOERR) ) {				
			parentKey = *(((ULONG *)(buf+1)) + currVolParams.BlockSizeL-2);
			while( (parentKey) && CheckKey(parentKey) && (ReadControl(parentKey, &buf) == CERR_NOERR) ) {
				buf->DirtyFlag = TRUE;
				cacheHdrPtr = (CacheHdrPtr)(buf+1);
				cacheHdrPtr->Type = T_DIRCACHE;		/* Force validation */
				parentKey = cacheHdrPtr->NextBlock;	/* Validate all blocks of this dir */
			}
			WriteCache();
			currVolParams.ValidateVol = TRUE;
		}
		(void) MarkFileBlocks(fileKey, TRUE);
	}
	return(success);
}

/*
 * Allocate free file hdr block in bitmap. Starts at root block and moves
 * higher. Returns TRUE if a block was found (block is in LastUsedKey).
 */
 
BOOL AllocFHKey()
{
	currVolParams.LastUsedKey = currVolParams.RootBlock;
	return( AllocFreeKey() );
}

/*
 * From the link filename passed, return pointer to its true name.
 * NOTE: Because a user could see the file from a network and not
 * be running OS V2.0.4 or later, we'd better check the OS version
 * and not stuff destination with anything if links not supported.
 * Must be fullpath name in "fileName".
 */

/* 
void GetLinkName(TextPtr fileName, TextPtr dest, BOOL soft)
{
	register TextPtr str = fileName;
	BPTR lock;
	register struct DevProc *devProcPtr;
	
	if( LibraryVersion((struct Library *)DOSBase) >= OSVERSION_2_0_4 ) {
		if( soft ) {
			devProcPtr = GetDeviceProc(str, NULL);
			if( IoErr() == ERROR_OBJECT_NOT_FOUND && devProcPtr->dvp_Flags & DVPF_ASSIGN ) {
				while( (devProcPtr = GetDeviceProc(str, devProcPtr)) == NULL);
			}
			if( devProcPtr != NULL ) {
				strSavePath[0] = 0;
				ReadLink( devProcPtr->dvp_Port, devProcPtr->dvp_Lock, 
					str, strSavePath, PATHSIZE);
				str = strSavePath;
				FreeDeviceProc(devProcPtr);
			}
		} else {
			if( lock = Lock(str, ACCESS_READ) ) {
				if( NameFromLock(lock, str, PATHSIZE) == 0 ) {
					*str = 0;
				} else {
					while( *str++ != ':' );
				}
				UnLock(lock);
			} else {
				*str = 0;
			}
		}
	}
	strcpy(dest, str);
}
*/
