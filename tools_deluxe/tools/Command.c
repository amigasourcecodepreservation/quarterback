/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Command parser
 */

#include <exec/types.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/intuition.h>

#include <rexx/errors.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/DOS.h>

#include <string.h>

#include "Tools.h"
#include "Proto.h"
#include "Version.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;

extern ScreenPtr	screen;

extern WindowPtr	cmdWindow;

extern TextChar	strBuff[], strProgName[];

extern DlgTemplPtr	dlgList[];

extern BOOL			force;
extern WORD			mode, action, stage, scriptError;

/*
 *	Local variables and definitions
 */


#define USERREQ_EDIT	2
#define USERREQ_TEXT	3

#define MENUCMD_NULL					MENUITEM(NOMENU, NOITEM, NOSUB)
#define MENUCMD_QUIT					MENUITEM(PROJECT_MENU, QUIT_ITEM, NOSUB)
#define MENUCMD_SAVEAS				MENUITEM(PROJECT_MENU, SAVEAS_ITEM, NOSUB)
#define MENUCMD_PREFS				MENUITEM(PROJECT_MENU, PREFS_ITEM, NOSUB)
#define MENUCMD_SAVEASDEFAULTS	MENUITEM(PROJECT_MENU, SETTINGS_ITEM, SAVEASSETTINGS_SUBITEM)
#define MENUCMD_LOADDEFAULTS		MENUITEM(PROJECT_MENU, SETTINGS_ITEM, LOADSETTINGS_SUBITEM)

/*
 * AREXX force options
 */

#define NUM_FORCE_OPTIONS		(sizeof(forceNames)/sizeof(TextPtr))

static TextPtr forceNames[] = {
	"Force"
};

typedef struct MenuCmdList {
	TextPtr	Name;
	UWORD		Num;
} MenuCmdList;

static MenuCmdList menuCmds[] = {
	{ "About",				MENUITEM(PROJECT_MENU, ABOUT_ITEM, 		NOSUB) },
	{ "SaveAs",				MENUITEM(PROJECT_MENU, SAVEAS_ITEM,		NOSUB) },
	{ "PageSetup",			MENUITEM(PROJECT_MENU, PAGESETUP_ITEM,	NOSUB) },
	{ "Print",				MENUITEM(PROJECT_MENU, PRINT_ITEM, 		NOSUB) },
	{ "SaveSettings",			MENUITEM(PROJECT_MENU, SETTINGS_ITEM,	SAVESETTINGS_SUBITEM) },
	{ "LoadSettings",			MENUITEM(PROJECT_MENU, SETTINGS_ITEM,	LOADSETTINGS_SUBITEM) },
	{ "Preferences",			MENUITEM(PROJECT_MENU, PREFS_ITEM,		NOSUB) },
	{ "Quit",				MENUITEM(PROJECT_MENU, QUIT_ITEM,		NOSUB) },
	
	{ "Repair",				MENUITEM(TOOLS_MENU,	  VOL_REPAIR_ITEM,	NOSUB) },
	{ "Recover",			MENUITEM(TOOLS_MENU,	  VOL_RECOVER_ITEM,	NOSUB) },
	{ "Optimize",			MENUITEM(TOOLS_MENU,	  VOL_OPTIMIZE_ITEM,	NOSUB) },
	{ "Edit",				MENUITEM(TOOLS_MENU,	  VOL_EDITOR_ITEM,	NOSUB) },
	{ "GetVolumeInfo",		MENUITEM(TOOLS_MENU,   VOL_STAT_ITEM,	NOSUB) },
	{ "GetDeviceInfo",		MENUITEM(TOOLS_MENU,	  VOL_STAT_ITEM2,	NOSUB) },
};

#define NUM_MENU_COMMANDS	(sizeof(menuCmds)/sizeof(MenuCmdList))
#define NUM_MISC_COMMANDS	(sizeof(miscCmdNames)/sizeof(TextPtr))

static TextPtr miscCmdNames[] = {
	"ProgName",		"ProgVersion",
	"ScreenToFront",	"ScreenToBack",
	"SetPrint", 	"PrintOne",
	"SetVolume",	"SetDevice", 		"Start",		"Tools",
	"RepairOptions",	"RecoverOptions", "OptimizeOptions",
	"Request1",		"Request2",			"Request3",	"RequestText", "SetPath",
};

enum {
	CMD_PROGNAME,	CMD_PROGVERSION,
	CMD_SCREENFRONT, 	CMD_SCREENBACK,
	CMD_SETPRINT,	CMD_PRINTONE,
	CMD_SETVOLUME,	CMD_SETDEVICE,		CMD_STARTOP,	CMD_ENDOP,
	CMD_SETREPAIROPT,	CMD_SETRECOVEROPT,CMD_SETOPTIMIZEOPT,
	CMD_REQUEST1,	CMD_REQUEST2,		CMD_REQUEST3,	CMD_REQUESTTEXT, CMD_SETPATH
};

/*
 *	Local prototypes
 */

static LONG	UserRequest(WORD, TextPtr, LONG *);
static BOOL	MatchCommand(TextPtr, UWORD, TextPtr);
static LONG DoMenuCommand(WindowPtr, UWORD, TextPtr, UWORD);
static BOOL CheckForForce(TextPtr);


/*
 *	Check for a match to a command string
 *	Return TRUE if equal
 */

static BOOL MatchCommand(TextPtr cmdText, UWORD cmdLen, TextPtr text)
{
	return( CmpString(cmdText, text, cmdLen, (WORD) strlen(text), FALSE) == 0 );
}

/*
 * Get next word from script
 */
 
void GetNextWord(register TextPtr text, TextPtr word, UWORD *length)
{
	register UWORD len;
	register UWORD start = 0;
	register UBYTE endChar;

	switch (text[0]) {
		case DQUOTE :
			endChar = DQUOTE;
			start = 1;
			break;
		case QUOTE :
			endChar = QUOTE;
			start = 1;
			break;
		default:
			endChar = ' ';
			break;
	}

	for( len = start; text[len] && text[len] != endChar; len++);

	BlockMove(&text[start], word, len-start);	/* set up first word */
	word[len-start] = '\0';

	if (text[len])
		for (len++;text[len] && text[len] == ' '; len++);

	*length = len;
}

/*
 * Process the macro command text
 */

LONG ProcessCommandText(register TextPtr cmdText, UWORD modifiers, BOOL resultEnabled, LONG *result2)
{ 
	register TextPtr argText;
	register WORD i, cmdLen, menuCmd;
	register LONG result = RC_OK;
	LONG (*func)(TextPtr) = NULL;
	Dir dir;
	
/*	menuCmd = -1; */
	*result2 = 0;

/*
	Get the command and data portions of argument
*/
	while (*cmdText == ' ')
		cmdText++;
	argText = cmdText;
	while (*argText && *argText != ' ')
		argText++;
	cmdLen = (UWORD) (argText - cmdText);
	while (*argText == ' ')
		argText++;

/*
	Check for menu commands
*/
	for (i = 0; i < NUM_MENU_COMMANDS; i++) {
		if (MatchCommand(cmdText, cmdLen, menuCmds[i].Name))
			break;
	}
/*
	Handle menu commands
*/
	if (i < NUM_MENU_COMMANDS) {
		menuCmd = menuCmds[i].Num;

		if (menuCmd == MENUCMD_NULL) {
			result = RC_FATAL;
		} else {
			result = DoMenuCommand(cmdWindow, menuCmd, argText, modifiers);
		}
	} else {
/*
	Check for non-menu commands
*/
		for (i = 0; i < NUM_MISC_COMMANDS; i++) {
			if (MatchCommand(cmdText, cmdLen, miscCmdNames[i]))
				break;
		}
		if (i >= NUM_MISC_COMMANDS) {
			result = RC_FATAL;
		} else {
/*
	Handle non-menu commands
*/
			switch (i) {
			case CMD_PROGNAME:
				if (!resultEnabled) {
					result = RC_FATAL;
				} else {
 					*result2 = (LONG) strProgName;
				}
				break;
			case CMD_PROGVERSION:
				if (!resultEnabled) {
					result = RC_FATAL;
				} else {
					NumToString((PROGRAM_VERSION >> 8) & 0xFF, strBuff);
					strcat(strBuff, ".");
					NumToString((PROGRAM_VERSION >> 4) & 0x0F, strBuff + strlen(strBuff));
					*result2 = (LONG) strBuff;
				}
				break;
			case CMD_SCREENFRONT:
				ScreenToFront(screen);
				break;
			case CMD_SCREENBACK:
				ScreenToBack(screen);
				break;
			case CMD_SETPRINT:
				func = SetPrintOption;
				break;
			case CMD_PRINTONE:
				if( !DoPrint(cmdWindow, 0, TRUE) ) {
					result = RC_ERROR;
				}
				break;
			case CMD_SETREPAIROPT:
				if( *argText ) {
					func = SetRepairOption;
				} else if( (mode != MODE_REPAIR) || !DoRepairOptions() ) {
					result = RC_ERROR;
				}
				break;
			case CMD_SETRECOVEROPT:
				if( *argText ) {
					func = SetRecoverOption;
				} else if( (mode != MODE_RECOVER) || !DoRecoverOptions() ) {
					result = RC_ERROR;
				}
				break;
			case CMD_SETOPTIMIZEOPT:
				if( *argText ) {
					func = SetOptimizeOption;
				} else if( (mode != MODE_OPTIMIZE) || !DoReorgOptions() ) {
					result = RC_ERROR;
				}
				break;
			case CMD_SETVOLUME:
			case CMD_SETDEVICE:
				if( !ProcessVolDevGadget(i == CMD_SETDEVICE, argText) ) {
					result = RC_ERROR;
				}
				break;
			case CMD_STARTOP:
				scriptError = 0;
				if( mode == MODE_REPAIR ) {
					if( (action == OPER_IN_PROG) || (action == OPER_PAUSE) || (!DoRepair()) ) {
						result = RC_ERROR;
					} else {
						result = scriptError;
					}
				} else if( mode == MODE_RECOVER ) {
					if( stage || (!DoRecoverStart()) ) {
						result = RC_ERROR;
					}
				} else if( mode == MODE_OPTIMIZE ) {
					if( (action != OPER_READY) || (!DoOptimize()) ) {
						result = RC_ERROR;
					}
				} else {
					result = RC_FATAL;
				}
				break;
			case CMD_ENDOP:
				ReturnToMain();
				break;
			case CMD_SETPATH:
				if( strlen(argText) == 0 || (dir = Lock(argText, ACCESS_READ)) == NULL) {
					result = RC_ERROR;
				} else {
					SetCurrentDir(dir);
					UnLock(dir);
				}
				break;
			case CMD_REQUEST1:
			case CMD_REQUEST2:
			case CMD_REQUEST3:
			case CMD_REQUESTTEXT:
				result = UserRequest(i, argText, result2);
				break;
			}
			if( func != NULL ) {
				result = (*func)(argText);
			}
		}
	}
/*
	Reply to message
*/
	force = FALSE;
	
	return(result);
}

/*
	Handle menu command, possibly a variation with parameters
*/

static LONG DoMenuCommand(WindowPtr cmdWindow, UWORD menuCmd, TextPtr argText, UWORD modifiers)
{
	LONG result = RC_OK;
	TextChar cmdBuff[256];
	UWORD len;
	
	if( *argText ) {
		GetNextWord(argText, cmdBuff, &len);
		switch(menuCmd) {
		case MENUCMD_SAVEAS:
			if( !SaveFileAs(cmdBuff) ) {
				result = RC_ERROR;
			}
			break;
		case MENUCMD_PREFS:
			result = SetPrefsOption(argText);
			break;
		case MENUCMD_QUIT:
			if( force = CheckForForce(argText) ) {
				break;
			}
		default:
			result = RC_WARN;
			break;
		}
	} else {
		result = DoMenu(cmdWindow, menuCmd, modifiers, FALSE) ? RC_OK : RC_ERROR;
	}
	return(result);
}

/*
 * Check for FORCE option
 */
 
static BOOL CheckForForce(TextPtr optName)
{
	BOOL found = FALSE;
	register WORD i;
	
	for( i = 0; i < NUM_FORCE_OPTIONS; i++ ) {
		if (CmpString(optName, forceNames[i], strlen(optName), (WORD) strlen(forceNames[i]), FALSE) == 0) {
			found = TRUE;
		}
	}
	return(found);
}

/*
 *	Handle user request dialogs
 */

static LONG UserRequest(WORD cmdNum, TextPtr prompt, LONG *result2)
{
	LONG result;
	register WORD dlgNum;
	DialogPtr dlg;

	switch(cmdNum) {
	case CMD_REQUEST1:
		dlgNum = DLG_USERREQ1;
		break;
	case CMD_REQUEST2:
		dlgNum = DLG_USERREQ2;
		break;
	case CMD_REQUEST3:
		dlgNum = DLG_USERREQ3;
		break;
	case CMD_REQUESTTEXT:
		dlgNum = DLG_USERREQTEXT;
		break;
	default:
		return(RC_FATAL);
	}
	dlgList[dlgNum]->Gadgets[USERREQ_TEXT].Info = prompt;
	BeginWait();
	if ((dlg = GetDialog(dlgList[dlgNum], screen, mainMsgPort)) == NULL) {
		Error(ERR_NO_MEM);
		EndWait();
		return(RC_ERROR);
	}
	OutlineOKButton(dlg);
	result = ModalDialog(mainMsgPort, dlg, DialogFilter);
	if (cmdNum == CMD_REQUESTTEXT && result == RC_OK ) {
		GetEditItemText(dlg->FirstGadget, USERREQ_EDIT, strBuff);
		*result2 = (LONG) strBuff;
	}
	DisposeDialog(dlg);
	EndWait();
	return(result);
}
