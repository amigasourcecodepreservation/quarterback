/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Error/Help report routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <dos/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/Image.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>

#include <typedefs.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	External variables
 */

/*extern struct RexxLib *RexxSysBase;*/

extern MsgPortPtr	mainMsgPort;
extern MsgPort		rexxMsgPort;
extern ScreenPtr	screen;
extern WindowPtr	cmdWindow, backWindow;

extern TextChar	strBuff[], screenTitle[];

extern ULONG		dialogVarNum;
extern TextPtr		dialogVarStr;
extern TextPtr		strsErrors[], bigStrsErrors[];
/*extern TextChar	strMemory[];*/
/*extern TextChar	strDemo[], strBadFile[];*/
extern TextChar	strDOSError[], strFileNotFound[], strDiskLocked[];
extern TextChar	strFileNoDelete[], strFileNoRead[], strFileBusy[];
extern TextChar	_strOK[], _strCancel[];

extern GadgetTemplate errorGadgets[], warnGadgets[], skipGadgets[], ignoreGadgets[];
extern ReqTemplPtr	reqList[];
extern DlgTemplPtr	dlgList[];
extern RequestPtr	requester;
extern BOOL			titleChanged, force, abortFlag;
extern WORD			mode;

/*
 *	Local variables
 */

#define ABOUT_CHIPMEM	1
#define ABOUT_FASTMEM	2
#define ABOUT_AREXX		3

/*
 * Copy source string to destination string, replacing "%" with the
 * dialog string variable
 */
 
void ParseDialogString(register TextPtr srcStr, register TextPtr destStr)
{
	register BOOL terminate = FALSE;
	register TextChar ch;
	TextChar buff[16];
	
	while( srcStr != NULL && !terminate ) {
		ch = *srcStr++;
		switch(ch) {
		case '~':
			NumToCommaString(dialogVarNum, buff);
			strcpy(destStr, buff);
			destStr += strlen(buff);
			break;
		case '%':
			if( dialogVarStr != NULL ) {
				strcpy(destStr, dialogVarStr);
				destStr += strlen(dialogVarStr);
			}
			break;
		case 0:
			terminate = ch == 0;
		default:
			*destStr++ = ch;
		}
	}
}

/*
 *	Return screen titles to default setting
 */

void FixTitle()
{
	SetWindowTitles(backWindow, (TextPtr) -1, screenTitle);
	SetWindowTitles(cmdWindow, (TextPtr) -1, screenTitle);
	titleChanged = FALSE;
}

/*
 * Interactive prompt to insert a disk into the drive.
 */
 
BOOL PromptForDisk()
{
	BOOL success;
	
	while( (!abortFlag) && (!(success = IsDiskPresent())) ) {
		abortFlag = WarningDialog(strsErrors[ERR_NO_DISK], BUTTON_CANCEL);
	}
	return(success);
}

/*
 * Interactive prompt to change the write-protect status to off.
 */
 
BOOL PromptForWriteProtected()
{
	BOOL success;
	
	while( (!abortFlag) && (!(success = PromptForDisk() && !IsWriteProtected())) ) {
		abortFlag = WarningDialog(strsErrors[ERR_WRITE_PROT], BUTTON_CANCEL);
	}
	return(success);
}

/*
 * If you run out of memory during an operation (repair/reorg/etc.), call this.
 * It aborts the operation by setting the "abortFlag", so don't call this if
 * you don't absolutely need to have the memory for the operation to continue.
 */
 
void OutOfMemory()
{
	ReportInfo(ERR_NO_MEM);
	abortFlag = TRUE;
}

/*
 * Report an invalid bitmap, since that too, can
 */
/*
 *	Display error message dialog
 *	If no memory for dialog, display error in title bar
 */

void Error(WORD errNum)
{
	if( errNum > ERR_MAX_ERROR ) {
		errNum = ERR_UNKNOWN;
	}
	WarningDialog(strsErrors[errNum], OK_BUTTON);
}

/*
 * Open dialog with the number of buttons ("lastButton-1"), up to two buttons.
 * Returns the button depressed, which is zero for OK_BUTTON.
 */
 
WORD WarningDialog(TextPtr textPtr, WORD lastButton)
{
	return( DoDialog(textPtr, lastButton, _strOK, _strCancel, NULL, FALSE) );
}

/*
 * Put up an informative dialog, the big kind, and just put up the OK button.
 */
 
void ExplainDialog(WORD id)
{
	DoDialog(bigStrsErrors[id], OK_BUTTON, _strOK, NULL, NULL, TRUE);
}

/*
 * First parse for any ~ or % characters.
 * Then call DoDialog().
 */
 
WORD DoDialog(TextPtr textPtr, register WORD lastButton, TextPtr b1, TextPtr b2, TextPtr b3, BOOL large)
{
	TextChar buff[300];
	
	ParseDialogString(textPtr, buff);
	return( DoNonParseDialog(buff, lastButton, b1, b2, b3, NULL, large) );
}

/*
 * Open dialog and be able to specify the button text for up to three buttons.
 */
 
WORD DoNonParseDialog(TextPtr textPtr, register WORD lastButton, TextPtr button1, TextPtr button2, TextPtr button3, TextPtr button4, BOOL large)
{
	register WORD item;
	register WORD id;
	register WORD len;
	register RequestPtr req;
	register GadgTemplPtr gadgTempl;
	WindowPtr window;
	BOOL isReq = mode != MODE_TOOLS;
	GadgetPtr gadgList;
	Ptr saveImage;
	
	if( force ) {
		return(OK_BUTTON);
	}
	len = strlen(textPtr);
	
	BeginWait();
	
	MotorOff();

	switch(lastButton) {
	default:
	case OK_BUTTON:
		gadgTempl = errorGadgets;
		break;
	case CANCEL_BUTTON:
		gadgTempl = warnGadgets;
		gadgTempl[CANCEL_BUTTON].Info = button2;
		break;
	case SKIP_BUTTON:
		gadgTempl = skipGadgets;
		gadgTempl[CANCEL_BUTTON].Info = button2;
		gadgTempl[SKIP_BUTTON].Info = button3;
		break;
	case SKIPFILE_BUTTON:
		gadgTempl = ignoreGadgets;
		gadgTempl[CANCEL_BUTTON].Info = button2;
		gadgTempl[SKIP_BUTTON].Info = button3;
		gadgTempl[SKIPFILE_BUTTON].Info = button4;
	}
	gadgTempl[OK_BUTTON].Info = button1;
/*
	Assumes that there will be no two buttons with the same first letter! Make sure
	this convention is adhered to, or you'll end up with no key equivalent.
*/
	for( item = OK_BUTTON ; item <= lastButton ; item++ ) {
		gadgTempl[item].KeyEquiv = ((UBYTE *)gadgTempl[item].Info)[0];
	}
	if( isReq ) {
		id = large ? REQ_BIG_ERROR : REQ_ERROR;
		reqList[id]->Gadgets = gadgTempl;
		if( item = (req = DoGetRequest(id)) != NULL ) {
			window = cmdWindow;
			gadgList = req->ReqGadget;
		}
	} else {
		id = large ? DLG_BIG_ERROR : DLG_ERROR;
		dlgList[id]->Gadgets = gadgTempl;
		req = NULL;
		if( item = (window = GetDialog(dlgList[id], screen, mainMsgPort)) != NULL ) {
			gadgList = window->FirstGadget;
		}
	}
	requester = req;
	saveImage = gadgTempl[lastButton+2].Info;
	gadgTempl[lastButton+2].Info = (Ptr) ((len && (textPtr[len-1] == '?')) ? IMAGE_ICON_CAUTION : IMAGE_ICON_STOP);

	if( item ) {
		SetGadgetItemText(gadgList, lastButton+1, window, req, textPtr);
		OutlineOKButton(window);
		ErrBeep();
		FlushMainPort();
		while( (item = ModalRequest(mainMsgPort, window, DialogFilter)) == -1);
		if( req ) {
			DestroyRequest(req);
		} else {
			DisposeDialog(window);
		}
	} else {
/*
	Make sure no line feeds are present in the string anymore.
*/
		strcpy(strBuff, textPtr);
		textPtr = strBuff;
		while( (*textPtr) && (*textPtr != '\n') ) {
			textPtr++;
		}
		*textPtr = '\0';
		SetWindowTitles(backWindow, (TextPtr) -1, strBuff);
		SetWindowTitles(cmdWindow, (TextPtr) -1, strBuff);
		ErrBeep();
		titleChanged = TRUE;
		item = lastButton;		/* Skip if you can, else cancel if you can, else OK */
	}
	gadgTempl[++lastButton].Info = NULL;
	gadgTempl[++lastButton].Info = saveImage;
	
	EndWait();
	
	return(item);
}

/*
 *	Convert DOS error code to text string.
 */

TextPtr DOSErrorText(LONG dosError)
{
	register TextPtr errText;
	
	switch (dosError) {
	/*
	case -2:
		errText = strDemo;
		break;
	case -1:
		errText = strBadFile;
		break;
	*/
	case 0:
	case ERROR_NO_FREE_STORE:
		errText = strsErrors[ERR_NO_MEM];
		break;
	case ERROR_OBJECT_NOT_FOUND:
	case ERROR_DIR_NOT_FOUND:
	case ERROR_DEVICE_NOT_MOUNTED:
	case ERROR_OBJECT_WRONG_TYPE:
		errText = strFileNotFound;
		break;
	case ERROR_DELETE_PROTECTED:
		errText = strFileNoDelete;
		break;
	case ERROR_DISK_WRITE_PROTECTED:
		errText = strDiskLocked;
		break;
	case ERROR_READ_PROTECTED:
		errText = strFileNoRead;
		break;
	case ERROR_OBJECT_IN_USE:
		errText = strFileBusy;
		break;
	/*
	case ERROR_DISK_FULL:
		errText = strDiskFull;
		break;
	*/
	default:
		if (dosError >= 0 && dosError <= 999) {
			errText = strBuff;
			dialogVarNum = dosError;
			ParseDialogString(strDOSError, errText);
		} else {
			errText = strsErrors[ERR_UNKNOWN];
		}
		break;
	}
	
	return(errText);
}

/*
 * Attach DOS error text to a given error string, and display dialog.
 */
 
void DOSError(WORD errNum, LONG dosError)
{
	TextChar buff[100];
	
	strcpy(buff, strsErrors[errNum]);
	strcat(buff, DOSErrorText(dosError));
	(void) WarningDialog(buff, OK_BUTTON);
}

#ifdef BLAH
/*
 *	Show information dialog with number parameter
 *	Replace first '#' character with actual number value
 */

void InfoDialog(TextPtr text, WORD num)
{
	register WORD i;
	TextChar buff[50];

	for (i = 0; *text && *text != '#'; i++)
		buff[i] = *text++;
	if (*text == '#') {
		NumToString(num, buff + i);
		strcat(buff, ++text);
	}
	else
		buff[i] = '\0';
	reqList[REQ_ERROR]->Gadgets[ERROR_TEXT].Info = buff;
	reqList[REQ_ERROR]->Gadgets[ERROR_ICON].Info = (Ptr) IMAGE_ICON_NOTE;
	(void) StdDialog(DLG_ERROR);
	reqList[REQ_ERROR]->Gadgets[ERROR_ICON].Info = (Ptr) IMAGE_ICON_STOP;
}
#endif

/*
 *	Display help dialog in specified window
 *	If cannot show full help dialog, just show memory info
 */

void DoHelp()
{
	/*
	DialogPtr dlg;
	TextChar chipMem[10], fastMem[10];

	NumToString(AvailMem(MEMF_CHIP)/1024, chipMem);
	NumToString(AvailMem(MEMF_FAST)/1024, fastMem);
	strcat(chipMem, "K");
	strcat(fastMem, "K");
	dlgList[DLG_ABOUT]->Gadgets[ABOUT_CHIPMEM].Info =
		dlgList[DLG_MEMORY]->Gadgets[ABOUT_CHIPMEM].Info = chipMem;
	dlgList[DLG_ABOUT]->Gadgets[ABOUT_FASTMEM].Info =
		dlgList[DLG_MEMORY]->Gadgets[ABOUT_FASTMEM].Info = fastMem;
	dlgList[DLG_ABOUT]->Gadgets[ABOUT_AREXX].Info =
		(RexxSysBase) ? rexxMsgPort.mp_Node.ln_Name : "----";
	if ((dlg = GetDialog(dlgList[DLG_ABOUT], screen, mainMsgPort)) == NULL &&
		(dlg = GetDialog(dlgList[DLG_MEMORY], screen, mainMsgPort)) == NULL)
		Error(ERR_NO_MEM);
	else {
		OutlineOKButton(dlg);
		(void) ModalDialog(mainMsgPort, dlg, DialogFilter);
		DisposeDialog(dlg);
	}
	*/
	DoAboutBox();
}

#ifdef CHECK_MEM
/*
 *	Check remaining memory and give warning if getting low
 */
 
void CheckMemory()
{
	if (AvailMem(0) < 0x3FFFL) {
		SetWindowTitles(backWindow, (TextPtr) -1, strMemory);
		SetWindowTitles(cmdWindow, (TextPtr) -1, strMemory);
		StdBeep();
	}
}

#endif