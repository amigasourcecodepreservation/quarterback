/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Global Stuff
 */


#ifndef DEVICES_PRINTER_H
#include <devices/printer.h>
#endif

#ifndef DOS_FILEHANDLER_H
#include <dos/filehandler.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

#ifndef TOOLBOX_LANGUAGE_H
#include <Toolbox/Language.h>
#endif

#include "Print.h"

#define MAX_TRACK_BUFS	100

#define vertSpacing 5
#define horizSpacing 10

#define MENUITEM(menu, item, sub)	\
	(SHIFTMENU(menu) | SHIFTITEM(item) | SHIFTSUB(sub))

/*
 *	Function key definition
 */

#define FKEY_MENU	0
#define FKEY_MACRO	1

#define BS			0x08
#define TAB			'\t'
#define REV_TAB_RAW	66
#define LF			'\n'
#define CR			13
#define ESC			0x1B

#define ABOX_BORDER		1

typedef struct {
	WORD	Type;		/* Menu equiv. or macro */
	Ptr		Data;		/* Menu item number or pointer to macro name */
} FKey;

#define INCLUDE_SECS		1

/*
 *	Icon numbers
 */

enum {
	ICON_TEXT,	ICON_PREFS,	ICON_FKEYS,	ICON_CAPTURE
};

/*
 *	Error numbers
 */

enum {
	INIT_NOSCREEN,	INIT_NOMEM,	INIT_ALREADY_UP,	INIT_NOWIN
};

enum {
	ERR_UNKNOWN,
	ERR_NO_MEM,
	ERR_SAVE,
	ERR_PRINT,
	ERR_ABORT,
	ERR_BAD_COPIES,
	ERR_PAGE_SIZE,
	ERR_NO_DISK,
	ERR_OUT_OF_RANGE,
	ERR_NO_DEV_INFO,
	ERR_ROOT_READ,
	ERR_TOO_MANY_BAD,
	ERR_WRITE_DIFF_DISK,
	ERR_WRITE_PROT,
	ERR_NO_DEVICE,
	ERR_BITMAP_READ,
	ERR_BITMAP_INVALID,
	ERR_FATAL_CACHE,
	ERR_DELETE_ERR,
	ERR_READ_BLOCK,
	ERR_WRITE_BLOCK,
	ERR_WRITE_BITMAP,
	ERR_BAD_BITMAP,
	ERR_BB_READ,
	ERR_BB_WRITE,
	ERR_CAPTURE_WRITE,
	ERR_FIND_FAIL,
	ERR_FIX_CHECKSUM,
	ERR_SAVE_BLOCK_WARN,
	ERR_UNKNOWN_DISK,
	ERR_NO_FS_PROMPT,
	ERR_NO_FS,
	ERR_BAD_RENAME,
	ERR_DIR_ALREADY_IN_LIST,
	ERR_FUTURE_FS,
	ERR_REORG_DATA_READ,
	ERR_CTRL_READ,
	ERR_CTRL_CKSUM,
	ERR_CTRL_TYPE,
	ERR_BAD_REMAP,
	ERR_BAD_DESTNAME,
	ERR_FILE_DEST,
	ERR_WRITE_DEST_FILE,
	ERR_NO_RECOVER_IN_PLACE,
	ERR_REORG_OK,
	ERR_PLEASE_VALIDATE,
	ERR_SAVE_PREFS,
	ERR_BB_FILE_EXISTS,
	ERR_BITMAP_WARN,
	ERR_NO_REXX,
	ERR_MACRO_FAIL,
	ERR_MAX_ERROR
};

/*
 * Huge error messages.
 */
 
enum {
	BIG_ERR_BB_IN_USE,
	BIG_ERR_REPAIR_WARN,
	BIG_ERR_NO_BLOCK_CHECK,
	BIG_ERR_ROOT_BAD,	
	BIG_ERR_REORG_WARN,
	BIG_ERR_REORG_BAD_BITMAP,
	BIG_ERR_REORG_VOL_TOO_FULL,
	BIG_ERR_REORG_BAD_BLOCK,
	BIG_ERR_REORG_BAD_DATA,
	BIG_ERR_REORG_MAYBE_BAD,
	BIG_ERR_REORG_FRAG_REPORT,
	BIG_ERR_REORG_REBOOT_NOW,
	BIG_ERR_REORG_FAILED,
	BIG_ERR_REORG_ABORT,
	BIG_ERR_RECOVER_LOST_DRAWERS,
	BIG_ERR_FILE_DAMAGED,
	BIG_ERR_READ_FILE_BLOCK,
	BIG_ERR_NO_READ_PRIV,
	BIG_ERR_NO_SAFE_AREA,
	BIG_ERR_MARK_FAIL,
};

/*
 *	Dialog IDs
 */

enum {
	DLG_ABOUT,
	DLG_CUSTOMMACRO,
	DLG_USERREQ1,
	DLG_USERREQ2,
	DLG_USERREQ3,
	DLG_USERREQTEXT,
	DLG_VOLINFO,
	DLG_ERROR,
	DLG_BIG_ERROR,
	DLG_PREFS,
};

/*
 * Requester IDs
 */
 
enum {
	REQ_PAGESETUP,
	REQ_PRINT,
	REQ_REPAIROPTS,
	REQ_FILESYSTEM,
	REQ_CANCELPRINT,
	REQ_NEXTPAGE,
	REQ_ERROR,
	REQ_BIG_ERROR,
	REQ_FIND,
	REQ_GOTO,
	REQ_HASH,
	REQ_FINDING,
	REQ_REORGOPTS,
	REQ_RECOVER,
	REQ_RECOVEROPTS,
	REQ_RENAMEFILE,
	REQ_SAVEBLOCKWARN,
};

/*
 * Repair errors
 */
 
enum {
	ANERR_HDR_EXTRANEOUS_DATA,
	ANERR_DRAWER_KEY_INVALID,
	ANERR_DRAWER_CHECKSUM,
	ANERR_BAD_ENTRY_TYPE,
	ANERR_DRAWER_ROOT,
	ANERR_ENTRY_CROSS_LINKED,
	ANERR_OWNKEY_MISMATCH,
	ANERR_DRAWER_DISK_ERR,
	ANERR_HASH_CHAIN_INVALID,
	ANERR_PARENTKEY_MISMATCH,
	ANERR_WRONG_HASH,
	ANERR_HASH_NOT_SORTED,
	ANERR_HDR_INVALID_NAME,
	ANERR_HDR_INVALID_COMMENT,
	ANERR_HDR_INVALID_DATE,
	ANERR_DATA_MISMATCH,
	ANERR_BAD_BLOCK_COUNT,
	ANERR_DATA_KEY_INVALID,
	ANERR_DATA_DISK_ERR,
	ANERR_DATA_CROSS_LINKED,
	ANERR_EXT_INVALID,
	ANERR_FILE_SIZE,
	ANERR_EXT_DISK_ERR,
	ANERR_EXT_CHECKSUM,
	ANERR_DATA_HDR,
	ANERR_EXT_BAD,
	ANERR_EXT_OWNKEY_MISMATCH,
	ANERR_EXT_PARENTKEY_MISMATCH,
	ANERR_EXT_CROSS_LINKED,
	ANERR_ROOT_DISK_ERR,
	ANERR_ROOT_CORRUPT,
	ANERR_BAD_BITMAP_KEY,
	ANERR_BITMAP_DISK_ERR,
	ANERR_BITMAP_CHECKSUM,
	ANERR_BAD_BITMAP_EXT_KEY,
	ANERR_BITMAP_EXT_DISK_ERR,
	ANERR_BITMAP_EXTRANEOUS_KEY,
	ANERR_BITMAP_EXTRANEOUS_EXT_KEY,
	ANERR_HASH_CROSS_LINKED,
	ANERR_BITMAP_INCORRECT,
	ANERR_DATA_CHECKSUM,				/* I added this- maybe bogus. */
	ANERR_ROOT_CHECKSUM,				/* In case of root block checksum error */
	ANERR_HARD_LINK_INVALID,		/* Link field contains invalid key */
	ANERR_SOFT_LINK_TOO_LONG,		/* Pathname of what soft link points to too long */
	ANERR_BAD_DIR_EXT_KEY,			/* Dir Cache prob- bogus ext key, so make another */
	ANERR_DIR_EXT_CROSS_LINKED,	/* Dir Cache prob- ext key already busy, make another */
	ANERR_DIR_EXT_DISK_ERR,			/* Dir Cache prob- couldn't read dir ext block */
	ANERR_DIR_EXT_OWNKEY_MISMATCH,/* Dir Cache prob- ownkey different, minor prob */
	ANERR_DIR_EXT_CHECKSUM,			/* Dir Cache prob- checksum bad, minor prob */
	ANERR_DIR_EXT_WRONG_PARENT,	/* Dir Cache prob- wrong parent */
	ANERR_DIR_EXT_GARBLED,			/* Dir Cache prob- something wrong with content */
	ANERR_FUTURE_DATE,				/* Date in future */
};

/*
 * Repair actions.
 */
 
enum {
	ANACT_NONE,
	ANACT_ENTRY_DELETED,
	ANACT_CHECKSUM_RECALC,
	ANACT_ERROR_FIXED,
	ANACT_RELINKED,
	ANACT_SORTED,
	ANACT_NAME_REPLACED,
	ANACT_COMMENT_DELETED,
	ANACT_DATE_RESET,
	ANACT_FILE_DELETED,
	ANACT_NONE_FILES_INACCESSIBLE,
	ANACT_KEY_ALLOCATED,
	ANACT_HASH_DELETED,
	ANACT_DATA_DELETED,
	ANACT_BITMAP_REDONE,
};

enum {
	STATUS_BAD, STATUS_GOOD, STATUS_WP
};

/*
 * Reorganization stages
 */
 
enum {
	STAGE_COUNT, STAGE_RESOLVING, STAGE_REMAP, STAGE_REORG, STAGE_DONE
};

/*
 * Cache error codes
 */
 
enum {
	CERR_NOERR = 0,
	CERR_FATAL,
	CERR_CHECKSUM,
	CERR_BADTYPE,
	CERR_IOERR,
};

enum {
	BLOCK_DIRCACHE = -3,
	BLOCK_BITMAP = -2,
	BLOCK_CONTROL = -1,
	BLOCK_DATA = 0,
	BLOCK_SPECIAL = 1,
};

#define CACHE_HASH_SIZE		512
#define CACHE_MODULO			((CACHE_HASH_SIZE >> 2) - 1)

/*
#define MAX_BAD_BLOCKS		1000
#define BAD_BLOCK_BUF_SIZE	((MAX_BAD_BLOCKS << 2) + 4)
*/

/*
 * Repair results for volume status.
 */
 
enum {
	VOL_GOOD,
	VOL_FIXED,
	VOL_PROBS,
	VOL_BAD
};

/*
 * Command keys
 */

#if (AMERICAN | BRITISH)
#define CMD_KEY_ABORT	'A'
#define CMD_KEY_PAUSE	'P'
#define CMD_KEY_RESUME	'R'
#define CMD_KEY_START	'S'
#define CMD_KEY_OK		'O'
#define CMD_KEY_CANCEL	'C'
#define CMD_KEY_SKIP		'S'
#define CMD_KEY_YES		'Y'
#define CMD_KEY_NO		'N'
#define CMD_KEY_ENTER	'E'
#define CMD_KEY_BACK		'B'
#define CMD_KEY_DISKS	'D'
#define CMD_KEY_NEXT		'N'
#define CMD_KEY_PREV		'P'
#define CMD_KEY_SAVE		'S'
#define CMD_KEY_PRINT	'P'
#define CMD_KEY_DEC		'D'
#define CMD_KEY_HEX		'H'
#define CMD_KEY_REREAD	'R'
#define CMD_KEY_WRITE	'W'
#define CMD_KEY_GO		'G'
#define CMD_KEY_TAG		'T'
#define CMD_KEY_UNTAG	'U'
#define CMD_KEY_BACK		'B'
#define CMD_KEY_ENTER	'E'
#define CMD_KEY_OPTIONS	'O'
#define CMD_KEY_PROCEED	'P'
#define CMD_KEY_STOP		'S'
#define CMD_KEY_STOP2	't'
#endif

#if GERMAN
#define STD_BUTTON_WIDTH		80
#define ERR_BUTTON_WIDTH		80
#define REPAIR_BUTTON_WIDTH	80
#else
#define STD_BUTTON_WIDTH		60
#define ERR_BUTTON_WIDTH		65
#define REPAIR_BUTTON_WIDTH	80
#endif

#define NUM_ICONS			4
#define TOOLS_ICON_WIDTH	48
#define TOOLS_ICON_HEIGHT	24
#define TOOLS_ICON_DEPTH	2
#define TOOLS_ICON_SIZE		((TOOLS_ICON_WIDTH >> 4) * TOOLS_ICON_HEIGHT * TOOLS_ICON_DEPTH)

#define MR_TOOLS_ICON_WIDTH	48
#define MR_TOOLS_ICON_HEIGHT	17
#define MR_TOOLS_ICON_DEPTH	2
#define MR_TOOLS_ICON_SIZE		((MR_TOOLS_ICON_WIDTH >> 4) * MR_TOOLS_ICON_HEIGHT * MR_TOOLS_ICON_DEPTH)

/*
 * Tools icons
 */
 
enum {
	TOOL_ICON_REPAIR, TOOL_ICON_RECOVER, TOOL_ICON_OPTIMIZE, TOOL_ICON_EDIT
};

/*
 *	Tools modes
 */

enum {
	MODE_REPAIR, MODE_RECOVER, MODE_OPTIMIZE, MODE_EDIT, MODE_TOOLS
};

/*
 * Main window gadget list
 */
 
enum {
	VOL_DEV_BORDER,	VOL_DEV_LABEL,
	BUTTON_REPAIR,	BUTTON_UNDELETE,	BUTTON_OPTIMIZE, BUTTON_EDIT,
	VOL_LIST,		VOL_UP_ARROW,		VOL_DOWN_ARROW,	VOL_PROP,
#ifdef POPUP_VOLDEV
	VOL_DEV_POPUP,
#else
	VOL_DEV_RAD1,	VOL_DEV_RAD2,
#endif
};

/*
 * Repair tool window gadget list
 */
 
enum {
	BUTTON_OK,		BUTTON_CANCEL,		BUTTON_OPTIONS,
	LEFT_ARROW,		RIGHT_ARROW,
	HORIZ_SCROLL,	SCROLL_LIST,
	UP_ARROW,		DOWN_ARROW,			VERT_SCROLL,
	HELP_TEXT,		LABEL_TEXT,			STATUS_TEXT,	PROGRESS_BAR,
};

/*
 * Undelete gadgets
 */
 
enum {
	UNDEL_BUTTON_OK,	UNDEL_BUTTON_ENTER,	UNDEL_BUTTON_BACK,
	UNDEL_BUTTON_OPTIONS = VERT_SCROLL+1,
	UNDEL_BUTTON_TAG,
	UNDEL_BUTTON_UNTAG,
	UNDEL_DEST_LABEL,
	UNDEL_DEST_POPUP,
	UNDEL_DEST_TEXT,
	UNDEL_PATHNAME_BORDER,
	UNDEL_FILES_BORDER,
	UNDEL_BYTES_BORDER,
};

/*
 * Edit gadgets
 */
 
enum {
	EDIT_BLOCK_LABEL,
	EDIT_BUTTON_REREAD,EDIT_BUTTON_WRITE,	EDIT_BUTTON_JUMP,
	EDIT_LEFT_ARROW,	EDIT_RIGHT_ARROW,
	EDIT_HORIZ_SCROLL,EDIT_SCROLL_LIST,
	EDIT_UP_ARROW,	EDIT_DOWN_ARROW,	EDIT_VERT_SCROLL, EDIT_POPUP_BASE,
	EDIT_TYPE_TEXT,	EDIT_FIELD_TEXT,	EDIT_TYPE_BORDER, EDIT_FIELD_BORDER,
};

/*
 * Reorganize window gadgets
 */
 
enum {
	ETA_BORDER = 1,
	BORDER_FRAGMENTATION = 3,
	FRAG_LABEL_TEXT,
	OPTIMIZE_BAR_GRAPH,
};

#define REORG_BORDER_WIDTH		512

/*
 * Requester button
 */
 
#define SKIP_BUTTON		2
#define SKIPFILE_BUTTON	3

/*
 *	Operations
 */

enum {
	OPER_READY, OPER_IN_PROG, OPER_PAUSE, OPER_DONE
};

/*
 * Print error codes
 */
 
#define PRINT_OK		0
#define PRINT_ABORT		1
#define PRINT_DONE		2
#define PRINT_ERROR		-1

#define NUM_STDCOLORS	4
#define MACRONAME_LEN	32		/* Customizable macro name length */

#define MAX(a,b) ((a)>(b)?(a):(b))
#define MIN(a,b) ((a)<(b)?(a):(b))

#if (AMERICAN | BRITISH)
#define MAX_ERR_HEIGHT	115
#elif (FRENCH | GERMAN)
#define MAX_ERR_HEIGHT	130
#endif

#define numModes 4

#define WINDOW_ZOOMED(x)	(x->Height <= (screen->BarHeight+1))
/*
 * Menu and menu item numbers
 */

enum {
	PROJECT_MENU,
	TOOLS_MENU,
	MACRO_MENU,
	EDIT_MENU,
};

enum {
	ABOUT_ITEM,
	SAVEAS_ITEM = 2,
	PAGESETUP_ITEM = 4,
	PRINT_ITEM,
	PREFS_ITEM = 7,
	SETTINGS_ITEM = 9,
	QUIT_ITEM = 11
};

enum {
	TOOLS_SELECT_ITEM,
	VOL_REPAIR_ITEM = 2,
	VOL_RECOVER_ITEM,
	VOL_OPTIMIZE_ITEM,
	VOL_EDITOR_ITEM,
	VOL_STAT_ITEM = 7,
	VOL_STAT_ITEM2 = 25,		/* Special number that's not really a menu item */
};

enum {
	OTHERMACRO_ITEM = 11,
	CUSTOMMACRO_ITEM = 13
};

enum {
	SAVESETTINGS_SUBITEM,
	SAVEASSETTINGS_SUBITEM,
	LOADSETTINGS_SUBITEM = 3
};

enum {
	FIND_ITEM,
	FIND_NEXT_ITEM,
	CALC_HASH_ITEM = 3,
	CALC_CKSUM_ITEM,
	AUTOCALC_CKSUM_ITEM,
	CAPTURE_CREATE_ITEM = 7,
	CAPTURE_WRITE_ITEM,
	CAPTURE_CLOSE_ITEM,
	GOTO_ITEM = 11,
	GOTO_PREV_ITEM,
	GOTO_FIELD_ITEM,
};

enum {
	GOTO_ROOT_SUBITEM,
	GOTO_BITMAP_SUBITEM,
	GOTO_HASH_SUBITEM = 3,
	GOTO_PARENT_SUBITEM,
	GOTO_EXT_SUBITEM,
	GOTO_HEADER_SUBITEM,
	GOTO_DATA_SUBITEM,
};

#define PATHSIZE 			512
#define MAX_FILENAME_LEN	31
#define MAX_COMMENT_LEN		81
#define SHIFTKEYS			(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)
#define ALTKEYS			(IEQUALIFIER_LALT | IEQUALIFIER_RALT)
#define CMDKEY				AMIGARIGHT

#define ID_PREFSFILE	0x51425400L
#define ID_FKEYSFILE	0x51425401L

#define ID_PC_DISK	0x4D534400L

#define QUOTE			'\''
#define QUOTE_OPEN		QUOTE
#define QUOTE_CLOSE		QUOTE
#define DQUOTE			'\"'
#define DQUOTE_OPEN		DQUOTE
#define DQUOTE_CLOSE	DQUOTE

#define WIDTH(x)	(((x)->MaxX - (x)->MinX)+1)
#define HEIGHT(x)	(((x)->MaxY - (x)->MinY)+1)

/*
 * Disk editor cursor modes
 */
 
enum {
	CMODE_NONE,	CMODE_HEX,	CMODE_ASCII
};

/*
 * Recovery mini-icon types
 */

#define NUM_MINI_ICONS	7

enum {
	DRAWER_ICON,	DOC_ICON,
	DRAWER_ICON_MASK,	DOC_ICON_MASK,	CHECK_ICON_MASK,
	DRAWER_ICON_GRAY,	DOC_ICON_GRAY
};

/*
 * Recovery destinations
 */
 
enum {
	DEST_SAME_VOL,	DEST_DIFF_VOL
};

/*
 * Tools-specific directory level structures
 */
 
/* 
	WARNING - keep first seven fields in same order.
   This is because some routines used in both Recover and Reorg.
*/
 
struct DirFib {
	struct DirFib *	df_Next;			/* link to next entry at this level */
	struct DirFib *	df_Parent;		/* link back to parent dir */
	struct DirFib *	df_Child;		/* link to lower level items or filehdr ext */
	ULONG				df_Ownkey;		/* file hdr or user dir key */
	struct DirFib *	df_CurFib;		/* for temporary use for whatever you like */
	UBYTE				df_Type;			/* See below */
	UBYTE				df_Flags;		/* See below */
	ULONG				df_Size;			/* Used for files only */
	UWORD				df_Date;			/* ds_Days */
	UWORD				df_Time;			/* (ds_Minute << 5) + bits 20-24 of ds_Tick */
	UBYTE				df_Percent;		/* Percent intact (maximum) */
	TextChar			df_Name;			/* VARIABLE SIZED RECORD!!! */
};

typedef struct DirFib DirFib, *DirFibPtr;

/* Redefinitions for filehdr entries */
#define	df_ExtHdr	df_Child

#define TYPE_DIR_MASK			128
#define TYPE_FILE_HDR_MASK		64
#define TYPE_HDR_EXT_MASK		32
#define TYPE_HARD_LINK_MASK	16
#define TYPE_SOFT_LINK_MASK	8
#define TYPE_CACHE_MASK			4

#define IS_DIR(x)	( (x & TYPE_DIR_MASK) && ((x & (TYPE_SOFT_LINK_MASK | TYPE_HARD_LINK_MASK)) == 0) )

#define FLAG_NEW_PARENT_MASK	128
#define FLAG_NOT_BUSY_MASK		64
#define FLAG_PART_BUSY_MASK	32
#define FLAG_DUMMY_MASK			16
#define FLAG_SEL_MASK			8
#define FLAG_PART_MASK			4
#define FLAG_BAD_BLOCK_MASK	2
#define FLAG_PROCESSED_MASK	1

/* 
	WARNING - keep first seven fields in same order.
   This is because some routines used in both Recover and Reorg.
*/

struct ReorgList {
	struct ReorgList *rl_Next;
	struct ReorgList *rl_Parent;
	struct ReorgList *rl_ExtHdr;
	ULONG				rl_Ownkey;
	ULONG				rl_Newkey;		/* assigned at start of reorg */
	UBYTE				rl_Type;
	UBYTE				rl_Flags;
	UWORD				rl_HashEntry;	/* parent's hash table entry (0 origin) */
	ULONG				rl_NewDatakey;	/* replaces rl_First during reorg */
	UWORD				rl_HBlks;		/* number of blocks in this hdr/ext */
};
	
typedef struct ReorgList ReorgList, *ReorgListPtr;
	
#define rl_Child	rl_ExtHdr
#define rl_LinkFrom	rl_NewDatakey

#define RTYPE_DIR_MASK			TYPE_DIR_MASK
#define RTYPE_FILE_HDR_MASK	TYPE_FILE_HDR_MASK
#define RTYPE_HDR_EXT_MASK		TYPE_HDR_EXT_MASK
#define RTYPE_HARD_LINK_MASK	TYPE_HARD_LINK_MASK
#define RTYPE_SOFT_LINK_MASK	TYPE_SOFT_LINK_MASK
#define RTYPE_CACHE_MASK		TYPE_CACHE_MASK

#define RFLAG_FILE_FRAG_MASK	128
#define RFLAG_HDR_FRAG_MASK	64
#define RFLAG_BUNDLE_MASK		32
#define RFLAG_DUMMY_MASK		16
#define RFLAG_WAIT_MASK			8
#define RFLAG_WRAPPED_MASK		4
#define RFLAG_DONT_MOVE_MASK	2
#define RFLAG_PROCESSED_MASK	1

enum {
	WB_OPTIMIZATION,	CLI_OPTIMIZATION
};

/*
 * Buffer control structure
 */
 
struct BufCtrl {
	struct BufCtrl	*Next;		/* These two fields MUST stay here... */
	struct BufCtrl	*Prev;		/* ...they are manipulated as a Node. */
	ULONG				HashChain;
	ULONG				Ownkey;
	ULONG				Parent;
	BOOL				DirtyFlag;
	WORD				BufType;
};

typedef struct BufCtrl BufCtrl, *BufCtrlPtr;

/*
 * ProcessDir() recursive variables made into non-recursive structure.
 */
 
typedef struct {
	struct MinNode	Node;
	ULONG				CurDirKey;
	ULONG				Offset;
	ULONG				KeyHashChain;
	ULONG				Counter;
	ULONG				HashChainPred;
	ULONG				HashChainSucc;
	BufCtrlPtr		CurDirBuf;
	BufCtrlPtr		CurEnt;
	BOOL				SortFlag;
} DirScanVars, *DirScanVarsPtr;
	
typedef struct {
	struct MinNode	Node;
	ULONG				Newkey;
	ReorgListPtr	CurrEntry;
} ReorgVars, *ReorgVarsPtr;

/*
 * Statistics for analyze/repair
 */
 
typedef struct {
	ULONG				MajorErrCount;
	ULONG				MinorErrCount;
	ULONG				TotalFileCount;
	ULONG				BadFileCount;
	ULONG				TotalLinkCount;	/* Hard and soft links */
	ULONG				BadLinkCount;		/* Hard and soft links that were bad, deleted */
	ULONG				TotalDirCount;
	ULONG				BadDirCount;
	ULONG				BadBlockCount;
	ULONG				CurrentBlockType;	/* Current block # or block type of current block */
	BYTE				BitMapErrFlag;		/* 1=Bitmap needs to be fixed */
	BYTE				VolBadFlag;			/* 1=Volume unusable (for stats) */
	BYTE				ExistingErr;		/* 0=No errors ignored, >0=minor, <0=major */
	BYTE				FileSkipped;		/* 0=Can repair bitmap because none skipped */
} RepairStats, *RepairStatsPtr;

typedef struct {
//	ULONG				*BadBlocksPtr;		/* Pointer to last entry in buffer */
	ULONG				BadBlocksKey;		/* Key for bad blocks file */
	ULONG				OldBadCount;		/* Count of old bad blocks */
	ULONG				NewBadCount;		/* Count of new bad blocks */
	ULONG				InUseCount;			/* Count of bad blocks already in use */
	BOOL				LoopDirFlag;		/* 1=Hash chain loop in a dir */
	BOOL				BadBlockFlag;		/* 1=There is a bad blocks file */
} BadBlockStats, *BadBlockStatsPtr;

/*
	Stats for recover
*/

typedef struct {
	ULONG				TotalFiles;
	ULONG				SelFiles;
	ULONG				MaxFiles;
	ULONG				DelFiles;
	ULONG				SelDelFiles;
	ULONG				SelActFiles;
	ULONG				SelDirs;
	WORD				PathLen;
	BOOL				TagsDone;
	DirFibPtr		DirTree;
	DirFibPtr		DummyTree;
	DirFibPtr		RecoverEntry;
	DirFibPtr		CurFib;
	DirFibPtr		SaveCurFib;
	DirFibPtr		ParentFib;
	ULONG				NextFreeKey;
	ULONG				NumAltKeys;
	ULONG				LostDirs;
} RecoverStats, *RecoverStatsPtr;

/*
	Stats for reorganization
*/

typedef struct {
	ULONG	KeyLimit;
	WORD	XSize;
	WORD	YSize;
	WORD	Shift;
	ULONG	(*GetKeyStatusFunc)(void);
	void	(*DrawKeyStatusFunc)(UWORD, ULONG);
} BitMapParams, *BitMapParamsPtr;

typedef struct {
	ULONG				NumFragments;
	ImagePtr			Image;
	ULONG				ImageSize;
	BitMapParamsPtr	BitMapTablePtr;
	ULONG				CurrBit;			/* All variables above are read-only after setup! */
	ULONG				CurrMask;
	ULONG				*CurrPtr;
	BOOL				BusyFragFlag;
	BOOL				MemKeysFlag;
	ULONG				ActiveFiles;
	ULONG				FragmentedFiles;
	ULONG				ActiveDirs;
	ULONG				ActiveLinks;
	ULONG				RemapBlocks;
	ULONG				BundleBlocks;
	ULONG				CacheBlocks;
	ULONG				ExtBlocks;
	ReorgListPtr	ExtList;
	ReorgListPtr	DirTree;
	ReorgListPtr	DummyTree;
	ULONG				LowestCtrlKey;
	ULONG				LowestFreeKey;
	ULONG				LowestDataKey;
	ULONG				CurrKey;
	ULONG				FinishedEntries;
} ReorgStats, *ReorgStatsPtr;

typedef struct {
	File				CaptureFH;
	BOOL				BlockBufferValid;
	BOOL				BlockBufferChanged;
	ULONG				BitMapKey;
	WORD				FindType;
	BOOL				FindWarn;
	BOOL				FindDirection;
	BOOL				FindRange;
	ULONG				FindStartBlock;
	ULONG				FindEndBlock;
	ULONG				FindCurrBlock;
	UWORD				FindStartOffset;
	UWORD				FindEndOffset;
	UWORD				FindCurrOffset;
	UWORD				FindLen;
	TextChar			FindString[100];
	TextChar			ASCIIString[100];
	TextChar			HexString[100];
	ULONG				LastGoToBlock;
	ULONG				HistoryBlock;
	UWORD				CursorMode;
	UWORD				CursorOffset;
	UWORD				CursorLX;
	UWORD				CursorLY;
	BOOL				CursorOn;
} EditStats, *EditStatsPtr;


typedef struct {
	WORD				PageWidth;
	WORD				PageHeight;
	UBYTE				MeasureUnit;
	UBYTE				Pad;
	PrintRecord 	PrintRec;
} PrintInfoRec, *PrintInfoPtr;

typedef struct {
	BOOL				SaveIcons;
	BOOL				Beep;
	BOOL				Flash;
	WORD				DecimalLabel;
	BOOL				BypassDOS;
	WORD				MemoryUsage;		/* Currently hardcoded to 10% */
	WORD				Interactive;		/* Zero = interactive, one = quiet */
} PrefsOptsRec, *PrefsOptsPtr;

typedef struct {
	Rectangle		WindowRect;
	BOOL				DoBadBlocks;
	BOOL				DoBadFiles;
	BOOL				Repair;				/* Analyze only or repair? */
	BOOL				BadBlockMode;		/* Zero = readable, one = readable/writable */
	BOOL				CheckFFSData;		/* Zero = skip data blocks, one = read them */
} RepairOptsRec, *RepairOptsPtr;

typedef struct {
	Rectangle		WindowRect;
	BOOL				ShowKeys;			/* Show block key number in catalog entries */
	BOOL				ShowDelOnly;		/* Deleted entries only? */
	BOOL				CreateDrawers;		/* Create scratch replacement drawers? */
	BOOL				Destination;		/* If non-zero, goes to different volume */
	TextChar			DestName[100];		/* Default name of different volume */
} RecoverOptsRec, *RecoverOptsPtr;

typedef struct {
	Rectangle		WindowRect;
	BOOL				Optimize;			/* Integrity check or actual reorganize? */
	WORD				OptimizeType;		/* CLI or WB? */
	BOOL				Buffer;				/* Buffer remap table in RAM? (NOT USED) */
} ReorgOptsRec, *ReorgOptsPtr;

typedef struct {
	Rectangle		WindowRect;
	BOOL				AutoChecksum;		/* Automatically adjust checksum on writes? */
} EditOptsRec, *EditOptsPtr;

typedef struct {
	TextChar			DefaultVolume[MAX_FILENAME_LEN+1];
	Rectangle		WindowRect;
	BOOL				Devices;				/* Volumes or devices in list? */
	PrefsOptsRec	PrefsOpts;
	RepairOptsRec	RepairOpts;
	RecoverOptsRec	RecoverOpts;
	ReorgOptsRec	ReorgOpts;
	EditOptsRec		EditOpts;
	PrintInfoRec	PrintInfo;
} OptionsRec, *OptionsPtr;

typedef struct {
	struct DosEnvec	Envec;
	ULONG				BlocksPerCyl;		/* Blocks per track multiplied by surfaces */
	ULONG				BlockSize;			/* Size of block in bytes */
	ULONG				BlockSizeL;			/* Block size in longwords */
	UWORD				BlockSizeShift;	/* Shift count to divide by block size */
	UWORD				BlockLongShift;	/* Shift count to divide by long size */
	ULONG				VolumeOffset;		/* Byte offset from start of physical device */
	ULONG				MaxCylinders;		/* Total cylinders on volume */
	ULONG				MaxBytes;			/* Volume (not disk!) size in bytes */
	ULONG				MaxBlocks;			/* Volume (not disk!) size in blocks */
	ULONG				FreeBytes;			/* Volume (not disk!) free space in bytes */
	ULONG				FreeBlocks;			/* Volume (not disk!) free space in blocks */
	ULONG				LowestKey;			/* Lowest valid key on volume */
	ULONG				HighestKey;			/* Highest valid key on volume */
	ULONG				MaxKeys;				/* Number of valid keys on volume */
	ULONG				FreeKeys;			/* Number of free keys on volume */
	ULONG				RootBlock;			/* Which block is the root? */
	ULONG				LastUsedKey;		/* Start search for free key here */
	UWORD				BitMapBlkBits;		/* Count of bits (blks) in one bitmap buf */
	UWORD				BitMapBlocks;		/* Count of bitmap blocks */
	UWORD				BitMapExtBlks;		/* Count of bitmap ext blocks */
	UWORD				HashTableSize;		/* Size in longwords = number of entries */
	ULONG				*BitMapBuf;			/* Bitmap buffer */
	ULONG				*BitMapAltBuf;		/* Alt-Bitmap buffer */
	ULONG				*BitMapBadBuf;		/* Bad-Bitmap buffer */
	ULONG				BitMapBufSize;		/* Bitmap buffer size for all three above. */
	ULONG				*BitMapKeyBuf;		/* Bitmap key buffer */
	ULONG				BitMapKeyBufSize;	/* Bitmap key buffer size */
	struct InfoData	Info;					/* Info() structure, MUST be long-alinged! */
	ULONG				DiskType;			/* Four-character DOS identifier in root */
	BOOL				DosDisk;				/* Does disk at least APPEAR to be AmigaDOS? */
	BOOL				NonFSDisk;			/* Is disk not to be treated as AmigaDOS? */
	BOOL				VolValid;			/* Is this valid volume (InfoData() valid)? */
	BOOL				International;		/* International case compare routines for hashing? */
	BOOL				DirCache;			/* Directory cache (V3.0 feature) */
	BOOL				FFSFlag;				/* Derived from the Envec.DOSType above. */
	WORD				PercentFull;		/* What percent of volume full? */
	TextChar			DeviceName[MAX_FILENAME_LEN+1];
	TextChar			ExecDevName[MAX_FILENAME_LEN+1];
	WORD				ExecUnit;			/* Unit to send OpenDevice() */
	ULONG				ExecFlags;			/* Flags to send OpenDevice() */
	BOOL				DeviceOpen;			/* Have we called OpenDevice() already? */
	BOOL				DeviceInhibited;	/* Is AmigaDOS already inhibited by us? */
	ULONG				DoneBlocks;			/* For bar graph only */
	ULONG				MaxBadBlocks;		/* Limit to bad blocks, for memory reasons */
	BOOL				IsTD;					/* Is trackdisk.device? */
	BOOL				IsTD525;				/* Is trackdisk.device and diskchange dumb? */
	ULONG				PrevCount;			/* Previous counter (for disk changes) */
	BOOL				ActiveLocks;		/* For reorganize, tells whether reboot needed */
	BOOL				PreventUninhibit;	/* Prevent AmigaDOS from coming back? */
	BOOL				BlockAccess;		/* Use block access only, not track */
	BOOL				ValidateVol;		/* Force validation flag on root block? */
} VolParams, *VolParamsPtr;
	
/*
 * Headers for root, dir, file, and fileext blocks. Access these normally.
 * The hash table follows these on root & dir, and to access this area,
 * simply add the size of RootBlock or DirBlock.
 */
 
typedef struct {
	ULONG				Type;
	ULONG				Ownkey;
	ULONG				Skip;
	ULONG				HashSize;
	ULONG				Skip2;
	ULONG				CkSum;
} RootBlock, *RootBlockPtr;

typedef struct {
	ULONG				Type;
	ULONG				Ownkey;
	ULONG				Skip;
	ULONG				Skip1;
	ULONG				Skip2;
	ULONG				CkSum;
} DirBlock, *DirBlockPtr;

typedef struct {
	ULONG				Type;
	ULONG				Ownkey;
	ULONG				BlkCnt;
	ULONG				Skip1;
	ULONG				FirstBlk;
	ULONG				CkSum;
} FileHdr, *FileHdrPtr;

typedef struct {
	ULONG				Type;
	ULONG				Ownkey;
	ULONG				BlkCnt;
	ULONG				Skip1;
	ULONG				Skip2;
	ULONG				CkSum;
} FileExt, *FileExtPtr;

typedef struct {
	ULONG				Type;
	ULONG				Ownkey;
	ULONG				Parent;
	ULONG				NumEntries;
	ULONG				NextBlock;
	ULONG				CkSum;
} CacheHdr, *CacheHdrPtr;

/*
 * The directory cache entries are implemented on the FS as:
 * ULONG				hdrKey;
 * ULONG				fileSize;
 * ULONG				protBits;
 * ULONG				ownerID;
 * WORD				dateStampDays;
 * WORD				dateStampMins;
 * WORD				dateStampTicks;
 * BYTE				entryType;
 * BSTR				fileName;
 * BSTR				fileComment;	// (a zero byte if no comment)
 */

typedef DirBlock SoftLinkHdr, *SoftLinkHdrPtr;
typedef DirBlock HardLinkHdr, *HardLinkHdrPtr;

/*
 * These are the root, dir, file, fileext structures that appear AFTER the
 * variable-sized hash table (if on root or dir blocks).
 * NOTE: These occur at the END of disk blocks, so to access any of these fields, 
 * you must add the block size and subtract the size of this structure from this.
 * To access a hash table, you should simply add the size of the BEFORE block, as
 * described above.
 * For soft links, the hash table is the pathname.
 */
 
typedef struct {
	ULONG				BMFlg;
	ULONG	 			BMPtrs[25];
	ULONG				BMExt;
	struct DateStamp	RMDate;
	TextChar			VolName[MAX_FILENAME_LEN+1];
	ULONG				Unknown[2];
	struct DateStamp	DMDate;
	struct DateStamp	CDate;
	ULONG				HashChain;			/* Should be zero */
	ULONG				Parent;				/* Should be zero */
	ULONG				Ext;					/* Zero in pre-DOS2 */
	LONG				SecType;
} RootBlockEnd, *RootBlockEndPtr;

typedef struct {
/* ULONG				BlkPtr1; */
	ULONG				Reserved;
	ULONG				OwnerXID;
	ULONG				Protect;
	ULONG				Skip;
	TextChar			Comment[92];
	struct DateStamp	MDate;
	TextChar			DirName[MAX_FILENAME_LEN+1];
	ULONG				Unknown[2];
	ULONG				HardLinkToMe;		/* Key of a hard link pointing to this key */
	ULONG				Skip1[5];
	ULONG				HashChain;
	ULONG				Parent;
	ULONG				Ext;					/* Used for DirCache File System */
	LONG				SecType;
} DirBlockEnd, *DirBlockEndPtr;

typedef struct {
	ULONG				Reserved;
	ULONG				OwnerXID;
	ULONG				Protect;
	ULONG				FileSize;
	TextChar			Comment[92];
	struct DateStamp	MDate;
	TextChar			FileName[MAX_FILENAME_LEN+1];
	ULONG				Unknown[2];
	ULONG				HardLinkToMe;		/* Key of a hard link pointing to this key */
	ULONG				Skip1[5];
	ULONG				HashChain;
	ULONG				Parent;
	ULONG				Ext;
	LONG				SecType;
} FileHdrEnd, *FileHdrEndPtr;

typedef struct {
	ULONG				Unused[46];
	ULONG				Skip;
	ULONG				Parent;
	ULONG				Ext;
	LONG				SecType;
} FileExtEnd, *FileExtEndPtr;

typedef struct {
	ULONG				Reserved[27];
	struct DateStamp	MDate;
	TextChar			LinkName[MAX_FILENAME_LEN+1];
	ULONG				Skip[2];
	ULONG				HardLinkToMe;
	ULONG				Skip1[5];
	ULONG				HashChain;
	ULONG				Parent;
	ULONG				Ext;
	ULONG				SecType;
} SoftLinkEnd, *SoftLinkEndPtr;

typedef struct {
	ULONG				Reserved;
	ULONG				OwnerXID;
	ULONG				Protect;
	ULONG				FileSize;
	TextChar			Comment[92];
	struct DateStamp	MDate;
	TextChar			LinkName[MAX_FILENAME_LEN+1];
	ULONG				Unknown;
	ULONG				HardLinkToObject;	/* Key of what I'm pointing to */
	ULONG				HardLinkToMe;		/* Chain of hard links if multiple to same */
	ULONG				Skip1[5];
	ULONG				HashChain;
	ULONG				Parent;
	ULONG				Ext;
	LONG				SecType;
} HardLinkEnd, *HardLinkEndPtr;

/*
 * Data block structures.
 */
 
typedef struct {
	ULONG				Type;
	ULONG				Hdrkey;
	ULONG				SeqNumber;
	ULONG				DataSize;
	ULONG				NextData;
	ULONG				CkSum;
	/*UBYTE			Data[488];*/		/* We could uncomment this if all blocks were 512 */
} OldDataBlk, *OldDataBlkPtr;

/*
 * These are merely provided for informational purposes only. These structures
 * obviously make no sense to use, since all blocks are not 512 bytes.
 */
/*
typedef struct {
	ULONG				CkSum;
	ULONG				Data[508];
} BitMapBlk, *BitMapBlkPtr;

typedef struct {
	ULONG				Keys[127];
	ULONG				CkSum;    DOES THIS EXIST OR IS THERE ANOTHER KEY?
} BitMapExt, *BitMapExtPtr;

typedef struct {
	ULONG				ID;
	ULONG				CkSum;
	ULONG				RootBlk;
	UBYTE				BootCode[500];
} BootBlock, *BootBlockPtr;
*/

#define T_SHORT				2	/* root, user dir, file hdr */
#define T_LIST					16	/* file extension block */
#define T_DATA					8	/* ofs data block */
#define T_DIRCACHE				32	/* dir cache block */

#define IS_EXT_KEY(x)			((x & 0x80000000) != 0)
