/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Analyze and repair bad blocks and files.
 */

#include <exec/types.h>
#include <exec/lists.h>
#include <dos/dos.h>
#include <dos/dosextens.h>
#include <intuition/intuition.h>
#include <rexx/errors.h>

#include <typedefs.h>
#include <string.h>

#include <proto/exec.h>
#include <proto/layers.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Border.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>
#include <Toolbox/DateTime.h>
#include <Toolbox/BarGraph.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/LocalData.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	Extern variables
 */

extern TextChar	toUpper[];
extern GadgetPtr	currGadgList;
extern BarGraphPtr	barGraph;

extern WindowPtr	cmdWindow;

extern TextPtr		strPath, strsErrors[], bigStrsErrors[], dialogVarStr;
extern TextChar	_strYes[], _strNo[], _strOK[];
extern TextChar	strBuff[], currFileName[], strColon[];
extern TextChar	strBlockScan[], strFileScan[];
extern TextChar	strErrorReadBlock[], strErrorWriteBlock[], strErrorCmpBlock[];
extern TextChar	strNoNewBadBlocks[], strNoBadBlocks[], strTotalBadBlocks[];

extern UBYTE		badBlockBSTR[];
extern WORD			action;
extern MsgPortPtr	mainMsgPort;

extern UWORD		charHeight;
extern LONG			dialogVarNum;
extern RepairStats	repairStats;
extern BadBlockStats	badBlockStats;
extern VolParams	currVolParams;
extern OptionsRec	options;
extern WORD			scriptError;
extern BOOL			abortFlag, cancelFlag, skipFlag;
extern BOOL			bitMapDirtyFlag, decimalLabel;
extern UBYTE		*blockBuffer;
extern ULONG		*bitMapDataPtr, *badBlockBuffer;

/*
 * Local prototypes
 */
 
/*static BOOL LongCompare(ULONG *, ULONG *, ULONG);*/
static ULONG ProcessBlockGroup(ULONG, UWORD, UBYTE *);
static void	ProcessBadBlock(ULONG, TextPtr);
static void InitStats(void);
static void ScanDirs(void);
static void FileKeyBusy(ULONG);
static void SaveBadKey(ULONG);
static BOOL CreateNewDCBlocks(void);
static void ProcessDir(ULONG);
static BOOL ProcessFile(ULONG, ULONG, BufCtrlPtr);
static BOOL ProcessSoftLink(UBYTE *, SoftLinkEndPtr);
static BOOL ProcessHardLink(DirScanVarsPtr, HardLinkEndPtr);
static BOOL	ProcessRootChecksum(RootBlockPtr, RootBlockEndPtr);
static void	ProcessBadDirEntry(DirScanVarsPtr);
static BOOL SortHashChain(DirScanVarsPtr);
static BOOL RelinkHashChain(DirScanVarsPtr);
static ULONG SqueezeBlockList(ULONG *);
static BOOL	DoCheckFileName(UBYTE *, UBYTE);
static BOOL	AllocBitMapExtension(BufCtrlPtr *, ULONG *, ULONG);
static WORD CheckEntries(ULONG *, ULONG);
static BOOL	ProcessDirExt(ULONG, ULONG *);
static BOOL	IsDCBlockGarbled(CacheHdrPtr, ULONG);
static BOOL ValidateDateTime(struct DateStamp *, BufCtrlPtr);
static BOOL ValidateComment(TextPtr, BufCtrlPtr);
static void	StoreDummyFileName(TextPtr);

/*
 *	CheckBadBlocks
 *	Check for bad blocks of the current volume.
 * Returns TRUE if test fully completed without any new bad blocks being found.
 */

BOOL CheckBadBlocks()
{
	register ULONG block;
	register RastPtr rPort = cmdWindow->RPort;
	TextChar wordBuff[80];
	ULONG topBlock, size;
	register ULONG newErrors = 0;
	BOOL success = FALSE;
	register UWORD remainder;
	register ULONG numBufs = MIN(MAX_TRACK_BUFS,currVolParams.Envec.de_BlocksPerTrack);
	UBYTE *bufPtr = NULL;
	BOOL deleteOld = FALSE;
	BOOL fileLoaded;
	
	BeginEventString(strcpy(wordBuff, strBlockScan));
	InitStats();
	
	abortFlag = TRUE;
	
	SetBarGraph(rPort, barGraph, currVolParams.DoneBlocks = 0);

	if( AllocateBadBlockBuffer() ) {
		if( ReadBitMap() ) {
			abortFlag = FALSE;
			CountFreeKeys();
			if( options.RepairOpts.Repair ) {
				if( fileLoaded = LoadBadBlocksFile() ) {
					if( badBlockStats.OldBadCount ) {
						dialogVarNum = badBlockStats.OldBadCount;
						deleteOld = (options.PrefsOpts.Interactive != 0) || DoDialog(strsErrors[ERR_BB_FILE_EXISTS], CANCEL_BUTTON, _strYes, _strNo, NULL, FALSE) == OK_BUTTON;
					}
				} else {
					if( badBlockStats.BadBlocksKey != 0L ) {
						if( DeleteBadBlocksFile() ) {
							(void) WriteBitMap();
							(void) WriteCache();
							(void) ReadBitMap();
						}
					}
				}
				if( (!fileLoaded) || deleteOld ) {
					if( badBlockStats.BadBlocksKey != 0L ) {
						MarkFileBlocks(badBlockStats.BadBlocksKey, TRUE);
					}
					*badBlockBuffer = 0L;
					badBlockStats.OldBadCount = 0;
					badBlockStats.BadBlockFlag = FALSE;
				}
			}
			RefreshBlockScan();
			topBlock = currVolParams.HighestKey /*- currVolParams.Envec.de_PreAlloc*/;
			SetBarGraphMax(rPort, barGraph, topBlock);
	
			remainder = numBufs - (currVolParams.LowestKey % numBufs);
			size = numBufs << currVolParams.BlockSizeShift;
			if( !currVolParams.BlockAccess ) {
				bufPtr = AllocIOBuffer(size);
			}
/*
	V2.0.1 fix- indicate only buffer available if failed to get track buffer.
*/
			if( bufPtr == NULL ) {
				remainder = numBufs = 1;
			}
			for( block = currVolParams.LowestKey; (block <= topBlock) && (action != OPER_DONE); ) {

				CheckMainPort();
				if( !abortFlag ) {
					newErrors += ProcessBlockGroup(block, remainder, bufPtr);
				}
				block += remainder;
				remainder = numBufs;
				currVolParams.DoneBlocks = block;
			}
			FreeIOBuffer(&bufPtr, size);
/*
	If aborted, erase bar graph fill area again, and erase "Block:" label as well.
*/
			if( abortFlag |= cancelFlag ) {
				SetBarGraph(rPort, barGraph, 0);
				EraseLabelRect();
			} else {
				currVolParams.DoneBlocks = topBlock;
				RefreshBlockStats();
				success = newErrors == 0;
				if( (newErrors + badBlockStats.OldBadCount) ) {
					if( dialogVarNum = newErrors ) {
						ParseDialogString(strTotalBadBlocks, strBuff);
					} else {
						strcpy(strBuff, strNoNewBadBlocks);
					}
				} else {
					strcpy(strBuff, strNoBadBlocks);
				}
				AddToScrollList(strBuff);
			}
			if( (!abortFlag) && newErrors && options.RepairOpts.Repair ) {
				if( (!deleteOld) || DeleteBadBlocksFile() ) {
					badBlockStats.NewBadCount += badBlockStats.OldBadCount;
					ForceRead();
					MarkBadBlocks();				/* Create/update "bad.blocks" file */
					if( (options.PrefsOpts.Interactive == 0) && (dialogVarNum = badBlockStats.InUseCount) ) {
						ExplainDialog(BIG_ERR_BB_IN_USE);
					}
				}
			}
			FreeBadBlockBuffer();
		} else {
			ExplainDialog(BIG_ERR_NO_BLOCK_CHECK);
		}
	} else {
		OutOfMemory();
	}
	FreeBitMapBuffers();
	MotorOff();
	EndEventString(strcpy(wordBuff, strBlockScan), !abortFlag);
	return(success);
}

/*
 * Process a group of blocks, dealing in whole tracks if possible and "buf" non-NULL.
 * If bad blocks are in the area, then we cannot deal in whole tracks.
 */
 
static ULONG ProcessBlockGroup(register ULONG block, register UWORD num, register UBYTE *buf)
{
	register UWORD i;
	register BOOL wait;
	UWORD saveNum;
	ULONG saveBlock;
	BOOL readFail;
	BOOL alwaysFlagErr = FALSE;
	ULONG newErrors = 0;
	
	if( (buf == NULL) || (num != MIN(MAX_TRACK_BUFS, currVolParams.Envec.de_BlocksPerTrack)) || (badBlockStats.BadBlockFlag && AnyBadInRange(block, num)) ) {
/*
	Read in a block at a time, and rewrite back out if necessary.
*/
		if( currVolParams.IsTD ) {
/*
	If trackdisk device, then first find if there any unreadable or previously
	marked bad blocks in this sector. If so, we'll have to mark the entire remainder
	as unwritable, which may come before our first I/O error.
*/
			saveNum = num;
			saveBlock = block;
			readFail = FALSE;
			
			for( wait = FALSE; (num != 0) && (!wait) ; num-- ) {
				wait = (badBlockStats.BadBlockFlag && !GetBadStatus(saveBlock)) || (readFail = ReadBlock(saveBlock, blockBuffer, FALSE) != 0);
				saveBlock++;
			}
			if( wait && (options.RepairOpts.BadBlockMode != 0) ) {
/*
	Mark all blocks before the pre-existing bad block or read failure as
	write errors if using trackdisk.device and write-verify is enabled.
*/
				while( block != (saveBlock-1) ) {
					ProcessBadBlock(block++, strErrorWriteBlock);
					newErrors++;
					alwaysFlagErr = TRUE;
				}
/*
	If the last block is a result of merely a pre-existing bad block and not
	the result of ReadBlock() failing, then do not count this as an error.
*/
 				if( readFail ) {
					ProcessBadBlock(block, strErrorReadBlock);
					newErrors++;
				}
				block++;
			} else {
/*
	If we did find a bad block somewhere in this group, then leave "num" where
	it is, and update "block" to the new block setting.
*/
				if( wait ) {
					block = saveBlock;
				} else if( options.RepairOpts.BadBlockMode != 0 ) {
					num = saveNum;		/* Redo those blocks if a write-test is needed */
				}
			}
		}
		for( i = num ; (i != 0) && (!abortFlag) ; i-- ) {
			CheckMainPort();
			if( wait = (!badBlockStats.BadBlockFlag) || GetBadStatus(block) ) {
				if( ReadBlock(block, blockBuffer, FALSE) != 0 ) {
					ProcessBadBlock(block, strErrorReadBlock);
					newErrors++;
					alwaysFlagErr = currVolParams.IsTD;
				} else if( (options.RepairOpts.BadBlockMode != 0) && (alwaysFlagErr || WriteBlock(block, buf, FALSE)) ) {
					ProcessBadBlock(block, strErrorWriteBlock);
					newErrors++;
				}
			}
			block++;
		}
		UpdateBlockStats();
	} else {
/*
	Yippee, we can read and write in whole tracks!!
*/
		ReadMulti(block, num, buf, TRUE);
		RefreshBlockStats();
		if( WaitBlock() == 0 ) {
			if( options.RepairOpts.BadBlockMode != 0 ) {
				CmdClear();
				if( (WriteMulti(block, num, buf, FALSE) != 0) || (ReadMulti(block, num, buf, FALSE) != 0) ) {
					for( i = num ; i != 0 ; i--/*, buf += currVolParams.BlockSize*/ ) {
						if( currVolParams.IsTD || WriteBlock(block, buf, FALSE) ) {
							ProcessBadBlock(block, strErrorWriteBlock);
							newErrors++;
						}
						block++;
					}
				}
			}
		} else {
			for( i = num ; (i != 0) && (!abortFlag); i--/*, buf += currVolParams.BlockSize*/ ) {
				CheckMainPort();
				if( ReadBlock(block, buf, FALSE) == 0 ) {
					if( options.RepairOpts.BadBlockMode != 0 ) {
						if( currVolParams.IsTD || WriteBlock(block, buf, FALSE) || ReadBlock(block, buf, FALSE) ) {
							ProcessBadBlock(block, strErrorWriteBlock);
							newErrors++;
						}
					}
				} else {
					ProcessBadBlock(block, strErrorReadBlock);
					newErrors++;
				}
				block++;
			}
		}
	}
	return(newErrors);
}

/*
 * Process a Bad block found. 
 * If repairing, add bad key to list.
 * In any event, output "text" to window.
 */
 
static void ProcessBadBlock(ULONG block, TextPtr text)
{
	TextChar wordBuff[40];
	
	scriptError = 1;								/* Bad block warning flag */
	if( (!options.RepairOpts.Repair) || AddBadKey(block) ) {
		RefreshBlockStats();
		OutputStatHexOrDecString(block, wordBuff, decimalLabel);
		dialogVarStr = wordBuff;
		ParseDialogString(text, strBuff);
		AddToScrollList(strBuff);
	}
	/*
	if( WarningDialog(strBuff, CANCEL_BUTTON) == CANCEL_BUTTON ) {
		action = OPER_DONE;
		abortFlag = TRUE;
	}
	*/
}


/*
 * Compare "size" number of longwords at p1 vs. p2. Return TRUE if different.
 */
/* 
static BOOL LongCompare(register ULONG *p1, register ULONG *p2, register ULONG size)
{
	while( size-- ) {
		if( *p1++ != *p2++ ) {
			return(TRUE);
		}
	}
	return(FALSE);
}
*/

void MarkBadBlocksBusy()
{
	register ULONG *keyPtr = badBlockBuffer;
	
	if( keyPtr != NULL ) {
		while( *keyPtr ) {
			MarkKeyBusy(*keyPtr++);
		}
	}
}

/*
 *	CheckBadFiles
 *	Routine to scan drive for bad files and drawers
 * Returns TRUE if runs through entire procedure without a single hitch!
 */

BOOL CheckBadFiles()
{
	BOOL success = FALSE;
	TextChar wordBuff[40];
	register ULONG max;
	register RastPtr rPort = cmdWindow->RPort;
	
	RefreshFileScan();
	
	BeginEventString(strcpy(wordBuff, strFileScan));
	InitStats();						/* Initialize the repair stats (to zeros) */

	SetBarGraph(rPort, barGraph, currVolParams.DoneBlocks = 0);
	max = currVolParams.MaxKeys - currVolParams.FreeBlocks - (currVolParams.BitMapBlocks + currVolParams.BitMapExtBlks);
	SetBarGraphMax(rPort, barGraph, max);

	if( abortFlag = currVolParams.BitMapBlocks != 0 ) {
		if( AllocateEmptyBitMap() ) {
			if( CheckBootBlock() ) {		/* Check and fix boot block */
				
				(void) LoadBadBlocksFile();
				
				MarkBadBlocksBusy();
				
				if( !(abortFlag = cancelFlag) ) {
					ScanDirs();						/* Repair in place */	

					if( success = !abortFlag ) {
						OutputRepairStats();
					}
					SetBarGraph(rPort, barGraph, success ? max : 0);
				}
			}
		} else {
			OutOfMemory();
		}
		FreeBitMapBuffers();
	}
	MotorOff();
	EndEventString(strcpy(wordBuff, strFileScan), !abortFlag);
	EraseLabelRect();
	return(success);
}

/*
 * Initialize statistics
 */
 
static void InitStats()
{
	BlockClear(&repairStats, sizeof(RepairStats));
	InitPath();
	cancelFlag = skipFlag = FALSE;
}

/*
 * Scan the directory structure painfully.
 */
 
static void ScanDirs()	
{
	register BOOL success;
	BufCtrlPtr buf;
	RootBlockEndPtr rootBlockEndPtr;
	BOOL altBitMapFlag;
	BOOL repairBitMap = FALSE;
	
	repairStats.CurrentBlockType = ST_ROOT;
	if( success = ReadRoot(TRUE, &buf) == 0 ) {
/*
	Just in case another volume is mounted with the same name and date!
*/
		rootBlockEndPtr = (RootBlockEndPtr)(((UBYTE *)buf) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(RootBlockEnd)));
		if( !currVolParams.VolValid ) {
			DateStamp(&rootBlockEndPtr->CDate);
			buf->DirtyFlag = TRUE;
		}
		BumpBarGraph();
		MarkKeyBusy(currVolParams.RootBlock);
		
		altBitMapFlag = currVolParams.DirCache && AllocateAltBitMap();
		
		if( ProcessDirExt(currVolParams.RootBlock, &rootBlockEndPtr->Ext) ) {
			ProcessDir(currVolParams.RootBlock);
		}
		currFileName[0] = '\0';
		DisplayFile();						/* Clear onscreen filename */
		if( !(abortFlag || cancelFlag) ) {
			repairBitMap = ProcessBitMapKeys();
		}
		if( altBitMapFlag ) {			/* Any DC blocks to allocate? */
			SetWaitPointer();				/* Do even if user cancelled operation...*/
			(void) CreateNewDCBlocks();/* ...since the user perceived that...*/
			FreeAltBitMap();				/* ...these were already repaired. */
			SetArrowPointer();
		}
		if( repairBitMap ) {
			if( repairStats.FileSkipped ) {
				Error(ERR_BITMAP_WARN);
			} else {
				RepairBitMap();
			}
		}
		if( options.RepairOpts.Repair ) {
			ForceValidate();
			WriteCache();
		}
	} else {
/*
	If the user hits OK, then we do what little we can, which is to do tell
	him what to do next.
*/
		if( !cancelFlag ) {
			if( options.PrefsOpts.Interactive == 0 ) {
				ExplainDialog(BIG_ERR_ROOT_BAD);
			} else {
				scriptError = RC_ERROR;
			}
		}
		repairStats.VolBadFlag = TRUE;
	}
	abortFlag |= cancelFlag;
}

/*
 * Force validation if flag is set
 */
 
void ForceValidate()
{
	BufCtrlPtr buffer;
	
	if( currVolParams.ValidateVol ) {
		if( ReadRoot(FALSE, &buffer) == CERR_NOERR ) {
			((RootBlockEndPtr) (((UBYTE *)buffer) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(RootBlockEnd)))->BMFlg = 0;
			buffer->DirtyFlag = TRUE;
			currVolParams.ValidateVol = FALSE;
		}
	}
}

/*
 * Find any parents of DC blocks to be created, and create skeleton DC blocks.
 * Break it into two searches so that we try not to end up with DC blocks at
 * the other end of the disk if we don't have to.
 */
 
static BOOL CreateNewDCBlocks()
{
	register ULONG i;
	register ULONG newKey;
	register ULONG newKeyStart = currVolParams.LowestKey;
	register ULONG maxKeys = currVolParams.MaxKeys;
	
	if( currVolParams.DirCache ) {
		for( i = newKeyStart ; i < maxKeys ; i++ ) {
			if( !GetAltStatus(i) ) {
				currVolParams.ValidateVol = TRUE;
				for( newKey = currVolParams.RootBlock ; newKey < maxKeys ; newKey++ ) {
					if( GetKeyStatus(newKey) && CreateDCBlock(newKey, i) ) {
						MarkKeyBusy(newKey);
						newKeyStart = newKey+1;
						break;
					}
				}
				if( newKey == maxKeys ) {
						maxKeys = currVolParams.RootBlock;
					for( newKey = newKeyStart ; newKey < maxKeys ; newKey++ ) {
						if( GetKeyStatus(newKey) && CreateDCBlock(newKey, i) ) {
							MarkKeyBusy(newKey);
							newKeyStart = newKey+1;
							break;
						}
					}
					if( newKey == maxKeys ) {
						return(FALSE);
					}
				}
			}
		}
	}
	return(TRUE);
}	

/*
 * Process a directory during analyze/repair.
 */
 
static void ProcessDir(ULONG blockNumber)
{
	register BOOL cont;
	struct List myList;
	DirScanVars initVars, *newVars;
	register DirScanVarsPtr vars = &initVars;
	register ULONG key = blockNumber;
	register FileHdrEndPtr fHdrEndPtr;
	register FileHdrPtr fileHdrPtr;
	register ULONG temp;
	ULONG hash, saveHashVal;
	BufCtrlPtr buf;

	NHInitList(&myList);
	do {
Loop:	/*++repairStats.TotalDirCount;*/	/* The boss says don't count the root! */
		vars->CurDirKey = key;
		DisplayPath();
		if( ReadControl(key, &vars->CurDirBuf) == CERR_NOERR ) {
			UnlinkCache(vars->CurDirBuf);
			vars->Offset = -sizeof(ULONG);
			vars->Counter = currVolParams.HashTableSize;
AdvHash:	if( !cancelFlag ) {
				vars->Offset += sizeof(ULONG);
				if( (vars->Counter--) == 0 ) {
					if( options.RepairOpts.Repair && vars->CurDirBuf->DirtyFlag ) {
						WriteBlockChecksum(vars->CurDirBuf);
					}
					/* I THINK THE BELOW DOES NOTHING, SINCE IT WAS UNLINKED ALREADY?
					PutCacheBuffer(vars->CurDirBuf);
					SO I HAVE PUT IN WHAT I THINK GEORGE MEANT DOWN BELOW...
					*/
					if( buf = ScanCache(vars->CurDirBuf->Ownkey) ) {
						PutCacheBuffer(buf);
					}
					goto Pop;
				}
NextHash:	skipFlag = FALSE;
				vars->SortFlag = FALSE;					/* No sort needed (yet) */
				vars->HashChainPred = NULL;			/* Predecessor is parent dir */
				if( (vars->KeyHashChain = *((ULONG *)(((UBYTE *)vars->CurDirBuf) + (sizeof(BufCtrl) + sizeof(DirBlock)) + vars->Offset))) == NULL ) {
					goto AdvHash;							/* No entry in hash table...try next */	
				}
NextLink:	CheckMainPort();
				skipFlag = FALSE;
				if( !(abortFlag || cancelFlag) ) {
					currFileName[0] = '\0';
					vars->HashChainSucc = NULL;
					vars->CurEnt = NULL;
					if( !CheckKey(vars->KeyHashChain) ) {
						(void) ReportError(-ANERR_DRAWER_KEY_INVALID, ANACT_ENTRY_DELETED);
						ProcessBadDirEntry(vars);
						goto EntryEnd;
					}
					if( GetCacheBuffer(&vars->CurEnt) == CERR_NOERR) {
						fileHdrPtr = (FileHdrPtr)(vars->CurEnt+1);
						(void) ReadBlock(vars->KeyHashChain, (UBYTE *)fileHdrPtr, TRUE);
						BumpBarGraph();
						if( WaitBlock() ) {
							repairStats.BadBlockCount++;
							if( ReportError(-ANERR_DRAWER_DISK_ERR, ANACT_ENTRY_DELETED) ) {
								ProcessBadDirEntry(vars);
							}
							goto EntryEnd;
						}
						fHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd) );
										
						if( fHdrEndPtr->FileName[0] < (MAX_FILENAME_LEN+1) ) {
							ConvertBCPLToCString(fHdrEndPtr->FileName, currFileName);
						}
						if( !CheckCtrlTypes(fileHdrPtr, &repairStats.CurrentBlockType) ) {
							if( ReportError(-ANERR_BAD_ENTRY_TYPE, ANACT_ENTRY_DELETED) ) {
								ProcessBadDirEntry(vars);
							}
							goto EntryEnd;
						}
						
						ValidateFileName(&fHdrEndPtr->FileName[0], fHdrEndPtr->SecType);
						if( skipFlag || cancelFlag ) {
							goto Pop;
						}
						temp = repairStats.CurrentBlockType;
						if( (temp != ST_USERDIR) && (temp != ST_ROOT) ) {
							DisplayFile();
						}
						
						if( CalcBlockChecksum((ULONG *)fileHdrPtr) ) {
							ReportError(ANERR_DRAWER_CHECKSUM, ANACT_CHECKSUM_RECALC);
							if( cancelFlag ) {
								goto Pop;
							} else if( !skipFlag ) {
								FixChecksum(fileHdrPtr, vars->CurEnt);
								vars->CurEnt->DirtyFlag |= options.RepairOpts.Repair;
							}
						}
						if( temp != ST_ROOT ) {	/* Don't process root this way. */
							if( !GetKeyStatus(vars->KeyHashChain) ) {
								SaveBadKey(vars->KeyHashChain);
								if( !ReportError(-ANERR_ENTRY_CROSS_LINKED, ANACT_ENTRY_DELETED) ) {
									goto Pop;
								}
								ProcessBadDirEntry(vars);
								goto EntryEnd;
							}
							if( vars->KeyHashChain != fileHdrPtr->Ownkey ) {
								if( ReportError(ANERR_OWNKEY_MISMATCH, ANACT_ERROR_FIXED) ) {
									SaveBadKey(vars->KeyHashChain);
									fileHdrPtr->Ownkey = vars->KeyHashChain;
									vars->CurEnt->DirtyFlag |= options.RepairOpts.Repair;
								} else {
									if( cancelFlag ) {
										goto Pop;
									} else if( skipFlag ) {
										goto EntryEnd;
									}
								}
							}
							if( fileHdrPtr->Skip1 != NULL ) {
								if( ReportError(ANERR_HDR_EXTRANEOUS_DATA, ANACT_ERROR_FIXED) ) {
									fileHdrPtr->Skip1 = NULL;
									vars->CurEnt->DirtyFlag |= options.RepairOpts.Repair;
								} else {
									if( cancelFlag ) {
										goto Pop;
									} else if( skipFlag ) {
										goto EntryEnd;
									}
								}
							}
/*
	The following error checks are more or less in order of decreasing
	significance, and are common to both directories and filehdrs.
*/
							if( (vars->HashChainSucc = fHdrEndPtr->HashChain) != NULL ) {
								if( !CheckKey(vars->HashChainSucc) ) {
									vars->HashChainSucc = NULL; /* Zero garbage link */
									if( !ReportError(-ANERR_HASH_CHAIN_INVALID, ANACT_ENTRY_DELETED) ) {
										goto Pop;
									}
									ProcessBadDirEntry(vars);	/* Since entry is deleted */
								} else {
									if( !GetKeyStatus(vars->HashChainSucc) ) {
										if( ReportError(-ANERR_HASH_CROSS_LINKED, ANACT_HASH_DELETED) ) {
											SaveBadKey(vars->HashChainSucc);
											fHdrEndPtr->HashChain = NULL;
											vars->CurEnt->DirtyFlag |= options.RepairOpts.Repair;
										}
										vars->HashChainSucc = NULL;
										if( cancelFlag ) {
											goto Pop;
										} else if( skipFlag ) {
											goto EntryEnd;
										}
									} else if( vars->HashChainSucc <= vars->KeyHashChain ) {
										++vars->SortFlag;
									}
								}
							}
							if( fHdrEndPtr->Parent != vars->CurDirKey ) {
								if( ReportError(ANERR_PARENTKEY_MISMATCH, ANACT_ERROR_FIXED) ) {
									SaveBadKey(fHdrEndPtr->Parent);
									fHdrEndPtr->Parent = vars->CurDirKey;
									vars->CurEnt->DirtyFlag |= options.RepairOpts.Repair;
								}
								if( cancelFlag ) {
									goto Pop;
								} else if( skipFlag ) {
									goto EntryEnd;
								}
							}
							ValidateDateTime(&fHdrEndPtr->MDate, vars->CurEnt);
							if( !(skipFlag || cancelFlag) ) {
								ValidateComment(fHdrEndPtr->Comment, vars->CurEnt);
								if( !(skipFlag || cancelFlag) ) {
/*
	Entry passed basic tests...now process the entry
*/
									MarkKeyBusy(vars->KeyHashChain);
									LinkCacheKey(vars->KeyHashChain, BLOCK_CONTROL, vars->CurEnt);
									hash = CalcHash(fHdrEndPtr->FileName);
									if( hash != vars->Offset ) {
										saveHashVal = hash;
										if( ReportError(-ANERR_WRONG_HASH, ANACT_RELINKED) ) {
											if( options.RepairOpts.Repair ) {
												RelinkHashChain(vars);	/* Should check for error! */
												if( saveHashVal > vars->Offset ) {
													MarkKeyFree(vars->KeyHashChain);
													vars->KeyHashChain = vars->HashChainPred;
													goto EntryEnd;
												} else {
													vars->HashChainSucc = NULL;
												}
											}
										}
									}
								}
							}
						}
						if( skipFlag || cancelFlag ) {
							goto EntryEnd;
						}
						switch(fHdrEndPtr->SecType) {
						case ST_FILE:
							ProcessFile(vars->KeyHashChain, vars->CurDirKey, vars->CurDirBuf);
							goto EntryEnd;
						case ST_SOFTLINK:
							if( !ProcessSoftLink((UBYTE *)(fileHdrPtr+1), (SoftLinkEndPtr) fHdrEndPtr) ) {
								DeleteFileEntry(vars->KeyHashChain, vars->CurDirKey, vars->CurDirBuf);
							}
							goto EntryEnd;
						case ST_LINKDIR:
						case ST_LINKFILE:
							if( !ProcessHardLink(vars, (HardLinkEndPtr) fHdrEndPtr) ) {
								DeleteFileEntry(vars->KeyHashChain, vars->CurDirKey, vars->CurDirBuf);
							}
							goto EntryEnd;
						case ST_ROOT:
							(void) ReportError(-ANERR_DRAWER_ROOT, ANACT_ENTRY_DELETED);
						default:							/* Unknown entries are disposed of */
							ProcessBadDirEntry(vars);
							goto EntryEnd;
						case ST_USERDIR:
							(void) ProcessDirExt(vars->KeyHashChain, &fHdrEndPtr->Ext);
							ConvertBCPLToCString(fHdrEndPtr->FileName, strBuff);
							AppendPath(strBuff);
							key = vars->KeyHashChain;
							if( (newVars = (DirScanVarsPtr) MemAlloc(sizeof(DirScanVars), MEMF_CLEAR)) != NULL ) {
								AddHead(&myList, (NodePtr) &vars->Node);
								vars = newVars;
								goto Loop;
							} else {
								OutOfMemory();
								cancelFlag = TRUE;
							}
						}
					}
				}
			}
		}
Pop:	if( cont = vars != &initVars ) {
			MemFree(vars, sizeof(DirScanVars));
			vars = (DirScanVarsPtr) RemHead(&myList);
			TruncatePath();
			DisplayPath();

EntryEnd:if( cancelFlag ) {
				goto Pop;
			} else {
				vars->HashChainPred = vars->KeyHashChain;
				if( vars->KeyHashChain = vars->HashChainSucc ) {
					goto NextLink;
				}
				if( vars->SortFlag && currVolParams.FFSFlag ) {
					if( ReportError(-ANERR_HASH_NOT_SORTED, ANACT_SORTED) ) {
						if( options.RepairOpts.Repair ) {
							SortHashChain(vars);	/* Need a check for read failure */
						}
					} else {
						goto Pop;
					}
				}
				goto AdvHash;
			}
		}
	} while( cont );
}

/*
 * Process directory cache's directory extension key.
 */
 
static BOOL ProcessDirExt(ULONG parentKey, ULONG *dirExtKeyPtr)
{
	register ULONG dirExtKey = *dirExtKeyPtr;
	register WORD err;
	register CacheHdrPtr cacheHdrPtr;
	UBYTE cacheErr;
	BufCtrlPtr buf, parentBuf;
	
	if( currVolParams.DirCache ) {
		do {
			dialogVarNum = dirExtKey;
			err = 0;
			if( (dirExtKey == 0) || !CheckKey(dirExtKey) ) {
				err = ANERR_BAD_DIR_EXT_KEY;
			} else if( !MarkKeyBusy(dirExtKey) ) {
				err = ANERR_DIR_EXT_CROSS_LINKED;
			}
			if( err ) {
				*dirExtKeyPtr = 0L;
				if( ReportError(err, ANACT_KEY_ALLOCATED) ) {
					MarkAltBusy(parentKey);
				}
				dirExtKey = 0L;
			} else {
				BumpBarGraph();
				switch( cacheErr = ReadCache(dirExtKey, BLOCK_DIRCACHE, &buf) ) {
				case CERR_NOERR:
				case CERR_CHECKSUM:
					if( ReadControl(parentKey, &parentBuf) == CERR_NOERR ) {
						cacheHdrPtr = (CacheHdrPtr)(buf+1);
						if( (cacheHdrPtr->Type > 255) || ((cacheHdrPtr->Type & T_DIRCACHE) == 0) || IsDCBlockGarbled(cacheHdrPtr, dirExtKey) ) {
							if( ReportError(ANERR_DIR_EXT_GARBLED, ANACT_KEY_ALLOCATED) ) {
								MarkAltBusy(parentKey);
								MarkKeyFree(dirExtKey);
								*dirExtKeyPtr = 0L;
							}
							dirExtKey = 0L;
							parentBuf->DirtyFlag = TRUE;
						} else {
							if( cacheErr == CERR_CHECKSUM ) {
								if( ReportError(ANERR_DIR_EXT_CHECKSUM, ANACT_CHECKSUM_RECALC) ) {
									FixChecksum(cacheHdrPtr, buf);
									err++;
								}
							}
							if( (!cancelFlag) && (cacheHdrPtr->Parent != parentBuf->Ownkey) ) {
								if( ReportError(ANERR_DIR_EXT_WRONG_PARENT, ANACT_ERROR_FIXED) ) {
									cacheHdrPtr->Parent = parentBuf->Ownkey;
									cacheHdrPtr->Type = T_DIRCACHE;
									err++;
								}
							}
							if( (!cancelFlag) && (cacheHdrPtr->Ownkey != dirExtKey) ) {
								if( ReportError(ANERR_DIR_EXT_OWNKEY_MISMATCH, ANACT_ERROR_FIXED) ) {
									cacheHdrPtr->Ownkey = dirExtKey;
									err++;
								}
							}
							dirExtKey = cacheHdrPtr->NextBlock;
						}
					}
					break;
				default:
					dirExtKey = 0L;
					err++;
					repairStats.BadBlockCount++;
					if( ReportError(-ANERR_DIR_EXT_DISK_ERR, ANACT_KEY_ALLOCATED) ) {
						MarkAltBusy(parentKey);
					}
					break;
				}
				if( err && (!cancelFlag) ) {
					buf->DirtyFlag = TRUE;
				} else {
					PutCacheBuffer(buf);
				}
			}
		} while( (!cancelFlag) && (dirExtKey != 0L) );
	}
	return(!cancelFlag);
}

#define MIN_DCE_SIZE	24

/*
 * Returns whether the DC block has any anomalies WHATSOEVER!
 */
 
static BOOL IsDCBlockGarbled(CacheHdrPtr cacheHdrPtr, register ULONG ownKey)
{
	register UBYTE *ptr = (UBYTE *)(cacheHdrPtr+1);
	register BOOL garbled = FALSE;
	BufCtrlPtr buf;
	register WORD numEntries = (cacheHdrPtr->NumEntries);
	UBYTE *endPtr = ((UBYTE *)cacheHdrPtr) + currVolParams.BlockSize;
	register FileHdrEndPtr fileHdrEndPtr;
	
	while( (numEntries--) && (!garbled) ) {
		garbled = TRUE;
		if( *((ULONG *)ptr) != ownKey ) {
			if( ReadControl(*((ULONG *)ptr), &buf) == CERR_NOERR ) {
				ptr += sizeof(ULONG);
				fileHdrEndPtr = (FileHdrEndPtr)(((UBYTE *)buf) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(FileHdrEnd));
				if( *((ULONG *)ptr) == fileHdrEndPtr->FileSize ) {
					ptr += sizeof(ULONG);
					if( *((ULONG *)ptr) == fileHdrEndPtr->Protect ) {
						ptr += sizeof(ULONG);
						if( *((ULONG *)ptr) == fileHdrEndPtr->OwnerXID ) {
							ptr += sizeof(ULONG) + (3*sizeof(UWORD));
							if( *ptr++ == (BYTE) fileHdrEndPtr->SecType ) {
								if( fileHdrEndPtr->FileName[0] && (CmpString(ptr, fileHdrEndPtr->FileName, ptr[0], fileHdrEndPtr->FileName[0], TRUE) == 0) ) {
									ptr += ptr[0]+1;
									if( CmpString(ptr, fileHdrEndPtr->Comment, ptr[0], fileHdrEndPtr->Comment[0], TRUE) == 0 ) {
										ptr += ptr[0]+1;
										if( ((ULONG)ptr) & 1 ) {
											ptr++;
										}
										garbled = ptr > endPtr;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return(garbled);
}
		
			
/*
 * Process a bad entry.
 */
 
static void ProcessBadDirEntry(register DirScanVarsPtr vars)
{
	DirBlockEndPtr dBlkPtr;
	BufCtrlPtr buf;
	
	repairStats.BadDirCount++;
	
	if( vars->CurEnt != NULL ) {
		PutCacheBuffer(vars->CurEnt);
	}
	if( options.RepairOpts.Repair ) {
		if( vars->HashChainPred == NULL ) {
			*(ULONG *)(((UBYTE *)vars->CurDirBuf) + sizeof(BufCtrl) + sizeof(DirBlock) + vars->Offset) = vars->HashChainSucc;
			vars->CurDirBuf->DirtyFlag = TRUE;
		} else {
			if( ReadControl(vars->HashChainPred, &buf) == CERR_NOERR ) {
				buf->DirtyFlag = TRUE;
				dBlkPtr = (DirBlockEndPtr) (((UBYTE *)buf) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(DirBlockEnd)) );
				dBlkPtr->HashChain = vars->HashChainSucc;
			}
		}
	}
	vars->KeyHashChain = vars->HashChainPred;
}

/*
 * Processes a file, called from ProcessDir()
 */
 
static BOOL ProcessFile(register ULONG fileHdrKey, ULONG parentKey, BufCtrlPtr dirBuf)
{
	BOOL errorFlag = FALSE;
	register ULONG temp;
	register ULONG totalBlockCount;
	register ULONG blockCount;
	register BOOL success;
	register FileHdrEndPtr fileHdrEndPtr;
	register FileHdrPtr fileHdrPtr;
	BufCtrlPtr buf;
	ULONG *keyPtr;
	BYTE err;
	ULONG fileSize;
	ULONG oldDoneBlocks = currVolParams.DoneBlocks;
	BOOL checkData = !currVolParams.FFSFlag;
	UWORD errorCount;
	UBYTE numBuff[16];
	BOOL isBadBlock;
	BOOL isBadBlockFile = badBlockStats.BadBlockFlag && (fileHdrKey == badBlockStats.BadBlocksKey);
	
	++repairStats.TotalFileCount;
	if( success = (ReadControl(fileHdrKey, &buf) == 0) ) {
		fileHdrPtr = (FileHdrPtr)(buf+1);
		fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
		fileSize = fileHdrEndPtr->FileSize;
		totalBlockCount = 0;
		errorCount = 0;
			
		if( fileHdrPtr->FirstBlk != *(((ULONG *)fileHdrEndPtr)-1) ) {
			if( ReportError(ANERR_DATA_MISMATCH, ANACT_ERROR_FIXED) ) {
				++errorCount;
				fileHdrPtr->FirstBlk = *(((ULONG *)fileHdrEndPtr)-1);
			} else {
				PutCacheBuffer(buf);
			}
			if( skipFlag || cancelFlag ) {
				goto ProcFileEx;
			}
		}
ProcHdr:
		keyPtr = &fileHdrEndPtr->Reserved;
		if( (temp = SqueezeBlockList(keyPtr)) != fileHdrPtr->BlkCnt ) {
			fileHdrPtr->BlkCnt = temp;
			if( ReportError(ANERR_BAD_BLOCK_COUNT, ANACT_ERROR_FIXED) ) {
				++errorCount;
			}
			if( skipFlag || cancelFlag ) {
				goto ProcFileEx;
			}
		}
		blockCount = fileHdrPtr->BlkCnt;
		totalBlockCount += blockCount;

		while( blockCount-- ) {
			CheckMainPort();
			if( skipFlag || abortFlag ) {
				/* success = FALSE; */
				goto ProcFileEx;
			}
			temp = *--keyPtr;
			if( success = CheckKey(temp) ) {
				isBadBlock = badBlockStats.BadBlockFlag && (!GetBadStatus(temp));
/*
	New for 2.0, we have an option to not read the data blocks if FFS (tons faster).
*/
				if( (checkData || options.RepairOpts.CheckFFSData) && (!isBadBlock) ) {
					(void) ReadBlock(temp, blockBuffer, TRUE);
					BumpBarGraph();							/* Draw while reading disk */
					if( WaitBlock() ) {
						++repairStats.BadBlockCount;	/* Couldn't read file. */
						dialogVarNum = temp;
						if( !ReportError(-ANERR_DATA_DISK_ERR, ANACT_FILE_DELETED) ) {
							goto ProcFileEx;
						}
						success = FALSE;
					}
				} else {
					BumpBarGraph();
					checkData = FALSE;
				}
			} else {
				BumpBarGraph();
				dialogVarNum = temp;
				if( !ReportError(-ANERR_DATA_KEY_INVALID, ANACT_DATA_DELETED) ) {
					goto ProcFileEx;
				}
			}
			if( success ) {
/*
	Before checking the contents, make sure not cross-linked. Also, don't do
	this test if on the bad blocks file, since we mark those bits in the bitmap
	busy at the outset.
*/				
				if( (!isBadBlockFile) && !(success &= MarkKeyBusy(temp)) ) {
					FileKeyBusy(temp);		/* Error if it was already busy. */
					dialogVarNum = temp;
					if( !ReportError(-ANERR_DATA_CROSS_LINKED, ANACT_FILE_DELETED) ) {
						goto ProcFileEx;
					}
				}
				if( success && checkData && (!isBadBlock) ) {
					if( (((OldDataBlkPtr)blockBuffer)->Type != T_DATA) ||
					    (((OldDataBlkPtr)blockBuffer)->Hdrkey != fileHdrKey) ) {
						success = FALSE;
						if( !ReportError(-ANERR_DATA_HDR, ANACT_FILE_DELETED) ) {
							goto ProcFileEx;
						}
					} else if( CalcBlockChecksum((ULONG *)blockBuffer) ) {
						if( ReportError(ANERR_DATA_CHECKSUM, ANACT_CHECKSUM_RECALC)) {
							FixChecksum(blockBuffer, NULL);
							while( success && (err = WriteBlock(temp, blockBuffer, FALSE)) ) {
								OutputStatHexOrDecString(temp, numBuff, decimalLabel);
								dialogVarStr = numBuff;
								success = WarningDialog(strsErrors[ERR_WRITE_BLOCK], TRUE) == OK_BUTTON;
							}
						} else if( skipFlag || cancelFlag ) {
							goto ProcFileEx;
						}
					}
				}
			}
			if( !success ) {
				DeleteFileEntry(fileHdrKey, parentKey, dirBuf);
				++errorCount;
				errorFlag = TRUE;
				*keyPtr = 0;				/* Bad key, get rid of it. */
			}
			if( cancelFlag ) {
				/*success = FALSE;*/
				goto ProcFileEx;
			}
		}									/* Loop for all keys in header. */
EndOfHdr:
		fileHdrPtr->BlkCnt = SqueezeBlockList(&fileHdrEndPtr->Reserved);
		temp = *((LONG *)&fileHdrEndPtr->Ext);
		if( errorCount && options.RepairOpts.Repair ) {
			buf->DirtyFlag = TRUE;
		}
		if( !buf->DirtyFlag ) {
			PutCacheBuffer(buf);			/* If no errors or not repairing, put back. */
		}
		if( temp == 0 ) {					/* If no ext filehdr, all done. */
			goto EndOfFile;
		}
ReadExtension:
		if( !CheckKey(temp) ) {			/* Sanity check on key... */
			if( !ReportError(-ANERR_EXT_INVALID, ANACT_FILE_DELETED) ) {
				goto ProcFileEx;
			}
			goto DoDelFile;
		} else {
			if( GetCacheBuffer(&buf) != CERR_NOERR ) {
				goto ProcFileEx;
			}
			fileHdrPtr = (FileHdrPtr)(buf+1);
			fileHdrEndPtr = (FileHdrEndPtr)(((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
			
			(void) ReadBlock(temp, (UBYTE *)fileHdrPtr, TRUE);
			BumpBarGraph();
			err = WaitBlock();
			if( err ) {	
				++repairStats.BadBlockCount;
				if( ReportError(-ANERR_EXT_DISK_ERR, ANACT_FILE_DELETED) ) {
					goto DoDelFile;
				} else {
					goto Err;
				}
			} else {
				errorCount = 0;		/* If non-zero, we fixed something, so write it */
				if( CalcBlockChecksum((ULONG *)fileHdrPtr) != 0 ) {
					if( ReportError(ANERR_EXT_CHECKSUM, ANACT_CHECKSUM_RECALC) ) {
						FixChecksum(fileHdrPtr, buf);
						errorCount++;
					} else if( skipFlag || cancelFlag ) {
						goto Err;
					}
				}
				if( (fileHdrPtr->Type != T_LIST) || (fileHdrEndPtr->SecType != ST_FILE) ) {
					if( ReportError(-ANERR_EXT_BAD, ANACT_FILE_DELETED) ) {
						goto DoDelFile;
					} else {
						goto Err;
					}
				} else {
					if( fileHdrPtr->Ownkey != temp ) {
						if( ReportError(ANERR_EXT_OWNKEY_MISMATCH, ANACT_ERROR_FIXED) ) {
							++errorCount;
							SaveBadKey(fileHdrPtr->Ownkey);
							fileHdrPtr->Ownkey = temp;
						} else if( skipFlag || cancelFlag ) {
							goto Err;
						}
					}
					if( fileHdrEndPtr->Parent != fileHdrKey ) {
						if( ReportError(ANERR_EXT_PARENTKEY_MISMATCH, ANACT_ERROR_FIXED) ) {
							SaveBadKey(fileHdrEndPtr->Parent);
							fileHdrEndPtr->Parent = fileHdrKey;
							++errorCount;
						} else if( skipFlag || cancelFlag ) {
							goto Err;
						}
					}
					if( MarkKeyBusy(temp) ) {
						LinkCacheKey(temp, BLOCK_CONTROL, buf);
						goto ProcHdr;		/* Exthdr looks good, continue. */
					} else {
						FileKeyBusy(temp);
						if( ReportError(-ANERR_EXT_CROSS_LINKED, ANACT_FILE_DELETED) ) {
							buf->DirtyFlag = TRUE;	/* Is this needed?  */
							goto DoDelFile;
						} 
					}
Err:				if( errorCount ) {
						LinkCacheKey(temp, BLOCK_CONTROL, buf);
						buf->DirtyFlag = options.RepairOpts.Repair;
					} else {
						PutCacheBuffer(buf);
					}
					goto ProcFileEx;
					
DoDelFile:		DeleteFileEntry(fileHdrKey, parentKey, dirBuf);
					errorFlag = TRUE;
				}
			}
		}
EndOfFile:
		if( ReadControl(fileHdrKey, &buf) != CERR_NOERR) {
			goto ProcFileEx;
		}
		temp = totalBlockCount << currVolParams.BlockSizeShift;
		fileHdrEndPtr = (FileHdrEndPtr)(((UBYTE *)buf) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(FileHdrEnd)));
		if( fileHdrEndPtr->FileSize > temp ) {
			fileHdrEndPtr->FileSize = temp;
			if( !ReportError(ANERR_FILE_SIZE, ANACT_ERROR_FIXED) ) {
				goto ProcFileEx;
			}
			if( options.RepairOpts.Repair ) {
				buf->DirtyFlag = TRUE;
			}
		}	
	}
ProcFileEx:
	if( errorFlag ) {
		repairStats.BadFileCount++;
		if( currVolParams.VolValid && currVolParams.Info.id_BytesPerBlock ) {
			temp = (fileSize / currVolParams.Info.id_BytesPerBlock) + (fileSize / (currVolParams.HashTableSize << currVolParams.BlockSizeShift));
			if( temp -= currVolParams.DoneBlocks - oldDoneBlocks ) {
				currVolParams.DoneBlocks += temp;
				BumpBarGraph();
			}
		}
	}
	return(success);
}

/*
 * Process a soft link during a repair.
 * Returns TRUE if the soft link's name is short enough to fit in the hash table,
 * or the user tells Tools to ignore it.
 */
 
static BOOL ProcessSoftLink(UBYTE *softLinkName, SoftLinkEndPtr softLinkEndPtr)
{
	BOOL success;
	
	repairStats.CurrentBlockType = 0;
	++repairStats.TotalLinkCount;
	if( strlen(softLinkName) >= (currVolParams.HashTableSize << 2) ) {
		if( success = !ReportError(-ANERR_SOFT_LINK_TOO_LONG, ANACT_ENTRY_DELETED) ) {
			++repairStats.BadLinkCount;
		}
	} else {
		success = TRUE;
	} 
	return(success);
}

/*
 * Process a hard link during a repair.
 * Returns TRUE if the following conditions are met or the user says "Ignore."
 * -> There must be a valid key in the link-to-object field.
 * -> The data in that key must be a valid control block.
 */
 
static BOOL ProcessHardLink(DirScanVarsPtr vars, HardLinkEndPtr hardLinkEndPtr)
{
	BOOL success = TRUE;
	BOOL errFlag = FALSE;
	ULONG secType;
	BufCtrlPtr buf;
	FileHdrPtr fileHdrPtr;
	/*
	FileHdrEndPtr fileHdrEndPtr;
	*/
	ULONG linkKey = hardLinkEndPtr->HardLinkToObject;
	
	repairStats.CurrentBlockType = 0;
	++repairStats.TotalLinkCount;
	if( !CheckKey(linkKey) ) {
		if( ReportError(-ANERR_HARD_LINK_INVALID, ANACT_ENTRY_DELETED) ) {
			success = FALSE;
		}
		errFlag = success;
	} else if( ReadSpecial(linkKey, &buf) == CERR_NOERR ) {
		fileHdrPtr = (FileHdrPtr) (buf+1);
		/*
		fileHdrEndPtr = (FileHdrEndPtr)(((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
		*/
		if( (!CheckCtrlTypes(fileHdrPtr, &secType)) ) {
			if( ReportError(-ANERR_HARD_LINK_INVALID, ANACT_ENTRY_DELETED) ) {
				success = FALSE;
			}
			errFlag = success;
		}
	}
	if( errFlag ) {
		++repairStats.BadLinkCount;
	}
	return(success);
}

/*
 * Scans filehdr block list, counting active blocks and squeezing out nulls.
 * Returns block count.
 */
 
static ULONG SqueezeBlockList(register ULONG *longPtr)
{
	register ULONG n = currVolParams.HashTableSize;
	register ULONG *destPtr = longPtr;
	register ULONG numBlocks = 0;
	register ULONG temp;
	
	while( n-- ) {
		if( temp = *--longPtr ) {
			*--destPtr = temp;
			++numBlocks;
		}
	}
	n = destPtr - longPtr;
	while( n-- ) {
		*--destPtr = 0;
	}
	return(numBlocks);
}
	
/*
 * Calculates hash table entry offset for Pascal/BCPL string passed.
 * Returns hash table byte offset.
 */
 
UWORD CalcHash(register UBYTE *fileName)
{
	register ULONG length = *fileName++;
	register ULONG hash = length;
	register UBYTE ch;
			
	if( currVolParams.International ) {
		for( ; length-- ; hash = ((hash * 13) + toUpper[*fileName++]) & 0x7FF );
	} else {
		while( length-- ) {
			ch = *fileName++;
			if( (ch >= 'a') && (ch <= 'z') ) {
				ch -= ('a' - 'A');
			}
			hash = ((hash * 13) + ch) & 0x7FF;
		}
	}
	return( (hash % currVolParams.HashTableSize) << 2 );
}

/*
 * Read the root block. Must have initialized the volume parameters ahead of time.
 * Returns error code to display, or zero if successful.
 *
 * If "rigorous" TRUE, this is part of a repair operation and so should be
 * rigorously checked for errors and also repaired if applicable.
 */
 
BYTE ReadRoot(BOOL rigorous, BufCtrlPtr *bufferHandle)
{
	register RootBlockPtr rootBlock;
	register RootBlockEndPtr rootEnd;
	BufCtrlPtr rootBuffer;
	BYTE len;
	register BOOL hashSizeDiff;
	register BYTE err;
	register BYTE retry;
	ULONG secType;
	register UWORD errorCount = 0;
	
	if( rigorous ) {
		if( (err = GetCacheBuffer(bufferHandle)) == CERR_NOERR ) {
			rootBuffer = *bufferHandle;
			rootBlock = (RootBlockPtr) (rootBuffer+1);
			if( err = ReadBlock(currVolParams.RootBlock, (UBYTE *)rootBlock, FALSE) ) {
				(void) ReportError(-ANERR_ROOT_DISK_ERR, ANACT_NONE_FILES_INACCESSIBLE);
			} else {
				if( !CheckCtrlTypes(rootBlock, &secType) ) {
					if( ReportError(-ANERR_ROOT_CORRUPT, ANACT_ERROR_FIXED) ) {
						rootBlock->Type = T_SHORT;
						*(((ULONG *) rootBlock) + currVolParams.BlockSizeL - 1) = ST_ROOT;
						FixChecksum(rootBlock, *bufferHandle);
						errorCount++;
					} else {
						err = TRUE;
					}
				}
			}
			if( err ) { 
				PutCacheBuffer(*bufferHandle);
			}
		}
	} else {
		do {
			retry = err = ReadCtrlBlock(currVolParams.RootBlock, bufferHandle, TRUE);
			if( err ) {
				retry = FALSE;
				if( err == CERR_BADTYPE ) {
					/*Error(ERR_ROOT_GARBLED);*/
				} else if( err == CERR_CHECKSUM ) {
					rootBuffer = *bufferHandle;
					if( ReportError(-ANERR_ROOT_CHECKSUM, ANACT_CHECKSUM_RECALC) ) {
						retry = TRUE;
						FixChecksum((rootBuffer+1), rootBuffer);
					} else {
						PutCacheBuffer(rootBuffer);
					}
				} else {
					Error(ERR_ROOT_READ);
				}
			}
		} while( retry );
	}
	if( err == CERR_NOERR ) {
		rootBlock = (RootBlockPtr) ((*bufferHandle)+1);
		rootEnd = (RootBlockEndPtr) (((UBYTE *)rootBlock) + currVolParams.BlockSize - sizeof(RootBlockEnd));
		len = (BYTE) rootEnd->VolName[0];
		hashSizeDiff = rootBlock->HashSize != currVolParams.HashTableSize;
		if( rigorous ) {
			if( hashSizeDiff ) {
				errorCount++;
				rootBlock->HashSize = currVolParams.HashTableSize;
				if( ReportError(ANERR_ROOT_CORRUPT, ANACT_ERROR_FIXED) ) {
					hashSizeDiff = FALSE;
				}
			}
			if( !hashSizeDiff ) {
				if( hashSizeDiff = !ProcessRootChecksum(rootBlock, rootEnd) ) {
					errorCount++;
					if( ReportError(ANERR_ROOT_CORRUPT, ANACT_ERROR_FIXED) ) {
						hashSizeDiff = FALSE;
					}
					
				}
			}
			if( !hashSizeDiff ) {
				rootBuffer = *bufferHandle;
				if( ValidateFileName(rootEnd->VolName, rootEnd->SecType) ) {
					errorCount++;
					(void) ValidateDateTime(&rootEnd->DMDate, rootBuffer);
					if( !cancelFlag ) {
						(void) ValidateDateTime(&rootEnd->CDate, rootBuffer);
					}
				}
				if( errorCount && options.RepairOpts.Repair ) {
					rootBuffer->DirtyFlag = TRUE;
				} else {
					hashSizeDiff = TRUE;
				}
				LinkCacheKey(currVolParams.RootBlock, BLOCK_CONTROL, rootBuffer);
			}
			if( hashSizeDiff ) {
				PutCacheBuffer(*bufferHandle);
			}
		} else { 
			if( hashSizeDiff || (rootEnd->SecType != ST_ROOT) ||
				(len < 0) || (len >= MAX_FILENAME_LEN) ) {
				err = CERR_BADTYPE;
				/*Error(ERR_ROOT_GARBLED);*/
			}
		}
	}
	return(err);
}

/*
 * Recalculates and corrects checksum of block passed, unconditionally.
 * If "buffer" is non-NULL, and repair mode in effect, then mark it as dirty.
 */
 
void FixChecksum(void *block, BufCtrlPtr buffer)
{
	((FileHdrPtr)block)->CkSum -= CalcBlockChecksum((ULONG *)block);
	if( options.RepairOpts.Repair ) {
		if( buffer != NULL ) {
			buffer->DirtyFlag = TRUE;
		}
	}
}

/* 
 * Sorts hashchain into dir block. This is necessary only
 * if FFS, where blocks must be sorted in ascending order.
 */
 
static BOOL SortHashChain(DirScanVarsPtr vars)
{
	register BOOL cont;
	register BOOL sortFlag = TRUE;
	register ULONG curKey;
	register ULONG entry;
	register ULONG *hashTablePtr;
	register ULONG *hashLinkPtr;
	ULONG *newHashLinkPtr;
	register UBYTE err;
	BufCtrlPtr buf1, buf2, buf3;

	curKey = vars->CurDirKey;
	buf1 = vars->CurDirBuf;
	LinkCacheKey(curKey, BLOCK_CONTROL, buf1);
	while( sortFlag && ((err = ReadControl(curKey, &buf1)) == 0) ) {
		buf1->DirtyFlag = TRUE;
		sortFlag = FALSE;
		hashTablePtr = (ULONG *) (((UBYTE *)buf1) + (sizeof(BufCtrl) + sizeof(DirBlock)) + vars->Offset);
		do {
			entry = *hashTablePtr;
			if( cont = (err = ReadControl(*hashTablePtr, &buf2)) == 0 ) {
				buf2->DirtyFlag = TRUE;
				hashLinkPtr = (ULONG *) &(((DirBlockEndPtr)(((UBYTE *)buf2) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(DirBlockEnd))))->HashChain);
				if( cont = *hashLinkPtr != 0 ) {
					if( *hashLinkPtr < entry ) {
						sortFlag = TRUE;
						if( cont = (err = ReadControl(*hashLinkPtr, &buf3)) == 0 ) {
							buf3->DirtyFlag = TRUE;
							newHashLinkPtr = (ULONG *)&(((DirBlockEndPtr)(((UBYTE *)buf3) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(DirBlockEnd))))->HashChain);
							*hashTablePtr = *hashLinkPtr;
							*hashLinkPtr = *newHashLinkPtr;
							*newHashLinkPtr = entry;
							hashLinkPtr = newHashLinkPtr;
						}
					}
					hashTablePtr = hashLinkPtr;
				}
			}
		} while( cont );
	}
	UnlinkCache(buf1);
	return( err == 0 );
}			

/*
 * Key of control block to relink in vars->KeyHashChain. Unlinks from current
 * chain and relinks into proper chain. Returns whether successful.
 */
 
static BOOL RelinkHashChain(register DirScanVarsPtr vars)
{
	register BOOL success;
	FileHdrEndPtr fileHdrEndPtr;
	BufCtrlPtr buf;
	
	if( success = vars->HashChainPred == 0L ) {
		*(ULONG *)(((UBYTE *)vars->CurDirBuf) + (sizeof(BufCtrl) + sizeof(DirBlock)) + vars->Offset) = vars->HashChainSucc;
		vars->CurDirBuf->DirtyFlag = TRUE;
	} else {
		if( success = ReadControl(vars->HashChainPred, &buf) == CERR_NOERR ) {
			buf->DirtyFlag = TRUE;
			fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)buf) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(FileHdrEnd)));
			fileHdrEndPtr->HashChain = vars->HashChainSucc;
		}
	}
	if( success ) {
		LinkCacheKey(vars->CurDirKey, BLOCK_CONTROL, vars->CurDirBuf);
		LinkParentDir(vars->KeyHashChain, vars->CurDirKey);
		UnlinkCache(vars->CurDirBuf);
	}
	return(success);
}

/*
 * Check for invalid root checksum and zero it its fields.
 */
 
static BOOL ProcessRootChecksum(RootBlockPtr rootBlock, RootBlockEndPtr rootEnd)
{
	register ULONG checksum;
	register ULONG *longPtr;
	
	longPtr = &rootBlock->Ownkey;
	checksum = *longPtr;
	*longPtr++ = 0L;
	checksum += *longPtr;
	*longPtr++ = 0L;
	longPtr++;					/* Skip hash table size field */
	checksum += *longPtr;
	*longPtr = 0L;
	longPtr = (ULONG *) &rootEnd->HashChain;
	checksum += *longPtr;		/* Hash chain */
	*longPtr++ = 0L;
	checksum += *longPtr;		/* Parent dir */
	*longPtr++ = 0L;
	if( !currVolParams.International ) {
		checksum += *longPtr;	/* Extension: Always zero before V2.1 DOS... */
		*longPtr = 0L;
	}
	return(checksum == 0);
}

/*
 * Verifies valid AmigaDOS BSTR filename.
 */

BOOL CheckBSTRFileName(UBYTE *bstr)
{
	return( DoCheckFileName(bstr+1, *bstr) );
}

/*
 * Verifies valid AmigaDOS C-style filename length and characters in name passed.
 */
 
BOOL CheckFileName(TextPtr fileName)
{
	return( DoCheckFileName(fileName, strlen(fileName)) );
}

/*
 * Given a series of bytes and its length, determine whether it is a valid filename.
 */
 
static BOOL DoCheckFileName(register UBYTE *ptr, register UBYTE len)
{
	register BOOL success;
	register WORD i;
	TextChar ch;

	if( success = (len != 0) && ((len > 1) || (*ptr != '*')) && (len < MAX_FILENAME_LEN) ) {
		for( i = 0 ; ((i < len) && success) ; i++ ) {
			ch = *ptr++;
			if( (ch & 0x7F) < 32 ) {
				success = FALSE;
			} else {
				switch(ch) {
				case ':':
				case '/':
					success = FALSE;
				default:
					break;
				}
			}
		}
	}
	return(success);
}

/*
 * Validate the Pascal/BCPL string filename passed.
 * If invalid, change to a dummy name if repair enabled.
 * Returns whether there is a valid filename coming out of here.
 */
 
BOOL ValidateFileName(register UBYTE *fileName, ULONG secType)
{
	BOOL status = TRUE;
	
	
	if( !CheckBSTRFileName(fileName) ) {
		repairStats.CurrentBlockType = secType;
		*fileName = MIN(MAX_FILENAME_LEN, *fileName);
		if( status = ReportError(ANERR_HDR_INVALID_NAME, ANACT_NAME_REPLACED) ) {
			if( options.RepairOpts.Repair ) {
				StoreDummyFileName(fileName);
			} else {
				status = FALSE;
			}
		}
	}
	return(status);
}

/*
 * Validates Pascal/BCPL AmigaDOS file comment length and characters.
 * If repair enabled, comment will be eliminated if invalid.
 */
 
static BOOL ValidateComment(TextPtr commentStr, BufCtrlPtr buf)
{
	register TextPtr ptr = commentStr;
	register UBYTE len;
	register UWORD i;
	register BOOL success = TRUE;
	
	if( len = *ptr++ ) {
		success = len < MAX_COMMENT_LEN;
		for( i = 0 ; (i < len) && success ; i++ ) {
			if( (*ptr++ & 0x7F) < 32 ) {
				success = FALSE;				/* Control characters invalid */
			}
		}
		if( !success ) {
			if( success = ReportError(ANERR_HDR_INVALID_COMMENT, ANACT_COMMENT_DELETED) ) {
				if( options.RepairOpts.Repair ) {
					buf->DirtyFlag = TRUE;
					*commentStr = '\0';
				}
			}
		}
	}
	return(success);
}

#define MAX_DAYS	20000		/* Handles dates until year 2032 */
#define MAX_MINS	24*60
#define MAX_TICKS	60*TICKS_PER_SECOND

/*
 * Validates datestamp for range errors.
 * Returns TRUE if was legal, or the user did not skip or abort the repair.
 */
 
static BOOL ValidateDateTime(struct DateStamp *dsPtr, BufCtrlPtr buf)
{
	BOOL success = TRUE;
	struct DateStamp dateStamp;
	register BOOL future;
	
	DateStamp(&dateStamp);
	if( (((ULONG)dsPtr->ds_Days) > MAX_DAYS) || (((ULONG)dsPtr->ds_Minute) > MAX_MINS) ||
		(((ULONG)dsPtr->ds_Tick) > MAX_TICKS) ) {
		if( success = ReportError(ANERR_HDR_INVALID_DATE, ANACT_DATE_RESET) ) {
			if( options.RepairOpts.Repair ) {
				buf->DirtyFlag = TRUE;
				DateStamp(dsPtr);
			}
		}
	} else {
		future = dsPtr->ds_Days > dateStamp.ds_Days;
		if( (!future) && (dsPtr->ds_Days == dateStamp.ds_Days) ) {
			future = dsPtr->ds_Minute > dateStamp.ds_Minute;
		}
		if( future ) {
			if( success = ReportError(ANERR_FUTURE_DATE, ANACT_DATE_RESET) ) {
				if( options.RepairOpts.Repair ) {
					buf->DirtyFlag = TRUE;
					DateStamp(dsPtr);
				}
			}
		}
	}
	return(success);
}

/*
 * Called only from within ProcessFile() to determine what to do if a data
 * block is cross-linked. For now, just record the problem and delete the
 * offending block. This routine does nothing yet.
 */
 
static void FileKeyBusy(ULONG busyKey)
{
}

/*
 * Someday, maybe we'll keep a list of troublesome blocks...
 */
 
static void SaveBadKey(ULONG badKey)
{
}

/*
 * Checks supplied root list of bitmap keys and possible extensions.
 * Marks bitmap blocks busy in bitmap.
 * Warning - this code MUST execute AFTER the new bitmap is completely built
 * so that we can find free keys for bitmap replacement, if necessary.
 */
 
BOOL ProcessBitMapKeys()
{
	BufCtrlPtr buffer;
	register ULONG *bitMapPtr;
	register WORD count;
	register ULONG num;
	register UBYTE *dataPtr;
	register ULONG key;
	register BOOL cont;
	register BOOL isGoodKey;
	BOOL success;
	WORD bitMapErrFlag = 0;		/* If non-zero, was an error accepted by user */
	RootBlockEndPtr rootBlockEnd;
	
	repairStats.CurrentBlockType = 0;
	if( success = ReadControl(currVolParams.RootBlock, &buffer) == CERR_NOERR ) {
		bitMapPtr = &((RootBlockEndPtr) (((UBYTE *)buffer) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(RootBlockEnd))))->BMPtrs[0];
		dataPtr = blockBuffer;
		num = currVolParams.BitMapBlocks;
		count = 25;
		cont = TRUE;
Loop:	while( (count--) && cont ) {
			if( isGoodKey = ((key = *bitMapPtr) && CheckKey(key)) ) {
				if( ReadBlock(key, dataPtr, FALSE) != CERR_NOERR ) {
					if( cont = ReportError(-ANERR_BITMAP_DISK_ERR, ANACT_KEY_ALLOCATED) ) {
						++bitMapErrFlag;
						goto GetNewBMKey;
					}
				} else {
					if( CalcBlockChecksum((ULONG *)dataPtr) != 0 ) {
						if( cont = ReportError(ANERR_BITMAP_CHECKSUM, ANACT_BITMAP_REDONE) ) {
							++bitMapErrFlag;
						}
					}
					if( cont && (isGoodKey = MarkKeyBusy(key)) ) {
FixKey:				bitMapPtr++;				/* Advance to next bitmap key */
						if( (--num) == 0 ) {		/* Found all active keys yet? */
							while( count-- ) {	/* Yes, look for extra keys. */
								if( *bitMapPtr ) {
									dialogVarNum = *bitMapPtr;
									if( !ReportError(ANERR_BITMAP_EXTRANEOUS_KEY, ANACT_ERROR_FIXED) ) {
										cont &= (!cancelFlag);
										break;
									} else {
										++bitMapErrFlag;
										*bitMapPtr = 0;
									}
								}
								bitMapPtr++;
							}
							if( *bitMapPtr && cont ) {
								
								if( ReportError(ANERR_BITMAP_EXTRANEOUS_EXT_KEY, ANACT_ERROR_FIXED) ) {
									++bitMapErrFlag;
									*bitMapPtr = 0;
								}
							}
							cont = FALSE;
						}
					}	
				}
			} 
			if( !isGoodKey ) {
				dialogVarNum = key;
				isGoodKey = TRUE;
				if( cont = ReportError(-ANERR_BAD_BITMAP_KEY, ANACT_ERROR_FIXED) ) {
GetNewBMKey:	++bitMapErrFlag;
					if( options.RepairOpts.Repair ) {
						currVolParams.LastUsedKey = currVolParams.RootBlock;
						(void) AllocFreeKey();
						*bitMapPtr = currVolParams.LastUsedKey;
					}
					goto FixKey;
				}
			}
		}
		if( cont ) {
			if( ((key = *bitMapPtr) != NULL) && CheckKey(key) && MarkKeyBusy(key) ) {
				if( ReadData(key, &buffer) != CERR_NOERR ) {
					repairStats.BadBlockCount++;
					if( ReportError(-ANERR_BITMAP_EXT_DISK_ERR, ANACT_KEY_ALLOCATED) ) {
						++bitMapErrFlag;
					}
				} else {
					count = currVolParams.BlockSizeL-1;	/* Subtract one for ext */
					bitMapPtr = (ULONG *) (buffer+1);
					goto Loop;
				}
			} else {
				if( ReportError(-ANERR_BAD_BITMAP_EXT_KEY, ANACT_KEY_ALLOCATED) ) {
					++bitMapErrFlag;
				}
			}
			success = AllocBitMapExtension(&buffer, bitMapPtr, num);
		}
		if( bitMapErrFlag && options.RepairOpts.Repair ) {
			rootBlockEnd = (RootBlockEndPtr) (((UBYTE *)buffer) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(RootBlockEnd)));
			rootBlockEnd->BMFlg = 0xFFFFFFFF;			/* New for V2.0.1- helps LoadBitMapKeys() */
			buffer->DirtyFlag = TRUE;
		} else if( !buffer->DirtyFlag ) {
			PutCacheBuffer(buffer);
		}
	}
	return(success);
}

/*
 * Allocate a bitmap extension and link it in.
 */
 
static BOOL AllocBitMapExtension(BufCtrlPtr *buffer, ULONG *bitMapPtr, ULONG key)
{
	BOOL success = TRUE;
	
	if( !cancelFlag ) {
		++repairStats.MajorErrCount;
		if( options.RepairOpts.Repair ) {
			currVolParams.LastUsedKey = currVolParams.RootBlock;
			do {
				if( success = AllocFreeKey() ) {
					*bitMapPtr = currVolParams.LastUsedKey;
					(*buffer)->DirtyFlag = TRUE;
					if( success= (GetCacheBuffer(buffer) == CERR_NOERR) ) {
						bitMapPtr = ((ULONG *)(buffer+1));
						if( success = StoreBitMapKeys(&bitMapPtr, &key, currVolParams.BlockSizeL) ) {
							(*buffer)->DirtyFlag = TRUE;
							LinkCacheKey(currVolParams.LastUsedKey, BLOCK_BITMAP, *buffer);
						}
					}
				}
			} while( key && success );
		}
	}
	return(success);
}

/*
 * Compares new bitmap with old bitmap and writes out new bitmap if they differ.
 */

void RepairBitMap()
{
	ULONG *keyPtr;
	register ULONG *dataPtr;
	register ULONG *ptr;
	register ULONG maxKeys;
	register ULONG count;
	register ULONG num;
	RootBlockEndPtr rootBlockEndPtr;
	BufCtrlPtr buffer;
	register BOOL success;
	UWORD errorCount;
	
/*	repairStats.CurrentBlockType = 0;*/
	errorCount = 0;
	if( LoadBitMapKeys() ) {
		if( !(success = repairStats.BitMapErrFlag) ) {
			dataPtr = bitMapDataPtr;
			keyPtr = currVolParams.BitMapKeyBuf;
			count = 32;
			maxKeys = currVolParams.MaxKeys;			/* Check this many keys */
			while( success = IS_EXT_KEY(*keyPtr) || (ReadBlock(*keyPtr, blockBuffer, FALSE) == CERR_NOERR) ) {
				if( !IS_EXT_KEY(*keyPtr++) ) {
					ptr = (ULONG *) (blockBuffer + sizeof(ULONG));
					num = currVolParams.BlockSizeL - 1;	/* Don't count checksum */
					while( (num--) && (maxKeys >= count) ) {
						if( *dataPtr++ != *ptr++ ) {
							++errorCount;
						}
						if( (maxKeys -= count) == 0 ) {
							goto Match;
						}
					}
					if( maxKeys < count ) {
						break;
					}
				}
			}
			if( success ) {
				num = *ptr ^ *dataPtr;
				count = 0;
				do {
					if( num & (1 << count) ) {
						++errorCount;
					}
					++count;
				} while( --maxKeys );
Match:		if( errorCount ) {
					errorCount = ReportError(ANERR_BITMAP_INCORRECT, ANACT_BITMAP_REDONE);
				}
			}
		}
		if( errorCount && options.RepairOpts.Repair ) {	/* In need of repair? */
			if( WriteBitMap() ) {							/* Replace old bitmap */
				if( ReadControl(currVolParams.RootBlock, &buffer) == CERR_NOERR ) {
					buffer->DirtyFlag = TRUE;				/* Mark reread root dirty */
					rootBlockEndPtr = (RootBlockEndPtr) (((UBYTE *)buffer) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(RootBlockEnd)));
					rootBlockEndPtr->BMFlg = 0xFFFFFFFF;	/* Force bitmap valid */
				}
			}
		}
	}
}
			
/*
 * Reads a control block into the buffer passed.
 */
 
BYTE ReadCtrlBlock(ULONG blockNum, BufCtrlPtr *buffer, BOOL rigorous)
{
	ULONG secType;
	BOOL success;
	UBYTE *dataPtr;
	BYTE err;
		
	if( (err = ReadControl(blockNum, buffer)) == CERR_NOERR ) {
		dataPtr = ((UBYTE *)*buffer) + sizeof(BufCtrl);
		if( rigorous ) {
			success = VerifyCtrlBlock(dataPtr, &secType);
		} else {
			success = CheckCtrlTypes(dataPtr, &secType);
		}
		if( !success ) {
			PutCacheBuffer(*buffer);			/* Bad block...don't keep it! */
			err = CERR_BADTYPE;
		}
	}
	return(err);
}
		
	
/*
 * Performs validation checks on block which claims to be filehdr/userdir.
 * Returns TRUE if successful, and the secondary type in "secType" field.
 */

BOOL VerifyCtrlBlock(UBYTE *block, ULONG *secType)
{
	FileHdrEndPtr fileHdr;
	WORD errors = 0;
	BOOL success;
	ULONG *entriesPtr;
	
	if( success = CheckCtrlTypes(block, secType) ) {
		fileHdr = (FileHdrEndPtr) (block + currVolParams.BlockSize - sizeof(FileHdrEnd));
		entriesPtr = &fileHdr->HashChain;
		errors += CheckEntries(entriesPtr, 3);
/*
	Don't check for keys in the hash table on a softlink, 'cos it's text!
*/
		if( *secType != ST_SOFTLINK ) {
			entriesPtr = (ULONG *)(block + sizeof(RootBlock));
			errors += CheckEntries(entriesPtr, currVolParams.HashTableSize);
		}
		success = errors == 0;
	}
	return(success);
}

/*
 * Verifies valid control block primary and secondary types for block passed.
 * Returns TRUE if valid, and the secondary type is returned.
 */
 
BOOL CheckCtrlTypes(Ptr ptr, ULONG *secType)
{
	BOOL success = FALSE;

	switch( *secType = ((RootBlockEndPtr)(((UBYTE *)ptr) + currVolParams.BlockSize - sizeof(RootBlockEnd)))->SecType) {
	case ST_ROOT:
	case ST_USERDIR:
	case ST_SOFTLINK:
	case ST_LINKDIR:
	case ST_FILE:
	case ST_LINKFILE:
		success = ((RootBlockPtr)ptr)->Type == T_SHORT;
	default:
		break;
	}
	return(success);
}

/*
 * Check a given number of entries for invalid keys and return the error count.
 */
 
static WORD CheckEntries(register ULONG *ptr, register ULONG numEntries)
{
	register WORD errs = 0;
	
	while( numEntries-- ) {
		if( !CheckKey(*ptr++) ) {
			errs++;
		}
	}
	return(errs);
}

/*
 * Checks key to be 0 or in valid range. If so, returns TRUE.
 */
 
BOOL CheckKey(register ULONG key)
{
	return( (key == 0) || ((key <= currVolParams.HighestKey) && (key >= currVolParams.LowestKey)) );
}

/*
 * Generates a dummy name of the form "BadNamennnn" using the current tick count.
 * Replaces the Pascal/BCPL string at the existing location.
 * NOTE: You will probably want to wait until the next tick before calling this
 * again, to avoid duplicate filenames. You can test explicitly, or just Delay(1).
 */
 
static void StoreDummyFileName(TextPtr fileName)
{
	struct DateStamp dateStamp;
	TextChar buff[15];
	register TextPtr ptr;
	
	DateStamp(&dateStamp);
	
	*((ULONG *)&buff[0]) = 0x30303030;		/* Set leading zeros */
	ptr = &buff[3];
	NumToString(dateStamp.ds_Tick, ptr);
	ptr += strlen(ptr);
	ptr -= 4;
	strcpy(strBuff, "BadName,");
	strcat(strBuff, ptr);
	ptr = fileName;
	*ptr = strlen(strBuff);
	BlockMove(strBuff, &ptr[1], *ptr);
}

/*
 * Deletes the bad file whose key is passed.
 */
 
void DeleteFileEntry(ULONG badKey, ULONG parentDirKey, BufCtrlPtr buf)
{
	++repairStats.MajorErrCount;
	if( options.RepairOpts.Repair ) {
		LinkCacheKey(parentDirKey, BLOCK_CONTROL, buf);
		RawDeleteFile(badKey, parentDirKey);
		UnlinkCache(buf);
		skipFlag = TRUE;
	}
}

/*
 * If the boot block does not begin with an AmigaDOS identifier (DosDisk is FALSE),
 * then prompt the user for the correct identifier, and write it out to the
 * boot block. Returns TRUE if the volume ends up with a valid identifier.
 */
 
BOOL CheckBootBlock()
{
	register BOOL success = TRUE;
	
	if( !currVolParams.DosDisk ) {
		++repairStats.MajorErrCount;
		if( success = CheckBootBlockReq() ) {
			success = currVolParams.DosDisk = InitBoot();
		}
	}
	return(success);
}

/*
 * Initialize boot block with the appropriate file system DOS identifier.
 */
 
BOOL InitBoot()
{
	register ULONG newType;
	BufCtrlPtr buffer;
	register ULONG *dataPtr;
	register BOOL success = FALSE;
		
	if( GetCacheBuffer(&buffer) == CERR_NOERR ) {
		PutCacheBuffer(buffer);
		dataPtr = (ULONG *) ((UBYTE *)(buffer+1));
		if( success = (ReadBoot((UBYTE *)dataPtr) == 0) ) {
			newType = ID_DOS_DISK;
			if( currVolParams.FFSFlag ) {
				newType |= 1;
			}
			if( currVolParams.DirCache ) {
				newType |= 4;
			} else if( currVolParams.International ) {
				newType |= 2;
			}
			*dataPtr = newType;
			if( options.RepairOpts.Repair ) {
				success = WriteBoot((ULONG *)dataPtr) == 0;
			}
		}
	}
	return(success);
}
