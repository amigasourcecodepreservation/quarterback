/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 * Quarterback Tools
 * Copyright (c) 1992 New Horizons Software, Inc.
 *
 * About box support routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>

#include <Toolbox/ColorPick.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Image.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Utility.h>

#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/layers.h>

#include <typedefs.h>
#include <string.h>

#include "Tools.h"
#include "Proto.h"

/*
 * External variables
 */

extern ScreenPtr	screen;
extern UBYTE		_tbPenWhite, _tbPenLight, _tbPenBlack, _tbPenDark;
extern WindowPtr	cmdWindow;
extern ImagePtr	iconImage[];
extern UWORD		iconImageData[NUM_ICONS][TOOLS_ICON_SIZE];
extern UWORD		charBaseline, charHeight;
extern UWORD		grayPat[];
extern TextChar	strAboutText[], strLoading[];
extern Icon			icon[];
extern UWORD		iconColors[];

/*
 * Local definitions
 */
 
static WORD index;

static void DrawHappyGuy(RastPtr, RectPtr);
static void DrawDrunkGuy(RastPtr, RectPtr);
static void DrawShockGuy(RastPtr, RectPtr);
static void FillBolt(RastPtr, WORD, PointPtr);

static Point boltPts[11] = {
	{ 155, 47 }, { 170, 27 }, { 164, 27 }, { 172, 11 }, { 166, 11 }, { 173, 0 },
	{ 167,  0 }, { 159, 14 }, { 165, 14 }, { 157, 30 }, { 163, 30 }
};

Icon		happyIcon,	drunkIcon,	shockIcon;
ImagePtr happyImage, drunkImage, shockImage;

/*
 * The comments on the right hand side of happyImageData are made after
 * changing the background from the light color to the dark.
 */
 
static UWORD __chip happyImageData[] = {
	0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFF00,	/* Was 0x7FFF,...,0xFC00 */
	0x8000,0x0000,0x0000,0x0000,0x0300,
	0x8000,0x000E,0x0380,0x0000,0x0300,
	0x8000,0x000E,0x0380,0x0540,0x0300,
	0x800F,0x8000,0x0000,0x0A80,0x0300,
	0x8000,0x0000,0x7005,0x5500,0x0300,
	0x8000,0x0000,0x7002,0xAA00,0x0300,
	0x8000,0x0000,0x0001,0x5400,0x0300,
	0x8000,0x000C,0x0180,0x0000,0x0300,
	0x8000,0x0003,0xFE00,0x0000,0x0300,
	0x8000,0x0000,0x0000,0x0000,0x0300,
	0x8000,0x0000,0x0000,0x0000,0x0300,
	0x8000,0x0000,0x0000,0x0000,0x0300,
	0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFF00,	/* Was 0x7FFF,...,0xFF00 */
	0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFF00,	/* Was 0x3FFF,...,0xFE00 */

	0x8000,0x0000,0x0000,0x0000,0x0300,	/* Was 0x0000,...,0x0000 */
	0x7FFF,0xFFFF,0xFFFF,0xFFFF,0xFD00,
	0x727F,0xFFF1,0xFC7F,0xFFFC,0x9C00,
	0x76FF,0xFFF1,0xFC7F,0xFABE,0xDC00,
	0x76FF,0xFFFF,0xFFFF,0xF57E,0xDC00,
	0x76FF,0xFFFF,0x8FFA,0xAAFE,0xDC00,
	0x76FF,0xFFFF,0x8FFD,0x55FE,0xDC00,
	0x76FF,0xFFFF,0xFFFE,0xABFE,0xDC00,
	0x76FF,0xFFF3,0xFE7F,0xFFFE,0xDC00,
	0x7600,0x007C,0x01F0,0x0000,0xDC00,
	0x77FF,0xFFFF,0xFFFF,0xFFFF,0xDC00,
	0x7000,0x001F,0xFFC0,0x0000,0x1C00,
	0x7FFF,0xFFFF,0xFFFF,0xFFFF,0xFC00,
	0x8000,0x0000,0x0000,0x0000,0x0000,	/* Was all zeros */
	0xC000,0x0000,0x0000,0x0000,0x0100,	/* Was all zeros */


};

static UWORD __chip shockImageData[] = {
	/*
	0x7FFF,0xFFFF,0xFFFF,0xFFFF,0xFC00,
	0x8000,0x0000,0x0000,0x0000,0x0200,
	*/
	0x8000,0x001C,0x01C0,0x0000,0x0300,
	0x8000,0x0022,0x0220,0x0540,0x0300,
	0x800F,0x8022,0x0220,0x0A80,0x0300,
	0x8000,0x001C,0x01C5,0x5500,0x0300,
	0x8000,0x0001,0xFC02,0xAA00,0x0300,
	0x8000,0x0002,0x0201,0x5400,0x0300,
	0x8000,0x0004,0x0100,0x0000,0x0300,
	0x8000,0x0004,0x0100,0x0000,0x0300,
	0x8000,0x0002,0x0200,0x0000,0x0300,
	0x8000,0x0001,0xFC00,0x0000,0x0300,
	/*
	0x8000,0x0000,0x0000,0x0000,0x0300,
	0x7FFF,0xFFFF,0xFFFF,0xFFFF,0xFF00,
	0x3FFF,0xFFFF,0xFFFF,0xFFFF,0xFE00,
	*/
	
	/*
	0x0000,0x0000,0x0000,0x0000,0x0000,
	0x7FFF,0xFFFF,0xFFFF,0xFFFF,0xFC00,
	*/
	0x727F,0xFFE3,0xFE3F,0xFFFC,0x9C00,
	0x76FF,0xFFDD,0xFDDF,0xFABE,0xDC00,
	0x76FF,0xFFDD,0xFDDF,0xF57E,0xDC00,
	0x76FF,0xFFE3,0xFE3A,0xAAFE,0xDC00,
	0x76FF,0xFFFE,0x03FD,0x55FE,0xDC00,
	0x76FF,0xFFFD,0xFDFE,0xABFE,0xDC00,
	0x76FF,0xFFFB,0xFEFF,0xFFFE,0xDC00,
	0x7600,0x007B,0xFEF0,0x0000,0xDC00,
	0x77FF,0xFFFD,0xFDFF,0xFFFF,0xDC00,
	0x7000,0x001E,0x03C0,0x0000,0x1C00,
	/*
	0x7FFF,0xFFFF,0xFFFF,0xFFFF,0xFC00,
	0x0000,0x0000,0x0000,0x0000,0x0000,
	0x0000,0x0000,0x0000,0x0000,0x0000,
	*/
};

static UWORD __chip drunkImageData[] = {
	/*
	0x7FFF,0xFFFF,0xFFFF,0xFFFF,0xFC00,
	0x8000,0x0000,0x0000,0x0000,0x0200,
	*/
	0x8000,0x0004,0x0100,0x0000,0x0300,
	0x8000,0x001F,0x07C0,0x0540,0x0300,
	0x800F,0x8004,0x0100,0x0A80,0x0300,
	0x8000,0x0000,0x7005,0x5500,0x0300,
	0x8000,0x0000,0x7002,0xAA00,0x0300,
	0x8000,0x0000,0x0001,0x5400,0x0300,
	0x8000,0x0003,0xFE00,0x0000,0x0300,
	0x8000,0x000C,0x0180,0x0000,0x0300,
	0x8000,0x0000,0x0000,0x0000,0x0300,
	0x8000,0x0000,0x0000,0x0000,0x0300,
	/*
	0x8000,0x0000,0x0000,0x0000,0x0300,
	0x7FFF,0xFFFF,0xFFFF,0xFFFF,0xFF00,
	0x3FFF,0xFFFF,0xFFFF,0xFFFF,0xFE00,
	*/
	
	/*
	0x0000,0x0000,0x0000,0x0000,0x0000,
	0x7FFF,0xFFFF,0xFFFF,0xFFFF,0xFC00,
	*/
	0x727F,0xFFFB,0xFEFF,0xFFFC,0x9C00,
	0x76FF,0xFFE0,0xF83F,0xFABE,0xDC00,
	0x76FF,0xFFFB,0xFEFF,0xF57E,0xDC00,
	0x76FF,0xFFFF,0x8FFA,0xAAFE,0xDC00,
	0x76FF,0xFFFF,0x8FFD,0x55FE,0xDC00,
	0x76FF,0xFFFF,0xFFFE,0xABFE,0xDC00,
	0x76FF,0xFFFC,0x01FF,0xFFFE,0xDC00,
	0x7600,0x0073,0xFE70,0x0000,0xDC00,
	0x77FF,0xFFFF,0xFFFF,0xFFFF,0xDC00,
	0x7000,0x001F,0xFFC0,0x0000,0x1C00,
	/*
	0x7FFF,0xFFFF,0xFFFF,0xFFFF,0xFC00,
	0x0000,0x0000,0x0000,0x0000,0x0000,
	0x0000,0x0000,0x0000,0x0000,0x0000,
	*/
};	

/*
 * Prepares about box rendering - returns gadget rectangle and created rastport.
 */
 
RastPtr BeginAboutBox(WindowPtr dlg, register RectPtr fullRect, BOOL intro)
{
	register RastPtr rastPtr = dlg->RPort;
	WORD depth = screen->RastPort.BitMap->Depth;
	register WORD x, y, i;
	
	index = 0;
	InsetRect(fullRect, 1, 1);

	x = WIDTH(fullRect);
	y = HEIGHT(fullRect) - 15;

	if( intro ) {
		happyIcon.Height = 15;
		happyIcon.Width = drunkIcon.Width = 72;
		happyIcon.Depth = drunkIcon.Depth = 2;
		happyIcon.Mask = drunkIcon.Mask = NULL;
		happyIcon.Data = (PLANEPTR) &happyImageData[0];
		happyIcon.ColorTable = drunkIcon.ColorTable = iconColors;
	
		drunkIcon.Height = 10;
		drunkIcon.Data = (PLANEPTR) &drunkImageData[0];
		shockIcon = drunkIcon;
		shockIcon.Data = (PLANEPTR) &shockImageData[0];

		SetAPen(rastPtr, _tbPenBlack);
		SetFont(rastPtr, screen->RastPort.Font);
		/*
		i = strlen(strLoading);
		Move(rastPtr, fullRect->MinX + ((x - TextLength(rastPtr, strLoading, i)) >> 1),
			fullRect->MinY + (y >> 1) + charHeight );
		Text(rastPtr, strLoading, i);
		*/
		rastPtr = NULL;
		
	} else {
		SetAPen(rastPtr, _tbPenDark);
		FillRect(rastPtr, fullRect);
		happyImage = MakeIconImage(&happyIcon, happyIcon.Width, happyIcon.Height, ICON_BOX_NONE);
		DrawHappyGuy(rastPtr, fullRect);
		drunkImage = MakeIconImage(&drunkIcon, drunkIcon.Width, drunkIcon.Height, ICON_BOX_NONE);
		shockImage = MakeIconImage(&shockIcon, shockIcon.Width, shockIcon.Height, ICON_BOX_NONE);

		if( (rastPtr = CreateRastPort(x, y, depth)) != NULL ) {

			x = ((x - 8) >> 1) - boltPts[0].x;
			for( i = 0 ; i < 11 ; i++ ) {
				boltPts[i].x += x;
			}
			boltPts[0].y = y;
			y /= 3;
			boltPts[1].y = boltPts[2].y = (y + y) - 3;
			boltPts[3].y = boltPts[4].y = y - 3;
			boltPts[7].y = boltPts[8].y = y;
			y += y;
			boltPts[9].y = boltPts[10].y = y;
	
			SetAPen(rastPtr, _tbPenBlack);
			SetDrMd(rastPtr, JAM1);
			SetFont(rastPtr, screen->RastPort.Font);
		}
	}
	return(rastPtr);
}

/*
 * Draw happy Amiga.
 */
 
static void DrawHappyGuy(RastPtr rastPtr, RectPtr rectPtr)
{
	if( happyImage != NULL ) {
		DrawImage(rastPtr, happyImage, ((WIDTH(rectPtr) - 72) >> 1) + rectPtr->MinX, rectPtr->MaxY - 14);
	}
}

/*
 * Draw drunk Amiga.
 */
 
static void DrawDrunkGuy(RastPtr rastPtr, RectPtr rectPtr)
{
	if( drunkImage != NULL ) {
		DrawImage(rastPtr, drunkImage, ((WIDTH(rectPtr) - 72) >> 1) + rectPtr->MinX, rectPtr->MaxY - 12);
	}
}

/* 
 * Draw shocked Amiga.
 */
 
static void DrawShockGuy(RastPtr rastPtr, RectPtr rectPtr)
{
	if( shockImage != NULL ) {
		DrawImage(rastPtr, shockImage, ((WIDTH(rectPtr) - 72) >> 1) + rectPtr->MinX, rectPtr->MaxY - 12);
	}
}

/*
 * Deallocates structures of BeginAboutBox().
 */
 
void EndAboutBox(RastPtr rastPtr)
{
	if( rastPtr != NULL ) {
		SetClip(rastPtr->Layer, NULL);
		DisposeRastPort(rastPtr);
	}
	if( happyImage != NULL ) {
		FreeImage(happyImage);
		happyImage = NULL;
	}
	if( drunkImage != NULL ) {
		FreeImage(drunkImage);
		drunkImage = NULL;
	}
	if( shockImage != NULL ) {
		FreeImage(shockImage);
		shockImage = NULL;
	}
}

/*
 * Draw a single animation frame for the about box.
 */

void DrawABoxFrame(register RastPtr rPort, WORD fWidth, WORD fHeight)
{
	register WORD n;
	register WORD temp;
	register WORD x, y;
	Rectangle rect;
	/*RegionPtr myRegion = NULL;*/
	BYTE backPen = /*_tbPenDark == _tbPenBlack ? _tbPenLight :*/ _tbPenDark;
	
	if( index == (432+12+(fWidth >> 1)) ) {
		index = 0;
	}
	
	if( (index == 0) || (index == 431) ) {
		SetRast(rPort, backPen);
	}
	n = ++index;
	if( n >= 432 ) {
		if( (n & 3) == 0 ) {
			n -= 432;

			rect.MinX = (fWidth >> 1) - n;
			rect.MaxX = (fWidth >> 1) + n;
			rect.MinY = 0;
			rect.MaxY = fHeight;
			SetRectClip(rPort->Layer, &rect);

			if( n & 4 ) {
				SetRast(rPort, _tbPenBlack);
				SetAPen(rPort, _tbPenLight);
			} else {
				SetRast(rPort, _tbPenLight);
				SetAPen(rPort, _tbPenBlack);
			}
			n = 0;
			temp = strlen(strAboutText);
			x = (fWidth - TextLength(rPort, strAboutText, temp)) >> 1;
			while( n < fHeight ) {
				Move(rPort, x, n + charBaseline);
				Text(rPort, strAboutText, temp);
				n += charHeight;
			}
		}
	} else if( n > 200 ) {
		SetRast(rPort, backPen);
		temp = n - 316;
		for( n = 0 ; n < NUM_ICONS ; n++ ) {
			x = ((fWidth - iconImage[n]->Width) >> 1);
			y = fHeight - (iconImage[n]->Height + 51);
			switch(n) {
			case 1:
				x += temp - 112;
				y += temp >> 1;
				break;
			case 0:
				x += temp - 112;
				y += 27 + (temp >> 2);
				break;
			case 3:
				x += (112 - temp);
				y += temp >> 1;
				break;
			case 2:
				x += (112 - temp);
				y += 27 + (temp >> 2);
				break;
			}
			DrawImage(rPort, iconImage[n], x, y);
		}
	} else if( n > 139 ) {
		SetRast(rPort, backPen);
		switch( (n & 6) >> 1 ) {
		case 0:
			FillBolt(rPort, 4, &boltPts[4]);
			break;
		case 1:
			FillBolt(rPort, 8, &boltPts[2]);
			break;
		case 2:
		case 3:
			FillBolt(rPort, 11, &boltPts[0]);
			break;
		}
	}
}

/*
 * Fill in a lightning bolt polygon into the given rastport.
 * Since the Toolbox unmercifully turns outlining off, we must code this ourselves.
 */
 
static void FillBolt(register RastPtr rPort, WORD numPts, register PointPtr pts)
{
	register WORD width = 19 + (16+2);
	register WORD height = pts[0].y + (1+2);
	UWORD *areaBuff;
	PLANEPTR planePtr;
	struct AreaInfo areaInfo;
	struct TmpRas tmpRas;
	register WORD i;
/*
	Do fill
*/
	SetAPen(rPort, _tbPenWhite);
	SetOPen(rPort, _tbPenBlack);
	if ((areaBuff = MemAlloc((numPts + 1)*5, MEMF_CLEAR)) != NULL) {
		if ((planePtr = AllocRaster(width, height)) != NULL) {
			InitArea(&areaInfo, areaBuff, numPts + 1);
			InitTmpRas(&tmpRas, planePtr, RASSIZE((LONG) width, (LONG) height));
			rPort->AreaInfo = &areaInfo;
			rPort->TmpRas = &tmpRas;
			AreaMove(rPort, pts[0].x, pts[0].y);
			for (i = 1; i < numPts; i++)
				AreaDraw(rPort, pts[i].x, pts[i].y);
			AreaEnd(rPort);
			WaitBlit();
			FreeRaster(planePtr, width, height);
			rPort->AreaInfo = NULL;
			rPort->TmpRas = NULL;
		}
		MemFree(areaBuff, (numPts + 1)*5);
	}
	BNDRYOFF(rPort);
}

/*
 * Draw the next frame in the sequence and wait for vertical beam sync
 */
 
void NextABoxFrame(RastPtr rPort, register RastPtr windowRPort, register RectPtr rect)
{
	register WORD clipWidth = WIDTH(rect);
	register WORD clipHeight = HEIGHT(rect);
	register WORD n = index;
	
	DrawABoxFrame(rPort, clipWidth, clipHeight);
	WaitBOVP(&screen->ViewPort);
	BltBitMapRastPort(rPort->BitMap, 0, 0, windowRPort, rect->MinX, rect->MinY, clipWidth, clipHeight - 15, 0xC0);
	if( n == 433 ) {
		DrawHappyGuy(windowRPort, rect);
	} else if( n == 200 ) {
		DrawDrunkGuy(windowRPort, rect);
	} else if( n == 142 ) {
		DrawShockGuy(windowRPort, rect);
	}
}
