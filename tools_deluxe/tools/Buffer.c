/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 * Buffer routines
 */

#include <exec/types.h>
#include <exec/lists.h>
#include <exec/nodes.h>
#include <dos/dosextens.h>

#include <proto/exec.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Utility.h>

#include <string.h>
#include <typedefs.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	External variables
 */

extern BufCtrlPtr 		emptyQueue;
extern struct List		busyQueue;
extern UBYTE			*cacheHashBuffer;
extern BYTE				cacheErr;
extern VolParams		currVolParams;
extern ULONG			remapBufSize, extentBufferSize;
extern BOOL				decimalLabel;
extern TextPtr			dialogVarStr;
extern UBYTE			*blockBuffer, *extentBuffer, *remapBuffer, *blockCacheBuffer;
extern ULONG			*badBlockBuffer;
extern OptionsRec		options;
extern TextChar		A2090DevSuffix[];

/*
 * Local definitions and prototypes
 */
 
static void		CorrectBlockChecksum(BufCtrlPtr);
static ULONG	GetRealMemType(void);
static BOOL		AllocateCacheBuffers(ULONG);
static ULONG	CalcMaxBuffers(void);
static void		RemoveFromBusyQueue(NodePtr);

/*
 * Finds a free cache buffer. If none on free list, uses oldest from busy
 * list, forcing write, if necessary.
 * Returns pointer to buffer control structure (actual data comes after),
 * or NULL if failure.
 */
 
BYTE GetCacheBuffer(BufCtrlPtr *buffer)
{
	register BufCtrlPtr buf;
	
	cacheErr = CERR_NOERR;
	if( (buf = emptyQueue) != NULL ) {
		emptyQueue = buf->Next;
	} else {
		if( (buf = (BufCtrlPtr) RemTail(&busyQueue)) == NULL ) {
			cacheErr = CERR_FATAL;
			Error(ERR_FATAL_CACHE);
		} else {
			UnlinkCacheHash(buf);
			if( buf->DirtyFlag ) {
				cacheErr = WriteBlockChecksum(buf);
			}
		}
	}
	if( (cacheErr == CERR_NOERR) && (buf != NULL) ) {
		BlockClear(buf, sizeof(BufCtrl));
	}
	*buffer = buf;
	return(cacheErr);
}

/*
 * Returns cache buffer passed to EmptyQueue.
 */
 
void PutCacheBuffer(register BufCtrlPtr buf)
{
	if( buf != NULL ) {
		if( buf->Ownkey ) {
			UnlinkCacheHash(buf);
		}
		if( buf->Next != NULL ) {
			RemoveFromBusyQueue((NodePtr)buf);
		}
		buf->Next = emptyQueue;
		emptyQueue = buf;
	}
}

/*
 * A la George's code, remove node and clear its links.
 */
 
static void RemoveFromBusyQueue(NodePtr node)
{
	Remove(node);
	node->ln_Pred = NULL;
	node->ln_Succ = NULL;
}

/*
 * Enter with key passed to read a block from cache or from disk.
 * (Each entrance is for a different type of block; 0=data, 1=special, -1=control)
 * Returns error code, or zero if successful.
 * If successful, buffer is changed to point to a buffer location.
 */
 
BYTE ReadSpecial(ULONG key, BufCtrlPtr *buffer)
{
	return( ReadCache(key, BLOCK_SPECIAL, buffer) );
}

BYTE ReadControl(ULONG key, BufCtrlPtr *buffer)
{
	return( ReadCache(key, BLOCK_CONTROL, buffer) );
}

BYTE ReadData(ULONG key, BufCtrlPtr *buffer)
{
	return( ReadCache(key, BLOCK_DATA, buffer) );
}

/*
 * Given a key, finds block in cache or reads from disk and
 * links into cache hash and busy lists.
 * Block types are: 0=data, -1= control 1=special.
 * If type is data and FFS, don't check checksum.
 * Returns error code, zero if successful.
 * If successful, "buffer" is changed to point to a buffer location.
 */
 
BYTE ReadCache(ULONG key, WORD blockType, BufCtrlPtr *buffer)
{
	register BufCtrlPtr bufCtrlPtr;
	BufCtrlPtr buf;
	UBYTE *dataPtr;

	cacheErr = CERR_NOERR;
	if( bufCtrlPtr = ScanCache(key) ) {
		RemoveFromBusyQueue((NodePtr)bufCtrlPtr);	/* Unlink from present pos... */
		AddHead(&busyQueue, (NodePtr)bufCtrlPtr);	/* ...and requeue at head. */
		bufCtrlPtr->Ownkey = key;
		bufCtrlPtr->BufType = blockType;
	} else {
		if( (cacheErr = GetCacheBuffer(&buf)) == CERR_NOERR ) {
			dataPtr = (UBYTE *)(buf+1);
			if( ReadBlock(key, dataPtr, FALSE) == 0 ) {
				if( (blockType < BLOCK_DATA) ||
					 ((blockType == BLOCK_DATA) && !currVolParams.FFSFlag) ) {
					if( CalcBlockChecksum((ULONG *)dataPtr) ) {
						cacheErr = CERR_CHECKSUM;	/* Warning: Remember to put it...*/
					}										/* back in cache if user cancels */
				}
				bufCtrlPtr = buf;
				LinkCacheKey(key, blockType, bufCtrlPtr);
			} else {
				cacheErr = CERR_IOERR;
				PutCacheBuffer(buf);
			}
		}
	}
	*buffer = bufCtrlPtr;
	return(cacheErr);
}
				
/*
 * Forces dirty cache buffers in the busyQueue to be written to disk, then 
 * marked clean.  Cache buffers are NOT requeued to emptyQueue.
 * Note: finds LOWEST key for write, so that heads move in one direction only.
 *
 * New note: If an extent buffer can be allocated, it blocks all the consecutive 
 * cached blocks it can into it in order to speed reorganize. This will speed the
 * reorganize substantially over V4.x.
 */
 
BYTE WriteCache()
{
	register BufCtrlPtr buf;
	register ULONG num;
	register BYTE err = 0;
	BufCtrlPtr firstBuffer;
	register UBYTE *ptr;
	register WORD keyCount = 0;
	register ULONG key;
	ULONG startKey;
	
	do {
		buf = (BufCtrlPtr) &busyQueue;
		num = 0xFFFFFFFF;							/* First entry will be the lowest */

		while( (buf = buf->Next)->Next != NULL ) {
			if( buf->DirtyFlag ) {				/* If not dirty, don't write */
				if( buf->Ownkey < num ) {		/* If not lowest, ignore */
					num = buf->Ownkey;
					firstBuffer = buf;
				}
			}
		}
		
		if( num != 0xFFFFFFFF ) {				/* Anything dirty in list? */
			buf = firstBuffer;
			
			if( blockCacheBuffer == NULL ) {
				err = WriteBlockChecksum(buf);
			} else {
				if( keyCount == 0 ) {
					ptr = blockCacheBuffer;
					key = startKey = buf->Ownkey;
				} else if( (buf->Ownkey != key) || (keyCount == currVolParams.HashTableSize) ) {
					ptr = blockCacheBuffer;
					key = buf->Ownkey;
					err = WriteMulti(startKey, keyCount, ptr, FALSE);
					startKey = key;
					keyCount = 0;
				}
				CorrectBlockChecksum(buf);
				buf->DirtyFlag = FALSE;
				CopyMemQuick(buf+1, ptr, currVolParams.BlockSize);
				ptr += currVolParams.BlockSize;
				key++;
				keyCount++;
			}
		} else if( keyCount ) {
			err = WriteMulti(startKey, keyCount, blockCacheBuffer, FALSE);
		}
	} while( (num != 0xFFFFFFFF) && (err == 0) );
	
	return(err);
}

/*
 * Correct the checksum of a block if necessary.
 */
 
static void CorrectBlockChecksum(BufCtrlPtr buffer)
{
	register LONG checksum;
	register WORD blockType;
	register ULONG *dataPtr;
	
	dataPtr = (ULONG *)(buffer + 1);
	blockType = buffer->BufType;
	if( (blockType < BLOCK_DATA) || ((blockType == BLOCK_DATA) && !currVolParams.FFSFlag) ) {
		checksum = CalcBlockChecksum(dataPtr);
		if( blockType == BLOCK_BITMAP ) {
			*dataPtr -= checksum;			/* Bitmap stores cksum in first long */
		} else {
			((RootBlockPtr)dataPtr)->CkSum -= checksum;
		}
	}
}

/*
 * Writes buffer passed to volume, after calculating a new checksum, depending on
 * type of block.
 */

BYTE WriteBlockChecksum(BufCtrlPtr buffer)
{
	BYTE err = CERR_BADTYPE;
	
	if( buffer != NULL ) {
		CorrectBlockChecksum(buffer);
		err = WriteBuffer(buffer);
	}
	return(err);
}

/*
 * Write a buffer, with interface prompts if necessary upon errors.
 */
 
BYTE WriteBuffer(register BufCtrlPtr buffer)
{
	BOOL retry = FALSE;
	BYTE err;
	UBYTE numBuff[16];
		
	do {
		err = WriteBlock(buffer->Ownkey, (ULONG *)(buffer+1), FALSE);
		if( err == CERR_NOERR ) {
			buffer->DirtyFlag = FALSE;
		} else {
			err = CERR_IOERR;
			if( IsWriteProtected() ) {		/* Error! Was it write-protected? */
				if( PromptForWriteProtected() ) {
					retry = TRUE;
				}
			} else {
				OutputStatHexOrDecString(buffer->Ownkey, numBuff, decimalLabel);
				dialogVarStr = numBuff;
				ReportInfo(ERR_WRITE_BLOCK);
			}
		}
	} while( retry );
	return(err);
}
		
	
/*
 * Given a key, return pointer to its cache buffer or NULL if failure.
 */
 
BufCtrlPtr ScanCache(ULONG key)
{
	register UWORD index;
	register BufCtrlPtr buf = NULL;
	
	if( (cacheHashBuffer != NULL) && (key != 0L) ) {
		index = (CACHE_MODULO & key) << 2;
		buf = *((BufCtrlPtr *)(&cacheHashBuffer[index]));
		while( (buf != NULL) && (buf->Ownkey != key) ) {
			buf = (BufCtrlPtr)buf->HashChain;
		}
	}
	return(buf);
}

/*
 * Calculates new checksum for block passed.
 * Note: should be longword-aligned.
 */
 
ULONG CalcBlockChecksum(register ULONG *ptr)
{
	register ULONG numLongs = currVolParams.BlockSizeL >> 3;
	register ULONG sum = 0L;
	
	while( numLongs-- ) {
		sum += *ptr++;		sum += *ptr++;
		sum += *ptr++;		sum += *ptr++;
		sum += *ptr++;		sum += *ptr++;
		sum += *ptr++;		sum += *ptr++;
	}
	return(sum);
}

/*
 * Relink all cache buffers from busyQueue to emptyQueue and zap the
 * CacheHashBuffer.
 */

void FlushCache()
{
	register BufCtrlPtr buffer;
	register BufCtrlPtr *ptr = &emptyQueue;

	/*
	while( (buffer = (BufCtrlPtr) RemHead(&busyQueue)) != NULL ) {
	*/
	if( busyQueue.lh_TailPred != NULL ) {
		while( (ULONG)&busyQueue != (ULONG)busyQueue.lh_TailPred ) {
			buffer = (BufCtrlPtr) RemHead(&busyQueue);
			buffer->Next = *ptr;
			*ptr = buffer;
		}
	}
	if( cacheHashBuffer != NULL ) {
		BlockClear(cacheHashBuffer, CACHE_HASH_SIZE);
	}
}

/* Given a key, links cache buffer passed into proper cache hash chain,
 * and also into busy queue.
 * Call LinkCache() instead if a buffer's Ownkey is valid (previously in list).
 */

void LinkCacheKey(ULONG key, WORD bufType, BufCtrlPtr buffer)
{
	buffer->Ownkey = key;
	buffer->BufType = bufType;
	LinkCache(buffer);
}

/*
 * Link the buffer passed into the busy queue.
 */
 
void LinkCache(register BufCtrlPtr buffer)
{
	register WORD index;
	
	if( (buffer != NULL) && (cacheHashBuffer != NULL) ) {
		index = (CACHE_MODULO & buffer->Ownkey) << 2;
		buffer->HashChain = *((ULONG *)(&cacheHashBuffer[index]));
		*((BufCtrlPtr *)(&cacheHashBuffer[index])) = buffer;
		AddHead(&busyQueue, (NodePtr)buffer);
	}
}

/*
 * Unlinks cache buffer from the busyQueue as well as cache list.
 */
 
void UnlinkCache(register BufCtrlPtr buffer)
{
	if( buffer != NULL ) {
		if( buffer->Next != NULL ) {
			RemoveFromBusyQueue((NodePtr)buffer);
		}
		UnlinkCacheHash(buffer);
	}
}

/*
 * Searches cache buffer chain for data buffer passed to find parent, so it can
 * unlink buffer from chain.
 */

void UnlinkCacheHash(register BufCtrlPtr buffer)
{ 	
	WORD index;
	ULONG nextHash;
	register ULONG *hashChainPtr;
	
	if( (buffer != NULL) && (cacheHashBuffer != NULL) ) {
		nextHash = buffer->HashChain;						/* Save forward link */
		index = (CACHE_MODULO & buffer->Ownkey) << 2;
		hashChainPtr = ((ULONG *)(&cacheHashBuffer[index]));
		
		while( (*hashChainPtr != NULL) && (*hashChainPtr != (ULONG)buffer) ) {
			hashChainPtr = &(((BufCtrlPtr)*hashChainPtr)->HashChain);
		}
		if( *hashChainPtr != NULL ) {
			*hashChainPtr = nextHash;						/* Fix predecessor's link */
		}
	}
}

/*
 *	Allocate track and return number of blocks allocated. If this fails,
 * just use the one block buffer in "blockBuffer" instead.
 */

UBYTE *AllocTrack(register LONG *numBlocks)
{
	register UBYTE *buff = NULL;

	*numBlocks = MIN(MAX_TRACK_BUFS, currVolParams.Envec.de_BlocksPerTrack);

	while( *numBlocks > 1 ) {
		if( (buff = AllocIOBuffer((*numBlocks) << currVolParams.BlockSizeShift)) != NULL ) {
			break;
		}
		*numBlocks >>= 1;
	}
	return(buff);
}

/*
 *	AllocBuffer
 *	Allocates a buffer the size of the current drive's
 *	block size using the current drive's specified memory type.
 */

UBYTE *AllocBuffer()
{
	return( AllocIOBuffer(currVolParams.BlockSize) );
}

/*
 * Allocates a buffer of the size specified using the
 * current drive's specified memory type.
 */
 
UBYTE *AllocIOBuffer(ULONG size)
{
	UBYTE *ptr;
	
	if( ptr = MemAlloc(size, GetRealMemType() | MEMF_CLEAR) ) {
		if( ((ULONG)ptr) != (((ULONG)ptr) & currVolParams.Envec.de_Mask) ) {
			MemFree(ptr, size);
			ptr = MemAlloc(size, MEMF_CHIP | MEMF_CLEAR);
		}
	}
	return(ptr);
}

/*
 * Convert the current buffer memory type into a type which takes into
 * account a mask that is not 0xFFFFFFFF.
 */
 
static ULONG GetRealMemType()
{
	register ULONG memType = currVolParams.Envec.de_BufMemType;
	
	if( (currVolParams.Envec.de_Mask & 0xFF000000) == 0 ) {
		if( SystemVersion() >= OSVERSION_2_0_4 ) {
			memType |= MEMF_24BITDMA;
		} else {
			memType |= MEMF_CHIP;
		}
	}
	return(memType);
}

/*
 *	Deallocates a buffer the size of the current drive's
 *	block size and allocated by AllocBuffer().
 * Checks for NULL and returns immediately if so.
 * Zeros the buffer variable after it has finished deallocating.
 */

void FreeBuffer(UBYTE **bufferHandle)
{
	FreeIOBuffer(bufferHandle, currVolParams.BlockSize);
}

/*
 * Matching deallocation of AllocIOBuffer().
 * By passing the address of the memory pointer, it can be automatically
 * cleared out.
 */
 
void FreeIOBuffer(register UBYTE **bufferHandle, ULONG size)
{
	if( (bufferHandle != NULL) && (*bufferHandle != NULL) ) {
		MemFree(*bufferHandle, size);
		*bufferHandle = NULL;
	}
}

/*
 * Allocate the scratch buffer for disk I/O
 */
 
BOOL AllocateBlock()
{
	return( (blockBuffer = AllocBuffer()) != NULL);
}

/*
 * Free the scratch buffer for disk I/O
 */
 
void FreeBlock()
{
	FreeBuffer(&blockBuffer);
}

/*
 * Allocates an I/O buffer large enough to hold the entire extent.
 * Saves size and pointer locally for later release.  Note: this code
 * needs to take into account the LowMem option?
 */

BOOL AllocateExtBuffer()
{
	FreeExtBuffer();
	extentBufferSize = currVolParams.BlockSize * currVolParams.HashTableSize;
	return( (extentBuffer = AllocIOBuffer(extentBufferSize)) != NULL);
}

/*
 * Matching free for above...
 */
 
void FreeExtBuffer()
{
	FreeIOBuffer(&extentBuffer, extentBufferSize);
}

/*
 * Allocates a buffer to hold keys of known bad blocks.
 */
 
BOOL AllocateBadBlockBuffer()
{
	FreeBadBlockBuffer();
	return( (badBlockBuffer = (ULONG *)AllocIOBuffer((currVolParams.MaxBadBlocks << 2) + 4)) != NULL);
}

/*
 * Matching free for above...
 */

void FreeBadBlockBuffer()
{
	FreeIOBuffer( (UBYTE **) &badBlockBuffer, (currVolParams.MaxBadBlocks << 2) + 4);
}

/*
 * Allocates a buffer of a specified size for the remapping buffer.
 */
 
BOOL AllocRemapBuffer(ULONG size)
{
	FreeRemapBuffer();
	remapBufSize = size;
	return( (remapBuffer = AllocIOBuffer(size)) != NULL);
}

/*
 * Matching free for above...
 */
 
void FreeRemapBuffer()
{
	FreeIOBuffer(&remapBuffer, remapBufSize);
}	

/*
 * The TOOLS cache buffer scheme works this way: each cache buffer consists
 * of a control area plus the data area.  The control area permits linking
 * the buffer into either a "free" list or a "busy" list, as well as a hash 
 * chain for fast access by key.  The control area also contains other useful
 * data such as ownkey, contents type, etc.
 *
 * Returns FALSE if unable to allocate all buffers as requested.
 */
 
#define MIN_DESIRED_BUFS	(72+3)	/* Extent size plus a few more */

BOOL AllocateCache(BOOL max)
{
	register LONG numBlocks;
	BOOL success;
	
	FreeCache();
	if( max ) {
		numBlocks = CalcMaxBuffers();
/*
	Leave at least 32K untouched for others, to be safe
*/
		if( numBlocks < 66 ) {
			numBlocks = 2;
		} else {
			numBlocks -= 64;
			if( numBlocks > MIN_DESIRED_BUFS ) {
				numBlocks = MIN_DESIRED_BUFS + (((numBlocks - MIN_DESIRED_BUFS) * options.PrefsOpts.MemoryUsage) / 100);
			}
			numBlocks = MIN(numBlocks, 1000);
			/*
			if( numBlocks == 0 ) {
				numBlocks++;
			} else if( numBlocks > 1000 ) {
				numBlocks = 1000;
			}
			*/
		}
	} else {
		numBlocks = 2;
	}
	if( !(success = AllocateCacheBuffers(numBlocks)) ) {
		FreeCache();
	}
	return( success );
}

/*
 * Allocate "numBlocks" blocks into the cache, return TRUE if successful.
 * Returns FALSE if, upon exit, no cache buffers are currently allocated.
 */

static BOOL AllocateCacheBuffers(register ULONG maxNum)
{
	register ULONG num = maxNum;
	register BOOL success = num != 0;
	register BufCtrlPtr *ptr;
	register BufCtrlPtr buf;
	register ULONG size = currVolParams.BlockSize + sizeof(BufCtrl);
	register ULONG memType = GetRealMemType();
	
	if( success && (cacheHashBuffer == NULL) ) {
		NHInitList(&busyQueue);
		ptr = &emptyQueue;
		*ptr = NULL;
		if( success = (cacheHashBuffer = AllocIOBuffer(CACHE_HASH_SIZE)) != NULL ) {
			while( num ) {
/*
	Because of the great number of blocks that can be allocated, speed is of the
	essence. We do not need these blocks cleared, either, so we will not call
	AllocIOBuffer() and instead call AllocMem().
*/
				buf = (BufCtrlPtr) MemAlloc(size, memType);
				if( buf != NULL ) {
					if( ((ULONG)buf) != (((ULONG)buf) & currVolParams.Envec.de_Mask) ) {
						MemFree(buf, size);
						buf = (BufCtrlPtr) MemAlloc(size, MEMF_CHIP);
					}
				}
				if( buf != NULL ) {
					buf->Next = *ptr;
					*ptr = buf;
				} else {
/*
	To expedite the loop, do an unstructured "break" here. Also, since memory
	could be fragmented, failure to allocate the maximum number is not an
	error condition, unless you failed to allocate ANY!
*/
					success = num != maxNum;
					break;
				}
				num--;
			}
		}
	}
	return(success);
}

/*
 * Free all buffers allocated by AllocateCache() or AllocateCacheBuffers().
 */
 
void FreeCache()
{
	register BufCtrlPtr *ptr;
	register BufCtrlPtr top;
	register ULONG size = currVolParams.BlockSize + sizeof(BufCtrl);
	
	FlushCache();							/* Force all cache buffers to EmptyQueue. */
	
	if( (ptr = &emptyQueue) != NULL ) {
		while( (top = *ptr) != NULL ) {	/* Get top item in queue. */
			*ptr = top->Next;					/* Unlink it. */
			MemFree(top, size);				/* Don't need to zap "top" or check for NULL */
		}
	}
	if( cacheHashBuffer != NULL ) {
		FreeIOBuffer(&cacheHashBuffer, CACHE_HASH_SIZE);
	}
}

/*
 * Calculate the maximum number of cache buffers that can be allocated now.
 */
 
static ULONG CalcMaxBuffers()
{
	FreeCache();
	return( AvailMem(GetRealMemType()) / (currVolParams.BlockSize + sizeof(BufCtrl)) );
}	

/*
 * Apparently, this is a kludge for a 2090 bug. 
 * I added the test for "-ddisk.device" myself.
 */
 
void ForceRead()
{
	register WORD kludgeCount;
	
	if( strcmp(&currVolParams.ExecDevName[1], A2090DevSuffix) == 0 ) {
		kludgeCount = 6;
		while( kludgeCount-- && (ReadBlock(currVolParams.RootBlock, blockBuffer, FALSE) != 0) );
	}
}

