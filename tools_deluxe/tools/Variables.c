/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software
 *
 *	Global variables
 */

#include <exec/types.h>
#include <libraries/dos.h>
#include <intuition/intuition.h>
#include <devices/console.h>
#include <dos/filehandler.h>
#include <dos/dosextens.h>

#include <Toolbox/BarGraph.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Image.h>
#include <Toolbox/List.h>
#include <Toolbox/Language.h>
#include <Toolbox/ScrollList.h>

#include "Tools.h"

struct Device			*ConsoleDevice = NULL;
struct Library			*LayersBase = NULL;
struct IntuitionBase		*IntuitionBase = NULL;
struct GfxBase			*GfxBase = NULL;
struct IconBase		*IconBase = NULL;
struct Library			*WorkbenchBase = NULL;
struct RexxLib			*RexxSysBase = NULL;
struct Library			*DiskBase = NULL;

APTR	SaveVec = NULL;

/*
 *	Font information
 */

TextFontPtr	smallFont;


/*
 *	Function key menu command and AREXX command tables (without/with shift key)
 */

TextChar macroNames1[10][MACRONAME_LEN];
TextChar macroNames2[10][MACRONAME_LEN] = {
	"Macro_1", "Macro_2", "Macro_3", "Macro_4", "Macro_5",
	"Macro_6", "Macro_7", "Macro_8", "Macro_9", "Macro_10"
};

FKey	fKeyTable1[] = {
	{ FKEY_MENU, (Ptr) MENUITEM(TOOLS_MENU,	VOL_STAT_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(TOOLS_MENU,	VOL_STAT_ITEM2, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU,	SAVEAS_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU,	PAGESETUP_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU,	PRINT_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(TOOLS_MENU,	VOL_REPAIR_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(TOOLS_MENU,	VOL_RECOVER_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(TOOLS_MENU,	VOL_OPTIMIZE_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(TOOLS_MENU,	VOL_EDITOR_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(TOOLS_MENU,	TOOLS_SELECT_ITEM, NOSUB) },
};

#if (AMERICAN | BRITISH | FRENCH)
#define strMacroNames2 macroNames2
#else
extern TextChar strMacroNames2[10][32];
#endif

FKey	fKeyTable2[] = {
	{ FKEY_MACRO, &strMacroNames2[0][0] },
	{ FKEY_MACRO, &strMacroNames2[1][0] },
	{ FKEY_MACRO, &strMacroNames2[2][0] },
	{ FKEY_MACRO, &strMacroNames2[3][0] },
	{ FKEY_MACRO, &strMacroNames2[4][0] },
	{ FKEY_MACRO, &strMacroNames2[5][0] },
	{ FKEY_MACRO, &strMacroNames2[6][0] },
	{ FKEY_MACRO, &strMacroNames2[7][0] },
	{ FKEY_MACRO, &strMacroNames2[8][0] },
	{ FKEY_MACRO, &strMacroNames2[9][0] },
};

BOOL			cancelFlag;		/* Operation cancelled? */
BOOL			skipFlag;		/* Entry skipped? */
BOOL			abortFlag;		/* Operation aborted by user? */
BOOL			closeFlag;		/* Flag that is set when user clicks on close */
BOOL			quitFlag;		/* Set when Quit chosen or close gadget on mode window */
MsgPortPtr	mainMsgPort;	/* The intuition message port */
MsgPort		rexxMsgPort;	/* The ARexx message port */
WindowPtr	backWindow;		/* The background window ptr */
WindowPtr	cmdWindow;		/* The operations window ptr */
RequestPtr	requester;		/* Current requester */
MenuPtr		menuStrip;
MsgPortPtr	appIconMsgPort;
MsgPortPtr	keyPort;			/* Need this for V1.3 alt key testing */
struct IOStdReq *keyRequest;
ULONG 		sigBits;			/* Signal bits for the main window */
WORD			intuiVersion;	/* Hold the intuition version */
ScreenPtr	screen;			/* The screen pointer */
ProcessPtr	process;			/* Tools process */
TextChar		progPathName[100];
TextChar		printerName[FILENAME_SIZE + 3];
WORD			mode, nextMode;/* Which operation selected? */
WORD			action;			/* Indicates if operation started, done, or paused */
WORD			stage;			/* From zero, a count of which stage in progress */
BOOL			inMacro;			/* Executing macro, inited to FALSE */
BOOL			fromCLI;			/* Started from CLI */
WORD			cpi,lpi;			/* For printing */
ScrollListPtr scrollList;	/* Scrollable list */
ULONG			lastTick;		/* For discretionary displays */
Ptr			fib;			/* For temporary FIB storage */
LONG			xoffset;			/* X-offset of horizontal scroll list */
LONG			maxWidth;		/* Maximum width of a text line in scroll list */
LONG			yoffset;			/* Y-offset of vertical scroll list in editor */
ULONG			topListNum;		/* Report list item top */

WORD			processingWidth;
ULONG			*bitMapAltPtr, *bitMapBadPtr, *bitMapDataPtr;
ListHeadPtr deviceList, volumeList;

/*
 *	Parameters determined from printer driver
 */
 
BOOL	pagePrinter/*,graphicPrinter, colorPrinter */;

/*
 *	Pathnames for stuffing a full directory name
 */

ULONG			dialogVarNum;
TextPtr		dialogVarStr;
TextPtr		strPath;
TextPtr		strSavePath;
TextPtr		destFileName;
TextChar		currFileName[MAX_FILENAME_LEN+1];

/*
 *	Preferences structure
 */
OptionsRec	options;

WORD			numColors;

TextChar 	strBuff[PATHSIZE+MAX_FILENAME_LEN];

UWORD    	grayPat[] = { 0xAAAA, 0x5555 };
	
UBYTE			badBlockBSTR[12]	= { 10, 'b','a','d','.','b','l','o','c','k','s',0 };

WORD 			waitCount;
BOOL 			force;
BOOL			titleChanged;
BOOL			altKey;
BOOL			decimalLabel;
WORD			scriptError;
	
/*
 *	Default font variables
 */

UWORD			charWidth;
UWORD 		charHeight;
UWORD 		charBaseline;
UWORD			arrowWidth, arrowHeight, leftEdge;

/*
 *	Gadget list pointer
 */

GadgetPtr	currGadgList;

/*
 * Where to draw the status line(s) of operations
 * The labelRect is the entire rectangle of the operation progress label,
 * which includes the label portion which does not need updating (for
 * example, the word "Blocks:", as opposed to the block number).
 * The statusRect is inside of the labelRect, and indicates the portion
 * which does need continuous updating during an operation.
 * The rectangle "statusRect2" is identical except the MinY has been
 * preset to the second line of the two-line rectangle.
 */
 
Rectangle	labelRect, statusRect, statusRect2;

/*
 * Global bar graph
 */
 
BarGraphPtr barGraph;

/*
 *	Image pointers
 */

ImagePtr iconImage[NUM_ICONS];
ImagePtr altIconImage[NUM_ICONS];
Icon		icon[NUM_ICONS];
Icon		mrIcon[NUM_ICONS];
UWORD		iconColors[4];

BOOL		bitMapFlag, bitMapDirtyFlag;
WORD		checkVolCounter;
UBYTE		*cacheHashBuffer;
UBYTE		*blockBuffer, *extentBuffer, *remapBuffer, *blockCacheBuffer;
ULONG		*badBlockBuffer;
ULONG		extentBufferSize, remapBufSize;
BYTE		cacheErr;
BYTE		ioErr;
BOOL		deviceGotten;
BOOL		blockAccess;
BOOL		waitOnBlock;

UBYTE		*baseDirFib, *curDBBase, *curDBPtr;
ULONG		curDBCount;
ULONG		prevSecs, startSecs;							/* For Reorg timer */

/*
 * Cache buffer lists
 */
 
struct List			busyQueue;
BufCtrlPtr			emptyQueue;

UWORD		tabbing[] = { 192,40,64, 80, 72, 80 };	/* Pixel widths of each field */
UWORD		tabChars[]= {  32, 5,10, 10,  9, 10 };	/* For printing (monospaced chars) */
UWORD		tabTable[]= {   0, 0, 0,  0,  0,  0 };	/* Equals tabbing[x] if to be shown */
	
ImagePtr	iconList[NUM_MINI_ICONS];

/*
 *	Global device structures - As long as the program is in a mode other
 *								than TOOLS, these variables have actual
 *								stuff in them.
 */

TextChar				deviceName[MAX_FILENAME_LEN+1],
						volumeName[MAX_FILENAME_LEN+1];
						choiceName[MAX_FILENAME_LEN+1];
						choiceNameColon[MAX_FILENAME_LEN+1];
struct MsgPort		*devPort, *diskPort;
VolParams			currVolParams;
struct IOExtTD		*diskReq;
RepairStats			repairStats;
BadBlockStats		badBlockStats;
RecoverStats		recoverStats;
ReorgStats			reorgStats;
EditStats			editStats;
