/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Device list stuff
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <dos/dos.h>
#include <dos/dosextens.h>
#include <dos/filehandler.h>
#include <devices/trackdisk.h>
#include <resources/disk.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <clib/alib_protos.h>

#include <string.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/DOS.h>
#include <Toolbox/List.h>
#include <Toolbox/Packet.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ProcessPtr			process;
extern struct DosLibrary	*DOSBase;
extern struct Library		*DiskBase;
extern APTR					SaveVec;
extern TextChar			strBuff[];
extern MsgPortPtr			mainMsgPort;
extern ScreenPtr			screen;
extern WindowPtr			cmdWindow;
extern TextChar			choiceName[], choiceNameColon[], deviceName[], volumeName[];
extern TextChar			strColon[];
extern struct MsgPort		*devPort, *diskPort;
extern OptionsRec			options;
extern struct IOExtTD		*diskReq;
extern ScrollListPtr			scrollList;
extern VolParams			currVolParams;
extern Ptr					fib;
extern WORD					intuiVersion, mode, checkVolCounter;
extern ListHeadPtr			deviceList, volumeList;
extern UBYTE				*blockBuffer;
extern BOOL					deviceGotten, blockAccess, waitOnBlock;
extern TextChar			A2090DevSuffix[];
extern BYTE					ioErr;

/*
 *	Local prototypes
 */

void MyGiveUnit(void);
static BOOL DiskListChanged(WORD);
static BOOL GetVolumeName(TextPtr, APTR);
static BOOL DeriveVolumeName(TextPtr, TextPtr);
static BOOL GetDeviceList(void);
static BOOL GetVolumeList(void);
static LONG	AddAndSortListItem(ListHeadPtr, TextPtr, WORD);
static void LoadDeviceParams(struct DevInfo *, VolParamsPtr);
static void LoadDosEnviron(struct DosEnvec *, VolParamsPtr);
static void CalcVolParams(void);

#define SIZEOFJMPINST	2		/* On 680x0 machines, vector starts after two bytes */
#define MAX_LIST_ITEMS	32768	/* Like we're really gonna have this many volumes! */

/*
 * Patch the GiveUnit of the disk resource
 */
 
BOOL PatchGiveUnit()
{
	APTR *funcPtr;
	
	if( SystemVersion() < OSVERSION_2_0 ) {
		if( DiskBase != NULL ) {
			funcPtr = (APTR *) ((char *) DiskBase + DR_GIVEUNIT + SIZEOFJMPINST);
			SaveVec = *funcPtr;
/*			intena = 0x4000;		Unknown reason */
			*funcPtr = MyGiveUnit;
/*			intena = -0x4000;		Unknown reason */
		}
	}

	return(DiskBase != NULL);	
}

/*
 * Unpatch above routine
 */
 
void UnPatchGiveUnit()
{
	APTR *funcPtr;
	
	if( SystemVersion() < OSVERSION_2_0 ) {
		if( SaveVec != NULL ) {
			funcPtr = (APTR *) ((UBYTE *) DiskBase + DR_GIVEUNIT + SIZEOFJMPINST);
			*funcPtr = SaveVec;
		}
	}
}

/*
 * Get a device, inhibiting it from AmigaDOS if "inhibit" TRUE.
 * Calls GetDevice() after grabbing its name from the scrolllist.
 * Returns if device successfully opened.
 */
 
BOOL DoGetDevice(BOOL inhibit)
{
	register BOOL success = FALSE;
	register WORD item;
	
	choiceName[0] = '\0';
	item = SLNextSelect(scrollList, -1);
	if( item != -1 ) {
		SLGetItem(scrollList, item, choiceName);
		
		if( options.Devices ) {
			strcpy(deviceName, choiceName);
		} else {
			strcpy(deviceName, GetListItem(deviceList, item));
		}
		strcpy(choiceNameColon, choiceName);
		if( !options.Devices ) {
			strcat(choiceNameColon, strColon);
		}
		if( !(success = GetDevice(inhibit)) ) {
			PutDevice();
			Error(ERR_NO_DEV_INFO);
		}
	}
	return(success);
}

	
/*
 *	Called when program is through with a device that was gotten with GetDevice().
 * This routine should clear all variables it deallocates.
 * It should be safe to call this routine multiple times.
 */

void PutDevice()
{
	LONG arg1;
	
	if( deviceGotten ) {
		deviceGotten = FALSE;
		
		FreeCache();
		if( diskReq != NULL ) {
/*
	DoPacket() calls here will not respect the prefs setting, because it
	makes no sense to leave the drive possibly inhibited.
*/
			if( currVolParams.DeviceInhibited ) {
				DoPacket(devPort, ACTION_FLUSH, NULL, 0);
			}
			MotorOff();
			CmdClear();
			if( currVolParams.DeviceOpen ) {
				CloseDevice((struct IORequest *)diskReq);
				currVolParams.DeviceOpen = FALSE;
			}
			DeleteExtIO((struct IORequest *)diskReq);
			diskReq = NULL;
		}
		if( diskPort != NULL ) {
			DeletePort(diskPort);
			diskPort = NULL;
		}
		if( currVolParams.DeviceInhibited && (!currVolParams.PreventUninhibit) ) {
			arg1 = DOSFALSE;
			DoPacket(devPort, ACTION_INHIBIT, &arg1, 1);
			currVolParams.DeviceInhibited = FALSE;
		}
		FreeBlock();
		devPort = NULL;
	}
}

/*
 * Just like the Toolbox AddListItem, except it always sorts, beginning with
 * the second character. Note "list" assumed to be non-NULL, "len" non-zero.
 */
 
static LONG AddAndSortListItem(list, text, len)
ListHeadPtr list;
TextPtr text;
register WORD len;
{
	register LONG num;
	register ListItemPtr item, prevItem;
	ListItemPtr newItem;
	
	text[len] = 0;
	if( (newItem = CreateListItem(text, len)) == NULL)
		return (-1);
	prevItem = NULL;
	num = 0;
	item = list->First;
	while (item && CmpString(text, (item->Text), len, (WORD) strlen(item->Text), FALSE) > 0) {
		prevItem = item;
		item = item->Next;
		num++;
	}
	AddToList(list, prevItem, newItem);
	/*
	len = TextLength(cmdWindow->RPort, text, len);
	if( len > pagePixWidth ) {
		pagePixWidth = len;
	}
	*/
	return(num);
}

/*
 *	Check disk/assign list for changes
 *	Return TRUE if changed
 */

static BOOL DiskListChanged(WORD oldDisks)
{
	register WORD newDisks;
	register struct DevInfo *dList;
	register struct DevInfo *dList2;
	struct DevInfo *saveList, *saveList2;
	
	newDisks = 0;
	for( saveList = dList = NHLockDosList(DLT_DEVICE); dList != NULL ; dList = NHNextDosEntry(dList, DLT_DEVICE) ) {
		if( (dList->dvi_Startup > 255) && (dList->dvi_Task != NULL) ) {
			for( saveList2 = dList2 = NHLockDosList(DLT_VOLUME); (dList2 != NULL); dList2 = NHNextDosEntry(dList2, DLT_VOLUME) ) {
				if( dList2->dvi_Task == dList->dvi_Task ) {
					newDisks++;
					break;
				}
			}
			NHUnlockDosList(saveList2, DLT_VOLUME);
		}
	}
	NHUnlockDosList(saveList, DLT_DEVICE);
	return( newDisks != oldDisks );
}

/*
 * Update volume list
 */
 
BOOL UpdateVolumeOrDeviceList(ScrollListPtr scrollList, BOOL alwaysUpdate)
{
	register WORD numItems;
	BOOL success = FALSE;
	register BOOL diff;
	
	numItems = SLNumItems(scrollList);
	diff = (!options.Devices) && DiskListChanged(numItems);
	if( diff || alwaysUpdate ) {
		success = GetVolumeOrDeviceList(scrollList);
		SetToolsMenu();						/* In case no volumes anymore */
	}
	checkVolCounter = 10;
	return(success);
}

/*
 * Stuff the scrollList passed with volumes or devices, whichever is appropriate.
 */
 
BOOL GetVolumeOrDeviceList(ScrollListPtr scrollList)
{
	register BOOL success;
	register ListHeadPtr list;
	register WORD i;
	register WORD num;
	register TextPtr str;
	register WORD entry = 0;
	
	SLDoDraw(scrollList, FALSE);

	i = SLNextSelect(scrollList, -1);
	if( i != -1 ) {
		strcpy(deviceName, GetListItem(deviceList, i));
	}
	SLRemoveAll(scrollList);
	if( success = StuffVolumeOrDeviceList() ) {
		list = options.Devices ? deviceList : volumeList;
		num = NumListItems(list);
		for( i = 0 ; i < num ; i++ ) {
			str = GetListItem(list, i);
			SLAddItem(scrollList, str, strlen(str), i);
		}
		if( (i = FindListItem(deviceList, deviceName)) != -1 ) {
			entry = i;
		}
		SLSelectItem(scrollList, entry, TRUE);
		SLAutoScroll(scrollList, entry);
	} else {
		SLRemoveAll(scrollList);
	}
	SLDoDraw(scrollList, TRUE);

	return(success);
}

/*
 * Given an existing list, call the appropriate routine to stuff it with
 * the names of volumes or devices.
 */
 
BOOL StuffVolumeOrDeviceList()
{
	DisposeListItems(deviceList);
	DisposeListItems(volumeList);
	
	return( options.Devices ? GetDeviceList() : GetVolumeList() );
}

/*
 *	Construct a list that contains all ADOS device names
 */

static BOOL GetDeviceList()
{
	register struct DevInfo *dList;
	register TextPtr str;
	TextChar buff[MAX_FILENAME_LEN+1];
	TextChar volBuff[MAX_FILENAME_LEN+1];
	register BOOL success = TRUE;
	LONG item;
	struct DevInfo *saveList;
	
	for( saveList = dList = NHLockDosList(DLT_DEVICE); (dList != NULL) && success ; dList = NHNextDosEntry(dList, DLT_DEVICE) ) {
		if( dList->dvi_Startup > 255 ) {
			ConvertBCPLToCString(BADDR(dList->dvi_Name), strBuff);
			str = &buff[0];
			strcat(strcpy(str, strBuff), strColon);
/*
	If insufficient memory to add to a list, then set "success" to FALSE in order
	to discontinue looping for other volumes.
*/
			if( success = (item = AddAndSortListItem(deviceList, str, strlen(str))) != -1) {
				if( GetVolumeName(volBuff, dList->dvi_Task) ) {
					success = InsertListItem(volumeList, volBuff, strlen(volBuff), item);
				}
			}
		}
	}
	NHUnlockDosList(saveList, DLT_DEVICE);
	return(success);
}

/*
 *	Construct a list that contains all Volume names
 */

static BOOL GetVolumeList()
{
	register struct DevInfo *dList;
	register TextPtr str;
	TextChar buff[MAX_FILENAME_LEN+1];
	TextChar volBuff[MAX_FILENAME_LEN+1];
	register BOOL success = TRUE;
	LONG item;
	struct DevInfo *saveList;
	
	for( saveList =dList = NHLockDosList(DLT_DEVICE); (dList != NULL) && success ; dList = NHNextDosEntry(dList, DLT_DEVICE) ) {
		if( dList->dvi_Startup > 255 ) {
			ConvertBCPLToCString( BADDR(dList->dvi_Name), strBuff);
			str = &buff[0];
			strcat(strcpy(str, strBuff), strColon);
			if( GetVolumeName(volBuff, dList->dvi_Task) ) {
/*
	If insufficient memory to add to a list, then set "success" to FALSE in order
	to discontinue looping for other volumes.
*/
				if( (success = (item = AddAndSortListItem(volumeList, volBuff, strlen(volBuff))) != -1) ) {
					success = InsertListItem(deviceList, str, strlen(str), item);
				}
			}
		}
	}	
	NHUnlockDosList(saveList, DLT_DEVICE);
	return(success);
}

/*
 * Return volume name given its task.
 */
 
static BOOL GetVolumeName(TextPtr volName, APTR task)
{
	register struct DevInfo *dList;
	register BOOL found = FALSE;
	struct DevInfo *saveList;
	
	for( saveList = dList = NHLockDosList(DLT_VOLUME); (dList != NULL); dList = NHNextDosEntry(dList, DLT_VOLUME) ) {
		if( (task != NULL) && task == dList->dvi_Task ) {
			found = TRUE;
			ConvertBCPLToCString(BADDR(dList->dvi_Name), volName);
			break;
		}
	}
	NHUnlockDosList(saveList, DLT_VOLUME);
	return(found);
}

/*
 * Derive a volume name from the device name passed. We can't do an Examine()
 * (a la Mark's code) because that returns success even if it doesn't stuff
 * the volume name into the FileInfoBlock (which happens with corrupted root).
 */
 
static BOOL DeriveVolumeName(TextPtr devName, TextPtr volName)
{
	register WORD lenDevName = strlen(devName);
	register struct DevInfo *dList;
	register struct DevInfo *saveList;
	register UBYTE *devAddr;
	APTR thisTask;
	
	if( lenDevName && (devName[lenDevName-1] == ':') ) {
		lenDevName--;
	}
	for( saveList = dList = NHLockDosList(DLT_DEVICE); dList != NULL ; dList = NHNextDosEntry(dList, DLT_DEVICE) ) {
		devAddr = BADDR(dList->dvi_Name);
		if( CmpString(devName, &devAddr[1], lenDevName, *devAddr, TRUE) == 0 ) {
			thisTask = dList->dvi_Task;
		}
	}
	NHUnlockDosList(saveList, DLT_DEVICE);
	return( GetVolumeName(volName, thisTask) );
}
	
/*
 *	Get all information about device passed.
 * Return FALSE if something went wrong, otherwise return TRUE.
 *	Also, inhibit the device from being used.
 * NOTE: The variable "deviceName" must be pre-initialized.
 * The variable "volumeName" will be set to the correct name.
 */

#define ID_QB_OLD_WORD	0x5142
#define ID_QB_WORD		0x5162
#define ID_AMIBACK_DISK	0x4241434B			/* BACK */
#define ID_BUSY_DISK		0x42555359			/* BUSY */

BOOL GetDevice(BOOL inhibit)
{
	register struct DevInfo *dEntry;
	register BOOL success;
	LONG arg1;
	struct DevInfo *saveList;
	struct IOStdReq *ioReq;
	Dir lock;
	struct FileLock *fileLock;
	WindowPtr oldWindow;
	
	diskPort = NULL;
	diskReq = NULL;

	BlockClear(&currVolParams, sizeof(VolParams));
	devPort = DeviceProc(deviceName);
	if( currVolParams.VolValid = DeriveVolumeName(deviceName, volumeName) ) {
		arg1 = MKBADDR((struct InfoData *)fib);
		if( DoToolsPacket(devPort, ACTION_DISK_INFO, &arg1, 1) ) {
			currVolParams.Info = *((struct InfoData *)fib);
			currVolParams.VolValid = currVolParams.Info.id_DiskState != ID_VALIDATING;
		}
	}
	if( success = devPort != NULL ) {
		oldWindow = process->pr_WindowPtr;
		process->pr_WindowPtr = (APTR)-1L;
		if( fileLock = (struct FileLock *) BADDR(lock = Lock(deviceName, ACCESS_READ)) ) {
			currVolParams.ActiveLocks = fileLock->fl_Link != NULL;
			UnLock(lock);
		}
		process->pr_WindowPtr = oldWindow;
		if( inhibit && (currVolParams.Info.id_DiskType != ID_BUSY_DISK) ) {
			if( !options.PrefsOpts.BypassDOS ) {
				(void) DoPacket(devPort, ACTION_FLUSH, NULL, 0);
				arg1 = DOSTRUE;
				if( success = DoPacket(devPort, ACTION_INHIBIT, &arg1, 1) ) {
					currVolParams.DeviceInhibited = TRUE;
				}
			}
		}
		if( success ) {
			success = FALSE;
			dEntry = saveList = NHLockDosList(DLT_DEVICE);
			if( dEntry = NHFindDosEntry(dEntry, deviceName, DLT_DEVICE) ) {
				LoadDeviceParams(dEntry, &currVolParams);
			}
			NHUnlockDosList(saveList, DLT_DEVICE);
			if( dEntry != NULL ) {
				CalcVolParams();
/*
	When allocating your I/O request, get something larger than a trackdisk block,
	because the A2090 and others use bigger request blocks.
*/
				if( (diskPort = CreatePort(NULL, NULL))
					 && (diskReq = (struct IOExtTD *)CreateExtIO(diskPort, sizeof(struct IOExtTD)+48))
					 && AllocateBlock()
					 && AllocateCache(inhibit) ) {
					if( !(success = currVolParams.DeviceOpen = OpenDevice(currVolParams.ExecDevName, currVolParams.ExecUnit, (struct IORequest *)diskReq, currVolParams.ExecFlags) == 0) ) {
						Error(ERR_NO_DEVICE);
					} else if( currVolParams.IsTD ) {
						ioReq = &diskReq->iotd_Req;
						ioReq->io_Command = TD_GETDRIVETYPE;
						ioReq->io_Flags = IOF_QUICK;
						DoIO(ioReq);
						currVolParams.IsTD525 = ioReq->io_Actual == DRIVE5_25;
					}
				} else {
					Error(ERR_NO_MEM);
				}
				GetDiskType();
			}
		}
	}
	return(deviceGotten = success);
}

/*
 * Assuming disk present, grab the disk type.
 */
 
void GetDiskType()
{
	register ULONG diskType;
	
	if( ReadBoot(blockBuffer) == 0 ) {
		diskType = currVolParams.DiskType = *((ULONG *)blockBuffer);
		if( currVolParams.DosDisk = (diskType & 0xFFFFFF00) == ID_DOS_DISK ) {
			diskType &= 0xFF;
			if( diskType > 5 ) {
				/*Error(ERR_FUTURE_FS);*/
				currVolParams.DosDisk = FALSE;
				currVolParams.NonFSDisk = TRUE;
			} else {
				currVolParams.FFSFlag = diskType & 1;
				currVolParams.International = diskType > 1;
				currVolParams.DirCache = (diskType & 4) != 0;
			}
		} else {
			switch(diskType) {
			case ID_AMIBACK_DISK:
			case ID_KICKSTART_DISK:
				currVolParams.NonFSDisk = TRUE;
				break;
			default:
				currVolParams.NonFSDisk = (currVolParams.LowestKey >= currVolParams.HighestKey) || 
					((diskType >> 16) == ID_QB_OLD_WORD) || ((diskType >> 16) == ID_QB_WORD);
			}
		}
	} else {
		currVolParams.DosDisk = FALSE;
		currVolParams.VolValid = FALSE;
	}
}

/*
 * Loads device information based on selected device node.
 */
 
static void LoadDeviceParams(register struct DevInfo *dEntry, register VolParamsPtr volParam)
{
	register UBYTE *ptr;
	register struct FileSysStartupMsg *startupMsg;
	
	startupMsg = BADDR(dEntry->dvi_Startup);
	ptr = BADDR(startupMsg->fssm_Device);
	BlockMove(&ptr[1], volParam->ExecDevName, *ptr);
	strcpy(volParam->DeviceName, deviceName);
	volParam->ExecUnit = startupMsg->fssm_Unit;
	volParam->ExecFlags = startupMsg->fssm_Flags;
	volParam->IsTD525 = FALSE;
	LoadDosEnviron((struct DosEnvec *)BADDR(startupMsg->fssm_Environ), volParam);
	if( volParam->IsTD = (strcmp("trackdisk.device", &ptr[1]) == 0) ) {
		if( LibraryVersion((struct Library *)DOSBase) < OSVERSION_2_0 ) {
			volParam->Envec.de_PreAlloc = 0;
		}
	}
}

#define NON_HASH_SIZE	((512-288)>>2)

/*
 * Loads DosEnviron data based on pointer passed.
 */
 
static void LoadDosEnviron(struct DosEnvec *envecPtr, register VolParamsPtr volParam)
{
	register LONG size;
	
	volParam->Envec.de_DosType = ID_DOS_DISK;
	volParam->Envec.de_MaxTransfer = 0L;
	volParam->Envec.de_BufMemType = MEMF_CHIP | MEMF_PUBLIC;
	size = MIN( envecPtr->de_TableSize, sizeof(struct DosEnvec));
	BlockMove( (UBYTE *)envecPtr, (UBYTE *)&volParam->Envec, size << 2);
	volParam->BlockSizeL = volParam->Envec.de_SizeBlock;
	volParam->BlockSize = volParam->BlockSizeL << 2;
	volParam->BlockSizeShift = (volParam->BlockSizeL >> 7) + 8;	/* Plus 8?? */
	volParam->BlockLongShift = volParam->BlockSizeShift - 2;
	volParam->HashTableSize = volParam->BlockSizeL - NON_HASH_SIZE;
}
	
/*
 * Calculate volume parameters from environment vector inputs
 */

static void CalcVolParams()
{
	register LONG temp;
	register VolParamsPtr volParam = &currVolParams;
	
	if( volParam->BlockSizeL > 1 ) {
		volParam->BlockAccess = blockAccess || (strcmp(&currVolParams.ExecDevName[1], A2090DevSuffix) == 0);
		volParam->BlocksPerCyl = volParam->Envec.de_Surfaces * volParam->Envec.de_BlocksPerTrack;
		volParam->BlockSize = volParam->Envec.de_SizeBlock << 2;
		volParam->VolumeOffset = (volParam->BlocksPerCyl * volParam->Envec.de_LowCyl) << volParam->BlockSizeShift;
		volParam->MaxCylinders = (volParam->Envec.de_HighCyl - volParam->Envec.de_LowCyl) + 1;
		volParam->MaxBlocks = volParam->BlocksPerCyl * volParam->MaxCylinders;
		volParam->MaxBadBlocks = MIN(1000, volParam->MaxBlocks);
		volParam->HighestKey = temp = (volParam->MaxBlocks - 1) /*- volParam->Envec.de_PreAlloc*/;
		volParam->LowestKey = volParam->Envec.de_Reserved;
		volParam->MaxKeys = (temp - volParam->LowestKey) + 1;
		volParam->RootBlock = (temp + volParam->LowestKey) >> 1;
		volParam->MaxBytes = volParam->MaxBlocks << volParam->BlockSizeShift;
		volParam->BitMapBlkBits = (volParam->BlockSizeL - 1) << 5;
		volParam->BitMapBlocks = (((volParam->BitMapBlkBits - 1) + temp) - volParam->LowestKey) / volParam->BitMapBlkBits;
		temp = volParam->BitMapBlocks - 25;			/* This many in root */
		volParam->BitMapExtBlks = (temp <= 0) ? 0 : 1 + ((temp - 1) / (volParam->BlockSizeL - 1));
		if( volParam->VolValid && volParam->Info.id_NumBlocks ) {
			if( (temp = volParam->MaxBlocks - volParam->Envec.de_Reserved) != volParam->Info.id_NumBlocks ) {
				temp = volParam->MaxBlocks;
			}
			volParam->FreeBlocks = temp - volParam->Info.id_NumBlocksUsed;
			volParam->FreeBytes = volParam->FreeBlocks * volParam->Info.id_BytesPerBlock;
			volParam->PercentFull = (volParam->Info.id_NumBlocksUsed * 100) / volParam->Info.id_NumBlocks;
		}
	}
}

/*
 * Read multiple blocks from disk.
 */
 
BYTE ReadMulti(ULONG blockNum, ULONG numBlocks, Ptr buffer, BOOL async)
{
	return( DoReadBlock(blockNum, buffer, numBlocks << currVolParams.BlockSizeShift, async) );
}

/*
 *	Read a block from the current drive.
 *	Caller must wait on diskPort if async is TRUE.
 *	If block out of range of current drive, then return -1.
 */

BYTE ReadBlock(ULONG blockNum, Ptr buffer, BOOL async)
{
	BYTE result;
	
	if( blockNum > currVolParams.HighestKey ) {
		Error(ERR_OUT_OF_RANGE);
		result = -1;
	} else {
		result = DoReadBlock(blockNum, buffer, currVolParams.BlockSize, async);
	}
	return(result);
}

/*
 * Do an unconditional read of the block passed.
 */

BYTE DoReadBlock(ULONG blockNum, Ptr buffer, ULONG len, BOOL async)
{
	register BYTE result;
	register struct IOStdReq *ioReq = &diskReq->iotd_Req;
	
	ioReq->io_Command = CMD_READ;
	ioReq->io_Data = buffer;
	ioReq->io_Length = len;
	ioReq->io_Offset = (blockNum << currVolParams.BlockSizeShift) + currVolParams.VolumeOffset;
	ioReq->io_Actual = 0;
	if( async ) {
		waitOnBlock = TRUE;
		SendIO((IOReqPtr)ioReq);
		result = 0;
	} else {
		if( (result = DoIO((IOReqPtr)ioReq)) == 0 ) {
			if( len != ioReq->io_Actual ) {
				result = -1;
			}
		}
		if( result != 0 ) {
			ioReq->io_Actual = 0;
			if( (result = DoIO((IOReqPtr)ioReq)) == 0 ) {
				if( len != ioReq->io_Actual ) {
					result = -1;
				}
			}
		}
	}
	return(result);
}

/*
 * Read the boot block (#0) into the buffer passed, synchronously.
 */
 
BYTE ReadBoot(Ptr buffer)
{
	return( DoReadBlock(0, buffer, currVolParams.BlockSize, FALSE) );
}

/*
 * Write multiple blocks to disk (during reorganization).
 */
 
BYTE WriteMulti(ULONG blockNum, ULONG numBlocks, Ptr buffer, BOOL async)
{
	return( DoWriteBlock(blockNum, buffer, numBlocks << currVolParams.BlockSizeShift, async) );
}
	
/*
 * Write a block to the current drive.
 * Caller must wait on diskPort if async is TRUE.
 * If block out of range of current drive, then return -1.
 */
 
BYTE WriteBlock(ULONG blockNum, Ptr buffer, BOOL async)
{
	BYTE result;
	
	if( blockNum > currVolParams.HighestKey ) {
		Error(ERR_OUT_OF_RANGE);
		result = -1;
	} else {
		result = DoWriteBlock(blockNum, buffer, currVolParams.BlockSize, async);
	}
	return(result);
}

/*
 * Do an unconditional write of the block
 */
 		
BYTE DoWriteBlock(ULONG blockNum, Ptr buffer, ULONG len, BOOL async)
{
	register BYTE result;
	register struct IOStdReq *ioReq = &diskReq->iotd_Req;
	
	ioReq->io_Command = CMD_WRITE;
	ioReq->io_Data = buffer;
	ioReq->io_Length = len;
	ioReq->io_Offset = (blockNum << currVolParams.BlockSizeShift) + currVolParams.VolumeOffset;
	ioReq->io_Actual = 0;
	if( async ) {
		waitOnBlock = TRUE;
		SendIO((IOReqPtr)ioReq);
		result = 0L;
	} else {
		if( (result = DoIO((IOReqPtr)ioReq)) == 0 ) {
			if( len != ioReq->io_Actual ) {
				result = -1;
			}
		}
		if( result != 0 ) {
			ioReq->io_Actual = 0;
			if( (result = DoIO((IOReqPtr)ioReq)) == 0 ) {
				if( len != ioReq->io_Actual ) {
					result = -1;
				}
			}
		}
	}
	return(result);
}

/*
 * Write the buffer passed into the boot block (#0), synchronously.
 */
 
BYTE WriteBoot(Ptr buffer)
{
	return( DoWriteBlock(0, buffer, currVolParams.BlockSize, FALSE) );
}

/*
 * Finish reading sent IO. Retrieve result of I/O.
 */
 
BYTE WaitBlock()
{
	register BYTE err;
	
	if( waitOnBlock ) {
		err = WaitIO((struct IORequest *)diskReq);
		if( diskReq->iotd_Req.io_Length != diskReq->iotd_Req.io_Actual ) {
			err = -1;
		}
		ioErr = err;
		waitOnBlock = FALSE;
	} else {
		err = ioErr;
	}
	return(err);
}
	
/*
 *	Turn motor off, but make sure unwritten buffers are updated first.
 */

void MotorOff()
{
	if( (diskReq != NULL) && IsDiskPresent() ) {		/* Does WaitIO() for us */
		diskReq->iotd_Req.io_Command = CMD_UPDATE;
		diskReq->iotd_Req.io_Length = 0;
		DoIO((struct IORequest *)diskReq);
		if( currVolParams.IsTD ) {
			diskReq->iotd_Req.io_Command = TD_MOTOR;
			DoIO((struct IORequest *)diskReq);
		}
	}
}

/*
 *	Check write-protect status, return TRUE if current drive is write protected.
 */

BOOL IsWriteProtected()
{
	BOOL success = FALSE;
	
	if( diskReq != NULL ) {
		(void) WaitBlock();
		diskReq->iotd_Req.io_Command = TD_PROTSTATUS;
		DoIO((struct IORequest *)diskReq);
		success = diskReq->iotd_Req.io_Actual != 0;
	}
	return(success);
}

/*
 * Force re-reading of boot track.
 */
 
void CmdClear()
{
	if( IsDiskPresent() ) {			/* Does a WaitIO() for us */
		/*
		diskReq->iotd_Req.io_Command = CMD_UPDATE;
		diskReq->iotd_Req.io_Length = 0;
		DoIO((struct IORequest *)diskReq);
		*/
		diskReq->iotd_Req.io_Command = CMD_CLEAR;
		DoIO((struct IORequest *)diskReq);
	}
}
	
/*
 *	Check for presence of disk in drive, return TRUE if disk present.
 */

BOOL IsDiskPresent()
{
	BOOL success = FALSE;
	
	if( diskReq != NULL ) {
		(void) WaitBlock();
		diskReq->iotd_Req.io_Command = TD_CHANGESTATE;
		DoIO((struct IORequest *)diskReq);
		success = diskReq->iotd_Req.io_Actual == 0;
	}
	return(success);
}

/*
 * Return whether disk has changed (always TRUE if 5.25" drive).
 */
 
BOOL IsDiskChanged()
{
	register struct IOStdReq *ioReq;
	BOOL diff;
	
	if( diff = (!currVolParams.IsTD525) && (diskReq != NULL) ) {
		(void) WaitBlock();
		ioReq = &diskReq->iotd_Req;
		ioReq->io_Command = TD_CHANGENUM;
		ioReq->io_Flags = IOF_QUICK;
		DoIO(ioReq);
		diff = currVolParams.PrevCount != ioReq->io_Actual;
		currVolParams.PrevCount = ioReq->io_Actual;
	}
	return(diff);
}
