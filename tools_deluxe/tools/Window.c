/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Window operations
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <graphics/gfxmacros.h>
#include <graphics/regions.h>
#include <intuition/intuition.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Image.h>
#include <Toolbox/Request.h>
#include <Toolbox/Border.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	External variables
 */

extern BOOL				_tbOnPubScreen, _tbSmartWindows;
extern UBYTE			_tbPenLight, _tbPenWhite, _tbPenDark, _tbPenBlack;
extern WORD				_tbXSize, _tbYSize;

extern struct NewWindow	newBackWindow, newMainWindow, newToolWindow;
extern ScreenPtr		screen;
extern WindowPtr		backWindow;
extern WindowPtr		cmdWindow;
extern MsgPortPtr		mainMsgPort, appIconMsgPort;
extern MsgPort			rexxMsgPort;
extern MenuPtr			menuStrip;
extern BOOL				closeFlag, quitFlag, decimalLabel;
extern WORD				intuiVersion, numColors, mode, action;

extern UWORD			grayPat[];
extern ScrollListPtr		scrollList;
extern TextChar		strBuff[], screenTitle[], strProgName[];
extern TextChar		strWindTitleSep[], choiceName[], deviceName[];
extern TextPtr			iconTitle[];

extern struct RexxLib	*RexxSysBase;
extern ULONG			sigBits;

extern GadgetTemplate 	modeGadgets[], repairGadgets[], recoverGadgets[], reorgGadgets[], editGadgets[];
extern GadgetPtr		currGadgList;
extern UWORD			charHeight, charBaseline, modeGadgetHeight;
extern OptionsRec		options;
extern VolParams		currVolParams;
extern ReorgStats		reorgStats;

/*
 *	Local variables and definitions
 */

typedef struct  {
	LayerPtr	Layer;
	Rectangle	Bounds;
	LONG		OffsetX, OffsetY;
} BackFillMsg, *BFMsgPtr;

static struct Hook	backFillHook, docBackFillHook;

#define	MAIN_HORIZ_MARGIN	60
#define	MAIN_VERT_MARGIN	40

/*
 *	Local prototypes
 */

static void __saveds __asm BackFill(register __a0 struct Hook *,
									register __a2 RastPtr,
									register __a1 BFMsgPtr);
static void __saveds __asm DocBackFill(register __a0 struct Hook *,
										register __a2 RastPtr,
										register __a1 BFMsgPtr);

static void			ClearBackWindow(RastPtr, Rectangle *);
static void			SetBorderGadgets(GadgetPtr);
static void			PreInitWindow(struct NewWindow *, WORD);
static WindowPtr	DoOpenWindow(struct NewWindow *);
static void			InitWindow(void);
static ULONG		GetPortSigBit(MsgPortPtr);

/*
 *	Back fill routine for backWindow
 */

static void __saveds __asm BackFill(register __a0 struct Hook *hook,
									register __a2 RastPtr rPort,
									register __a1 BFMsgPtr bfMsg)
{
	RastPort newRPort;

	newRPort = *rPort;			/* Make a copy so we can modify it */
	newRPort.Layer = NULL;
	ClearBackWindow(&newRPort, &bfMsg->Bounds);
}

/*
 *	Back fill routine for backWindow
 */

static void __saveds __asm DocBackFill(register __a0 struct Hook *hook,
										register __a2 RastPtr rPort,
										register __a1 BFMsgPtr bfMsg)
{
	RastPort newRPort;

	newRPort = *rPort;			/* Make a copy so we can modify it */
	newRPort.Layer = NULL;
	SetAPen(&newRPort, _tbPenLight);
	SetDrMd(&newRPort, JAM1);
	RectFill(&newRPort, bfMsg->Bounds.MinX, bfMsg->Bounds.MinY,
			 bfMsg->Bounds.MaxX, bfMsg->Bounds.MaxY);
}

/*
 *	Clear backWindow
 */

static void ClearBackWindow(RastPtr rPort, Rectangle *rect)
{
	if( numColors >= 4 ) {
		SetAPen(rPort, _tbPenLight);
		SetDrMd(rPort, JAM1);
	} else {
		SetAPen(rPort, _tbPenBlack);
		SetBPen(rPort, _tbPenWhite);
		SetDrMd(rPort, JAM2);
		SetAfPt(rPort, grayPat, 1);
	}
	RectFill(rPort, rect->MinX, rect->MinY, rect->MaxX, rect->MaxY);
}

/*
 *	Open backWindow
 */

WindowPtr OpenBackWindow()
{
	Rectangle rect;
	ULONG oldIDCMPFlags;
	
	newBackWindow.Screen = screen;
	newBackWindow.Type = CUSTOMSCREEN;
	if (_tbOnPubScreen) {
		newBackWindow.Width = newBackWindow.Height = 1;
		newBackWindow.Flags |= NOCAREREFRESH;
	}
	else {
		newBackWindow.Width = screen->Width;
		newBackWindow.Height = screen->Height;
	}
	oldIDCMPFlags = newBackWindow.IDCMPFlags;
	newBackWindow.IDCMPFlags = 0;
	if (intuiVersion < OSVERSION_2_0)
		backWindow = OpenWindow(&newBackWindow);
	else {
		if (_tbOnPubScreen)
			newBackWindow.Type = PUBLICSCREEN;
		newBackWindow.Flags |= NOCAREREFRESH;
		backFillHook.h_Entry = (ULONG (*)()) BackFill;
		backWindow = OpenWindowTags(&newBackWindow,
									WA_BackFill, &backFillHook,
									WA_NewLookMenus, TRUE,
									TAG_END);
	}
	if (backWindow != NULL) {
		backWindow->UserPort = mainMsgPort;		/* Was monitorMsgPort in ProWrite */
		ModifyIDCMP(backWindow, oldIDCMPFlags);
		SetStdPointer(backWindow, POINTER_ARROW);
		if(_tbOnPubScreen) {
			ScreenToFront(screen);	  /* In case it is not in front */
			WindowToBack(backWindow);
			SetWindowTitles(backWindow, (TextPtr) -1, screenTitle);
		}
		else if( intuiVersion < OSVERSION_2_0 ) {
			SetRect(&rect, 0, 0, (WORD) (backWindow->Width - 1), (WORD) (backWindow->Height - 1));
			ClearBackWindow(backWindow->RPort, &rect);
		}
	}
	return (backWindow);
}

/*
 *	Install clip region for window
 */

void SetWindowClip(WindowPtr window)
{
	Rectangle rect;

	GetWindowRect(window, &rect);
	SetRectClip(window->WLayer, &rect);
}

/*
 * Make sure Intuition realizes these are border gadgets.
 */
 
static void SetBorderGadgets(register GadgetPtr gadgList)
{
	GadgetItem(gadgList, UP_ARROW)->Activation		|= GACT_RIGHTBORDER;
	GadgetItem(gadgList, DOWN_ARROW)->Activation		|= GACT_RIGHTBORDER;
	GadgetItem(gadgList, VERT_SCROLL)->Activation	|= GACT_RIGHTBORDER;
	GadgetItem(gadgList, LEFT_ARROW)->Activation		|= GACT_BOTTOMBORDER;
	GadgetItem(gadgList, RIGHT_ARROW)->Activation	|= GACT_BOTTOMBORDER;
	GadgetItem(gadgList, HORIZ_SCROLL)->Activation	|= GACT_BOTTOMBORDER;
}

/*
 *	Open the main operation window
 *	Return pointer to window if successful, NULL if not
 */

WindowPtr OpenModeWindow()
{
	Rectangle rect, rect2, rect3;
	register WindowPtr window = NULL;
	register WORD topOffset, temp1, temp2;
	WORD minWidth, minHeight;
	struct NewWindow newWindow = newMainWindow;
	
	minWidth = newWindow.MinWidth;
	minHeight = topOffset = screen->BarHeight + 1;
	if( screen->ViewPort.Modes & LACE ) {
		topOffset++;				/* Two pixel wide line reduces flicker */
	}
			
	rect2 = options.WindowRect;
	GetScreenViewRect(screen, &rect);
	UnionRect(&rect2, &rect, &rect3);
	if( EmptyRect(&rect2) || !EqualRect(&rect, &rect3) ||
		(HEIGHT(&rect2) < screen->BarHeight) || (WIDTH(&rect2) < minWidth) ) {
		newWindow.Width = MIN( ((newWindow.Width * _tbXSize) >> 3), (rect.MaxX - rect.MinX) + 1);
		newWindow.Height = MIN( (((newWindow.Height + screen->BarHeight) * _tbYSize) / 11), ((rect.MaxY - rect.MinY) + 1) - topOffset);
		temp1 = HEIGHT(&rect);
		temp2 = temp1 - newWindow.Height;
		newWindow.TopEdge = (temp2 >= topOffset) ? topOffset + ((temp2 - topOffset) >> 1) : (MAX(temp1, newWindow.Height) - newWindow.Height) >> 1;
		newWindow.LeftEdge = (WIDTH(&rect) - newWindow.Width) >> 1;
	} else {
		newWindow.LeftEdge = rect2.MinX;
		newWindow.TopEdge = rect2.MinY;
		newWindow.Width = WIDTH(&rect2);
		newWindow.Height = HEIGHT(&rect2);
	}
	newWindow.MinHeight = minHeight;
	newWindow.MinWidth = minWidth;
	
	temp1 = modeGadgets[VOL_LIST].HeightOffset - 1;
	temp2 = modeGadgets[VOL_LIST].TopEdge;
	while( temp1-- && (temp1 + (((temp2 + modeGadgets[VOL_LIST].Height) * _tbYSize) / 11) > newWindow.Height) ) {
		modeGadgets[VOL_LIST].Height -= 11;
		modeGadgets[VOL_LIST].HeightOffset--;
	}
	if( (currGadgList = GetGadgets(modeGadgets)) != NULL ) {
		if( (scrollList = NewScrollList(SL_SINGLESELECT)) != NULL) {
			SetIconImages(currGadgList);
			
			newWindow.Flags = 0;
			PreInitWindow(&newWindow, -1);	
			window = DoOpenWindow(&newWindow);
		}
		if( (cmdWindow = window) != NULL) {
			InitWindow();
			InitScrollList(scrollList, GadgetItem(currGadgList, VOL_LIST), window, NULL);
			
			RefreshToolsWindow(TRUE);
			GadgetOn(window, VOL_DEV_RAD1 + options.Devices);
			
			ModifyIDCMP(window,	GADGETDOWN | GADGETUP | CLOSEWINDOW |
									MENUPICK | NEWSIZE | REFRESHWINDOW |
									ACTIVEWINDOW | INACTIVEWINDOW | MOUSEBUTTONS | RAWKEY |
									INTUITICKS | DISKINSERTED | DISKREMOVED );
			mode = MODE_TOOLS;
			action = OPER_READY;
			SetAllMenus();
			SetArrowPointer();
		}
	}
	if( window == NULL ) {
		RemoveWindow(NULL);
	}
	return(window);
}

/*
 *	Open the Repair window
 *	Return pointer to window if successful, NULL if not
 */

WindowPtr OpenRepairWindow()
{
	Rectangle rect, rect2, rect3;
	register WindowPtr window;
	register WORD topOffset, minHeight, minWidth;
	struct NewWindow newWindow = newToolWindow;
	WORD maxWidth, maxHeight;
	
/*
	Open window and set message port to correct port
	All windows default SMARTREFRESH windows
*/
	if ((currGadgList = GetGadgets(repairGadgets)) != NULL) {
		SetBorderGadgets(currGadgList);
		if( (scrollList = NewScrollList(SL_NOSELECT)) != NULL) {
			
			newWindow.Flags = WFLG_SIZEGADGET | WFLG_SIZEBRIGHT | WFLG_SIZEBBOTTOM;
	
			GetScreenViewRect(screen, &rect);
	
			topOffset = screen->BarHeight + 1;
			if( screen->ViewPort.Modes & LACE ) {
				topOffset++;				/* Two pixel wide line reduces flicker */
			}
			maxWidth = WIDTH(&rect);
			maxHeight = HEIGHT(&rect) - topOffset;
			minWidth = MIN( ((280 * _tbXSize) >> 3), maxWidth );
			minHeight = MIN( (((121 * _tbYSize) / 11) + (3*(charHeight+1)) + ((2*screen->BarHeight)+10)), maxHeight );
			
			rect2 = options.RepairOpts.WindowRect;
			UnionRect(&rect2, &rect, &rect3);
			if( EmptyRect(&rect2) || !EqualRect(&rect, &rect3) ||
				(HEIGHT(&rect2) < screen->BarHeight) || (WIDTH(&rect2) < minWidth) ) {
				if( _tbOnPubScreen ) {
					newWindow.Width = MIN(minWidth + (minWidth >> 1) + 12, maxWidth);
					newWindow.Height = MIN(minHeight + ((charHeight+1) * 7), maxHeight);
					newWindow.LeftEdge = ((maxWidth - newWindow.Width) >> 1);
					newWindow.TopEdge = topOffset + ((maxHeight-newWindow.Height) >> 1);
				} else {
					newWindow.LeftEdge = rect.MinX;
					newWindow.TopEdge = rect.MinY + topOffset;
					newWindow.Width = maxWidth;
					newWindow.Height = maxHeight;
				}
			} else {
				newWindow.LeftEdge = rect2.MinX;
				newWindow.TopEdge = rect2.MinY;
				newWindow.Width = WIDTH(&rect2);
				newWindow.Height = HEIGHT(&rect2);
			}
			newWindow.MinHeight = minHeight;
			newWindow.MinWidth = minWidth;

			PreInitWindow(&newWindow, TOOL_ICON_REPAIR);
			window = DoOpenWindow(&newWindow);
			
			if( (cmdWindow = window) != NULL) {
				InitWindow();
				ModifyIDCMP(window,	GADGETDOWN | GADGETUP | CLOSEWINDOW | MENUPICK |
										NEWSIZE | REFRESHWINDOW | RAWKEY |
										ACTIVEWINDOW | MOUSEBUTTONS | INACTIVEWINDOW |
										INTUITICKS | DISKINSERTED | DISKREMOVED );
				if( !SetupRepairMode() ) {
					CleanupRepairMode();
					return(NULL);
				}
				SetArrowPointer();
			}
		}
	}
	if( window == NULL ) {
		RemoveWindow(NULL);
	}
	return(window);
}

/*
 *	Open the file recovery window
 *	Return pointer to window if successful, NULL if not
 */

WindowPtr OpenRecoverWindow()
{
	Rectangle rect, rect2, rect3;
	register WindowPtr window;
	register WORD topOffset, minHeight, minWidth;
	struct NewWindow newWindow = newToolWindow;
	WORD maxWidth, maxHeight;
	
/*
	Open window and set message port to correct port
	All windows default SMARTREFRESH windows
*/
	recoverGadgets[UNDEL_DEST_POPUP].Value = options.RecoverOpts.Destination != 0;
	if ((currGadgList = GetGadgets(recoverGadgets)) != NULL) {
		SetBorderGadgets(currGadgList);
		if( (scrollList = NewScrollList(SL_MULTISELECT)) != NULL) {
			
			newWindow.Flags = WFLG_SIZEGADGET | WFLG_SIZEBRIGHT | WFLG_SIZEBBOTTOM;
	
			GetScreenViewRect(screen, &rect);
	
			topOffset = screen->BarHeight + 1;
			if( screen->ViewPort.Modes & LACE ) {
				topOffset++;				/* Two pixel wide line reduces flicker */
			}
			maxWidth = WIDTH(&rect);
			maxHeight = HEIGHT(&rect) - topOffset;
			minWidth = MIN( (((460+32) * _tbXSize) >> 3), maxWidth );
			minHeight = MIN( ((193 * _tbYSize) / 11), maxHeight );
			
			rect2 = options.RecoverOpts.WindowRect;
			UnionRect(&rect2, &rect, &rect3);
			if( EmptyRect(&rect2) || !EqualRect(&rect, &rect3) ||
				(HEIGHT(&rect2) < screen->BarHeight) || (WIDTH(&rect2) < minWidth) ) {
				if( _tbOnPubScreen ) {
					newWindow.Width = MIN(minWidth, maxWidth);
					newWindow.Height = MIN(minHeight + (minHeight >> 1), maxHeight);
					newWindow.LeftEdge = ((maxWidth - newWindow.Width) >> 1);
					newWindow.TopEdge = topOffset + ((maxHeight-newWindow.Height) >> 1);
				} else {
					newWindow.LeftEdge = rect.MinX;
					newWindow.TopEdge = rect.MinY + topOffset;
					newWindow.Width = maxWidth;
					newWindow.Height = maxHeight;
				}
			} else {
				newWindow.LeftEdge = rect2.MinX;
				newWindow.TopEdge = rect2.MinY;
				newWindow.Width = WIDTH(&rect2);
				newWindow.Height = HEIGHT(&rect2);
			}
			newWindow.MinHeight = minHeight;
			newWindow.MinWidth = minWidth;

			PreInitWindow(&newWindow, TOOL_ICON_RECOVER);
			window = DoOpenWindow(&newWindow);

			if( (cmdWindow = window) != NULL) {
				InitWindow();
				ModifyIDCMP(window,	GADGETDOWN | GADGETUP | CLOSEWINDOW |
										MENUPICK | NEWSIZE | REFRESHWINDOW | RAWKEY |
										ACTIVEWINDOW | MOUSEBUTTONS | INACTIVEWINDOW |
										INTUITICKS | DISKINSERTED | DISKREMOVED );
				if( !SetupRecoverMode() ) {
					CleanupRecoverMode();
					return(NULL);
				}
				SetArrowPointer();
			}
		}
	}
	if( window == NULL ) {
		RemoveWindow(NULL);
	}
	return(window);
}
/*
 *	Open the optimize/reorganize window.
 *	Return pointer to window if successful, NULL if not
 */

WindowPtr OpenReorgWindow()
{
	Rectangle rect, rect2, rect3;
	register WindowPtr window;
	WORD topOffset;
	register WORD minHeight, minWidth;
	register WORD maxHeight;
	WORD maxWidth;
	register WORD temp;
	struct NewWindow newWindow = newToolWindow;

	topOffset = screen->BarHeight + 1;
	if( screen->ViewPort.Modes & LACE ) {
		topOffset++;				/* Two pixel wide line reduces flicker */
	}
	GetScreenViewRect(screen, &rect);
	BlockClear(&reorgStats, sizeof(ReorgStats));
	GetBitMapTablePtr();
	
	maxWidth = WIDTH(&rect);
	maxHeight = HEIGHT(&rect) - topOffset;
	minWidth = MIN( REORG_BORDER_WIDTH+2+30, maxWidth );
	
/*
	Cleverly pre-flight to determine what the volume fragmentation rectangle will
	be, and make the window the appropriate size for it.
*/
	temp = 2 + ((1 + ( (currVolParams.MaxKeys / (1 << reorgStats.BitMapTablePtr->Shift)) / (REORG_BORDER_WIDTH / reorgStats.BitMapTablePtr->XSize))) * reorgStats.BitMapTablePtr->YSize);
	minHeight = MAX(38, temp) + ((80 * _tbYSize) / 11);
	minHeight = MAX(minHeight, (((MAX_ERR_HEIGHT+14) * _tbYSize) / 11));
	minHeight = MIN(minHeight, maxHeight);
	reorgGadgets[BORDER_FRAGMENTATION].HeightOffset = temp;
	
	temp = reorgGadgets[ETA_BORDER].LeftEdge + reorgGadgets[ETA_BORDER].Width + 12;
	temp = minWidth - (34 + ((temp * _tbXSize) >> 3));
	reorgGadgets[OPTIMIZE_BAR_GRAPH].WidthOffset = temp;
	reorgGadgets[OPTIMIZE_BAR_GRAPH].LeftOffset = 528 - temp;
	if( currVolParams.NonFSDisk ) {
		Error(ERR_NO_FS);
	} else if( PromptForDisk() ) {
		temp = ReadBitMap();
		MotorOff();
		if( !temp ) {
			ExplainDialog(BIG_ERR_REORG_BAD_BITMAP);
			FreeBitMapBuffers();
		} else {
			rect2 = options.ReorgOpts.WindowRect;
			UnionRect(&rect2, &rect, &rect3);
			if( EmptyRect(&rect2) || !EqualRect(&rect, &rect3) ||
				(HEIGHT(&rect2) < minHeight) || (WIDTH(&rect2) < minWidth) ) {
				newWindow.Width = MIN(minWidth, maxWidth);
				newWindow.Height = minHeight;
				newWindow.LeftEdge = ((maxWidth-newWindow.Width) >> 1);
				newWindow.TopEdge = topOffset + ((maxHeight-newWindow.Height) >> 1);
			} else {
				newWindow.LeftEdge = rect2.MinX;
				newWindow.TopEdge = rect2.MinY;
				newWindow.Width = WIDTH(&rect2);
				newWindow.Height = HEIGHT(&rect2);
			}	
			newWindow.MinWidth = minWidth;
			newWindow.MinHeight = minHeight;	
/*
	Open window and set message port to correct port
	All windows default SMARTREFRESH windows
*/
			if ((currGadgList = GetGadgets(reorgGadgets)) != NULL) {

				newWindow.Flags = 0;
				PreInitWindow(&newWindow, TOOL_ICON_OPTIMIZE);
				window = DoOpenWindow(&newWindow);
	
				if( (cmdWindow = window) != NULL) {
					InitWindow();
					ModifyIDCMP(window,	GADGETDOWN | GADGETUP | CLOSEWINDOW |
											MENUPICK | REFRESHWINDOW | RAWKEY |
											ACTIVEWINDOW | INACTIVEWINDOW | MOUSEBUTTONS |
											INTUITICKS | NEWSIZE | DISKINSERTED | DISKREMOVED );
					if( !SetupReorgMode() ) {
						CleanupReorgMode();
						window = NULL;
					}
				}
			} else {
				FreeBitMapBuffers();
			}
		}
	}
	return(window);
}


/*
 *	Open the disk editor window
 *	Return pointer to window if successful, NULL if not
 */

WindowPtr OpenEditWindow()
{
	Rectangle rect, rect2, rect3;
	register WindowPtr window;
	register WORD topOffset, minHeight, minWidth;
	register WORD maxHeight;
	WORD maxWidth, maxScrHeight, fullHeight;
	struct NewWindow newWindow = newToolWindow;
	
/*
	Open window and set message port to correc-t port
	All windows default SMARTREFRESH windows
*/
	editGadgets[EDIT_POPUP_BASE].Value = decimalLabel = options.PrefsOpts.DecimalLabel != 0;
	if( charHeight > 12 ) {		/* Arbitrary value */
		editGadgets[EDIT_LEFT_ARROW].Height = 
		editGadgets[EDIT_RIGHT_ARROW].Height =
		editGadgets[EDIT_HORIZ_SCROLL].Height = IMAGE_ARROW_HEIGHT - 3;
		
		editGadgets[EDIT_LEFT_ARROW].HeightOffset =
		editGadgets[EDIT_RIGHT_ARROW].HeightOffset =
		editGadgets[EDIT_HORIZ_SCROLL].HeightOffset = 0;
	}
	if ((currGadgList = GetGadgets(editGadgets)) != NULL) {

		topOffset = screen->BarHeight + 1;
		if( screen->ViewPort.Modes & LACE ) {
			topOffset++;				/* Two pixel wide line reduces flicker */
		}
		GetScreenViewRect(screen, &rect);
		
		maxWidth = WIDTH(&rect);
		maxScrHeight = HEIGHT(&rect) - topOffset;
/*
	Use the register "minHeight" to perform a temporary calculation of how
	many pixels minimum would it take to hold a full disk block in "topaz/8".
	Note: The volume parameters should obviously have been initialized by GetDevice().
*/
		fullHeight = (2*currVolParams.BlockSizeL)+10+(((72+22) * _tbYSize) / 11);
		maxHeight = MIN(fullHeight, maxScrHeight);	/* Confine to screen, please. */
		
		minWidth = MIN( ((494 * _tbXSize) >> 3), maxWidth );
		minHeight = MIN( (16*8)+9+(((73+22) * _tbYSize) / 11), maxHeight );
		
		rect2 = options.EditOpts.WindowRect;
		UnionRect(&rect2, &rect, &rect3);
		if( EmptyRect(&rect2) || !EqualRect(&rect, &rect3) ||
			(HEIGHT(&rect2) < minHeight) || (WIDTH(&rect2) < minWidth) ) {
			newWindow.Width = minWidth;
			if( fullHeight > maxScrHeight ) {
				newWindow.Height = minHeight;
			} else {
				newWindow.Height = fullHeight;
			}
			newWindow.LeftEdge = ((maxWidth-newWindow.Width) >> 1);
			newWindow.TopEdge = topOffset + ((maxScrHeight-newWindow.Height) >> 1);
		} else {
			newWindow.LeftEdge = rect2.MinX;
			newWindow.TopEdge = rect2.MinY;
			newWindow.Width = WIDTH(&rect2);
			newWindow.Height = HEIGHT(&rect2);
		}	
		newWindow.MinWidth = minWidth;
		newWindow.MinHeight = minHeight;
		newWindow.Flags = 0;
		PreInitWindow(&newWindow, TOOL_ICON_EDIT);
		
		window = DoOpenWindow(&newWindow);

		if( (cmdWindow = window) != NULL) {
			InitWindow();
			ModifyIDCMP(window, GADGETDOWN | GADGETUP | CLOSEWINDOW |
									MENUPICK | REFRESHWINDOW | RAWKEY |
									ACTIVEWINDOW | INACTIVEWINDOW | MOUSEBUTTONS |
									INTUITICKS | NEWSIZE | DISKINSERTED | DISKREMOVED );
			if( !SetupEditMode() ) {
				CleanupEditMode();
				window = NULL;
			}
		}
	}
	return(window);
}

/*
 * Pre-Allocate a title for the window. This makes it possible to have the window
 * come up without a blank title at first (ugly, especially under V1.3).
 * Other initializations done as well.
 */
 
static void PreInitWindow(struct NewWindow *nw, WORD newMode)
{
	register UWORD len;
	register UBYTE *ptr = &strBuff[0];
	
	nw->Screen = screen;
	nw->Type = CUSTOMSCREEN;
	nw->IDCMPFlags = 0;
/*
	Don't add gadgets until window is already up- this is because we clear the
	entire window anyway on V1.3, and drawing it twice is ugly.
*/
	nw->FirstGadget = NULL;
	nw->Flags |= WINDOWDEPTH | WINDOWCLOSE | WINDOWDRAG | ACTIVATE |
		 (_tbSmartWindows ? SMART_REFRESH : SIMPLE_REFRESH);
		 
	if( newMode < 0 ) {
		strcpy(ptr, strProgName);
	} else {
		ConstructWindowTitle(ptr, newMode);
	}
	len = strlen(ptr) + 1;

	if( nw->Title = MemAlloc(len, MEMF_ANY) ) {
		BlockMove(strBuff, nw->Title, len);
	}
}

/*
 * Make new window title
 */
 
void ConstructWindowTitle(register TextPtr ptr, WORD newMode)
{
	strcpy(ptr, iconTitle[newMode]);
	strcat(ptr, strWindTitleSep);
	if( IsDiskPresent() ) {
		strcat(ptr, choiceName);
	} else {
		strcat(ptr, deviceName);
	}
}

/*
 * Open a tool window.
 * Assumes "strBuff" has window-title-to-be (setup from AllocWindowTitle()).
 */
 
static WindowPtr DoOpenWindow(register struct NewWindow *newWindow)
{
	register WindowPtr window;
	WORD zoomSize[4];
	UWORD newLen = 86 + (strlen(strBuff) * _tbXSize);
	
	newWindow->MinWidth = MAX(newLen, newWindow->MinWidth);
/*
	Calculate zoom rect size
*/
	if( intuiVersion >= OSVERSION_3_0 ) {
		zoomSize[0] = zoomSize[1] = -1;		// Size-only zooming
	} else {
		zoomSize[0] = newWindow->LeftEdge;
		zoomSize[1] = newWindow->TopEdge;
	}
	zoomSize[2] = newLen;
	zoomSize[3] = screen->WBorTop + screen->Font->ta_YSize + 1;

	if( intuiVersion < OSVERSION_2_0) {
		window = OpenWindow(newWindow);
	} else {
		docBackFillHook.h_Entry = (ULONG (*)()) DocBackFill;
		window = OpenWindowTags(newWindow, WA_Zoom, &zoomSize[0],
				WA_BackFill, &docBackFillHook, WA_NewLookMenus, TRUE, TAG_END);
	}
	if( (window == NULL) && (newWindow->Title != NULL) ) {
		MemFree(newWindow->Title, strlen(newWindow->Title));
	}
	return(window);
}

/*
 * Common initialization for each type of tool window.
 */
 
static void InitWindow()
{
	register WindowPtr window = cmdWindow;
	
	SetWaitPointer();
	SetWTitle(window, strBuff);
	if (_tbOnPubScreen) {
		SetWindowTitles(window, (TextPtr) -1, screenTitle);
	}
	SetWindowClip(window);
	window->UserPort = mainMsgPort;
	window->UserData = (BYTE *)WKIND_DIALOG;
	InsertMenuStrip(window, menuStrip);
	BuildSigBits();
	SetFont(window->RPort, screen->RastPort.Font);
	closeFlag = FALSE;
}

/*
 *	Remove the specified outline window and free all memory it allocated.
 * If window is NULL, then free what could have been allocated before its failure.
 */

void RemoveWindow(WindowPtr window)
{
	RegionPtr clipRgn;

	SetStdPointer(backWindow, POINTER_WAIT);
	if( window != NULL ) {
		SetWaitPointer();
		ClearMenuStrip(window);
		window->UserPort = mainMsgPort;
		SetWTitle(window, NULL);
		clipRgn = InstallClipRegion(window->WLayer, NULL);
		if( clipRgn != NULL ) {
			DisposeRegion(clipRgn);
		}
		CloseWindowSafely(window, mainMsgPort);
	}
	if( currGadgList != NULL ) {
		DisposeGadgets(currGadgList);
		currGadgList = NULL;
	}
	if( scrollList != NULL ) {
		DisposeScrollList(scrollList);
		scrollList = NULL;
	}
	cmdWindow = NULL;
}

/*
 * Remove the window currently open based on 'mode'.
 */
 
BOOL RemoveToolWindow()
{
	BOOL success = TRUE;
	
	switch(mode) {
	case MODE_TOOLS:
		RemoveWindow(cmdWindow);
		break;
	case MODE_REPAIR:
		CleanupRepairMode();
		break;
	case MODE_RECOVER:
		CleanupRecoverMode();
		break;
	case MODE_OPTIMIZE:
		CleanupReorgMode();
		break;
	case MODE_EDIT:
		success = CleanupEditMode();
		break;
	}
	quitFlag &= success;
	return(success);
}

/*
 *	Remove the window currently open based on 'mode' and
 * then release the current device if mode is not MODE_TOOLS.
 */

BOOL RemoveCurrentWindow(WindowPtr window)
{
	BOOL success;
	
	if( success = RemoveToolWindow() ) {
		if( mode != MODE_TOOLS ) {
			PutDevice();
		}
	}
	return(success);
}

/*
 *	Handle resizing of window
 */

void DoNewSize()
{
	SetWindowClip(cmdWindow);

	switch(mode) {
	case MODE_TOOLS:
		RefreshToolsWindow(TRUE);
		break;
	case MODE_REPAIR:
		RefreshRepairWindow(TRUE);
		break;
	case MODE_RECOVER:
		RefreshRecoverWindow(TRUE);
		break;
	case MODE_OPTIMIZE:
		RefreshReorgWindow(TRUE);
		break;
	case MODE_EDIT:
		RefreshEditWindow(TRUE);
		break;
	}
}

/*
 *	Handle refreshing the window
 */

void DoRefresh(register WindowPtr window)
{
	Rectangle rect;
	
	if( window == backWindow ) {
		if( intuiVersion < OSVERSION_2_0 ) {
			BeginRefresh(window);
			GetWindowRect(window, &rect);
			ClearBackWindow(window->RPort, &rect);
			EndRefresh(window, TRUE);
		}
	} else {
		switch(mode) {
		case MODE_TOOLS:
			RefreshToolsWindow(FALSE);
			break;
		case MODE_REPAIR:
			RefreshRepairWindow(FALSE);
			break;
		case MODE_RECOVER:
			RefreshRecoverWindow(FALSE);
			break;
		case MODE_OPTIMIZE:
			RefreshReorgWindow(FALSE);
			break;
		case MODE_EDIT:
			RefreshEditWindow(FALSE);
			break;
		}
	}	
}

/*
 * Builds signal bits for use by Wait() during operation loops
 */

void BuildSigBits()
{
	sigBits = GetPortSigBit(mainMsgPort);
	sigBits |= GetPortSigBit(appIconMsgPort);
	if( RexxSysBase )
		sigBits |= GetPortSigBit(&rexxMsgPort);
}

/*
 * Utility routine to return the mask of the signal bit of this port
 */

static ULONG GetPortSigBit(MsgPortPtr port)
{
	ULONG result = 0L;

	if( port != NULL ) {
		result = 1 << port->mp_SigBit;
	}
	return(result);
}