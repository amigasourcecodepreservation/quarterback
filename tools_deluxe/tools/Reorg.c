/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Disk optimize/reorganize code.
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <dos/dos.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/intuition.h>

#include <Toolbox/BarGraph.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	Externals
 */

extern TextChar		_strOK[], _strYes[], _strNo[];

extern WindowPtr		cmdWindow;
extern ScreenPtr		screen;
extern MsgPortPtr		mainMsgPort;
extern OptionsRec		options;
extern GadgetPtr		currGadgList;
extern ULONG			dialogVarNum;
extern TextPtr			bigStrsErrors[], iconTitle[], strPath, dialogVarStr;
extern TextChar		strBuff[];
extern TextChar		strOptimizeVerify[];
extern UBYTE			*blockBuffer;
extern WORD				action, mode, stage;
extern ReqTemplPtr		reqList[];
extern RequestPtr		requester;
extern VolParams		currVolParams;
extern BOOL				abortFlag;
extern BadBlockStats		badBlockStats;
extern ReorgStats		reorgStats;
extern ULONG			curDBCount, startSecs;
extern UBYTE			*curDBBase, *curDBPtr, *baseDirFib, *extentBuffer;
extern ULONG			*remapBuffer, *bitMapAltPtr;
extern BarGraphPtr		barGraph;


/*
 * Local prototypes
 */

static WORD			AskReorg(TextPtr);
static BOOL			ProcessControlBlock(ULONG, UBYTE *);
static BOOL			ProcessCacheBlock(ULONG, CacheHdrPtr);
static BOOL			ProcessFileHdr(ULONG, FileHdrPtr);
static BOOL			ProcessUserDir(ULONG, DirBlockPtr);
static BOOL			ProcessExtHdr(ULONG, FileExtPtr);
static BOOL			ProcessHardLink(ULONG, HardLinkHdrPtr);
static BOOL			ProcessSoftLink(ULONG, SoftLinkHdrPtr);
static void			MarkDataKeysBusy(ULONG *);
static ReorgListPtr	SearchExtList(ULONG);
static void			LinkExtList(ReorgListPtr);
static void			UnlinkExtList(ReorgListPtr);
static void			LinkParent(ReorgListPtr, ReorgListPtr);
static BOOL			CreateDummyExt(ULONG, ReorgListPtr);
static ReorgListPtr	CreateDummyParent(ULONG);
static void			CalibrateHardLinks(void);
static void			SetDirCachePointers(void);
static ReorgListPtr	SearchTreeLink(ULONG);
/*static void			CheckForIcon(UBYTE *, ReorgListPtr);*/
static BOOL			CountBlocks(FileHdrEndPtr, UWORD *);
static BOOL			UpdateBlockCounts(void);
static void			AdjustFileEntry(ReorgListPtr);
static ReorgListPtr	BuildFileEntry(ULONG, UBYTE);
static ReorgListPtr	BuildDirEntry(ULONG);
static void			CalcLowestKeys(void);
static ULONG		AdjustBadBlocks(ULONG, ULONG);
static void 		BumpCacheBlocks(ReorgListPtr);
static void			AssignNewKeys(void);
static void			AssignCtrlKeys(ULONG);
static void			AssignDataKeys(ULONG);
static void			AssignFile(ReorgListPtr);
static void			AssignExtent(ReorgListPtr);
static void 		ResortHashChains(ReorgListPtr);
static ULONG		AdjustBlockCount(ReorgListPtr);
static void			CheckBadKey(void);
static void			AdvanceKey(void);
static BOOL			UnmarkDataKeys(void);
static BOOL			InitRemapping(void);
static void			CloseRemapping(void);
static BOOL			ReadExtent(BufCtrlPtr, ReorgListPtr);
static BOOL			WriteExtent(ULONG, ULONG);
static BOOL			RepositionFiles(void);
static BOOL			ReorgNextDir(ReorgListPtr, ReorgVarsPtr);
static BOOL			RepositionLoop(void);
static BOOL			ReattachHardLinks(ReorgListPtr);
static BOOL			AppendEntryName(BufCtrlPtr);
static BOOL			RelocateCtrlBlock(ULONG, ULONG, BufCtrlPtr);
static BOOL			RelocateExtent(BufCtrlPtr, ReorgListPtr, ULONG);
static BOOL			RelocateFile(ReorgListPtr);
static void			RangeCheckKeys(BufCtrlPtr, ReorgListPtr);
static BOOL			UpdateMapping(ULONG, ULONG);
static void			MarkKeyBusyAndDraw(ULONG);
static void			MarkKeyFreeAndDraw(ULONG);
static BOOL			ReadCtrlBlk(ULONG, BufCtrlPtr *);
static BOOL			UpdateCtrlBlock(ReorgListPtr);
static BOOL			UpdateCacheBlock(ReorgListPtr);
static BOOL			UpdateDirEntry(ReorgListPtr, FileHdrPtr);
static BOOL			UpdateFileHdr(ReorgListPtr, FileHdrPtr);
static void			UpdateOFSBlocks(ReorgListPtr, ULONG, ULONG);

/*
 * Assuming all systems are go, start munging, I mean reorganizing, the disk.
 */
 
BOOL DoReorganize()
{
	register BOOL success = FALSE;
	ULONG newPos;
	ULONG dummyMicros;
	BOOL updated;
	
	reorgStats.FinishedEntries = 0;
	CalcLowestKeys();
	AssignNewKeys();
	if( reorgStats.ActiveLinks ) {
		CalibrateHardLinks();
	}
	/*SetDirCachePointers();*/
	if( InitRemapping() ) {
		if( RepositionBitMap(&newPos) ) {
			CurrentTime(&startSecs, &dummyMicros);
			if( success = RepositionFiles() ) {
				SetWaitPointer();
				CloseRemapping();
				updated = UpdateBitMapKeys();
			}
			if( abortFlag || success ) {
				SetWaitPointer();
				WriteBitMap();
				WriteCache();
				FlushCache();
			}
			currVolParams.PreventUninhibit = (success &= updated) && currVolParams.ActiveLocks;
		}
	}
	CloseRemapping();
	SetArrowPointer();
	return(success);
}

/*
 * Verifies that enough free blocks exist to reorganize the volume.
 */
 
BOOL CheckRemapSpace()
{
	register UWORD temp;
	BOOL success = TRUE;
	
	if( options.ReorgOpts.Optimize ) {
		temp = currVolParams.BlockSizeL;
		reorgStats.RemapBlocks = ((temp - 1) + currVolParams.MaxKeys) / temp;
#ifdef OPTION_REMAP
		if( !(reorgStats.MemKeysFlag = options.ReorgOpts.Buffer) ) {
			if( reorgStats.RemapBlocks > currVolParams.FreeKeys ) {
				dialogVarNum = reorgStats.RemapBlocks;
				ExplainDialog(BIG_ERR_REORG_VOL_TOO_FULL);
				success = FALSE;
			}
		}
#else
		if( !(reorgStats.MemKeysFlag = AllocRemapBuffer(reorgStats.RemapBlocks << currVolParams.BlockSizeShift)) ) {
			if( reorgStats.RemapBlocks > currVolParams.FreeKeys ) {
				dialogVarNum = reorgStats.RemapBlocks;
				ExplainDialog(BIG_ERR_REORG_VOL_TOO_FULL);
				success = FALSE;
			}
		} else {
			FreeRemapBuffer();
		}
	}
#endif
	return(success);
}

/*
 * Examine integrity of volume and count files that are fragmented.
 */

BOOL CountFragmentedFiles()
{
	register ULONG block;
	register UWORD index = 0;
	register UWORD remainder;
	register UBYTE *ptr;
	register UBYTE *basePtr;
	register ULONG temp;
	ULONG saveTemp;
	ULONG numBufs;
	BOOL cancelFlag = FALSE;
	TextChar numBuff[16];
	UBYTE *bufPtr = NULL;
	BYTE err;
	
/*
	Free other cache, allocate only the bare minimum to get and keep bad blocks file.
*/
/*
	if( AllocateCache(FALSE) ) {
		(void) LoadBadBlocksFile();
	}
*/
	(void) LoadBadBlocksFile();
	
	if( currVolParams.BlockAccess || (bufPtr = basePtr = AllocTrack(&numBufs)) == NULL ) {
		numBufs = 1;
		basePtr = blockBuffer;
	}
	dialogVarStr = iconTitle[0];

	remainder = numBufs - (currVolParams.LowestKey % numBufs);
	
	for( block = currVolParams.LowestKey ; (block <= currVolParams.HighestKey) && !(abortFlag || cancelFlag) ; block++ ) {
		CheckMainPort();
		if( !abortFlag ) {
			if( index == 0 ) {
				ptr = basePtr;
				if( AnyBadInRange(block, remainder) ) {
					if( GetBadStatus(block) ) {	/* There's bad ones ahead: is this one? */
						index = 1;					/* Nope, so allow I/O to it. */
					} else {
						AddBarGraph(1);
					}
					if( --remainder == 0 ) {		/* In any event, bump remainder down */
						remainder = numBufs;
					}
				} else {
					index = remainder;
					remainder = numBufs;
				}
				if( index ) {							/* Don't try to read if index is 0. */
					DoReadBlock(block, ptr, index << currVolParams.BlockSizeShift, TRUE);
					AddBarGraph(index);
					if( err = WaitBlock() ) {
						ExplainDialog(BIG_ERR_REORG_BAD_BLOCK);
						abortFlag = TRUE;
						index = 0;					/* Force non-processing of block */
					}
				}
			} else {
				ptr += currVolParams.BlockSize;
			}
			if( index ) {
				index--;
/*
	Only processes blocks in use and non-bitmap blocks.
*/
				if( !GetKeyStatus(block) && !IsBitMapKey(block) ) {
					switch( ((FileHdrPtr)ptr)->Type ) {
					case T_SHORT:
						if( !ProcessControlBlock(block, ptr) ) {
							cancelFlag = TRUE;
						}
						break;
					case T_LIST:
						if( ((FileHdrEndPtr)(ptr + currVolParams.BlockSize - sizeof(FileHdrEnd)))->SecType == ST_FILE ) {
							if( !ProcessExtHdr(block, (FileExtPtr)ptr) ) {
								cancelFlag = TRUE;
							}
						}
					case T_DATA:
						break;
					case T_DIRCACHE | 1:
						if( !ProcessCacheBlock(block, (CacheHdrPtr)ptr) ) {
							cancelFlag = TRUE;
						}
						break;
					case T_DIRCACHE:
						if( !currVolParams.FFSFlag ) {
							Error(ERR_PLEASE_VALIDATE);
						}
					default:
						cancelFlag = !currVolParams.FFSFlag;
						break;
					}
				}
			}
		}
	}
	if( bufPtr != NULL ) {
		FreeIOBuffer(&bufPtr, numBufs << currVolParams.BlockSizeShift);
	}
	temp = FALSE;
	/*
	if( !(cancelFlag || abortFlag) ) {
		if( temp = cancelFlag = (!AllocateCache(TRUE)) ) {
			OutOfMemory();
		}
	}
	*/
	if( !temp ) {
		if( (!(cancelFlag || abortFlag)) && UpdateBlockCounts() ) {
			if( (reorgStats.ExtList != NULL) ||
				((reorgStats.DummyTree != NULL) && (reorgStats.DummyTree->rl_Child != NULL)) ) {
				ParseDialogString(bigStrsErrors[BIG_ERR_REORG_MAYBE_BAD], strBuff);
				abortFlag = AskReorg(strBuff);
			} else {
				if( options.PrefsOpts.Interactive != 0 ) {
					abortFlag = FALSE;
				} else {
					dialogVarNum = reorgStats.FragmentedFiles;
					NumToCommaString(reorgStats.ActiveFiles, dialogVarStr = numBuff);
					ParseDialogString(bigStrsErrors[BIG_ERR_REORG_FRAG_REPORT], strBuff);
					if( block = reorgStats.BundleBlocks ) {
						block -= reorgStats.ActiveFiles;
						temp = reorgStats.FinishedEntries;
						while( block > 32767 ) {
							block >>= 1;
							temp >>= 1;
						}
						saveTemp = temp;
						if( block ) {
							temp /= block;
						}
						NumToString(temp, numBuff);
						temp = saveTemp;
						strcat(numBuff, ".");
						strcat(strBuff, numBuff);
						if( block ) {
							temp *= 100;
							temp /= block;
						}
						if( (temp == 0) && reorgStats.FinishedEntries ) {
							temp++;
						}
						NumToString(temp, numBuff);
						if( numBuff[1] == '\0' ) {
							numBuff[1] = numBuff[0];
							numBuff[2] = '\0';
							numBuff[0] = '0';
						}
						strcat(strBuff, numBuff);
						strcat(strBuff, "%");
					}
					abortFlag = AskReorg(strBuff);
				}
			}
		} else {
			ExplainDialog(BIG_ERR_REORG_BAD_DATA);
			abortFlag = TRUE;
		}
	}
	InitBitMapBuffer(bitMapAltPtr);
	ForceRead();
	MotorOff();						/* In case aborted without dialog somehow */
	cancelFlag |= abortFlag;
	return( !cancelFlag );
}

/*
 * A utility routine which puts up a reorganize info requester, but appends
 * a confirmation question to the end if reorganize is enabled (and also
 * creates Y/N buttons instead of just OK).
 */
 
static WORD AskReorg(TextPtr buff)
{
	TextPtr defButtonText;
	UWORD lastButton;
	
	defButtonText = _strOK;
	lastButton = OK_BUTTON;
	if( options.ReorgOpts.Optimize ) {
		strcat(buff, strOptimizeVerify);
		defButtonText = _strYes;
		lastButton++;
	}
	return( DoNonParseDialog(buff, lastButton, defButtonText, _strNo, NULL, NULL, TRUE) );
}

/*
 * Processes a file header or user dir block during the integrity check
 * before reorganization.
 */
 
static BOOL ProcessControlBlock(register ULONG key, register UBYTE *ptr)
{
	register BOOL success = currVolParams.FFSFlag;
	ULONG secType;
	
	if( (key == currVolParams.RootBlock) ||
		((key == ((FileHdrPtr)ptr)->Ownkey) && (((FileHdrPtr)ptr)->Skip1 == 0L)) ) {
		if( VerifyCtrlBlock(ptr, &secType) ) {
			if( CalcBlockChecksum((ULONG *)ptr) == 0L ) {
				switch( secType ) {
				case ST_USERDIR:
					if( ((RootBlockPtr)ptr)->Skip == 0L ) {
						success = ProcessUserDir(key, (DirBlockPtr) ptr);
					}
					break;
				case ST_FILE:
					if( ((FileHdrPtr)ptr)->BlkCnt <= currVolParams.HashTableSize ) {
						success = ProcessFileHdr(key, (FileHdrPtr) ptr);
					}
					break;
				case ST_ROOT:
					if( success = key == currVolParams.RootBlock ) {
						if( success = ((RootBlockPtr)ptr)->Skip == 0L ) {
							++reorgStats.ActiveDirs;
						}
					}
					break;
				case ST_SOFTLINK:
					success = ProcessSoftLink(key, (SoftLinkHdrPtr) ptr);
					break;
				case ST_LINKDIR:
				case ST_LINKFILE:
					success = ProcessHardLink(key, (HardLinkHdrPtr) ptr);
					break;
				default:
					break;
				}
			}
		}
	}
	return(success);
}

/*
 * Processes a file as part of integrity check before reorganization.
 */
 
static BOOL ProcessFileHdr(register ULONG key, FileHdrPtr fileHdrPtr)
{
	register FileHdrEndPtr fileHdrEndPtr;
	register ULONG parentKey = -2;
	register ReorgListPtr reorgEntry;
	ReorgListPtr parentEntry;
	ReorgListPtr foundEntry;
	register ULONG extKey;
	register BOOL success = FALSE;
	
	++reorgStats.ActiveFiles;
	fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));

	extKey = fileHdrEndPtr->Ext;
	
	if( fileHdrEndPtr->Parent != key ) {
		parentKey = fileHdrEndPtr->Parent;
		if( parentEntry = SearchTree(parentKey, &reorgStats.DirTree) ) {
			if( (parentEntry->rl_Type & RTYPE_DIR_MASK) == 0 ) {
				parentKey = -2;
			} else {
				success = TRUE;
			}
		}
	}
/*
	If parent key was set to -2, we have a new filehdr which claims to have a parent
	dir which is already in the dirtree as a FILE.  The error is probably in the 
	new filehdr, since we only enter a file into the dirtree if it is real.
*/
	if( !success ) {
	 	if( (parentEntry = SearchTree(parentKey, &reorgStats.DummyTree)) == NULL ) {
			parentEntry = CreateDummyParent(parentKey);
		}
	}
	if( success = parentEntry != NULL ) {
		if( reorgEntry = SearchTree(key, &reorgStats.DummyTree) ) {
			reorgEntry->rl_Ownkey = -2;		/* Zap the dummy parent's key */
		}
		if( reorgEntry = BuildFileEntry(key, RTYPE_FILE_HDR_MASK) ) {
			reorgEntry->rl_HashEntry = CalcHash(&fileHdrEndPtr->FileName[0]);
			if( CountBlocks(fileHdrEndPtr, &reorgEntry->rl_HBlks) ) {
				reorgEntry->rl_Flags |= RFLAG_HDR_FRAG_MASK;
			}
			if( key == badBlockStats.BadBlocksKey ) {
				reorgEntry->rl_Flags |= RFLAG_DONT_MOVE_MASK;
			} else {
				reorgEntry->rl_Flags |= RFLAG_BUNDLE_MASK;
			}	
			MarkDataKeysBusy( ((ULONG *) ((UBYTE *)(fileHdrPtr+1)) + currVolParams.HashTableSize) );
			
			if( extKey != 0L ) {
				if( (foundEntry = SearchExtList(extKey)) != NULL ) {
					reorgEntry->rl_ExtHdr = foundEntry;
					UnlinkExtList(foundEntry);
				} else {
					success = CreateDummyExt(extKey, reorgEntry);
				}
			}
			if( success ) {
				LinkParent(parentEntry, reorgEntry);
			}
		} else {
			OutOfMemory();
		}
	}
 	return(success);
}
	
/*
 * Process user directory block (see above).
 */
 
static BOOL ProcessUserDir(register ULONG key, DirBlockPtr dirBlockPtr)
{
	register DirBlockEndPtr dirBlockEndPtr;
	register ReorgListPtr reorgListPtr;
	ReorgListPtr parentEntry;
	register ULONG parentKey = -2;
	register BOOL flag = FALSE;
	
	++reorgStats.ActiveDirs;
	dirBlockEndPtr = (DirBlockEndPtr) (((UBYTE *)dirBlockPtr) + currVolParams.BlockSize - sizeof(DirBlockEnd));
	
	if( dirBlockEndPtr->Parent != key ) {
		parentKey = dirBlockEndPtr->Parent;
		if( (parentEntry = SearchTree(parentKey, &reorgStats.DirTree)) != NULL ) {
			if( (parentEntry->rl_Type & RTYPE_DIR_MASK) == 0 ) {
				parentKey = -2;
			} else {
				flag = TRUE;
			}
		} 
	}
	if( !flag ) {
		if( (parentEntry = SearchTree(parentKey, &reorgStats.DummyTree)) == NULL ) {
			flag = (parentEntry = CreateDummyParent(parentKey)) != NULL;
		} else {
			flag = TRUE;
		}
	}
	if( flag ) {
		flag = FALSE;
		if( reorgListPtr = SearchTree(key, &reorgStats.DirTree) ) {
			Error(ERR_DIR_ALREADY_IN_LIST);			/* Should NEVER happen!!! */
		} else {
			if( (reorgListPtr = SearchTree(key, &reorgStats.DummyTree)) != NULL ) {
				flag = TRUE;
				UnlinkDummy(reorgListPtr, reorgStats.DummyTree);
			} else {
				if( !(flag = (reorgListPtr = BuildDirEntry(key)) != NULL) ) {
					OutOfMemory();
				}
			}
			if( flag ) {
				reorgListPtr->rl_HashEntry = CalcHash(&dirBlockEndPtr->DirName[0]);
				LinkParent(parentEntry, reorgListPtr);
			}
		}
	}
	return( flag );
}

/*
 * Process extension header block (see above).
 */
 
static BOOL ProcessExtHdr(ULONG key, FileExtPtr fileExtPtr)
{
	register FileExtEndPtr fileExtEndPtr;
	register ReorgListPtr reorgListPtr;
	register BOOL success = currVolParams.FFSFlag;
	ReorgListPtr foundEntry;
	ULONG extKey;
	
	if( (fileExtPtr->Ownkey == key) &&
		 (fileExtPtr->BlkCnt <= currVolParams.HashTableSize) &&
		 (CalcBlockChecksum((ULONG *)fileExtPtr) == 0) ) {
		
		success = FALSE;
		fileExtEndPtr = (FileExtEndPtr) (((UBYTE *)fileExtPtr) + currVolParams.BlockSize - sizeof(FileExtEnd));
		if( fileExtEndPtr->SecType == ST_FILE ) {	/* It's an ext hdr, right? */
			extKey = fileExtEndPtr->Ext;				/* Save extension field. */
			if( fileExtEndPtr->Parent != key ) {	/* Ext claims itself as parent? */
				success = TRUE;
				if( (reorgListPtr = SearchExtList(key)) != NULL ) {
					if( (reorgListPtr->rl_Flags & RFLAG_DUMMY_MASK) != 0 ) {
						reorgListPtr->rl_Flags &= ~RFLAG_DUMMY_MASK;
						UnlinkExtList(reorgListPtr); /* Get curr exthdr out of list */
						reorgListPtr->rl_Parent->rl_ExtHdr = reorgListPtr;
						reorgListPtr->rl_Parent->rl_Flags &= ~RFLAG_WAIT_MASK;
					}
				} else {
					if( (reorgListPtr = BuildFileEntry(key, RTYPE_HDR_EXT_MASK)) != NULL ) {
						LinkExtList(reorgListPtr);
					} else {
						OutOfMemory();
						return(FALSE);
					}
				}
				++reorgStats.ExtBlocks;
				if( CountBlocks((FileHdrEndPtr)fileExtEndPtr, &reorgListPtr->rl_HBlks) ) {
					reorgListPtr->rl_Flags |= RFLAG_HDR_FRAG_MASK;
				}
				if( fileExtEndPtr->Parent == badBlockStats.BadBlocksKey ) {
					reorgListPtr->rl_Flags |= RFLAG_DONT_MOVE_MASK;
				}
				MarkDataKeysBusy( ((ULONG *) ((UBYTE *)(fileExtPtr+1)) + currVolParams.HashTableSize) );
				if( extKey != 0L ) {
					if( (foundEntry = SearchExtList(extKey)) != NULL ) {
						reorgListPtr->rl_ExtHdr = foundEntry;
						UnlinkExtList(foundEntry);
					} else {
						success = CreateDummyExt(extKey, reorgListPtr);
					}
				}
			}
		}
	}
	return(success);
}

/*
 * Process a hard link.
 */
 
static BOOL ProcessHardLink(ULONG key, HardLinkHdrPtr hardLinkPtr)
{
	register BOOL success = FALSE;
	register ULONG parentKey;
	register ReorgListPtr parentEntry;
	register ReorgListPtr reorgEntry;
	ReorgListPtr foundEntry;
	HardLinkEndPtr hardLinkEndPtr;
	
	++reorgStats.ActiveLinks;
	hardLinkEndPtr = (HardLinkEndPtr)(((UBYTE *)hardLinkPtr) + currVolParams.BlockSize - sizeof(HardLinkEnd));

	if( key != (parentKey = hardLinkEndPtr->Parent) ) {
		if( parentEntry = SearchTree(parentKey, &reorgStats.DirTree) ) {
			if( (parentEntry->rl_Type & RTYPE_DIR_MASK) == 0 ) {
				parentKey = -2;
			} else {
				success = TRUE;
			}
		}
	} else {
		parentKey = -2;
	}
	if( !success ) {
	 	if( (parentEntry = SearchTree(parentKey, &reorgStats.DummyTree)) == NULL ) {
			parentEntry = CreateDummyParent(parentKey);
		}
	}
	if( success = parentEntry != NULL ) {
		if( foundEntry = SearchTree(key, &reorgStats.DummyTree) ) {
			foundEntry->rl_Ownkey = -2;		/* Zap the dummy parent's key */
		}
		if( reorgEntry = BuildFileEntry(key, RTYPE_HARD_LINK_MASK) ) {
			reorgEntry->rl_LinkFrom = hardLinkEndPtr->HardLinkToMe;
			reorgEntry->rl_Child = (ReorgListPtr) hardLinkEndPtr->HardLinkToObject;
			reorgEntry->rl_HashEntry = CalcHash(&hardLinkEndPtr->LinkName[0]);
			LinkParent(parentEntry, reorgEntry);
		} else {
			OutOfMemory();
		}
	}			
	return(success);
}

/*
 * Process a soft link.
 */
 
static BOOL ProcessSoftLink(ULONG key, SoftLinkHdrPtr softLinkPtr)
{
	register BOOL success = FALSE;
	register ULONG parentKey;
	register ReorgListPtr parentEntry;
	register ReorgListPtr reorgEntry;
	ReorgListPtr foundEntry;
	SoftLinkEndPtr softLinkEndPtr;
	
	++reorgStats.ActiveLinks;
	softLinkEndPtr = (SoftLinkEndPtr)(((UBYTE *)softLinkPtr) + currVolParams.BlockSize - sizeof(SoftLinkEnd));

	if( key != (parentKey = softLinkEndPtr->Parent) ) {
		if( parentEntry = SearchTree(parentKey, &reorgStats.DirTree) ) {
			if( (parentEntry->rl_Type & RTYPE_DIR_MASK) == 0 ) {
				parentKey = -2;
			} else {
				success = TRUE;
			}
		}
	} else {
		parentKey = -2;
	}
	if( !success ) {
	 	if( (parentEntry = SearchTree(parentKey, &reorgStats.DummyTree)) == NULL ) {
			parentEntry = CreateDummyParent(parentKey);
		}
	}
	if( success = parentEntry != NULL ) {
		if( foundEntry = SearchTree(key, &reorgStats.DummyTree) ) {
			foundEntry->rl_Ownkey = -2;		/* Zap the dummy parent's key */
		}
		if( reorgEntry = BuildFileEntry(key, RTYPE_SOFT_LINK_MASK) ) {
			reorgEntry->rl_HashEntry = CalcHash(&softLinkEndPtr->LinkName[0]);
			LinkParent(parentEntry, reorgEntry);
		} else {
			OutOfMemory();
		}
	}
	return(success);
}

/*
 * Process one of those $%*$#&@#% directory cache blocks...
 * Return TRUE if not really a cache block or successfully built cache entry.
 */
 
static BOOL ProcessCacheBlock(register ULONG key, CacheHdrPtr cachePtr)
{
	register BOOL success = TRUE;
	register ULONG parentKey;
	register ReorgListPtr parentEntry;
	register ReorgListPtr reorgEntry;
	ReorgListPtr foundEntry;

	if( (key == cachePtr->Ownkey) && currVolParams.DirCache && (CalcBlockChecksum((ULONG *)cachePtr) == 0L) ) {
		success = FALSE;
		++reorgStats.CacheBlocks;
		if( key != (parentKey = cachePtr->Parent) ) {
			if( parentEntry = SearchTree(parentKey, &reorgStats.DirTree) ) {
				if( (parentEntry->rl_Type & RTYPE_DIR_MASK) == 0 ) {
					parentKey = -2;
				} else {
					success = TRUE;
				}
			}
		} else {
			parentKey = -2;
		}
		if( !success ) {
		 	if( (parentEntry = SearchTree(parentKey, &reorgStats.DummyTree)) == NULL ) {
				parentEntry = CreateDummyParent(parentKey);
			}
		}
		if( success = parentEntry != NULL ) {
			++parentEntry->rl_HBlks;		/* Bump parent's count of cache blocks */
			if( foundEntry = SearchTree(key, &reorgStats.DummyTree) ) {
				foundEntry->rl_Ownkey = -2;		/* Zap the dummy parent's key */
			}
			if( reorgEntry = BuildFileEntry(key, RTYPE_CACHE_MASK) ) {
				if( (foundEntry = (ReorgListPtr) parentEntry->rl_NewDatakey) != NULL ) {
					reorgEntry->rl_Next = foundEntry->rl_Next;
					foundEntry->rl_Next = reorgEntry;
				} else {
					parentEntry->rl_NewDatakey = (ULONG) reorgEntry;
				}
				reorgEntry->rl_Parent = parentEntry;
				reorgEntry->rl_HashEntry = -1;
				reorgEntry->rl_Child = (ReorgListPtr) cachePtr->NextBlock;
				/*LinkParent(parentEntry, reorgEntry);*/
			} else {
				OutOfMemory();
			}
		}
	}	
	return(success);
}

/* 
 * Mark all data keys busy in the alternate bitmap, for later use in checking to
 * see if any of the file blocks were actually data blocks in disguise.
 */
 
static void MarkDataKeysBusy(register ULONG *keyPtr)
{
	register UWORD i = 0;
	register UWORD max = currVolParams.HashTableSize;
	
	while( (*--keyPtr != NULL) && (i++ < max) ) {
		MarkAltBusy(*keyPtr);
	}
}

/*
 * Search for the link key "linkKey" in the directory tree in "treePtr".
 * Returns pointer to matching entry, or NULL if not found.
 * NOTE: "treePtr" should be the ADDRESS of the tree you want to search.
 */
 
static ReorgListPtr SearchTreeLink(register ULONG linkKey)
{
	register BOOL cont;
	register ReorgListPtr reorgListPtr;

	cont = TRUE;
	reorgListPtr = (ReorgListPtr) &reorgStats.DirTree;
	do {
		if( reorgListPtr->rl_Next == NULL ) {
			reorgListPtr = reorgListPtr->rl_Parent;
			cont = reorgListPtr != NULL;
		} else {
			reorgListPtr = reorgListPtr->rl_Next;
	
Retry:	if( reorgListPtr->rl_Type & RTYPE_HARD_LINK_MASK ) {
				if( reorgListPtr->rl_Ownkey == linkKey ) {
					cont = FALSE;			/* Found the link entry, it's Miller time */
				}
			} else if( reorgListPtr->rl_Type & RTYPE_DIR_MASK ) {
				if( reorgListPtr->rl_Child != NULL ) {
					reorgListPtr = reorgListPtr->rl_Child;
					goto Retry;
				}
			}
		}
	} while( cont );
	return( reorgListPtr );
}

/*
 * For each hard link in the tree, stuff the entry that it points to into its
 * "rl_Child/rl_ExtHdr" field. The key number was previously placed in the same
 * field while the tree was being built.
 */
 
static void CalibrateHardLinks()
{
	register BOOL cont;
	register ReorgListPtr reorgListPtr;
	
	cont = TRUE;
	reorgListPtr = (ReorgListPtr) &reorgStats.DirTree;
	do {
		if( reorgListPtr->rl_Next == NULL ) {
			reorgListPtr = reorgListPtr->rl_Parent;
			cont = reorgListPtr != NULL;
		} else {
			reorgListPtr = reorgListPtr->rl_Next;
			if( reorgListPtr->rl_Type & RTYPE_HARD_LINK_MASK ) {
				if( reorgListPtr->rl_LinkFrom != 0 ) {
					reorgListPtr->rl_LinkFrom = (ULONG) SearchTreeLink(reorgListPtr->rl_LinkFrom);
				}
				reorgListPtr->rl_Child = (ReorgListPtr) SearchTree((ULONG)reorgListPtr->rl_Child, &reorgStats.DirTree);
			} else if( reorgListPtr->rl_Type & RTYPE_DIR_MASK ) {
				if( reorgListPtr->rl_Child != NULL ) {
					reorgListPtr = reorgListPtr->rl_Child;
				}
			}
		}
	} while( cont );
}


/*
 * Given link keys in "child" field, find its reorgListPtr.
 */
 
/*
static void SetDirCachePointers()
{
	register BOOL cont;
	register ReorgListPtr reorgListPtr;
	
	if( currVolParams.DirCache ) {
		cont = TRUE;
		reorgListPtr = (ReorgListPtr) &reorgStats.DirTree;
		do {
			if( reorgListPtr->rl_Next == NULL ) {
				reorgListPtr = reorgListPtr->rl_Parent;
				cont = reorgListPtr != NULL;
			} else {
				reorgListPtr = reorgListPtr->rl_Next;
				if( reorgListPtr->rl_Type & RTYPE_CACHE_MASK ) {
					if( reorgListPtr->rl_Child != 0 ) {
						reorgListPtr->rl_Child = SearchTree((ULONG)reorgListPtr->rl_Child, &reorgStats.DirTree);	
					}
				} else if( reorgListPtr->rl_Type & RTYPE_DIR_MASK ) {
					if( reorgListPtr->rl_Child != NULL ) {
						reorgListPtr = reorgListPtr->rl_Child;
					}
				}
			}
		} while( cont );
	}
}
*/

/*
 * Examines the filename for ".info".
 * If so, sets icon flag.
 */
 
/*
static void CheckForIcon(register UBYTE *entryName, ReorgListPtr reorgListPtr)
{
	if( options.ReorgOpts.OptimizeType != CLI_OPTIMIZATION ) {
		if( (entryName[0] >= 5) && (CmpString(".info", &entryName[entryName[0]-4], 5, 5, FALSE) == 0) ) {
			reorgListPtr->rl_Flags |= RFLAG_BUNDLE_MASK;
		}
	}
}
*/

/*
 * Looks for match of key passed with Ownkey field.
 * Returns NULL if not found, or ReorgListPtr of entry found.
 */
 
static ReorgListPtr SearchExtList(register ULONG key)
{
	register ReorgListPtr reorgListPtr = reorgStats.ExtList;
	
	while( (reorgListPtr != NULL) && (reorgListPtr->rl_Ownkey != key) ) {
		reorgListPtr = reorgListPtr->rl_Next;
	}
	return(reorgListPtr);
}

/*
 * Link entry passed into ExtList.
 */

static void LinkExtList(ReorgListPtr newEntry)
{
	ReorgListPtr saveEntryPtr = reorgStats.ExtList;
	
	reorgStats.ExtList = newEntry;
	newEntry->rl_Next = saveEntryPtr;
}

/*
 * Remove entry passed from ExtList.
 */

static void UnlinkExtList(register ReorgListPtr removeEntry)
{
	register ReorgListPtr reorgListPtr = (ReorgListPtr) &reorgStats.ExtList;
	
	while( (reorgListPtr != NULL) && (reorgListPtr->rl_Next != removeEntry) ) {
		reorgListPtr = reorgListPtr->rl_Next;
	}
	if( reorgListPtr != NULL ) {
		reorgListPtr->rl_Next = removeEntry->rl_Next;
		removeEntry->rl_Next = NULL;
	}
}

/*
 * Create a dummy ext hdr entry for linking into ExtList.
 * Key passed in "key", parent filehdr/exthdr entry in "parentEntry".
 * Returns TRUE if successfully allocated.
 */
 
static BOOL CreateDummyExt(ULONG key, ReorgListPtr parentEntry)
{
	BOOL success;
	register ReorgListPtr reorgListPtr;
	
	parentEntry->rl_Flags |= RFLAG_WAIT_MASK;
	if( success = (reorgListPtr = BuildFileEntry(key, RTYPE_HDR_EXT_MASK)) != NULL ) {
		reorgListPtr->rl_Flags |= RFLAG_DUMMY_MASK;
		reorgListPtr->rl_Parent = parentEntry;
		LinkExtList(reorgListPtr);
	} else {
		OutOfMemory();
	}
	return(success);
}

/*
 * This routine is called when a file hdr is found which references a parent
 * directory which is not yet present in the directory list.
 * Creates a dummy directory entry for parent key passed.
 * Returns it or NULL if failure.
 */
 
static ReorgListPtr CreateDummyParent(ULONG key)
{
	ReorgListPtr reorgListPtr;
	
	if( (reorgListPtr = BuildDirEntry(key)) != NULL ) {
		LinkChild(reorgStats.DummyTree, reorgListPtr);
	} else {
		OutOfMemory();
	}
	return(reorgListPtr);
}

/*
 * Links new dir entry in "childEntry" to front of child list for 
 * parent dir "parentEntry".
 */
 
void LinkChild(ReorgListPtr parentEntry, register ReorgListPtr childEntry)
{
	ReorgListPtr saveEntryPtr = parentEntry->rl_Child;
	
	parentEntry->rl_Child = childEntry;
	childEntry->rl_Next = saveEntryPtr;
	childEntry->rl_Parent = parentEntry;
}

/*
 * Inserts new entry into parent dir's child chain in order by increasing hashchain
 * offset. Parent in "parentEntry", new dir or file entry in "childEntry".
 */
 
static void LinkParent(ReorgListPtr parentEntry, register ReorgListPtr childEntry)
{
	register ReorgListPtr reorgListPtr;
	register UWORD hash;
	
	childEntry->rl_Parent = parentEntry;
	hash = childEntry->rl_HashEntry;
	reorgListPtr = (ReorgListPtr) &parentEntry->rl_Child;

	while( (reorgListPtr->rl_Next != NULL) && (reorgListPtr->rl_Next->rl_HashEntry <= hash) ) {
		reorgListPtr = reorgListPtr->rl_Next;
	}
	childEntry->rl_Next = reorgListPtr->rl_Next;
	reorgListPtr->rl_Next = childEntry;
}

/*
 * Counts filehdr/exthdr data blocks and returns the number in "count".
 * Returns TRUE if blocks are fragmented.
 */
 
static BOOL CountBlocks(FileHdrEndPtr fileHdrEndPtr, register UWORD *count)
{
	register ULONG key;
	register ULONG startBlock;
	register ULONG *ptr;
	register BOOL fragmented;
	
	ptr = &fileHdrEndPtr->Reserved;
	startBlock = *(ptr-1);
	*count = 0;
	fragmented = FALSE;
	
	while( key = *--ptr ) {				/* If zero, end of blocks reached */
		if( startBlock != key ) {		/* If not matching, then fragmented */
			fragmented = TRUE;
			++reorgStats.FinishedEntries;
		}
/*
	At James' request, lie to user about fragmentation by not counting wrapped files
	as being fragmented files.
*/
		if( ++startBlock > currVolParams.HighestKey ) {
			startBlock = currVolParams.LowestKey;
		}
		if( ++*count >= currVolParams.HashTableSize ) {
			break;
		}
	}
	return( fragmented );
}

/*
 * This is a generic routine which traverses the disk tree.
 * Adds together block counts for each filehdr/exthdrs for all files in DirTree.
 * Also counts exthdrs. Updates total counts in filehdr entry and also updates
 * FILE_FRAG flag for file. Made Non-Recursive for V2.0.
 */

static BOOL UpdateBlockCounts()
{
	register ReorgListPtr saveReorgListPtr;
	register ReorgListPtr reorgListPtr;
	register ULONG errors = 0;
	register BOOL cont = TRUE;
	ReorgListPtr nextPtr, origPtr;
	
	reorgStats.BundleBlocks = 0L;
	reorgListPtr = (ReorgListPtr) &reorgStats.DirTree->rl_Child;
	if( reorgListPtr->rl_Next != NULL ) {		/* If no files, get outta here */
	
		do {
			if( reorgListPtr->rl_Next == NULL ) {
				reorgListPtr = reorgListPtr->rl_Parent;
				cont = reorgListPtr != NULL;
			} else {
				reorgListPtr = reorgListPtr->rl_Next;
				while( (reorgListPtr->rl_Type & RTYPE_DIR_MASK) && (reorgListPtr->rl_Child != NULL) ) {
					reorgListPtr = reorgListPtr->rl_Child;
				}
				if( reorgListPtr->rl_Type & RTYPE_FILE_HDR_MASK ) {
/*
	If this is marked as a data block, then subtract from the file count and
	remove any extent blocks it may possess.
*/
					if( !GetAltStatus(reorgListPtr->rl_Ownkey) ) {
						errors++;
						break;
					}
				}
			}
		} while( cont );
		
		if( errors && currVolParams.FFSFlag && UnmarkDataKeys() ) {
			errors = 0;
		}
		
		if( (errors == 0) && (!abortFlag) ) {
			cont = TRUE;
			reorgListPtr = (ReorgListPtr) &reorgStats.DirTree->rl_Child;
			errors = 0;
			do {
				if( reorgListPtr->rl_Next == NULL ) {
					reorgListPtr = reorgListPtr->rl_Parent;		
					cont = reorgListPtr != NULL;
				} else {
					reorgListPtr = reorgListPtr->rl_Next;
					while( (reorgListPtr->rl_Type & RTYPE_DIR_MASK) && (reorgListPtr->rl_Child != NULL) ) {
						reorgListPtr = reorgListPtr->rl_Child;
					}
					if( reorgListPtr->rl_Type & RTYPE_FILE_HDR_MASK ) {
/*
	If this is marked as a data block, then subtract from the file count and
	remove any extent blocks it may possess.
*/
						if( !GetAltStatus(reorgListPtr->rl_Ownkey) ) {
							--reorgStats.ActiveFiles;
							reorgListPtr->rl_Flags |= RFLAG_WAIT_MASK;
							saveReorgListPtr = reorgListPtr;
							while( reorgListPtr = reorgListPtr->rl_ExtHdr ) {
								UnlinkExtList(reorgListPtr);
							}
							reorgListPtr = saveReorgListPtr;
						} else {
							if( reorgListPtr->rl_Flags & RFLAG_WAIT_MASK ) {
								errors++;
							} else {
								AdjustFileEntry(reorgListPtr);
							}
						}
					}
				}
			} while( cont );
			
			reorgListPtr = reorgStats.ExtList;
			while( reorgListPtr != NULL ) {
				origPtr = reorgListPtr;
				nextPtr = reorgListPtr->rl_Next;
				saveReorgListPtr = reorgListPtr;
				do {
					if( !GetAltStatus(reorgListPtr->rl_Ownkey) ) {
						UnlinkExtList(origPtr);
					}
					reorgListPtr = saveReorgListPtr->rl_Parent;
					saveReorgListPtr = reorgListPtr;
				} while( reorgListPtr != NULL );
				reorgListPtr = nextPtr;
			}
		}
	}
	return( (errors == 0) && (!abortFlag) );
}

/*
 * Checks for fragmented extents and counts icon blocks.
 */
 
static void AdjustFileEntry(register ReorgListPtr fileEntry)
{
	register BOOL fragmented = FALSE;
	register ULONG total = 0;
	register ReorgListPtr reorgListPtr = fileEntry;
	
	do {
		total += reorgListPtr->rl_HBlks;
		if( reorgListPtr->rl_Flags & RFLAG_HDR_FRAG_MASK ) {
			fragmented = TRUE;
		}
	} while( reorgListPtr = reorgListPtr->rl_ExtHdr );
	
	if( fileEntry->rl_Flags & RFLAG_BUNDLE_MASK ) {
		reorgStats.BundleBlocks += total;
	}

	if( fragmented ) {
		fileEntry->rl_Flags |= RFLAG_FILE_FRAG_MASK;
		++reorgStats.FragmentedFiles;
	}
}


/*
 * Builds a full dir entry for linkage into dir tree- key of dir passed in "key".
 * If successful, returns ptr to the new dir entry, else NULL.
 */
 
static ReorgListPtr BuildDirEntry(ULONG key)
{
	return( BuildFileEntry(key, RTYPE_DIR_MASK) );
}

/*
 * Builds a file header entry for linkage into parent dir's child list
 * (whose key is in "key"). Type code is in "fileType" (can be called for
 * ext hdr). If successful, returns ptr to it, else NULL.
 */
  
static ReorgListPtr BuildFileEntry(ULONG key, UBYTE fileType)
{
	register ReorgListPtr reorgListPtr = NULL;
	register const UWORD size = sizeof(ReorgList);
	
	if( (curDBCount >= size) || AllocateDirBlock() ) {
		reorgListPtr = (ReorgListPtr) curDBPtr;
		reorgListPtr->rl_Ownkey = key;
		reorgListPtr->rl_Type = fileType;
		curDBCount -= size;
		curDBPtr += size;
	}
	return(reorgListPtr);
}

/*
 * Initialize dir/file lists
 */
 		
BOOL InitDirLists()
{
	BOOL success = FALSE;
	
	curDBBase = (UBYTE *) &baseDirFib;
	curDBCount = 0L;
	if( reorgStats.DirTree = BuildDirEntry(currVolParams.RootBlock) ) {
		if( reorgStats.DummyTree = BuildDirEntry(-1) ) {
			success = TRUE;
			reorgStats.ExtList = NULL;
		} else {
			FreeDirBlocks();
		}
	}
	if( !success ) {
		OutOfMemory();
	}
	return(success);
}

/*
 * This routine calculates the amount of space which must be reservd immediately
 * following the root block for filehdrs and userdirs. Exthdrs are NOT counted here,
 * since they are stored with the data blocks. Bitmap blocks, bitmap ext blocks, and
 * the root are counted. Bad blocks in range must be counted.
 */
 
static void CalcLowestKeys()
{
	register ULONG temp;
	register ULONG keys = reorgStats.BundleBlocks + reorgStats.ExtBlocks;
	
	reorgStats.LowestCtrlKey = (currVolParams.RootBlock + 1) + currVolParams.BitMapBlocks + currVolParams.BitMapExtBlks;
	
	temp = reorgStats.ActiveFiles + reorgStats.ActiveDirs + reorgStats.ActiveLinks + reorgStats.CacheBlocks;
	if( options.ReorgOpts.OptimizeType == WB_OPTIMIZATION ) {
		temp += keys;
	}
	temp--;												/* Subtract root (but not its icon) */
	reorgStats.LowestFreeKey = AdjustBadBlocks(reorgStats.LowestCtrlKey, temp);
	
	if( (options.ReorgOpts.OptimizeType == WB_OPTIMIZATION) || (!badBlockStats.BadBlockFlag) ) {
		temp = reorgStats.LowestFreeKey + currVolParams.FreeKeys;
		
		if( temp > currVolParams.HighestKey ) {	/* Starts in upper end... */
			temp -= currVolParams.MaxKeys;			/* Else swap to lower end.*/
		}
	} else {
		temp = currVolParams.RootBlock;				/* This assures no unused blocks... */
		while( keys-- ) {									/* ...will exist before root block */
			while( !GetBadStatus(temp--) ) {
				if( temp < currVolParams.LowestKey ) {
					temp = currVolParams.HighestKey;
				}
			}
		}
	}
	reorgStats.LowestDataKey = temp;				/* First data block goes here */
}

/*
 * Calculates number of bad blocks exist in the range from base "baseKey" to
 * "baseKey"+"numBlocks", and adds it to "baseKey"+"numBlocks".
 */
 
static ULONG AdjustBadBlocks(ULONG baseKey, register ULONG numBlocks)
{
	register ULONG num = baseKey + numBlocks;

	if( num > currVolParams.HighestKey ) {
		num -= currVolParams.MaxKeys;
	}
	if( badBlockStats.BadBlockFlag ) {
		reorgStats.CurrKey = baseKey;
		while( numBlocks-- ) {
			AdvanceKey();
			CheckBadKey();
		}
		num = reorgStats.CurrKey;
	}
	return(num);
}

/*
 * Assigns new key to each entry in catalog. All entries in a given directory are
 * grouped together in order by hashchain. Exthdrs are allocated immediately before
 * the data blocks. Data blocks are grouped contiguously, with a couple of minor
 * exceptions relating to bad blocks (and files which cannot be moved). Data blocks
 * for all files (except for the bad blocks file, which is not bundleable) are placed 
 * immediately after the filehdr if reorganizing into "file-access" (formerly
 * Workbench) mode. Otherwise, they are completely separate from the control blocks.
 */
 
static void AssignNewKeys()
{
	reorgStats.DirTree->rl_Newkey = currVolParams.RootBlock;
	AssignCtrlKeys(reorgStats.LowestCtrlKey);
	if( options.ReorgOpts.OptimizeType == CLI_OPTIMIZATION ) {
		AssignDataKeys(reorgStats.LowestDataKey);
	}
}

/*
 * Bump directory cache blocks.
 */
 
static void BumpCacheBlocks(register ReorgListPtr nextPtr)
{
	if( nextPtr->rl_Type & RTYPE_DIR_MASK ) {
		nextPtr = (ReorgListPtr) nextPtr->rl_NewDatakey;
		while( nextPtr != NULL ) {
			CheckBadKey();
			nextPtr->rl_Newkey = reorgStats.CurrKey;
			AdvanceKey();
			nextPtr = nextPtr->rl_Next;
		}
	}
}

/*
 * Call whenever a drawer's contents wrap around from the high end to the 
 * low end of the disk and you must sort the hash chains (in FFS) in
 * ascending order. I believe you will only find one hash chain at most
 * to adjust, and most likely, the hash chain will already be OK.
 */

static void ResortHashChains(register ReorgListPtr reorgListPtr)
{
	register UWORD currHash = -1;
	ReorgListPtr startEntry = reorgListPtr;
	register ULONG lastKey = 0;
	register BOOL sortFlag;
	ReorgListPtr temp;
	ReorgListPtr prevEntry = (ReorgListPtr) &reorgListPtr->rl_Parent->rl_Child;
	ReorgListPtr savePrevEntry, nextEntry;
	
	while( (reorgListPtr != NULL) && (currHash == -1) ) {
		if( reorgListPtr->rl_Newkey < lastKey ) {
			currHash = reorgListPtr->rl_HashEntry;
		}
		lastKey = reorgListPtr->rl_Newkey;
		reorgListPtr = reorgListPtr->rl_Next;
	}
	reorgListPtr = startEntry;
	
	while( reorgListPtr != NULL ) {
		if( reorgListPtr->rl_HashEntry == currHash ) {
/*
	OK, we are now at the first entry of the portion of the list which MAY
	need resorting. We will examine all entries which match the hash value
	and resort them if necessary.
*/
			startEntry = reorgListPtr;
			savePrevEntry = prevEntry;
			
			do {
				reorgListPtr = startEntry;
				prevEntry = savePrevEntry;
				sortFlag = FALSE;
				
				while( (reorgListPtr->rl_Next != NULL) && (reorgListPtr->rl_Next->rl_HashEntry == currHash) ) {
					nextEntry = reorgListPtr->rl_Next;
					if( reorgListPtr->rl_Newkey > reorgListPtr->rl_Next->rl_Newkey ) {
						sortFlag = TRUE;
						prevEntry->rl_Next = reorgListPtr->rl_Next;
						nextEntry = reorgListPtr;
						temp = reorgListPtr->rl_Next->rl_Next;
						reorgListPtr->rl_Next->rl_Next = reorgListPtr;
						prevEntry = reorgListPtr->rl_Next;
						reorgListPtr->rl_Next = temp;
					} else {
						prevEntry = reorgListPtr;
					}
					reorgListPtr = nextEntry;
				}
			} while( sortFlag );
			
			reorgListPtr = NULL;					/* Miller time */
		} else {
			prevEntry = reorgListPtr;
			reorgListPtr = reorgListPtr->rl_Next;
		}
	}
}

typedef struct {
	struct MinNode	Node;
	ULONG				StartKey;
	ReorgListPtr	DirLevel;
} ReorgScanVars, *ReorgScanVarsPtr;

/*
 * Assigns keys to control blocks (user dirs and filehdrs), based on key passed.
 * File data blocks are NOT assigned here unless file is marked as bundled,
 * meaning that it is not a bad blocks file, and we are optimizing for file access.
 */
 
static void AssignCtrlKeys(ULONG key)
{
	struct List myList;
	ReorgScanVars initVars, *newVars;
	register ReorgScanVarsPtr vars = &initVars;	
	register BOOL cont = TRUE;
	register ReorgListPtr nextPtr;
	ReorgListPtr reorgListPtr;
	register BOOL isBundled = options.ReorgOpts.OptimizeType == WB_OPTIMIZATION;
	
	reorgStats.CurrKey = key;	
	
	reorgListPtr = nextPtr = (ReorgListPtr) &reorgStats.DirTree->rl_Child;
	vars->DirLevel = nextPtr;
	vars->StartKey = key;
	NHInitList(&myList);
		
	BumpCacheBlocks(reorgStats.DirTree);
	do {
		while( (nextPtr = nextPtr->rl_Next) != NULL ) {
			CheckBadKey();
			nextPtr->rl_Newkey = reorgStats.CurrKey;
			AdvanceKey();
			if( ((nextPtr->rl_Type & RTYPE_DIR_MASK) == 0) &&
				 (nextPtr->rl_Type & RTYPE_FILE_HDR_MASK) &&
				 (/*(nextPtr->rl_Flags & RFLAG_BUNDLE_MASK) &&*/ isBundled) ) {
				AssignFile(nextPtr);
			}
		}
		nextPtr = reorgListPtr;
		if( currVolParams.DirCache ) {
			while( (nextPtr = nextPtr->rl_Next) != NULL ) {
				BumpCacheBlocks(nextPtr);
			}
		}
Fnd1:	nextPtr = reorgListPtr;
		while( (nextPtr = nextPtr->rl_Next) && ((nextPtr->rl_Type & RTYPE_DIR_MASK) == 0) );
		if( nextPtr != NULL ) {
			if( (newVars = (ReorgScanVarsPtr) MemAlloc(sizeof(ReorgScanVars), MEMF_CLEAR)) != NULL ) {
				vars->DirLevel = nextPtr;
				AddHead(&myList, (NodePtr) &vars->Node);
				vars = newVars;
				vars->StartKey = reorgStats.CurrKey;
				reorgListPtr = nextPtr = (ReorgListPtr) &nextPtr->rl_Child;
			} else {
				OutOfMemory();
				nextPtr = NULL;
			}
		}
		if( nextPtr == NULL ) {
			if( (reorgStats.CurrKey < vars->StartKey) && currVolParams.FFSFlag ) {
				ResortHashChains(reorgListPtr);
			}
			do {
				if( cont = vars != &initVars ) {
					MemFree(vars, sizeof(ReorgScanVars));
					vars = (ReorgScanVarsPtr) RemHead(&myList);
					reorgListPtr = vars->DirLevel;
				}
			} while( abortFlag && cont );
			if( cont ) {
				goto Fnd1;
			}
		}
	} while( cont );
}

/*
 * Assigns keys to files, based on key passed.
 */
 
static void AssignDataKeys(ULONG key)
{
	struct List myList;
	ReorgScanVars initVars, *newVars;
	register ReorgScanVarsPtr vars = &initVars;	
	register BOOL cont = TRUE;
	register ReorgListPtr nextPtr;
	ReorgListPtr reorgListPtr;
/*	register BOOL notBundled = options.ReorgOpts.OptimizeType != WB_OPTIMIZATION;*/
	
	reorgStats.CurrKey = key;

	reorgListPtr = nextPtr = (ReorgListPtr) &reorgStats.DirTree->rl_Child;
	vars->DirLevel = nextPtr;
	vars->StartKey = key;
	NHInitList(&myList);
	
	do {
		while( (nextPtr = nextPtr->rl_Next) != NULL ) {
			if( (nextPtr->rl_Type & RTYPE_FILE_HDR_MASK) /*&&
				 (notBundled || ((nextPtr->rl_Flags & RFLAG_BUNDLE_MASK) == 0))*/ ) {
				AssignFile(nextPtr);
			}
		}
Fnd2:	nextPtr = reorgListPtr;
		while( (nextPtr = nextPtr->rl_Next) && ((nextPtr->rl_Type & RTYPE_DIR_MASK) == 0) );
		if( nextPtr != NULL ) {
			if( (newVars = (ReorgScanVarsPtr) MemAlloc(sizeof(ReorgScanVars), MEMF_CLEAR)) != NULL ) {
				vars->DirLevel = nextPtr;
				AddHead(&myList, (NodePtr) &vars->Node);
				vars = newVars;
				vars->StartKey = reorgStats.CurrKey;
				reorgListPtr = nextPtr = (ReorgListPtr) &nextPtr->rl_Child;
			} else {
				OutOfMemory();
				nextPtr = NULL;
			}
		}
		if( nextPtr == NULL ) {
			if( (reorgStats.CurrKey < vars->StartKey) && currVolParams.FFSFlag ) {
				ResortHashChains(reorgListPtr);
			}
			do {
				if( cont = vars != &initVars ) {
					MemFree(vars, sizeof(ReorgScanVars));
					vars = (ReorgScanVarsPtr) RemHead(&myList);
					reorgListPtr = vars->DirLevel;
				}
			} while( abortFlag && cont );
			if( cont ) {
				goto Fnd2;
			}
		}
	} while( cont );
}

/*
 * Assigns current keys to file whose entry is passed.
 */
 
static void AssignFile(ReorgListPtr entryPtr)
{
	register ReorgListPtr reorgListPtr = (ReorgListPtr) entryPtr->rl_ExtHdr;
	
	while( reorgListPtr != NULL ) {
		CheckBadKey();
		reorgListPtr->rl_Newkey = reorgStats.CurrKey;
		AdvanceKey();
		reorgListPtr = reorgListPtr->rl_ExtHdr;
	}
	
	AssignExtent(entryPtr);
	reorgListPtr = (ReorgListPtr) entryPtr->rl_ExtHdr;
	while( reorgListPtr != NULL ) {
		AssignExtent(reorgListPtr);
		reorgListPtr = reorgListPtr->rl_ExtHdr;
	}
}

/*
 * Assigns data block keys to extent passed. If the extent must wrap around
 * one or more bad keys, current key is adjusted properly, and the bad keys get
 * skipped at a later time.
 */
 
static void AssignExtent(register ReorgListPtr reorgListPtr)
{
	if( reorgListPtr->rl_HBlks ) {
		if( (reorgListPtr->rl_Flags & RFLAG_DONT_MOVE_MASK) == 0 ) {
			CheckBadKey();
			reorgListPtr->rl_NewDatakey = reorgStats.CurrKey;
			reorgStats.CurrKey += AdjustBlockCount(reorgListPtr);
			if( reorgStats.CurrKey > currVolParams.HighestKey ) {
				reorgStats.CurrKey -= currVolParams.MaxKeys;
				reorgListPtr->rl_Flags |= RFLAG_WRAPPED_MASK;
			}
		}
	}
}

/*
 * Given a filehdr/exthdr entry, returns adjusted count of blocks.
 * Adds one to block count for every bad key that falls into the range of extent.
 */
 
static ULONG AdjustBlockCount(ReorgListPtr reorgListPtr)
{
	register ULONG result;
	register ULONG blocks;
	register ULONG key;
	
	result = reorgListPtr->rl_HBlks;
	
	if( badBlockStats.BadBlockFlag ) {
		blocks = result;
		key = reorgListPtr->rl_NewDatakey;
		while( blocks ) {
			if( ++key > currVolParams.HighestKey ) {
				key = currVolParams.LowestKey;
			}
			if( !GetBadStatus(key) ) {
				++result;
			} else {
				blocks--;
			}
		}
	}
	return(result);
}

/*
 * Checks that current key is not a bad block. Advances key if so.
 */
 
static void CheckBadKey()
{
	if( badBlockStats.BadBlockFlag ) {
		while( !GetBadStatus(reorgStats.CurrKey) ) {
			AdvanceKey();
		}
	}
}

/*
 * Advances the current key: if reaches top, is sent back to bottom.
 */
 		
static void AdvanceKey()
{
	if( ++reorgStats.CurrKey > currVolParams.HighestKey ) {
		reorgStats.CurrKey = currVolParams.LowestKey;
	}
}

/*
 * Process the whole tree and find conflicts
 */
 
static BOOL UnmarkDataKeys()
{
	register BOOL cont;
	struct List myList;
	DirScanVars initVars, *newVars;
	register DirScanVarsPtr vars = &initVars;
	register ULONG key = currVolParams.RootBlock;
	register FileHdrEndPtr fileHdrEndPtr;
	register FileHdrPtr fileHdrPtr;
	ULONG blockType;
	register WORD err = 0;
	ULONG max = reorgStats.ActiveFiles;
	
	stage = STAGE_RESOLVING;
	RefreshFragmentationLabel();
	SetBarGraph(cmdWindow->RPort, barGraph, currVolParams.DoneBlocks = 0);
	SetBarGraphMax(cmdWindow->RPort, barGraph, max);
	NHInitList(&myList);
	do {
Loop:	vars->CurDirKey = key;
		if( ReadControl(key, &vars->CurDirBuf) == CERR_NOERR ) {
			UnlinkCache(vars->CurDirBuf);
			vars->Offset = -sizeof(ULONG);
			vars->Counter = currVolParams.HashTableSize;
AdvHash:	vars->Offset += sizeof(ULONG);
			if( (vars->Counter--) == 0 ) {
				PutCacheBuffer(vars->CurDirBuf);
				goto Pop;
			}
NextHash:vars->HashChainPred = NULL;			/* Predecessor is parent dir */
			if( (vars->KeyHashChain = *((ULONG *)(((UBYTE *)vars->CurDirBuf) + (sizeof(BufCtrl) + sizeof(DirBlock)) + vars->Offset))) == NULL ) {
				goto AdvHash;							/* No entry in hash table...try next */	
			}
NextLink:CheckMainPort();
			if( abortFlag ) {
				goto Pop;
			}
			vars->HashChainSucc = NULL;
			if( !CheckKey(vars->KeyHashChain) ) {
				goto EntryEnd;
			}
			if( GetCacheBuffer(&vars->CurEnt) == CERR_NOERR) {
				fileHdrPtr = (FileHdrPtr)(vars->CurEnt+1);
				if( ReadBlock(vars->KeyHashChain, (UBYTE *)fileHdrPtr, FALSE) ) {
					goto EntryEnd;
				}
				fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd) );
									
				if( !CheckCtrlTypes(fileHdrPtr, &blockType) ) {
					goto EntryEnd;
				}
					
				if( blockType != ST_ROOT ) {
					if( (vars->KeyHashChain != fileHdrPtr->Ownkey) ||
						(fileHdrPtr->Skip1 != NULL) ) {
						err++;
						goto EntryEnd;
					}
					if( (vars->HashChainSucc = fileHdrEndPtr->HashChain) != NULL ) {
						if( !CheckKey(vars->HashChainSucc) ) {
							err++;
							PutCacheBuffer(vars->CurDirBuf);
							goto Pop;
						}
					}
					if( fileHdrEndPtr->Parent != vars->CurDirKey ) {
						err++;
						goto EntryEnd;
					}
					LinkCacheKey(vars->KeyHashChain, BLOCK_CONTROL, vars->CurEnt);
					if( CalcHash(fileHdrEndPtr->FileName) != vars->Offset ) {
						err++;
						goto EntryEnd;
					}
					key = vars->KeyHashChain;
					switch(fileHdrEndPtr->SecType) {
					case ST_FILE:
						BumpBarGraph();
						MarkAltFree(key);
						key = fileHdrEndPtr->Ext;
						while( key != NULL ) {
							if( CheckKey(key) && (ReadBlock(key, blockBuffer, FALSE) == 0) ) {
								MarkAltFree(key);
								key = *(((ULONG *)blockBuffer) + currVolParams.BlockSize-2);
							} else {
								key = NULL;
							}
						}
						goto EntryEnd;
					case ST_USERDIR:
						MarkAltFree(key);
						if( (newVars = (DirScanVarsPtr) MemAlloc(sizeof(DirScanVars), MEMF_CLEAR)) != NULL ) {
							AddHead(&myList, (NodePtr) &vars->Node);
							vars = newVars;
							goto Loop;
						} else {
							OutOfMemory();
						}
					}
				}
			}
		}
Pop:	if( cont = vars != &initVars ) {
			MemFree(vars, sizeof(DirScanVars));
			vars = (DirScanVarsPtr) RemHead(&myList);
EntryEnd:vars->HashChainPred = vars->KeyHashChain;
			if( vars->KeyHashChain = vars->HashChainSucc ) {
				goto NextLink;
			}
			goto AdvHash;
		}
	} while( cont );
	SetBarGraph(cmdWindow->RPort, barGraph, max);
	return((err == 0) && (!abortFlag));
}

/*
 * Allocates a buffer for the keys of these blocks, allocates the blocks,
 * and zaps them to zero.
 */
 
static BOOL InitRemapping()
{
	register ULONG size = reorgStats.RemapBlocks;
	register ULONG num;
	register ULONG numBlocks = size;
	register ULONG *ptr;
	register RastPtr rPort = cmdWindow->RPort;
	ULONG max = currVolParams.MaxKeys - currVolParams.FreeKeys - (currVolParams.BitMapBlocks + currVolParams.BitMapExtBlks);
	BufCtrlPtr buf;
	ULONG *savePtr;
	BOOL success;
	ULONG numBufs;
	ULONG trkBufSize;
	ULONG startBlock;
	UBYTE *trkBuf;
	register ULONG i;
	
	if( reorgStats.MemKeysFlag ) {
		size <<= currVolParams.BlockSizeShift;
		if( !(success = AllocRemapBuffer(size)) ){
			OutOfMemory();
		}
	} else {
		stage = STAGE_REMAP;
		RefreshFragmentationLabel();
		SetBarGraph(rPort, barGraph, currVolParams.DoneBlocks = 0);
		SetBarGraphMax(rPort, barGraph, size + size);
		size <<= 2;								/* Gotta prepare disk for table. */
		if( success = AllocRemapBuffer(size) ) {
			savePtr = ptr = remapBuffer;
			currVolParams.LastUsedKey = reorgStats.LowestFreeKey;
			num = numBlocks;
			while( num-- ) {
				if( !AllocFreeKey() ) {
					return(FALSE);
				}
				*ptr++ = currVolParams.LastUsedKey;
			}
/*
	For speed on huge drives, attempt to read as many consecutive tracks as possible,
	regardless of start position on each block, with a maximum of 100 or "BlocksPerTrack".
*/
			numBufs = MIN(MAX_TRACK_BUFS, currVolParams.Envec.de_BlocksPerTrack);
			trkBufSize = numBufs << currVolParams.BlockSizeShift;
			ptr = savePtr;
			if( (trkBuf = AllocIOBuffer(trkBufSize)) != NULL ) {
				num = numBlocks;
				while( num != 0 ) {
					startBlock = *ptr;
					numBufs = MIN(num, numBufs);
					for( i = 1 ; i < numBufs ; i++ ) {
						if( *ptr != (*(ptr+1))-1 ) {
							break;
						}
						ptr++;
					}
					DoWriteBlock(startBlock, trkBuf, i << currVolParams.BlockSizeShift, TRUE);
					AddBarGraph(i);
					CheckMainPort();
					if( (WaitBlock() != 0) || abortFlag ) {
						FreeIOBuffer(&trkBuf, trkBufSize);
						return(FALSE);
					}
					num -= i;
					ptr++;
				}
				FreeIOBuffer(&trkBuf, trkBufSize);
			} else { 
				BlockClear(blockBuffer, currVolParams.BlockSize);
				num = numBlocks;
				while( num-- ) {
					WriteBlock(*ptr++, blockBuffer, TRUE);
					BumpBarGraph();
					CheckMainPort();
					if( (WaitBlock() != 0) || abortFlag ) {
						return(FALSE);
					}
				}
			}
			ptr = savePtr;
			num = numBlocks;
			reorgStats.CurrKey = reorgStats.LowestFreeKey;
			while( num-- ) {							/* Read the remap blocks. */
				CheckMainPort();
				if( (!abortFlag) && ReadSpecial(*ptr, &buf) == CERR_NOERR ) {
					MarkKeyFree(*ptr);
					CheckBadKey();
					RelocateBlock(reorgStats.CurrKey, *ptr);
					UnlinkCache(buf);
					buf->DirtyFlag = TRUE;
					LinkCacheKey(reorgStats.CurrKey, BLOCK_SPECIAL, buf);
					MarkKeyBusy(*ptr);
					*ptr++ = reorgStats.CurrKey;
					AdvanceKey();
					BumpBarGraph();
				} else {
					return(FALSE);
				}
			}
		}
	}
	SetBarGraph(rPort, barGraph, currVolParams.DoneBlocks = 0);
	SetBarGraphMax(rPort, barGraph, max);
	stage = STAGE_REORG;
	RefreshFragmentationLabel();
	return(success);
}

/*
 * Releases remapping blocks and buffer.
 */
 
static void CloseRemapping()
{
	register ULONG size;
	register ULONG *ptr;
	
	if( remapBuffer != NULL ) {
		if( !reorgStats.MemKeysFlag ) {
			size = reorgStats.RemapBlocks;
			ptr = remapBuffer;
			if( ptr != NULL ) {
				while( size-- ) {
					MarkKeyFree(*ptr++);
					++currVolParams.FreeKeys;
				}
			}
		}
		FreeRemapBuffer();
	}
}

/*
 * Repositions everything except bitmap blocks.
 */
 
static BOOL RepositionFiles()
{
	BOOL success;

	success = RepositionLoop();
	RefreshFragmentationMap();
	(void) WriteCache();
	return(success);
}

/*
 * The main reorganization loop!
 * Instead of the V1.x Tools recursion, we simply use the existing tree structure.
 * In keeping with the old style, we process all of the files/dirs in a directory
 * and enter any subdirectories only after the scan has completed for the current
 * directory.
 */
 
static BOOL RepositionLoop()
{
	register ReorgListPtr reorgListPtr = (ReorgListPtr) &reorgStats.DirTree;
	register BOOL cont = TRUE;
	register ReorgListPtr currEntry;
	register BOOL success = TRUE;
	BufCtrlPtr buf;
	ULONG key;
	register ULONG newKey;
	register BOOL processChild;
	ReorgListPtr saveEntry;
	
	do {
		saveEntry = reorgListPtr;
		reorgListPtr = reorgListPtr->rl_Next;
		
		while( (reorgListPtr != NULL) && ((reorgListPtr->rl_Flags & RFLAG_WAIT_MASK) == 0) && success ) {
			currEntry = reorgListPtr;
			do {
				newKey = currEntry->rl_Newkey;
				key = currEntry->rl_Ownkey;
				if( success = RemapKey(&key) ) {
					currEntry->rl_Flags |= RFLAG_PROCESSED_MASK;
					if( success = ReadCtrlBlk(key, &buf) ) {
						BumpBarGraph();
						if( ((currEntry->rl_Type & (RTYPE_HDR_EXT_MASK | RTYPE_CACHE_MASK)) == 0) &&
							(newKey != currVolParams.RootBlock) ) {
							AppendEntryName(buf);
						}
						if( success = ((newKey == key) || RelocateCtrlBlock(key, newKey, buf)) ) {
							if( reorgListPtr->rl_Type & RTYPE_FILE_HDR_MASK ) {
								currEntry = currEntry->rl_ExtHdr;
							} else {
								if( currEntry->rl_Type & RTYPE_CACHE_MASK ) {
									currEntry = currEntry->rl_Next;
								} else if( (currEntry->rl_Type & RTYPE_DIR_MASK) &&
									(currEntry->rl_NewDatakey != NULL) ) {
									currEntry = (ReorgListPtr) currEntry->rl_NewDatakey;
								} else {
									goto Out;
								}
							}
						}
					}
				}
			} while( success && (currEntry != NULL) );
			if( success ) {
				if( reorgListPtr->rl_Type & RTYPE_FILE_HDR_MASK ) {
					if( success = RelocateFile(reorgListPtr) ) {
						success = UpdateCtrlBlock(reorgListPtr);
					}
				}
Out:			TruncatePath();
				reorgListPtr = reorgListPtr->rl_Next;
				CheckMainPort();
				success &= !abortFlag;
			}
			cont &= success;
		}
/*
	Now find another user dir and process that too.
	NOTE: "saveEntry" should NEVER be NULL!
*/
		reorgListPtr = saveEntry;
Dir:	processChild = FALSE;
		while( ((currEntry = reorgListPtr->rl_Next) != NULL) && success && (!processChild) ) { 
			if( currEntry->rl_Type & (RTYPE_DIR_MASK | RTYPE_HARD_LINK_MASK | RTYPE_SOFT_LINK_MASK | RTYPE_CACHE_MASK) ) {
				reorgStats.FinishedEntries++;
				if( currEntry->rl_Newkey != currVolParams.RootBlock ) {
					if( currEntry->rl_Type & RTYPE_DIR_MASK ) {
						if( currEntry->rl_Child != NULL ) {
							processChild = TRUE;
							if( success = ReadCtrlBlk(currEntry->rl_Newkey, &buf) ) {
								(void) AppendEntryName(buf);
							}
						}
					} else if( currEntry->rl_Type & RTYPE_HARD_LINK_MASK ) {
						success = ReattachHardLinks(currEntry);
					}
				} else {
					processChild = currEntry->rl_Child != NULL;
				}
				if( success ) {
					success = UpdateCtrlBlock(currEntry);
				}
			}
			reorgListPtr = currEntry;
		}
		if( processChild ) {
			reorgListPtr = (ReorgListPtr) &reorgListPtr->rl_Child;
		} else if( cont &= success ) {
			if( cont = (reorgListPtr->rl_Parent != NULL) ) {
				TruncatePath();
				reorgListPtr = reorgListPtr->rl_Parent;
				goto Dir;
			}
		}
	} while( cont );
	return(success);
}
		
/*
 * Get whatever hard link is pointing to this buffer and adjust it.
 */
 
static BOOL ReattachHardLinks(register ReorgListPtr currEntry)
{
	ULONG key;
	FileHdrEndPtr fileHdrEndPtr;
	register HardLinkEndPtr hardLinkEndPtr;
	BufCtrlPtr buf;
	BufCtrlPtr buf2;
	BOOL success = FALSE;

	if( ReadCtrlBlk(currEntry->rl_Newkey, &buf) ) {
		hardLinkEndPtr = (HardLinkEndPtr) (((UBYTE *)buf) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(HardLinkEnd));
		key = currEntry->rl_Child->rl_Ownkey;
		if( (currEntry->rl_Flags & RFLAG_PROCESSED_MASK) || RemapKey(&key) ) {
			if( ReadCtrlBlk(key, &buf2) ) {
				hardLinkEndPtr->HardLinkToObject = currEntry->rl_Child->rl_Newkey;
				fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)buf2) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(FileHdrEnd));
				if( fileHdrEndPtr->HardLinkToMe == currEntry->rl_Ownkey ) {
					fileHdrEndPtr->HardLinkToMe = currEntry->rl_Newkey;
					buf2->DirtyFlag = TRUE;
				}
				success = TRUE;
			}
		}
		if( currEntry->rl_LinkFrom != NULL ) {
			hardLinkEndPtr->HardLinkToMe = ((ReorgListPtr)currEntry->rl_LinkFrom)->rl_Newkey;
		}
	}
	return(success);
}


/*
 * Append the passed buf's entry name to the global variable "strPath".
 */
 
static BOOL AppendEntryName(BufCtrlPtr buf)
{
	FileHdrEndPtr fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)buf) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(FileHdrEnd));
	register UWORD len = fileHdrEndPtr->FileName[0];
	register UWORD pathLen = strlen(strPath);
	BOOL success = len && ((pathLen + len) < (PATHSIZE - 2));
	
	if( success ) {
		if( strPath[pathLen-1] != ':' ) {
			strPath[pathLen] = '/';
			pathLen++;
		}
		BlockMove(&fileHdrEndPtr->FileName[1], &strPath[pathLen], len);
		strPath[pathLen+len] = '\0';
		DisplayReorgPath();
	}
	return(success);
}
	
/*
 * Checks OLD filehdr/exthdr data block keys for range of new keys of extent.  
 * If it finds an old key inside the new range, it swaps key into proper 
 * place in the list of data block keys, such that the old key now maps 
 * to itself.  (Got that?)  By definition, such a key is free, so no 
 * relocation will take place to that key (which would swap data INTO the 
 * area about to be written with extent data). This code gets VERY tricky 
 * if the file is wrapped.
 */
 
static void RangeCheckKeys(BufCtrlPtr buf, ReorgListPtr reorgListPtr)
{
	register ULONG baseKey;
	register ULONG key;
	register ULONG numBlocks;
	register ULONG *keyPtr;
	register ULONG *keyTablePtr;
	register LONG index;
	ULONG upperRange;
	ULONG baseKeyWrapped;
	ULONG blockCount;
	BOOL cont;
	
	baseKey = reorgListPtr->rl_NewDatakey;
	baseKeyWrapped = baseKey;
	numBlocks = blockCount = /*AdjustBlockCount(reorgListPtr);*/reorgListPtr->rl_HBlks;
	upperRange = blockCount + baseKey;
	
	keyTablePtr = &((FileHdrEndPtr) (((UBYTE *)buf) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(ULONG) - sizeof(FileHdrEnd)))->Reserved;
	
	do {
		keyPtr = keyTablePtr;
		while( numBlocks ) {
			if( (*keyPtr >= baseKey) && (*keyPtr < upperRange) ) {
				index = -(*keyPtr - baseKeyWrapped);
				if( (key = keyTablePtr[index]) != *keyPtr ) {
					keyTablePtr[index] = *keyPtr;
					*keyPtr = key;
					buf->DirtyFlag = TRUE;
					continue;					/* George: "DON'T TOUCH...not sure why...sigh"! */
				}
			}
			numBlocks--;
			keyPtr--;
		}
		if( cont = ((reorgListPtr->rl_Flags & RFLAG_WRAPPED_MASK) && (baseKey != currVolParams.LowestKey)) ) {
			numBlocks = currVolParams.MaxKeys;
			if( badBlockStats.BadBlockFlag ) {
				key = reorgStats.CurrKey;
				for( index = blockCount; index != 0; index-- ) {
					while( !GetBadStatus(reorgStats.CurrKey) ) {
						AdvanceKey();
						numBlocks--;
					}
					AdvanceKey();
				}
				reorgStats.CurrKey = key;
			}
			baseKey = currVolParams.LowestKey;
			upperRange -= numBlocks;
			baseKeyWrapped -= numBlocks;	/* Yep, this will make it negative! */
			numBlocks = blockCount;
		}
	} while( cont );
}

/*
 * Updates entry for key "oldPos" to show that it is now mapped to key "newPos".
 * These keys are kept in sorted order.
 */
 
static BOOL UpdateMapping(register ULONG oldPos, ULONG newPos)
{
	register BOOL success;
	register ULONG remapIndex;
	BufCtrlPtr buf;
	
	if( success = oldPos <= currVolParams.HighestKey ) {
		oldPos -= currVolParams.Envec.de_Reserved;
		if( reorgStats.MemKeysFlag ) {
			remapBuffer[oldPos] = newPos;
		} else {
			remapIndex = oldPos >> currVolParams.BlockLongShift;
			oldPos -= remapIndex << currVolParams.BlockLongShift;
			if( success = ReadSpecial(remapBuffer[remapIndex], &buf) == CERR_NOERR ) {
				((ULONG *)(buf+1))[oldPos] = newPos;
				buf->DirtyFlag = TRUE;
			}
		}
	}
	return(success);
}

/* 
 * Relocates contents of key at "oldPos" to "newPos" if old position busy in bitmap.
 */
 
BOOL RelocateBlock(register ULONG oldPos, register ULONG newPos)
{
	BOOL success = TRUE;
	BufCtrlPtr buf;
	
	if( oldPos != newPos ) {
		if( !GetKeyStatus(oldPos) ) {
			MarkAltBusy(oldPos);
			if( success = UpdateMapping(oldPos, newPos) ) {
				if( success = ReadData(oldPos, &buf) == CERR_NOERR ) {
					UnlinkCache(buf);
					buf->DirtyFlag = TRUE;
					LinkCacheKey(newPos, BLOCK_DATA, buf);
					MarkKeyBusy(newPos);
				}
			}
		}
	}
	return(success);
}

/*
 * Relocates contents of control block.
 * Should NOT be called if old position is same as new position, so check beforehand.
 */
 
static BOOL RelocateCtrlBlock(register ULONG oldPos, register ULONG newPos, BufCtrlPtr buf)
{
	BOOL success;

	buf->DirtyFlag = TRUE;
	MarkKeyFreeAndDraw(oldPos);
	UnlinkCache(buf);
	if( success = RelocateBlock(newPos, oldPos) ) {
		LinkCacheKey(newPos, BLOCK_CONTROL, buf);
		MarkKeyBusyAndDraw(newPos);
	}
	return(success);
}

/*
 * Checks NEW keys of extent to see if busy. If so, existing contents are relocated
 * to the OLD keys just vacated by reading the extent data into the extentBuffer.
 */
 
static BOOL RelocateExtent(BufCtrlPtr buf, ReorgListPtr reorgListPtr, ULONG baseKey)
{
	register BOOL success = TRUE;
	register UWORD numBlocks = reorgListPtr->rl_HBlks;	
	register ULONG *keyPtr = &((FileHdrEndPtr) (((UBYTE *)buf) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(FileHdrEnd)))->Reserved;
	ULONG key;
	
	reorgStats.CurrKey = baseKey;
	RangeCheckKeys(buf, reorgListPtr);
	while( numBlocks-- ) {
		key = *--keyPtr;
		if( RemapKey(&key) && RelocateBlock(reorgStats.CurrKey, key) ) {
			MarkKeyBusyAndDraw(reorgStats.CurrKey);
			AdvanceKey();
			CheckBadKey();
		} else {
			success = FALSE;
			break;
		}
	}
	return(success);
}

/*
 * Relocates file entry passed, returns TRUE if successful.
 */
 
static BOOL RelocateFile(register ReorgListPtr fileEntry)
{
	register ReorgListPtr reorgListPtr = fileEntry;
	register ULONG newKey = reorgListPtr->rl_Newkey;
	register BOOL success = TRUE;
	BufCtrlPtr buf;
	register BOOL movable = (reorgListPtr->rl_Flags & RFLAG_DONT_MOVE_MASK) == 0;

	do {
		if( reorgListPtr->rl_HBlks ) {
			if( movable ) {
				if( success = ReadCtrlBlk(reorgListPtr->rl_Newkey, &buf) ) {
					if( badBlockStats.BadBlockFlag || 
						( (reorgListPtr->rl_NewDatakey != *((ULONG *)(&((FileHdrEndPtr)(((UBYTE *)buf) + currVolParams.BlockSize + (sizeof(BufCtrl) - sizeof(ULONG)) - sizeof(FileHdrEnd)))->Reserved)) ) ||
						(reorgListPtr->rl_Flags & RFLAG_HDR_FRAG_MASK) ) ) {
						UnlinkCache(buf);
						if( success = ReadExtent(buf, reorgListPtr) ) {
							if( !currVolParams.FFSFlag ) {
								UpdateOFSBlocks(reorgListPtr, reorgListPtr->rl_NewDatakey, newKey);
							}
							if( success = RelocateExtent(buf, reorgListPtr, reorgListPtr->rl_NewDatakey) ) {
								if( reorgListPtr->rl_HBlks ) {
									success = WriteExtent(reorgListPtr->rl_NewDatakey, reorgListPtr->rl_HBlks);
								}
								LinkCache(buf);
							}
						}
					} else {
						AddBarGraph(reorgListPtr->rl_HBlks);
						RefreshTimeRemaining();
					}
				}
			} else {
				AddBarGraph(reorgListPtr->rl_HBlks);
				RefreshTimeRemaining();
			}
		}
		CheckMainPort();
		success &= !abortFlag;
	} while( success && ((reorgListPtr = reorgListPtr->rl_ExtHdr) != NULL) );
	if( success && movable ) {
		success = WriteCache() == 0;
	}
	++reorgStats.FinishedEntries;					/* For better time prediction */
/*
	Put back extension blocks - commented out 'cos it may slow us down.
*/
/*
	reorgListPtr = fileEntry;
	while( (reorgListPtr != NULL) && (buf = ScanCache(reorgListPtr->rl_Newkey)) ) {
		PutCacheBuffer(buf);
		reorgListPtr = reorgListPtr->rl_ExtHdr;
	}
*/
	return( success );
}

/*
 * Given the filehdr or exthdr block, and entry ptr, reads all
 * data blocks of an extent into the ExtentBuffer.  Data blocks may have been
 * remapped.

 * Fills extent buffer with data from filehdr/exthdr block and entry.
 * Reads remapped blocks, if necessary.  Frees keys.  Returns FALSE on error.
 * 
 * Note: uses ScanCache(), not ReadData(), to avoid having to move data from cache
 * buffer to extent buffer.  If in cache, just copy out, but if read is really
 * necessary, read it directly into the proper place (and save clobbering a
 * cache buffer).
 */

static BOOL ReadExtent(BufCtrlPtr fileHdrBuf, ReorgListPtr reorgListPtr)
{
	FileHdrEndPtr fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrBuf) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(FileHdrEnd));
	register UWORD numBlocks = reorgListPtr->rl_HBlks;
	register ULONG *keyPtr = &fileHdrEndPtr->Reserved;
	register UBYTE *blockBuf = extentBuffer;
	register BOOL success = TRUE;
	register BOOL blockDone;
	register BOOL waiting = FALSE;
	ULONG currKey;
	BufCtrlPtr buf;
	TextChar numBuff[16];
	
	while( (numBlocks--) && success ) {
		currKey = *--keyPtr;
		if( waiting ) {
			success = WaitBlock() == 0;
		}
		if( success && (success = RemapKey(&currKey)) ) {
			blockDone = FALSE;
			if( currKey != *keyPtr ) {
				*keyPtr = currKey;
				fileHdrBuf->DirtyFlag = TRUE;
				if( buf = ScanCache(*keyPtr) ) {
					PutCacheBuffer(buf);			/* Free cache buff- no accidental writes! */
					CopyMemQuick((ULONG *)(buf+1), (ULONG *)blockBuf, currVolParams.BlockSize);
					blockDone = TRUE;
				}
			}
			if( !blockDone ) {
				if( success ) {
					success = ReadBlock(*keyPtr, blockBuf, TRUE) == 0;
					waiting = TRUE;
				}
			}
			if( success ) {
				blockBuf += currVolParams.BlockSize;
				MarkKeyFreeAndDraw(*keyPtr);
			}
		}
	}
	if( success ) {
		RefreshFragmentationMap();
		if( waiting ) {
			success = WaitBlock() == 0;
		}
	}
	if( !success ) {
		OutputStatHexOrDecString(currKey, numBuff, options.PrefsOpts.DecimalLabel);
		dialogVarStr = numBuff;
		Error(ERR_REORG_DATA_READ);
	}
	return(success);
}

/*
 * Marks key busy in bitmap and sets bitmap image (no need to touch alt bitmap).
 */
 
static void MarkKeyBusyAndDraw(ULONG key)
{
	MarkKeyBusy(key);
	DrawKeyPixel(key, 0);
}

/*
 * Marks key free in both regular and alt bitmap, and sets bitmap image.
 */
 
static void MarkKeyFreeAndDraw(register ULONG key)
{
	MarkKeyFree(key);
	MarkAltFree(key);
	DrawKeyPixel(key, 1);
}

/*
 * Given starting key and block count, write as much as it can and then "wrap" to
 * lower area of volume. Note: this code ASSUMES that the extent is defragged!!
 * if BadBlocks known to exist, performs block-by-block write instead of multi-block.
 */
 
static BOOL WriteExtent(register ULONG startKey, ULONG blocksToWrite)
{
	register BOOL success = TRUE;
	register UBYTE *ptr = extentBuffer;
	register ULONG blockCount = blocksToWrite;
	TextChar numBuff[16];
	
	if( blockCount ) {
		if( badBlockStats.BadBlockFlag ) {
			reorgStats.CurrKey = startKey;
			while( blockCount-- ) {
				(void) WriteBlock(reorgStats.CurrKey, ptr, TRUE);
				BumpBarGraph();
				if( success = WaitBlock() == 0 ) {
					ptr += currVolParams.BlockSize;
					AdvanceKey();
					CheckBadKey();
				} else {
					OutputStatHexOrDecString(reorgStats.CurrKey, numBuff, options.PrefsOpts.DecimalLabel);
					dialogVarStr = numBuff;
					Error(ERR_WRITE_BLOCK);
					break;
				}
			}
			RefreshFragmentationMap();
			RefreshTimeRemaining();
		} else {
/*
	Check to make sure it will fit before hitting the high key. If it bumps into
	it then we'll break it up into two writes: one here as well as the one below.
*/
			if( (startKey + blockCount) > (currVolParams.HighestKey+1) ) {
				blockCount = (currVolParams.HighestKey - startKey)+1;
				if( success = WriteMulti(startKey, blockCount, ptr, FALSE) == 0 ) {
					ptr += blockCount << currVolParams.BlockSizeShift;
					startKey = currVolParams.LowestKey;
					blockCount = blocksToWrite - blockCount;
				}
			}
			if( success ) {
				(void) WriteMulti(startKey, blockCount, ptr, TRUE);
				AddBarGraph(blocksToWrite);
				RefreshFragmentationMap();
				RefreshTimeRemaining();
				success = WaitBlock() == 0;
			}
		}
	}
	return(success);
}

/*
 * Checks for possible remap of key passed inside of "newPos".
 * Returns TRUE if successful, and the new position in "newPos".
 */

BOOL RemapKey(ULONG *newPos)
{
	register BOOL success = TRUE;
	register BOOL done;
	register ULONG currKey = *newPos;
	register ULONG remapIndex;							/* Index into remap table */
	BufCtrlPtr buf;

	do {
		if( !(done = GetAltStatus(currKey)) ) {	/* Has key been remapped? */
			currKey -= currVolParams.Envec.de_Reserved;
			if( reorgStats.MemKeysFlag ) {			/* Yes, so can't return original */
				currKey = remapBuffer[currKey];
			} else {
				remapIndex = currKey >> currVolParams.BlockLongShift;
				currKey -= remapIndex << currVolParams.BlockLongShift;
				if( success = ReadSpecial(remapBuffer[remapIndex], &buf) == CERR_NOERR ) {
					currKey = ((ULONG *)(buf+1))[currKey];
					success = currKey >= currVolParams.Envec.de_Reserved;
				}
				if( done = !success ) {
					Error(ERR_BAD_REMAP);
				}
			}
		}
	} while( !done );
	*newPos = currKey;
	return(success);
}

/*
 * Local ReadCtrlBlock(), because this code ASSUMES that an ExtHdr is a valid
 * control block. Performs minimal checking, releases block if bad.
 */
 
static BOOL ReadCtrlBlk(ULONG key, BufCtrlPtr *buf)
{
	register BOOL success = FALSE;
	register WORD err;
	register ULONG *dataPtr;
	ULONG secType;
	TextChar numBuff[16];
	
	if( (err = ReadControl(key, buf)) == CERR_NOERR ) {
		dataPtr = (ULONG *)((*buf)+1);
		if( *dataPtr == T_LIST ) {
			success = ((RootBlockEndPtr)(((UBYTE *)dataPtr) + currVolParams.BlockSize - sizeof(RootBlockEnd)))->SecType == ST_FILE;
		} else if( *dataPtr & T_DIRCACHE ) {
			success = TRUE;
		} else {
			success = CheckCtrlTypes(dataPtr, &secType);
		}
		if( !success ) {
			err = CERR_BADTYPE;
		}
		if( !success ) {
			PutCacheBuffer(*buf);
		}
	}
	if( !success ) {
		switch(err) {
		case CERR_CHECKSUM:
			err = ERR_CTRL_CKSUM;
			break;
		case CERR_BADTYPE:
			err = ERR_CTRL_TYPE;
			break;
		case CERR_IOERR:
		default:
			OutputStatHexOrDecString(key, numBuff, options.PrefsOpts.DecimalLabel);
			dialogVarStr = numBuff;
			err = ERR_CTRL_READ;
			break;
		}
		Error(err);
	}
	return(success);
}
			
/*
 * Corrects data block keys and hash table keys for control blocks.
 */
 
static BOOL UpdateCtrlBlock(register ReorgListPtr reorgListPtr)
{
	register BOOL success = FALSE;
	register FileHdrPtr fileHdrPtr;
	FileHdrEndPtr fileHdrEndPtr;
	register ULONG currKey;
	register UBYTE entryType;
	BufCtrlPtr buf;
	
	currKey = reorgListPtr->rl_Newkey;
	reorgListPtr->rl_Ownkey = currKey;
	if( ReadCtrlBlk(currKey, &buf) ) {
		buf->DirtyFlag = TRUE;
		fileHdrPtr = (FileHdrPtr)(buf+1);

		if( currKey != currVolParams.RootBlock ) {
			fileHdrPtr->Ownkey = currKey;
		} else {
			if( currVolParams.DirCache ) {
				((RootBlockEndPtr)(((UBYTE *)buf) + currVolParams.BlockSize + sizeof(BufCtrl) - sizeof(RootBlockEnd)))->BMFlg = 0L;
			}
		}
/*
	Fix parent field setting.
*/		
		currKey = 0L;
		if( reorgListPtr->rl_Parent != NULL ) {
			currKey = ((ReorgListPtr)reorgListPtr->rl_Parent)->rl_Ownkey;
		}
		
		fileHdrEndPtr = (FileHdrEndPtr)(((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
		fileHdrEndPtr->Parent = currKey;
/*
	Fix hash chain setting.
*/	
		currKey = 0L;
		if( reorgListPtr->rl_Next != NULL ) {
			if( ((ReorgListPtr)reorgListPtr->rl_Next)->rl_HashEntry == reorgListPtr->rl_HashEntry ) {
				currKey = reorgListPtr->rl_Next->rl_Newkey;
			}
		}
		fileHdrEndPtr->HashChain = currKey;

		if( (entryType = reorgListPtr->rl_Type) & RTYPE_DIR_MASK ) {
			success = UpdateDirEntry(reorgListPtr, fileHdrPtr);
		} else if( entryType & RTYPE_FILE_HDR_MASK ) {
			success = UpdateFileHdr(reorgListPtr, fileHdrPtr);
		} else {
			success = (reorgListPtr->rl_Type & (RTYPE_HARD_LINK_MASK | RTYPE_SOFT_LINK_MASK) ) != 0;
		}
		if( success ) {
			WriteBlockChecksum(buf);
		}
		PutCacheBuffer(buf);
	}
	return(success);
}
			
/*
 * Corrects data block keys and hash table keys for control blocks.
 */
 
static BOOL UpdateCacheBlock(register ReorgListPtr reorgListPtr)
{
	register CacheHdrPtr cacheHdrPtr;
	register ULONG currKey;
	BufCtrlPtr buf;
	register BOOL success = FALSE;
	
	currKey = reorgListPtr->rl_Newkey;
	reorgListPtr->rl_Ownkey = currKey;
	if( ReadCtrlBlk(currKey, &buf) ) {
		buf->DirtyFlag = TRUE;
		cacheHdrPtr = (CacheHdrPtr)(buf+1);
		cacheHdrPtr->Ownkey = currKey;
		
		currKey = 0L;
		if( reorgListPtr->rl_Parent != NULL ) {
			currKey = ((ReorgListPtr)reorgListPtr->rl_Parent)->rl_Ownkey;
		}
		cacheHdrPtr->Parent = currKey;
		cacheHdrPtr->Type = T_DIRCACHE;	/* Zero vers causes update */

		reorgStats.CurrKey = 0L;
		if( reorgListPtr->rl_Child ) {
			reorgStats.CurrKey = reorgListPtr->rl_Newkey;
			AdvanceKey();
			CheckBadKey();
		}
		cacheHdrPtr->NextBlock = reorgStats.CurrKey;
		success = WriteBlockChecksum(buf) == 0;
	}
	return(success);
}

/*
 * Updates hash chain entries for directory.
 */
 
static BOOL UpdateDirEntry(ReorgListPtr reorgEntry, FileHdrPtr fileHdrPtr)
{
	register ReorgListPtr reorgListPtr = reorgEntry->rl_Child;
	register WORD saveEntry = -1;							/* Cause initial mismatch */
	register WORD entry;
	register UBYTE *hashTable = (UBYTE *) (fileHdrPtr+1);
	register BOOL success = TRUE;
	
	while( (reorgListPtr != NULL) && ((reorgListPtr->rl_Flags & RFLAG_WAIT_MASK) == 0) ) {
		entry = reorgListPtr->rl_HashEntry;
		if( entry != saveEntry ) {
			*((ULONG *)(&hashTable[entry])) = reorgListPtr->rl_Newkey;
			saveEntry = entry;
		}
		reorgListPtr = reorgListPtr->rl_Next;
	}
	if( reorgListPtr = (ReorgListPtr) reorgEntry->rl_NewDatakey ) {
		((DirBlockEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(DirBlockEnd)))->Ext = reorgListPtr->rl_Newkey;
		while( (reorgListPtr != NULL) && success ) {
			success = UpdateCacheBlock(reorgListPtr);
			reorgListPtr = reorgListPtr->rl_Next;
		}
	}
	return(success);
}

/*
 * Updates data keys for filehdr/exthdr.
 */
 
static BOOL UpdateFileHdr(register ReorgListPtr reorgListPtr, register FileHdrPtr fileHdrPtr)
{
	register BOOL success = TRUE;
	FileHdrEndPtr fileHdrEndPtr;
	ULONG fileHdrKey;
	register UWORD numBlocks;
	register ULONG *keyPtr;
	BufCtrlPtr buf;
	register BOOL movable = (reorgListPtr->rl_Flags & RFLAG_DONT_MOVE_MASK) == 0;

	fileHdrKey = reorgListPtr->rl_Ownkey;

	do {
		reorgStats.CurrKey = reorgListPtr->rl_NewDatakey;
		if( reorgStats.CurrKey && ((reorgListPtr->rl_Type & RTYPE_HDR_EXT_MASK) == 0) ) {
			fileHdrPtr->FirstBlk = reorgListPtr->rl_NewDatakey;
		}
		fileHdrEndPtr = (FileHdrEndPtr) (((UBYTE *)fileHdrPtr) + currVolParams.BlockSize - sizeof(FileHdrEnd));
		
		if( movable ) {
			numBlocks = reorgListPtr->rl_HBlks;
			keyPtr = &fileHdrEndPtr->Reserved;
		
			while( numBlocks-- ) {
				*--keyPtr = reorgStats.CurrKey;
				AdvanceKey();
				CheckBadKey();
			}
		}
		if( (reorgListPtr->rl_Type & RTYPE_FILE_HDR_MASK) == 0 ) {
			fileHdrEndPtr->Parent = fileHdrKey;
		}
		if( (reorgListPtr = reorgListPtr->rl_ExtHdr) != NULL ) {
			if( success = ReadCtrlBlk(fileHdrEndPtr->Ext = (reorgListPtr->rl_Ownkey = reorgListPtr->rl_Newkey), &buf) ) {
				buf->DirtyFlag = TRUE;
				fileHdrPtr = (FileHdrPtr)(buf+1);
				fileHdrPtr->Ownkey = reorgListPtr->rl_Newkey;
			}
		}
	} while( success && (reorgListPtr != NULL) );
	return(success);
}

/*	
 * This routine updates the od_Hdrkey and od_NextData fields and recalculates the 
 * checksum for data blocks for the OFS.  These fields do not exist in the new FS.
 * Called with ptr to filehdr/exthdr and filehdr key, and its new position.
 */
 
static void UpdateOFSBlocks(ReorgListPtr reorgListPtr, ULONG fileHdrKey, register ULONG newPos)
{
	register UWORD numBlocks = reorgListPtr->rl_HBlks;
	register OldDataBlkPtr oldDataBlkPtr = (OldDataBlkPtr) extentBuffer;
	
	reorgStats.CurrKey = fileHdrKey;
		
	while( numBlocks-- ) {
		oldDataBlkPtr->Hdrkey = newPos;

		AdvanceKey();
		CheckBadKey();

		if( oldDataBlkPtr->NextData != 0L ) {
			oldDataBlkPtr->NextData = reorgStats.CurrKey;
		}
		oldDataBlkPtr->CkSum -= CalcBlockChecksum((ULONG *)oldDataBlkPtr);
		oldDataBlkPtr = (OldDataBlkPtr) (((UBYTE *)oldDataBlkPtr) + currVolParams.BlockSize);
	}
}
