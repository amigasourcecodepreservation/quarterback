/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Quarterback Tools
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 * Vertical scroll operations (not handled by SLGadgetMessage()).
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Gadget.h>

#include "Tools.h"
#include "Proto.h"

/*
 *	Local prototypes
 */

static WORD CalcListHeight(void);
static void AdjustVScrollBar(BOOL);
static void AdjustToVertScroll(BOOL);
static void TrackVSlider(WindowPtr, WORD);

/*
 *	External variables
 */

extern LONG				yoffset;
extern VolParams 		currVolParams;
extern WindowPtr		cmdWindow;
extern BOOL				altKey;
extern GadgetPtr		currGadgList;
extern EditStats		editStats;

/*
 *	Local variables
 */

static GadgetPtr		trackVProp;

/*
 *	Returns the width in lines of the tallest item in the scroll list.
 * Usually 32 lines (in a 512 byte block size).
 */

WORD CalcMaxHeight()
{
/*
	Divide by 16 per line, multiply by charHeight of topaz/8.
*/
	return(currVolParams.BlockSize >> 4);
}

/*
 * Calculate the scroll list domain.
 */
 
static WORD CalcListHeight()
{
	Rectangle rect;
	
	GetGadgetRect(GadgetItem(currGadgList, EDIT_SCROLL_LIST), cmdWindow, NULL, &rect);
	return( (cmdWindow->Height - cmdWindow->BorderBottom - rect.MinY) >> 3 );
}

/*
 *	Scroll up
 *	Called by TrackGadget()
 */

void ScrollUp(WindowPtr window, WORD gadgNum)
{
	if( yoffset ) {
		CursorOff();
		yoffset--;
		AdjustVScrollBar(TRUE);
		CursorOn();
	}
}

/*
 * Scroll down
 *	Called by TrackGadget()
 */

void ScrollDown(WindowPtr window, WORD gadgNum)
{
	register WORD height = CalcMaxHeight() - CalcListHeight();
	
	if( height - yoffset ) {
		CursorOff();
		yoffset++;
		AdjustVScrollBar(TRUE);
		CursorOn();
	}
}

/*
 *	Track vertical slider
 *	Called by TrackGadget()
 */

static void TrackVSlider(WindowPtr window, WORD gadgNum)
{
	AdjustToVertScroll(/*altKey, */FALSE);
	
}

/*
 *	Adjust vertical scroll bar setting
 */

static void AdjustVScrollBar(BOOL refreshBlock)
{
	ULONG totAmount = CalcMaxHeight();
	UWORD listHeight = CalcListHeight();
	PropInfoPtr propInfo;
	register ULONG newPot;
	register ULONG newBody;
	
	if( !(trackVProp->Flags & SELECTED) ) { 
		if( totAmount < listHeight + yoffset ) {
			totAmount = listHeight + yoffset;
		}
		if( totAmount == listHeight ) {
			newPot = 0xFFFF;
		} else {
			newPot = (0xFFFFL*yoffset)/(totAmount-listHeight);
		}
		if( !(newBody = (0xFFFFL*listHeight)/((UWORD)totAmount)) ) {
			newBody++;
		}
		propInfo = (PropInfoPtr) trackVProp->SpecialInfo;
		if( (propInfo->VertPot != newPot) || (propInfo->VertBody != newBody) ) {
			NewModifyProp(trackVProp, cmdWindow, NULL, propInfo->Flags, 0, newPot, 0xFFFF, newBody, 1);
			if( refreshBlock ) {
				RefreshEditBlock(TRUE);
			}
		}
	}
}

/*
 *	Adjust offset to current scroll bar setting
 */

static void AdjustToVertScroll(/*BOOL updateBlock, */BOOL alwaysUpdate)
{
	register LONG totAmount = CalcMaxHeight();
	register WORD listHeight = CalcListHeight();
	PropInfoPtr propInfo = (PropInfoPtr) trackVProp->SpecialInfo;
	register LONG currPos = yoffset;
	register ULONG offset;
	
	if( totAmount < listHeight + currPos ) {
		totAmount = listHeight + currPos;
	}
	offset = propInfo->VertPot*(totAmount-listHeight);
	offset = (offset + 0x7FFFL)/0xFFFFL;

	if( (currPos != offset) || alwaysUpdate ) {
		/*alwaysUpdate = FALSE;*/		/* Subsequent retries must earn it! */
		offset -= currPos;
		CursorOff();
		yoffset += offset;
		RefreshEditBlock(TRUE);
		CursorOn();
	}
}

/*
 *	Handle gadget down/up on horizontal scroll list gadgets
 *	This routine will reply to the message
 *	firstGadget - the left arrow of the HScroll bar
 *		Gadgets are assumed to be in position of LEFT_ARROW, RIGHT_ARROW, SCROLL_BAR
 */

void HandleVertScroll(MsgPortPtr msgPort, IntuiMsgPtr intuiMsg, GadgetPtr firstGadget)
{
	register GadgetPtr gadget = (GadgetPtr) intuiMsg->IAddress;
	WindowPtr window = intuiMsg->IDCMPWindow;
	ULONG class = intuiMsg->Class;
	
	altKey = ((intuiMsg->Qualifier & ALTKEYS) != 0);

	ReplyMsg((MsgPtr) intuiMsg);
	if( window == cmdWindow ) {
		switch (class) {
		case GADGETDOWN:
			trackVProp = firstGadget->NextGadget->NextGadget;
			if( gadget == firstGadget )
				TrackGadget(msgPort, window, gadget, ScrollUp);
			else if( gadget == firstGadget->NextGadget)
				TrackGadget(msgPort, window, gadget, ScrollDown);
			else if( gadget == trackVProp )
				TrackGadget(msgPort, window, gadget, TrackVSlider);
			break;
		case GADGETUP:
			if( gadget == trackVProp ) {
				AdjustToVertScroll(/*TRUE,*/ TRUE);
			} else {
				AdjustVScrollBar(TRUE);
			}
			break;
		}
	}
}

/*
 *	Make the HScroll bar represent the scroll list
 *	firstGadget - the up arrow
 * ASSUMES PROP IS THE SECOND GADGET AFTER THE UP ARROW!
 */

void UpdateVScrollBar(GadgetPtr firstGadget, BOOL doUpdate)
{
	trackVProp = firstGadget->NextGadget->NextGadget;
	AdjustVScrollBar(doUpdate);
}
