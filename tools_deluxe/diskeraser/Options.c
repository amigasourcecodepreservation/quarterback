/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Wipe Disk
 *
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Options routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Request.h>

#include "WipeDisk.h"
#include "Proto.h"

/*
 *  External variables
 */

extern WindowPtr	window;

extern MsgPortPtr mainMsgPort;

extern ReqTemplPtr reqList[];

extern Options	options;

/*
 	Local variables and definitions
 */

static RequestPtr	optionsReq;

#define SHIFTKEYS        (IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)

enum {
	VALUEUP_ARROW = 2,
	VALUEDOWN_ARROW,
	VALUE_TEXT,
	REPEATUP_ARROW,
	REPEATDOWN_ARROW,
	REPEAT_TEXT,
	STD_RADBTN,
	GOVT_RADBTN
};

#define MIN_WIPEVALUE 	0x00
#define MIN_REPEATCOUNT	1

#define MAX_WIPEVALUE 	0xFF
#define MAX_REPEATCOUNT	100

/*
	local prototypes
 */

static void ByteToHexString(UBYTE, TextPtr);
static BOOL MatchOptions(TextPtr, TextPtr, TextPtr);
static void ChangeValue(RequestPtr, UBYTE *, WORD);
static void ChangeCount(RequestPtr, WORD *, WORD);
static void	SetOptionsDisplay(RequestPtr, Options *);

/*
 *	Hex character array.
 */

static UBYTE hexChars[] = {
	'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'
};

/*
 * Convert an unsigned longword into a hex string eight(+1) characters long.
 * Terminate with a space, but no null-terminator needed.
 */

static void ByteToHexString(register UBYTE val, register TextPtr buff)
{
	buff[0] = '$';
	buff[2] = hexChars[val & 0xF];
	val >>=	4;
	buff[1] = hexChars[val & 0xF];
	buff[3] = '\0';
}

/*
 *	Match screen options
 */

static BOOL MatchOptions(register TextPtr text, TextPtr opt1, TextPtr opt2)
{
	register WORD	len;

/*
	Get command length
*/
	for (len = 0; text[len] && text[len] != ':' && text[len] != '='; len++) ;
	if (text[len])
		len++;
/*
	Check for match
*/
	return ((BOOL) (CmpString(text, opt1, len, strlen(opt1), FALSE) == 0 ||
					CmpString(text, opt2, len, strlen(opt2), FALSE) == 0));
}


/*
 *	Set appropriate option from string
 */

void SetOption(TextPtr text)
{
	register TextPtr	param;
	LONG				num;

/*
	Get pointer to command param (if any)
*/
	for (param = text; *param && *param != ':' && *param != '='; param++) ;
	if (*param)
		param++;

	if (MatchOptions(text, "value:", "v:") ||
		MatchOptions(text, "value=", "v=") ) {
		num = StringToNum(param);
		if (num < 0 || num > 256)
			return;
		options.StdWipeValue = options.GovtWipeValue = num;
		return;
	}
	if (MatchOptions(text, "repeat:", "r:") ||
		MatchOptions(text, "repeat=", "r=")) {
		num = StringToNum(param);
		if (num <= 0 || num > 100)
			return;
		options.StdRepeatCount = options.GovtRepeatCount = num;
		return;
	}
	if (MatchOptions(text, "government", "g")) {
		options.GovtWipe = TRUE;
		return;
	}
}

/*
 *	Init option structure
 */

void InitOptions()
{
	options.StdWipeValue	= 0x00;
	options.GovtWipeValue	= 0xF6;
	options.StdRepeatCount	= 1;
	options.GovtRepeatCount	= 3;
	options.GovtWipe		= FALSE;
}

/*
 *	Change wipe value in options requester
 */

static void ChangeValue(RequestPtr req, UBYTE *value, WORD inc)
{
	GadgetPtr	gadgList = req->ReqGadget;
	TextChar	strBuff[5];

	*value += inc;
	if (*value >= MAX_WIPEVALUE)
		*value = MAX_WIPEVALUE;
	if (*value <= MIN_WIPEVALUE)
		*value = MIN_WIPEVALUE;
	ByteToHexString(*value, strBuff);
	SetGadgetItemText(gadgList, VALUE_TEXT, window, req, strBuff);
/*
	Do enable/disable after displaying new value, since these will be blocked
		if gadgets are selected
*/
	EnableGadgetItem(gadgList, VALUEUP_ARROW, window, req, *value < MAX_WIPEVALUE);
	EnableGadgetItem(gadgList, VALUEDOWN_ARROW, window, req, *value > MIN_WIPEVALUE);
}

/*
 *	Change repeat count in options requester
 */

static void ChangeCount(RequestPtr req, WORD *count, WORD inc)
{
	GadgetPtr	gadgList = req->ReqGadget;
	TextChar	strBuff[4];

	*count += inc;
	if (*count >= MAX_REPEATCOUNT)
		*count = MAX_REPEATCOUNT;
	if (*count <= MIN_REPEATCOUNT)
		*count = MIN_REPEATCOUNT;
	NumToString(*count, strBuff);
	SetGadgetItemText(gadgList, REPEAT_TEXT, window, req, strBuff);
/*
	Do enable/disable after displaying new value, since these will be blocked
		if gadgets are selected
*/
	EnableGadgetItem(gadgList, REPEATUP_ARROW, window, req, *count < MAX_REPEATCOUNT);
	EnableGadgetItem(gadgList, REPEATDOWN_ARROW, window, req, *count > MIN_REPEATCOUNT);
}


/*
 *	Set all items in options requester to correct values
 */

static void SetOptionsDisplay(RequestPtr req, Options *opts)
{
	UBYTE		*wipeValuePtr;
	WORD		*repeatCountPtr;
	GadgetPtr	gadgList = req->ReqGadget;
	TextChar	strBuff[5];

	SetGadgetItemValue(gadgList, STD_RADBTN, window, req, !opts->GovtWipe);
	SetGadgetItemValue(gadgList, GOVT_RADBTN, window, req, opts->GovtWipe);

	wipeValuePtr = (opts->GovtWipe) ? &opts->GovtWipeValue : &opts->StdWipeValue;
	ByteToHexString(*wipeValuePtr, strBuff);
	SetGadgetItemText(gadgList, VALUE_TEXT, window, req, strBuff);

	repeatCountPtr = (opts->GovtWipe) ? &opts->GovtRepeatCount : &opts->StdRepeatCount;
	NumToString(*repeatCountPtr, strBuff);
	SetGadgetItemText(gadgList, REPEAT_TEXT, window, req, strBuff);

	EnableGadgetItem(gadgList, VALUEDOWN_ARROW, window, req, *wipeValuePtr > MIN_WIPEVALUE);
	EnableGadgetItem(gadgList, VALUEUP_ARROW, window, req, *wipeValuePtr < MAX_WIPEVALUE);
	EnableGadgetItem(gadgList, REPEATDOWN_ARROW, window, req, *repeatCountPtr > MIN_REPEATCOUNT);
	EnableGadgetItem(gadgList, REPEATUP_ARROW, window, req, *repeatCountPtr < MAX_REPEATCOUNT);
}

/*
 *	Options dialog filter
 */

static BOOL OptionDialogFilter(register IntuiMsgPtr intuiMsg, register WORD *item)
{
	register ULONG	class = intuiMsg->Class;
	register UWORD	code = intuiMsg->Code;
	UWORD			modifier = intuiMsg->Qualifier;
	register WORD	gadgNum;
	GadgetPtr		gadgList = optionsReq->ReqGadget;

	if (intuiMsg->IDCMPWindow == window) {
		switch (class) {
		case GADGETDOWN:
		case GADGETUP:
			gadgNum = GadgetNumber(intuiMsg->IAddress);
			if (gadgNum == VALUEUP_ARROW || gadgNum == VALUEDOWN_ARROW ||
				gadgNum == REPEATUP_ARROW || gadgNum == REPEATDOWN_ARROW) {
				ReplyMsg((MsgPtr) intuiMsg);
				*item = (class == GADGETDOWN) ? gadgNum : -1;
				return (TRUE);
			}
			break;
		case RAWKEY:
			if (code == CURSORUP || code == CURSORDOWN) {
				ReplyMsg((MsgPtr) intuiMsg);
				if (modifier & SHIFTKEYS)
					gadgNum = (code == CURSORUP) ? REPEATUP_ARROW : REPEATDOWN_ARROW;
				else
					gadgNum = (code == CURSORUP) ? VALUEUP_ARROW : VALUEDOWN_ARROW;
				if (DepressGadget(GadgetItem(gadgList, gadgNum), window, optionsReq))
					*item = gadgNum;
				return (TRUE);
			}
			break;
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Display and handle options requester
 */

void DoOptions()
{
	GadgetPtr	gadgList;
	BOOL		done;
	WORD		item, delay, inc;
	UBYTE		*wipeValuePtr;
	WORD		*repeatCountPtr;
	Options		opts;

	opts = options;			// Save local copy to work on
/*
	Bring up request
*/
	if ((optionsReq = GetRequest(reqList[REQ_OPTIONS], window, TRUE)) == NULL) {
		Error(ERR_NO_MEM);
		return;
	}
	gadgList = optionsReq->ReqGadget;
	OutlineButton(GadgetItem(gadgList, OK_BUTTON), window, optionsReq, TRUE);
	SetOptionsDisplay(optionsReq, &opts);
/*
	Handle requester
*/
	delay = 0;
	done = FALSE;
	do {
		WaitPort(mainMsgPort);
		item = CheckRequest(mainMsgPort, window, OptionDialogFilter);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case GOVT_RADBTN:
		case STD_RADBTN:
			opts.GovtWipe = (item == GOVT_RADBTN);
			SetOptionsDisplay(optionsReq, &opts);
			break;
/*
	Handle INTUITICKS messages
*/
		case -1:
			if (delay > 0)
				delay--;
			else {
				if (delay > -9) {
					delay--;
					inc = 1;
				}
				else
					inc = 5;
				wipeValuePtr = (opts.GovtWipe) ? &opts.GovtWipeValue : &opts.StdWipeValue;
				repeatCountPtr = (opts.GovtWipe) ? &opts.GovtRepeatCount : &opts.StdRepeatCount;
				if (GadgetSelected(gadgList, VALUEUP_ARROW))
					ChangeValue(optionsReq, wipeValuePtr, inc);
				else if (GadgetSelected(gadgList, VALUEDOWN_ARROW))
					ChangeValue(optionsReq, wipeValuePtr, (WORD) -inc);
				else if (GadgetSelected(gadgList, REPEATUP_ARROW))
					ChangeCount(optionsReq, repeatCountPtr, inc);
				else if (GadgetSelected(gadgList, REPEATDOWN_ARROW))
					ChangeCount(optionsReq, repeatCountPtr, (WORD) -inc);
			}
			break;
/*
	Handle value up and down arrows
*/
		case VALUEUP_ARROW:
		case VALUEDOWN_ARROW:
			wipeValuePtr = (opts.GovtWipe) ? &opts.GovtWipeValue : &opts.StdWipeValue;
			ChangeValue(optionsReq, wipeValuePtr, (item == VALUEUP_ARROW) ? +1 : -1);
			delay = 5;
			break;
/*
	Handle repeat count up and down arrows
*/
		case REPEATUP_ARROW:
		case REPEATDOWN_ARROW:
			repeatCountPtr = (opts.GovtWipe) ? &opts.GovtRepeatCount : &opts.StdRepeatCount;
			ChangeCount(optionsReq, repeatCountPtr, (item == REPEATUP_ARROW) ? +1 : -1);
			delay = 5;
			break;
		}
	} while (!done);

	EndRequest(optionsReq, window);
	DisposeRequest(optionsReq);
	if (item == CANCEL_BUTTON)
		return;
/*
	Save new values
*/
	options = opts;
}
