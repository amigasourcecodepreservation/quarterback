/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Wipe Disk
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Main procedure
 */

#include <proto/exec.h>
#include <proto/intuition.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>

#include "WipeDisk.h"
#include "Proto.h"

/*
	external variables
 */

extern WindowPtr 		window;
extern GadgetPtr		gadgList;
extern MsgPortPtr		mainMsgPort;
extern BOOL 			titleChanged;
extern ScrollListPtr	diskScrollList;

/*
 *	Local variables and definitions
 */

static BOOL 	quitFlag;
static BOOL		entire;

#define RAWKEY_HELP	0x5F

/*
 *	Prototypes
 */

static void DoIntuiMessage(IntuiMsgPtr);

/*
 *	Main routine
 */

void main(int argc,char *argv[])
{
	register IntuiMsgPtr	intuiMsg;

	Init(argc, argv);
	entire = FALSE;
	SetButtons(entire);

	quitFlag = FALSE;
	while (!quitFlag) {
		while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort))
			DoIntuiMessage(intuiMsg);
		if (!quitFlag)
			Wait(1L << mainMsgPort->mp_SigBit);
	}

	ShutDown();
}

static void DoIntuiMessage(register IntuiMsgPtr intuiMsg)
{
	register ULONG	class;
	register UWORD	code;
	register APTR	iAddress;
	register UWORD	modifier;
	WORD			gadgNum, itemHit, diskNum = -1;

	class		= intuiMsg->Class;
	code		= intuiMsg->Code;
	iAddress	= intuiMsg->IAddress;
	modifier	= intuiMsg->Qualifier;
/*
	Process the message
*/
	if ((class == GADGETDOWN || class == GADGETUP) &&
		((gadgNum = GadgetNumber(iAddress)) == DISK_BOX || gadgNum == DISK_UP ||
		 gadgNum == DISK_DOWN || gadgNum == DISK_SCROLL)) {
		SLGadgetMessage(diskScrollList, mainMsgPort, intuiMsg);
		diskNum = SLNextSelect(diskScrollList, -1);
		SetButtons(entire);
		return;
	}
	else if (DialogSelect(intuiMsg, &window, &itemHit)) {
		switch (itemHit) {
		case DLG_CLOSE_BOX:
			quitFlag = TRUE;
			break;
		case WIPE_BUTTON:
			if ((diskNum = SLNextSelect(diskScrollList, -1)) == -1) {
				SysBeep(5);
				SetButtons(entire);
				break;
			}
			DoWipeDisk(diskNum, entire);
			break;
		case OPTIONS_BUTTON:
			DoOptions();
			break;
		case UNUSED_OPT:
		case ENTIRE_OPT:
			entire = (itemHit == ENTIRE_OPT);
			SetButtons(entire);
			break;
		}
	}
	else {
		switch (class) {
		case INTUITICKS:
			if (!DiskListChanged())
				break;					// Else fall through
		case DISKINSERTED:
		case DISKREMOVED:
			(void) GetDiskList();
			SetButtons(entire);			// To disable WIPE button
			break;
		case RAWKEY:
			if ((code == CURSORUP || code == CURSORDOWN) &&
				((modifier & IEQUALIFIER_REPEAT) == 0)) {
				SLCursorKey(diskScrollList, code);
				SetButtons(entire);
			}
			else if (code == RAWKEY_HELP)
				DoHelp();
			break;
		case REFRESHWINDOW:
			RefreshWindow();
			break;
		}
	}
	ReplyMsg((MsgPtr) intuiMsg);
}
