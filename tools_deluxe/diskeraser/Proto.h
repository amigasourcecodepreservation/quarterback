/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *  Wipe
 *
 *  Global function Prototypes
 */

/*
 *  Main.c
 */

void 	main(int,char **);

/*
 *  Init.c
 */

void 	Init(int, char **);
void 	ShutDown(void);

/*
 *  Window.c
 */

void 	SetButtons(BOOL);
void 	CreateWindow(void);
void	RemoveWindow(void);
void 	FixTitle(void);
void	Error(WORD);
void	DoHelp(void);
BOOL 	ConfirmWipe(TextPtr, BOOL);

BOOL	GetStatusRequest(TextPtr, ULONG);
void	SetStatusPassNum(WORD, WORD);
void	SetStatusBarGraph(ULONG);
void	DisposeStatusReq(void);
void	StatusEnableAbort(BOOL);
BOOL	StatusCheckAbort(void);

void	RefreshWindow(void);
BOOL	DialogFilter(IntuiMsgPtr, WORD *);

/*
 * WipeDisk.c
 */

BOOL 	GetDiskList(void);
void	DisposeDiskList(void);
BOOL	DiskListChanged(void);

void	DoWipeDisk(WORD,BOOL);

/*
 *	Options.c
 */

void	SetOption(TextPtr);
void	InitOptions(void);
void	DoOptions(void);