/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Wipe File/Disk Definitions
 */

/*
 *	Dialog and requester definitions
 */

enum {
	DLG_MAIN, 	DLG_ERROR,	DLG_ABOUT,
};

enum {
	REQ_WIPESTAT,	REQ_OPTIONS,	REQ_CONFIRMENTIRE,	REQ_CONFIRMUNUSED
};

/*
 *	Error definitions
 */

enum {
	INIT_ERR_WINDOW, 	INIT_ERR_PORT
};

enum {
	ERR_NO_MEM,		ERR_CANT_WIPE,	ERR_NO_INFO,	ERR_WIPE_FAIL,
	ERR_NO_FORMAT,	ERR_UNKNOWN
};

/*
 *	Window definitions
 */

enum {
	WIPE_BUTTON = 0,
	OPTIONS_BUTTON,
	DISK_BOX,
	DISK_UP,
	DISK_DOWN,
	DISK_SCROLL,
	ENTIRE_OPT,
	UNUSED_OPT
};

#define MAX_FILENAME_LEN 512

/*
 *	Macros
 */

#define MAX(a,b)	((a)>(b)?(a):(b))
#define MIN(a,b)	((a)<(b)?(a):(b))

/*
 *	Options
 */

typedef struct {
	BOOL	GovtWipe;
	UBYTE	StdWipeValue, GovtWipeValue;
	WORD	StdRepeatCount, GovtRepeatCount;
} Options;
