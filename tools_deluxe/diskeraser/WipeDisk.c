/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Wipe Disk
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Wipe Disk procedure
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <dos/dos.h>
#include <dos/dosextens.h>
#include <dos/filehandler.h>		// For fssm_ fields

#include <libraries/dos.h>
#include <devices/trackdisk.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/intuition.h>

#include <clib/alib_protos.h>

#include <Typedefs.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Utility.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Request.h>
#include <Toolbox/DOS.h>
#include <Toolbox/Packet.h>

#include <string.h>

#include "WipeDisk.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct DosLibrary	*DOSBase;

extern WindowPtr		window;
extern MsgPortPtr		mainMsgPort;

extern  ScrollListPtr	diskScrollList;

extern Options options;

/*
 *	Local variables and definitions
 */

#define ID_DOS0		(0x444F5300L)	// 'DOS\0'
#define ID_DOS1		(0x444F5301L)
#define ID_DOS2		(0x444F5302L)
#define ID_DOS3		(0x444F5303L)
#define ID_DOS4		(0x444F5304L)
#define ID_DOS5		(0x444F5305L)

#define MAX_CONSEC	50

#define WIPE_SUCCESS	1			// Return values from WipeUnused() and WipeEntire()
#define WIPE_FAIL		0
#define WIPE_ABORT		-1

typedef struct DosEnvec				DosEnvec, *DosEnvPtr;
typedef struct FileSysStartupMsg	FileSysStartupMsg, *FSSMsgPtr;
typedef struct IOExtTD				IOExtTD, *IOExtTDPtr;
typedef struct InfoData				InfoData, *InfoDataPtr;
typedef struct DevInfo				DevInfo, *DevInfoPtr;

static ListHeadPtr	deviceList;
static ListHeadPtr	driverList;
static BOOL			isTD;

static UBYTE	*bitmapBlock;		// Bitmap data
static ULONG	bitmapBlockNum;
static BOOL		bitmapBlockValid;	// Inited to FALSE

static UBYTE	*listBlock;		// List of bitmap blocks
static WORD		listIndex = -1;	// List block number (0 == root, 1 == first ext., etc.)

static DosEnvec	dosEnv;			// DOS environment for volume being wiped

/*
 *	Local prototypes
 */

static void 	ConvertBSTR(BSTR, TextPtr);
static BSTR		MakeBSTR(TextPtr);
static void		FreeBSTR(BSTR);

static void		ClearDiskList(void);

static BOOL		GetDosEnv(IOExtTDPtr, TextPtr, LONG, LONG);

static ULONG	BlockSize(DosEnvPtr);
static ULONG	TotalBlocks(DosEnvPtr);
static ULONG	VolumeOffset(DosEnvPtr);
static ULONG	RootLocation(DosEnvPtr);

static void 	SetDiskBusy(MsgPortPtr, BOOL);
static void 	MotorOff(IOExtTDPtr);

static BOOL		ReadBlock(IOExtTDPtr, UBYTE *, ULONG);
static BOOL		WriteBlocks(IOExtTDPtr, UBYTE *, ULONG, WORD);

static BOOL		AllocBitmapBuffers(void);
static void		FreeBitmapBuffers(void);
static ULONG	ReadBitmap(IOExtTDPtr, ULONG, WORD);

static UBYTE	*AllocBuffer(WORD *, UBYTE);
static void		FreeBuffer(UBYTE *, WORD);
static WORD		WipeEntire(IOExtTDPtr, WORD);
static WORD 	WipeUnused(IOExtTDPtr, WORD);

static BOOL		DoFormat(MsgPortPtr, TextPtr, LONG);

/*
 *	Convert B-string to C-string
 */

static void ConvertBSTR(BSTR bStr, TextPtr cStr)
{
	register TextPtr	str;
	register UWORD		len;

	str = (TextPtr) BADDR(bStr);
	len = str[0];
	BlockMove(str + 1, cStr, len);
	cStr[len] = '\0';
}

/*
 *	Make a B-string from a C-string
 */

static BSTR MakeBSTR(TextPtr cStr)
{
	TextPtr	str;
	WORD	len;

	len = strlen(cStr);
	if ((str = MemAlloc(len + 2, 0)) == NULL) {
		Error(ERR_NO_MEM);
		return (NULL);
	}
	str[0] = (TextChar) len;
	strcpy(str + 1, cStr);
	return (MKBADDR(str));
}

/*
 *	Dispose of B-string created by MakeBSTR()
 */

static void FreeBSTR(BSTR bStr)
{
	TextPtr	str;

	if (bStr) {
		str = BADDR(bStr);
		MemFree(str, str[0] + 2);
	}
}

/*
 *	Get the volume name for a given device
 *	Return TRUE if mounted volume is found
 *	Note: Can't do an Examine() because that returns success even if it doesn't put
 *		  the volume name into the FileInfoBlock (which happens with corrupted root)
 */

static BOOL GetVolumeName(TextPtr devName, TextPtr volName)
{
	BOOL				success;
	LONG				args[1];
	InfoDataPtr			infoData;
	struct DeviceNode	*devNode;

	if ((infoData = MemAlloc(sizeof(InfoData), MEMF_CLEAR)) == NULL)
		return (FALSE);

	success = FALSE;
	volName[0] = '\0';
	args[0] = MKBADDR(infoData);
	(void) LockDosList1(LDF_DEVICES | LDF_READ);
	if (DoPacket(DeviceProc(devName), ACTION_DISK_INFO, args, 1) &&
		(devNode = (struct DeviceNode *) BADDR(infoData->id_VolumeNode)) != NULL) {
		ConvertBSTR(devNode->dn_Name, volName);
		success = TRUE;
	}
	UnLockDosList1(LDF_DEVICES | LDF_READ);

	MemFree(infoData, sizeof(InfoData));
	return(success);
}

/*
 *	Clear the current disk list
 */

static void ClearDiskList()
{
	if (driverList) {
		DisposeList(driverList);
		driverList = NULL;
	}
	if (deviceList) {
		DisposeList(deviceList);
		deviceList = NULL;
	}
	if (diskScrollList)
		SLRemoveAll(diskScrollList);
}

/*
 * 	Build list of DOS devices with mountted volumes, exec device driver names,
 *		root locations, and scroll list with volume names
 */

BOOL GetDiskList()
{
	BOOL			success;
	WORD			i, numItems, selItem;
	DosListPtr		dosEntry;
	FSSMsgPtr		startupMsg;
	TextChar		selName[30];
	TextChar		deviceName[30], volumeName[30], driverName[50];

	SLDoDraw(diskScrollList, FALSE);
/*
	Remember previously selected volume name
*/
	if ((selItem = SLNextSelect(diskScrollList, -1)) != -1)
		SLGetItem(diskScrollList, selItem, selName);
/*
	Clear and create new lists
*/
	ClearDiskList();
	if ((driverList = CreateList()) == NULL || (deviceList = CreateList()) == NULL)
		return (FALSE);
/*
	Scan device list and add devices with mounted volumes
*/
	success = TRUE;
	dosEntry = LockDosList1(LDF_DEVICES | LDF_READ);
	while ((dosEntry = NextDosEntry1(dosEntry, LDF_DEVICES | LDF_READ)) != NULL) {
		if (dosEntry->dol_Type == DLT_DEVICE &&
			dosEntry->dol_misc.dol_handler.dol_Startup > 255) {
			ConvertBSTR(dosEntry->dol_Name, deviceName);
			strcat(deviceName, ":");
			if (LibraryVersion((struct Library *) DOSBase) < OSVERSION_2_0 ||
				IsFileSystem(deviceName)) {
				if (GetVolumeName(deviceName, volumeName)) {
					startupMsg = BADDR(dosEntry->dol_misc.dol_handler.dol_Startup);
					ConvertBSTR(startupMsg->fssm_Device, driverName);
					if (!InsertListItem(driverList, driverName, strlen(driverName), 0x7FFF) ||
						!InsertListItem(deviceList, deviceName, strlen(deviceName), 0x7FFF)) {
						success = FALSE;
						break;
					}
					SLAddItem(diskScrollList, volumeName, strlen(volumeName), 0x7FFF);
				}
			}
		}
	}
	UnLockDosList1(LDF_DEVICES | LDF_READ);
/*
	Select previously selected volume
*/
	if (selItem != -1) {
		numItems = SLNumItems(diskScrollList);
		for (i = 0; i < numItems; i++) {
			SLGetItem(diskScrollList, i, volumeName);
			if (CmpString(selName, volumeName, strlen(selName), strlen(volumeName), FALSE) == 0) {
				SLSelectItem(diskScrollList, i, TRUE);
				break;
			}
		}
	}
/*
	All done
*/
	SLDoDraw(diskScrollList, TRUE);
	return (success);
}

/*
 *	Dispose of the current disk list
 */

void DisposeDiskList()
{
	ClearDiskList();
	if (diskScrollList)
		DisposeScrollList(diskScrollList);
}

/*
 *	Check disk list to see if it is still correct
 *	Return TRUE if correct
 */

BOOL DiskListChanged()
{
	register WORD	numItems;
	DosListPtr		dosEntry;
	TextChar		deviceName[30], volumeName[30];

/*
	Scan device list and count devices with mounted volumes
*/
	numItems = 0;
	dosEntry = LockDosList1(LDF_DEVICES | LDF_READ);
	while ((dosEntry = NextDosEntry1(dosEntry, LDF_DEVICES | LDF_READ)) != NULL) {
		if (dosEntry->dol_Type == DLT_DEVICE &&
			dosEntry->dol_misc.dol_handler.dol_Startup > 255) {
			ConvertBSTR(dosEntry->dol_Name, deviceName);
			strcat(deviceName, ":");
			if (LibraryVersion((struct Library *) DOSBase) < OSVERSION_2_0 ||
				IsFileSystem(deviceName)) {
				if (GetVolumeName(deviceName, volumeName))
					numItems++;
			}
		}
	}
	UnLockDosList1(LDF_DEVICES | LDF_READ);
	return (numItems != NumListItems(deviceList));
}

/*
 *	Get DOS environment for device and save in dosEnv
 */

static BOOL GetDosEnv(IOExtTDPtr ioReq, TextPtr devName, LONG dosType, LONG bufMemType)
{
	WORD		nameLen;
	DosListPtr	dosEntry;
	DosEnvPtr	env;
	FSSMsgPtr	startupMsg;
	TextChar	name[30];

/*
	Get DOS environment info
	Note: devName is already guaranteed to be a file system device
*/
	env = NULL;
	nameLen = strlen(devName);
	dosEntry = LockDosList1(LDF_DEVICES | LDF_READ);
	while ((dosEntry = NextDosEntry1(dosEntry, LDF_DEVICES | LDF_READ)) != NULL) {
		if (dosEntry->dol_Type == DLT_DEVICE) {
			ConvertBSTR(dosEntry->dol_Name, name);
			strcat(name, ":");
			if (CmpString(devName, name, nameLen, strlen(name), FALSE) == 0) {
				startupMsg = BADDR(dosEntry->dol_misc.dol_handler.dol_Startup);
				env = (DosEnvPtr) BADDR(startupMsg->fssm_Environ);
				break;
			}
		}
	}
	if (env)
		BlockMove(env, &dosEnv, (env->de_TableSize + 1)*4);	// Make copy of contents
	UnLockDosList1(LDF_DEVICES | LDF_READ);
	if (env == NULL)
		return (FALSE);
/*
	If no dosType or bufMemType then save defaults provided
*/
	if (dosEnv.de_TableSize < 12)
		dosEnv.de_BufMemType = bufMemType;
	if (dosEnv.de_TableSize < 16)
		dosEnv.de_DosType = dosType;
/*
	All done
*/
	return (TRUE);
}

/*
 *	Return block size of volume in bytes
 */

static ULONG BlockSize(DosEnvPtr dosEnv)
{
	return (dosEnv->de_SizeBlock << 2);
}

/*
 *	Return total number of blocks in volume
 */

static ULONG TotalBlocks(register DosEnvPtr dosEnv)
{
	register ULONG	tracks;

	tracks = (dosEnv->de_HighCyl - dosEnv->de_LowCyl + 1)*dosEnv->de_Surfaces;
	return (tracks*dosEnv->de_BlocksPerTrack);
}

/*
 *	Return volume offset in bytes to start of volume
 */

static ULONG VolumeOffset(register DosEnvPtr dosEnv)
{
	return (dosEnv->de_LowCyl*dosEnv->de_Surfaces*dosEnv->de_BlocksPerTrack
			*dosEnv->de_SizeBlock << 2);
}

/*
 *	Return root location of volume, relative to start of volume
 */

static ULONG RootLocation(DosEnvPtr dosEnv)
{
	return ((TotalBlocks(dosEnv) - 1 + dosEnv->de_Reserved) >> 1);
}

/*
 *	Set disk to busy or free
 */

static void SetDiskBusy(MsgPortPtr procID, BOOL busy)
{
	LONG	args[1];

	args[0] = (busy) ? DOSTRUE: DOSFALSE;
	DoPacket(procID, ACTION_INHIBIT, args, 1);
}

/*
 *	Turn drive motor off
 */

static void MotorOff(IOExtTDPtr ioReq)
{
	if (isTD) {
		ioReq->iotd_Req.io_Command = CMD_UPDATE;
		ioReq->iotd_Req.io_Length = 0;
		DoIO((IOReqPtr) ioReq);
		ioReq->iotd_Req.io_Command = TD_MOTOR;		// Turn off the motor
		DoIO((IOReqPtr) ioReq);
		ioReq->iotd_Req.io_Command = CMD_CLEAR;
		DoIO((IOReqPtr) ioReq);
	}
}

/*
 *	Allocate a buffer for the specified number of blocks
 */

static UBYTE *AllocBlock(WORD num)
{
	UBYTE	*block;
	LONG	size;

	size = (LONG) num*BlockSize(&dosEnv);
	block = AllocMem(size, dosEnv.de_BufMemType);
	if (block && ((ULONG) block & dosEnv.de_Mask) != (ULONG) block) {
		FreeMem(block, size);
		block = AllocMem(size, dosEnv.de_BufMemType | MEMF_24BITDMA);
		if (block && ((ULONG) block & dosEnv.de_Mask) != (ULONG) block) {
			FreeMem(block, size);
			block = AllocMem(size, dosEnv.de_BufMemType | MEMF_CHIP);
		}
	}
	return (block);
}

/*
 *	Free block buffer
 */

static void FreeBlock(UBYTE *block, WORD num)
{
	if (block)
		FreeMem(block, (LONG) num*BlockSize(&dosEnv));
}

/*
 *	Read a single block from drive
 */

static BOOL ReadBlock(IOExtTDPtr ioReq, UBYTE *buff, ULONG blockNum)
{
	LONG	blockSize = BlockSize(&dosEnv);

	ioReq->iotd_Req.io_Command	= CMD_READ;
	ioReq->iotd_Req.io_Offset	= blockNum*blockSize + VolumeOffset(&dosEnv);
	ioReq->iotd_Req.io_Data		= buff;
	ioReq->iotd_Req.io_Length	= blockSize;

	return (DoIO((IOReqPtr) ioReq) == 0);
}

/*
 *  Write one or more blocks to drive
 */

static BOOL WriteBlocks(IOExtTDPtr ioReq, UBYTE *buff, ULONG blockNum, WORD numBlocks)
{
	LONG	blockSize = BlockSize(&dosEnv);

	ioReq->iotd_Req.io_Command	= CMD_WRITE;
	ioReq->iotd_Req.io_Offset	= blockNum*blockSize + VolumeOffset(&dosEnv);
	ioReq->iotd_Req.io_Data		= buff;
	ioReq->iotd_Req.io_Length	= numBlocks*blockSize;

	return (DoIO((IOReqPtr)ioReq) == 0);
}

/*
 *	Allocate bitmap and bitmap list blocks
 */

static BOOL AllocBitmapBuffers()
{
	bitmapBlock = AllocBlock(1);
	listBlock = AllocBlock(1);
	bitmapBlockValid = FALSE;
	listIndex = -1;
	return (bitmapBlock != NULL && listBlock != NULL);
}

/*
 *	Free bitmap and bitmap list blocks
 */

static void FreeBitmapBuffers()
{
	FreeBlock(bitmapBlock, 1);
	FreeBlock(listBlock, 1);
	bitmapBlockValid = FALSE;
}

/*
 *	Read a long-word from bitmap block
 */

static ULONG ReadBitmap(IOExtTDPtr ioReq, ULONG blockNum, WORD index)
{
	if (!bitmapBlockValid || bitmapBlockNum != blockNum) {
		bitmapBlockValid = FALSE;
		if (!ReadBlock(ioReq, bitmapBlock, blockNum))
			return (0);
		bitmapBlockNum   = blockNum;
		bitmapBlockValid = TRUE;
	}
	return (((ULONG *) bitmapBlock)[index]);
}

/*
 *	Determine if block is in use by file system (bitmap bit == 0)
 */

static BOOL BlockInUse(IOExtTDPtr ioReq, ULONG blockNum)
{
	register WORD	bitmapIndex, needListIndex;
	WORD			offset, index, bitNum, bitsPerBlock;
	ULONG			bits;
	ULONG			blockSize = BlockSize(&dosEnv);

	blockNum -= dosEnv.de_Reserved;	// Reserved blocks are not part of bitmap
	bitsPerBlock = (blockSize - 4)*8;
/*
	Determine which bitmap number and list number are needed
*/
	bitmapIndex = blockNum/bitsPerBlock;
	needListIndex = 0;
	if ((dosEnv.de_DosType & 1) == 0) {	// Old file system
		if (bitmapIndex >= 26)			// Only supports 26 bitmap keys
			return (TRUE);
	}
	else {								// Fast file system
		if (bitmapIndex >= 25) {		// 25 bitmap keys in root block
			needListIndex++;
			bitmapIndex -= 25;
		}
		while (bitmapIndex >= 126) {	// 126 bitmap keys in each extension
			needListIndex++;
			bitmapIndex -= 126;
		}
	}
/*
	Read needed list block (if not already present)
*/
	if (listIndex == -1 || needListIndex < listIndex) {		// Start at root
		if (!ReadBlock(ioReq, listBlock, RootLocation(&dosEnv)))
			return (TRUE);
		listIndex = 0;
	}
	while (listIndex < needListIndex) {
		index = blockSize/4 - ((listIndex == 0) ? 24 : 1);
		if (!ReadBlock(ioReq, listBlock, ((ULONG *) listBlock)[index]))
			return (TRUE);
		listIndex++;
	}
/*
	Get value from bitmap
*/
	bitNum = blockNum % bitsPerBlock;
	index = bitNum/32 + 1;
	offset = (listIndex == 0) ?  (blockSize/4 - 49) : 0;
	bits = ReadBitmap(ioReq, ((ULONG *) listBlock)[bitmapIndex + offset], index);
	return (((bits >> (bitNum % 32)) & 1) == 0);
}

/*
 *	Allocate erase buffer and clear to value
 *	May change numBlocks value (if not enough memory for value passed)
 */

static UBYTE *AllocBuffer(WORD *numBlocks, register UBYTE value)
{
	register UBYTE	*buff;
	register LONG	size;

/*
	Allocate buffer
*/
	buff = NULL;
	while (*numBlocks > 0) {
		if ((buff = AllocBlock(*numBlocks)) != NULL)
			break;
		*numBlocks -= 5;
	}
/*
	Clear buffer
*/
	if (buff) {
		size = (LONG) (*numBlocks)*BlockSize(&dosEnv);
		while (size--)
			buff[size] = value;
	}
	return (buff);
}

/*
 *	Free memory used by erase buffer
 */

static void FreeBuffer(UBYTE *buff, WORD numBlocks)
{
	FreeBlock(buff, numBlocks);
}

/*
 *	Wipe only unused blocks on a volume
 */

static WORD WipeUnused(IOExtTDPtr ioReq, WORD wipeValue)
{
	UBYTE	*buff;
	WORD	numConsec, maxConsec, result;
	ULONG	blockNum, numBlocks;

	numBlocks = TotalBlocks(&dosEnv);
	maxConsec = (isTD) ? 1 : MAX_CONSEC;
	buff = NULL;
	result = WIPE_FAIL;
/*
	Allocate write buffer
*/
	if (!AllocBitmapBuffers() ||
		(buff = AllocBuffer(&maxConsec, wipeValue)) == NULL) {
		Error(ERR_NO_MEM);
		goto Exit;
	}
/*
	Scan for blocks in use, and wipe unused
*/
	result = WIPE_SUCCESS;
	blockNum = dosEnv.de_Reserved;
	while (blockNum < numBlocks) {
		if (StatusCheckAbort()) {
			result = WIPE_ABORT;
			break;
		}
/*
	Skip over consecutive blocks in use
*/
		numConsec = 0;
		while (blockNum < numBlocks && BlockInUse(ioReq, blockNum)) {
			blockNum++;
			if (numConsec++ > 10) {
				if (StatusCheckAbort()) {
					result = WIPE_ABORT;
					break;
				}
				SetStatusBarGraph(blockNum);
				numConsec = 0;
			}
		}
		if (result == WIPE_ABORT)
			break;
/*
	Look for several consecutive blocks that are not in use
*/
		for (numConsec = 0; numConsec < maxConsec && blockNum < numBlocks;
			 numConsec++, blockNum++) {
			if (BlockInUse(ioReq, blockNum))
				break;
		}
		if (numConsec && !WriteBlocks(ioReq, buff, blockNum - numConsec, numConsec)) {
			result = WIPE_FAIL;
			break;
		}
		SetStatusBarGraph(blockNum);
	}
	if (result == WIPE_SUCCESS)
		SetStatusBarGraph(numBlocks);		// Set to 100% complete
/*
	All done
*/
Exit:
	FreeBuffer(buff, maxConsec);
	FreeBitmapBuffers();
	return (result);
}

/*
 *	Wipe all blocks on a volume
 */

static WORD WipeEntire(IOExtTDPtr ioReq, WORD wipeValue)
{
	UBYTE	*buff;
	WORD	numConsec, maxConsec, result;
	ULONG 	blockNum, numBlocks;

	numBlocks = TotalBlocks(&dosEnv);
	maxConsec = (isTD) ? 1 : MAX_CONSEC;
	buff = NULL;
	result = WIPE_FAIL;
/*
	Allocate buffer and wipe to specified value
*/
	if ((buff = AllocBuffer(&maxConsec, wipeValue)) == NULL) {
		Error(ERR_NO_MEM);
		goto Exit;
	}
/*
	Wipe all sectors on volume
*/
	result = WIPE_SUCCESS;
	for (blockNum = 0; blockNum < numBlocks; blockNum++) {
		if (StatusCheckAbort()) {
			result = WIPE_ABORT;
			break;
		}
		numConsec = MIN(numBlocks - blockNum, maxConsec);
		if (!WriteBlocks(ioReq, buff, blockNum, numConsec)) {
			result = WIPE_FAIL;
			break;
		}
		SetStatusBarGraph(blockNum);
	}
	if (result == WIPE_SUCCESS)
		SetStatusBarGraph(numBlocks);		// Set to 100% complete
/*
	All done
*/
Exit:
	FreeBuffer(buff, maxConsec);
	return (result);
}

/*
 *	Format disk
 */

static BOOL DoFormat(MsgPortPtr procID, TextPtr volName, LONG dosType)
{
	BOOL	success;
	BPTR	bVolName;
	LONG	args[2];

	success = FALSE;
	if ((bVolName = MakeBSTR(volName)) != NULL) {
		args[0] = bVolName;
		args[1] = dosType;
		success = DoPacket(procID, ACTION_FORMAT, args, 2);
		FreeBSTR(bVolName);
	}
	return (success);
}

/*
 * 	Wipe disk
 */

void DoWipeDisk(WORD diskNum, BOOL entire)
{
	WORD		wipeNum, numWipes, wipeValue, result;
	LONG		dosType;
	BOOL		devOpen;
	Dir			lock;
	InfoDataPtr	infoData;
	IOExtTDPtr	ioReq;
	MsgPortPtr	ioMsgPort, procID;
	TextChar	deviceName[30], volumeName[30], driverName[50];

	infoData	= NULL;
	lock		= NULL;
	ioMsgPort	= NULL;
	ioReq		= NULL;
	devOpen		= FALSE;
/*
	Confirm the user really wants to wipe the disk
*/
	SLGetItem(diskScrollList, diskNum, volumeName);
	if (!ConfirmWipe(volumeName, entire))
		return;
	SetStdPointer(window, POINTER_WAIT);
/*
	Get device and driver names
*/
	strcpy(deviceName, GetListItem(deviceList, diskNum));
	strcpy(driverName, GetListItem(driverList, diskNum));
/*
	Check for known DOS type
*/
	if ((infoData = MemAlloc(sizeof(InfoData), MEMF_CLEAR)) == NULL) {
		Error(ERR_NO_MEM);
		goto Exit;
	}
	strcat(volumeName, ":");
	lock = Lock(volumeName, ACCESS_READ);
	volumeName[strlen(volumeName) - 1] = '\0';		// Remove trailing ":"
	if (lock == NULL || !Info(lock, infoData)) {
		Error(ERR_NO_INFO);
		goto Exit;
	}
	UnLock(lock);
	lock = NULL;
	dosType = infoData->id_DiskType;
	if (dosType < ID_DOS0 || dosType > ID_DOS5) {
		Error(ERR_CANT_WIPE);
		goto Exit;
	}
/*
	Get DOS environment info
*/
	if (!GetDosEnv(ioReq, deviceName, dosType, MEMF_CHIP | MEMF_PUBLIC)) {
		Error(ERR_CANT_WIPE);
		goto Exit;
	}
/*
	Open device and get DOS environment
*/
	isTD = (CmpString(TD_NAME, driverName, strlen(TD_NAME), strlen(driverName), FALSE) == 0);
	if ((ioMsgPort = CreatePort(0, 0)) == NULL ||
		(ioReq = (IOExtTDPtr) CreateExtIO(ioMsgPort, sizeof(IOExtTD))) == NULL) {
		Error(ERR_NO_MEM);
		goto Exit;
	}
	if (OpenDevice(driverName, infoData->id_UnitNumber, (IOReqPtr) ioReq, 0)) {
		Error(ERR_CANT_WIPE);
		goto Exit;
	}
	ioReq->iotd_Req.io_Message.mn_ReplyPort = ioMsgPort;
	devOpen = TRUE;
/*
	Do actual wipe
*/
	if (!GetStatusRequest(volumeName, TotalBlocks(&dosEnv)))
		goto Exit;
	procID = DeviceProc(deviceName);
	SetDiskBusy(procID, TRUE);
/*
	Do Government wipe
*/
	SetStdPointer(window, POINTER_ARROW);
	if (options.GovtWipe) {
		numWipes = 2*options.GovtRepeatCount;		// Not including final wipe
		for (wipeNum = 0; wipeNum < numWipes; wipeNum++) {
			wipeValue = (wipeNum & 1) ? 0xFF : 0x00;
			SetStatusPassNum(wipeNum + 1, numWipes + 1);
			result = (entire) ? WipeEntire(ioReq, wipeValue) :
								WipeUnused(ioReq, wipeValue);
			if (result != WIPE_SUCCESS)
				break;
		}
		if (result == WIPE_SUCCESS) {
			SetStatusPassNum(numWipes + 1, numWipes + 1);
			result = (entire) ? WipeEntire(ioReq, options.GovtWipeValue) :
								WipeUnused(ioReq, options.GovtWipeValue);
		}
	}
/*
	Do standard wipe
*/
	else {
		for (wipeNum = 0; wipeNum < options.StdRepeatCount; wipeNum++) {
			SetStatusPassNum(wipeNum + 1, options.StdRepeatCount);
			result = (entire) ? WipeEntire(ioReq, options.StdWipeValue) :
								WipeUnused(ioReq, options.StdWipeValue);
			if (result != WIPE_SUCCESS)
				break;
		}
	}
	SetStdPointer(window, POINTER_WAIT);
	StatusEnableAbort(FALSE);
	MotorOff(ioReq);
	if (result == WIPE_FAIL)
		Error(ERR_WIPE_FAIL);
/*
	If wiped entire disk, format volume (only if user did not abort)
*/
	if (entire && result == WIPE_SUCCESS) {
		if (!DoFormat(procID, volumeName, dosEnv.de_DosType)) {
			Error(ERR_NO_FORMAT);
			result = WIPE_FAIL;
		}
	}
/*
	Finish up
*/
	SetDiskBusy(procID, FALSE);
	DisposeStatusReq();
/*
	All done
*/
Exit:
	if (devOpen)
		CloseDevice((IOReqPtr) ioReq);
	if (ioReq)
		DeleteExtIO((IOReqPtr) ioReq);
	if (ioMsgPort)
		DeletePort(ioMsgPort);
	if (lock)
		UnLock(lock);
	if (infoData)
		MemFree(infoData, sizeof(InfoData));
	SetStdPointer(window, POINTER_ARROW);
	if (result == WIPE_SUCCESS)
		SysBeep(5);						// In case the window was zoomed down
}
