/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Wipe Disk
 *
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Window routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/layers.h>
#include <proto/dos.h>
#include <proto/graphics.h>

#include <string.h>
#include <stdio.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Bargraph.h>
#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "WipeDisk.h"
#include "Proto.h"

/*
 *  External variables
 */

extern WindowPtr		window;
extern MsgPortPtr		mainMsgPort;
extern ScrollListPtr	diskScrollList;

extern BOOL	titleChanged;

extern TextChar screenTitle[], strPass[], strOf[];

extern TextPtr	errMessage[];

extern DlgTemplPtr	dlgList[];
extern ReqTemplPtr	reqList[];

extern Options	options;

/*
 *	Local variables and definitions
 */

static RequestPtr	statusReq;
static BarGraphPtr	barGraph;

#define SHIFTKEYS        (IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)

#define ERROR_TEXT 	1		// Error requester

enum {				// Status requester
	ABORT_BUTTON,
	STAT_BARGRAPH,
	DISK_NAME,
	STATNUM_TEXT
};

enum {				// Confirm wipe requesters
	NO_BUTTON,
	YES_BUTTON,
	CONFIRMNAME_TEXT
};

/*
 *	Local prototypes
 */

static void	DrawWindow(void);
static void	DrawStatusRequest(void);
static BOOL	StatusRequestFilter(IntuiMsgPtr, WORD *);

/*
 *	Set buttons for main window
 */

void SetButtons(BOOL entire)
{
	GadgetPtr	gadgList = window->FirstGadget;

	SetGadgetItemValue(gadgList, ENTIRE_OPT, window, NULL, entire);
	SetGadgetItemValue(gadgList, UNUSED_OPT, window, NULL, !entire);
	EnableGadgetItem(gadgList, WIPE_BUTTON, window, NULL,
					 (SLNextSelect(diskScrollList, -1) != -1));
}

/*
 *  Create and draw main window
 */

void CreateWindow()
{
	if ((diskScrollList = NewScrollList(SL_SINGLESELECT)) == NULL ||
		(window = GetDialog(dlgList[DLG_MAIN], NULL, mainMsgPort)) == NULL)
		return;
    SetStdPointer(window, POINTER_ARROW);
    SetWindowTitles(window, (UBYTE *) -1, screenTitle);
	ModifyIDCMP(window, window->IDCMPFlags | DISKINSERTED | DISKREMOVED);
	InitScrollList(diskScrollList, GadgetItem(window->FirstGadget, DISK_BOX), window, NULL);
	DrawWindow();
	ModifyIDCMP(window, window->IDCMPFlags | (DISKINSERTED | DISKREMOVED));
	(void) GetDiskList();
}

/*
 *	Remove main window
 */

void RemoveWindow()
{
	DisposeDiskList();
	if (window) {
		SetClip(window->WLayer, NULL);
		DisposeDialog(window);
		window = NULL;
	}
}

/*
 *	Draw main window contents
 */

static void DrawWindow()
{
	SLDrawBorder(diskScrollList);
	SLDrawList(diskScrollList);
	OutlineButton(GadgetItem(window->FirstGadget, WIPE_BUTTON), window, NULL, TRUE);
}

/*
 *	Refresh window (and status dialog if it is present)
 */

void RefreshWindow()
{
	RastPtr		rPort;
	Rectangle	rect;

	GetWindowRect(window, &rect);
	if (!EmptyRect(&rect)) {
/*
	If old color scheme, fill with _tbPenLight
	Note: Cannot call RefreshGadgets() within Begin/EndRefresh()
*/
		if (_tbPenLight != 0) {
			rPort = window->RPort;
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &rect);
			RefreshGadgets(GadgetItem(window->FirstGadget, 0), window, NULL);
		}
		BeginRefresh(window);
		DrawWindow();
		EndRefresh(window, TRUE);
	}
}

/*
 *	Return screen titles to default setting
 */

void FixTitle()
{
	SetWindowTitles(window, (TextPtr) -1, screenTitle);
	titleChanged = FALSE;
}

/*
 *  Error report routine
 *  If not enough memory for requester, put error message on screen title bar
 */

void Error(register WORD errNum)
{
	if (errNum > ERR_UNKNOWN)
		errNum = ERR_UNKNOWN;
	dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = errMessage[errNum];
	if (StdAlert(dlgList[DLG_ERROR], window->WScreen, mainMsgPort, DialogFilter) == -1) {
		SetWindowTitles(window, (UBYTE *) -1, errMessage[errNum]);
   		DisplayBeep(window->WScreen);
    	SysBeep(5);
		titleChanged = TRUE;
	}
}

/*
 *	Handle HELP key
 */

void DoHelp()
{
	DialogPtr	dlg;

	if ((dlg = GetDialog(dlgList[DLG_ABOUT], window->WScreen, mainMsgPort)) == NULL)
		Error(ERR_NO_MEM);
	else {
		OutlineButton(GadgetItem(dlg->FirstGadget, OK_BUTTON), dlg, NULL, TRUE);
		(void) ModalDialog(mainMsgPort, dlg, DialogFilter);
		DisposeDialog(dlg);
	}
}

/*
 *	Confirm that user wants to wipe wipename
 */

BOOL ConfirmWipe(TextPtr wipeName, BOOL entire)
{
	BOOL		success;
	WORD		reqID;
	RequestPtr 	req;

	success = FALSE;
	SetDefaultButtons(NO_BUTTON, NO_BUTTON);
/*
	Display and handle requester
*/
	reqID = (entire) ? REQ_CONFIRMENTIRE : REQ_CONFIRMUNUSED;
	reqList[reqID]->Gadgets[CONFIRMNAME_TEXT].Info = wipeName;
	if ((req = GetRequest(reqList[reqID], window, TRUE)) == NULL)
		Error(ERR_NO_MEM);
	else {
		SysBeep(5);
		OutlineButton(GadgetItem(req->ReqGadget, NO_BUTTON), window, req, TRUE);
		if (ModalRequest(mainMsgPort, window, DialogFilter) == YES_BUTTON)
			success = TRUE;
		EndRequest(req, window);
		DisposeRequest(req);
	}
	SetDefaultButtons(OK_BUTTON, CANCEL_BUTTON);
	return (success);
}

/*
 *	Bring up wipe status requester, and initialize bar graph
 */

BOOL GetStatusRequest(TextPtr diskName, ULONG totalBlocks)
{
	GadgetPtr	gadgList;
	Rectangle	rect;
	TextChar	quotedName[30];

	strcpy(quotedName, "\"");
	strcat(quotedName, diskName);
	strcat(quotedName,"\"");
	reqList[REQ_WIPESTAT]->Gadgets[DISK_NAME].Info = quotedName;
	if ((statusReq = GetRequest(reqList[REQ_WIPESTAT], window, TRUE)) == NULL) {
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	gadgList = statusReq->ReqGadget;
	GetGadgetRect(GadgetItem(gadgList, STAT_BARGRAPH), window, statusReq, &rect);
	barGraph = NewBarGraph(&rect, totalBlocks, 8, (BG_HORIZONTAL | BG_SHOWPERCENT));
	DrawStatusRequest();
	return (TRUE);
}

/*
 *	Draw status request contents
 */

static void DrawStatusRequest()
{
	if (statusReq && statusReq->ReqLayer) {
		DrawBarGraph(statusReq->ReqLayer->rp, barGraph);
		OutlineButton(GadgetItem(statusReq->ReqGadget, ABORT_BUTTON), window, statusReq, TRUE);
	}
}

/*
 *	Set pass number in status dialog
 */

void SetStatusPassNum(WORD wipeNum, WORD totWipes)
{
	TextChar	strBuff[50];

	strcpy(strBuff, strPass);
	NumToString(wipeNum, strBuff + strlen(strBuff));
	strcat(strBuff, strOf);
	NumToString(totWipes, strBuff + strlen(strBuff));
	SetGadgetItemText(statusReq->ReqGadget, STATNUM_TEXT, window, statusReq, strBuff);
}

/*
 *	Set status requester bar graph setting
 */

void SetStatusBarGraph(ULONG blockNum)
{
	if (statusReq->ReqLayer)
		SetBarGraph(statusReq->ReqLayer->rp, barGraph, blockNum);
}

/*
 *	Remove status requester
 */

void DisposeStatusReq()
{
	DisposeBarGraph(barGraph);
	EndRequest(statusReq, window);
	DisposeRequest(statusReq);
	statusReq = NULL;
}

/*
 *	Enable or disable abort button in status requester
 */

void StatusEnableAbort(BOOL enable)
{
	EnableGadgetItem(statusReq->ReqGadget, ABORT_BUTTON, window, statusReq, enable);
}

/*
 *	Check for abort request from user
 */

BOOL StatusCheckAbort()
{
	return (CheckRequest(mainMsgPort, window, DialogFilter) == ABORT_BUTTON);
}

/*
 *	Filter function for ModalDialog/ModalRequest
 *	Handles window updates
 */

BOOL DialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	RequestPtr	req;

	if (intuiMsg->IDCMPWindow == window && intuiMsg->Class == REFRESHWINDOW) {
		ReplyMsg((MsgPtr) intuiMsg);
		if (statusReq)
			DrawStatusRequest();
		else if ((req = window->FirstRequest) != NULL)
			OutlineButton(GadgetItem(req->ReqGadget, OK_BUTTON), window, req, TRUE);
		RefreshWindow();
		*item = -1;
		return (TRUE);
	}
	return (FALSE);
}
