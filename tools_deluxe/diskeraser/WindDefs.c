/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Wipe Disk
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Window and requester definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Language.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Request.h>

/*
 *	Standard gadget definitions
 */

#define GT_ADJUST_UPARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,													\
		left, (top)+5-ARROW_HEIGHT, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		CURSORKEY_UP, 0, (Ptr) IMAGE_ARROW_UP }

#define GT_ADJUST_DOWNARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,										\
		left, (top)+5, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		CURSORKEY_DOWN, 0, (Ptr) IMAGE_ARROW_DOWN }

#define GT_ADJUST_TEXT(left, top)	\
	{ GADG_STAT_TEXT, (left)+10+ARROW_WIDTH, top, 0, 0, 0, 0, 0, 0, 0, 0, NULL }

/*
 *	Cursur key equivalents
 */

#define CURSORKEY_UP	0x1C
#define CURSORKEY_DOWN	0x1D
#define CURSORKEY_RIGHT	0x1E
#define CURSORKEY_LEFT	0x1F

/*
 *  Main dialog
 */

#define DISK_WIDTH	(350 - 130)

#define DISK_NUM	6

static GadgetTemplate diskGadgets[] = {
	{ GADG_PUSH_BUTTON, -90, 10, 0, 0, 70, 20, 0, 0, 'E', 0, "Erase" },

	{ GADG_PUSH_BUTTON, -90, 40, 0, 0, 70, 20, 0, 0, 'O', 0, "Options" },

	SL_GADG_BOX(20, 30, DISK_WIDTH, DISK_NUM),
	SL_GADG_UPARROW(20, 30, DISK_WIDTH, DISK_NUM),
	SL_GADG_DOWNARROW(20, 30, DISK_WIDTH, DISK_NUM),
	SL_GADG_SLIDER(20, 30, DISK_WIDTH, DISK_NUM),

	{ GADG_RADIO_BUTTON, 225, -25, 0, 0, 0,  0, 0, 0, 'E', 0, "Entire Disk" },

	{ GADG_RADIO_BUTTON,  80, -25, 0, 0, 0,  0, 0, 0, 'U', 0, "Unused Space" },

	{ GADG_STAT_TEXT,  20,-25, 0, 0, 0,  0, 0, 0, 0, 0, "Erase:  " },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0,  0, 0, 0, 0, "Select a disk to erase:" },

	{ GADG_ITEM_NONE },
};

static DialogTemplate mainDialog = {
	DLG_TYPE_WINDOW, DLG_FLAG_CLOSE | DLG_FLAG_DEPTH | DLG_FLAG_ZOOM,
	-1, -1, 350, 30 + DISK_NUM*12 + 35, diskGadgets, "Disk Eraser"
};

/*
 *  Error dialog
 */

static GadgetTemplate errorGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT, 55, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },

	{ GADG_ITEM_NONE }
};

static DialogTemplate errorDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 90, errorGadgets, NULL
};

/*
 *  About dialog
 */

static GadgetTemplate aboutGadgets[] = {
	{ GADG_PUSH_BUTTON, 100, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT, 78, 15, 0, 0, 0, 0, 0, 0, 0, 0, "Disk Eraser 1.0" },
	{ GADG_STAT_TEXT, 42, 40, 0, 0, 0, 0, 0, 0, 0, 0, "Designed and developed" },
	{ GADG_STAT_TEXT, 78, 55, 0, 0, 0, 0, 0, 0, 0, 0, "by Beth Henry" },
	{ GADG_STAT_TEXT, 66, 80, 0, 0, 0, 0, 0, 0, 0, 0, "Copyright \251 1993" },
	{ GADG_STAT_TEXT, 42, 95, 0, 0, 0, 0, 0, 0, 0, 0, "Central Coast Software" },
	{ GADG_STAT_TEXT, 78,110, 0, 0, 0, 0, 0, 0, 0, 0, "A division of" },
	{ GADG_STAT_TEXT, 22,125, 0, 0, 0, 0, 0, 0, 0, 0, "New Horizons Software, Inc."},
	{ GADG_STAT_TEXT, 54,140, 0, 0, 0, 0, 0, 0, 0, 0, "All Rights Reserved" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboutDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 195, aboutGadgets, NULL
};

/*
 * 	Confirm wipe entire dialog
 */

static GadgetTemplate confirmEntireGadgets[] = {
	{ GADG_PUSH_BUTTON, 180, -30, 0, 0, 60, 20, 0, 0, 'N', 0, "No" },
	{ GADG_PUSH_BUTTON,  60, -30, 0, 0, 60, 20, 0, 0, 'Y', 0, "Yes" },
	{ GADG_STAT_TEXT, 165, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL},

	{ GADG_STAT_TEXT,  55, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Erasing disk:\nwill irretrievably erase all\ninformation on the disk!" },
	{ GADG_STAT_TEXT,  20, 55, 0, 0, 0, 0, 0, 0, 0, 0, "Is this what you want to do?"},
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_CAUTION },

	{ GADG_ITEM_NONE }
};

static RequestTemplate confirmEntireRequest = {
	-1, -1, 300, 110, confirmEntireGadgets
};

/*
 * 	Confirm wipe unused space dialog
 */

static GadgetTemplate confirmUnusedGadgets[] = {
	{ GADG_PUSH_BUTTON, 180, -30, 0, 0, 60, 20, 0, 0, 'N', 0, "No" },
	{ GADG_PUSH_BUTTON,  60, -30, 0, 0, 60, 20, 0, 0, 'Y', 0, "Yes" },
	{ GADG_STAT_TEXT, 165, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL},

	{ GADG_STAT_TEXT,  55, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Erasing disk:\nwill irretrievably erase all\ndeleted files on the disk!" },
	{ GADG_STAT_TEXT,  20, 55, 0, 0, 0, 0, 0, 0, 0, 0, "Is this what you want to do?"},
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_CAUTION },

	{ GADG_ITEM_NONE }
};

static RequestTemplate confirmUnusedRequest = {
	-1, -1, 300, 110, confirmUnusedGadgets
};

/*
 *	Wiping status requester
 */

static GadgetTemplate statusGadgets[] = {
	{ GADG_PUSH_BUTTON, 120, -30, 0, 0,  60, 20, 0, 0, 'S', 0, "Stop" },

	{ GADG_STAT_BORDER,  20,  60, 0, 0, -40, 20, 0, 0,   0, 0, NULL },
	{ GADG_STAT_TEXT,	120,  10, 0, 0,   0,  0, 0, 0,   0, 0, NULL },
	{ GADG_STAT_TEXT,	 20,  40, 0, 0,   0,  0, 0, 0,   0, 0, NULL },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 300 - 40, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,	   20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Erasing Disk" },

	{ GADG_ITEM_NONE }
};

static RequestTemplate statusRequest = {
	-1, -1, 300, 125, statusGadgets
};

/*
 *	Options requester
 */

static GadgetTemplate optionsGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, 'C', 0, "Cancel" },

	GT_ADJUST_UPARROW(80, 85),		// Wipe value
	GT_ADJUST_DOWNARROW(80,85),
	GT_ADJUST_TEXT(80, 85),

	GT_ADJUST_UPARROW(250, 85),		// Repeat count
	GT_ADJUST_DOWNARROW(250, 85),
	GT_ADJUST_TEXT(250, 85),

	{ GADG_RADIO_BUTTON ,  70, 40, 0, 0, 0,  0, 0, 0, 'S', 0, "Standard Erase" },
	{ GADG_RADIO_BUTTON ,  70, 55, 0, 0, 0,  0, 0, 0, 'U', 0, "US Government Erase" },

	{ GADG_STAT_STDBORDER, 20, 25, 0,  0, 330 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT,	20, 10, 0, 0,  0,  0, 0, 0,   0, 0, "Erase Options" },
	{ GADG_STAT_TEXT,	20, 40, 0, 0,  0,  0, 0, 0,   0, 0, "Type:" },
	{ GADG_STAT_TEXT,	20, 85, 0, 0,  0,  0, 0, 0,   0, 0, "Value: " },
	{ GADG_STAT_TEXT,  180, 85, 0, 0,  0,  0, 0, 0,   0, 0, "Repeat:" },

	{ GADG_ITEM_NONE }
};

static RequestTemplate optionsRequest = {
	-1, -1, 330, 110, optionsGadgets
};

/*
 *	Dialog list
 */

DlgTemplPtr dlgList[] = {
	&mainDialog,
	&errorDialog,
	&aboutDialog,
};

/*
 *	RequestList
 */

ReqTemplPtr reqList[] = {
	&statusRequest,
	&optionsRequest,
	&confirmEntireRequest,
	&confirmUnusedRequest
};