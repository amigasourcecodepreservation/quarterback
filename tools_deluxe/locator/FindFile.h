/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	FindFile Definitions
 */

/*
	menu definitions
 */

#define PROJ_MENU 0

enum {
	DISK_ITEM,	ABOUT_ITEM, QUIT_ITEM = 3
};

/*
	error definitions
 */

enum {
	INIT_ERR_WINDOW, 	INIT_ERR_PORT
};

enum {
	ERR_VIEW, ERR_NO_MEM, ERR_DIR_ACC, ERR_NO_FILE, ERR_UNKNOWN, ERR_MAX_ERROR
};

/*
	window definitions
 */

enum {
	DLG_MAIN,	DLG_ERROR,	DLG_ABOUT
};


#define FILE_WIDTH		200
#define FILE_NUM		5

#define PATH_WIDTH		FILE_WIDTH
#define PATH_NUM		7

#define INFO_WIDTH		240
#define INFO_HEIGHT		4*11

#define VIEW_WIDTH		INFO_WIDTH
#define VIEW_HEIGHT		105

enum {
	CONTAINS_ITEM,
	STARTS_ITEM,
	ENDS_ITEM,
	IS_ITEM,
	ISNOT_ITEM,
	NOTCONTAIN_ITEM,
	PATTERN_ITEM = 7
};

enum {
	FIND_BUTTON,	NAME_TEXT,		PATH_TEXT,	PATTERN_POPUP,
	LIST_BOX,		LIST_UP,		LIST_DOWN,	LIST_SCROLL,
	VIEW_RECT,
	PATH_BOX,		PATH_UP,		PATH_DOWN,	PATH_SCROLL,
	INFO_RECT,		SEARCH_INDIC,	NUMFILES,	FILESTXT
};

#define MAX_FILENAME_LEN	202

/*
 *	Key equivalents for "Find" and "Stop" buttons
 */

#define CHR_FIND	'F'
#define CHR_STOP	'S'

/*
 *	Key equivalents for find type pop-up (need to be upper case)
 */

#define CHR_CONTAINS	'C'
#define CHR_STARTS		'T'
#define CHR_ENDS		'E'
#define CHR_IS			'I'
#define CHR_ISNOT		'N'
#define CHR_NOTCONTAIN	'D'
#define CHR_PATTERN		'P'
