/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	FindFile
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Text strings
 */

#include <exec/types.h>

#include <Typedefs.h>

#include <Toolbox/Language.h>

TextChar screenTitle[] = " Locator 1.0 - \251 1993 Central Coast Software";

TextChar version[] = "$VER: Locator 1.0";

TextPtr	initError[] = {
	" Can't open window.",
	" Can't create message port.",
};

TextPtr	errMessage[] = {
	"Unable to view file.",
	"Not enough memory.",
	"Can't access directory\n",
	"No matching files found.",
	"Unknown internal error."
};

TextChar strDate[] = "Date:";
TextChar strSize[] = "Size:";
TextChar strComment[] = "Comment:";
TextChar strBytes[] = " Bytes";

TextChar strStop[] = "Stop";
TextChar strFind[] = "Find";
TextChar strFilesFound[] = "Files found:  ";
