/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Find File
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Menu definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Menu.h>

/*
 *  Project menu
 */

static MenuItemTemplate projItems[] = {
    { MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Disks", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "About...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'Q', 0, 0, "Quit",  NULL },
    { MENU_NO_ITEM }
};

/*
 *  Menu strip template
 */

MenuTemplate newMenus[] = {
    { " Project ", &projItems[0] },
    { NULL, NULL }
};
