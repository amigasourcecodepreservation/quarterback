/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *  FindFile
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *  Eye icon procedures
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Typedefs.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Image.h>
#include <Toolbox/Gadget.h>

#include "FindFile.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr	window;

/*
 *	Local variables and definitions
 */

#define CHANGE_MILLIS	500L

#define DIFF_MILLIS(sec1, micro1, sec2, micro2)	\
	(((sec2)-(sec1))*1000L+((micro2)-(micro1))/1000L)

enum {
	IMAGE_CLOSED,
	IMAGE_LEFT,
	IMAGE_RIGHT
};

static BOOL	searchOn, isLeft;

static ULONG	prevSecs, prevMicros;		// For toggling eyes display

static ImagePtr	images[3];

static ULONG __chip hrClosedEyesData[] = {
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x01FFE000, 0x07FF8000,
	0x1FFFFE00, 0x7FFFF800,
	0xFFFFFFC3, 0xFFFFFF00,
	0x7FFFFF81, 0xFFFFFE00,
	0xBFFFFF42, 0xFFFFFD00,
	0x1FFFFE00, 0x7FFFF800,
	0x27FFF900, 0x9FFFE400,
	0x427F9081, 0x09FE4200,
	0x02211000, 0x08844000,
	0x04210800, 0x10842000,
	0x04408800, 0x11022000,
	0x00408000, 0x01020000,

	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x01FFE000, 0x07FF8000,
	0x1FFFFE00, 0x7FFFF800,
	0x7FFFFF81, 0xFFFFFE00,
	0x3FFFFF00, 0xFFFFFC00,
	0x0FFFFC00, 0x3FFFF000,
	0x03FFF000, 0x0FFFC000,
	0x003F0000, 0x00FC0000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
};

static Icon  hrClosedEyesIcon = {
	56, 19, 2, NULL, (PLANEPTR) hrClosedEyesData, NULL
};

static ULONG __chip mrClosedEyesData[] = {
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x01FFE000, 0x07FF8000,
	0x1FFFFE00, 0x7FFFF800,
	0x7FFFFF81, 0xFFFFFE00,
	0xBFFFFF42, 0xFFFFFD00,
	0x17FFF900, 0x5FFFE800,
	0x227F9080, 0x89FE4400,
	0x04210800, 0x10842000,
	0x00408000, 0x01020000,

	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x01FFE000, 0x07FF8000,
	0x1FFFFE00, 0x7FFFF800,
	0x1FFFFE00, 0x7FFFF800,
	0x03FFF000, 0x0FFFC000,
	0x003F0000, 0x00FC0000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
};

static Icon  mrClosedEyesIcon = {
	56, 12, 2, NULL, (PLANEPTR) mrClosedEyesData, NULL
};

static ULONG __chip hrLeftEyesData[] = {
	0x00408000, 0x01020000,
	0x04408800, 0x11022000,
	0x04210800, 0x10842000,
	0x027F9000, 0x09FE4000,
	0x47FFF881, 0x1FFFE200,
	0x3FC0FF00, 0xFF03FC00,
	0x3FF00F00, 0xFFC03C00,
	0xF7F003C3, 0xDFC00F00,
	0xCFF800C3, 0x3FE00300,
	0x8FF80042, 0x3FE00100,
	0x4FF80081, 0x3FE00200,
	0x37F00300, 0xDFC00C00,
	0x1FF00E00, 0x7FC03800,
	0x07C0F800, 0x1F03E000,
	0x007F8000, 0x01FE0000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,

	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x005E8000, 0x017A0000,
	0x05806800, 0x1601A000,
	0x183F0600, 0x60FC1800,
	0x23FFF100, 0x8FFFC400,
	0x4FFFFC81, 0x3FFFF200,
	0xBE3FFF42, 0xF8FFFD00,
	0x7E3FFF81, 0xF8FFFE00,
	0x3E3FFF00, 0xF8FFFC00,
	0x0FFFFC00, 0x3FFFF000,
	0x03FFF000, 0x0FFFC000,
	0x003F0000, 0x00FC0000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
};

static Icon hrLeftEyesIcon = {
	56, 19, 2, NULL, (PLANEPTR) hrLeftEyesData, NULL
};

static ULONG __chip mrLeftEyesData[] = {
	0x00408000, 0x01020000,
	0x047F8800, 0x11FE2000,
	0x27FFF900, 0x9FFFE400,
	0x3FC0FF00, 0xFF03FC00,
	0xFFF00FC3, 0xFFC03F00,
	0x6FF80181, 0xBFE00600,
	0x6FF80181, 0xBFE00600,
	0x3FF00F00, 0xFFC03C00,
	0x07C0F800, 0x1F03E000,
	0x007F8000, 0x01FE0000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,

	0x00000000, 0x00000000,
	0x005E8000, 0x017A0000,
	0x05806800, 0x1601A000,
	0x283F0500, 0xA0FC1400,
	0x43FFF081, 0x0FFFC200,
	0x1E3FFE00, 0x78FFF800,
	0x1E3FFE00, 0x78FFF800,
	0x03FFF000, 0x0FFFC000,
	0x003F0000, 0x00FC0000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
};

static Icon mrLeftEyesIcon = {
	56, 12, 2, NULL, (PLANEPTR) mrLeftEyesData, NULL
};

static ULONG __chip hrRightEyesData[] = {
	0x00408000, 0x01020000,
	0x04408800, 0x11022000,
	0x04210800, 0x10842000,
	0x027F9000, 0x09FE4000,
	0x47FFF881, 0x1FFFE200,
	0x3FC0FF00, 0xFF03FC00,
	0x3C03FF00, 0xF00FFC00,
	0xF003FBC3, 0xC00FEF00,
	0xC007FCC3, 0x001FF300,
	0x8007FC42, 0x001FF100,
	0x4007FC81, 0x001FF200,
	0x3003FB00, 0xC00FEC00,
	0x1C03FE00, 0x700FF800,
	0x07C0F800, 0x1F03E000,
	0x007F8000, 0x01FE0000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,

	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x005E8000, 0x017A0000,
	0x05806800, 0x1601A000,
	0x183F0600, 0x60FC1800,
	0x23FFF100, 0x8FFFC400,
	0x4FFFFC81, 0x3FFFF200,
	0xBFFF1F42, 0xFFFC7D00,
	0x7FFF1F81, 0xFFFC7E00,
	0x3FFF1F00, 0xFFFC7C00,
	0x0FFFFC00, 0x3FFFF000,
	0x03FFF000, 0x0FFFC000,
	0x003F0000, 0x00FC0000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
};

static Icon hrRightEyesIcon = {
	56, 19, 2, NULL, (PLANEPTR) hrRightEyesData, NULL
};

static ULONG __chip mrRightEyesData[] = {
	0x00408000, 0x01020000,
	0x047F8800, 0x11FE2000,
	0x27FFF900, 0x9FFFE400,
	0x3FC0FF00, 0xFF03FC00,
	0xFC03FFC3, 0xF00FFF00,
	0x6007FD81, 0x801FF600,
	0x6007FD81, 0x801FF600,
	0x3C03FF00, 0xF00FFC00,
	0x07C0F800, 0x1F03E000,
	0x007F8000, 0x01FE0000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,

	0x00000000, 0x00000000,
	0x005E8000, 0x017A0000,
	0x05806800, 0x1601A000,
	0x283F0500, 0xA0FC1400,
	0x43FFF081, 0x0FFFC200,
	0x1FFF1E00, 0x7FFC7800,
	0x1FFF1E00, 0x7FFC7800,
	0x03FFF000, 0x0FFFC000,
	0x003F0000, 0x00FC0000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
};

static Icon mrRightEyesIcon = {
	56, 12, 2, NULL, (PLANEPTR) mrRightEyesData, NULL
};

/*
 *	Local prototypes
 */

static BOOL	IsTopaz8ScreenFont(void);
static void	SetSearchIndic(WORD);

/*
 *	Return TRUE if screen font is topaz-8 or equivalent
 */

static BOOL IsTopaz8ScreenFont()
{
	TextFontPtr	font = _tbScreen->RastPort.Font;

	return (font->tf_YSize == 8 && font->tf_XSize == 8 &&
			(font->tf_Flags & FPF_PROPORTIONAL) == 0);
}

/*
 *	Set gadget images for icons
 */

void GetSearchIndic()
{
	ColorMapPtr	colorMap = _tbScreen->ViewPort.ColorMap;
	UWORD		iconColors[4];

	iconColors[0] = GetRGB4(colorMap, _tbPenLight);
	iconColors[1] = GetRGB4(colorMap, _tbPenBlack);
	iconColors[2] = GetRGB4(colorMap, _tbPenWhite);
	iconColors[3] = GetRGB4(colorMap, _tbPenDark);

	if (IsTopaz8ScreenFont()) {
		mrClosedEyesIcon.ColorTable = iconColors;
		mrLeftEyesIcon.ColorTable = iconColors;
		mrRightEyesIcon.ColorTable = iconColors;
		images[IMAGE_CLOSED] = MakeIconImage(&mrClosedEyesIcon, mrClosedEyesIcon.Width,
							   mrClosedEyesIcon.Height, ICON_BOX_NONE);
		images[IMAGE_LEFT] = MakeIconImage(&mrLeftEyesIcon, mrLeftEyesIcon.Width,
							 mrLeftEyesIcon.Height, ICON_BOX_NONE);
		images[IMAGE_RIGHT] = MakeIconImage(&mrRightEyesIcon, mrRightEyesIcon.Width,
							  mrRightEyesIcon.Height, ICON_BOX_NONE);
	}
	else {
		hrClosedEyesIcon.ColorTable = iconColors;
		hrLeftEyesIcon.ColorTable = iconColors;
		hrRightEyesIcon.ColorTable = iconColors;
		images[IMAGE_CLOSED] = MakeIconImage(&hrClosedEyesIcon, 0, 0, ICON_BOX_NONE);
		images[IMAGE_LEFT] = MakeIconImage(&hrLeftEyesIcon, 0, 0, ICON_BOX_NONE);
		images[IMAGE_RIGHT] = MakeIconImage(&hrRightEyesIcon, 0, 0, ICON_BOX_NONE);
	}
	searchOn = FALSE;
}

/*
 *	Dispose of search indic icons
 */

void DisposeSearchIndic()
{
	register WORD	i;

	for (i = 0; i < 3; i++) {
		if (images[i])
			FreeImage(images[i]);
	}
}

/*
 *	Set icon images for icons
 */

static void SetSearchIndic(WORD imageNum)
{
	register WORD		width, height;
	register GadgetPtr	srchGadg;
	register ImagePtr	image;
	Rectangle			rect;

	image = images[imageNum];
	srchGadg = GadgetItem(window->FirstGadget, SEARCH_INDIC);
	GetGadgetRect(srchGadg, window, NULL, &rect);
	width = rect.MaxX - rect.MinX + 1;
	height = rect.MaxY - rect.MinY + 1;
	image->LeftEdge = (width - image->Width)/2;
	image->TopEdge = (height - image->Height)/2;
	srchGadg->GadgetRender = image;
	RefreshGList(srchGadg, window, NULL, 1);
}

/*
 *	Toggle whether eyes open or closed
 */

void OnOffSearchIndic(BOOL on)
{
	if (on) {
		SetSearchIndic(IMAGE_LEFT);
		isLeft = TRUE;
		CurrentTime(&prevSecs, &prevMicros);
	}
	else
		SetSearchIndic(IMAGE_CLOSED);
	searchOn = on;
}

/*
 *	Toggle eyes left or right
 */

void UpdateSearchIndic()
{
	ULONG	newSecs, newMicros;

	if (!searchOn)
		return;
/*
	Check to see if we need to change (change once per second)
*/
	CurrentTime(&newSecs, &newMicros);
	if (DIFF_MILLIS(prevSecs, prevMicros, newSecs, newMicros) >= CHANGE_MILLIS) {
		SetSearchIndic((isLeft) ? IMAGE_RIGHT : IMAGE_LEFT);
		isLeft = !isLeft;
		prevSecs = newSecs;
		prevMicros = newMicros;
	}
}
