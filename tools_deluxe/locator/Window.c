/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Find File
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Window routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>
#include <Toolbox/DOS.h>

#include "Proto.h"
#include "FindFile.h"

/*
 *  External variables
 */

extern WindowPtr	window;
extern MsgPortPtr	mainMsgPort;

extern DialogTemplate	mainDialog;

extern ScrollListPtr	fileList;
extern ScrollListPtr	pathList;

extern MenuTemplate		newMenus[];

extern TextChar	screenTitle[], strStop[], strFind[], strFilesFound[];

extern TextPtr	errMessage[];

extern DlgTemplPtr	dlgList[];

extern BOOL	titleChanged;

/*
 *	Local variables
 */

typedef struct FileInfoBlock	FIB, *FIBPtr;

#define ERROR_TEXT 	1
#define ERROR_WIDTH	375

/*
 *	Local prototypes
 */

static void	DrawWindow(void);

/*
 *  Create and draw main window
 */

void CreateWindow()
{
	MenuPtr		menuStrip;
	WORD		len;
	GadgetPtr	gadgList;

	len = TextLength(&_tbScreen->RastPort, strFilesFound, strlen(strFilesFound));
	dlgList[DLG_MAIN]->Gadgets[NUMFILES].LeftEdge =
	dlgList[DLG_MAIN]->Gadgets[FILESTXT].LeftEdge + len;

	if ((fileList = NewScrollList(SL_SINGLESELECT)) == NULL ||
		(pathList = NewScrollList(SL_NOSELECT)) == NULL)
		return;

	GetSearchIndic();

	if ((window = GetDialog(dlgList[DLG_MAIN], NULL, mainMsgPort)) == NULL) {
		DisposeScrollList(fileList);
		DisposeScrollList(pathList);
		DisposeSearchIndic();
		return;
	}

    SetStdPointer(window, POINTER_ARROW);
    SetWindowTitles(window, (UBYTE *) -1, screenTitle);
	ModifyIDCMP(window, window->IDCMPFlags | MENUPICK | DISKINSERTED | DISKREMOVED);

	gadgList = window->FirstGadget;
	InitScrollList(fileList, GadgetItem(gadgList, LIST_BOX), window, NULL);
	InitScrollList(pathList, GadgetItem(gadgList, PATH_BOX), window, NULL);
	DrawWindow();
	OnOffSearchIndic(FALSE);
/*
	Get menu
*/
	if ((menuStrip = GetMenuStrip(newMenus)) == NULL) {
		RemoveWindow();
		return;
	}
	InsertMenuStrip(window, menuStrip);
}

/*
 *  Remove main window
 */

void RemoveWindow()
{
	register RegionPtr	clipRegion;
	MenuPtr				menuStrip;

	if (window) {
		SetStdPointer(window, POINTER_WAITDELAY);
		menuStrip = window->MenuStrip;
		DisposeDiskList();
		DisposeSearchIndic();
		ClearMenuStrip(window);
		clipRegion = (RegionPtr) InstallClipRegion(window->WLayer, NULL);
		ClearScrollList(fileList);
		DisposeDialog(window);
		DisposeScrollList(fileList);
		DisposeScrollList(pathList);
		if (menuStrip)
			DisposeMenuStrip(menuStrip);
		window = NULL;
		if (clipRegion)
			DisposeRegion(clipRegion);
	}
}

/*
 *	Draw window contents
 */

static void DrawWindow()
{
	GadgetPtr	srchGadg;
	Rectangle	rect;

	OutlineButton(GadgetItem(window->FirstGadget, FIND_BUTTON), window, NULL, TRUE);
	srchGadg = GadgetItem(window->FirstGadget, SEARCH_INDIC);
	GetGadgetRect(srchGadg, window, NULL, &rect);
	DrawShadowBox(window->RPort, &rect, 0, FALSE);
	SLDrawBorder(fileList);
	SLDrawBorder(pathList);
	ClearBox(VIEW_RECT);
	ClearBox(INFO_RECT);
	SLDrawList(fileList);
}

/*
 *	Refresh window
 */

void RefreshWindow()
{
	WORD		num;
	RastPtr		rPort;
	Rectangle	rect;

	GetWindowRect(window, &rect);
	if (!EmptyRect(&rect)) {
/*
	If old color scheme, fill with _tbPenLight
	Note: Cannot call RefreshGadgets() within Begin/EndRefresh()
*/
		if (_tbPenLight != 0) {
			rPort = window->RPort;
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &rect);
			RefreshGadgets(GadgetItem(window->FirstGadget, 0), window, NULL);
		}
		BeginRefresh(window);
		DrawWindow();
		EndRefresh(window, TRUE);
		if ((num = SLNextSelect(fileList, -1)) != -1)
			ViewFile(num);			// Can't do this inside Begin/EndRefresh
	}
}

/*
 *	Return screen titles to default setting
 */

void FixTitle()
{
	SetWindowTitles(window, (TextPtr) -1, screenTitle);
	titleChanged = FALSE;
}

/*
 *  Error report routine
 *  Put error message on screen title bar
 */

void Error(register WORD errNum)
{
	if (errNum > ERR_MAX_ERROR)
		errNum = ERR_UNKNOWN;
	dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = errMessage[errNum];

	if (StdAlert(dlgList[DLG_ERROR], window->WScreen, mainMsgPort, DialogFilter) == -1) {
		SetWindowTitles(window, (UBYTE *) -1, errMessage[errNum]);
   		DisplayBeep(window->WScreen);
    	SysBeep(5);
		titleChanged = TRUE;
	}
}

/*
 *  Directory Error report routine
 *  Put error message on screen title bar if not enough memory
 */

void DirError(TextPtr dirName)
{
	WORD 		startPos, endPos, numLines;
	RastPort	rPort = _tbScreen->RastPort;
	TextChar	strBuff[MAX_FILENAME_LEN + 256], fullName[MAX_FILENAME_LEN];

/*
	Get full path name
*/
	NameFromLock1(GetCurrentDir(), fullName, MAX_FILENAME_LEN);
	if (fullName[strlen(fullName) - 1] != ':')
		strcat(fullName, "/");
	strcat(fullName, dirName);
/*
	Make directory path fit in dialog
*/
	startPos = numLines = 0;
	while (TextLength(&rPort, fullName + startPos, strlen(fullName) - startPos) >= ERROR_WIDTH - 75) {
		endPos = strlen(fullName);
		while (TextLength(&rPort, fullName + startPos, endPos - startPos) >= ERROR_WIDTH - 75)
			endPos--;
		while (endPos > startPos && fullName[endPos - 1] != '/')
			endPos--;
		BlockMove(fullName + endPos, fullName + endPos + 1, strlen(fullName) - endPos + 1);
		fullName[endPos] = '\n';
		startPos = endPos + 1;
		numLines++;
	}
/*
	Display error message
*/
	strcpy(strBuff, errMessage[ERR_DIR_ACC]);
	strcat(strBuff, fullName);

	dlgList[DLG_ERROR]->Height += numLines*15;
	dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = strBuff;

	if (StdAlert(dlgList[DLG_ERROR], window->WScreen, mainMsgPort, DialogFilter) == -1) {
		SetWindowTitles(window, (UBYTE *) -1, errMessage[ERR_DIR_ACC]);
   		DisplayBeep(window->WScreen);
    	SysBeep(5);
		titleChanged = TRUE;
	}
	dlgList[DLG_ERROR]->Height -= numLines*15;
}

void DoHelp()
{
	DialogPtr	dlg;

	if ((dlg = GetDialog(dlgList[DLG_ABOUT], window->WScreen, mainMsgPort)) == NULL)
		Error(ERR_NO_MEM);
	else {
		OutlineButton(GadgetItem(dlg->FirstGadget,0), dlg, NULL, TRUE);
		(void) ModalDialog(mainMsgPort, dlg, DialogFilter);
		DisposeDialog(dlg);
	}
}

/*
 *	Filter function for ModalDialog/ModalRequest
 *	Handles window updates
 */

BOOL DialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	RequestPtr	req;

	if (intuiMsg->IDCMPWindow == window && intuiMsg->Class == REFRESHWINDOW) {
		ReplyMsg((MsgPtr) intuiMsg);
		if ((req = window->FirstRequest) != NULL)
			OutlineButton(GadgetItem(req->ReqGadget, OK_BUTTON), window, req, TRUE);
		RefreshWindow();
		*item = -1;
		return (TRUE);
	}
	return (FALSE);
}
