/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *  FindFile
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *  Display procedures
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <dos/dos.h>
#include <graphics/gfxmacros.h>

#include <proto/graphics.h>
#include <proto/dos.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Window.h>
#include <Toolbox/DateTime.h>
#include <Toolbox/Utility.h>
#include <Toolbox/DOS.h>

#include <string.h>

#include "FindFile.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScrollListPtr	fileList;
extern WORD				numFiles;
extern ScrollListPtr	pathList;
extern WindowPtr 		window;

extern TextChar strDate[];
extern TextChar strSize[];
extern TextChar strComment[];
extern TextChar strBytes[];

/*
 *	Local variables and definitions
 */

#define MIN(a,b)	((a)<(b)?(a):(b))

static TextAttr	topaz8Attr = {
	"topaz.font", 8, FS_NORMAL, FPF_ROMFONT | FPF_DISKFONT | FPF_PROPORTIONAL
};

typedef struct LockNode {
	Dir				Lock;
	struct LockNode	*Next;
} LockNode, *LockNodePtr;

LockNodePtr		fileLocks;			// list of locks on parent dirs

/*
 *	Local prototypes
 */

static void	DisplayInfoBox(TextPtr, Dir);
static void PathDrawProc(RastPtr, TextPtr, RectPtr);
static void	DisplayPath(TextPtr, Dir);
static void RemoveControlChars(TextPtr, WORD);
static void DrawString(RastPtr, TextPtr, WORD);
static void FillViewBox(TextPtr, Dir);

/*
 *	Clear information box
 */

void ClearBox(WORD gadgetNum)
{
	Rectangle	rect;

	GetGadgetRect(GadgetItem(window->FirstGadget, gadgetNum), window, NULL, &rect);
	DrawShadowBox(window->RPort, &rect, -1, FALSE);
	SetAPen(window->RPort, _tbPenLight);
	SetDrMd(window->RPort, JAM1);
	FillRect(window->RPort, &rect);
}

/*
 *	Display file information for file
 */

static void DisplayInfoBox(TextPtr fileName, Dir parentDir)
{
	register WORD	i, j, left, top, len, frac;
	WORD		fontHeight;
	Dir			origDir, lock;
	struct FileInfoBlock	*fib;
	RastPtr		rPort = window->RPort;
	RegionPtr	oldRgn;
	Rectangle 	rect;
	TextChar	strBuff[256], tempBuff[40];

	if ((fib = MemAlloc(sizeof(struct FileInfoBlock), 0)) == NULL)
		return;

	oldRgn = GetClip(window->WLayer);
	origDir = CurrentDir(parentDir);
	if ((lock = Lock(fileName,ACCESS_READ)) == NULL)
		goto Exit;
	if (!Examine(lock,fib))
		goto Exit;

	GetGadgetRect(GadgetItem(window->FirstGadget, INFO_RECT), window, NULL, &rect);
	SetRectClip(window->WLayer, &rect);
	left = rect.MinX + 5;
	top = rect.MinY;
	fontHeight = rPort->Font->tf_Baseline + 2;
/*
	Draw titles
*/
	SetAPen(rPort, _tbPenBlack);
	SetDrMd(rPort, JAM1);

	SetSoftStyle(rPort, FSF_BOLD, 0xFF);
	Move(rPort, left, top + fontHeight);		// first line
	Text(rPort, strDate, strlen(strDate));
	Move(rPort, left, top + 2*fontHeight);		// 2nd line
	Text(rPort, strSize, strlen(strSize));
	Move(rPort, left, top + 3*fontHeight);		// 3rd line
	Text(rPort, strComment, strlen(strComment));
/*
	Draw info
*/
	left = rect.MaxX - 5;
	SetSoftStyle(rPort, FS_NORMAL, 0xFF);
	DateString(&(fib->fib_Date), DATE_ABBR, strBuff);
	strcat(strBuff,", ");
	TimeString(&(fib->fib_Date), FALSE, TRUE, tempBuff);
	strcat(strBuff,tempBuff);
	Move(rPort,left - TextLength(rPort,strBuff,strlen(strBuff)),
							top + fontHeight);		// 1st line	 date and time
	Text(rPort, strBuff, strlen(strBuff));

	NumToString(fib->fib_Size, strBuff);
	len = strlen(strBuff);
	strcpy(tempBuff, strBuff);
	if ((frac = len % 3) != 0)
		BlockMove(tempBuff, strBuff, frac);
	j = frac;
	for (i = frac; i < len; i += 3) {
		if (frac || i > frac)
			strBuff[j++] = ',';
		BlockMove(tempBuff + i, strBuff + j, 3);
		j += 3;
	}
	strBuff[j] = '\0';
	strcat(strBuff, strBytes);
	Move(rPort,left - TextLength(rPort, strBuff, strlen(strBuff)),
							top + 2*fontHeight);		// 2nd line size
	Text(rPort, strBuff, strlen(strBuff));

	Move(rPort, rect.MinX + 15,top + 4*fontHeight);		// 4th line comment
	Text(rPort, fib->fib_Comment, strlen(fib->fib_Comment));
/*
	All done
*/
Exit:
	CurrentDir(origDir);
	if (lock)
		UnLock(lock);
	if (fib)
		MemFree(fib, sizeof(struct FileInfoBlock));
	SetClip(window->WLayer, oldRgn);
}

/*
 *	Clear scroll list
 */

void ClearScrollList(ScrollListPtr scrollList)
{
	LockNodePtr	node, nextNode;

	if (scrollList == fileList) {
		for (node = fileLocks; node; node = nextNode) {
			nextNode = node->Next;
			UnLock(node->Lock);
			node->Lock = NULL;
			MemFree(node, sizeof(LockNode));
		}
		numFiles = 0;
		fileLocks = NULL;
		ShowNumber(numFiles);
	}
	SLRemoveAll(scrollList);
}

/*
 *	Show number of files found
 */

void ShowNumber(WORD num)
{
	TextChar	numText[10];

	numText[0] = '\0';
	if (num != 0)
		NumToString(num, numText);
	SetGadgetItemText(window->FirstGadget, NUMFILES, window, NULL, numText);
}

/*
 *	Display file name in scrollList
 */

BOOL DisplayFile(TextPtr fileName, Dir dir)
{
	LockNodePtr	newNode, node;

/*
	Add directory lock to lock list
*/
	if ((newNode = MemAlloc(sizeof(LockNode), MEMF_CLEAR)) == NULL) {
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	newNode->Lock = DupLock(dir);
	if (numFiles) {
		for (node = fileLocks; node->Next; node = node->Next) ;
		node->Next = newNode;
	}
	else
		fileLocks = newNode;
/*
	Add file name to list
*/
	SLAddItem(fileList, fileName, strlen(fileName), numFiles);
	numFiles++;
	ShowNumber(numFiles);
/*
	All done
*/
	return (TRUE);
}

/*
 *	FindFile custom draw procedure
 */

static void PathDrawProc(RastPtr rPort, register TextPtr text, RectPtr rect)
{
	register WORD	levels;

	levels = *text++;
	Move(rPort, rPort->cp_x + levels*TextLength(rPort, " ", 1), rPort->cp_y);
	TextInWidth(rPort, text, strlen(text), rect->MaxX - rPort->cp_x + 1, FALSE);
}

/*
 *	Display the path of filename
 */

static void DisplayPath(TextPtr fileName, Dir dir)
{
	UWORD		i, len, levels;
	TextChar	name[35], pathName[MAX_FILENAME_LEN];

/*
	Set up scroll list
*/
	SLDoDraw(pathList, FALSE);
	SLSetDrawProc(pathList, PathDrawProc);
/*
	Get full path to file
*/
	NameFromLock1(dir, pathName, MAX_FILENAME_LEN);
	if (pathName[strlen(pathName) - 1] != ':')
		strcat(pathName, "/");
/*
	Add path name to scroll list
*/
	levels = 0;
	len = 1;
	for (i = 0; i < strlen(pathName); i++) {
		name[len++] = pathName[i];
		if (pathName[i] == '/' || pathName[i] == ':') {
			name[len] = '\0';
			name[0] = levels++;
			SLAddItem(pathList, name, len, SLNumItems(pathList));
			len = 1;
		}

	}
/*
	Add file name to scroll list
*/
	name[0] = levels;
	strcpy(name + 1, fileName);
	SLAddItem(pathList, name, strlen(fileName) + 1, SLNumItems(pathList));
	SLDoDraw(pathList, TRUE);
}

/*
 *	Change all control chars to '.'
 */

static void RemoveControlChars(TextPtr buffer, WORD len)
{
	register WORD		i;
	register TextChar	ch;

	for (i = 0; i < len; i++) {
		ch = buffer[i];
		if ((ch & 0x7F) >= 0x00 && (ch & 0x7F) <= 0x1F && ch != '\n')
			buffer[i] = '.';
	}
}

/*
 *	Draw text with word and line breaks
 *	Note: Assumes font is topaz-8
 */

static void DrawString(RastPtr rPort, register TextPtr text, WORD maxWidth)
{
	register WORD	pos, startPos, maxPos;
	WORD			x, y, len;
	BOOL			foundEOL;

	x = rPort->cp_x;
	y = rPort->cp_y;
/*
	Display text one line at a time
*/
	len = strlen(text);
	startPos = pos = 0;
	while (startPos < len) {
		maxPos = startPos + maxWidth/8;
		if (maxPos > len)
			maxPos = len;
/*
	Search for word or line breaks
*/
		foundEOL = FALSE;
		for (pos = startPos; pos < maxPos; pos++) {	// Search for line feed
			if (text[pos] == '\n') {
				foundEOL = TRUE;
				break;
			}
		}
		if (!foundEOL) {							// No line feed found
			for (pos = maxPos; pos > startPos; pos--) {	// Search for space
				if (text[pos - 1] == ' ')
					break;
			}
			if (pos == startPos)					// No space found
				pos = maxPos;
		}
/*
	Draw line
*/
		if (pos > startPos) {
			Move(rPort, x, y);
			Text(rPort, text + startPos, pos - startPos);
		}
		y += 8;
		startPos = pos;
		if (foundEOL)
			startPos++;
	}
}

/*
 *	Read in a section of the file and put in viewList
 */

static void FillViewBox(TextPtr fileName, Dir dir)
{
	WORD		numChars, len,left,top,pos,numLines,buffSize;
	File		file;
	Dir			origDir;
	TextPtr		buffer;
	Rectangle	rect;
	TextFontPtr oldFont, font = NULL;
	RastPtr		rPort = window->RPort;
	RegionPtr	oldRgn;

	oldRgn = GetClip(window->WLayer);
	origDir = CurrentDir(dir);
/*
	Determine how many lines to read
*/
	GetGadgetRect(GadgetItem(window->FirstGadget, VIEW_RECT), window, NULL, &rect);
	SetRectClip(window->WLayer, &rect);
	left = rect.MinX + 2;
	top = rect.MinY;
	pos = 0;
	numChars = (rect.MaxX - left - 2)/8;
	numLines = ((rect.MaxY - top)/8) + 1;
/*
	Allocate buffer
*/
	buffSize = numChars*numLines;
	if ((buffer = MemAlloc(buffSize, MEMF_CLEAR)) == NULL) {
		Error(ERR_NO_MEM);
		goto Exit;
	}
/*
	Open and read file
*/
	if ((file = Open(fileName, MODE_OLDFILE)) == NULL)
		goto Exit;
	len = Read(file, buffer, buffSize);
	Close(file);
	file = NULL;
	if (len <= 0)
		goto Exit;
/*
	Display
*/
	RemoveControlChars(buffer, len);
	if ((font = GetFont(&topaz8Attr)) == NULL)
		goto Exit;
	oldFont = rPort->Font;
	SetFont(rPort, font);
	SetAPen(rPort, _tbPenBlack);
	SetDrMd(rPort, JAM1);
	Move(rPort, left, top + font->tf_Baseline + 1);
	DrawString(rPort, buffer, numChars*8);
/*
	Clean up
*/
Exit:
	if (buffer)
		MemFree(buffer,buffSize);
	if (font) {
		CloseFont(font);
		SetFont(rPort, oldFont);
	}
	CurrentDir(origDir);
	SetClip(window->WLayer, oldRgn);
}

/*
 *	Display path and contents of file
 */

void ViewFile(WORD num)
{
	TextChar 	fileName[30];
	Dir		 	dir;
	WORD	 	i;
	LockNodePtr	node;

/*
	Clear existing displays
*/
	ClearScrollList(pathList);
	ClearBox(VIEW_RECT);
	ClearBox(INFO_RECT);
/*
	Get file to display and lock on directory
*/
	SLGetItem(fileList, num, fileName);

	for (i = 0, node = fileLocks; i < num && node; i++, node = node->Next) ;
	if (node)
		dir = DupLock(node->Lock);
	if (node == NULL || dir == NULL) {
		Error(ERR_VIEW);
		return;
	}
/*
	Display information
*/
	DisplayInfoBox(fileName, dir);
	DisplayPath(fileName, dir);
	FillViewBox(fileName, dir);
	if (dir)
		UnLock(dir);
}
