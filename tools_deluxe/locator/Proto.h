/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *  FindFile
 *
 *  Global function Prototypes
 */

#include <Toolbox/ScrollList.h>

/*
 *  Main.c
 */

void 	main(int,char **);

/*
 *  Init.c
 */

void 	Init(int, char **);
void 	ShutDown(void);

/*
 *  Window.c
 */

void 	CreateWindow(void);
void	RemoveWindow(void);
void 	RefreshWindow(void);
void 	FixTitle(void);
void	Error(WORD);
void	DirError(TextPtr);
void	DoHelp(void);
BOOL	DialogFilter(IntuiMsgPtr, WORD *);

/*
 *  Display.c
 */

void	ClearBox(WORD);
void	ClearScrollList(ScrollListPtr);
void 	ShowNumber(WORD);
BOOL 	DisplayFile(TextPtr,Dir);
void 	ViewFile(WORD);

/*
 *	Disk.c
 */

void	DisposeDiskList(void);
void	DoDiskInserted(void);
BOOL	CheckDiskList(void);
void	DoDiskMenu(UWORD);

/*
 *	Eyes.c
 */

void	GetSearchIndic(void);
void	DisposeSearchIndic(void);
void	OnOffSearchIndic(BOOL);
void	UpdateSearchIndic(void);
