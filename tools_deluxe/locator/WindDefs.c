/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Find File
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Window routines
 */

#include <exec/types.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Image.h>
#include <Toolbox/ScrollList.h>

#include "Proto.h"
#include "FindFile.h"

/*
 *	External variables
 */

extern TextChar strFilesFound[];

/*
 *	Main dialog window definition
 */

static TextPtr	patternOptList[] = {
	"contains","starts with", "ends with", "is", "is not", "doesn't contain", "", "matches pattern", NULL };

#define INFO_BOX(left, top, width, height)	\
	{ GADG_ACTIVE_BORDER, left, top, 0, 0, width, height, 0, 0, NULL }

#define FIRST_LEVEL  	75
#define SECOND_LEVEL	FIRST_LEVEL + 25 + INFO_HEIGHT
#define THIRD_LEVEL 	FIRST_LEVEL + 35 + 11*FILE_NUM

#define WINDOW_WIDTH	20 + FILE_WIDTH + 20 + INFO_WIDTH + 20
#define WINDOW_HEIGHT	SECOND_LEVEL + 10 + VIEW_HEIGHT

GadgetTemplate windowGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, 'F', 0, "Find" },

	{ GADG_EDIT_TEXT, 220, 10, 0, 0, WINDOW_WIDTH-320, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT, 220, 30, 0, 0, WINDOW_WIDTH-320, 11, 0, 0, 0, 0, NULL },

	{ GADG_POPUP,      70, 10, 0, 0, 130, 11, 0, 0, 0, 0, &patternOptList },
/*
	file list
*/
	SL_GADG_BOX(20,FIRST_LEVEL,FILE_WIDTH,FILE_NUM),
	SL_GADG_UPARROW(20,FIRST_LEVEL,FILE_WIDTH,FILE_NUM),
	SL_GADG_DOWNARROW(20,FIRST_LEVEL,FILE_WIDTH,FILE_NUM),
	SL_GADG_SLIDER(20,FIRST_LEVEL,FILE_WIDTH,FILE_NUM),

/*
	contents box
*/
	INFO_BOX(20 + FILE_WIDTH + 20, SECOND_LEVEL, VIEW_WIDTH, VIEW_HEIGHT),

/*
	path list
*/
	SL_GADG_BOX(20,THIRD_LEVEL,PATH_WIDTH,PATH_NUM),
	SL_GADG_UPARROW(20,THIRD_LEVEL,PATH_WIDTH,PATH_NUM),
	SL_GADG_DOWNARROW(20,THIRD_LEVEL,PATH_WIDTH,PATH_NUM),
	SL_GADG_SLIDER(20,THIRD_LEVEL,PATH_WIDTH,PATH_NUM),
/*
	info box
*/
	INFO_BOX(20 + FILE_WIDTH + 20, FIRST_LEVEL, INFO_WIDTH, INFO_HEIGHT),

/*
	search indicator
 */
	{ GADG_STAT_IMAGE,  -80, 40, 0, 0,  60, 25, 0, 0, 0, 0, NULL },
/* number of files found */

	{ GADG_STAT_TEXT, 125, FIRST_LEVEL-15, 0, 0,0, 0, 0, 0,0, 0, NULL },
	{ GADG_STAT_TEXT, 20, FIRST_LEVEL-15, 0, 0,0, 0, 0, 0,0, 0,	strFilesFound },
	{ GADG_STAT_TEXT, 20, 10, 0, 0,0, 0, 0, 0,0, 0, "Name:  " },
	{ GADG_STAT_TEXT, 20, 30, 0, 0,0, 0, 0, 0,0, 0, "Search here (optional): " },

	{ GADG_STAT_TEXT, 20+PATH_WIDTH+20, SECOND_LEVEL-15, 0, 0,0, 0, 0, 0,0, 0, "Contents " },
	{ GADG_STAT_TEXT, 20, THIRD_LEVEL-15, 0, 0,0, 0, 0, 0,0, 0, "Path " },
	{ GADG_STAT_TEXT, 20+FILE_WIDTH+20, FIRST_LEVEL-15, 0, 0,0, 0, 0, 0,0, 0, "Information" },

	{ GADG_ITEM_NONE}
};

/*
 *	Dialog template for main window
 */

DialogTemplate mainDialog = {
	DLG_TYPE_WINDOW, DLG_FLAG_CLOSE | DLG_FLAG_DEPTH | DLG_FLAG_ZOOM,
	-1, -1, WINDOW_WIDTH, WINDOW_HEIGHT, windowGadgets, "Locator"
};

/*
 *  Error dialog
 */

static GadgetTemplate errorGadgets[] = {
	{ GADG_PUSH_BUTTON, -90, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT, 55, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 55, 30, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },

	{ GADG_ITEM_NONE }
};

DialogTemplate errorDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 370, 80, errorGadgets, NULL
};

/*
 *  About dialog
 */

static GadgetTemplate aboutGadgets[] = {
	{ GADG_PUSH_BUTTON, 100, -30, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },

	{ GADG_STAT_TEXT, 86, 15, 0, 0, 0, 0, 0, 0, 0, 0, "Locator 1.0" },
	{ GADG_STAT_TEXT, 42, 40, 0, 0, 0, 0, 0, 0, 0, 0, "Designed and developed" },
	{ GADG_STAT_TEXT, 78, 55, 0, 0, 0, 0, 0, 0, 0, 0, "by Beth Henry" },
	{ GADG_STAT_TEXT, 66, 80, 0, 0, 0, 0, 0, 0, 0, 0, "Copyright \251 1993" },
	{ GADG_STAT_TEXT, 42, 95, 0, 0, 0, 0, 0, 0, 0, 0, "Central Coast Software" },
	{ GADG_STAT_TEXT, 78,110, 0, 0, 0, 0, 0, 0, 0, 0, "A division of" },
	{ GADG_STAT_TEXT, 22,125, 0, 0, 0, 0, 0, 0, 0, 0, "New Horizons Software, Inc."},
	{ GADG_STAT_TEXT, 54,140, 0, 0, 0, 0, 0, 0, 0, 0, "All Rights Reserved" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboutDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 195, aboutGadgets, NULL
};
/*
 *	Dialog list
 */

DlgTemplPtr	dlgList[] = {
	&mainDialog,
	&errorDialog,
	&aboutDialog
};