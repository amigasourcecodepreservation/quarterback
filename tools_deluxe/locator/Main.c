/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	FindFile
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Main routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <dos/dos.h>

#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/intuition.h>

#include <Typedefs.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/DOS.h>

#include <string.h>

#include "Proto.h"
#include "FindFile.h"

/*
 *	External variables
 */

extern ScrollListPtr	fileList;
extern ScrollListPtr	pathList;
extern WORD				numFiles;		// number of files
extern WindowPtr 		window;
extern MsgPortPtr		mainMsgPort;
extern TextChar			volumeName[30];

extern TextChar	strFind[], strStop[];

extern BOOL	titleChanged;

/*
 *	Local variables and definitions
 */

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)
#define RAWKEY_HELP	0x5F

static TextChar	pattern[MAX_FILENAME_LEN], path[MAX_FILENAME_LEN];
static WORD		patType;

typedef struct FileInfoBlock	FIB, *FIBPtr;

static BOOL		quitFlag, searching, abort;		// Inited to FALSE

static TextChar	patKeyEquivs[] = {		// Indexed by pop-up item number
	CHR_CONTAINS,	CHR_STARTS,			CHR_ENDS,	CHR_IS,
	CHR_ISNOT,		CHR_NOTCONTAIN,		0,			CHR_PATTERN
};

/*
 *	Prototypes
 */

static void DoIntuiMessage(IntuiMsgPtr);
static void FindFile(TextPtr);
static void SetUpPattern(void);
static void DoFind(void);
static void DoMenu(UWORD);
static void DoMainDialogItem(WORD);

/*
 *	Main routine
 */

void main(int argc, char *argv[])
{
	register IntuiMsgPtr	intuiMsg;
	register Dir			dir;
	GadgetPtr				gadgList;

	Init(argc, argv);
	gadgList = window->FirstGadget;
/*
	Set initial settings
*/
	numFiles = 0;
	SetEditItemText(gadgList, PATH_TEXT, window, NULL, volumeName);
	DoDiskInserted();				// Get initial disk list
/*
	If started from CLI, get parameters and start
*/
	if (argc == 2 || argc == 3) {
		HiliteGadget(GadgetItem(gadgList, NAME_TEXT), window, NULL, FALSE);
		SetEditItemText(gadgList, NAME_TEXT, window, NULL, argv[1]);
		if (argc == 3) {
			dir = Lock(argv[2], ACCESS_READ);
			if (dir) {
				NameFromLock1(dir, path, MAX_FILENAME_LEN);
				SetEditItemText(gadgList, PATH_TEXT, window, NULL, path);
				UnLock(dir);
			}
		}
		DoFind();
	}
/*
	Get and process events
*/
	quitFlag = FALSE;
	while (!quitFlag) {
		while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort))
			DoIntuiMessage(intuiMsg);
		if (!quitFlag)
			Wait(1L << mainMsgPort->mp_SigBit);
	}
	ShutDown();
}

/*
 *	Handle gadget messages in scroll box
 */

static BOOL MainDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register ULONG 	class = intuiMsg->Class;
	register UWORD 	code = intuiMsg->Code;
	register UWORD 	qualifier = intuiMsg->Qualifier;
	register APTR  	iAddress = intuiMsg->IAddress;
	WORD			gadgNum, oldNum, newNum;
	BOOL			msgHandled;


	oldNum = SLNextSelect(fileList, -1);
	*item = -1;
	msgHandled = FALSE;

	switch (class) {
	case GADGETDOWN:
	case GADGETUP:
		gadgNum = GadgetNumber(iAddress);
		switch (gadgNum) {
		case LIST_BOX:
		case LIST_UP:
		case LIST_DOWN:
		case LIST_SCROLL:
			SLGadgetMessage(fileList, mainMsgPort, intuiMsg);
			newNum = SLNextSelect(fileList, -1);
			if (newNum != oldNum)
				*item = gadgNum;
			msgHandled = TRUE;
			break;
		case PATH_BOX:
		case PATH_UP:
		case PATH_DOWN:
		case PATH_SCROLL:
			SLGadgetMessage(pathList, mainMsgPort, intuiMsg);
			newNum = SLNextSelect(fileList, -1);
			if (newNum != oldNum)
				*item = gadgNum;
			msgHandled = TRUE;
			break;
		}
		break;
	case RAWKEY:
		switch (code) {
		case CURSORUP:
		case CURSORDOWN:
			ReplyMsg((MsgPtr) intuiMsg);
			if (qualifier & SHIFTKEYS) {
				SLCursorKey(pathList, code);
				*item = PATH_BOX;
			}
			else {
				SLCursorKey(fileList, code);
				newNum = SLNextSelect(fileList, -1);
				if (newNum != oldNum)
					*item = LIST_BOX;
			}
			msgHandled = TRUE;
			break;
		}
		break;
	case VANILLAKEY:
		if (searching)
			break;
		newNum = -1;
		code = toUpper[code];
		for (newNum = CONTAINS_ITEM; newNum <= PATTERN_ITEM; newNum++) {
			if (code == patKeyEquivs[newNum])
				break;
		}
		if (newNum <= PATTERN_ITEM) {
			ReplyMsg((MsgPtr) intuiMsg);
			SetGadgetItemValue(window->FirstGadget, PATTERN_POPUP, window, NULL, newNum);
			msgHandled = TRUE;
		}
		break;
	}
	return (msgHandled);
}

static void DoIntuiMessage(register IntuiMsgPtr intuiMsg)
{
	register ULONG	class;
	register UWORD	code;
	WORD 			itemHit;
	GadgetPtr		gadgList = window->FirstGadget;
	TextChar		strBuff[GADG_MAX_STRING];

	if (intuiMsg->Class == RAWKEY)
		ConvertKeyMsg(intuiMsg);	// Convert some RAWKEY to VANILLAKEY
	class = intuiMsg->Class;
	code = intuiMsg->Code;

	if (titleChanged)
		FixTitle();
/*
	Process the message
*/
	if (MainDialogFilter(intuiMsg, &itemHit))
		DoMainDialogItem(itemHit);
	else if (DialogSelect(intuiMsg, &window, &itemHit)) {
		ReplyMsg((MsgPtr) intuiMsg);
		DoMainDialogItem(itemHit);
	}
	else {
		ReplyMsg((MsgPtr) intuiMsg);
		switch (class) {
		case MENUPICK:
			DoMenu(code);
			break;
		case DISKINSERTED:
		case DISKREMOVED:
		case INTUITICKS:
			if (searching)
				break;
			if (!CheckDiskList())
				DoDiskInserted();
			GetEditItemText(gadgList, NAME_TEXT, strBuff);
			EnableGadgetItem(gadgList, FIND_BUTTON, window, NULL, (strBuff[0] != '\0'));
			break;
		case RAWKEY :
			if (code == RAWKEY_HELP)
				DoHelp();
			break;
		case REFRESHWINDOW:
			RefreshWindow();
			break;
		}
	}
}

/*
 *	Recursively search directory for matching files
 */

static void FindFile(TextPtr dirName)
{
	WORD		len1, len2;
	BOOL		match;
	TextPtr		fileName;
	Dir			origDir, dir;
	FIBPtr		fib;
	IntuiMsgPtr	intuiMsg;

/*
	Get lock on path
*/
	if ((dir = Lock(dirName, ACCESS_READ)) == NULL) {
		DirError(dirName);
		return;
	}
/*
	Switch to directory and scan
*/
	if ((fib = AllocMem(sizeof(FIB), MEMF_CLEAR)) == NULL)
		Error(ERR_NO_MEM);
	else {
		if (!Examine(dir, fib) || fib->fib_DirEntryType <= 0)
			DirError(dirName);
		else {
			origDir = CurrentDir(dir);
			len1 = strlen(pattern);
			fileName = fib->fib_FileName;
			while (!quitFlag && ExNext(dir, fib)) {
				if (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort))
					DoIntuiMessage(intuiMsg);
				if (abort)
					break;
				if (fib->fib_DirEntryType > 0)
					FindFile(fileName);
				else {
					switch (patType) {
					case IS_ITEM:
					case ISNOT_ITEM:
						len2 = strlen(fileName);
						match = (len1 == len2 &&
								 CmpString(pattern, fileName, len1, len2, FALSE) == 0);
						if (patType == ISNOT_ITEM)
							match = !match;
						break;
					default:
						match = MatchFilename(pattern, fileName);
						if (patType == NOTCONTAIN_ITEM)
							match = !match;
						break;
					}
					if (match)
						(void) DisplayFile(fileName, dir);
				}
				UpdateSearchIndic();
			}
			(void) CurrentDir(origDir);
		}
		FreeMem(fib, sizeof(FIB));
	}
/*
	All done
*/
	UnLock(dir);
}

/*
 *	Set pattern and patType globals from user entries
 */

static void SetUpPattern()
{
	GadgetPtr	gadgList = window->FirstGadget;
	TextChar	wildCard[] = "#?";
	TextChar	strBuff[GADG_MAX_STRING];

/*
	Get pattern type and user's pattern
*/
	patType = GetGadgetValue(GadgetItem(gadgList, PATTERN_POPUP));
	GetEditItemText(gadgList, NAME_TEXT, strBuff);
/*
	Convert pattern
*/
	switch (patType) {
	case CONTAINS_ITEM:
	case NOTCONTAIN_ITEM:
		strcpy(pattern, wildCard);
		strcat(pattern, strBuff);
		strcat(pattern, wildCard);
		break;
	case STARTS_ITEM:
		strcpy(pattern, strBuff);
		strcat(pattern, wildCard);
		break;
	case ENDS_ITEM:
		strcpy(pattern, wildCard);
		strcat(pattern, strBuff);
		break;
	default:						// IS_ITEM, ISNOT_ITEM, or PATTERN_ITEM
		strcpy(pattern, strBuff);
		break;
	}
}

/*
 *	Perform find file function
 */

static void DoFind()
{
	GadgetPtr	gadgList = window->FirstGadget;

/*
	Set up for find
*/
	SetStdPointer(window, POINTER_WAITDELAY);
	EnableGadgetItem(gadgList, PATTERN_POPUP, window, NULL, FALSE);
	SetUpPattern();
	GetEditItemText(gadgList, PATH_TEXT, path);
	ClearScrollList(fileList);
	ClearScrollList(pathList);
	ClearBox(VIEW_RECT);
	ClearBox(INFO_RECT);
	SetButtonItem(gadgList, FIND_BUTTON, window, NULL, strStop, CHR_STOP);
	searching = TRUE;
	abort = FALSE;
/*
	Do find
*/
	SetStdPointer(window, POINTER_ARROW);
	OnOffSearchIndic(TRUE);
	FindFile(path);
	OnOffSearchIndic(FALSE);
/*
	All done
*/
	SetButtonItem(window->FirstGadget, FIND_BUTTON, window, NULL, strFind, CHR_FIND);
	EnableGadgetItem(gadgList, PATTERN_POPUP, window, NULL, TRUE);
	searching = FALSE;
	ShowNumber(numFiles);			// Make sure it's up to date
	if (!abort) {
		if (numFiles)
			SysBeep(5);
		else
			Error(ERR_NO_FILE);
	}
}

/*
 *	Handle menu command
 */

static void DoMenu(UWORD menuNumber)
{
	register UWORD	item, sub;

	while (menuNumber != MENUNULL) {
		sub = SUBNUM(menuNumber);
		item = ITEMNUM(menuNumber);
		switch (item) {
		case DISK_ITEM:
			if (ItemAddress(window->MenuStrip, menuNumber) == NULL)
				return;				// Menus have changed
			DoDiskMenu(sub);
			break;
		case ABOUT_ITEM:
			DoHelp();
			break;
		case QUIT_ITEM:
			quitFlag = TRUE;
			break;
		}
		menuNumber = ItemAddress(window->MenuStrip, menuNumber)->NextSelect;
	}
}

/*
 *	Handle main dialog items
 */

static void DoMainDialogItem(WORD item)
{
	WORD	fileNum;

	switch (item) {
	case DLG_CLOSE_BOX:
		quitFlag = TRUE;
		if (searching)
			abort = TRUE;
		break;
	case FIND_BUTTON:
		if (searching)			// "Find" button is actually "Stop" button
			abort = TRUE;
		else
			DoFind();
		break;
	case LIST_BOX:
		if ((fileNum = SLNextSelect(fileList, -1)) != -1)
			ViewFile(fileNum);
		break;
	case PATTERN_POPUP:
		ActivateGadget(GadgetItem(window->FirstGadget, NAME_TEXT), window, NULL);
		break;
	}
}