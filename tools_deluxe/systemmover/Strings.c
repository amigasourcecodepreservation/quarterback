/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright (c) 1987-93 New Horizons Software, Inc.
 *
 *	Text strings
 */

#include <exec/types.h>

#include <Typedefs.h>
#include <Toolbox/Language.h>

TextChar	strVER[] = "$VER: SystemMover 2.4";	// Also used for "$VER:" comparison

#if (AMERICAN | BRITISH)

TextChar	strScreenTitle[] = " System Mover 2.4 - \251 1987-93 New Horizons Software, Inc.";

TextPtr strInitError[] = {
	"Not enough memory.",
	"Can't open window."
};

TextChar strSampleText[] = "The quick brown fox jumps over the lazy dog.";

TextChar strSelectOptions[] = "Select options from the menus.";

TextChar strName[]		= "Name: ";
TextChar strVersion[]	= "Version: ";
TextChar strRevision[]	= "Revision: ";
TextChar strSize[]		= "Size: ";
TextChar strCreated[]	= "Created: ";
TextChar strBytes[]		= " bytes";

#elif GERMAN

TextChar	strScreenTitle[] = " System Mover DE 2.4 - \251 1987-93 New Horizons Software, Inc.";

TextPtr strInitError[] = {
	"Not enough memory.",
	"Can't open window."
};

TextChar strSampleText[] = "The quick brown fox jumps over the lazy dog.";

TextChar strSelectOptions[] = "Select options from the menus.";

TextChar strName[]		= "Name: ";
TextChar strVersion[]	= "Version: ";
TextChar strRevision[]	= "Revision: ";
TextChar strSize[]		= "Size: ";
TextChar strCreated[]	= "Created: ";
TextChar strBytes[]		= " bytes";

#elif FRENCH

TextChar	strScreenTitle[] = " System Mover FR 2.4 - \251 1987-93 New Horizons Software, Inc.";

TextPtr strInitError[] = {
	"M�moire insuffisante.",
	"Ouverture fen�tre impossible."
};

TextChar strSampleText[] = "C'est fi�re et joviale que Zazi paye et monte dans le vieux...";

TextChar strSelectOptions[] = "Utilisez les options des menus.";

TextChar strName[]		= "Nom: ";
TextChar strVersion[]	= "Version: ";
TextChar strRevision[]	= "R�vision: ";
TextChar strSize[]		= "Taille: ";
TextChar strCreated[]	= "Cr�� le: ";
TextChar strBytes[]		= " octets";

#elif SWEDISH

TextChar	strScreenTitle[] = " Systemflyttaren SV 2.4 - \251 1987-93 New Horizons Software, Inc.";

TextPtr strInitError[] = {
	"Ej tillr�ckligt med minne.",
	"Kan ej �ppna f�nstret."
};

TextChar strSampleText[] = "P� lingonr�da tuvor upp� villande mo...";

TextChar strSelectOptions[] = "V�lj fr�n menyerna.";

TextChar strName[]		= "Namn: ";
TextChar strVersion[]	= "Version: ";
TextChar strRevision[]	= "Revision: ";
TextChar strSize[]		= "Storlek: ";
TextChar strCreated[]	= "Skapad: ";
TextChar strBytes[]		= " bytes";

#elif SPANISH

TextChar	strScreenTitle[] = " System Mover ES 2.4 - \251 1987-93 New Horizons Software, Inc.";

TextPtr strInitError[] = {
	"Memoria insuficiente.",
	"No puede abrir ventana."
};

TextChar strSampleText[] = "El respeto al derecho ajeno es la paz.";

TextChar strSelectOptions[] = "Seleccione opci�nes de men�s.";

TextChar strName[]		= "Nombre: ";
TextChar strVersion[]	= "Versi�n: ";
TextChar strRevision[]	= "Revisi�n: ";
TextChar strSize[]		= "Tama�o: ";
TextChar strCreated[]	= "Creados: ";
TextChar strBytes[]		= " bytes";

#endif
