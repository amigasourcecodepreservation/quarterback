/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Definitions
 */

/*
 *	Menu definitions
 */

enum {
	DISK_MENU = 0,	MOVE_MENU
};

enum {
	FROMDISK_ITEM = 0,	TODISK_ITEM,	QUIT_ITEM = 3
};

enum {
	FONTS_ITEM = 0,		PRINTERS_ITEM,	KEYMAPS_ITEM,
	LIBRARIES_ITEM = 4,	DEVICES_ITEM,	HANDLERS_ITEM,
	CLI_ITEM = 8,		SCRIPTS_ITEM
};

/*
 *	Gadget definitions
 */

enum {
	COPY_BUTTON = 0,	REMOVE_BUTTON,	LIST_BOX,	LIST_UP,
	LIST_DOWN,			LIST_SCROLL,	FROM_TEXT,	TO_TEXT
};

/*
 *	Init errors
 */

enum {
	INIT_BADSYS = 0,	INIT_NOMEM,	INIT_NOWINDOW
};

/*
 *	Misc definitions
 */

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)
