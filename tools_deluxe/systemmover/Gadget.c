/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Gadget routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>

#include "Mover.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr		mainMsgPort;
extern WindowPtr		window;
extern ScrollListPtr	scrollList;

extern Dir	fromLock, toLock;

extern ListHeadPtr	fileList;

/*
 *	Local variables and definitions
 */

#define VOLUME(lock)	\
	((struct DeviceList *) BADDR(((struct FileLock *) BADDR(lock))->fl_Volume))

static WORD	listOffset;

/*
 *	Disable all buttons
 */

void OffButtons()
{
	GadgetPtr gadgList = window->FirstGadget;

	EnableGadgetItem(gadgList, COPY_BUTTON, window, NULL, FALSE);
	EnableGadgetItem(gadgList, REMOVE_BUTTON, window, NULL, FALSE);
}

/*
 *	Enable/disable buttons in window
 */

void SetButtons()
{
	BOOL copy, remove;
	GadgetPtr gadgList = window->FirstGadget;

	remove = (fromLock && SLNextSelect(scrollList, -1) != -1);
	copy = (remove && toLock && VOLUME(fromLock) != VOLUME(toLock));
	EnableGadgetItem(gadgList, COPY_BUTTON, window, NULL, copy);
	EnableGadgetItem(gadgList, REMOVE_BUTTON, window, NULL, remove);
}

/*
 *	Disable menus
 */

void DisableInput()
{
	OffMenu(window, FULLMENUNUM(DISK_MENU, NOITEM, NOITEM));
	OffMenu(window, FULLMENUNUM(MOVE_MENU, NOITEM, NOITEM));
}

/*
 *	Enable menus
 */

void EnableInput()
{
	OnMenu(window, FULLMENUNUM(DISK_MENU, NOITEM, NOITEM));
	OnMenu(window, FULLMENUNUM(MOVE_MENU, NOITEM, NOITEM));
}

/*
 *	De-select all items in list
 */

void UnSelectAll()
{
	WORD i, num;

	num = SLNumItems(scrollList);
	for (i = 0; i < num; i++)
		SLSelectItem(scrollList, i, FALSE);
}
