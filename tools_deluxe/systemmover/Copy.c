/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Copy routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <libraries/diskfont.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>

#include "Mover.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr		window;
extern ScrollListPtr	scrollList;

extern TextFontPtr	topaz8Font;

extern Dir		fromLock, toLock;

extern ListHeadPtr	fileList;

extern WORD		moveType;
extern TextPtr	moveDirName;

/*
 *	Local variables and definitions
 */

#define IS_FILE(fib)	((fib)->fib_DirEntryType < 0)

typedef struct {
	ULONG					NextSegment;
	ULONG					ReturnCode;
	struct DiskFontHeader	DFH;
} FontData, *FontDataPtr;

#define BUFF_SIZE		1024

typedef struct _CopyBuff{
	struct _CopyBuff	*Next;
	WORD				Size;
	UBYTE				Buff[BUFF_SIZE];
} CopyBuff, *CopyBuffPtr;

#define FCH_SIZE	(sizeof(struct FontContentsHeader))
#define FC_SIZE		(sizeof(struct FontContents))

/*
 *	Local prototypes
 */

static BOOL	MakeDir(TextPtr);

static void			FreeCopyBuff(CopyBuffPtr);
static CopyBuffPtr	LoadFile(TextPtr);
static BOOL			SaveFile(TextPtr, CopyBuffPtr);
static BOOL			CopyFont(TextPtr);
static BOOL			CopyFile(TextPtr);

/*
 *	Build file name from font data file name
 */

void BuildName(register TextPtr fileName, register TextPtr extra,
			   register TextPtr pathName)
{
	while (*fileName != '/')
		*pathName++ = *fileName++;
	strcpy(pathName, extra);
}

/*
 *	Make directory with specified name if one does not already exist
 *	name could include multiple directories (e.g. "devs/printers")
 *	Return success status
 */

static BOOL MakeDir(register TextPtr name)
{
	register WORD i, len;
	register BOOL success;
	register BPTR lock;
	register struct FileInfoBlock *fib;

	success = FALSE;
	lock = NULL;
	if ((fib = AllocMem(sizeof(struct FileInfoBlock), 0)) == NULL)
		goto Exit;
	len = strlen(name);
	i = 0;
	do {
		while (name[i] != '/' && i < len)
			i++;
		name[i] = '\0';
		if ((lock = Lock(name, ACCESS_READ)) != NULL) {
			if (!Examine(lock, fib) || IS_FILE(fib))
				goto Exit;
		}
		else {
			if ((lock = CreateDir(name)) == NULL)
				goto Exit;
		}
		UnLock(lock);
		lock = NULL;
		if (i < len)
			name[i] = '/';
	} while (i++ < len);
	success = TRUE;
Exit:
	if (lock)
		UnLock(lock);
	if (fib)
		FreeMem(fib, sizeof(struct FileInfoBlock));
	return (success);
}

/*
 *	Free all buffers used by copy buffer list
 */

static void FreeCopyBuff(register CopyBuffPtr copyBuff)
{
	register CopyBuffPtr nextBuff;

	while (copyBuff) {
		nextBuff = copyBuff->Next;
		FreeMem(copyBuff, sizeof(CopyBuff));
		copyBuff = nextBuff;
	}
}

/*
 *	Read file into linked buffers
 *	Return pointer to first buffer or NULL if failure
 */

static CopyBuffPtr LoadFile(TextPtr name)
{
	register WORD size;
	register File file;
	register CopyBuffPtr firstBuff, currBuff, newBuff;
	UBYTE buff[BUFF_SIZE];

	if ((file = Open(name, MODE_OLDFILE)) == NULL)
		return (NULL);
	firstBuff = currBuff = NULL;
	while ((size = Read(file, buff, BUFF_SIZE)) > 0) {
		if ((newBuff = AllocMem(sizeof(CopyBuff), 0)) == NULL) {
			size = -1;						/* Simulate error */
			break;
		}
		memcpy(newBuff->Buff, buff, size);
		newBuff->Size = size;
		newBuff->Next = NULL;
		if (firstBuff == NULL)
			firstBuff = newBuff;
		else
			currBuff->Next = newBuff;
		currBuff = newBuff;
	}
	Close(file);
	if (size < 0) {
		FreeCopyBuff(firstBuff);
		return (NULL);
	}
	return (firstBuff);
}

/*
 *	Save copy buffer to file
 *	Return success status
 */

static BOOL SaveFile(TextPtr name, register CopyBuffPtr copyBuff)
{
	register BOOL success;
	register BPTR file;

	(void) DeleteFile(name);
	if ((file = Open(name, MODE_NEWFILE)) == NULL)
		return (FALSE);
	success = FALSE;
	while (copyBuff) {
		if (Write(file, copyBuff->Buff, copyBuff->Size) != copyBuff->Size)
			goto Exit;
		copyBuff = copyBuff->Next;
	}
	success = TRUE;
Exit:
	Close(file);
	if (!success)
		(void) DeleteFile(name);
	return (success);
}

/*
 *	Copy specified font
 *	Return success status
 */

static BOOL CopyFont(TextPtr fileName)
{
	register WORD size, num;
	register BOOL success;
	Dir dir;
	BPTR lock, fontSeg, file;
	register FontDataPtr fontData;
	register CopyBuffPtr fontBuff, metricBuff;
	TextChar name[64];
	struct FontContentsHeader fch;
	struct FontContents fc, fcBuff;

	success = FALSE;
	fontBuff = metricBuff = NULL;
	file = NULL;
/*
	Switch to source fonts directory
*/
	dir = DupLock(fromLock);
	SetDir(dir);
	if ((dir = Lock("fonts", ACCESS_READ)) == NULL)
		goto Exit;
	SetDir(dir);
/*
	Create info for ".font" file
*/
	memset((BYTE *) &fc, 0, FC_SIZE);
	if ((fontSeg = LoadSeg(fileName)) == NULL)
		goto Exit;
	fontData = (FontDataPtr) BADDR(fontSeg);
	strcpy(fc.fc_FileName, fileName);
	fc.fc_YSize = fontData->DFH.dfh_TF.tf_YSize;
	fc.fc_Style = fontData->DFH.dfh_TF.tf_Style;
	fc.fc_Flags = fontData->DFH.dfh_TF.tf_Flags;
	UnLoadSeg(fontSeg);
/*
	Load font file
*/
	if ((fontBuff = LoadFile(fileName)) == NULL)
		goto Exit;
/*
	Load metric file (if it exists)
*/
	BuildName(fileName, ".metric", name);
	metricBuff = LoadFile(name);				/* NULL means no metric file */
/*
	Switch to destination fonts directory
*/
	dir = DupLock(toLock);
	SetDir(dir);
	if (!MakeDir("fonts") || (dir = Lock("fonts", ACCESS_READ)) == NULL)
		goto Exit;
	SetDir(dir);
	BuildName(fileName, "", name);
	if (!MakeDir(name) || !SaveFile(fileName, fontBuff))
		goto Exit;
/*
	Now add (or update) entry to ".font" file
*/
	BuildName(fileName, ".font", name);
	if ((file = Open(name, MODE_OLDFILE)) != NULL) {
		if (Read(file, (BYTE *) &fch, FCH_SIZE) != FCH_SIZE)
			goto Exit;
		num = 0;
		while ((size = Read(file, (BYTE *) &fcBuff, FC_SIZE)) == FC_SIZE) {
			if (CmpString(fcBuff.fc_FileName, fileName,
						   (WORD) strlen(fcBuff.fc_FileName),
						   (WORD) strlen(fileName), FALSE) == 0) {
				if (Seek(file, -FC_SIZE, OFFSET_CURRENT) == -1 ||
					Write(file, (BYTE *) &fc, FC_SIZE) != FC_SIZE)
					goto Exit;
				break;
			}
			num++;
		}
		if (size != FC_SIZE && size != 0)
			goto Exit;
		if (size == 0) {				/* Entry not found */
			fch.fch_FileID = FCH_ID;
			fch.fch_NumEntries = num + 1;
			if (Write(file, (BYTE *) &fc, FC_SIZE) != FC_SIZE ||
				Seek(file, 0, OFFSET_BEGINNING) == -1 ||
				Write(file, (BYTE *) &fch, FCH_SIZE) != FCH_SIZE)
				goto Exit;
		}
	}
	else if ((file = Open(name, MODE_NEWFILE)) != NULL) {
		fch.fch_FileID = FCH_ID;
		fch.fch_NumEntries = 1;
		if (Write(file, (BYTE *) &fch, FCH_SIZE) != FCH_SIZE ||
			Write(file, (BYTE *) &fc, FC_SIZE) != FC_SIZE)
			goto Exit;
	}
	else						/* Can't make ".font" file */
		goto Exit;
	Close(file);
	file = NULL;
/*
	If no metric file in destination then save metric file
*/
	BuildName(fileName, ".metric", name);
	if ((lock = Lock(name, ACCESS_READ)) != NULL)
		UnLock(lock);
	else if (metricBuff && !SaveFile(name, metricBuff))
		goto Exit;
/*
	All done
*/
	success = TRUE;
Exit:
	if (file)
		Close(file);
	if (metricBuff)
		FreeCopyBuff(metricBuff);
	if (fontBuff)
		FreeCopyBuff(fontBuff);
	return (success);
}

/*
 *	Copy a file from one place to another
 *	Return success status
 */

static BOOL CopyFile(TextPtr fileName)
{
	register BOOL success;
	register BPTR dir;
	register CopyBuffPtr fileBuff;

	success = FALSE;
	fileBuff = NULL;
/*
	Switch to source directory
*/
	dir = DupLock(fromLock);
	SetDir(dir);
	if ((dir = Lock(moveDirName, ACCESS_READ)) == NULL)
		goto Exit;
	SetDir(dir);
/*
	Load font file
*/
	if ((fileBuff = LoadFile(fileName)) == NULL)
		goto Exit;
/*
	Switch to destination directory
*/
	dir = DupLock(toLock);
	SetDir(dir);
	if (!MakeDir(moveDirName) || (dir = Lock(moveDirName, ACCESS_READ)) == NULL)
		goto Exit;
	SetDir(dir);
/*
	Save the file
*/
	(void) DeleteFile(fileName);
	if (!SaveFile(fileName, fileBuff))
		goto Exit;
/*
	All done
*/
	success = TRUE;
Exit:
	if (fileBuff)
		FreeCopyBuff(fileBuff);
	return (success);
}

/*
 *	Handle "Copy" button
 */

void DoCopy()
{
	register WORD i;
	register BOOL success;
	register TextPtr fileName;

	if (SLNextSelect(scrollList, -1) == -1 ||
		fromLock == NULL || toLock == NULL || fileList == NULL)
		return;
/*
	Copy each item one by one
*/
	SetStdPointer(window, POINTER_WAITDELAY);
	DisableInput();
	OffButtons();
	i = -1;
	for (;;) {
		i = SLNextSelect(scrollList, i);
		if (i == -1)
			break;
		fileName = GetListItem(fileList, i);
		switch (moveType) {
		case FONTS_ITEM:
			success = CopyFont(fileName);
			break;
		default:
			success = CopyFile(fileName);
			break;
		}
		if (!success) {
			SysBeep(5);
			DisplayBeep(window->WScreen);
			break;
		}
	}
	SetStdPointer(window, POINTER_ARROW);
	EnableInput();
	SetButtons();
}
