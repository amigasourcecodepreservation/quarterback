/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Initialization
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <clib/alib_protos.h>

#include <stdlib.h>				// For exit() prototype

#include <Toolbox/StdInit.h>
#include <Toolbox/Utility.h>

#include "Mover.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct NewWindow	newWindow;

extern WindowPtr	window;
extern MsgPortPtr	mainMsgPort;

extern TextPtr	strInitError[];

/*
 *	Local variables and definitions
 */

static Dir	startupDir;

/*
 *	Local prototypes
 */

static ScreenPtr GetWorkbenchScreen(void);

/*
 *	Get pointer to workbench screen
 */

static ScreenPtr GetWorkbenchScreen()
{
	LONG				intuiLock;
	register ScreenPtr	screen;

	if (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0)
		screen = LockPubScreen(NULL);
	else {
		intuiLock = LockIBase(0);
		for (screen = IntuitionBase->FirstScreen; screen; screen = screen->NextScreen) {
			if ((screen->Flags & SCREENTYPE) == WBENCHSCREEN)
				break;
		}
		UnlockIBase(intuiLock);
	}
	return (screen);
}

/*
 *	Initialization routine
 *	Open necessary libraries and devices, and open window
 */

void Init()
{
	ScreenPtr	screen;

	StdInit(OSVERSION_1_2);
	if ((screen = GetWorkbenchScreen()) == NULL)
		InitError(strInitError[INIT_NOWINDOW]);
	InitToolbox(screen);
/*
	Create message port
*/
	if ((mainMsgPort = CreatePort(NULL, 0)) == NULL)
		InitError(strInitError[INIT_NOMEM]);
/*
	Open main window
*/
	CreateWindow();
	if (window == NULL)
		InitError(strInitError[INIT_NOWINDOW]);
	SetStdPointer(window, POINTER_ARROW);
}

/*
 *	Shut down program
 *	Close window and all opened libraries
 */

void ShutDown()
{
	MsgPtr msg;

	if (window)
		RemoveWindow();
	if (mainMsgPort) {
		while ((msg = GetMsg(mainMsgPort)) != NULL)
			ReplyMsg(msg);
		DeletePort(mainMsgPort);
	}
	StdShutDown();
}
