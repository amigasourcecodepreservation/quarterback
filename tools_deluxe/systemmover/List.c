/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	List routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>

#include "Mover.h"
#include "Proto.h"

/*
 *	External variables
 */

extern Dir	fromLock;

extern ListHeadPtr	fileList;

extern WindowPtr		window;
extern ScrollListPtr	scrollList;
extern TextFontPtr		windowFont;

extern WORD		moveType;
extern TextPtr	moveDirName;

/*
 *	Local variables and definitions
 */

#define IS_DIR(fib)		((fib)->fib_DirEntryType > 0)
#define IS_FILE(fib)	((fib)->fib_DirEntryType < 0)
#define IS_INFOFILE(name, len)	\
	((len) > 4 && CmpString((name) + (len) - 5, ".info", 5, 5, FALSE) == 0)

/*
 *	Local prototypes
 */

static void	GetFontLists(void);
static void	GetGenLists(TextPtr, TextPtr);

/*
 *	Get lists of fonts
 */

static void GetFontLists()
{
	WORD i, num, nameLen, sizeLen;
	WORD dots, nameDots, sizeDots, miscDots, maxDots;
	File file;
	Dir dir, dir1, dir2;
	LONG header;
	register struct FileInfoBlock *fib1, *fib2;
	TextPtr fontName, sizeName;
	RastPtr rPort = window->RPort;
	GadgetPtr listBox;
	TextChar name[100], itemName[50];

	moveDirName = "fonts";
	SLRemoveAll(scrollList);
	SetFont(window->RPort, windowFont);
	listBox = GadgetItem(window->FirstGadget, LIST_BOX);
	maxDots = listBox->Width - 1;
/*
	Go to fonts directory
*/
	dir = DupLock(fromLock);
	SetDir(dir);
	if ((dir = Lock(moveDirName, ACCESS_READ)) == NULL)
		return;
	SetDir(dir);
/*
	Set up
*/
	dir1 = NULL;
	fib1 = fib2 = NULL;
	if ((dir1 = DupLock(dir)) == NULL)
		goto Exit;
	if ((fib1 = AllocMem(sizeof(struct FileInfoBlock), 0)) == NULL)
		goto Exit;
	if ((fib2 = AllocMem(sizeof(struct FileInfoBlock), 0)) == NULL)
		goto Exit;
	fontName = fib1->fib_FileName;
	sizeName = fib2->fib_FileName;
/*
	Scan fonts directory
*/
	SLDoDraw(scrollList, FALSE);
	if (!Examine(dir1, fib1) || IS_FILE(fib1))
		goto Exit;
	while (ExNext(dir1, fib1)) {
		if (IS_FILE(fib1))
			continue;
		if ((dir2 = Lock(fontName, ACCESS_READ)) == NULL)
			continue;
		if (!Examine(dir2, fib2)) {
			UnLock(dir2);
			continue;
		}
		nameLen = strlen(fontName);
/*
	Get list of actual font data files
*/
		while (ExNext(dir2, fib2)) {
			if (IS_DIR(fib2))
				continue;
			sizeLen = strlen(sizeName);
			if (IS_INFOFILE(sizeName, sizeLen))
				continue;
/*
	Make sure it is an actual font data file
*/
			strcpy(name, fontName);
			strcat(name, "/");
			strcat(name, sizeName);
			if ((file = Open(name, MODE_OLDFILE)) == NULL)
				continue;
			num = Read(file, &header, sizeof(LONG));
			Close(file);
			if (num != sizeof(LONG) || header != 0x03F3)
				continue;
/*
	Add to scrollList first (this list should be sorted)
*/
			nameDots = TextLength(rPort, fontName, nameLen);
			sizeDots = TextLength(rPort, sizeName, sizeLen);
			if (nameDots + sizeDots > maxDots) {
				miscDots = TextLength(rPort, "...", 3);
				num = nameLen;
				for (num = nameLen; num > 0; num--) {
					dots = TextLength(rPort, fontName, num) + sizeDots + miscDots;
					if (dots <= maxDots)
						break;
				}
				memcpy(name, fontName, num);
				strcpy(name + num, "...");
				strcat(name, sizeName);
			}
			else {
				miscDots = TextLength(rPort, " ", 1);
				if (miscDots == 0)
					miscDots = 8;
				num = (maxDots - nameDots - sizeDots)/miscDots;
				strcpy(name, fontName);
				for (i = 0; i < num; i++)
					name[i + nameLen] = ' ';
				strcpy(name + nameLen + num, sizeName);
			}
			num = SLNumItems(scrollList);
			for (i = 0; i < num; i++) {
				SLGetItem(scrollList, i, itemName);
				if (CmpString(name, itemName, strlen(name), strlen(itemName), FALSE) < 0)
					break;
			}
			SLAddItem(scrollList, name, strlen(name), i);
/*
	Now add to fileList at same location in list as scrollList
*/
			strcpy(name, fontName);
			strcat(name, "/");
			strcat(name, sizeName);
			if (!InsertListItem(fileList, name, strlen(name), i)) {
				SLRemoveItem(scrollList, i);
				break;
			}
		}
		UnLock(dir2);
	}
/*
	Clean up
*/
Exit:
	SLDoDraw(scrollList, TRUE);
	if (fib2)
		FreeMem(fib2, sizeof(struct FileInfoBlock));
	if (fib1)
		FreeMem(fib1, sizeof(struct FileInfoBlock));
	if (dir1)
		UnLock(dir1);
}

/*
 *	Get file and window lists for specified directory
 *	If fileExt is specified then only names with proper file extension will be added
 */

static void GetGenLists(TextPtr dirName, TextPtr fileExt)
{
	register WORD i, num, nameLen, extLen;
	Dir dir, dir1;
	struct FileInfoBlock *fib1;
	TextPtr fileName;
	TextChar name[50];

	moveDirName = dirName;
	SLRemoveAll(scrollList);
/*
	Go to specified directory
*/
	dir = DupLock(fromLock);
	SetDir(dir);
	if ((dir = Lock(dirName, ACCESS_READ)) == NULL)
		return;
	SetDir(dir);
/*
	Set up
*/
	dir1 = NULL;
	fib1 = NULL;
	if ((dir1 = DupLock(dir)) == NULL)
		goto Exit;
	if ((fib1 = AllocMem(sizeof(struct FileInfoBlock), 0)) == NULL)
		goto Exit;
	fileName = fib1->fib_FileName;
	extLen = (fileExt) ? strlen(fileExt) : 0;
/*
	Scan directory
*/
	SLDoDraw(scrollList, FALSE);
	if (!Examine(dir1, fib1) || IS_FILE(fib1))
		goto Exit;
	while (ExNext(dir1, fib1)) {
		if (IS_DIR(fib1))
			continue;
		nameLen = strlen(fileName);
		if (IS_INFOFILE(fileName, nameLen))
			continue;
		if (extLen &&
			(nameLen < extLen || CmpString(fileName + nameLen - extLen, fileExt,
											extLen, extLen, FALSE) != 0))
			continue;
/*
	Add to scrollList first (this list should be sorted)
*/
		num = SLNumItems(scrollList);
		for (i = 0; i < num; i++) {
			SLGetItem(scrollList, i, name);
			if (CmpString(fileName, name, nameLen, strlen(name), FALSE) < 0)
				break;
		}
		SLAddItem(scrollList, fileName, strlen(fileName), i);
/*
	Now add to fileList at same location in list as scrollList
*/
		if (!InsertListItem(fileList, fileName, nameLen, i)) {
			SLRemoveItem(scrollList, i);
			break;
		}
	}
/*
	Clean up
*/
Exit:
	SLDoDraw(scrollList, TRUE);
	if (fib1)
		FreeMem(fib1, sizeof(struct FileInfoBlock));
	if (dir1)
		UnLock(dir1);
}

/*
 *	Dispose of file and window lists
 */

void DisposeFileList()
{
	if (fileList)
		DisposeList(fileList);
	fileList = NULL;
}

/*
 *	Get file and window lists
 */

void GetFileList()
{
/*
	Create empty list
*/
	if (fileList)
		DisposeListItems(fileList);
	else
		fileList = CreateList();
	if (fileList == NULL || fromLock == NULL)
		return;
/*
	Get appropriate lists and display
*/
	SetStdPointer(window, POINTER_WAITDELAY);
	OffButtons();
	switch (moveType) {
	case FONTS_ITEM:
		GetFontLists();
		break;
	case PRINTERS_ITEM:
		GetGenLists("devs/printers", NULL);
		break;
	case KEYMAPS_ITEM:
		GetGenLists("devs/keymaps", NULL);
		break;
	case LIBRARIES_ITEM:
		GetGenLists("libs", ".library");
		break;
	case DEVICES_ITEM:
		GetGenLists("devs", ".device");
		break;
	case HANDLERS_ITEM:
		GetGenLists("l", NULL);
		break;
	case CLI_ITEM:
		GetGenLists("c", NULL);
		break;
	case SCRIPTS_ITEM:
		GetGenLists("s", NULL);
		break;
	}
	SetButtons();
	SetStdPointer(window, POINTER_ARROW);
}
