/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright 1987-92 New Horizons Software, Inc.
 *
 *	Window routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Border.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Mover.h"
#include "Proto.h"

/*
 *	External variables
 */

extern DialogTemplate	mainDialog;
extern MenuTemplate		newMenus[];

extern MsgPortPtr		mainMsgPort;
extern WindowPtr		window;
extern ScrollListPtr	scrollList;
extern TextFontPtr		windowFont;

extern TextChar	strScreenTitle[];

/*
 *	Local prototypes
 */

static void	DrawWindow(void);

/*
 *	Draw disk name in window
 *	Position of 0 means upper (from) location, 1 means lower (to) location
 */

void DrawDiskName(TextPtr name, WORD position)
{
	SetGadgetItemText(window->FirstGadget, FROM_TEXT + position, window, NULL, name);
}

/*
 *	Create and draw main window
 */

void CreateWindow()
{
	MenuPtr		menuStrip;

	window = NULL;
	scrollList = NULL;
/*
	Open dialog window
*/
	if (LibraryVersion((struct Library *) IntuitionBase) < OSVERSION_2_0)
		mainDialog.Gadgets[LIST_SCROLL].Type &= ~GADG_PROP_NOBORDER;
	if ((window = GetDialog(&mainDialog, NULL, mainMsgPort)) == NULL)
		return;
	SetWindowTitles(window, (TextPtr) -1, strScreenTitle);
	ModifyIDCMP(window, window->IDCMPFlags | MENUPICK | DISKINSERTED | DISKREMOVED);
/*
	Set up scroll list
*/
	if ((scrollList = NewScrollList(SL_MULTISELECT)) == NULL) {
		RemoveWindow();
		return;
	}
	InitScrollList(scrollList, GadgetItem(window->FirstGadget, LIST_BOX), window, NULL);
/*
	Get window font for info display and draw window
*/
	if ((windowFont = GetFont(window->WScreen->Font)) == NULL) {
		RemoveWindow();
		return;
	}
	SetFont(window->RPort, windowFont);
	DrawWindow();
/*
	Get menu
*/
	if ((menuStrip = GetMenuStrip(newMenus)) == NULL) {
		RemoveWindow();
		return;
	}
	InsertMenuStrip(window, menuStrip);
}

/*
 *	Remove main window
 */

void RemoveWindow()
{
	MenuPtr		menuStrip;

	if (window == NULL)
		return;
	menuStrip = window->MenuStrip;
	ClearMenuStrip(window);
	SetClip(window->WLayer, NULL);
	DisposeDialog(window);
	if (scrollList)
		DisposeScrollList(scrollList);
	if (menuStrip)
		DisposeMenuStrip(menuStrip);
	if (windowFont)
		CloseFont(windowFont);
}

/*
 *	Refresh window
 */

void RefreshWindow()
{
	RastPtr		rPort;
	Rectangle	rect;

	GetWindowRect(window, &rect);
	if (!EmptyRect(&rect)) {
/*
	If old color scheme, fill with _tbPenLight
	Note: Cannot call RefreshGadgets() within Begin/EndRefresh()
*/
		if (_tbPenLight != 0) {
			rPort = window->RPort;
			SetAPen(rPort, _tbPenLight);
			FillRect(rPort, &rect);
			RefreshGadgets(GadgetItem(window->FirstGadget, 0), window, NULL);
		}
		BeginRefresh(window);
		DrawWindow();
		EndRefresh(window, TRUE);
	}
}

/*
 *	Draw window
 */

static void DrawWindow()
{
	Rectangle	rect;

	SLDrawBorder(scrollList);
	GetInfoRect(&rect);
	DrawShadowBox(window->RPort, &rect, -1, FALSE);
	SLDrawList(scrollList);
	ShowInfo();
}

/*
 *	Filter function for ModalDialog/ModalRequest
 *	Handles window updates
 */

BOOL DialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	RequestPtr	req;

	if (intuiMsg->IDCMPWindow == window && intuiMsg->Class == REFRESHWINDOW) {
		ReplyMsg((MsgPtr) intuiMsg);
		if ((req = window->FirstRequest) != NULL)
			OutlineButton(GadgetItem(req->ReqGadget, OK_BUTTON), window, req, TRUE);
		RefreshWindow();
		*item = -1;
		return (TRUE);
	}
	return (FALSE);
}
