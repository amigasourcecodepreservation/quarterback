/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Global variables
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <Toolbox/ScrollList.h>

#include "Mover.h"

struct IntuitionBase	*IntuitionBase;
struct GfxBase			*GfxBase;
struct LibBase			*LayersBase;
struct LibBase			*IconBase;
struct Device			*ConsoleDevice;

WindowPtr		window;
MsgPortPtr		mainMsgPort;
ScrollListPtr	scrollList;
TextFontPtr		windowFont;

BOOL	quitFlag;	/* Inited to FALSE */

BOOL	titleChanged;	/* Screen title was changed by Error() (Inited to FALSE) */

Dir	fromLock, toLock;	/* From/To locks (Inited to NULL) */

ListHeadPtr	fileList;	/* Inited to NULL */

WORD	moveType = FONTS_ITEM;	/* Type of item to move */
TextPtr	moveDirName = "fonts";
