/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright (c) 1987-92 New Horizons Software, Inc.
 *
 *	Menu definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Language.h>
#include <Toolbox/Menu.h>

/*
 *  Disk menu
 */

static MenuItemTemplate diskItems[] = {
#if (AMERICAN | BRITISH)
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "From Disk", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "To Disk", NULL },
	{ MENU_TEXT_ITEM },
    { MENU_TEXT_ITEM, MENU_ENABLED, 'Q', 0, 0, "Quit", NULL },
#elif GERMAN
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "From Disk", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "To Disk", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'Q', 0, 0, "Quit", NULL },
#elif FRENCH
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Source", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Destination", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'Q', 0, 0, "Quitter", NULL },
#elif SWEDISH
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Fr�n Disk", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Till Disk", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'Q', 0, 0, "Avsluta", NULL },
#elif SPANISH
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Del Disco", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Al Disco", NULL },
	{ MENU_TEXT_ITEM },
    { MENU_TEXT_ITEM, MENU_ENABLED, 'Q', 0, 0, "Salir", NULL },
#endif
	{ MENU_NO_ITEM }
};

/*
 *  Type menu
 */

static MenuItemTemplate moveItems[] = {
#if (AMERICAN | BRITISH)
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   0, 0, 0x376, "Fonts", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x375, "Printers", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x373, "Key Maps", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x367, "Libraries", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x357, "Devices", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x337, "Handlers", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x277, "CLI Commands", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x177, "CLI Scripts", NULL },
#elif GERMAN
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   0, 0, 0x376, "Fonts", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x375, "Printers", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x373, "Key Maps", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x367, "Libraries", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x357, "Devices", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x337, "Handlers", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x277, "CLI Commands", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x177, "CLI Scripts", NULL },
#elif FRENCH
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   0, 0, 0x376, "Fontes", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x375, "Imprimantes", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x373, "Cartes clavier", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x367, "Librairies", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x357, "Devices", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x337, "Handlers", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x277, "Commandes CLI", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x177, "Scripts CLI", NULL },
#elif SWEDISH
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   0, 0, 0x376, "Typsnitt", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x375, "Skrivare", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x373, "Tangentbord", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x367, "Libraries", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x357, "Devices", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x337, "Handlers", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x277, "Shell kommandon", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x177, "Shell manuskript", NULL },
#elif SPANISH
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   0, 0, 0x376, "Fuentes", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x375, "Impresoras", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x373, "Teclados", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x367, "Librerias", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x357, "Devices", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x337, "Handlers", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x277, "Comandos del CLI", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x177, "Scripts del CLI", NULL },
#endif
	{ MENU_NO_ITEM }
};

/*
 *  Menu strip template
 */

MenuTemplate newMenus[] = {
#if (AMERICAN | BRITISH)
	{ " Disk ", diskItems },
	{ " Move ", moveItems },
#elif GERMAN
	{ " Disk ", diskItems },
	{ " Move ", moveItems },
#elif FRENCH
	{ " Disque ", diskItems },
	{ " Copier ", moveItems },
#elif SWEDISH
	{ " Disk ", diskItems },
	{ " Flytta ", moveItems },
#elif SPANISH
	{ " Disco ", diskItems },
	{ " Mover ", moveItems },
#endif
	{ NULL, NULL }
};
