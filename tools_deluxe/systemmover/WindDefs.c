/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright (c) 1987-93 New Horizons Software, Inc.
 *
 *	Window and gadget definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Language.h>
#include <Toolbox/Image.h>
#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>

#include "Mover.h"

/*
 *	Local variables and definitions
 */

#define NUM_ENTRIES	7

#define LIST_WIDTH	(21*8 + 20)		/* Allows 21 char file names */
#define LIST_HEIGHT	(NUM_ENTRIES*12)

#define SL_GADG_BOX(left, top, width, num)	\
	{ GADG_ACTIVE_BORDER, left, top, 0, 0, width-ARROW_WIDTH, (num)*11, -1, num, NULL }

#define SL_GADG_UPARROW(left, top, width, num)	\
	{ GADG_ACTIVE_STDIMAGE,														\
		(left)+(width)-ARROW_WIDTH, (top)+(num)*11-2*ARROW_HEIGHT, 0, (num)+1,	\
		ARROW_WIDTH, ARROW_HEIGHT, 0, 0, 0, 0, (Ptr) IMAGE_ARROW_UP }

#define SL_GADG_DOWNARROW(left, top, width, num)	\
	{ GADG_ACTIVE_STDIMAGE,														\
		(left)+(width)-ARROW_WIDTH, (top)+(num)*11-ARROW_HEIGHT, 0, (num)+1,	\
		ARROW_WIDTH, ARROW_HEIGHT, 0, 0, 0, 0, (Ptr) IMAGE_ARROW_DOWN }

#define SL_GADG_SLIDER(left, top, width, num)	\
	{ GADG_PROP_VERT | GADG_PROP_NOBORDER,		\
		(left)+(width)-ARROW_WIDTH, top, 1, 0,	\
		ARROW_WIDTH, (num)*11-2*ARROW_HEIGHT, -2, num, NULL }

/*
 *	Gadget template for main window
 */

static GadgetTemplate windowGadgets[] = {
#if (AMERICAN | BRITISH)
	{ GADG_PUSH_BUTTON, LIST_WIDTH + 40, 40, 0, 0, 60, 20, 0, 0, 'C', 0, "Copy" },
	{ GADG_PUSH_BUTTON, LIST_WIDTH + 40, 70, 0, 0, 60, 20, 0, 0, 'R', 0, "Remove" },
#elif GERMAN
	{ GADG_PUSH_BUTTON, LIST_WIDTH + 40, 40, 0, 0, 60, 20, 0, 0, 'C', 0, "Copy" },
	{ GADG_PUSH_BUTTON, LIST_WIDTH + 40, 70, 0, 0, 60, 20, 0, 0, 'R', 0, "Remove" },
#elif FRENCH
	{ GADG_PUSH_BUTTON, LIST_WIDTH + 40, 40, 0, 0, 70, 20, 0, 0, 'C', 0, "Copier" },
	{ GADG_PUSH_BUTTON, LIST_WIDTH + 40, 70, 0, 0, 70, 20, 0, 0, 'E', 0, "Effacer" },
#elif SWEDISH
	{ GADG_PUSH_BUTTON, LIST_WIDTH + 40, 40, 0, 0, 70, 20, 0, 0, 'K', 0, "Kopiera" },
	{ GADG_PUSH_BUTTON, LIST_WIDTH + 40, 70, 0, 0, 70, 20, 0, 0, 'R', 0, "Radera" },
#elif SPANISH
	{ GADG_PUSH_BUTTON, LIST_WIDTH + 40, 40, 0, 0, 70, 20, 0, 0, 'C', 0, "Copiar" },
	{ GADG_PUSH_BUTTON, LIST_WIDTH + 40, 70, 0, 0, 70, 20, 0, 0, 'R', 0, "Remover" },
#endif

	SL_GADG_BOX(20, 40, LIST_WIDTH, NUM_ENTRIES),
	SL_GADG_UPARROW(20, 40, LIST_WIDTH, NUM_ENTRIES),
	SL_GADG_DOWNARROW(20, 40, LIST_WIDTH, NUM_ENTRIES),
	SL_GADG_SLIDER(20, 40, LIST_WIDTH, NUM_ENTRIES),

#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT, 70,  5, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 70, 20, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 20,  5, 0, 0, 0, 0, 0, 0, 0, 0, "From:" },
	{ GADG_STAT_TEXT, 20, 20, 0, 0, 0, 0, 0, 0, 0, 0, "To:" },
#elif GERMAN
	{ GADG_STAT_TEXT, 70,  5, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 70, 20, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 20,  5, 0, 0, 0, 0, 0, 0, 0, 0, "From:" },
	{ GADG_STAT_TEXT, 20, 20, 0, 0, 0, 0, 0, 0, 0, 0, "To:" },
#elif FRENCH
	{ GADG_STAT_TEXT, 85,  5, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 85, 20, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 20,  5, 0, 0, 0, 0, 0, 0, 0, 0, "Source:" },
	{ GADG_STAT_TEXT, 20, 20, 0, 0, 0, 0, 0, 0, 0, 0, "Dest:" },
#elif SWEDISH
	{ GADG_STAT_TEXT, 70,  5, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 70, 20, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 20,  5, 0, 0, 0, 0, 0, 0, 0, 0, "Fr�n:" },
	{ GADG_STAT_TEXT, 20, 20, 0, 0, 0, 0, 0, 0, 0, 0, "till:" },
#elif SPANISH
	{ GADG_STAT_TEXT, 55,  5, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 55, 20, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 20,  5, 0, 0, 0, 0, 0, 0, 0, 0, "De:" },
	{ GADG_STAT_TEXT, 20, 20, 0, 0, 0, 0, 0, 0, 0, 0, "A:" },
#endif
	{ GADG_ITEM_NONE }
};

/*
 *	Dialog template for main window
 */

DialogTemplate mainDialog = {
	DLG_TYPE_WINDOW, DLG_FLAG_CLOSE | DLG_FLAG_DEPTH | DLG_FLAG_ZOOM,
#if (AMERICAN | BRITISH | GERMAN)
	-1, -1, 20 + LIST_WIDTH + 100, 40 + LIST_HEIGHT + 65,
#elif (FRENCH | SWEDISH | SPANISH)
	-1, -1, 20 + LIST_WIDTH + 110, 40 + LIST_HEIGHT + 65,
#endif
#if (AMERICAN | BRITISH | GERMAN | FRENCH | SPANISH)
	windowGadgets, "System Mover"
#elif SWEDISH
	windowGadgets, "Systemflyttaren"
#endif
};

/*
 *	Remove files dialog
 */

static GadgetTemplate removeGadgets[] = {
#if (AMERICAN | BRITISH)
	{ GADG_PUSH_BUTTON,  40, 50, 0, 0, 60, 20, 0, 0, 'Y', 0, "Yes" },
	{ GADG_PUSH_BUTTON, 140, 50, 0, 0, 60, 20, 0, 0, 'N', 0, "No" },
	{ GADG_STAT_TEXT,    55, 10, 0, 0,  0,  0, 0, 0,   0, 0, "Permanently remove\nselected files?" },
#elif GERMAN
	{ GADG_PUSH_BUTTON,  40, 50, 0, 0, 60, 20, 0, 0, 'J', 0, "Ja" },
	{ GADG_PUSH_BUTTON, 140, 50, 0, 0, 60, 20, 0, 0, 'N', 0, "Nein" },
	{ GADG_STAT_TEXT,    55, 10, 0, 0,  0,  0, 0, 0,   0, 0, "Permanently remove\nselected files?" },
#elif FRENCH
	{ GADG_PUSH_BUTTON,  40, 50, 0, 0, 60, 20, 0, 0, 'O', 0, "Oui" },
	{ GADG_PUSH_BUTTON, 140, 50, 0, 0, 60, 20, 0, 0, 'N', 0, "Non" },
	{ GADG_STAT_TEXT,    55, 10, 0, 0,  0,  0, 0, 0,   0, 0, "Effacer les fichiers\ns�lectionn�s?" },
#elif SWEDISH
	{ GADG_PUSH_BUTTON,  40, 50, 0, 0, 60, 20, 0, 0, 'J', 0, "Ja" },
	{ GADG_PUSH_BUTTON, 140, 50, 0, 0, 60, 20, 0, 0, 'N', 0, "Nej" },
	{ GADG_STAT_TEXT,    55, 10, 0, 0,  0,  0, 0, 0,   0, 0, "Permanent radering\nav valda filer?" },
#elif SPANISH
	{ GADG_PUSH_BUTTON,  50, 50, 0, 0, 60, 20, 0, 0, 'Y', 0, "S�" },
	{ GADG_PUSH_BUTTON, 150, 50, 0, 0, 60, 20, 0, 0, 'N', 0, "No" },
	{ GADG_STAT_TEXT,    55, 10, 0, 0,  0,  0, 0, 0,   0, 0, "�Remover definitivamente\narchivos seleccionados?" },
#endif
	{ GADG_STAT_STDIMAGE,10, 10, 0, 0,  0,  0, 0, 0,   0, 0, (Ptr) IMAGE_ICON_CAUTION },
	{ GADG_ITEM_NONE }
};

RequestTemplate removeRequest = {
#if (AMERICAN | BRITISH | GERMAN | FRENCH | SWEDISH)
	-1, -1, 240, 80, removeGadgets
#elif (SPANISH)
	-1, -1, 260, 80, removeGadgets
#endif
};
