/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright (c) 1987-92 New Horizons Software, Inc.
 *
 *	Main Program
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>

#include "Mover.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr		window;
extern MsgPortPtr		mainMsgPort;
extern ScrollListPtr	scrollList;

extern BOOL	quitFlag;

/*
 *	Local prototypes
 */

static void	DoIntuiMessage(IntuiMsgPtr);
static void	DoMenu(UWORD, UWORD);

/*
 *	Main program
 */

void main()
{
	register IntuiMsgPtr	intuiMsg;

/*
	Initialize
*/
	Init();
	OffButtons();
	DoDiskInserted();				// Get initial disk list
/*
	Get and respond to messages
*/
	do {
/*
	Process intuition messages
*/
		while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort))
			DoIntuiMessage(intuiMsg);
/*
	If not quitting then wait for messages
*/
		if (!quitFlag)
			Wait(1 << mainMsgPort->mp_SigBit);
	} while (!quitFlag);
/*
	Quitting
*/
	SetStdPointer(window, POINTER_WAIT);
	UnLockDisks();
	DisposeDiskList();
	DisposeFileList();
	ShutDown();
}

/*
 *	Process intuition messages
 */

static void DoIntuiMessage(register IntuiMsgPtr intuiMsg)
{
	register ULONG		class;
	register UWORD		code;
	register APTR		iAddress;
	register UWORD		qualifier;
	register WindowPtr	msgWindow;
	DialogPtr			dlg;
	WORD				gadgNum, itemHit;

	class = intuiMsg->Class;
	code = intuiMsg->Code;
	iAddress = intuiMsg->IAddress;
	if (class != INTUITICKS)
		qualifier = intuiMsg->Qualifier;
	msgWindow = intuiMsg->IDCMPWindow;
	if (msgWindow != window) {
		ReplyMsg((MsgPtr) intuiMsg);
		return;
	}
/*
	Process the message
*/
	if ((class == GADGETDOWN || class == GADGETUP) &&
		((gadgNum = GadgetNumber(iAddress)) == LIST_BOX || gadgNum == LIST_UP ||
		 gadgNum == LIST_DOWN || gadgNum == LIST_SCROLL)) {
		SLGadgetMessage(scrollList, mainMsgPort, intuiMsg);
		if (gadgNum == LIST_BOX) {
			ShowInfo();
			SetButtons();
		}
	}
	else if (DialogSelect(intuiMsg, &dlg, &itemHit)) {
		ReplyMsg((MsgPtr) intuiMsg);
		switch (itemHit) {
		case DLG_CLOSE_BOX:
			quitFlag = TRUE;
			break;
		case COPY_BUTTON:
			DoCopy();
			break;
		case REMOVE_BUTTON:
			DoRemove();
			break;
		}
	}
	else {
		ReplyMsg((MsgPtr) intuiMsg);
		switch (class) {
		case MENUPICK:
			DoMenu(code, qualifier);
			break;
		case DISKINSERTED:
		case DISKREMOVED:
		case INTUITICKS:				// In case somebody removed a lock
			if (!CheckDiskList())
				DoDiskInserted();
			break;
		case RAWKEY:
			if (code == CURSORUP || code == CURSORDOWN) {
				SLCursorKey(scrollList, code);
				ShowInfo();
				SetButtons();
			}
			break;
		case REFRESHWINDOW:
			RefreshWindow();
			break;
		}
	}
}

/*
 *	Handle menu events
 */

static void DoMenu(UWORD menuNumber, UWORD qualifier)
{
	register UWORD	menu, item, sub;

	while (menuNumber != MENUNULL) {
		menu = MENUNUM(menuNumber);
		item = ITEMNUM(menuNumber);
		sub = SUBNUM(menuNumber);
		switch (menu) {
		case DISK_MENU:
			if (ItemAddress(window->MenuStrip, menuNumber) == NULL)
				return;				/* Menus have changed */
			DoDiskMenu(item, sub);
			break;
		case MOVE_MENU:
			DoMoveMenu(item);
			break;
		}
		menuNumber = ItemAddress(window->MenuStrip, menuNumber)->NextSelect;
	}
}
