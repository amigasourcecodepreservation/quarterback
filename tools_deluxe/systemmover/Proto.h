/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Prototypes
 */

/*
 *	Main.c
 */

void	main(void);

/*
 *	Init.c
 */

void	Init(void);
void	ShutDown(void);

/*
 *	Disk.c
 */

void	SetDir(Dir);
void	DisposeDiskList(void);
void	SetDiskMenu(void);
void	DoDiskInserted(void);
BOOL	CheckDiskList(void);
Dir		LockDisk(TextPtr);
void	UnLockDisks(void);
void	DoDiskMenu(UWORD, UWORD);
void	DoMoveMenu(UWORD);

/*
 *	Window.c
 */

void	DrawDiskName(TextPtr, WORD);
void	CreateWindow(void);
void	RemoveWindow(void);
void	RefreshWindow(void);
BOOL	DialogFilter(IntuiMsgPtr, WORD *);

/*
 *	Gadget.c
 */

void	OffButtons(void);
void	SetButtons(void);
void	DisableInput(void);
void	EnableInput(void);
void	UnSelectAll(void);

/*
 *	List.c
 */

void	DisposeFileList(void);
void	GetFileList(void);

/*
 *	Copy.c
 */

void	BuildName(TextPtr, TextPtr, TextPtr);
void	DoCopy(void);

/*
 *	Remove.c
 */

void	DoRemove(void);

/*
 *	Info.c
 */

WORD	InfoTop(void);
WORD	InfoHeight(void);
WORD	InfoMargin(void);
void	GetInfoRect(RectPtr);
void	EraseInfo(void);
void	ShowInfo(void);
