/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Remove routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <libraries/diskfont.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Request.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>

#include "Mover.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr		mainMsgPort;
extern WindowPtr		window;
extern ScrollListPtr	scrollList;

extern Dir	fromLock;

extern ListHeadPtr	fileList;

extern WORD		moveType;
extern TextPtr	moveDirName;

extern RequestTemplate	removeRequest;

/*
 *	Local variables and definitions
 */

typedef struct _FCList {
	struct _FCList		*Next;
	struct FontContents	FC;
} FCList, *FCListPtr;

#define FCH_SIZE	(sizeof(struct FontContentsHeader))
#define FC_SIZE		(sizeof(struct FontContents))
#define FCL_SIZE	(sizeof(FCList))

/*
 *	Local prototypes
 */

static BOOL	RemoveFont(TextPtr);
static BOOL	RemoveFile(TextPtr);

/*
 *	Remove specified font
 *	Return success status
 */

static BOOL RemoveFont(TextPtr fileName)
{
	register WORD num;
	register BOOL success;
	register Dir dir;
	register File file;
	TextChar name[64];
	register FCListPtr firstFCL, currFCL, newFCL;
	struct FontContentsHeader fch;
	struct FontContents fc;

	success = FALSE;
	firstFCL = currFCL = NULL;
	file = NULL;
/*
	Switch to source fonts directory
*/
	dir = DupLock(fromLock);
	SetDir(dir);
	if ((dir = Lock("fonts", ACCESS_READ)) == NULL)
		goto Exit;
	SetDir(dir);
/*
	Remove font
*/
	if (!DeleteFile(fileName))
		goto Exit;
	success = TRUE;				/* Success even if ".font" file is incorrect */
/*
	Remove entry for font in ".font" file
	First build list of data files to remain in ".font" file
*/
	BuildName(fileName, ".font", name);
	if ((file = Open(name, MODE_OLDFILE)) == NULL)
		goto Exit;
	if (Read(file, (BYTE *) &fch, FCH_SIZE) != FCH_SIZE)
		goto Exit;
	num = 0;
	while (Read(file, (BYTE *) &fc, FC_SIZE) == FC_SIZE){
		if (CmpString(fc.fc_FileName, fileName, (WORD) strlen(fc.fc_FileName),
					   (WORD) strlen(fileName), FALSE) == 0)
			continue;
		num++;
		if ((newFCL = (FCListPtr) AllocMem(FCL_SIZE, 0)) == NULL)
			goto Exit;
		memcpy(&(newFCL->FC), &fc, FC_SIZE);
		newFCL->Next = NULL;
		if (firstFCL == NULL)
			firstFCL = newFCL;
		else
			currFCL->Next = newFCL;
		currFCL = newFCL;
	}
	Close(file);
	file = NULL;
/*
	Write new ".font" file
*/
	if (!DeleteFile(name))
		goto Exit;
	if (num == 0) {								/* No fonts in this family */
		BuildName(fileName, ".metric", name);
		(void) DeleteFile(name);				/* Delete metric file */
		BuildName(fileName, "", name);
		(void) DeleteFile(name);				/* Delete family directory */
	}
	else {
		if ((file = Open(name, MODE_NEWFILE)) == NULL)
			goto Exit;
		fch.fch_NumEntries = num;
		if (Write(file, (BYTE *) &fch, FCH_SIZE) != FCH_SIZE)
			goto Exit;
		currFCL = firstFCL;
		while (num--) {
			if (Write(file, (BYTE *) &(currFCL->FC), FC_SIZE) != FC_SIZE)
				goto Exit;
			currFCL = currFCL->Next;
		}
	}
/*
	All done
*/
Exit:
	while (firstFCL) {
		newFCL = firstFCL->Next;
		FreeMem((BYTE *) firstFCL, FCL_SIZE);
		firstFCL = newFCL;
	}
	if (file)
		Close(file);
	return (success);
}

/*
 *	Remove specified file
 *	Return success status
 */

static BOOL RemoveFile(TextPtr fileName)
{
	register BOOL success;
	register Dir dir;

	success = FALSE;
/*
	Switch to source directory
*/
	dir = DupLock(fromLock);
	SetDir(dir);
	if ((dir = Lock(moveDirName, ACCESS_READ)) == NULL)
		goto Exit;
	SetDir(dir);
/*
	Remove file
*/
	if (!DeleteFile(fileName))
		goto Exit;
/*
	All done
*/
	success = TRUE;
Exit:
	return (success);
}

/*
 *	Handle "Remove" button
 */

void DoRemove()
{
	WORD i;
	BOOL success;
	TextPtr fileName;
	RequestPtr request;

	if (SLNextSelect(scrollList, -1) == -1 ||
		fromLock == NULL || fileList == NULL)
		return;
/*
	Make sure this is what we really want to do
*/
	if ((request = GetRequest(&removeRequest, window, TRUE)) == NULL)
		return;
	OutlineButton(request->ReqGadget, window, request, TRUE);
	SysBeep(5);
	do {
		i = ModalRequest(mainMsgPort, window, DialogFilter);
	} while (i != OK_BUTTON && i != CANCEL_BUTTON);
	EndRequest(request, window);
	DisposeRequest(request);
	if (i == CANCEL_BUTTON)
		return;
/*
	Remove each item one by one
*/
	SetStdPointer(window, POINTER_WAITDELAY);
	DisableInput();
	OffButtons();
	EraseInfo();
	for (;;) {
		i = SLNextSelect(scrollList, -1);
		if (i == -1)
			break;
		fileName = GetListItem(fileList, i);
		switch (moveType) {
		case FONTS_ITEM:
			success = RemoveFont(fileName);
			break;
		default:
			success = RemoveFile(fileName);
			break;
		}
		if (!success) {
			SysBeep(5);
			DisplayBeep(window->WScreen);
			break;
		}
		SLRemoveItem(scrollList, i);
		RemoveListItem(fileList, i);
	}
	SetStdPointer(window, POINTER_ARROW);
	UnSelectAll();
	EnableInput();
	SetButtons();
}
