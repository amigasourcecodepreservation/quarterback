/*
* This file is part of Quarterback and Quarterback Tools.
* Copyright (C) 1996-2018 Canux Corporation
* 
* Quarterback and Quarterback Tools is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Quarterback and Quarterback Tools is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Quarterback and Quarterback Tools.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	System Mover
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Info routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <exec/resident.h>
#include <intuition/intuition.h>
#include <libraries/diskfont.h>
#include <libraries/dos.h>
#include <devices/prtbase.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/DateTime.h>
#include <Toolbox/Utility.h>

#include "Mover.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr		window;
extern ScrollListPtr	scrollList;
extern TextFontPtr		windowFont;

extern Dir	fromLock;

extern ListHeadPtr	fileList;

extern WORD		moveType;
extern TextPtr	moveDirName;

extern TextChar strSampleText[];
extern TextChar	strName[], strVersion[], strRevision[];
extern TextChar	strSize[], strCreated[], strBytes[], strVER[];
extern TextChar	strSelectOptions[];

/*
 *	Local variables and definitions
 */

typedef struct _FontData {
	ULONG					NextSegment, ReturnCode;
	struct DiskFontHeader	DFH;
} FontData, *FontDataPtr;

typedef struct _LibData {
	ULONG	NextSegment;
	UWORD	Data[1];			/* First word in segment data */
} LibData, *LibDataPtr;

#define FILEBUFF_SIZE	488		/* Optimized for floppies */

static UBYTE	fileBuff[FILEBUFF_SIZE];

static WORD	buffPtr, buffSize;

/*
 *	Local prototypes
 */

static void	ClearBuff(void);
static WORD	GetByte(File);

static void	GetInfoBaselines(WORD *, WORD *);
static void	DrawString(RastPtr, TextPtr);
static void	ShowFontInfo(TextPtr);
static void	ShowPrinterInfo(TextPtr);
static void	ShowLibsInfo(TextPtr);
static void	ShowGenInfo(TextPtr, BOOL);
static void	ShowHelpInfo(void);

/*
 *	Buffered I/O for GetGenInfo()
 *	Returns -1 if no more bytes
 */

static void ClearBuff()
{
	buffPtr = buffSize = FILEBUFF_SIZE;
}

static WORD GetByte(File file)
{
	if (buffPtr == buffSize) {
		if ((buffSize = Read(file, fileBuff, FILEBUFF_SIZE)) <= 0)
			return (-1);
		buffPtr = 0;
	}
	return ((WORD) fileBuff[buffPtr++]);
}


/*
 *	Return the location of the top of the info area
 */

WORD InfoTop()
{
	register WORD top;
	GadgetPtr copyBtn, listBox;

	copyBtn = GadgetItem(window->FirstGadget, COPY_BUTTON);
	listBox = GadgetItem(window->FirstGadget, LIST_BOX);
	top = listBox->TopEdge + listBox->Height + copyBtn->Height/2 - listBox->LeftEdge/4;
	return (top);
}

/*
 *	Return the height of the info area
 */

WORD InfoHeight()
{
	register WORD height;

	height = window->Height - window->BorderBottom - InfoTop();
	return (height);
}

/*
 *	Return margin around info area
 */

WORD InfoMargin()
{
	register WORD margin;
	GadgetPtr listBox;

	listBox = GadgetItem(window->FirstGadget, LIST_BOX);
	margin = listBox->LeftEdge/4;
	return (margin);
}

/*
 *	Return rectangle of info region (inset by margins)
 */

void GetInfoRect(RectPtr rect)
{
	WORD margin;

	margin = InfoMargin();
	rect->MinX = window->BorderLeft + margin;
	rect->MaxX = window->Width - window->BorderRight - 1 - margin;
	rect->MinY = InfoTop() + margin;
	rect->MaxY = window->Height - window->BorderBottom - 1 - margin;
}

/*
 *	Get baseline positions for two lines of info text
 */

static void GetInfoBaselines(register WORD *base1, register WORD *base2)
{
	register WORD height, ySize;
	Rectangle rect;

	GetInfoRect(&rect);
	height = rect.MaxY - rect.MinY + 1;
	ySize = windowFont->tf_YSize;
	*base1 = rect.MinY + (height - 2*ySize)/4;
	if (*base1 < rect.MinY)
		*base1 = rect.MinY;
	*base1 += windowFont->tf_Baseline;
	*base2 = rect.MinY + 3*(height - 2*ySize)/4 + ySize;
	if (*base2 + ySize > rect.MaxY)
		*base2 = rect.MaxY - ySize;
	*base2 += windowFont->tf_Baseline;
}

/*
 *	Draw text of string terminated with NULL, CR, or LF
 */

static void DrawString(RastPtr rPort, TextPtr text)
{
	register WORD len;

	for (len = 0; text[len] && text[len] != '\r' && text[len] != '\n'; len++); 
	Text(rPort, text, len);
}

/*
 *	Show font info
 */

static void ShowFontInfo(TextPtr fileName)
{
	register WORD baseline;
	register TextFontPtr font;
	BPTR fontSeg;
	FontDataPtr fontData;
	RastPtr rPort = window->RPort;
	Rectangle rect;

/*
	Load the font file
*/
	if ((fontSeg = LoadSeg(fileName)) == NULL)
		goto Exit;
	fontData = (FontDataPtr) BADDR(fontSeg);
	font = &(fontData->DFH.dfh_TF);
/*
	Draw sample text in this font
*/
	GetInfoRect(&rect);
	baseline = font->tf_Baseline + (rect.MaxY - rect.MinY + 1 - font->tf_YSize)/2;
	if (baseline < font->tf_Baseline)
		baseline = font->tf_Baseline;
	baseline += rect.MinY;
	if (baseline > rect.MaxY)
		baseline = rect.MaxY;
	SetAPen(rPort, _tbPenBlack);
	SetDrMd(rPort, JAM1);
	SetFont(rPort, font);
	Move(rPort, rect.MinX, baseline);
	DrawString(rPort, strSampleText);
/*
	Clean up
*/
	UnLoadSeg(fontSeg);
Exit:
	SetFont(rPort, windowFont);
}

/*
 *	Show printer driver info
 */

static void ShowPrinterInfo(TextPtr fileName)
{
	WORD base1, base2, offset;
	BPTR segData;
	struct PrinterSegment *printSeg;
	RastPtr rPort = window->RPort;
	Rectangle rect;
	UBYTE buff[10];

/*
	Load the library or device file
*/
	if ((segData = LoadSeg(fileName)) == NULL)
		return;
	printSeg = (struct PrinterSegment *) BADDR(segData);
/*
	Display info
*/
	GetInfoRect(&rect);
	offset = InfoMargin();
	GetInfoBaselines(&base1, &base2);
	SetAPen(rPort, _tbPenBlack);
	SetDrMd(rPort, JAM1);
	SetFont(rPort, windowFont);
	Move(rPort, rect.MinX + offset, base1);
	DrawString(rPort, strName);
	DrawString(rPort, printSeg->ps_PED.ped_PrinterName);
	Move(rPort, rect.MinX + offset, base2);
	DrawString(rPort, strVersion);
	NumToString(printSeg->ps_Version, buff);
	DrawString(rPort, buff);
	Move(rPort, (rect.MinX + rect.MaxX)/2, base2);
	DrawString(rPort, strRevision);
	NumToString(printSeg->ps_Revision, buff);
	DrawString(rPort, buff);
/*
	Clean up
*/
	UnLoadSeg(segData);
}

/*
 *	Show library and device info
 */

static void ShowLibsInfo(TextPtr fileName)
{
	register WORD i, size;
	WORD base1, base2, offset;
	BPTR segData;
	LONG *longPtr;
	register LibDataPtr libData;
	struct Resident *res;
	RastPtr rPort = window->RPort;
	Rectangle rect;
	UBYTE buff[10];

/*
	Load the library or device file
*/
	if ((segData = LoadSeg(fileName)) == NULL)
		return;
	libData = (LibDataPtr) BADDR(segData);
/*
	Find start of resident struct
*/
	longPtr = (LONG *) libData;
	size = (*(longPtr - 1) - sizeof(LONG) - sizeof(struct Resident))/sizeof(WORD);
	for (i = 0; i <= size; i++) {
		if (libData->Data[i] == RTC_MATCHWORD)
			break;
	}
	if (libData->Data[i] != RTC_MATCHWORD) {
		UnLoadSeg(segData);
		ShowGenInfo(fileName, FALSE);
		return;
	}
	res = (struct Resident *) &(libData->Data[i]);
/*
	Display info
*/
	GetInfoRect(&rect);
	offset = InfoMargin();
	GetInfoBaselines(&base1, &base2);
	SetAPen(rPort, _tbPenBlack);
	SetDrMd(rPort, JAM1);
	SetFont(rPort, windowFont);
	Move(rPort, rect.MinX + offset, base1);
	DrawString(rPort, res->rt_IdString);
	Move(rPort, rect.MinX + offset, base2);
	DrawString(rPort, strVersion);
	NumToString(res->rt_Version, buff);
	DrawString(rPort, buff);
/*
	Clean up
*/
	UnLoadSeg(segData);
}

/*
 *	Show general file info
 */

static void ShowGenInfo(TextPtr fileName, BOOL hasVers)
{
	register WORD i, len, byte, frac;
	WORD base1, base2, offset;
	BPTR lock;
	File file;
	struct FileInfoBlock *fib;
	RastPtr rPort = window->RPort;
	Rectangle rect;
	UBYTE buff[100];

	fib = NULL;
	lock = NULL;
	file = NULL;
/*
	Get information on the file
*/
	if ((fib = AllocMem(sizeof(struct FileInfoBlock), 0)) == NULL)
		return;
	if ((lock = Lock(fileName, ACCESS_READ)) == NULL || !Examine(lock, fib))
		goto Exit;
/*
	Search for version string
*/
	if (!hasVers ||
		(file = Open(fileName, MODE_OLDFILE)) == NULL) {
		hasVers = FALSE;
		goto ShowInfo;
	}
	ClearBuff();
	for (i = 0; i < 5; i++) {
		if ((byte = GetByte(file)) == -1) {
			hasVers = FALSE;
			goto ShowInfo;
		}
		buff[i] = byte;
	}
	while (CmpString(buff, strVER, 5, 5, TRUE) != 0) {
		for (i = 1; i < 5; i++)
			buff[i - 1] = buff[i];
		if ((byte = GetByte(file)) == -1) {
			hasVers = FALSE;
			break;
		}
		buff[4] = byte;
	}
/*
	Show info
*/
ShowInfo:
	GetInfoRect(&rect);
	offset = InfoMargin();
	GetInfoBaselines(&base1, &base2);
	SetAPen(rPort, _tbPenBlack);
	SetDrMd(rPort, JAM1);
	SetFont(rPort, windowFont);
/*
	Show version info (if available)
*/
	if (hasVers) {
		for (i = 0; i < 50; i++) {
			if ((byte = GetByte(file)) == -1 ||
				byte == '\0' || byte == '\n' || byte == '\r')
				break;
			buff[i] = byte;
		}
		buff[i] = '\0';
		for (i = 0; buff[i] == ' '; i++) ;
		Move(rPort, rect.MinX + offset, base1);
		DrawString(rPort, buff + i);
	}
/*
	Show file size
*/
	Move(rPort, rect.MinX + offset, (hasVers) ? base2 : base1);
	DrawString(rPort, strSize);
	NumToString(fib->fib_Size, buff);
	len = strlen(buff);
	if ((frac = len % 3) != 0)
		Text(rPort, buff, frac);
	for (i = frac; i < len; i += 3) {
		if (frac || i > frac)
			Text(rPort, ",", 1);
		Text(rPort, buff + i, 3);
	}
	DrawString(rPort, strBytes);
/*
	Show creation date (if no version info available)
*/
	if (!hasVers) {
		Move(rPort, rect.MinX + offset, base2);
		DrawString(rPort, strCreated);
		DateString(&(fib->fib_Date), DATE_ABBR, buff);
		DrawString(rPort, buff);
		DrawString(rPort, ", ");
		TimeString(&(fib->fib_Date), FALSE, TRUE, buff);
		DrawString(rPort, buff);
	}
/*
	Clean up
*/
Exit:
	if (file)
		Close(file);
	if (lock)
		UnLock(lock);
	if (fib)
		FreeMem(fib, sizeof(struct FileInfoBlock));
}

/*
 *	Show help information
 */

static void ShowHelpInfo()
{
	WORD		len, left, top;
	RastPtr		rPort = window->RPort;
	Rectangle	rect;

	GetInfoRect(&rect);
	SetFont(rPort, windowFont);
	SetDrMd(rPort, JAM1);
	SetAPen(rPort, _tbPenBlack);
	top = InfoTop();
	len = strlen(strSelectOptions);
	left = (rect.MinX + rect.MaxX - TextLength(rPort, strSelectOptions, len))/2;
	top += (InfoHeight() - windowFont->tf_YSize)/2 + windowFont->tf_Baseline;
	Move(rPort, left, top);
	Text(rPort, strSelectOptions, len);
}

/*
 *	Erase info area of window
 */

void EraseInfo()
{
	RastPtr		rPort = window->RPort;
	Rectangle	rect;

	GetInfoRect(&rect);
	SetAPen(rPort, _tbPenLight);
	SetDrMd(rPort, JAM1);
	RectFill(rPort, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
}

/*
 *	Show info for selected item(s)
 */

void ShowInfo()
{
	WORD				item;
	BOOL				updating;
	register TextPtr	fileName;
	register Dir		dir;
	LayerPtr			layer = window->WLayer;
	Rectangle			rect;

	EraseInfo();
/*
	If no "from" disk selected, just draw help info
*/
	if (fromLock == NULL) {
		ShowHelpInfo();
		return;
	}
/*
	Find item to show
*/
	item = SLNextSelect(scrollList, -1);
	if (item == -1 || SLNextSelect(scrollList, item) != -1)
		return;
	updating = (layer->Flags & LAYERUPDATING);
	if (!updating)
		SetStdPointer(window, POINTER_WAITDELAY);	// Has problems if window is updating
/*
	Set clip region to info area
*/
	GetInfoRect(&rect);
	if (updating)
		EndUpdate(layer, FALSE);
	SetRectClip(layer, &rect);
	if (updating)
		BeginUpdate(layer);
/*
	Switch to appropriate source directory
*/
	dir = DupLock(fromLock);
	SetDir(dir);
	if ((dir = Lock(moveDirName, ACCESS_READ)) == NULL)
		goto Exit;
	SetDir(dir);
	fileName = GetListItem(fileList, item);
/*
	Show appropriate type of info
*/
	switch (moveType) {
	case FONTS_ITEM:
		ShowFontInfo(fileName);
		break;
	case PRINTERS_ITEM:
		ShowPrinterInfo(fileName);
		break;
	case LIBRARIES_ITEM:
	case DEVICES_ITEM:
		ShowLibsInfo(fileName);
		break;
	case HANDLERS_ITEM:
	case CLI_ITEM:
		ShowGenInfo(fileName, TRUE);
		break;
	case KEYMAPS_ITEM:
	case SCRIPTS_ITEM:
		ShowGenInfo(fileName, FALSE);
		break;
	}
/*
	Clean up
*/
Exit:
	if (updating)
		EndUpdate(layer, FALSE);
	SetClip(layer, NULL);
	if (updating)
		BeginUpdate(layer);
	SetStdPointer(window, POINTER_ARROW);
}
